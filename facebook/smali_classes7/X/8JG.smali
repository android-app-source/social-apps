.class public final enum LX/8JG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8JG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8JG;

.field public static final enum DOWN:LX/8JG;

.field public static final enum DOWNLEFT:LX/8JG;

.field public static final enum DOWNRIGHT:LX/8JG;

.field public static final enum LEFT:LX/8JG;

.field public static final enum RIGHT:LX/8JG;

.field public static final enum UP:LX/8JG;

.field public static final enum UPLEFT:LX/8JG;

.field public static final enum UPRIGHT:LX/8JG;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1327948
    new-instance v0, LX/8JG;

    const-string v1, "UP"

    invoke-direct {v0, v1, v3}, LX/8JG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8JG;->UP:LX/8JG;

    .line 1327949
    new-instance v0, LX/8JG;

    const-string v1, "DOWN"

    invoke-direct {v0, v1, v4}, LX/8JG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8JG;->DOWN:LX/8JG;

    .line 1327950
    new-instance v0, LX/8JG;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v5}, LX/8JG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8JG;->LEFT:LX/8JG;

    .line 1327951
    new-instance v0, LX/8JG;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v6}, LX/8JG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8JG;->RIGHT:LX/8JG;

    .line 1327952
    new-instance v0, LX/8JG;

    const-string v1, "DOWNLEFT"

    invoke-direct {v0, v1, v7}, LX/8JG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8JG;->DOWNLEFT:LX/8JG;

    .line 1327953
    new-instance v0, LX/8JG;

    const-string v1, "DOWNRIGHT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/8JG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8JG;->DOWNRIGHT:LX/8JG;

    .line 1327954
    new-instance v0, LX/8JG;

    const-string v1, "UPLEFT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/8JG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8JG;->UPLEFT:LX/8JG;

    .line 1327955
    new-instance v0, LX/8JG;

    const-string v1, "UPRIGHT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/8JG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8JG;->UPRIGHT:LX/8JG;

    .line 1327956
    const/16 v0, 0x8

    new-array v0, v0, [LX/8JG;

    sget-object v1, LX/8JG;->UP:LX/8JG;

    aput-object v1, v0, v3

    sget-object v1, LX/8JG;->DOWN:LX/8JG;

    aput-object v1, v0, v4

    sget-object v1, LX/8JG;->LEFT:LX/8JG;

    aput-object v1, v0, v5

    sget-object v1, LX/8JG;->RIGHT:LX/8JG;

    aput-object v1, v0, v6

    sget-object v1, LX/8JG;->DOWNLEFT:LX/8JG;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/8JG;->DOWNRIGHT:LX/8JG;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/8JG;->UPLEFT:LX/8JG;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/8JG;->UPRIGHT:LX/8JG;

    aput-object v2, v0, v1

    sput-object v0, LX/8JG;->$VALUES:[LX/8JG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1327947
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8JG;
    .locals 1

    .prologue
    .line 1327945
    const-class v0, LX/8JG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8JG;

    return-object v0
.end method

.method public static values()[LX/8JG;
    .locals 1

    .prologue
    .line 1327946
    sget-object v0, LX/8JG;->$VALUES:[LX/8JG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8JG;

    return-object v0
.end method
