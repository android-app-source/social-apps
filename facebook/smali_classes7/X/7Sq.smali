.class public LX/7Sq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/7Sp;

.field public b:LX/5Pa;

.field private c:LX/1FZ;

.field private d:I

.field private e:I

.field private f:F

.field private g:F

.field public h:I

.field private i:I

.field private j:F

.field private k:F

.field private final l:[F

.field private m:[LX/7Sr;

.field private n:I

.field private o:I

.field private p:F

.field private q:F

.field private r:F


# direct methods
.method public constructor <init>(LX/1FZ;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v5, 0x60

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1209231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1209232
    iput-object p1, p0, LX/7Sq;->c:LX/1FZ;

    .line 1209233
    new-instance v0, LX/7Sp;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, LX/7Sp;-><init>(I)V

    iput-object v0, p0, LX/7Sq;->a:LX/7Sp;

    .line 1209234
    new-array v0, v5, [F

    iput-object v0, p0, LX/7Sq;->l:[F

    .line 1209235
    new-array v0, v5, [LX/7Sr;

    iput-object v0, p0, LX/7Sq;->m:[LX/7Sr;

    .line 1209236
    iput v3, p0, LX/7Sq;->d:I

    .line 1209237
    iput v3, p0, LX/7Sq;->e:I

    .line 1209238
    iput v2, p0, LX/7Sq;->f:F

    .line 1209239
    iput v2, p0, LX/7Sq;->g:F

    .line 1209240
    const/4 v0, -0x1

    iput v0, p0, LX/7Sq;->h:I

    .line 1209241
    iput v3, p0, LX/7Sq;->i:I

    .line 1209242
    iput v2, p0, LX/7Sq;->j:F

    .line 1209243
    iput v2, p0, LX/7Sq;->k:F

    .line 1209244
    iput v3, p0, LX/7Sq;->n:I

    .line 1209245
    iput v3, p0, LX/7Sq;->o:I

    .line 1209246
    iput v4, p0, LX/7Sq;->p:F

    .line 1209247
    iput v4, p0, LX/7Sq;->q:F

    .line 1209248
    iput v2, p0, LX/7Sq;->r:F

    .line 1209249
    return-void
.end method

.method public static a(LX/7Sq;Landroid/graphics/Typeface;III)Z
    .locals 10

    .prologue
    .line 1209250
    iput p3, p0, LX/7Sq;->d:I

    .line 1209251
    iput p4, p0, LX/7Sq;->e:I

    .line 1209252
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 1209253
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1209254
    int-to-float v0, p2

    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1209255
    const/4 v0, -0x1

    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1209256
    invoke-virtual {v6, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1209257
    invoke-virtual {v6}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    .line 1209258
    iget v1, v0, Landroid/graphics/Paint$FontMetrics;->bottom:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, LX/7Sq;->f:F

    .line 1209259
    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LX/7Sq;->g:F

    .line 1209260
    const/4 v0, 0x2

    new-array v1, v0, [C

    .line 1209261
    const/4 v0, 0x0

    iput v0, p0, LX/7Sq;->k:F

    iput v0, p0, LX/7Sq;->j:F

    .line 1209262
    const/4 v0, 0x2

    new-array v3, v0, [F

    .line 1209263
    const/4 v2, 0x0

    .line 1209264
    const/16 v0, 0x20

    :goto_0
    const/16 v4, 0x7e

    if-gt v0, v4, :cond_1

    .line 1209265
    const/4 v4, 0x0

    aput-char v0, v1, v4

    .line 1209266
    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v6, v1, v4, v5, v3}, Landroid/graphics/Paint;->getTextWidths([CII[F)I

    .line 1209267
    iget-object v4, p0, LX/7Sq;->l:[F

    const/4 v5, 0x0

    aget v5, v3, v5

    aput v5, v4, v2

    .line 1209268
    iget-object v4, p0, LX/7Sq;->l:[F

    aget v4, v4, v2

    iget v5, p0, LX/7Sq;->j:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 1209269
    iget-object v4, p0, LX/7Sq;->l:[F

    aget v4, v4, v2

    iput v4, p0, LX/7Sq;->j:F

    .line 1209270
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 1209271
    add-int/lit8 v0, v0, 0x1

    int-to-char v0, v0

    goto :goto_0

    .line 1209272
    :cond_1
    const/4 v0, 0x0

    const/16 v4, 0x20

    aput-char v4, v1, v0

    .line 1209273
    const/4 v0, 0x0

    const/4 v4, 0x1

    invoke-virtual {v6, v1, v0, v4, v3}, Landroid/graphics/Paint;->getTextWidths([CII[F)I

    .line 1209274
    iget-object v0, p0, LX/7Sq;->l:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    aput v3, v0, v2

    .line 1209275
    iget-object v0, p0, LX/7Sq;->l:[F

    aget v0, v0, v2

    iget v3, p0, LX/7Sq;->j:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    .line 1209276
    iget-object v0, p0, LX/7Sq;->l:[F

    aget v0, v0, v2

    iput v0, p0, LX/7Sq;->j:F

    .line 1209277
    :cond_2
    iget v0, p0, LX/7Sq;->f:F

    iput v0, p0, LX/7Sq;->k:F

    .line 1209278
    iget v0, p0, LX/7Sq;->j:F

    float-to-int v0, v0

    iget v2, p0, LX/7Sq;->d:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    iput v0, p0, LX/7Sq;->n:I

    .line 1209279
    iget v0, p0, LX/7Sq;->k:F

    float-to-int v0, v0

    iget v2, p0, LX/7Sq;->e:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    iput v0, p0, LX/7Sq;->o:I

    .line 1209280
    iget v0, p0, LX/7Sq;->n:I

    iget v2, p0, LX/7Sq;->o:I

    if-le v0, v2, :cond_4

    iget v0, p0, LX/7Sq;->n:I

    .line 1209281
    :goto_1
    const/4 v2, 0x6

    if-lt v0, v2, :cond_3

    const/16 v2, 0xb4

    if-le v0, v2, :cond_5

    .line 1209282
    :cond_3
    const/4 v0, 0x0

    .line 1209283
    :goto_2
    return v0

    .line 1209284
    :cond_4
    iget v0, p0, LX/7Sq;->o:I

    goto :goto_1

    .line 1209285
    :cond_5
    const/16 v2, 0x18

    if-gt v0, v2, :cond_7

    .line 1209286
    const/16 v0, 0x100

    iput v0, p0, LX/7Sq;->i:I

    .line 1209287
    :goto_3
    iget-object v0, p0, LX/7Sq;->c:LX/1FZ;

    iget v2, p0, LX/7Sq;->i:I

    iget v3, p0, LX/7Sq;->i:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v2, v3, v4}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v9

    .line 1209288
    invoke-virtual {v9}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/graphics/Bitmap;

    .line 1209289
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1209290
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 1209291
    iget v2, p0, LX/7Sq;->d:I

    int-to-float v4, v2

    .line 1209292
    iget v2, p0, LX/7Sq;->o:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    iget v3, p0, LX/7Sq;->g:F

    sub-float/2addr v2, v3

    iget v3, p0, LX/7Sq;->e:I

    int-to-float v3, v3

    sub-float v5, v2, v3

    .line 1209293
    const/16 v2, 0x20

    move v8, v2

    :goto_4
    const/16 v2, 0x7e

    if-gt v8, v2, :cond_a

    .line 1209294
    const/4 v2, 0x0

    aput-char v8, v1, v2

    .line 1209295
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText([CIIFFLandroid/graphics/Paint;)V

    .line 1209296
    iget v2, p0, LX/7Sq;->n:I

    int-to-float v2, v2

    add-float/2addr v2, v4

    .line 1209297
    iget v3, p0, LX/7Sq;->n:I

    int-to-float v3, v3

    add-float/2addr v3, v2

    iget v4, p0, LX/7Sq;->d:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, p0, LX/7Sq;->i:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_6

    .line 1209298
    iget v2, p0, LX/7Sq;->d:I

    int-to-float v2, v2

    .line 1209299
    iget v3, p0, LX/7Sq;->o:I

    int-to-float v3, v3

    add-float/2addr v5, v3

    .line 1209300
    :cond_6
    add-int/lit8 v3, v8, 0x1

    int-to-char v3, v3

    move v8, v3

    move v4, v2

    goto :goto_4

    .line 1209301
    :cond_7
    const/16 v2, 0x28

    if-gt v0, v2, :cond_8

    .line 1209302
    const/16 v0, 0x200

    iput v0, p0, LX/7Sq;->i:I

    goto :goto_3

    .line 1209303
    :cond_8
    const/16 v2, 0x50

    if-gt v0, v2, :cond_9

    .line 1209304
    const/16 v0, 0x400

    iput v0, p0, LX/7Sq;->i:I

    goto :goto_3

    .line 1209305
    :cond_9
    const/16 v0, 0x800

    iput v0, p0, LX/7Sq;->i:I

    goto :goto_3

    .line 1209306
    :cond_a
    const/4 v2, 0x0

    const/16 v3, 0x20

    aput-char v3, v1, v2

    .line 1209307
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText([CIIFFLandroid/graphics/Paint;)V

    .line 1209308
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 1209309
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 1209310
    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p0, LX/7Sq;->h:I

    .line 1209311
    const/16 v0, 0xde1

    iget v1, p0, LX/7Sq;->h:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1209312
    const/16 v0, 0xde1

    const/16 v1, 0x2801

    const/high16 v2, 0x46180000    # 9728.0f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1209313
    const/16 v0, 0xde1

    const/16 v1, 0x2800

    const v2, 0x46180400    # 9729.0f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1209314
    const/16 v0, 0xde1

    const/16 v1, 0x2802

    const v2, 0x47012f00    # 33071.0f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1209315
    const/16 v0, 0xde1

    const/16 v1, 0x2803

    const v2, 0x47012f00    # 33071.0f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1209316
    const/16 v0, 0xde1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v7, v2}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 1209317
    const/16 v0, 0xde1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1209318
    invoke-virtual {v9}, LX/1FJ;->close()V

    .line 1209319
    const/4 v3, 0x0

    .line 1209320
    const/4 v4, 0x0

    .line 1209321
    const/4 v0, 0x0

    move v7, v0

    :goto_5
    const/16 v0, 0x60

    if-ge v7, v0, :cond_c

    .line 1209322
    iget-object v8, p0, LX/7Sq;->m:[LX/7Sr;

    new-instance v0, LX/7Sr;

    iget v1, p0, LX/7Sq;->i:I

    int-to-float v1, v1

    iget v2, p0, LX/7Sq;->i:I

    int-to-float v2, v2

    iget v5, p0, LX/7Sq;->n:I

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    iget v6, p0, LX/7Sq;->o:I

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    invoke-direct/range {v0 .. v6}, LX/7Sr;-><init>(FFFFFF)V

    aput-object v0, v8, v7

    .line 1209323
    iget v0, p0, LX/7Sq;->n:I

    int-to-float v0, v0

    add-float/2addr v0, v3

    .line 1209324
    iget v1, p0, LX/7Sq;->n:I

    int-to-float v1, v1

    add-float/2addr v1, v0

    iget v2, p0, LX/7Sq;->i:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_b

    .line 1209325
    const/4 v0, 0x0

    .line 1209326
    iget v1, p0, LX/7Sq;->o:I

    int-to-float v1, v1

    add-float/2addr v4, v1

    .line 1209327
    :cond_b
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v3, v0

    goto :goto_5

    .line 1209328
    :cond_c
    const/4 v0, 0x1

    goto/16 :goto_2
.end method


# virtual methods
.method public final a(LX/5Pa;)V
    .locals 12

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1209329
    move-object v0, p0

    move v2, v1

    move v3, v1

    move v4, v1

    move-object v5, p1

    .line 1209330
    iput-object v5, v0, LX/7Sq;->b:LX/5Pa;

    .line 1209331
    iget-object v6, v0, LX/7Sq;->b:LX/5Pa;

    const-string v7, "vTextColor"

    move v8, v1

    move v9, v2

    move v10, v3

    move v11, v4

    invoke-virtual/range {v6 .. v11}, LX/5Pa;->a(Ljava/lang/String;FFFF)LX/5Pa;

    .line 1209332
    iget-object v6, v0, LX/7Sq;->a:LX/7Sp;

    iget v7, v0, LX/7Sq;->h:I

    .line 1209333
    const/16 v8, 0xde1

    invoke-static {v8, v7}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1209334
    invoke-static {v6}, LX/7Sp;->a(LX/7Sp;)V

    .line 1209335
    return-void
.end method

.method public final a(Ljava/lang/String;FF)V
    .locals 10

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 1209336
    iget v0, p0, LX/7Sq;->o:I

    int-to-float v0, v0

    iget v1, p0, LX/7Sq;->q:F

    mul-float v4, v0, v1

    .line 1209337
    iget v0, p0, LX/7Sq;->n:I

    int-to-float v0, v0

    iget v1, p0, LX/7Sq;->p:F

    mul-float v3, v0, v1

    .line 1209338
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    .line 1209339
    div-float v0, v3, v5

    iget v1, p0, LX/7Sq;->d:I

    int-to-float v1, v1

    iget v2, p0, LX/7Sq;->p:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    add-float v1, p2, v0

    .line 1209340
    div-float v0, v4, v5

    iget v2, p0, LX/7Sq;->e:I

    int-to-float v2, v2

    iget v5, p0, LX/7Sq;->q:F

    mul-float/2addr v2, v5

    sub-float/2addr v0, v2

    add-float v2, p3, v0

    .line 1209341
    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v9, :cond_1

    .line 1209342
    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x20

    .line 1209343
    if-ltz v0, :cond_0

    const/16 v5, 0x60

    if-lt v0, v5, :cond_2

    .line 1209344
    :cond_0
    const/16 v0, 0x5f

    move v7, v0

    .line 1209345
    :goto_1
    iget-object v0, p0, LX/7Sq;->a:LX/7Sp;

    iget-object v5, p0, LX/7Sq;->m:[LX/7Sr;

    aget-object v5, v5, v7

    iget-object v6, p0, LX/7Sq;->b:LX/5Pa;

    invoke-virtual/range {v0 .. v6}, LX/7Sp;->a(FFFFLX/7Sr;LX/5Pa;)V

    .line 1209346
    iget-object v0, p0, LX/7Sq;->l:[F

    aget v0, v0, v7

    iget v5, p0, LX/7Sq;->r:F

    add-float/2addr v0, v5

    iget v5, p0, LX/7Sq;->p:F

    mul-float/2addr v0, v5

    add-float/2addr v1, v0

    .line 1209347
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    .line 1209348
    :cond_1
    return-void

    :cond_2
    move v7, v0

    goto :goto_1
.end method
