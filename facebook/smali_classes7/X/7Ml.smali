.class public final LX/7Ml;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Iw;


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

.field private b:Z

.field private c:I


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1198995
    iput-object p1, p0, LX/7Ml;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1198996
    iput-boolean v0, p0, LX/7Ml;->b:Z

    .line 1198997
    iput v0, p0, LX/7Ml;->c:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;B)V
    .locals 0

    .prologue
    .line 1198994
    invoke-direct {p0, p1}, LX/7Ml;-><init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 1198998
    iget-object v0, p0, LX/7Ml;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    invoke-static {v0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->J(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    .line 1198999
    iget-object v0, p0, LX/7Ml;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    invoke-static {v0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->I(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    .line 1199000
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 1198983
    iget-object v0, p0, LX/7Ml;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1198984
    iget-boolean v1, p0, LX/7Ml;->b:Z

    if-eqz v1, :cond_0

    iget v1, p0, LX/7Ml;->c:I

    if-eq v1, v0, :cond_1

    .line 1198985
    :cond_0
    iput v0, p0, LX/7Ml;->c:I

    .line 1198986
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/7Ml;->b:Z

    .line 1198987
    iget-object v1, p0, LX/7Ml;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->L:LX/0AU;

    iget-object v2, p0, LX/7Ml;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    invoke-static {v2}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->getCurrentVideoId(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)Ljava/lang/String;

    move-result-object v2

    .line 1198988
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1198989
    const-string v4, "cast_device_count"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1198990
    const-string v4, "video_id"

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1198991
    const-string v4, "cast_availability"

    invoke-static {v1, v4, v3}, LX/0AU;->a(LX/0AU;Ljava/lang/String;Ljava/util/Map;)V

    .line 1198992
    :cond_1
    invoke-direct {p0}, LX/7Ml;->e()V

    .line 1198993
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1198981
    invoke-direct {p0}, LX/7Ml;->e()V

    .line 1198982
    return-void
.end method

.method public final dK_()V
    .locals 0

    .prologue
    .line 1198979
    invoke-direct {p0}, LX/7Ml;->e()V

    .line 1198980
    return-void
.end method

.method public final dL_()V
    .locals 2

    .prologue
    .line 1198972
    iget-object v0, p0, LX/7Ml;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iget-object v1, p0, LX/7Ml;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v1}, LX/37Y;->n()I

    move-result v1

    .line 1198973
    iput v1, v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->C:I

    .line 1198974
    iget-object v0, p0, LX/7Ml;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iget v0, v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->B:I

    if-gtz v0, :cond_0

    .line 1198975
    iget-object v0, p0, LX/7Ml;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iget-object v1, p0, LX/7Ml;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    invoke-virtual {v1}, LX/37Y;->o()I

    move-result v1

    .line 1198976
    iput v1, v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->B:I

    .line 1198977
    :cond_0
    iget-object v0, p0, LX/7Ml;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    invoke-virtual {v0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->i()V

    .line 1198978
    return-void
.end method
