.class public LX/73w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/73v;


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final c:Z


# instance fields
.field private A:J

.field private B:Z

.field public C:I

.field public D:J

.field private final E:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final F:LX/74E;

.field private final G:LX/0oz;

.field public a:LX/74L;

.field private final d:LX/0Zb;

.field public final e:LX/03V;

.field private final f:LX/0kb;

.field public final g:LX/0So;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:I

.field public o:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:J

.field private s:J

.field private t:J

.field private u:J

.field private v:J

.field private w:J

.field private x:J

.field private y:J

.field private z:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1166959
    const-class v0, LX/73w;

    sput-object v0, LX/73w;->b:Ljava/lang/Class;

    .line 1166960
    const-string v0, "PhotoFlowLogger"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, LX/73w;->c:Z

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/03V;LX/0kb;LX/0So;LX/0Ot;LX/0Or;LX/74L;Ljava/lang/String;LX/74E;LX/0oz;)V
    .locals 1
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/base/analytics/upload/PhotoUploadResizeScheme;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0kb;",
            "LX/0So;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/74L;",
            "Ljava/lang/String;",
            "LX/74E;",
            "LX/0oz;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1166962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1166963
    const/4 v0, -0x1

    iput v0, p0, LX/73w;->n:I

    .line 1166964
    const-string v0, ""

    iput-object v0, p0, LX/73w;->o:Ljava/lang/String;

    .line 1166965
    iput-object p1, p0, LX/73w;->d:LX/0Zb;

    .line 1166966
    iput-object p2, p0, LX/73w;->e:LX/03V;

    .line 1166967
    iput-object p3, p0, LX/73w;->f:LX/0kb;

    .line 1166968
    iput-object p4, p0, LX/73w;->g:LX/0So;

    .line 1166969
    iput-object p5, p0, LX/73w;->h:LX/0Ot;

    .line 1166970
    const/4 v0, 0x0

    iput-object v0, p0, LX/73w;->j:Ljava/lang/String;

    .line 1166971
    iput-object p6, p0, LX/73w;->E:LX/0Or;

    .line 1166972
    iput-object p7, p0, LX/73w;->a:LX/74L;

    .line 1166973
    iput-object p8, p0, LX/73w;->p:Ljava/lang/String;

    .line 1166974
    iput-object p9, p0, LX/73w;->F:LX/74E;

    .line 1166975
    iput-object p10, p0, LX/73w;->G:LX/0oz;

    .line 1166976
    return-void
.end method

.method private static a(JJ)J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 1166961
    cmp-long v2, p2, v0

    if-lez v2, :cond_0

    long-to-float v0, p0

    long-to-float v1, p2

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    div-float/2addr v0, v1

    float-to-long v0, v0

    :cond_0
    return-wide v0
.end method

.method private static a(JJJ)J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 1166957
    invoke-static {p2, p3, p4, p5}, LX/73w;->a(JJ)J

    move-result-wide v2

    .line 1166958
    cmp-long v4, v2, v0

    if-lez v4, :cond_0

    div-long v0, p0, v2

    :cond_0
    return-wide v0
.end method

.method public static a(LX/0QB;)LX/73w;
    .locals 1

    .prologue
    .line 1166956
    invoke-static {p0}, LX/73w;->b(LX/0QB;)LX/73w;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;LX/0ck;LX/742;)LX/74b;
    .locals 2

    .prologue
    .line 1166955
    new-instance v1, LX/74b;

    iget-object v0, p0, LX/73w;->E:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, p1, p2, p3, v0}, LX/74b;-><init>(Ljava/lang/String;LX/0ck;LX/742;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74R;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1166953
    iget-object v0, p0, LX/73w;->j:Ljava/lang/String;

    invoke-static {p0, p1, p2, v0, p3}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 1166954
    return-void
.end method

.method public static a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74R;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1166922
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1166923
    invoke-virtual {p1}, LX/74R;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 1166924
    invoke-direct {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1166925
    const-string v0, "composer"

    .line 1166926
    iput-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1166927
    if-nez p2, :cond_0

    .line 1166928
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object p2

    .line 1166929
    :cond_0
    iget-object v0, p0, LX/73w;->f:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->p()Ljava/lang/String;

    move-result-object v0

    .line 1166930
    const-string v1, "reachability_status"

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166931
    iget-object v0, p0, LX/73w;->k:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1166932
    const-string v0, "batch_type"

    iget-object v1, p0, LX/73w;->k:Ljava/lang/String;

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166933
    :cond_1
    iget-object v0, p0, LX/73w;->q:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1166934
    const-string v0, "custom_tags"

    iget-object v1, p0, LX/73w;->q:Ljava/lang/String;

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166935
    :cond_2
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1166936
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 1166937
    :cond_3
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1166938
    iput-object p3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1166939
    :cond_4
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1166940
    invoke-virtual {v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->i(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1166941
    :cond_5
    sget-boolean v0, LX/73w;->c:Z

    if-eqz v0, :cond_7

    .line 1166942
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1166943
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1166944
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v3

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1166945
    const-string v7, "%s%s:%s"

    const/4 p4, 0x3

    new-array p4, p4, [Ljava/lang/Object;

    if-eqz v1, :cond_6

    const-string v1, ""

    :goto_2
    aput-object v1, p4, v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    aput-object v1, p4, v3

    const/4 v1, 0x2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, p4, v1

    invoke-static {v7, p4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v4

    .line 1166946
    goto :goto_1

    .line 1166947
    :cond_6
    const-string v1, ", "

    goto :goto_2

    .line 1166948
    :cond_7
    const-string v0, "funnel_event_enabled"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1166949
    iget-object v0, p0, LX/73w;->d:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1166950
    invoke-virtual {p1}, LX/74R;->isUploadEvent()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1166951
    iget-object v0, p0, LX/73w;->a:LX/74L;

    invoke-virtual {v0, p1, p3, p2}, LX/74L;->a(LX/74R;Ljava/lang/String;Ljava/util/Map;)V

    .line 1166952
    :cond_8
    return-void
.end method

.method private static a(LX/73w;LX/74b;ILX/741;Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 1166920
    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-virtual/range {v1 .. v12}, LX/73w;->a(LX/74b;ILX/741;Ljava/lang/String;JJJLjava/lang/String;)V

    .line 1166921
    return-void
.end method

.method private static a(LX/73w;Ljava/lang/String;Ljava/lang/String;IIIIIIIIJJJJLjava/util/HashMap;)V
    .locals 15
    .param p0    # LX/73w;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IIIIIIIIJJJJ",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1166902
    move-object/from16 v0, p19

    invoke-static {p0, v0}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1166903
    invoke-static/range {p1 .. p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1166904
    const-string v2, "session_photo_id"

    move-object/from16 v0, p19

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166905
    :cond_0
    const-string v2, "source_width"

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p19

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166906
    const-string v2, "source_height"

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p19

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166907
    const-string v2, "source_bit_rate"

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p19

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166908
    const-string v2, "source_frame_rate"

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p19

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166909
    const-string v2, "target_width"

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p19

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166910
    const-string v2, "target_height"

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p19

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166911
    const-string v2, "original_file_size"

    invoke-static/range {p13 .. p14}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p19

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166912
    const-string v2, "bytes"

    invoke-static/range {p15 .. p16}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p19

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166913
    const-string v2, "quality"

    invoke-static/range {p17 .. p18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p19

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166914
    const-string v2, "target_bit_rate"

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p19

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166915
    const-string v2, "target_frame_rate"

    invoke-static/range {p10 .. p10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p19

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166916
    iget-wide v2, p0, LX/73w;->t:J

    move-object/from16 v0, p19

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166917
    sget-object v2, LX/74R;->MEDIA_UPLOAD_PROCESS_SUCCESS:LX/74R;

    const/4 v3, 0x0

    move-object/from16 v0, p19

    invoke-static {p0, v2, v0, v3}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166918
    iget-object v2, p0, LX/73w;->F:LX/74E;

    move-object/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    move-wide/from16 v12, p11

    invoke-virtual/range {v2 .. v13}, LX/74E;->a(Ljava/lang/String;IIIIIIIIJ)V

    .line 1166919
    return-void
.end method

.method private static a(LX/73w;Ljava/util/HashMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1166789
    const-string v0, "connection_manager_bandwidth_quality"

    iget-object v1, p0, LX/73w;->G:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->b()LX/0p3;

    move-result-object v1

    invoke-virtual {v1}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166790
    const-string v0, "connection_manager_guessed_bandwidth_quality"

    iget-object v1, p0, LX/73w;->G:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->c()LX/0p3;

    move-result-object v1

    invoke-virtual {v1}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166791
    const-string v0, "connection_manager_bandwidth_kbps"

    iget-object v1, p0, LX/73w;->G:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->f()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166792
    const-string v0, "connection_manager_latency_quality"

    iget-object v1, p0, LX/73w;->G:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->e()LX/0p3;

    move-result-object v1

    invoke-virtual {v1}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166793
    const-string v0, "connection_manager_rtt_ms"

    iget-object v1, p0, LX/73w;->G:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->k()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166794
    return-void
.end method

.method public static a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger$UploadInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1166868
    if-eqz p2, :cond_1

    .line 1166869
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1166870
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1166871
    const-string v1, "target_type"

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166872
    :cond_0
    const-string v0, "media_attachment_count"

    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166873
    const-string v0, "auto_retry_count"

    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166874
    const-string v0, "interrupted_count"

    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->f()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166875
    const-string v0, "current_auto_retry_count"

    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->g()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166876
    const-string v0, "extra_hasfailed_retrymightwork"

    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->h()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166877
    const-string v0, "extra_op_may_auto_retry"

    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->k()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166878
    const-string v0, "extra_hasfailed_network_error"

    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->i()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166879
    const-string v0, "extra_hasfailed_nonnetwork_error"

    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->j()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166880
    const-string v0, "extra_nonnetwork_error_count"

    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->l()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166881
    const-string v0, "extra_last_user_attempt_time"

    .line 1166882
    iget-object v4, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->E:LX/8LU;

    .line 1166883
    iget-wide v6, v4, LX/8LU;->b:J

    move-wide v4, v6

    .line 1166884
    move-wide v2, v4

    .line 1166885
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166886
    const-string v0, "extra_last_auto_retry_attempt_time"

    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->n()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166887
    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->a()LX/0ck;

    move-result-object v0

    sget-object v1, LX/0ck;->VIDEO:LX/0ck;

    if-ne v0, v1, :cond_2

    .line 1166888
    const-string v0, "attempt_number"

    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166889
    :goto_0
    iget-object v0, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->N:Ljava/lang/String;

    move-object v0, v0

    .line 1166890
    if-eqz v0, :cond_1

    .line 1166891
    const-string v0, "parent_op"

    .line 1166892
    iget-object v1, p2, Lcom/facebook/photos/upload/operation/UploadOperation;->N:Ljava/lang/String;

    move-object v1, v1

    .line 1166893
    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166894
    :cond_1
    invoke-static {p0, p1}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1166895
    return-void

    .line 1166896
    :cond_2
    const-string v0, "manual_retry_count"

    invoke-virtual {p2}, Lcom/facebook/photos/upload/operation/UploadOperation;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a(LX/73w;ZLandroid/net/Uri;)V
    .locals 3

    .prologue
    .line 1166861
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 1166862
    if-eqz p2, :cond_0

    .line 1166863
    const-string v0, "uri"

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166864
    :cond_0
    const-string v2, "has_video"

    if-eqz p1, :cond_1

    const-string v0, "true"

    :goto_0
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166865
    sget-object v0, LX/74R;->PICKED_MEDIA:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166866
    return-void

    .line 1166867
    :cond_1
    const-string v0, "false"

    goto :goto_0
.end method

.method public static a(Ljava/util/HashMap;J)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 1166859
    const-string v0, "dt"

    invoke-static {p1, p2}, LX/73w;->c(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166860
    return-void
.end method

.method public static a(Ljava/util/HashMap;LX/73y;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/73y;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1166856
    if-eqz p1, :cond_0

    .line 1166857
    invoke-interface {p1}, LX/73y;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/73y;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/73y;->e()I

    move-result v3

    invoke-interface {p1}, LX/73y;->f()I

    move-result v4

    invoke-interface {p1}, LX/73y;->g()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1}, LX/73y;->b()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    invoke-static/range {v0 .. v6}, LX/73w;->a(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 1166858
    :cond_0
    return-void
.end method

.method public static a(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, -0x1

    .line 1166843
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1166844
    const-string v0, "ex_type"

    invoke-virtual {p0, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166845
    :cond_0
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1166846
    const-string v0, "ex_msg"

    invoke-virtual {p0, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166847
    :cond_1
    if-eq p3, v2, :cond_2

    .line 1166848
    const-string v0, "ex_code"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166849
    :cond_2
    if-eq p4, v2, :cond_3

    .line 1166850
    const-string v0, "http_status_code"

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166851
    :cond_3
    invoke-static {p5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1166852
    const-string v0, "error_type"

    invoke-virtual {p0, v0, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166853
    :cond_4
    invoke-static {p6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1166854
    const-string v0, "ex_inner_msg"

    invoke-virtual {p0, v0, p6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166855
    :cond_5
    return-void
.end method

.method private static a(ZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZZZZZJJJJJJJJJJ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1166819
    const-string v2, "trans_is_audio_track"

    invoke-static {p0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166820
    const-string v2, "trans_is_init_complete"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166821
    const-string v2, "trans_audio_track_is_set"

    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166822
    const-string v2, "trans_video_track_is_set"

    invoke-static {p3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166823
    const-string v2, "trans_got_first_audio"

    invoke-static {p4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166824
    const-string v2, "trans_got_first_video"

    invoke-static {p5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166825
    const-string v2, "trans_audio_video_track_reset"

    invoke-static {p6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166826
    const-string v2, "trans_start_time_us"

    invoke-static {p7, p8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166827
    const-string v2, "trans_end_time_us"

    invoke-static {p9, p10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166828
    const-string v2, "trans_adjusted_end_time_us"

    invoke-static/range {p11 .. p12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166829
    const-string v2, "trans_sync_start_time_us"

    invoke-static/range {p13 .. p14}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166830
    const-string v2, "trans_first_video_time_us"

    invoke-static/range {p15 .. p16}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166831
    const-string v2, "trans_last_video_time_us"

    invoke-static/range {p17 .. p18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166832
    const-string v2, "trans_first_audio_time_us"

    invoke-static/range {p19 .. p20}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166833
    const-string v2, "trans_last_audio_time_us"

    invoke-static/range {p21 .. p22}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166834
    const-string v2, "trans_num_video_samples_muxed"

    invoke-static/range {p23 .. p24}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166835
    const-string v2, "trans_num_audio_samples_muxed"

    invoke-static/range {p25 .. p26}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p30

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166836
    invoke-static/range {p27 .. p27}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1166837
    const-string v2, "trans_num_exception"

    move-object/from16 v0, p30

    move-object/from16 v1, p27

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166838
    :cond_0
    invoke-static/range {p28 .. p28}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1166839
    const-string v2, "trans_num_exception_cause"

    move-object/from16 v0, p30

    move-object/from16 v1, p28

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166840
    :cond_1
    invoke-static/range {p29 .. p29}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1166841
    const-string v2, "trans_num_call_stack"

    move-object/from16 v0, p30

    move-object/from16 v1, p29

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166842
    :cond_2
    return-void
.end method

.method private static b(JJJ)F
    .locals 2

    .prologue
    .line 1166816
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gtz v0, :cond_0

    .line 1166817
    const/4 v0, 0x0

    .line 1166818
    :goto_0
    return v0

    :cond_0
    add-long v0, p2, p4

    long-to-float v0, v0

    long-to-float v1, p0

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/73w;
    .locals 11

    .prologue
    .line 1166811
    new-instance v0, LX/73w;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v3

    check-cast v3, LX/0kb;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    const/16 v5, 0x245

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1601

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/74L;->a(LX/0QB;)LX/74L;

    move-result-object v7

    check-cast v7, LX/74L;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1166812
    new-instance v10, LX/74E;

    invoke-static {p0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v9

    check-cast v9, LX/11i;

    invoke-direct {v10, v9}, LX/74E;-><init>(LX/11i;)V

    .line 1166813
    move-object v9, v10

    .line 1166814
    check-cast v9, LX/74E;

    invoke-static {p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v10

    check-cast v10, LX/0oz;

    invoke-direct/range {v0 .. v10}, LX/73w;-><init>(LX/0Zb;LX/03V;LX/0kb;LX/0So;LX/0Ot;LX/0Or;LX/74L;Ljava/lang/String;LX/74E;LX/0oz;)V

    .line 1166815
    return-object v0
.end method

.method public static b(LX/73w;Ljava/lang/String;LX/0ck;LX/742;)Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0ck;",
            "LX/742;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1166810
    invoke-direct {p0, p1, p2, p3}, LX/73w;->a(Ljava/lang/String;LX/0ck;LX/742;)LX/74b;

    move-result-object v0

    invoke-virtual {v0}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/73w;Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1166807
    const-string v1, "app_state"

    iget-object v0, p0, LX/73w;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "background"

    :goto_0
    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166808
    return-void

    .line 1166809
    :cond_0
    const-string v0, "foreground"

    goto :goto_0
.end method

.method public static b(LX/73w;Ljava/util/HashMap;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 1166805
    iget-object v0, p0, LX/73w;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    sub-long/2addr v0, p2

    invoke-static {p1, v0, v1}, LX/73w;->a(Ljava/util/HashMap;J)V

    .line 1166806
    return-void
.end method

.method private static b(LX/73w;Ljava/util/HashMap;LX/73y;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/73y;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1166795
    invoke-static {p0, p1}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1166796
    invoke-static {p1, p2}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 1166797
    iget-wide v0, p0, LX/73w;->t:J

    invoke-static {p0, p1, v0, v1}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166798
    sget-object v0, LX/74R;->MEDIA_UPLOAD_PROCESS_FAILURE:LX/74R;

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166799
    iget-object v0, p0, LX/73w;->F:LX/74E;

    invoke-interface {p2}, LX/73y;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, LX/73y;->d()Ljava/lang/String;

    move-result-object v2

    .line 1166800
    const/4 p0, 0x0

    invoke-static {v0, p0}, LX/74E;->a(LX/74E;Z)Ljava/util/Map;

    move-result-object p0

    .line 1166801
    const-string p1, "failure_reason"

    invoke-interface {p0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166802
    const-string p1, "failure_message"

    invoke-interface {p0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166803
    const-string p1, "ProcessVideoMarker"

    invoke-static {p0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object p0

    invoke-static {v0, p1, p0}, LX/74E;->c(LX/74E;Ljava/lang/String;LX/0P1;)V

    .line 1166804
    return-void
.end method

.method private static c(J)J
    .locals 2

    .prologue
    .line 1167114
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    const-wide/32 v0, 0x50e22700

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    .line 1167115
    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return-wide p0

    .line 1167116
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1167117
    :cond_1
    const-wide/16 p0, -0x1

    goto :goto_1
.end method

.method private static c(LX/73w;Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1166984
    invoke-static {p0, p1}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1166985
    sget-object v0, LX/74R;->MEDIA_UPLOAD_PROCESS_START:LX/74R;

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166986
    iget-object v0, p0, LX/73w;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/73w;->t:J

    .line 1166987
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1166988
    const-string v1, "ProcessVideoMarker"

    invoke-static {v0, v1}, LX/74E;->a(LX/74E;Ljava/lang/String;)V

    .line 1166989
    return-void
.end method

.method private static d(LX/73w;J)J
    .locals 3

    .prologue
    .line 1167118
    iget-object v0, p0, LX/73w;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    sub-long/2addr v0, p1

    invoke-static {v0, v1}, LX/73w;->c(J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;IIII)LX/74Z;
    .locals 2

    .prologue
    .line 1167119
    new-instance v0, LX/74Y;

    invoke-direct {v0}, LX/74Y;-><init>()V

    .line 1167120
    iput-object p1, v0, LX/74Y;->a:Ljava/lang/String;

    .line 1167121
    move-object v0, v0

    .line 1167122
    iput p2, v0, LX/74Y;->b:I

    .line 1167123
    move-object v0, v0

    .line 1167124
    iput p3, v0, LX/74Y;->c:I

    .line 1167125
    move-object v0, v0

    .line 1167126
    const/4 v1, 0x0

    .line 1167127
    iput v1, v0, LX/74Y;->d:I

    .line 1167128
    move-object v0, v0

    .line 1167129
    iput p4, v0, LX/74Y;->e:I

    .line 1167130
    move-object v0, v0

    .line 1167131
    iput p5, v0, LX/74Y;->f:I

    .line 1167132
    move-object v0, v0

    .line 1167133
    new-instance v1, LX/74Z;

    invoke-direct {v1, v0}, LX/74Z;-><init>(LX/74Y;)V

    move-object v0, v1

    .line 1167134
    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/742;)LX/74b;
    .locals 1

    .prologue
    .line 1167135
    sget-object v0, LX/0ck;->VIDEO:LX/0ck;

    invoke-direct {p0, p1, v0, p2}, LX/73w;->a(Ljava/lang/String;LX/0ck;LX/742;)LX/74b;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1167136
    iget-object v0, p0, LX/73w;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final a(IZ)V
    .locals 3

    .prologue
    .line 1167137
    iput p1, p0, LX/73w;->n:I

    .line 1167138
    if-eqz p2, :cond_0

    .line 1167139
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 1167140
    const-string v1, "camera_index"

    iget v2, p0, LX/73w;->n:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167141
    sget-object v1, LX/74R;->CAMERA_SOURCE_SELECT:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167142
    :cond_0
    return-void
.end method

.method public final a(LX/74W;)V
    .locals 3

    .prologue
    .line 1167143
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 1167144
    const-string v1, "relationship_type"

    iget-object v2, p1, LX/74W;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167145
    const-string v1, "profile_id"

    iget-object v2, p1, LX/74W;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167146
    const-string v1, "tab_name"

    iget-object v2, p1, LX/74W;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167147
    const-string v1, "prev_tab_name"

    iget-object v2, p1, LX/74W;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167148
    move-object v1, v0

    .line 1167149
    sget-object v2, LX/74R;->PHOTOS_TAB_NAV:LX/74R;

    .line 1167150
    iget-object v0, p1, LX/74W;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1167151
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v2, v1, v0}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167152
    return-void
.end method

.method public final a(LX/74b;)V
    .locals 3

    .prologue
    .line 1167153
    sget-object v0, LX/74R;->MEDIA_UPLOAD_HASH_COMPUTE_START:LX/74R;

    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167154
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1167155
    const-string v1, "HashComputeMarker"

    invoke-static {v0, v1}, LX/74E;->a(LX/74E;Ljava/lang/String;)V

    .line 1167156
    return-void
.end method

.method public final a(LX/74b;IJ)V
    .locals 5

    .prologue
    .line 1167157
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1167158
    invoke-static {p0, v0}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1167159
    const-string v1, "num_total_segments"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167160
    const-string v1, "video_duration"

    long-to-float v2, p3

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167161
    sget-object v1, LX/74R;->MEDIA_UPLOAD_SEGMENTED_TRANSCODE_START:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167162
    return-void
.end method

.method public final a(LX/74b;IJIIII)V
    .locals 5

    .prologue
    .line 1167102
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1167103
    invoke-static {p0, v0}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1167104
    const-string v1, "num_total_segments"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167105
    const-string v1, "video_duration"

    long-to-float v2, p3

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167106
    const-string v1, "segment_id"

    invoke-static {p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167107
    const-string v1, "segment_type"

    invoke-static {p6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167108
    const-string v1, "segment_start_time"

    invoke-static {p7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167109
    const-string v1, "segment_end_time"

    invoke-static {p8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167110
    sget-object v1, LX/74R;->MEDIA_UPLOAD_SEGMENTED_TRANSCODE_FAILURE:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167111
    return-void
.end method

.method public final a(LX/74b;ILX/741;)V
    .locals 1

    .prologue
    .line 1167112
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, LX/73w;->a(LX/73w;LX/74b;ILX/741;Ljava/lang/String;)V

    .line 1167113
    return-void
.end method

.method public final a(LX/74b;ILX/741;Ljava/lang/String;JJJLjava/lang/String;)V
    .locals 7
    .param p11    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1167086
    invoke-virtual {p1, p3}, LX/74b;->a(LX/741;)V

    .line 1167087
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v2

    .line 1167088
    const-string v3, "photo_index"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167089
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1167090
    const-string v3, "extension"

    invoke-virtual {v2, v3, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167091
    :cond_0
    const-wide/16 v4, 0x0

    cmp-long v3, p5, v4

    if-lez v3, :cond_1

    .line 1167092
    const-string v3, "original_file_size"

    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167093
    :cond_1
    const-wide/16 v4, 0x0

    cmp-long v3, p7, v4

    if-lez v3, :cond_2

    .line 1167094
    const-string v3, "bytes"

    invoke-static {p7, p8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167095
    :cond_2
    const-wide/16 v4, 0x0

    cmp-long v3, p9, v4

    if-lez v3, :cond_3

    .line 1167096
    const-string v3, "video_duration"

    move-wide/from16 v0, p9

    long-to-float v4, v0

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167097
    :cond_3
    if-eqz p11, :cond_4

    .line 1167098
    const-string v3, "video_upload_quality"

    move-object/from16 v0, p11

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167099
    :cond_4
    sget-object v3, LX/74R;->MEDIA_UPLOAD_START:LX/74R;

    const/4 v4, 0x0

    invoke-static {p0, v3, v2, v4}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167100
    iget-object v2, p0, LX/73w;->g:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/73w;->u:J

    .line 1167101
    return-void
.end method

.method public final a(LX/74b;ILjava/lang/String;ZZJJ)V
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1167077
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1167078
    const-string v1, "original_file_size"

    invoke-static {p6, p7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167079
    const-string v1, "bytes"

    invoke-static {p8, p9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167080
    const-string v1, "success"

    invoke-static {p5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167081
    const-string v1, "dimensions"

    int-to-long v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167082
    const-string v1, "filter"

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167083
    const-string v1, "has_overlay"

    invoke-static {p4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167084
    sget-object v1, LX/74R;->MEDIA_UPLOAD_PROCESS_ENHANCEMENT:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167085
    return-void
.end method

.method public final a(LX/74b;JIZZIII)V
    .locals 4

    .prologue
    .line 1167067
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1167068
    const-string v1, "original_file_size"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167069
    const-string v1, "estimated_resized_file_size"

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167070
    const-string v1, "attempt_video_resize"

    invoke-static {p5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167071
    const-string v1, "higher_quality_transcode"

    invoke-static {p6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167072
    const-string v1, "specified_transcode_bit_rate"

    invoke-static {p7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167073
    const-string v1, "estimated_video_bit_rate"

    invoke-static {p8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167074
    const-string v1, "estimated_audio_bit_rate"

    invoke-static {p9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167075
    sget-object v1, LX/74R;->MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_SUCCESS:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167076
    return-void
.end method

.method public final a(LX/74b;JJI)V
    .locals 4

    .prologue
    .line 1167057
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1167058
    invoke-static {p0, v0}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1167059
    const-string v1, "sent_bytes"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167060
    const-string v1, "total_bytes"

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167061
    const-string v1, "auto_retry_count"

    invoke-static {p6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167062
    iget-wide v2, p0, LX/73w;->v:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1167063
    sget-object v1, LX/74R;->MEDIA_UPLOAD_TRANSFER_CANCEL:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167064
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1167065
    const-string v1, "TransferMarker"

    invoke-static {v0, v1}, LX/74E;->d(LX/74E;Ljava/lang/String;)V

    .line 1167066
    return-void
.end method

.method public final a(LX/74b;JJILX/73y;)V
    .locals 4

    .prologue
    .line 1167046
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1167047
    invoke-static {p0, v0}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1167048
    const-string v1, "sent_bytes"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167049
    const-string v1, "total_bytes"

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167050
    const-string v1, "auto_retry_count"

    invoke-static {p6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167051
    invoke-static {v0, p7}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 1167052
    iget-wide v2, p0, LX/73w;->v:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1167053
    sget-object v1, LX/74R;->MEDIA_UPLOAD_TRANSFER_FAILURE:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167054
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1167055
    const-string v1, "TransferMarker"

    invoke-static {v0, v1}, LX/74E;->c(LX/74E;Ljava/lang/String;)V

    .line 1167056
    return-void
.end method

.method public final a(LX/74b;JJJJZZZJJZFIZ)V
    .locals 4

    .prologue
    .line 1167029
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1167030
    const-string v1, "video_transcode_flow_count"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167031
    const-string v1, "video_transcode_start_count"

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167032
    const-string v1, "video_transcode_success_count"

    invoke-static {p6, p7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167033
    const-string v1, "video_transcode_fail_count"

    invoke-static {p8, p9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167034
    const-string v1, "video_transcode_is_segmented"

    invoke-static {p10}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167035
    const-string v1, "video_requested_server_settings"

    invoke-static {p11}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167036
    const-string v1, "video_server_settings_available"

    invoke-static/range {p12 .. p12}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167037
    const-string v1, "server_specified_transcode_bitrate"

    invoke-static/range {p13 .. p14}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167038
    const-string v1, "server_specified_transcode_dimension"

    invoke-static/range {p15 .. p16}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167039
    const-string v1, "video_using_contextual_config"

    invoke-static/range {p17 .. p17}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167040
    if-gtz p19, :cond_0

    const/4 v1, 0x0

    cmpl-float v1, p18, v1

    if-lez v1, :cond_1

    .line 1167041
    :cond_0
    const-string v1, "video_skip_percentage_threshold"

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float v2, v2, p18

    float-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167042
    const-string v1, "video_skip_bytes_threshold"

    invoke-static/range {p19 .. p19}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167043
    :cond_1
    const-string v1, "video_resize_codec_init_error"

    invoke-static/range {p20 .. p20}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167044
    sget-object v1, LX/74R;->MEDIA_UPLOAD_PROCESS_READ_PERSISTED_TRANSCODE_INFO:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167045
    return-void
.end method

.method public final a(LX/74b;JJZ)V
    .locals 4

    .prologue
    .line 1167023
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1167024
    const-string v1, "hash_file_size"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167025
    const-string v1, "bytes"

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167026
    const-string v1, "transcode_success"

    invoke-static {p6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167027
    sget-object v1, LX/74R;->MEDIA_UPLOAD_HASH_COMPUTE_SKIPPED:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167028
    return-void
.end method

.method public final a(LX/74b;JLcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 4

    .prologue
    .line 1167015
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1167016
    invoke-static {p0, v0, p4}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1167017
    const-string v1, "upload_session_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167018
    iget-wide v2, p0, LX/73w;->x:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1167019
    sget-object v1, LX/74R;->MEDIA_UPLOAD_INIT_SUCCESS:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167020
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1167021
    const-string v1, "InitMarker"

    invoke-static {v0, v1}, LX/74E;->b(LX/74E;Ljava/lang/String;)V

    .line 1167022
    return-void
.end method

.method public final a(LX/74b;JLjava/lang/String;)V
    .locals 4

    .prologue
    .line 1167008
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1167009
    const-string v1, "hash_file_size"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167010
    const-string v1, "extra_hash"

    invoke-virtual {v0, v1, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167011
    sget-object v1, LX/74R;->MEDIA_UPLOAD_HASH_COMPUTE_END:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167012
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1167013
    const-string v1, "HashComputeMarker"

    invoke-static {v0, v1}, LX/74E;->b(LX/74E;Ljava/lang/String;)V

    .line 1167014
    return-void
.end method

.method public final a(LX/74b;JZLX/73y;)V
    .locals 4

    .prologue
    .line 1167001
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1167002
    const-string v1, "original_file_size"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167003
    const-string v1, "attempt_video_resize"

    invoke-static {p4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1167004
    invoke-static {p0, v0}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1167005
    invoke-static {v0, p5}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 1167006
    sget-object v1, LX/74R;->MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_FAILURE:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1167007
    return-void
.end method

.method public final a(LX/74b;LX/73y;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 4

    .prologue
    .line 1166990
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166991
    invoke-static {v0, p2}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 1166992
    invoke-static {p0, v0, p3}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166993
    iget-wide v2, p0, LX/73w;->x:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166994
    sget-object v1, LX/74R;->MEDIA_UPLOAD_INIT_FAILURE:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166995
    iget-object v0, p0, LX/73w;->F:LX/74E;

    invoke-interface {p2}, LX/73y;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, LX/73y;->d()Ljava/lang/String;

    move-result-object v2

    .line 1166996
    const/4 v3, 0x0

    invoke-static {v0, v3}, LX/74E;->a(LX/74E;Z)Ljava/util/Map;

    move-result-object v3

    .line 1166997
    const-string p0, "failure_reason"

    invoke-interface {v3, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166998
    const-string p0, "failure_message"

    invoke-interface {v3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166999
    const-string p0, "InitMarker"

    invoke-static {v3}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v3

    invoke-static {v0, p0, v3}, LX/74E;->c(LX/74E;Ljava/lang/String;LX/0P1;)V

    .line 1167000
    return-void
.end method

.method public final a(LX/74b;LX/73y;ZZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 37

    .prologue
    .line 1166897
    invoke-virtual/range {p1 .. p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v35

    .line 1166898
    const-string v4, "video_resize_codec_init_error"

    invoke-static/range {p3 .. p3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    move-wide/from16 v12, p11

    move-wide/from16 v14, p13

    move-wide/from16 v16, p15

    move-wide/from16 v18, p17

    move-wide/from16 v20, p19

    move-wide/from16 v22, p21

    move-wide/from16 v24, p23

    move-wide/from16 v26, p25

    move-wide/from16 v28, p27

    move-wide/from16 v30, p29

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    .line 1166899
    invoke-static/range {v5 .. v35}, LX/73w;->a(ZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 1166900
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, LX/73w;->b(LX/73w;Ljava/util/HashMap;LX/73y;)V

    .line 1166901
    return-void
.end method

.method public final a(LX/74b;LX/741;)V
    .locals 4

    .prologue
    .line 1166977
    iput-object p2, p1, LX/74b;->c:LX/741;

    .line 1166978
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166979
    iget-wide v2, p0, LX/73w;->u:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166980
    sget-object v1, LX/74R;->MEDIA_UPLOAD_CANCEL:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166981
    sget-object v0, LX/741;->UNKNOWN:LX/741;

    .line 1166982
    iput-object v0, p1, LX/74b;->c:LX/741;

    .line 1166983
    return-void
.end method

.method public final a(LX/74b;LX/741;LX/73y;)V
    .locals 4

    .prologue
    .line 1166526
    iput-object p2, p1, LX/74b;->c:LX/741;

    .line 1166527
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166528
    invoke-static {v0, p3}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 1166529
    iget-wide v2, p0, LX/73w;->u:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166530
    sget-object v1, LX/74R;->MEDIA_UPLOAD_FAILURE:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166531
    sget-object v0, LX/741;->UNKNOWN:LX/741;

    .line 1166532
    iput-object v0, p1, LX/74b;->c:LX/741;

    .line 1166533
    return-void
.end method

.method public final a(LX/74b;LX/741;Ljava/lang/String;Ljava/lang/String;JI)V
    .locals 5
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1166625
    iput-object p2, p1, LX/74b;->c:LX/741;

    .line 1166626
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166627
    const-string v1, "fbid"

    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166628
    const-string v1, "auto_retry_count"

    invoke-static {p7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166629
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1166630
    const-string v1, "session_photo_id"

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166631
    :cond_0
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1166632
    const-string v1, "projection_type"

    invoke-virtual {v0, v1, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166633
    :cond_1
    iget-wide v2, p0, LX/73w;->u:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166634
    sget-object v1, LX/74R;->MEDIA_UPLOAD_SUCCESS:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166635
    sget-object v0, LX/741;->UNKNOWN:LX/741;

    .line 1166636
    iput-object v0, p1, LX/74b;->c:LX/741;

    .line 1166637
    return-void
.end method

.method public final a(LX/74b;LX/74Z;)V
    .locals 3

    .prologue
    .line 1166620
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166621
    invoke-virtual {p2, v0}, LX/74Z;->a(Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 1166622
    sget-object v1, LX/74R;->MEDIA_PUBLISH_START:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166623
    iget-object v0, p0, LX/73w;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/73w;->w:J

    .line 1166624
    return-void
.end method

.method public final a(LX/74b;LX/74Z;LX/73y;)V
    .locals 4

    .prologue
    .line 1166614
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166615
    invoke-virtual {p2, v0}, LX/74Z;->a(Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 1166616
    invoke-static {v0, p3}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 1166617
    iget-wide v2, p0, LX/73w;->w:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166618
    sget-object v1, LX/74R;->MEDIA_PUBLISH_FAILURE:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166619
    return-void
.end method

.method public final a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 4

    .prologue
    .line 1166609
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166610
    invoke-static {p0, v0, p2}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166611
    iget-wide v2, p0, LX/73w;->s:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166612
    sget-object v1, LX/74R;->MEDIA_UPLOAD_BATCH_SUCCESS:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166613
    return-void
.end method

.method public final a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;I)V
    .locals 4

    .prologue
    .line 1166603
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166604
    invoke-static {p0, v0, p2}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166605
    const-string v1, "multi_success"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166606
    iget-wide v2, p0, LX/73w;->s:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166607
    sget-object v1, LX/74R;->MEDIA_UPLOAD_BATCH_CANCEL:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166608
    return-void
.end method

.method public final a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;IILX/73y;)V
    .locals 4

    .prologue
    .line 1166595
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166596
    invoke-static {p0, v0, p2}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166597
    const-string v1, "multi_success"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166598
    const-string v1, "multi_fail"

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166599
    invoke-static {v0, p5}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 1166600
    iget-wide v2, p0, LX/73w;->s:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166601
    sget-object v1, LX/74R;->MEDIA_UPLOAD_BATCH_FAILURE:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166602
    return-void
.end method

.method public final a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;IIZLX/73y;)V
    .locals 4

    .prologue
    .line 1166586
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166587
    invoke-static {p0, v0, p2}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166588
    const-string v1, "multi_success"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166589
    const-string v1, "multi_fail"

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166590
    invoke-static {v0, p6}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 1166591
    iget-wide v2, p0, LX/73w;->r:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166592
    const-string v1, "upload_fail"

    invoke-static {p5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166593
    sget-object v1, LX/74R;->MEDIA_UPLOAD_ATTEMPT_FAILURE:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166594
    return-void
.end method

.method public final a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;ILX/73y;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1166577
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166578
    invoke-static {p0, v0, p2}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166579
    const-string v1, "multi_success"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166580
    const-string v1, "action_source"

    invoke-virtual {v0, v1, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166581
    invoke-static {v0, p4}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 1166582
    iget-wide v2, p0, LX/73w;->r:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166583
    sget-object v1, LX/74R;->MEDIA_UPLOAD_FLOW_CANCEL:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166584
    iget-object v0, p0, LX/73w;->F:LX/74E;

    invoke-virtual {v0}, LX/74E;->c()V

    .line 1166585
    return-void
.end method

.method public final a(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74b;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger$UploadInfo;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1166561
    iget-object v0, p0, LX/73w;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/73w;->x:J

    .line 1166562
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166563
    invoke-static {p0, v0, p2}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166564
    invoke-interface {p3}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1166565
    const-string v1, "video_metadata"

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p3}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166566
    const-string v1, "source_width"

    const-string v2, "source_width"

    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166567
    const-string v1, "source_height"

    const-string v2, "source_height"

    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166568
    const-string v1, "source_bit_rate"

    const-string v2, "source_bit_rate"

    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166569
    const-string v1, "source_audio_bit_rate"

    const-string v2, "source_audio_bit_rate"

    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166570
    const-string v1, "source_rotation_angle"

    const-string v2, "source_rotation_angle"

    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166571
    :cond_0
    const-string v1, "video_original_file_path"

    invoke-virtual {v0, v1, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166572
    const-string v1, "mime_type"

    invoke-virtual {v0, v1, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166573
    sget-object v1, LX/74R;->MEDIA_UPLOAD_INIT_START:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166574
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1166575
    const-string v1, "InitMarker"

    invoke-static {v0, v1}, LX/74E;->a(LX/74E;Ljava/lang/String;)V

    .line 1166576
    return-void
.end method

.method public final a(LX/74b;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1166554
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166555
    invoke-static {p0, v0}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1166556
    const-string v1, "extra_hash"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166557
    sget-object v1, LX/74R;->MEDIA_UPLOAD_TRANSFER_SKIP:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166558
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1166559
    invoke-static {v0}, LX/74E;->x(LX/74E;)LX/11o;

    move-result-object v1

    const-string v2, "SkipUploadEvent"

    const p0, 0x31628b50

    invoke-static {v1, v2, p0}, LX/096;->e(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1166560
    return-void
.end method

.method public final a(LX/74b;Ljava/lang/String;IIIIIIIIJJJZZZZZZZJJJJJJJJJJ)V
    .locals 32

    .prologue
    .line 1166550
    invoke-virtual/range {p1 .. p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v31

    .line 1166551
    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    move/from16 v1, p17

    move/from16 v2, p18

    move/from16 v3, p19

    move/from16 v4, p20

    move/from16 v5, p21

    move/from16 v6, p22

    move/from16 v7, p23

    move-wide/from16 v8, p24

    move-wide/from16 v10, p26

    move-wide/from16 v12, p28

    move-wide/from16 v14, p30

    move-wide/from16 v16, p32

    move-wide/from16 v18, p34

    move-wide/from16 v20, p36

    move-wide/from16 v22, p38

    move-wide/from16 v24, p40

    move-wide/from16 v26, p42

    invoke-static/range {v1 .. v31}, LX/73w;->a(ZZZZZZZJJJJJJJJJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 1166552
    const/4 v13, 0x0

    const-wide/16 v29, 0x0

    move-object/from16 v12, p0

    move-object/from16 v14, p2

    move/from16 v15, p3

    move/from16 v16, p4

    move/from16 v17, p5

    move/from16 v18, p6

    move/from16 v19, p7

    move/from16 v20, p8

    move/from16 v21, p9

    move/from16 v22, p10

    move-wide/from16 v23, p11

    move-wide/from16 v25, p13

    move-wide/from16 v27, p15

    invoke-static/range {v12 .. v31}, LX/73w;->a(LX/73w;Ljava/lang/String;Ljava/lang/String;IIIIIIIIJJJJLjava/util/HashMap;)V

    .line 1166553
    return-void
.end method

.method public final a(LX/74b;Ljava/lang/String;IJJILjava/lang/String;)V
    .locals 10

    .prologue
    .line 1166372
    iget-object v2, p0, LX/73w;->g:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/73w;->y:J

    .line 1166373
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v2

    .line 1166374
    const-string v3, "chunk_offset"

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166375
    const-string v3, "chunk_size"

    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166376
    const-string v3, "auto_retry_count"

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166377
    const-string v3, "upload_session_id"

    invoke-virtual {v2, v3, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166378
    const-string v3, "video_chunk_id"

    move-object/from16 v0, p9

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166379
    const-string v3, "is_using_fbuploader"

    iget-boolean v4, p0, LX/73w;->B:Z

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166380
    invoke-static {p0, v2}, LX/73w;->a(LX/73w;Ljava/util/HashMap;)V

    .line 1166381
    sget-object v3, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_START:LX/74R;

    const/4 v4, 0x0

    invoke-static {p0, v3, v2, v4}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166382
    iget-object v2, p0, LX/73w;->F:LX/74E;

    move v3, p3

    move-wide v4, p4

    move-wide/from16 v6, p6

    move/from16 v8, p8

    invoke-virtual/range {v2 .. v8}, LX/74E;->a(IJJI)V

    .line 1166383
    return-void
.end method

.method public final a(LX/74b;Ljava/lang/String;JJIJLjava/lang/String;)V
    .locals 11

    .prologue
    .line 1166384
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v8

    .line 1166385
    const-string v2, "bytes"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166386
    const-string v2, "chunk_offset"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166387
    const-string v2, "auto_retry_count"

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166388
    const-string v9, "transfer_progress"

    move-wide/from16 v2, p8

    move-wide v4, p3

    move-wide/from16 v6, p5

    invoke-static/range {v2 .. v7}, LX/73w;->b(JJJ)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v9, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166389
    const-string v2, "chunk_bandwidth"

    iget-wide v4, p0, LX/73w;->y:J

    invoke-static {p0, v4, v5}, LX/73w;->d(LX/73w;J)J

    move-result-wide v4

    move-wide/from16 v0, p5

    invoke-static {v0, v1, v4, v5}, LX/73w;->a(JJ)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166390
    const-string v9, "estimated_total_time"

    iget-wide v2, p0, LX/73w;->y:J

    invoke-static {p0, v2, v3}, LX/73w;->d(LX/73w;J)J

    move-result-wide v6

    move-wide/from16 v2, p8

    move-wide/from16 v4, p5

    invoke-static/range {v2 .. v7}, LX/73w;->a(JJJ)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v9, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166391
    const-string v2, "upload_session_id"

    invoke-virtual {v8, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166392
    const-string v2, "video_chunk_id"

    move-object/from16 v0, p10

    invoke-virtual {v8, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166393
    const-string v2, "is_using_fbuploader"

    iget-boolean v3, p0, LX/73w;->B:Z

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166394
    iget-wide v2, p0, LX/73w;->y:J

    invoke-static {p0, v8, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166395
    iget-object v2, p0, LX/73w;->g:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/73w;->z:J

    .line 1166396
    invoke-static {p0, v8}, LX/73w;->a(LX/73w;Ljava/util/HashMap;)V

    .line 1166397
    sget-object v2, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_SUCCESS:LX/74R;

    const/4 v3, 0x0

    invoke-static {p0, v2, v8, v3}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166398
    iget-object v2, p0, LX/73w;->F:LX/74E;

    invoke-virtual {v2}, LX/74E;->t()V

    .line 1166399
    return-void
.end method

.method public final a(LX/74b;Ljava/lang/String;JJILX/8O2;)V
    .locals 5

    .prologue
    .line 1166400
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166401
    const-string v1, "sent_bytes"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166402
    const-string v1, "total_bytes"

    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166403
    const-string v1, "auto_retry_count"

    invoke-static {p7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166404
    const-string v1, "upload_session_id"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166405
    if-eqz p8, :cond_0

    .line 1166406
    const-string v1, "video_chunk_id"

    .line 1166407
    iget-object v2, p8, LX/8O2;->h:Ljava/lang/String;

    move-object v2, v2

    .line 1166408
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166409
    :cond_0
    const-string v1, "is_using_fbuploader"

    iget-boolean v2, p0, LX/73w;->B:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166410
    iget-wide v2, p0, LX/73w;->y:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166411
    invoke-static {p0, v0}, LX/73w;->a(LX/73w;Ljava/util/HashMap;)V

    .line 1166412
    sget-object v1, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_CANCEL:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166413
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1166414
    const-string v1, "ChunkMarker"

    invoke-static {v0, v1}, LX/74E;->d(LX/74E;Ljava/lang/String;)V

    .line 1166415
    return-void
.end method

.method public final a(LX/74b;Ljava/lang/String;JJILX/8O2;LX/73y;)V
    .locals 5

    .prologue
    .line 1166416
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166417
    const-string v1, "sent_bytes"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166418
    const-string v1, "total_bytes"

    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166419
    const-string v1, "auto_retry_count"

    invoke-static {p7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166420
    const-string v1, "upload_session_id"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166421
    if-eqz p8, :cond_0

    .line 1166422
    const-string v1, "chunk_offset"

    invoke-virtual {p8}, LX/8O2;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166423
    const-string v1, "chunk_size"

    invoke-virtual {p8}, LX/8O2;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166424
    const-string v1, "immediate_retry_count"

    invoke-virtual {p8}, LX/8O2;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166425
    const-string v1, "upload_speed_previous"

    .line 1166426
    iget v2, p8, LX/8O2;->d:F

    move v2, v2

    .line 1166427
    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166428
    const-string v1, "upload_speed_updated"

    .line 1166429
    iget v2, p8, LX/8O2;->e:F

    move v2, v2

    .line 1166430
    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166431
    const-string v1, "video_chunk_id"

    .line 1166432
    iget-object v2, p8, LX/8O2;->h:Ljava/lang/String;

    move-object v2, v2

    .line 1166433
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166434
    :cond_0
    const-string v1, "is_using_fbuploader"

    iget-boolean v2, p0, LX/73w;->B:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166435
    invoke-static {v0, p9}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 1166436
    iget-wide v2, p0, LX/73w;->y:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166437
    invoke-static {p0, v0}, LX/73w;->a(LX/73w;Ljava/util/HashMap;)V

    .line 1166438
    sget-object v1, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_FAILURE:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166439
    iget-object v0, p0, LX/73w;->F:LX/74E;

    invoke-interface {p9}, LX/73y;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p9}, LX/73y;->d()Ljava/lang/String;

    move-result-object v2

    .line 1166440
    const/4 v3, 0x0

    invoke-static {v0, v3}, LX/74E;->a(LX/74E;Z)Ljava/util/Map;

    move-result-object v3

    .line 1166441
    const-string v4, "failure_reason"

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166442
    const-string v4, "failure_message"

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166443
    const-string v4, "ChunkMarker"

    invoke-static {v3}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v3

    invoke-static {v0, v4, v3}, LX/74E;->c(LX/74E;Ljava/lang/String;LX/0P1;)V

    .line 1166444
    return-void
.end method

.method public final a(LX/74b;Ljava/lang/String;JJJILX/8O2;)V
    .locals 5

    .prologue
    .line 1166445
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166446
    const-string v1, "chunk_offset"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166447
    const-string v1, "chunk_bytes"

    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166448
    const-string v1, "chunk_size"

    invoke-static {p7, p8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166449
    const-string v1, "auto_retry_count"

    invoke-static {p9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166450
    const-string v1, "upload_session_id"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166451
    const-string v1, "is_using_fbuploader"

    iget-boolean v2, p0, LX/73w;->B:Z

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166452
    iget-wide v2, p0, LX/73w;->z:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166453
    if-eqz p10, :cond_0

    .line 1166454
    const-string v1, "chunk_offset"

    invoke-virtual {p10}, LX/8O2;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166455
    const-string v1, "chunk_size"

    invoke-virtual {p10}, LX/8O2;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166456
    const-string v1, "immediate_retry_count"

    invoke-virtual {p10}, LX/8O2;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166457
    const-string v1, "upload_speed_previous"

    .line 1166458
    iget v2, p10, LX/8O2;->d:F

    move v2, v2

    .line 1166459
    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166460
    const-string v1, "upload_speed_updated"

    .line 1166461
    iget v2, p10, LX/8O2;->e:F

    move v2, v2

    .line 1166462
    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166463
    const-string v1, "chunk_offset_updated"

    invoke-virtual {p10}, LX/8O2;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166464
    const-string v1, "chunk_size_updated"

    invoke-virtual {p10}, LX/8O2;->h()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166465
    const-string v1, "video_chunk_id"

    .line 1166466
    iget-object v2, p10, LX/8O2;->h:Ljava/lang/String;

    move-object v2, v2

    .line 1166467
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166468
    :cond_0
    sget-object v1, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_RESPONSE:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166469
    return-void
.end method

.method public final a(LX/74b;Ljava/lang/String;LX/73y;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 4

    .prologue
    .line 1166470
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166471
    invoke-static {p0, v0, p4}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166472
    const-string v1, "upload_session_id"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166473
    invoke-static {v0, p3}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 1166474
    iget-wide v2, p0, LX/73w;->A:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166475
    sget-object v1, LX/74R;->MEDIA_POST_FAILURE:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166476
    iget-object v0, p0, LX/73w;->F:LX/74E;

    invoke-interface {p3}, LX/73y;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3}, LX/73y;->d()Ljava/lang/String;

    move-result-object v2

    .line 1166477
    const/4 v3, 0x0

    invoke-static {v0, v3}, LX/74E;->a(LX/74E;Z)Ljava/util/Map;

    move-result-object v3

    .line 1166478
    const-string p0, "failure_reason"

    invoke-interface {v3, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166479
    const-string p0, "failure_message"

    invoke-interface {v3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166480
    const-string p0, "PostMarker"

    invoke-static {v3}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v3

    invoke-static {v0, p0, v3}, LX/74E;->c(LX/74E;Ljava/lang/String;LX/0P1;)V

    .line 1166481
    return-void
.end method

.method public final a(LX/74b;Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 4

    .prologue
    .line 1166482
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166483
    invoke-static {p0, v0, p3}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166484
    const-string v1, "upload_session_id"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166485
    iget-object v1, p0, LX/73w;->g:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/73w;->A:J

    .line 1166486
    sget-object v1, LX/74R;->MEDIA_POST_START:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166487
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1166488
    const-string v1, "PostMarker"

    invoke-static {v0, v1}, LX/74E;->a(LX/74E;Ljava/lang/String;)V

    .line 1166489
    return-void
.end method

.method public final a(LX/74b;Ljava/lang/String;Ljava/lang/String;IIIIIIIIJJJJ)V
    .locals 22
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1166490
    invoke-virtual/range {p1 .. p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v20

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    move-wide/from16 v12, p12

    move-wide/from16 v14, p14

    move-wide/from16 v16, p16

    move-wide/from16 v18, p18

    .line 1166491
    invoke-static/range {v1 .. v20}, LX/73w;->a(LX/73w;Ljava/lang/String;Ljava/lang/String;IIIIIIIIJJJJLjava/util/HashMap;)V

    .line 1166492
    return-void
.end method

.method public final a(LX/74b;ZILjava/lang/String;ZIIZILandroid/graphics/RectF;J)V
    .locals 3

    .prologue
    .line 1166493
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166494
    const-string v1, "higher_quality_transcode"

    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166495
    const-string v1, "specified_transcode_bit_rate"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166496
    const-string v1, "video_transcode_profile"

    invoke-virtual {v0, v1, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166497
    const-string v1, "is_video_trim"

    invoke-static {p5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166498
    if-eqz p5, :cond_0

    .line 1166499
    const-string v1, "video_trim_start_time_ms"

    invoke-static {p6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166500
    const-string v1, "video_trim_end_time_ms"

    invoke-static {p7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166501
    :cond_0
    const-string v1, "is_video_muted"

    invoke-static {p8}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166502
    const-string v1, "video_output_rotation_angle"

    invoke-static {p9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166503
    if-eqz p10, :cond_1

    .line 1166504
    const-string v1, "video_crop_rectangle"

    const/16 p2, 0x2c

    .line 1166505
    if-nez p10, :cond_2

    .line 1166506
    const/4 v2, 0x0

    .line 1166507
    :goto_0
    move-object v2, v2

    .line 1166508
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166509
    :cond_1
    const-string v1, "video_transcode_immediate_retry_count"

    invoke-static {p11, p12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166510
    invoke-static {p0, v0}, LX/73w;->c(LX/73w;Ljava/util/HashMap;)V

    .line 1166511
    return-void

    .line 1166512
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1166513
    iget p1, p10, Landroid/graphics/RectF;->left:F

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1166514
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1166515
    iget p1, p10, Landroid/graphics/RectF;->top:F

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1166516
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1166517
    iget p1, p10, Landroid/graphics/RectF;->right:F

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1166518
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1166519
    iget p1, p10, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1166520
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1166521
    iput-object p1, p0, LX/73w;->j:Ljava/lang/String;

    .line 1166522
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1166523
    iget-object p0, v0, LX/74E;->c:LX/74C;

    move-object v0, p0

    .line 1166524
    iput-object p1, v0, LX/74C;->a:Ljava/lang/String;

    .line 1166525
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0ck;LX/742;LX/743;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 1166534
    invoke-static {p0, p1, p2, p3}, LX/73w;->b(LX/73w;Ljava/lang/String;LX/0ck;LX/742;)Ljava/util/HashMap;

    move-result-object v0

    .line 1166535
    iget-object v1, p4, LX/743;->value:Ljava/lang/String;

    sget-object v2, LX/743;->NOT_RELEVANT:LX/743;

    iget-object v2, v2, LX/743;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1166536
    const-string v1, "video_product_type"

    iget-object v2, p4, LX/743;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166537
    :cond_0
    invoke-static {p5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1166538
    const-string v1, "target_type"

    invoke-virtual {v0, v1, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166539
    :cond_1
    const-string v1, "media_attachment_count"

    invoke-static {p6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166540
    const-string v1, "tags_user"

    invoke-static {p7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166541
    invoke-static {p8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1166542
    const-string v1, "composer_set_privacy"

    invoke-virtual {v0, v1, p8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166543
    :cond_2
    invoke-static {p9}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1166544
    const-string v1, "target_id"

    invoke-virtual {v0, v1, p9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166545
    :cond_3
    const-string v1, "upload_queue_size"

    invoke-static {p10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166546
    sget-object v1, LX/74R;->MEDIA_UPLOAD_FLOW_START:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166547
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1166548
    const-string v1, "FlowMarker"

    const/4 v2, 0x0

    invoke-static {v0, v2}, LX/74E;->a(LX/74E;Z)Ljava/util/Map;

    move-result-object v2

    invoke-static {v2}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/74E;->a(LX/74E;Ljava/lang/String;LX/0P1;)V

    .line 1166549
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0ck;LX/742;Lcom/facebook/photos/upload/operation/UploadOperation;ILX/73y;J)V
    .locals 3

    .prologue
    .line 1166705
    invoke-static {p0, p1, p2, p3}, LX/73w;->b(LX/73w;Ljava/lang/String;LX/0ck;LX/742;)Ljava/util/HashMap;

    move-result-object v0

    .line 1166706
    invoke-static {p0, v0, p4}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166707
    const-string v1, "multi_success"

    invoke-static {p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166708
    invoke-static {v0, p6}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 1166709
    invoke-static {v0, p7, p8}, LX/73w;->a(Ljava/util/HashMap;J)V

    .line 1166710
    sget-object v1, LX/74R;->MEDIA_UPLOAD_FLOW_FATAL:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166711
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1166712
    const-string v1, "FlowMarker"

    invoke-static {v0, v1}, LX/74E;->c(LX/74E;Ljava/lang/String;)V

    .line 1166713
    iget-object v1, v0, LX/74E;->b:LX/11i;

    sget-object v2, LX/74E;->a:LX/74D;

    invoke-interface {v1, v2}, LX/11i;->b(LX/0Pq;)V

    .line 1166714
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0ck;LX/742;Lcom/facebook/photos/upload/operation/UploadOperation;ILjava/lang/String;J)V
    .locals 5

    .prologue
    .line 1166779
    invoke-static {p0, p1, p2, p3}, LX/73w;->b(LX/73w;Ljava/lang/String;LX/0ck;LX/742;)Ljava/util/HashMap;

    move-result-object v0

    .line 1166780
    invoke-static {p0, v0, p4}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166781
    if-ltz p5, :cond_0

    .line 1166782
    const-string v1, "multi_success"

    invoke-static {p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166783
    :cond_0
    invoke-static {p6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1166784
    const-string v1, "error_code"

    invoke-virtual {v0, v1, p6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166785
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v1, p7, v2

    if-ltz v1, :cond_2

    .line 1166786
    invoke-static {v0, p7, p8}, LX/73w;->a(Ljava/util/HashMap;J)V

    .line 1166787
    :cond_2
    sget-object v1, LX/74R;->MEDIA_UPLOAD_ATTEMPT_INCOMPLETE:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166788
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0ck;LX/742;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73y;JLjava/lang/String;)V
    .locals 4

    .prologue
    .line 1166770
    invoke-static {p0, p1, p2, p3}, LX/73w;->b(LX/73w;Ljava/lang/String;LX/0ck;LX/742;)Ljava/util/HashMap;

    move-result-object v0

    .line 1166771
    const-string v1, "multi_success"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166772
    const-string v1, "action_source"

    invoke-virtual {v0, v1, p8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166773
    invoke-static {p0, v0, p4}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166774
    invoke-static {v0, p5}, LX/73w;->a(Ljava/util/HashMap;LX/73y;)V

    .line 1166775
    invoke-static {v0, p6, p7}, LX/73w;->a(Ljava/util/HashMap;J)V

    .line 1166776
    sget-object v1, LX/74R;->MEDIA_UPLOAD_FLOW_CANCEL:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166777
    iget-object v0, p0, LX/73w;->F:LX/74E;

    invoke-virtual {v0}, LX/74E;->c()V

    .line 1166778
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0ck;LX/742;Lcom/facebook/photos/upload/operation/UploadOperation;ZJLjava/lang/String;Z)V
    .locals 4

    .prologue
    .line 1166760
    invoke-static {p0, p1, p2, p3}, LX/73w;->b(LX/73w;Ljava/lang/String;LX/0ck;LX/742;)Ljava/util/HashMap;

    move-result-object v1

    .line 1166761
    invoke-static {p0, v1, p4}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166762
    const-string v2, "retry"

    if-eqz p5, :cond_1

    const-string v0, "auto_retry"

    :goto_0
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166763
    const-string v0, "gcm_based_network_retry"

    invoke-static {p9}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166764
    invoke-static {v1, p6, p7}, LX/73w;->a(Ljava/util/HashMap;J)V

    .line 1166765
    invoke-static {p8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1166766
    const-string v0, "error_code"

    invoke-virtual {v1, v0, p8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166767
    :cond_0
    sget-object v0, LX/74R;->MEDIA_UPLOAD_ATTEMPT_RETRY:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166768
    return-void

    .line 1166769
    :cond_1
    const-string v0, "user_retry"

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1166756
    invoke-virtual {p0, p1}, LX/73w;->j(Ljava/lang/String;)LX/74b;

    move-result-object v0

    invoke-virtual {v0}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    .line 1166757
    invoke-static/range {v0 .. v6}, LX/73w;->a(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 1166758
    sget-object v1, LX/74R;->MEDIA_UPLOAD_DIAGNOSTIC:LX/74R;

    invoke-static {p0, v1, v0, v6}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166759
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1166752
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1166753
    iget-object p0, v0, LX/74E;->c:LX/74C;

    move-object v0, p0

    .line 1166754
    iput-object p1, v0, LX/74C;->b:Ljava/util/List;

    .line 1166755
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1166750
    iput-boolean p1, p0, LX/73w;->B:Z

    .line 1166751
    return-void
.end method

.method public final b(LX/74b;)V
    .locals 1

    .prologue
    .line 1166747
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166748
    invoke-static {p0, v0}, LX/73w;->c(LX/73w;Ljava/util/HashMap;)V

    .line 1166749
    return-void
.end method

.method public final b(LX/74b;IJ)V
    .locals 5

    .prologue
    .line 1166741
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166742
    invoke-static {p0, v0}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1166743
    const-string v1, "num_total_segments"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166744
    const-string v1, "video_duration"

    long-to-float v2, p3

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166745
    sget-object v1, LX/74R;->MEDIA_UPLOAD_SEGMENTED_TRANSCODE_SUCCESS:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166746
    return-void
.end method

.method public final b(LX/74b;JI)V
    .locals 4

    .prologue
    .line 1166732
    iget-object v0, p0, LX/73w;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/73w;->v:J

    .line 1166733
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166734
    invoke-static {p0, v0}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1166735
    const-string v1, "bytes"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166736
    const-string v1, "auto_retry_count"

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166737
    sget-object v1, LX/74R;->MEDIA_UPLOAD_TRANSFER_START:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166738
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1166739
    const-string v1, "TransferMarker"

    invoke-static {v0, v1}, LX/74E;->a(LX/74E;Ljava/lang/String;)V

    .line 1166740
    return-void
.end method

.method public final b(LX/74b;JJJJZZZJJZFIZ)V
    .locals 4

    .prologue
    .line 1166715
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166716
    const-string v1, "video_transcode_flow_count"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166717
    const-string v1, "video_transcode_start_count"

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166718
    const-string v1, "video_transcode_success_count"

    invoke-static {p6, p7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166719
    const-string v1, "video_transcode_fail_count"

    invoke-static {p8, p9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166720
    const-string v1, "video_transcode_is_segmented"

    invoke-static {p10}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166721
    const-string v1, "video_requested_server_settings"

    invoke-static {p11}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166722
    const-string v1, "video_server_settings_available"

    invoke-static/range {p12 .. p12}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166723
    const-string v1, "server_specified_transcode_bitrate"

    invoke-static/range {p13 .. p14}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166724
    const-string v1, "server_specified_transcode_dimension"

    invoke-static/range {p15 .. p16}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166725
    const-string v1, "video_using_contextual_config"

    invoke-static/range {p17 .. p17}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166726
    if-gtz p19, :cond_0

    const/4 v1, 0x0

    cmpl-float v1, p18, v1

    if-lez v1, :cond_1

    .line 1166727
    :cond_0
    const-string v1, "video_skip_percentage_threshold"

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float v2, v2, p18

    float-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166728
    const-string v1, "video_skip_bytes_threshold"

    invoke-static/range {p19 .. p19}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166729
    :cond_1
    const-string v1, "video_resize_codec_init_error"

    invoke-static/range {p20 .. p20}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166730
    sget-object v1, LX/74R;->MEDIA_UPLOAD_PROCESS_CHECKPOINT_PERSISTED_TRANSCODE_INFO:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166731
    return-void
.end method

.method public final b(LX/74b;LX/74Z;)V
    .locals 4

    .prologue
    .line 1166643
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166644
    invoke-virtual {p2, v0}, LX/74Z;->a(Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 1166645
    iget-wide v2, p0, LX/73w;->w:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166646
    sget-object v1, LX/74R;->MEDIA_PUBLISH_SENT:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166647
    return-void
.end method

.method public final b(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 4

    .prologue
    .line 1166699
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166700
    invoke-static {p0, v0, p2}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166701
    iget-wide v2, p0, LX/73w;->r:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166702
    sget-object v1, LX/74R;->MEDIA_UPLOAD_FLOW_SUCCESS:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166703
    iget-object v0, p0, LX/73w;->F:LX/74E;

    invoke-virtual {v0}, LX/74E;->f()V

    .line 1166704
    return-void
.end method

.method public final b(LX/74b;Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 4

    .prologue
    .line 1166691
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166692
    invoke-static {p0, v0, p3}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166693
    const-string v1, "upload_session_id"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166694
    iget-wide v2, p0, LX/73w;->A:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166695
    sget-object v1, LX/74R;->MEDIA_POST_SUCCESS:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166696
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1166697
    const-string v1, "PostMarker"

    invoke-static {v0, v1}, LX/74E;->b(LX/74E;Ljava/lang/String;)V

    .line 1166698
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1166688
    const-string v0, "2.1"

    sget-object v1, LX/0ck;->VIDEO:LX/0ck;

    sget-object v2, LX/742;->CHUNKED:LX/742;

    invoke-static {p0, v0, v1, v2}, LX/73w;->b(LX/73w;Ljava/lang/String;LX/0ck;LX/742;)Ljava/util/HashMap;

    move-result-object v0

    .line 1166689
    sget-object v1, LX/74R;->COMPOSER_SUBMIT_VIDEO:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166690
    return-void
.end method

.method public final c(LX/74b;)V
    .locals 4

    .prologue
    .line 1166681
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166682
    invoke-static {p0, v0}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1166683
    iget-wide v2, p0, LX/73w;->t:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166684
    sget-object v1, LX/74R;->MEDIA_UPLOAD_PROCESS_CANCEL:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166685
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1166686
    const-string v1, "ProcessVideoMarker"

    invoke-static {v0, v1}, LX/74E;->d(LX/74E;Ljava/lang/String;)V

    .line 1166687
    return-void
.end method

.method public final c(LX/74b;JI)V
    .locals 4

    .prologue
    .line 1166672
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166673
    invoke-static {p0, v0}, LX/73w;->b(LX/73w;Ljava/util/HashMap;)V

    .line 1166674
    const-string v1, "bytes"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166675
    const-string v1, "auto_retry_count"

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166676
    iget-wide v2, p0, LX/73w;->v:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166677
    sget-object v1, LX/74R;->MEDIA_UPLOAD_TRANSFER_SUCCESS:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166678
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1166679
    const-string v1, "TransferMarker"

    invoke-static {v0, v1}, LX/74E;->b(LX/74E;Ljava/lang/String;)V

    .line 1166680
    return-void
.end method

.method public final c(LX/74b;LX/73y;)V
    .locals 1

    .prologue
    .line 1166669
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166670
    invoke-static {p0, v0, p2}, LX/73w;->b(LX/73w;Ljava/util/HashMap;LX/73y;)V

    .line 1166671
    return-void
.end method

.method public final c(LX/74b;LX/74Z;)V
    .locals 4

    .prologue
    .line 1166664
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166665
    invoke-virtual {p2, v0}, LX/74Z;->a(Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 1166666
    iget-wide v2, p0, LX/73w;->w:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166667
    sget-object v1, LX/74R;->MEDIA_PUBLISH_RECEIVED:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166668
    return-void
.end method

.method public final d(LX/74b;)V
    .locals 3

    .prologue
    .line 1166661
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166662
    sget-object v1, LX/74R;->MEDIA_UPLOAD_CHECKPOINT_FAILURE:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166663
    return-void
.end method

.method public final d(LX/74b;LX/74Z;)V
    .locals 4

    .prologue
    .line 1166656
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166657
    invoke-virtual {p2, v0}, LX/74Z;->a(Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 1166658
    iget-wide v2, p0, LX/73w;->w:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166659
    sget-object v1, LX/74R;->MEDIA_PUBLISH_SUCCESS:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166660
    return-void
.end method

.method public final d(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 3

    .prologue
    .line 1166638
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166639
    invoke-static {p0, v0, p2}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166640
    sget-object v1, LX/74R;->MEDIA_UPLOAD_BATCH_START:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166641
    iget-object v0, p0, LX/73w;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/73w;->s:J

    .line 1166642
    return-void
.end method

.method public final e(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 4

    .prologue
    .line 1166649
    invoke-virtual {p1}, LX/74b;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 1166650
    invoke-static {p0, v0, p2}, LX/73w;->a(LX/73w;Ljava/util/HashMap;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1166651
    iget-wide v2, p0, LX/73w;->x:J

    invoke-static {p0, v0, v2, v3}, LX/73w;->b(LX/73w;Ljava/util/HashMap;J)V

    .line 1166652
    sget-object v1, LX/74R;->MEDIA_UPLOAD_INIT_CANCEL:LX/74R;

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1166653
    iget-object v0, p0, LX/73w;->F:LX/74E;

    .line 1166654
    const-string v1, "InitMarker"

    invoke-static {v0, v1}, LX/74E;->d(LX/74E;Ljava/lang/String;)V

    .line 1166655
    return-void
.end method

.method public final j(Ljava/lang/String;)LX/74b;
    .locals 2

    .prologue
    .line 1166648
    sget-object v0, LX/0ck;->PHOTO:LX/0ck;

    sget-object v1, LX/742;->NOT_RELEVANT:LX/742;

    invoke-direct {p0, p1, v0, v1}, LX/73w;->a(Ljava/lang/String;LX/0ck;LX/742;)LX/74b;

    move-result-object v0

    return-object v0
.end method
