.class public LX/8Fw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/61B;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8Fq;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/8GN;

.field public c:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;LX/8Fr;LX/8GN;)V
    .locals 4
    .param p1    # Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v3, 0xf

    .line 1318346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318347
    iput-object p1, p0, LX/8Fw;->c:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1318348
    iput-object p3, p0, LX/8Fw;->b:LX/8GN;

    .line 1318349
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/8Fw;->a:Ljava/util/List;

    .line 1318350
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 1318351
    iget-object v1, p0, LX/8Fw;->a:Ljava/util/List;

    const/4 v2, 0x0

    invoke-virtual {p2, v2}, LX/8Fr;->a(Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;)LX/8Fq;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1318352
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1318353
    :cond_0
    iget-object v0, p0, LX/8Fw;->c:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    if-eqz v0, :cond_1

    .line 1318354
    invoke-static {p0}, LX/8Fw;->a(LX/8Fw;)V

    .line 1318355
    :cond_1
    return-void
.end method

.method public static a(LX/8Fw;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 1318356
    iget-object v0, p0, LX/8Fw;->c:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    if-nez v0, :cond_0

    move-object v0, v7

    .line 1318357
    :goto_0
    if-eqz v0, :cond_3

    .line 1318358
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a()LX/0Px;

    move-result-object v3

    .line 1318359
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    const/16 v1, 0xf

    if-gt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1318360
    :goto_2
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v6, v0, :cond_3

    .line 1318361
    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 1318362
    iget-object v1, p0, LX/8Fw;->a:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Fq;

    .line 1318363
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->d()Landroid/net/Uri;

    move-result-object v2

    if-nez v2, :cond_2

    move-object v2, v7

    .line 1318364
    :goto_3
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->newBuilder()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->m()F

    move-result v4

    invoke-virtual {v2, v4}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setLeftPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->n()F

    move-result v4

    invoke-virtual {v2, v4}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setTopPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->e()F

    move-result v4

    invoke-virtual {v2, v4}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setWidthPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->f()F

    move-result v0

    invoke-virtual {v2, v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->setHeightPercentage(F)Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams$Builder;->a()Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8Fq;->a(Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;)V

    .line 1318365
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 1318366
    :cond_0
    iget-object v0, p0, LX/8Fw;->b:LX/8GN;

    iget-object v1, p0, LX/8Fw;->c:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    iget v2, p0, LX/8Fw;->d:I

    iget v3, p0, LX/8Fw;->e:I

    const-string v4, ""

    iget-object v5, p0, LX/8Fw;->c:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-virtual {v5}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/8GN;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;IILjava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v6

    .line 1318367
    goto :goto_1

    .line 1318368
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->d()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 1318369
    :cond_3
    :goto_4
    iget-object v0, p0, LX/8Fw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_4

    .line 1318370
    iget-object v0, p0, LX/8Fw;->a:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Fq;

    .line 1318371
    invoke-virtual {v0, v7}, LX/8Fq;->a(Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;)V

    .line 1318372
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 1318373
    :cond_4
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 1318374
    iget v0, p0, LX/8Fw;->d:I

    if-ne p1, v0, :cond_0

    iget v0, p0, LX/8Fw;->e:I

    if-eq p2, v0, :cond_1

    .line 1318375
    :cond_0
    iput p1, p0, LX/8Fw;->d:I

    .line 1318376
    iput p2, p0, LX/8Fw;->e:I

    .line 1318377
    invoke-static {p0}, LX/8Fw;->a(LX/8Fw;)V

    .line 1318378
    :cond_1
    iget-object v0, p0, LX/8Fw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Fq;

    .line 1318379
    invoke-virtual {v0, p1, p2}, LX/8Fq;->a(II)V

    goto :goto_0

    .line 1318380
    :cond_2
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 2

    .prologue
    .line 1318381
    iget-object v0, p0, LX/8Fw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Fq;

    .line 1318382
    invoke-virtual {v0, p1}, LX/8Fq;->a(LX/5Pc;)V

    goto :goto_0

    .line 1318383
    :cond_0
    return-void
.end method

.method public final a([F[F[FJ)V
    .locals 8

    .prologue
    .line 1318384
    iget-object v0, p0, LX/8Fw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Fq;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    .line 1318385
    invoke-virtual/range {v0 .. v5}, LX/8Fq;->a([F[F[FJ)V

    goto :goto_0

    .line 1318386
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1318387
    iget-object v0, p0, LX/8Fw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Fq;

    .line 1318388
    invoke-virtual {v0}, LX/8Fq;->b()V

    goto :goto_0

    .line 1318389
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1318390
    const/4 v0, 0x1

    return v0
.end method
