.class public final LX/7B1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/search/api/GraphSearchQuery;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1177687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1177689
    const/4 v6, 0x0

    .line 1177690
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1177691
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, LX/7BH;

    .line 1177692
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1177693
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1177694
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 1177695
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 1177696
    invoke-virtual {p1, v6}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 1177697
    new-instance v0, Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_1

    invoke-static {v4}, LX/103;->valueOf(Ljava/lang/String;)LX/103;

    move-result-object v4

    :goto_0
    invoke-static {v8}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    invoke-static {v8}, LX/7B5;->valueOf(Ljava/lang/String;)LX/7B5;

    move-result-object v6

    .line 1177698
    :cond_0
    sget-object v8, LX/0Rg;->a:LX/0Rg;

    move-object v8, v8

    .line 1177699
    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/facebook/search/api/GraphSearchQuery;-><init>(Ljava/lang/String;LX/7BH;Ljava/lang/String;LX/103;Ljava/lang/String;LX/7B5;ZLX/0P1;B)V

    .line 1177700
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1177701
    const-class v2, Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1177702
    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v1

    .line 1177703
    iput-object v1, v0, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    .line 1177704
    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 1177705
    iput-boolean v1, v0, Lcom/facebook/search/api/GraphSearchQuery;->m:Z

    .line 1177706
    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 1177707
    iput-boolean v1, v0, Lcom/facebook/search/api/GraphSearchQuery;->n:Z

    .line 1177708
    return-object v0

    :cond_1
    move-object v4, v6

    .line 1177709
    goto :goto_0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1177688
    new-array v0, p1, [Lcom/facebook/search/api/GraphSearchQuery;

    return-object v0
.end method
