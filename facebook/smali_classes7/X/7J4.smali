.class public final LX/7J4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/27U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/27U",
        "<",
        "LX/7Yd;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/37Z;


# direct methods
.method public constructor <init>(LX/37Z;)V
    .locals 0

    .prologue
    .line 1193182
    iput-object p1, p0, LX/7J4;->a:LX/37Z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2NW;)V
    .locals 5

    .prologue
    .line 1193183
    check-cast p1, LX/7Yd;

    .line 1193184
    invoke-interface {p1}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1193185
    iget-object v0, p0, LX/7J4;->a:LX/37Z;

    iget-object v1, p0, LX/7J4;->a:LX/37Z;

    iget-object v1, v1, LX/37Z;->j:LX/2wX;

    invoke-static {v0, v1, p1}, LX/37Z;->a$redex0(LX/37Z;LX/2wX;LX/7Yd;)V

    .line 1193186
    :goto_0
    return-void

    .line 1193187
    :cond_0
    iget-object v0, p0, LX/7J4;->a:LX/37Z;

    iget-object v0, v0, LX/37Z;->e:LX/37f;

    invoke-interface {p1}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    iget v2, v1, Lcom/google/android/gms/common/api/Status;->i:I

    move v1, v2

    .line 1193188
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CastApplicationManager.launch:join: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    iget-object v4, v3, Lcom/google/android/gms/common/api/Status;->j:Ljava/lang/String;

    move-object v3, v4

    .line 1193189
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/37f;->a(ILjava/lang/String;)V

    .line 1193190
    iget-object v0, p0, LX/7J4;->a:LX/37Z;

    invoke-interface {p1}, LX/2NW;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    iget v2, v1, Lcom/google/android/gms/common/api/Status;->i:I

    move v1, v2

    .line 1193191
    const-string v2, "joinApplication"

    invoke-virtual {v0, v1, v2}, LX/37Z;->a(ILjava/lang/String;)V

    goto :goto_0
.end method
