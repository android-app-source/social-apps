.class public final LX/7OV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1201704
    const/4 v13, 0x0

    .line 1201705
    const/4 v12, 0x0

    .line 1201706
    const/4 v11, 0x0

    .line 1201707
    const/4 v10, 0x0

    .line 1201708
    const/4 v9, 0x0

    .line 1201709
    const/4 v8, 0x0

    .line 1201710
    const/4 v7, 0x0

    .line 1201711
    const/4 v6, 0x0

    .line 1201712
    const/4 v5, 0x0

    .line 1201713
    const/4 v4, 0x0

    .line 1201714
    const/4 v3, 0x0

    .line 1201715
    const/4 v2, 0x0

    .line 1201716
    const/4 v1, 0x0

    .line 1201717
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 1201718
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1201719
    const/4 v1, 0x0

    .line 1201720
    :goto_0
    return v1

    .line 1201721
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1201722
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_b

    .line 1201723
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 1201724
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1201725
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 1201726
    const-string v15, "__type__"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_2

    const-string v15, "__typename"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1201727
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v13

    goto :goto_1

    .line 1201728
    :cond_3
    const-string v15, "id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1201729
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 1201730
    :cond_4
    const-string v15, "video_channel_can_viewer_follow"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 1201731
    const/4 v4, 0x1

    .line 1201732
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 1201733
    :cond_5
    const-string v15, "video_channel_can_viewer_subscribe"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1201734
    const/4 v3, 0x1

    .line 1201735
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 1201736
    :cond_6
    const-string v15, "video_channel_curator_profile"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 1201737
    invoke-static/range {p0 .. p1}, LX/7OU;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1201738
    :cond_7
    const-string v15, "video_channel_has_viewer_subscribed"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 1201739
    const/4 v2, 0x1

    .line 1201740
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 1201741
    :cond_8
    const-string v15, "video_channel_is_viewer_following"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 1201742
    const/4 v1, 0x1

    .line 1201743
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto/16 :goto_1

    .line 1201744
    :cond_9
    const-string v15, "video_channel_subtitle"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 1201745
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1201746
    :cond_a
    const-string v15, "video_channel_title"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1201747
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1201748
    :cond_b
    const/16 v14, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 1201749
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1201750
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1201751
    if-eqz v4, :cond_c

    .line 1201752
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->a(IZ)V

    .line 1201753
    :cond_c
    if-eqz v3, :cond_d

    .line 1201754
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->a(IZ)V

    .line 1201755
    :cond_d
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1201756
    if-eqz v2, :cond_e

    .line 1201757
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 1201758
    :cond_e
    if-eqz v1, :cond_f

    .line 1201759
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 1201760
    :cond_f
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1201761
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1201762
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1201763
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1201764
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1201765
    if-eqz v0, :cond_0

    .line 1201766
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1201767
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1201768
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1201769
    if-eqz v0, :cond_1

    .line 1201770
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1201771
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1201772
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1201773
    if-eqz v0, :cond_2

    .line 1201774
    const-string v1, "video_channel_can_viewer_follow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1201775
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1201776
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1201777
    if-eqz v0, :cond_3

    .line 1201778
    const-string v1, "video_channel_can_viewer_subscribe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1201779
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1201780
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1201781
    if-eqz v0, :cond_4

    .line 1201782
    const-string v1, "video_channel_curator_profile"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1201783
    invoke-static {p0, v0, p2, p3}, LX/7OU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1201784
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1201785
    if-eqz v0, :cond_5

    .line 1201786
    const-string v1, "video_channel_has_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1201787
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1201788
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1201789
    if-eqz v0, :cond_6

    .line 1201790
    const-string v1, "video_channel_is_viewer_following"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1201791
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1201792
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1201793
    if-eqz v0, :cond_7

    .line 1201794
    const-string v1, "video_channel_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1201795
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1201796
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1201797
    if-eqz v0, :cond_8

    .line 1201798
    const-string v1, "video_channel_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1201799
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 1201800
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1201801
    return-void
.end method
