.class public LX/7EJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:I

.field public f:LX/7FH;

.field private final g:Landroid/content/res/Resources;

.field private final h:LX/1x4;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/1x4;)V
    .locals 0

    .prologue
    .line 1184613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184614
    iput-object p1, p0, LX/7EJ;->g:Landroid/content/res/Resources;

    .line 1184615
    iput-object p2, p0, LX/7EJ;->h:LX/1x4;

    .line 1184616
    return-void
.end method

.method public static a(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;LX/2uF;)LX/7EQ;
    .locals 4
    .param p1    # Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1184609
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1184610
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/39O;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1184611
    iget-object v1, p0, LX/7EJ;->h:LX/1x4;

    invoke-static {v0, v1, p2}, LX/7FS;->a(Ljava/lang/String;LX/1x4;LX/2uF;)Ljava/lang/String;

    move-result-object v0

    .line 1184612
    :cond_0
    new-instance v1, LX/7EQ;

    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;->a()I

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, LX/7EQ;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;I)LX/7FA;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v5, -0x1

    .line 1184597
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1184598
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v3

    .line 1184599
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->q()LX/2uF;

    move-result-object v4

    .line 1184600
    if-ne p2, v5, :cond_2

    move-object v2, v1

    .line 1184601
    :goto_0
    if-eqz v4, :cond_0

    invoke-virtual {v4}, LX/39O;->a()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1184602
    iget-object v5, p0, LX/7EJ;->h:LX/1x4;

    invoke-static {v0, v5, v4}, LX/7FS;->a(Ljava/lang/String;LX/1x4;LX/2uF;)Ljava/lang/String;

    move-result-object v0

    .line 1184603
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->CHECKBOX:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    if-ne v4, v5, :cond_1

    .line 1184604
    iget-object v1, p0, LX/7EJ;->g:Landroid/content/res/Resources;

    const v4, 0x7f081a87

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1184605
    :cond_1
    new-instance v4, LX/7FA;

    invoke-direct {v4, v2, v0, v1, v3}, LX/7FA;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v4

    .line 1184606
    :cond_2
    iget v2, p0, LX/7EJ;->d:I

    if-ne v2, v5, :cond_3

    .line 1184607
    iget-object v2, p0, LX/7EJ;->g:Landroid/content/res/Resources;

    const v5, 0x7f081a85

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v2, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1184608
    :cond_3
    iget-object v2, p0, LX/7EJ;->g:Landroid/content/res/Resources;

    const v5, 0x7f081a84

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    iget v7, p0, LX/7EJ;->d:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v2, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static a(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;ZLjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;",
            "Z",
            "Ljava/util/List",
            "<",
            "LX/7F3;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1184505
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->q()LX/2uF;

    move-result-object v0

    invoke-static {p0, p2, v0}, LX/7EJ;->a(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;LX/2uF;)LX/7EQ;

    move-result-object v1

    .line 1184506
    if-eqz p3, :cond_0

    .line 1184507
    new-instance v0, LX/7FD;

    sget-object v2, LX/7F7;->RADIOWRITEIN:LX/7F7;

    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v1, v3}, LX/7FD;-><init>(LX/7F7;LX/7EQ;Ljava/lang/String;)V

    .line 1184508
    :goto_0
    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184509
    new-instance v0, LX/7F3;

    sget-object v1, LX/7F7;->DIVIDER:LX/7F7;

    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7F3;-><init>(LX/7F7;Ljava/lang/String;)V

    .line 1184510
    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184511
    return-void

    .line 1184512
    :cond_0
    new-instance v0, LX/7FB;

    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7FB;-><init>(LX/7EQ;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/List;LX/7F3;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7F3;",
            ">;",
            "LX/7F3;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1184593
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184594
    new-instance v0, LX/7F3;

    sget-object v1, LX/7F7;->DIVIDER:LX/7F7;

    invoke-direct {v0, v1, p2}, LX/7F3;-><init>(LX/7F7;Ljava/lang/String;)V

    .line 1184595
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184596
    return-void
.end method

.method public static d(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/7F3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1184617
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1184618
    iget v1, p0, LX/7EJ;->e:I

    invoke-static {p0, p1, v1}, LX/7EJ;->a(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;I)LX/7FA;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184619
    new-instance v1, LX/7F5;

    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/7F5;-><init>(Ljava/lang/String;)V

    .line 1184620
    invoke-virtual {p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->l()Z

    move-result v2

    .line 1184621
    iput-boolean v2, v1, LX/7F5;->d:Z

    .line 1184622
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184623
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/7F3;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v5, 0x10

    .line 1184517
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1184518
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1184519
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;

    .line 1184520
    if-eqz v0, :cond_0

    .line 1184521
    iget v3, p0, LX/7EJ;->e:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/7EJ;->e:I

    .line 1184522
    iget v3, p0, LX/7EJ;->d:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    iget v3, p0, LX/7EJ;->e:I

    iget v4, p0, LX/7EJ;->d:I

    if-gt v3, v4, :cond_5

    .line 1184523
    :cond_1
    sget-object v3, LX/7EI;->a:[I

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyQuestionType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 1184524
    :pswitch_0
    const/4 p1, 0x0

    .line 1184525
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1184526
    iget v3, p0, LX/7EJ;->e:I

    invoke-static {p0, v0, v3}, LX/7EJ;->a(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;I)LX/7FA;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184527
    new-instance v3, LX/7FC;

    const/16 v6, 0xc

    invoke-direct {v3, v6}, LX/7FC;-><init>(I)V

    .line 1184528
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184529
    new-instance v3, LX/7F3;

    sget-object v6, LX/7F7;->DIVIDER:LX/7F7;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v6, v7}, LX/7F3;-><init>(LX/7F7;Ljava/lang/String;)V

    .line 1184530
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184531
    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->p()LX/0Px;

    move-result-object v6

    .line 1184532
    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->a()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1184533
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v6, v3}, LX/0Ph;->c(Ljava/lang/Iterable;I)Ljava/lang/Iterable;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;

    .line 1184534
    invoke-static {p0, v0, v3, p1, v4}, LX/7EJ;->a(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;ZLjava/util/List;)V

    goto :goto_1

    .line 1184535
    :cond_2
    invoke-static {v6}, LX/0Ph;->g(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;

    const/4 v6, 0x1

    invoke-static {p0, v0, v3, v6, v4}, LX/7EJ;->a(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;ZLjava/util/List;)V

    .line 1184536
    :cond_3
    move-object v0, v4

    .line 1184537
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1184538
    new-instance v0, LX/7FC;

    invoke-direct {v0, v5}, LX/7FC;-><init>(I)V

    .line 1184539
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1184540
    :pswitch_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1184541
    iget v3, p0, LX/7EJ;->e:I

    invoke-static {p0, v0, v3}, LX/7EJ;->a(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;I)LX/7FA;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184542
    new-instance v3, LX/7FC;

    const/16 v6, 0xc

    invoke-direct {v3, v6}, LX/7FC;-><init>(I)V

    .line 1184543
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184544
    new-instance v3, LX/7F3;

    sget-object v6, LX/7F7;->DIVIDER:LX/7F7;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v6, v7}, LX/7F3;-><init>(LX/7F7;Ljava/lang/String;)V

    .line 1184545
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184546
    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->p()LX/0Px;

    move-result-object v6

    .line 1184547
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v6, v3}, LX/0Ph;->c(Ljava/lang/Iterable;I)Ljava/lang/Iterable;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;

    .line 1184548
    new-instance v8, LX/7F4;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->q()LX/2uF;

    move-result-object p1

    invoke-static {p0, v3, p1}, LX/7EJ;->a(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;LX/2uF;)LX/7EQ;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v8, v3, p1}, LX/7F4;-><init>(LX/7EQ;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v8, v3}, LX/7EJ;->a(Ljava/util/List;LX/7F3;Ljava/lang/String;)V

    goto :goto_2

    .line 1184549
    :cond_4
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    const/4 v3, 0x1

    :goto_3
    invoke-static {v3}, LX/0nE;->a(Z)V

    .line 1184550
    invoke-static {v6}, LX/0Ph;->g(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->q()LX/2uF;

    move-result-object v6

    invoke-static {p0, v3, v6}, LX/7EJ;->a(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;LX/2uF;)LX/7EQ;

    move-result-object v3

    .line 1184551
    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->a()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1184552
    new-instance v6, LX/7FD;

    sget-object v7, LX/7F7;->CHECKBOXWRITEIN:LX/7F7;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v3, v8}, LX/7FD;-><init>(LX/7F7;LX/7EQ;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v6, v3}, LX/7EJ;->a(Ljava/util/List;LX/7F3;Ljava/lang/String;)V

    .line 1184553
    :goto_4
    move-object v0, v4

    .line 1184554
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1184555
    new-instance v0, LX/7FC;

    invoke-direct {v0, v5}, LX/7FC;-><init>(I)V

    .line 1184556
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1184557
    :pswitch_2
    invoke-static {p0, v0}, LX/7EJ;->d(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;)Ljava/util/List;

    move-result-object v0

    .line 1184558
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1184559
    new-instance v0, LX/7FC;

    invoke-direct {v0, v5}, LX/7FC;-><init>(I)V

    .line 1184560
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1184561
    :pswitch_3
    const/16 v7, 0xc

    .line 1184562
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1184563
    const/4 v4, -0x1

    invoke-static {p0, v0, v4}, LX/7EJ;->a(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;I)LX/7FA;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184564
    new-instance v4, LX/7FC;

    invoke-direct {v4, v7}, LX/7FC;-><init>(I)V

    .line 1184565
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184566
    new-instance v4, LX/7F8;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, LX/7F8;-><init>(Ljava/lang/String;)V

    .line 1184567
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184568
    new-instance v4, LX/7FC;

    invoke-direct {v4, v7}, LX/7FC;-><init>(I)V

    .line 1184569
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184570
    new-instance v4, LX/7F3;

    sget-object v6, LX/7F7;->DIVIDER:LX/7F7;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v6, v7}, LX/7F3;-><init>(LX/7F7;Ljava/lang/String;)V

    .line 1184571
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184572
    move-object v0, v3

    .line 1184573
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1184574
    new-instance v0, LX/7FC;

    invoke-direct {v0, v5}, LX/7FC;-><init>(I)V

    .line 1184575
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184576
    iget v0, p0, LX/7EJ;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/7EJ;->e:I

    goto/16 :goto_0

    .line 1184577
    :pswitch_4
    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;->NOTIFICATION:Lcom/facebook/graphql/enums/GraphQLStructuredSurveyCustomQuestionType;

    if-ne v0, v3, :cond_0

    .line 1184578
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1184579
    new-instance v3, LX/7F9;

    iget-object v4, p0, LX/7EJ;->f:LX/7FH;

    invoke-direct {v3, v4}, LX/7F9;-><init>(LX/7FH;)V

    .line 1184580
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184581
    move-object v0, v0

    .line 1184582
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1184583
    new-instance v0, LX/7FC;

    invoke-direct {v0, v5}, LX/7FC;-><init>(I)V

    .line 1184584
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184585
    iget v0, p0, LX/7EJ;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/7EJ;->e:I

    goto/16 :goto_0

    .line 1184586
    :cond_5
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1184587
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No Valid Types In Question List"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1184588
    :cond_6
    return-object v1

    .line 1184589
    :cond_7
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;

    .line 1184590
    invoke-static {p0, v0, v3, p1, v4}, LX/7EJ;->a(LX/7EJ;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyResponseOptionFragmentModel;ZLjava/util/List;)V

    goto :goto_5

    .line 1184591
    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 1184592
    :cond_9
    new-instance v6, LX/7F4;

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v3, v7}, LX/7F4;-><init>(LX/7EQ;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyConfiguredQuestionFragmentModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v6, v3}, LX/7EJ;->a(Ljava/util/List;LX/7F3;Ljava/lang/String;)V

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/7FH;)V
    .locals 2
    .param p1    # LX/7FH;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1184513
    if-nez p1, :cond_0

    .line 1184514
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null SurveyNotificationWrapper"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1184515
    :cond_0
    iput-object p1, p0, LX/7EJ;->f:LX/7FH;

    .line 1184516
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1184501
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1184502
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null/Empty Intro Toast Text"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1184503
    :cond_0
    iput-object p1, p0, LX/7EJ;->a:Ljava/lang/String;

    .line 1184504
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1184497
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1184498
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null/Empty Intro Toast Button Text"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1184499
    :cond_0
    iput-object p1, p0, LX/7EJ;->b:Ljava/lang/String;

    .line 1184500
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1184493
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1184494
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null/Empty Outro Toast Text"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1184495
    :cond_0
    iput-object p1, p0, LX/7EJ;->c:Ljava/lang/String;

    .line 1184496
    return-void
.end method
