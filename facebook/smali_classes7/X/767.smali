.class public final LX/767;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Me;


# instance fields
.field private a:Landroid/os/IBinder;


# direct methods
.method public constructor <init>(Landroid/os/IBinder;)V
    .locals 0

    .prologue
    .line 1170774
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1170775
    iput-object p1, p0, LX/767;->a:Landroid/os/IBinder;

    .line 1170776
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;)V
    .locals 5

    .prologue
    .line 1170778
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 1170779
    :try_start_0
    const-string v0, "com.facebook.push.mqtt.ipc.MqttChannelStateListener"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 1170780
    if-eqz p1, :cond_0

    .line 1170781
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1170782
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/push/mqtt/ipc/MqttChannelStateInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1170783
    :goto_0
    iget-object v0, p0, LX/767;->a:Landroid/os/IBinder;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-interface {v0, v2, v1, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1170784
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1170785
    return-void

    .line 1170786
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1170787
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 1170777
    iget-object v0, p0, LX/767;->a:Landroid/os/IBinder;

    return-object v0
.end method
