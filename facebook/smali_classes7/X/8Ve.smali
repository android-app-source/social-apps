.class public LX/8Ve;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3Rb;


# direct methods
.method public constructor <init>(LX/3Rb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1353199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1353200
    iput-object p1, p0, LX/8Ve;->a:LX/3Rb;

    .line 1353201
    return-void
.end method

.method public static b(LX/0QB;)LX/8Ve;
    .locals 2

    .prologue
    .line 1353202
    new-instance v1, LX/8Ve;

    invoke-static {p0}, LX/3Rb;->a(LX/0QB;)LX/3Rb;

    move-result-object v0

    check-cast v0, LX/3Rb;

    invoke-direct {v1, v0}, LX/8Ve;-><init>(LX/3Rb;)V

    .line 1353203
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/8Vd;
    .locals 3

    .prologue
    .line 1353204
    new-instance v0, Lcom/facebook/user/model/UserKey;

    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-direct {v0, v1, p1}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1353205
    new-instance v1, LX/8Vd;

    iget-object v2, p0, LX/8Ve;->a:LX/3Rb;

    invoke-direct {v1, v2, v0}, LX/8Vd;-><init>(LX/3Rb;LX/0Px;)V

    return-object v1
.end method

.method public final a(Ljava/util/List;)LX/8Vd;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/8Vd;"
        }
    .end annotation

    .prologue
    .line 1353206
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1353207
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v0, 0x3

    if-ge v1, v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1353208
    new-instance v3, Lcom/facebook/user/model/UserKey;

    sget-object v4, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v3, v4, v0}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1353209
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1353210
    :cond_0
    new-instance v0, LX/8Vd;

    iget-object v1, p0, LX/8Ve;->a:LX/3Rb;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/8Vd;-><init>(LX/3Rb;LX/0Px;)V

    return-object v0
.end method
