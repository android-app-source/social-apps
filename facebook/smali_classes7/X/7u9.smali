.class public final LX/7u9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1270072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;
    .locals 6

    .prologue
    .line 1270073
    if-nez p0, :cond_0

    .line 1270074
    const/4 v0, 0x0

    .line 1270075
    :goto_0
    return-object v0

    .line 1270076
    :cond_0
    new-instance v2, LX/4Z6;

    invoke-direct {v2}, LX/4Z6;-><init>()V

    .line 1270077
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1270078
    iput-object v0, v2, LX/4Z6;->b:Ljava/lang/String;

    .line 1270079
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1270080
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1270081
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1270082
    invoke-virtual {p0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;

    .line 1270083
    if-nez v0, :cond_3

    .line 1270084
    const/4 v4, 0x0

    .line 1270085
    :goto_2
    move-object v0, v4

    .line 1270086
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1270087
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1270088
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1270089
    iput-object v0, v2, LX/4Z6;->c:LX/0Px;

    .line 1270090
    :cond_2
    invoke-virtual {v2}, LX/4Z6;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v0

    goto :goto_0

    .line 1270091
    :cond_3
    new-instance v4, LX/4Vm;

    invoke-direct {v4}, LX/4Vm;-><init>()V

    .line 1270092
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->a()I

    move-result v5

    .line 1270093
    iput v5, v4, LX/4Vm;->b:I

    .line 1270094
    invoke-virtual {v0}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel$TemplateTokensModel;->b()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v5

    .line 1270095
    iput-object v5, v4, LX/4Vm;->c:Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    .line 1270096
    invoke-virtual {v4}, LX/4Vm;->a()Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;

    move-result-object v4

    goto :goto_2
.end method
