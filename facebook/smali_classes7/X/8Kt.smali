.class public final LX/8Kt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/upload/operation/UploadOperation;

.field public final synthetic b:Lcom/facebook/photos/upload/operation/UploadOperation;

.field public final synthetic c:Z

.field public final synthetic d:LX/1EZ;


# direct methods
.method public constructor <init>(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;Lcom/facebook/photos/upload/operation/UploadOperation;Z)V
    .locals 0

    .prologue
    .line 1330797
    iput-object p1, p0, LX/8Kt;->d:LX/1EZ;

    iput-object p2, p0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    iput-object p3, p0, LX/8Kt;->b:Lcom/facebook/photos/upload/operation/UploadOperation;

    iput-boolean p4, p0, LX/8Kt;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 16

    .prologue
    .line 1330798
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    iget-object v2, v2, LX/1EZ;->x:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v3}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1330799
    :cond_0
    :goto_0
    return-void

    .line 1330800
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    iget-object v2, v2, LX/1EZ;->w:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v3}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330801
    if-eqz v2, :cond_2

    .line 1330802
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Kt;->d:LX/1EZ;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-static {v3, v4}, LX/1EZ;->k(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1330803
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Kt;->d:LX/1EZ;

    sget-object v4, LX/8Kx;->UserRestart:LX/8Kx;

    const-string v5, "User requested upload restart"

    invoke-virtual {v3, v2, v4, v5}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Kx;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1330804
    :catch_0
    move-exception v2

    move-object v3, v2

    .line 1330805
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    iget-object v2, v2, LX/1EZ;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v4, "UploadManager onFailure throwable"

    invoke-virtual {v2, v4, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1330806
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->u()Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    move-result-object v7

    .line 1330807
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/photos/upload/operation/UploadOperation;->a(Lcom/facebook/photos/upload/operation/UploadInterruptionCause;)V

    .line 1330808
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/photos/upload/operation/UploadOperation;->a(Lcom/facebook/photos/upload/operation/UploadRecords;)V

    .line 1330809
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/8Kt;->c:Z

    if-eqz v2, :cond_3

    .line 1330810
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Kt;->d:LX/1EZ;

    iget-object v3, v3, LX/1EZ;->k:LX/0SF;

    invoke-virtual {v3}, LX/0SF;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/facebook/photos/upload/operation/UploadOperation;->c(J)V

    .line 1330811
    :cond_3
    const/4 v5, 0x1

    .line 1330812
    const/4 v4, 0x0

    .line 1330813
    const/4 v3, 0x0

    .line 1330814
    if-nez p1, :cond_8

    .line 1330815
    new-instance p1, Ljava/lang/Throwable;

    const-string v2, "null"

    move-object/from16 v0, p1

    invoke-direct {v0, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    move-object v8, v3

    move-object v9, v4

    move-object/from16 v3, p1

    .line 1330816
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2, v9}, Lcom/facebook/photos/upload/operation/UploadOperation;->a(Lcom/facebook/photos/upload/operation/UploadInterruptionCause;)V

    .line 1330817
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2, v8}, Lcom/facebook/photos/upload/operation/UploadOperation;->a(Lcom/facebook/photos/upload/operation/UploadRecords;)V

    .line 1330818
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->a()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1330819
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->s()V

    .line 1330820
    :cond_4
    if-eqz v9, :cond_11

    invoke-virtual {v9}, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->h()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1330821
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->al()V

    .line 1330822
    const/4 v2, 0x0

    move v12, v2

    .line 1330823
    :goto_2
    sget-boolean v2, LX/1EZ;->a:Z

    if-eqz v2, :cond_5

    .line 1330824
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    .line 1330825
    :cond_5
    instance-of v2, v3, Ljava/util/concurrent/CancellationException;

    if-nez v2, :cond_6

    instance-of v2, v3, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v2, :cond_b

    move-object v0, v3

    check-cast v0, Lcom/facebook/fbservice/service/ServiceException;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/facebook/fbservice/service/ServiceException;->getErrorCode()LX/1nY;

    move-result-object v2

    sget-object v4, LX/1nY;->CANCELLED:LX/1nY;

    if-ne v2, v4, :cond_b

    .line 1330826
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    iget-object v2, v2, LX/1EZ;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8L4;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2, v3}, LX/8L4;->c(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1330827
    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 1330828
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    iget-object v2, v2, LX/1EZ;->z:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v3}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 1330829
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1330830
    if-nez v2, :cond_7

    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->az()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1330831
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    iget-object v2, v2, LX/1EZ;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8LX;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2, v3}, LX/8LX;->d(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/73w;

    move-result-object v2

    .line 1330832
    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v3}, Lcom/facebook/photos/upload/operation/UploadOperation;->av()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1330833
    const-string v3, "2.1"

    sget-object v4, LX/742;->CHUNKED:LX/742;

    invoke-virtual {v2, v3, v4}, LX/73w;->a(Ljava/lang/String;LX/742;)LX/74b;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2, v3, v4}, LX/73w;->b(LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1330834
    :cond_7
    :goto_3
    const/4 v3, 0x1

    .line 1330835
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    invoke-static {v2}, LX/1EZ;->t(LX/1EZ;)Z

    move-result v2

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1330836
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    iget-object v2, v2, LX/1EZ;->q:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8Kp;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v4}, Lcom/facebook/photos/upload/operation/UploadOperation;->N()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/8Kp;->b(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    .line 1330837
    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v4, v0, LX/8Kt;->d:LX/1EZ;

    iget-object v4, v4, LX/1EZ;->q:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-static {v4, v2}, LX/8Kp;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 1330838
    const/4 v2, 0x0

    .line 1330839
    :goto_4
    if-eqz v2, :cond_0

    .line 1330840
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-static {v2, v3}, LX/1EZ;->k(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1330841
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->i()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1330842
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    iget-object v2, v2, LX/1EZ;->h:LX/1Ea;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Kt;->d:LX/1EZ;

    invoke-virtual {v2, v3}, LX/1Ea;->a(LX/1EZ;)V

    .line 1330843
    :goto_5
    if-eqz v12, :cond_0

    .line 1330844
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-static {v2, v3}, LX/1EZ;->j(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto/16 :goto_0

    .line 1330845
    :cond_8
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v2, :cond_9

    .line 1330846
    move-object/from16 v0, p1

    check-cast v0, Lcom/facebook/fbservice/service/ServiceException;

    move-object v2, v0

    .line 1330847
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/ServiceException;->getResult()Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Parcelable;

    .line 1330848
    if-eqz v2, :cond_13

    instance-of v6, v2, Landroid/os/Bundle;

    if-eqz v6, :cond_13

    .line 1330849
    check-cast v2, Landroid/os/Bundle;

    .line 1330850
    invoke-static {v2}, LX/8OT;->a(Landroid/os/Bundle;)Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    move-result-object v3

    .line 1330851
    invoke-static {v2}, LX/8OT;->b(Landroid/os/Bundle;)Lcom/facebook/photos/upload/operation/UploadRecords;

    move-result-object v2

    :goto_6
    move-object v8, v2

    move-object v9, v3

    move-object/from16 v3, p1

    .line 1330852
    goto/16 :goto_1

    :cond_9
    move-object/from16 v0, p1

    instance-of v2, v0, LX/8OT;

    if-eqz v2, :cond_12

    .line 1330853
    move-object/from16 v0, p1

    check-cast v0, LX/8OT;

    move-object v2, v0

    .line 1330854
    invoke-virtual {v2}, LX/8OT;->a()Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    move-result-object v4

    .line 1330855
    invoke-virtual {v2}, LX/8OT;->b()Lcom/facebook/photos/upload/operation/UploadRecords;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v3

    move-object v8, v3

    move-object v9, v4

    move-object/from16 v3, p1

    goto/16 :goto_1

    .line 1330856
    :catchall_0
    move-exception v2

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v2

    .line 1330857
    :cond_a
    const-string v3, "2.0"

    move-object/from16 v0, p0

    iget-object v4, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v4}, Lcom/facebook/photos/upload/operation/UploadOperation;->a()LX/0ck;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-static {v6}, LX/1EZ;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/742;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/8Kt;->d:LX/1EZ;

    iget-object v8, v8, LX/1EZ;->k:LX/0SF;

    invoke-virtual {v8}, LX/0SF;->a()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v10, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v10}, Lcom/facebook/photos/upload/operation/UploadOperation;->ak()J

    move-result-wide v10

    sub-long/2addr v8, v10

    const-string v10, "Exception"

    invoke-virtual/range {v2 .. v10}, LX/73w;->a(Ljava/lang/String;LX/0ck;LX/742;Lcom/facebook/photos/upload/operation/UploadOperation;LX/73y;JLjava/lang/String;)V

    goto/16 :goto_3

    .line 1330858
    :cond_b
    if-eqz v9, :cond_c

    if-nez v8, :cond_d

    .line 1330859
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    iget-object v2, v2, LX/1EZ;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v4, "UploadManager"

    const-string v5, "Missing UploadInterruptionCause"

    invoke-virtual {v2, v4, v5, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1330860
    new-instance v9, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v4, "Missing UploadInterruptionCause"

    invoke-direct {v2, v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v2}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v2

    invoke-direct {v9, v2}, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;-><init>(LX/73z;)V

    .line 1330861
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2, v9}, Lcom/facebook/photos/upload/operation/UploadOperation;->a(Lcom/facebook/photos/upload/operation/UploadInterruptionCause;)V

    .line 1330862
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    new-instance v3, Lcom/facebook/photos/upload/operation/UploadRecords;

    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/photos/upload/operation/UploadRecords;-><init>(Ljava/util/Map;)V

    invoke-virtual {v2, v3}, Lcom/facebook/photos/upload/operation/UploadOperation;->a(Lcom/facebook/photos/upload/operation/UploadRecords;)V

    .line 1330863
    :cond_d
    invoke-virtual {v9}, Lcom/facebook/photos/upload/operation/UploadInterruptionCause;->h()Z

    move-result v2

    if-nez v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->az()Z

    move-result v2

    if-nez v2, :cond_e

    .line 1330864
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    iget-object v2, v2, LX/1EZ;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8LX;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2, v3}, LX/8LX;->d(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/73w;

    move-result-object v3

    const-string v4, "2.0"

    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->a()LX/0ck;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-static {v6}, LX/1EZ;->f(Lcom/facebook/photos/upload/operation/UploadOperation;)LX/742;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v8}, Lcom/facebook/photos/upload/operation/UploadRecords;->a()LX/0P1;

    move-result-object v2

    invoke-virtual {v2}, LX/0P1;->size()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    iget-object v2, v2, LX/1EZ;->k:LX/0SF;

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->ak()J

    move-result-wide v14

    sub-long/2addr v10, v14

    invoke-virtual/range {v3 .. v11}, LX/73w;->a(Ljava/lang/String;LX/0ck;LX/742;Lcom/facebook/photos/upload/operation/UploadOperation;ILX/73y;J)V

    .line 1330865
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-static {v2, v3}, LX/1EZ;->l(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto/16 :goto_3

    .line 1330866
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, LX/8Kt;->d:LX/1EZ;

    const-string v3, "Upload failed retry"

    invoke-virtual {v2, v3}, LX/1EZ;->d(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_5

    :cond_10
    move v2, v3

    goto/16 :goto_4

    :cond_11
    move v12, v5

    goto/16 :goto_2

    :cond_12
    move-object v8, v3

    move-object v9, v4

    move-object/from16 v3, p1

    goto/16 :goto_1

    :cond_13
    move-object v2, v3

    move-object v3, v4

    goto/16 :goto_6
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 1330867
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v1, 0x0

    .line 1330868
    :try_start_0
    iget-object v0, p0, LX/8Kt;->d:LX/1EZ;

    invoke-static {v0}, LX/1EZ;->t(LX/1EZ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1330869
    iget-object v0, p0, LX/8Kt;->d:LX/1EZ;

    iget-object v0, v0, LX/1EZ;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Kp;

    iget-object v2, p0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330870
    iget-object v3, v0, LX/8Kp;->f:Ljava/util/Map;

    .line 1330871
    iget-object v4, v2, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v4, v4

    .line 1330872
    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1330873
    :cond_0
    sget-boolean v0, LX/1EZ;->a:Z

    if-eqz v0, :cond_1

    .line 1330874
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Future.onSuccess() for "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330875
    iget-object v3, v2, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v2, v3

    .line 1330876
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1330877
    iget-boolean v2, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v2, v2

    .line 1330878
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", error: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1330879
    iget-object v2, p1, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v2, v2

    .line 1330880
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1330881
    iget-object v2, p1, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    move-object v2, v2

    .line 1330882
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1330883
    :cond_1
    iget-object v0, p0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->a(Lcom/facebook/photos/upload/operation/UploadInterruptionCause;)V

    .line 1330884
    iget-object v0, p0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    const/4 v2, 0x0

    .line 1330885
    iput-object v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->G:Lcom/facebook/photos/upload/operation/UploadRecords;

    .line 1330886
    iget-object v0, p0, LX/8Kt;->d:LX/1EZ;

    iget-object v2, p0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-static {v0, v2}, LX/1EZ;->k(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3

    .line 1330887
    :try_start_1
    const-string v0, "fbids"

    invoke-virtual {p1, v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-object v3, v0

    .line 1330888
    :goto_0
    :try_start_2
    const-string v0, "graphql_story"

    invoke-virtual {p1, v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1330889
    if-eqz v0, :cond_b

    .line 1330890
    const-string v2, "graphql_story"

    invoke-static {v0, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    move-object v4, v0

    .line 1330891
    :goto_2
    :try_start_3
    const-string v0, "shot_fbid"

    invoke-virtual {p1, v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1330892
    if-eqz v0, :cond_2

    .line 1330893
    const-string v2, "shot_fbid"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v1

    :cond_2
    move-object v5, v1

    .line 1330894
    :goto_3
    :try_start_4
    iget-object v0, p0, LX/8Kt;->d:LX/1EZ;

    iget-object v0, v0, LX/1EZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8L4;

    iget-object v1, p0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330895
    iget-object v2, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v2, v2

    .line 1330896
    const/4 v10, 0x1

    .line 1330897
    iget-object v7, v0, LX/8L4;->f:LX/8Ko;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    .line 1330898
    :try_start_5
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1330899
    iget-object v6, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v6, v6

    .line 1330900
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1330901
    iget-object v6, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->ac:Ljava/lang/String;

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_10

    const/4 v6, 0x1

    :goto_4
    move v6, v6

    .line 1330902
    if-eqz v6, :cond_c
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3

    .line 1330903
    :goto_5
    :try_start_6
    iget-object v0, p0, LX/8Kt;->d:LX/1EZ;

    iget-object v0, v0, LX/1EZ;->A:LX/0ad;

    invoke-static {v0}, LX/8Nv;->a(LX/0ad;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1330904
    if-nez v3, :cond_4

    .line 1330905
    :cond_3
    :goto_6
    return-void

    .line 1330906
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 1330907
    iget-object v0, p0, LX/8Kt;->d:LX/1EZ;

    iget-object v0, v0, LX/1EZ;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "Upload success getResultDataParcelableNullOk (FBIDS)"

    invoke-virtual {v0, v3, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v3, v1

    goto :goto_0

    .line 1330908
    :catch_1
    move-exception v0

    move-object v2, v0

    .line 1330909
    iget-object v0, p0, LX/8Kt;->d:LX/1EZ;

    iget-object v0, v0, LX/1EZ;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v4, "Upload success getResultDataParcelableNullOk (GraphQLStory)"

    invoke-virtual {v0, v4, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v4, v1

    goto :goto_2

    .line 1330910
    :catch_2
    move-exception v0

    move-object v2, v0

    .line 1330911
    iget-object v0, p0, LX/8Kt;->d:LX/1EZ;

    iget-object v0, v0, LX/1EZ;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v5, "Upload success getResultDataParcelableNullOk (shotFbid)"

    invoke-virtual {v0, v5, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v5, v1

    goto :goto_3

    .line 1330912
    :cond_4
    iget-object v0, p0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->aa()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1330913
    iget-object v0, p0, LX/8Kt;->b:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330914
    iget-object v1, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v0, v1

    .line 1330915
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/VideoItem;

    .line 1330916
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1330917
    :goto_7
    iget-object v1, p0, LX/8Kt;->d:LX/1EZ;

    iget-object v2, p0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1330918
    iget-object v4, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v4, v4

    .line 1330919
    const/4 v6, 0x0

    .line 1330920
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1330921
    iget-object v5, v2, Lcom/facebook/photos/upload/operation/UploadOperation;->a:LX/0Px;

    move-object v9, v5

    .line 1330922
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v7, v6

    :goto_8
    if-ge v7, v10, :cond_6

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/ipc/media/MediaItem;

    .line 1330923
    instance-of v11, v5, Lcom/facebook/photos/base/media/VideoItem;

    if-eqz v11, :cond_5

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1330924
    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1330925
    :cond_5
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_8

    .line 1330926
    :cond_6
    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->ad()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1330927
    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1330928
    :cond_7
    iget-object v7, v1, LX/1EZ;->r:LX/1Ec;

    invoke-virtual {v2}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v5

    if-eqz v5, :cond_13

    .line 1330929
    iget-object v5, v2, Lcom/facebook/photos/upload/operation/UploadOperation;->am:Lcom/facebook/audience/model/UploadShot;

    move-object v5, v5

    .line 1330930
    if-nez v5, :cond_13

    const/4 v5, 0x1

    :goto_9
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    new-instance v8, LX/8Ku;

    invoke-direct {v8, v1, v2, v4}, LX/8Ku;-><init>(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    invoke-virtual {v7, v0, v5, v6, v8}, LX/1Ec;->a(Ljava/lang/String;ZLX/0Px;LX/8Ku;)LX/8Nm;

    move-result-object v5

    .line 1330931
    iget-object v6, v5, LX/8Nm;->d:LX/3ib;

    iget-object v7, v5, LX/8Nm;->h:LX/0Px;

    new-instance v8, LX/8Nk;

    invoke-direct {v8, v5}, LX/8Nk;-><init>(LX/8Nm;)V

    invoke-virtual {v6, v7, v8}, LX/3ib;->a(LX/0Px;LX/8Nj;)LX/8Np;

    move-result-object v6

    .line 1330932
    invoke-virtual {v6}, LX/8Np;->a()V

    .line 1330933
    :cond_8
    iget-object v0, p0, LX/8Kt;->d:LX/1EZ;

    const-string v1, "Upload success retry"

    invoke-virtual {v0, v1}, LX/1EZ;->c(Ljava/lang/String;)V

    .line 1330934
    iget-object v0, p0, LX/8Kt;->d:LX/1EZ;

    iget-object v1, p0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-static {v0, v1}, LX/1EZ;->j(LX/1EZ;Lcom/facebook/photos/upload/operation/UploadOperation;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_6

    .line 1330935
    :catch_3
    move-exception v0

    move-object v1, v0

    .line 1330936
    iget-object v0, p0, LX/8Kt;->d:LX/1EZ;

    iget-object v0, v0, LX/1EZ;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "UploadManager onSuccess throwable"

    invoke-virtual {v0, v2, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_6

    .line 1330937
    :cond_9
    :try_start_7
    iget-object v0, p0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, LX/8Kt;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    invoke-virtual {v0}, Lcom/facebook/photos/upload/operation/UploadOperation;->ad()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1330938
    :cond_a
    iget-object v0, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v0, v0
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    .line 1330939
    goto/16 :goto_7

    :cond_b
    move-object v0, v1

    goto/16 :goto_1

    .line 1330940
    :cond_c
    :try_start_8
    iget-object v6, v0, LX/8L4;->b:Landroid/content/Context;

    invoke-virtual {v7, v6, v1}, LX/8Ko;->b(Landroid/content/Context;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6, v1}, LX/8L4;->a$redex0(LX/8L4;Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;

    move-result-object v8

    .line 1330941
    iget-object v6, v0, LX/8L4;->b:Landroid/content/Context;

    invoke-virtual {v7, v6, v1}, LX/8Ko;->a(Landroid/content/Context;Lcom/facebook/photos/upload/operation/UploadOperation;)Ljava/lang/String;

    move-result-object v6

    .line 1330942
    invoke-static {v0, v1}, LX/8L4;->n(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)Z

    move-result v9

    if-eqz v9, :cond_d

    iget-object v9, v0, LX/8L4;->x:LX/7ma;

    invoke-virtual {v9}, LX/7ma;->c()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v9

    if-gt v9, v10, :cond_e

    .line 1330943
    :cond_d
    iget-object v6, v0, LX/8L4;->b:Landroid/content/Context;

    .line 1330944
    const v9, 0x7f082026

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v6}, LX/8Ko;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v6, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    move-object v6, v9

    .line 1330945
    :cond_e
    iget-boolean v9, v0, LX/8L4;->y:Z

    if-eqz v9, :cond_11

    .line 1330946
    sget-object v9, LX/8Ky;->a:[I

    invoke-static {v0, v1}, LX/8L4;->o(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)LX/8L2;

    move-result-object v10

    invoke-virtual {v10}, LX/8L2;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 1330947
    :goto_a
    :pswitch_0
    iget-object v9, v0, LX/8L4;->r:Landroid/app/PendingIntent;

    .line 1330948
    :goto_b
    move-object v9, v9

    .line 1330949
    invoke-static {v0, v1}, LX/8L4;->i(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1330950
    new-instance v10, LX/2HB;

    iget-object v11, v0, LX/8L4;->b:Landroid/content/Context;

    invoke-direct {v10, v11}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7}, LX/8Ko;->c()I

    move-result v7

    invoke-virtual {v10, v7}, LX/2HB;->a(I)LX/2HB;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v6

    .line 1330951
    iput-object v9, v6, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1330952
    move-object v6, v6

    .line 1330953
    invoke-virtual {v6}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v6

    .line 1330954
    invoke-static {v0, v1, v6}, LX/8L4;->a$redex0(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;Landroid/app/Notification;)V

    .line 1330955
    iget-boolean v6, v0, LX/8L4;->a:Z

    if-eqz v6, :cond_f
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    .line 1330956
    :cond_f
    :try_start_9
    iget-object v12, v0, LX/8L4;->d:LX/0b3;

    new-instance v6, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    invoke-virtual {v12, v6}, LX/0b4;->a(LX/0b7;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_3

    :try_start_a
    goto/16 :goto_5

    .line 1330957
    :catch_4
    move-exception v6
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_3

    .line 1330958
    :try_start_b
    iget-object v7, v0, LX/8L4;->j:LX/03V;

    const-string v8, "Upload Success broadcast throwable"

    invoke-virtual {v7, v8, v6}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_3

    :try_start_c
    goto/16 :goto_5

    .line 1330959
    :catch_5
    move-exception v6

    .line 1330960
    iget-object v7, v0, LX/8L4;->j:LX/03V;

    const-string v8, "Upload Success throwable"

    invoke-virtual {v7, v8, v6}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1330961
    invoke-static {v0, v1}, LX/8L4;->i(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto/16 :goto_5
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_3

    :cond_10
    :try_start_d
    const/4 v6, 0x0

    goto/16 :goto_4
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_3

    .line 1330962
    :pswitch_1
    :try_start_e
    invoke-static {v0}, LX/8L4;->a(LX/8L4;)Landroid/app/PendingIntent;

    move-result-object v9

    goto :goto_b

    .line 1330963
    :pswitch_2
    invoke-static {v0}, LX/8L4;->c(LX/8L4;)Landroid/app/PendingIntent;

    move-result-object v9

    goto :goto_b
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_3

    .line 1330964
    :pswitch_3
    :try_start_f
    invoke-static {v0, v1, v2}, LX/8L4;->d(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)Landroid/app/PendingIntent;
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_3

    :try_start_10
    move-result-object v9

    goto :goto_b

    .line 1330965
    :catch_6
    move-exception v9

    .line 1330966
    iget-object v10, v0, LX/8L4;->j:LX/03V;

    const-string v11, "Failed to build upload success intent"

    invoke-virtual {v10, v11, v9}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_a

    .line 1330967
    :cond_11
    invoke-virtual {v1}, Lcom/facebook/photos/upload/operation/UploadOperation;->ab()Z

    move-result v9

    if-nez v9, :cond_12
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_3

    .line 1330968
    :try_start_11
    invoke-static {v0, v1, v2}, LX/8L4;->d(LX/8L4;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)Landroid/app/PendingIntent;
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_3

    :try_start_12
    move-result-object v9

    goto :goto_b

    .line 1330969
    :catch_7
    move-exception v9

    .line 1330970
    iget-object v10, v0, LX/8L4;->j:LX/03V;

    const-string v11, "Failed to build upload success intent"

    invoke-virtual {v10, v11, v9}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1330971
    :cond_12
    iget-object v9, v0, LX/8L4;->r:Landroid/app/PendingIntent;

    goto/16 :goto_b
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_3

    :cond_13
    move v5, v6

    .line 1330972
    goto/16 :goto_9

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
