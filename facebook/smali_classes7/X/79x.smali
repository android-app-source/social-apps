.class public LX/79x;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/03V;

.field public final c:LX/17W;

.field private final d:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1174482
    const-class v0, LX/79x;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/79x;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/17W;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1174483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1174484
    iput-object p1, p0, LX/79x;->b:LX/03V;

    .line 1174485
    iput-object p2, p0, LX/79x;->c:LX/17W;

    .line 1174486
    iput-object p3, p0, LX/79x;->d:LX/0Uh;

    .line 1174487
    return-void
.end method

.method public static b(LX/0QB;)LX/79x;
    .locals 4

    .prologue
    .line 1174488
    new-instance v3, LX/79x;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v1

    check-cast v1, LX/17W;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-direct {v3, v0, v1, v2}, LX/79x;-><init>(LX/03V;LX/17W;LX/0Uh;)V

    .line 1174489
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;)V
    .locals 4
    .param p2    # Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1174490
    iget-object v0, p0, LX/79x;->d:LX/0Uh;

    const/16 v1, 0x21c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1174491
    iget-object v0, p0, LX/79x;->c:LX/17W;

    sget-object v1, LX/0ax;->eP:Ljava/lang/String;

    .line 1174492
    if-eqz p2, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    if-ne p2, v2, :cond_4

    .line 1174493
    :cond_0
    iget-object v2, p0, LX/79x;->b:LX/03V;

    sget-object v3, LX/79x;->a:Ljava/lang/String;

    const-string p3, "Not enough information to launch native saved dashboard"

    invoke-virtual {v2, v3, p3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1174494
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    .line 1174495
    :goto_0
    move-object v2, v2

    .line 1174496
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ALL:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    invoke-virtual {v2, v3}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2, p4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1174497
    :goto_1
    return-void

    .line 1174498
    :cond_1
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string p3, "https://m.facebook.com/saved"

    .line 1174499
    :cond_2
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1174500
    iget-object v0, p0, LX/79x;->b:LX/03V;

    sget-object v1, LX/79x;->a:Ljava/lang/String;

    const-string v2, "Not enough information to launch Faceweb saved dashboard"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1174501
    :cond_3
    :goto_2
    goto :goto_1

    :cond_4
    invoke-static {p2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    goto :goto_0

    .line 1174502
    :cond_5
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1174503
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "cref_name"

    invoke-virtual {p4}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1174504
    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_3
    move-object v0, v0

    .line 1174505
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1174506
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1174507
    const-string v2, "titlebar_with_modal_done"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1174508
    iget-object v2, p0, LX/79x;->c:LX/17W;

    invoke-virtual {v2, p1, v0, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_2

    :cond_6
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method
