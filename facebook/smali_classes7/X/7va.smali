.class public LX/7va;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1274814
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274815
    iput-object p1, p0, LX/7va;->a:LX/0tX;

    .line 1274816
    return-void
.end method

.method public static a(Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/4EL;
    .locals 3

    .prologue
    .line 1274817
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    .line 1274818
    invoke-virtual {v0, p1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1274819
    if-eqz p2, :cond_0

    .line 1274820
    invoke-virtual {v0, p2}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1274821
    :cond_0
    new-instance v1, LX/4EG;

    invoke-direct {v1}, LX/4EG;-><init>()V

    .line 1274822
    invoke-virtual {v1, p3}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1274823
    invoke-virtual {p0}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1274824
    if-eqz p4, :cond_1

    .line 1274825
    invoke-virtual {v1, p4}, LX/4EG;->c(Ljava/lang/String;)LX/4EG;

    .line 1274826
    :cond_1
    new-instance v2, LX/4EL;

    invoke-direct {v2}, LX/4EL;-><init>()V

    .line 1274827
    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 1274828
    return-object v2
.end method

.method public static b(LX/0QB;)LX/7va;
    .locals 2

    .prologue
    .line 1274829
    new-instance v1, LX/7va;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-direct {v1, v0}, LX/7va;-><init>(LX/0tX;)V

    .line 1274830
    return-object v1
.end method
