.class public LX/7Tx;
.super LX/4or;
.source ""


# instance fields
.field private final a:LX/1HI;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1HI;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1211190
    invoke-direct {p0, p1}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 1211191
    iput-object p2, p0, LX/7Tx;->a:LX/1HI;

    .line 1211192
    invoke-virtual {p0, v2}, LX/7Tx;->setPersistent(Z)V

    .line 1211193
    const-string v0, "clearimagecache"

    invoke-virtual {p0, v0}, LX/7Tx;->setKey(Ljava/lang/String;)V

    .line 1211194
    const-string v0, "Clear image cache"

    invoke-virtual {p0, v0}, LX/7Tx;->setTitle(Ljava/lang/CharSequence;)V

    .line 1211195
    const-string v0, "Clears the image cache in memory and on disk"

    invoke-virtual {p0, v0}, LX/7Tx;->setSummary(Ljava/lang/CharSequence;)V

    .line 1211196
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Clear image cache"

    aput-object v1, v0, v2

    .line 1211197
    invoke-virtual {p0, v0}, LX/7Tx;->setEntries([Ljava/lang/CharSequence;)V

    .line 1211198
    invoke-virtual {p0, v0}, LX/7Tx;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 1211199
    return-void
.end method

.method public static a(LX/0QB;)LX/7Tx;
    .locals 3

    .prologue
    .line 1211200
    new-instance v2, LX/7Tx;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v1

    check-cast v1, LX/1HI;

    invoke-direct {v2, v0, v1}, LX/7Tx;-><init>(Landroid/content/Context;LX/1HI;)V

    .line 1211201
    move-object v0, v2

    .line 1211202
    return-object v0
.end method


# virtual methods
.method public final getPersistedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1211203
    const-string v0, ""

    return-object v0
.end method

.method public final persistString(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1211204
    iget-object v0, p0, LX/7Tx;->a:LX/1HI;

    invoke-virtual {v0}, LX/1HI;->b()V

    .line 1211205
    const/4 v0, 0x1

    return v0
.end method
