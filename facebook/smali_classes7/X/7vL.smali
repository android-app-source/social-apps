.class public LX/7vL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:D

.field public final b:D


# direct methods
.method public constructor <init>(DD)V
    .locals 1

    .prologue
    .line 1274470
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274471
    iput-wide p1, p0, LX/7vL;->a:D

    .line 1274472
    iput-wide p3, p0, LX/7vL;->b:D

    .line 1274473
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1274474
    if-ne p0, p1, :cond_1

    .line 1274475
    :cond_0
    :goto_0
    return v0

    .line 1274476
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1274477
    :cond_3
    check-cast p1, LX/7vL;

    .line 1274478
    iget-wide v2, p1, LX/7vL;->a:D

    iget-wide v4, p0, LX/7vL;->a:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    .line 1274479
    :cond_4
    iget-wide v2, p1, LX/7vL;->b:D

    iget-wide v4, p0, LX/7vL;->b:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    const/16 v8, 0x20

    const-wide/16 v6, 0x0

    .line 1274480
    iget-wide v0, p0, LX/7vL;->a:D

    cmpl-double v0, v0, v6

    if-eqz v0, :cond_1

    iget-wide v0, p0, LX/7vL;->a:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    .line 1274481
    :goto_0
    ushr-long v4, v0, v8

    xor-long/2addr v0, v4

    long-to-int v0, v0

    .line 1274482
    iget-wide v4, p0, LX/7vL;->b:D

    cmpl-double v1, v4, v6

    if-eqz v1, :cond_0

    iget-wide v2, p0, LX/7vL;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 1274483
    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    ushr-long v4, v2, v8

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 1274484
    return v0

    :cond_1
    move-wide v0, v2

    .line 1274485
    goto :goto_0
.end method
