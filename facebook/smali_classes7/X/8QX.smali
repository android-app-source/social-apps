.class public LX/8QX;
.super LX/8QM;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;III)V
    .locals 7

    .prologue
    .line 1343280
    sget-object v1, LX/8vA;->SPECIFIC_FRIENDS:LX/8vA;

    const/4 v6, 0x0

    move-object v0, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, LX/8QM;-><init>(LX/8vA;IIILjava/lang/String;Ljava/lang/String;)V

    .line 1343281
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1343282
    if-ne p1, p0, :cond_1

    .line 1343283
    :cond_0
    :goto_0
    return v0

    .line 1343284
    :cond_1
    instance-of v2, p1, LX/8QX;

    if-nez v2, :cond_2

    move v0, v1

    .line 1343285
    goto :goto_0

    .line 1343286
    :cond_2
    check-cast p1, LX/8QX;

    .line 1343287
    iget-object v2, p0, LX/8QM;->h:Ljava/lang/String;

    invoke-virtual {p1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, LX/8QM;->e:I

    invoke-virtual {p1}, LX/8QL;->d()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget v2, p0, LX/8QM;->f:I

    invoke-virtual {p1}, LX/8QL;->e()I

    move-result v3

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1343288
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/8QM;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, LX/8QM;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, LX/8QM;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
