.class public LX/8YV;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1Ha;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1356296
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1Ha;
    .locals 7

    .prologue
    .line 1356297
    const-class v1, LX/8YV;

    monitor-enter v1

    .line 1356298
    :try_start_0
    sget-object v0, LX/8YV;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1356299
    sput-object v2, LX/8YV;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1356300
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1356301
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1356302
    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1GP;->a(LX/0QB;)LX/1GP;

    move-result-object v4

    check-cast v4, LX/1GQ;

    invoke-static {v0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v5

    check-cast v5, LX/0pi;

    invoke-static {v0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v6

    check-cast v6, LX/0pq;

    invoke-static {v0}, LX/1Fs;->a(LX/0QB;)LX/1Fs;

    move-result-object p0

    check-cast p0, LX/1Ft;

    invoke-static {v3, v4, v5, v6, p0}, LX/8Yj;->a(Landroid/content/Context;LX/1GQ;LX/0pi;LX/0pq;LX/1Ft;)LX/1Ha;

    move-result-object v3

    move-object v0, v3

    .line 1356303
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1356304
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1Ha;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1356305
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1356306
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1356307
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/1GP;->a(LX/0QB;)LX/1GP;

    move-result-object v1

    check-cast v1, LX/1GQ;

    invoke-static {p0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v2

    check-cast v2, LX/0pi;

    invoke-static {p0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v3

    check-cast v3, LX/0pq;

    invoke-static {p0}, LX/1Fs;->a(LX/0QB;)LX/1Fs;

    move-result-object v4

    check-cast v4, LX/1Ft;

    invoke-static {v0, v1, v2, v3, v4}, LX/8Yj;->a(Landroid/content/Context;LX/1GQ;LX/0pi;LX/0pq;LX/1Ft;)LX/1Ha;

    move-result-object v0

    return-object v0
.end method
