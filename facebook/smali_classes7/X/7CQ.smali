.class public final enum LX/7CQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7CQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7CQ;

.field public static final enum ACTIVITY_LOG_LAUNCHED:LX/7CQ;

.field public static final enum BACK_BUTTON_PRESSED:LX/7CQ;

.field public static final enum FETCH_STATE_CHANGED:LX/7CQ;

.field public static final enum FRAGMENT_PAUSED:LX/7CQ;

.field public static final enum FRAGMENT_RESUMED:LX/7CQ;

.field public static final enum NEW_TYPEAHEAD_TEXT_TYPED:LX/7CQ;

.field public static final enum RESULTS_LOAD_FAILURE:LX/7CQ;

.field public static final enum SERP_TAB_CLICKED:LX/7CQ;

.field public static final enum SOFT_ERROR:LX/7CQ;

.field public static final enum TYPEAHEAD_UNITS_LOADED:LX/7CQ;

.field public static final enum TYPEAHEAD_UNIT_CLICKED:LX/7CQ;

.field public static final enum TYPEAHEAD_UNIT_LONG_CLICKED:LX/7CQ;

.field public static final enum UP_BUTTON_PRESSED:LX/7CQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1180524
    new-instance v0, LX/7CQ;

    const-string v1, "FRAGMENT_RESUMED"

    invoke-direct {v0, v1, v3}, LX/7CQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7CQ;->FRAGMENT_RESUMED:LX/7CQ;

    .line 1180525
    new-instance v0, LX/7CQ;

    const-string v1, "TYPEAHEAD_UNIT_CLICKED"

    invoke-direct {v0, v1, v4}, LX/7CQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7CQ;->TYPEAHEAD_UNIT_CLICKED:LX/7CQ;

    .line 1180526
    new-instance v0, LX/7CQ;

    const-string v1, "TYPEAHEAD_UNIT_LONG_CLICKED"

    invoke-direct {v0, v1, v5}, LX/7CQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7CQ;->TYPEAHEAD_UNIT_LONG_CLICKED:LX/7CQ;

    .line 1180527
    new-instance v0, LX/7CQ;

    const-string v1, "ACTIVITY_LOG_LAUNCHED"

    invoke-direct {v0, v1, v6}, LX/7CQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7CQ;->ACTIVITY_LOG_LAUNCHED:LX/7CQ;

    .line 1180528
    new-instance v0, LX/7CQ;

    const-string v1, "NEW_TYPEAHEAD_TEXT_TYPED"

    invoke-direct {v0, v1, v7}, LX/7CQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7CQ;->NEW_TYPEAHEAD_TEXT_TYPED:LX/7CQ;

    .line 1180529
    new-instance v0, LX/7CQ;

    const-string v1, "FETCH_STATE_CHANGED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7CQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7CQ;->FETCH_STATE_CHANGED:LX/7CQ;

    .line 1180530
    new-instance v0, LX/7CQ;

    const-string v1, "BACK_BUTTON_PRESSED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/7CQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7CQ;->BACK_BUTTON_PRESSED:LX/7CQ;

    .line 1180531
    new-instance v0, LX/7CQ;

    const-string v1, "UP_BUTTON_PRESSED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/7CQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7CQ;->UP_BUTTON_PRESSED:LX/7CQ;

    .line 1180532
    new-instance v0, LX/7CQ;

    const-string v1, "TYPEAHEAD_UNITS_LOADED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/7CQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7CQ;->TYPEAHEAD_UNITS_LOADED:LX/7CQ;

    .line 1180533
    new-instance v0, LX/7CQ;

    const-string v1, "SERP_TAB_CLICKED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/7CQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7CQ;->SERP_TAB_CLICKED:LX/7CQ;

    .line 1180534
    new-instance v0, LX/7CQ;

    const-string v1, "RESULTS_LOAD_FAILURE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/7CQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7CQ;->RESULTS_LOAD_FAILURE:LX/7CQ;

    .line 1180535
    new-instance v0, LX/7CQ;

    const-string v1, "FRAGMENT_PAUSED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/7CQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7CQ;->FRAGMENT_PAUSED:LX/7CQ;

    .line 1180536
    new-instance v0, LX/7CQ;

    const-string v1, "SOFT_ERROR"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/7CQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7CQ;->SOFT_ERROR:LX/7CQ;

    .line 1180537
    const/16 v0, 0xd

    new-array v0, v0, [LX/7CQ;

    sget-object v1, LX/7CQ;->FRAGMENT_RESUMED:LX/7CQ;

    aput-object v1, v0, v3

    sget-object v1, LX/7CQ;->TYPEAHEAD_UNIT_CLICKED:LX/7CQ;

    aput-object v1, v0, v4

    sget-object v1, LX/7CQ;->TYPEAHEAD_UNIT_LONG_CLICKED:LX/7CQ;

    aput-object v1, v0, v5

    sget-object v1, LX/7CQ;->ACTIVITY_LOG_LAUNCHED:LX/7CQ;

    aput-object v1, v0, v6

    sget-object v1, LX/7CQ;->NEW_TYPEAHEAD_TEXT_TYPED:LX/7CQ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7CQ;->FETCH_STATE_CHANGED:LX/7CQ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7CQ;->BACK_BUTTON_PRESSED:LX/7CQ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7CQ;->UP_BUTTON_PRESSED:LX/7CQ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7CQ;->TYPEAHEAD_UNITS_LOADED:LX/7CQ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7CQ;->SERP_TAB_CLICKED:LX/7CQ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7CQ;->RESULTS_LOAD_FAILURE:LX/7CQ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7CQ;->FRAGMENT_PAUSED:LX/7CQ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/7CQ;->SOFT_ERROR:LX/7CQ;

    aput-object v2, v0, v1

    sput-object v0, LX/7CQ;->$VALUES:[LX/7CQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1180538
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7CQ;
    .locals 1

    .prologue
    .line 1180539
    const-class v0, LX/7CQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7CQ;

    return-object v0
.end method

.method public static values()[LX/7CQ;
    .locals 1

    .prologue
    .line 1180540
    sget-object v0, LX/7CQ;->$VALUES:[LX/7CQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7CQ;

    return-object v0
.end method
