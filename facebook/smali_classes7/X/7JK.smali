.class public final LX/7JK;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/37Y;


# direct methods
.method public constructor <init>(LX/37Y;)V
    .locals 0

    .prologue
    .line 1193409
    iput-object p1, p0, LX/7JK;->a:LX/37Y;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1193410
    const/4 v1, 0x0

    .line 1193411
    move v0, v1

    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_2

    .line 1193412
    :try_start_0
    invoke-virtual {p0}, LX/7JK;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1193413
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1193414
    :goto_1
    return-object v0

    .line 1193415
    :cond_0
    iget-object v2, p0, LX/7JK;->a:LX/37Y;

    iget-boolean v2, v2, LX/37Y;->j:Z

    if-nez v2, :cond_1

    .line 1193416
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/7JK;->cancel(Z)Z

    .line 1193417
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 1193418
    :cond_1
    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1193419
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1193420
    :cond_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    :catch_0
    goto :goto_2
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1193421
    iget-object v0, p0, LX/7JK;->a:LX/37Y;

    invoke-virtual {v0}, LX/37Y;->g()LX/38p;

    move-result-object v0

    invoke-virtual {v0}, LX/38p;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1193422
    iget-object v0, p0, LX/7JK;->a:LX/37Y;

    iget-object v0, v0, LX/37Y;->g:LX/37a;

    invoke-virtual {v0}, LX/37a;->a()V

    .line 1193423
    :cond_0
    iget-object v0, p0, LX/7JK;->a:LX/37Y;

    const/4 v1, 0x0

    .line 1193424
    iput-boolean v1, v0, LX/37Y;->j:Z

    .line 1193425
    return-void
.end method
