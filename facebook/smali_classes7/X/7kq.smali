.class public LX/7kq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1232552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232553
    return-void
.end method

.method public static a(ILcom/facebook/composer/attachments/ComposerAttachment;LX/0Px;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232543
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p2}, LX/7kq;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1232544
    invoke-interface {v0, p0, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1232545
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Px;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232546
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1232547
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1232548
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1232549
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1232550
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1232551
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Px;LX/0Px;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232554
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1232555
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1232556
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1232557
    invoke-static {v0, p1}, LX/7kq;->a(Lcom/facebook/composer/attachments/ComposerAttachment;Ljava/lang/Iterable;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    .line 1232558
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1232559
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1232560
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Px;Lcom/facebook/composer/attachments/ComposerAttachment;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232561
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p0}, LX/7kq;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1232562
    invoke-static {p1, v0}, LX/7kq;->a(Lcom/facebook/composer/attachments/ComposerAttachment;Ljava/lang/Iterable;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v1

    .line 1232563
    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1232564
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0Px;LX/2rb;)Landroid/location/Location;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;",
            "LX/2rb;",
            ")",
            "Landroid/location/Location;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1232565
    const/4 v0, 0x0

    .line 1232566
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {p0}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1232567
    sget-object v2, Lcom/facebook/ipc/media/MediaItem;->b:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1232568
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1232569
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1232570
    const/4 v5, 0x0

    .line 1232571
    const/4 v4, 0x2

    new-array v6, v4, [F

    .line 1232572
    :try_start_0
    new-instance v4, Landroid/media/ExifInterface;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 1232573
    invoke-virtual {v4, v6}, Landroid/media/ExifInterface;->getLatLong([F)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1232574
    new-instance v4, Landroid/location/Location;

    const-string v7, "photo-exif"

    invoke-direct {v4, v7}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 1232575
    const/4 v7, 0x0

    aget v7, v6, v7

    float-to-double v8, v7

    invoke-virtual {v4, v8, v9}, Landroid/location/Location;->setLatitude(D)V

    .line 1232576
    const/4 v7, 0x1

    aget v6, v6, v7

    float-to-double v6, v6

    invoke-virtual {v4, v6, v7}, Landroid/location/Location;->setLongitude(D)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1232577
    :goto_1
    move-object v1, v4

    .line 1232578
    if-eqz v1, :cond_0

    .line 1232579
    invoke-virtual {p1, v0}, LX/2rb;->a(Lcom/facebook/ipc/media/MediaItem;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setTime(J)V

    .line 1232580
    :goto_2
    return-object v1

    :cond_0
    move-object v0, v1

    .line 1232581
    goto :goto_0

    :cond_1
    move-object v1, v0

    goto :goto_2

    :cond_2
    move-object v4, v5

    .line 1232582
    goto :goto_1

    .line 1232583
    :catch_0
    move-object v4, v5

    goto :goto_1
.end method

.method private static a(Lcom/facebook/composer/attachments/ComposerAttachment;Ljava/lang/Iterable;)Lcom/facebook/composer/attachments/ComposerAttachment;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "Lcom/facebook/composer/attachments/ComposerAttachment;"
        }
    .end annotation

    .prologue
    .line 1232584
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    .line 1232585
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1232586
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1232587
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1232588
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v3

    .line 1232589
    iget-object v4, v3, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    move-object v3, v4

    .line 1232590
    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v4

    .line 1232591
    iget-object p1, v4, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    move-object v4, p1

    .line 1232592
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1232593
    :goto_0
    return-object v0

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/composer/attachments/ComposerAttachment;)Z
    .locals 2

    .prologue
    .line 1232594
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    sget-object v1, LX/4gF;->VIDEO:LX/4gF;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0Px;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/media/PhotoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232595
    const-class v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1232596
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1232597
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1232598
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1232599
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    .line 1232600
    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1232601
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1232602
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1232603
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 1232604
    return-object v0
.end method

.method public static b(Lcom/facebook/composer/attachments/ComposerAttachment;)Z
    .locals 2

    .prologue
    .line 1232605
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    sget-object v1, LX/4gF;->PHOTO:LX/4gF;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/0Px;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232606
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1232607
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1232608
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1232609
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v4

    .line 1232610
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v4

    sget-object v5, LX/4gF;->PHOTO:LX/4gF;

    if-ne v4, v5, :cond_0

    .line 1232611
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1232612
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1232613
    :cond_1
    return-object v2
.end method

.method public static c(Lcom/facebook/composer/attachments/ComposerAttachment;)Z
    .locals 2

    .prologue
    .line 1232614
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1232615
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, v1

    .line 1232616
    sget-object v1, LX/4gQ;->Video:LX/4gQ;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/facebook/ipc/media/data/MimeType;->d:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1232617
    iget-object p0, v1, Lcom/facebook/ipc/media/data/MediaData;->mMimeType:Lcom/facebook/ipc/media/data/MimeType;

    move-object v1, p0

    .line 1232618
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/media/data/MimeType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/0Px;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1232619
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1232620
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->p()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 1232621
    :goto_1
    return v0

    .line 1232622
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1232623
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static d(Lcom/facebook/composer/attachments/ComposerAttachment;)Z
    .locals 2

    .prologue
    .line 1232536
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    sget-object v1, LX/4gF;->PHOTO:LX/4gF;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/0Px;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232537
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1232538
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1232539
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1232540
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1232541
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1232542
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static f(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "Lcom/facebook/composer/attachments/ComposerAttachment;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1232476
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1232477
    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    goto :goto_0
.end method

.method public static g(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "Lcom/facebook/composer/attachments/ComposerAttachment;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1232478
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1232479
    invoke-static {v0}, LX/7kq;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1232480
    :goto_1
    return-object v0

    .line 1232481
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1232482
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static h(LX/0Px;)Landroid/net/Uri;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "Landroid/net/Uri;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1232483
    invoke-static {p0}, LX/7kq;->g(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    .line 1232484
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1232485
    :cond_0
    const/4 v0, 0x0

    .line 1232486
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static i(LX/0Px;)Landroid/os/Bundle;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 1232487
    invoke-static {p0}, LX/7kq;->g(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    .line 1232488
    if-nez v0, :cond_0

    .line 1232489
    const/4 v0, 0x0

    .line 1232490
    :goto_0
    return-object v0

    .line 1232491
    :cond_0
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->u(LX/0Px;)LX/0Px;

    move-result-object v0

    .line 1232492
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    goto :goto_0
.end method

.method public static j(LX/0Px;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1232493
    invoke-static {p0}, LX/7kq;->g(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(LX/0Px;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1232494
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1232495
    invoke-static {v0}, LX/7kq;->c(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1232496
    const/4 v0, 0x1

    .line 1232497
    :goto_1
    return v0

    .line 1232498
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1232499
    goto :goto_1
.end method

.method public static l(LX/0Px;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1232500
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1232501
    invoke-static {v0}, LX/7kq;->b(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1232502
    const/4 v0, 0x1

    .line 1232503
    :goto_1
    return v0

    .line 1232504
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1232505
    goto :goto_1
.end method

.method public static m(LX/0Px;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1232506
    invoke-static {p0}, LX/7kq;->j(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o(LX/0Px;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1232507
    invoke-static {p0}, LX/7kq;->j(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/7kq;->l(LX/0Px;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static p(LX/0Px;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1232508
    invoke-static {p0}, LX/7kq;->l(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/7kq;->j(LX/0Px;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static q(LX/0Px;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1232509
    invoke-static {p0}, LX/7kq;->j(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r(LX/0Px;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1232510
    invoke-static {p0}, LX/7kq;->k(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(LX/0Px;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232531
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1232532
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1232533
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1232534
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1232535
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static u(LX/0Px;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "LX/0Px",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1232511
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1232512
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1232513
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1232514
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v1}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v1

    .line 1232515
    const-string v6, "caption"

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {v5, v6, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232516
    const-string v1, "creative_editing_metadata"

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1232517
    const-string v1, "video_creative_editing_metadata"

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1232518
    const-string v1, "video_upload_quality"

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232519
    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1232520
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1232521
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static w(LX/0Px;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1232522
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    if-eq v0, v3, :cond_0

    move v0, v2

    .line 1232523
    :goto_0
    return v0

    .line 1232524
    :cond_0
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v5

    move v4, v2

    move v1, v2

    :goto_1
    if-ge v4, v5, :cond_1

    invoke-virtual {p0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1232525
    invoke-static {v0}, LX/7kq;->d(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v6

    instance-of v6, v6, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v6, :cond_3

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1232526
    iget-boolean v6, v0, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    move v0, v6

    .line 1232527
    if-eqz v0, :cond_3

    .line 1232528
    add-int/lit8 v0, v1, 0x1

    .line 1232529
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_1

    .line 1232530
    :cond_1
    if-lez v1, :cond_2

    move v0, v3

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method
