.class public final enum LX/7C8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7C8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7C8;

.field public static final enum VIDEOS:LX/7C8;


# instance fields
.field private final mFilterName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1179867
    new-instance v0, LX/7C8;

    const-string v1, "VIDEOS"

    const-string v2, "videosearch"

    invoke-direct {v0, v1, v3, v2}, LX/7C8;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7C8;->VIDEOS:LX/7C8;

    .line 1179868
    const/4 v0, 0x1

    new-array v0, v0, [LX/7C8;

    sget-object v1, LX/7C8;->VIDEOS:LX/7C8;

    aput-object v1, v0, v3

    sput-object v0, LX/7C8;->$VALUES:[LX/7C8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1179869
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1179870
    iput-object p3, p0, LX/7C8;->mFilterName:Ljava/lang/String;

    .line 1179871
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7C8;
    .locals 1

    .prologue
    .line 1179872
    const-class v0, LX/7C8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7C8;

    return-object v0
.end method

.method public static values()[LX/7C8;
    .locals 1

    .prologue
    .line 1179873
    sget-object v0, LX/7C8;->$VALUES:[LX/7C8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7C8;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1179874
    iget-object v0, p0, LX/7C8;->mFilterName:Ljava/lang/String;

    return-object v0
.end method
