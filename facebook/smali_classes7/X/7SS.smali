.class public LX/7SS;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# instance fields
.field public final c:LX/7SR;

.field public final d:Lcom/facebook/common/callercontext/CallerContext;

.field private final e:Z

.field public final f:Ljava/util/concurrent/ExecutorService;

.field public final g:LX/1HI;

.field public final h:Ljava/util/concurrent/locks/Lock;

.field private i:LX/5Pf;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field public j:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1208855
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "uBirthAnimationTexture"

    aput-object v1, v0, v2

    const-string v1, "uBirthAnimationNumberOfSprites"

    aput-object v1, v0, v3

    const-string v1, "uBirthAnimationPlays"

    aput-object v1, v0, v4

    const-string v1, "uBirthAnimationFramerate"

    aput-object v1, v0, v5

    const-string v1, "uBirthAnimationSpriteSize"

    aput-object v1, v0, v6

    sput-object v0, LX/7SS;->a:[Ljava/lang/String;

    .line 1208856
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "uDeathAnimationTexture"

    aput-object v1, v0, v2

    const-string v1, "uDeathAnimationNumberOfSprites"

    aput-object v1, v0, v3

    const-string v1, "uDeathAnimationPlays"

    aput-object v1, v0, v4

    const-string v1, "uDeathAnimationFramerate"

    aput-object v1, v0, v5

    const-string v1, "uDeathAnimationSpriteSize"

    aput-object v1, v0, v6

    sput-object v0, LX/7SS;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/7SR;Lcom/facebook/common/callercontext/CallerContext;ZLjava/util/concurrent/ExecutorService;LX/1HI;)V
    .locals 1
    .param p1    # LX/7SR;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1208847
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1208848
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, LX/7SS;->h:Ljava/util/concurrent/locks/Lock;

    .line 1208849
    iput-boolean p3, p0, LX/7SS;->e:Z

    .line 1208850
    iput-object p2, p0, LX/7SS;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1208851
    iput-object p1, p0, LX/7SS;->c:LX/7SR;

    .line 1208852
    iput-object p4, p0, LX/7SS;->f:Ljava/util/concurrent/ExecutorService;

    .line 1208853
    iput-object p5, p0, LX/7SS;->g:LX/1HI;

    .line 1208854
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1208845
    iget-object v0, p0, LX/7SS;->f:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/videocodec/effects/renderers/animatedsprites/SpriteAnimationTexture$1;

    invoke-direct {v1, p0}, Lcom/facebook/videocodec/effects/renderers/animatedsprites/SpriteAnimationTexture$1;-><init>(LX/7SS;)V

    const v2, -0x677c2c1f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1208846
    return-void
.end method

.method public final a(LX/5Pa;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1208827
    iget-object v0, p0, LX/7SS;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1208828
    :goto_0
    return-void

    .line 1208829
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/7SS;->i:LX/5Pf;

    if-nez v0, :cond_3

    .line 1208830
    iget-object v0, p0, LX/7SS;->j:LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 1208831
    iget-object v0, p0, LX/7SS;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 1208832
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/7SS;->j:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1208833
    if-eqz v0, :cond_2

    .line 1208834
    new-instance v1, LX/5Pe;

    invoke-direct {v1}, LX/5Pe;-><init>()V

    .line 1208835
    iput-object v0, v1, LX/5Pe;->c:Landroid/graphics/Bitmap;

    .line 1208836
    move-object v0, v1

    .line 1208837
    invoke-virtual {v0}, LX/5Pe;->a()LX/5Pf;

    move-result-object v0

    iput-object v0, p0, LX/7SS;->i:LX/5Pf;

    .line 1208838
    :cond_2
    iget-object v0, p0, LX/7SS;->j:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1208839
    const/4 v0, 0x0

    iput-object v0, p0, LX/7SS;->j:LX/1FJ;

    .line 1208840
    :cond_3
    iget-boolean v0, p0, LX/7SS;->e:Z

    if-eqz v0, :cond_4

    sget-object v0, LX/7SS;->a:[Ljava/lang/String;

    .line 1208841
    :goto_1
    const/4 v1, 0x0

    aget-object v1, v0, v1

    iget-object v2, p0, LX/7SS;->i:LX/5Pf;

    invoke-virtual {p1, v1, v2}, LX/5Pa;->a(Ljava/lang/String;LX/5Pf;)LX/5Pa;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v2, v0, v2

    iget-object v3, p0, LX/7SS;->c:LX/7SR;

    iget v3, v3, LX/7SR;->f:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, LX/5Pa;->a(Ljava/lang/String;F)LX/5Pa;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v2, v0, v2

    iget-object v3, p0, LX/7SS;->c:LX/7SR;

    iget v3, v3, LX/7SR;->e:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, LX/5Pa;->a(Ljava/lang/String;F)LX/5Pa;

    move-result-object v1

    const/4 v2, 0x3

    aget-object v2, v0, v2

    iget-object v3, p0, LX/7SS;->c:LX/7SR;

    iget v3, v3, LX/7SR;->b:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, LX/5Pa;->a(Ljava/lang/String;F)LX/5Pa;

    move-result-object v1

    const/4 v2, 0x4

    aget-object v0, v0, v2

    iget-object v2, p0, LX/7SS;->c:LX/7SR;

    iget v2, v2, LX/7SR;->d:I

    int-to-float v2, v2

    div-float v2, v4, v2

    iget-object v3, p0, LX/7SS;->c:LX/7SR;

    iget v3, v3, LX/7SR;->c:I

    int-to-float v3, v3

    div-float v3, v4, v3

    invoke-virtual {v1, v0, v2, v3}, LX/5Pa;->a(Ljava/lang/String;FF)LX/5Pa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1208842
    iget-object v0, p0, LX/7SS;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 1208843
    :cond_4
    :try_start_2
    sget-object v0, LX/7SS;->b:[Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1208844
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/7SS;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1208817
    iget-object v0, p0, LX/7SS;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1208818
    :try_start_0
    iget-object v0, p0, LX/7SS;->i:LX/5Pf;

    if-eqz v0, :cond_0

    .line 1208819
    iget-object v0, p0, LX/7SS;->i:LX/5Pf;

    invoke-virtual {v0}, LX/5Pf;->a()V

    .line 1208820
    const/4 v0, 0x0

    iput-object v0, p0, LX/7SS;->i:LX/5Pf;

    .line 1208821
    :cond_0
    iget-object v0, p0, LX/7SS;->j:LX/1FJ;

    if-eqz v0, :cond_1

    .line 1208822
    iget-object v0, p0, LX/7SS;->j:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1208823
    const/4 v0, 0x0

    iput-object v0, p0, LX/7SS;->j:LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1208824
    :cond_1
    iget-object v0, p0, LX/7SS;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1208825
    return-void

    .line 1208826
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/7SS;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
