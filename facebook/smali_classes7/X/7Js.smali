.class public LX/7Js;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/36s;

.field private final c:LX/0Sh;

.field private final d:LX/1m0;

.field private final e:Landroid/net/Uri;

.field private final f:LX/0So;

.field private final g:LX/3FK;

.field private final h:LX/7Q0;

.field private final i:LX/0TD;

.field private j:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private k:Z

.field private l:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1194206
    const-class v0, LX/7Js;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Js;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/36s;Landroid/net/Uri;LX/0Sh;LX/1m0;LX/0So;LX/0ad;LX/0Uh;LX/7Q0;LX/0TD;)V
    .locals 2
    .param p1    # LX/36s;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/VideoPerformanceExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1194207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194208
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/7Js;->j:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1194209
    iput-boolean v1, p0, LX/7Js;->k:Z

    .line 1194210
    iput-object p1, p0, LX/7Js;->b:LX/36s;

    .line 1194211
    iput-object p2, p0, LX/7Js;->e:Landroid/net/Uri;

    .line 1194212
    iput-object p3, p0, LX/7Js;->c:LX/0Sh;

    .line 1194213
    iput-object p4, p0, LX/7Js;->d:LX/1m0;

    .line 1194214
    iput-object p5, p0, LX/7Js;->f:LX/0So;

    .line 1194215
    new-instance v0, LX/3FK;

    invoke-direct {v0, p6, p7}, LX/3FK;-><init>(LX/0ad;LX/0Uh;)V

    iput-object v0, p0, LX/7Js;->g:LX/3FK;

    .line 1194216
    iput-object p8, p0, LX/7Js;->h:LX/7Q0;

    .line 1194217
    iput-object p9, p0, LX/7Js;->i:LX/0TD;

    .line 1194218
    iput v1, p0, LX/7Js;->l:I

    .line 1194219
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1194220
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7Js;->k:Z

    .line 1194221
    return-void
.end method
