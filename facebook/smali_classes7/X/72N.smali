.class public final LX/72N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6u8;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;)V
    .locals 0

    .prologue
    .line 1164264
    iput-object p1, p0, LX/72N;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/common/locale/Country;)V
    .locals 3

    .prologue
    .line 1164265
    iget-object v0, p0, LX/72N;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    .line 1164266
    sget-object v1, Lcom/facebook/common/locale/Country;->a:Lcom/facebook/common/locale/Country;

    invoke-virtual {v1, p1}, Lcom/facebook/common/locale/Country;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LX/6xe;->REQUIRED:LX/6xe;

    .line 1164267
    :goto_0
    iget-object v2, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->t:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    .line 1164268
    iput-object v1, v2, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->u:LX/6xe;

    .line 1164269
    iget-object v0, p0, LX/72N;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    .line 1164270
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->w:LX/73G;

    invoke-virtual {v1, p1}, LX/73G;->a(Lcom/facebook/common/locale/Country;)V

    .line 1164271
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->l:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v2, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->w:LX/73G;

    invoke-virtual {v2}, LX/73G;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setMaxLength(I)V

    .line 1164272
    iget-object v0, p0, LX/72N;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    .line 1164273
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->x:LX/73H;

    invoke-virtual {v1, p1}, LX/73H;->a(Lcom/facebook/common/locale/Country;)V

    .line 1164274
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->m:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v2, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->x:LX/73H;

    invoke-virtual {v2}, LX/73H;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setMaxLength(I)V

    .line 1164275
    iget-object v0, p0, LX/72N;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    .line 1164276
    sget-object v1, Lcom/facebook/common/locale/Country;->a:Lcom/facebook/common/locale/Country;

    invoke-virtual {v1, p1}, Lcom/facebook/common/locale/Country;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1164277
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v2, 0x7f081e4e

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1164278
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v2, 0x7f081e4f

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1164279
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->l:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v2, 0x7f081e50

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1164280
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->m:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v2, 0x7f081e51

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1164281
    :goto_1
    return-void

    .line 1164282
    :cond_0
    sget-object v1, LX/6xe;->OPTIONAL:LX/6xe;

    goto :goto_0

    .line 1164283
    :cond_1
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v2, 0x7f081e52

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1164284
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v2, 0x7f081e53

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1164285
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->l:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v2, 0x7f081e54

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1164286
    iget-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->m:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v2, 0x7f081e55

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
