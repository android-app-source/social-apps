.class public LX/7f3;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/nio/FloatBuffer;

.field private static final b:Ljava/nio/FloatBuffer;


# instance fields
.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/7f2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1220285
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, LX/7f5;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, LX/7f3;->a:Ljava/nio/FloatBuffer;

    .line 1220286
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    invoke-static {v0}, LX/7f5;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, LX/7f3;->b:Ljava/nio/FloatBuffer;

    return-void

    nop

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 1220287
    :array_1
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1220288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1220289
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v0, p0, LX/7f3;->c:Ljava/util/Map;

    return-void
.end method

.method public static a(LX/7f3;Ljava/lang/String;[F)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 1220290
    iget-object v0, p0, LX/7f3;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1220291
    iget-object v0, p0, LX/7f3;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7f2;

    .line 1220292
    :goto_0
    iget-object v1, v0, LX/7f2;->a:LX/7f4;

    invoke-virtual {v1}, LX/7f4;->a()V

    .line 1220293
    iget v0, v0, LX/7f2;->b:I

    invoke-static {v0, v6, v4, p2, v4}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 1220294
    return-void

    .line 1220295
    :cond_0
    new-instance v0, LX/7f2;

    invoke-direct {v0, p1}, LX/7f2;-><init>(Ljava/lang/String;)V

    .line 1220296
    iget-object v1, p0, LX/7f3;->c:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1220297
    iget-object v1, v0, LX/7f2;->a:LX/7f4;

    invoke-virtual {v1}, LX/7f4;->a()V

    .line 1220298
    const-string v1, "precision mediump float;\nvarying vec2 interp_tc;\n\nuniform sampler2D y_tex;\nuniform sampler2D u_tex;\nuniform sampler2D v_tex;\n\nvoid main() {\n  float y = texture2D(y_tex, interp_tc).r;\n  float u = texture2D(u_tex, interp_tc).r - 0.5;\n  float v = texture2D(v_tex, interp_tc).r - 0.5;\n  gl_FragColor = vec4(y + 1.403 * v,                       y - 0.344 * u - 0.714 * v,                       y + 1.77 * u, 1);\n}\n"

    if-ne p1, v1, :cond_1

    .line 1220299
    iget-object v1, v0, LX/7f2;->a:LX/7f4;

    const-string v2, "y_tex"

    invoke-virtual {v1, v2}, LX/7f4;->a(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1, v4}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 1220300
    iget-object v1, v0, LX/7f2;->a:LX/7f4;

    const-string v2, "u_tex"

    invoke-virtual {v1, v2}, LX/7f4;->a(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1, v6}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 1220301
    iget-object v1, v0, LX/7f2;->a:LX/7f4;

    const-string v2, "v_tex"

    invoke-virtual {v1, v2}, LX/7f4;->a(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1, v5}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 1220302
    :goto_1
    const-string v1, "Initialize fragment shader uniform values."

    invoke-static {v1}, LX/7f5;->a(Ljava/lang/String;)V

    .line 1220303
    iget-object v1, v0, LX/7f2;->a:LX/7f4;

    const-string v2, "in_pos"

    sget-object v3, LX/7f3;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v2, v5, v3}, LX/7f4;->a(Ljava/lang/String;ILjava/nio/FloatBuffer;)V

    .line 1220304
    iget-object v1, v0, LX/7f2;->a:LX/7f4;

    const-string v2, "in_tc"

    sget-object v3, LX/7f3;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v2, v5, v3}, LX/7f4;->a(Ljava/lang/String;ILjava/nio/FloatBuffer;)V

    goto :goto_0

    .line 1220305
    :cond_1
    const-string v1, "precision mediump float;\nvarying vec2 interp_tc;\n\nuniform sampler2D rgb_tex;\n\nvoid main() {\n  gl_FragColor = texture2D(rgb_tex, interp_tc);\n}\n"

    if-ne p1, v1, :cond_2

    .line 1220306
    iget-object v1, v0, LX/7f2;->a:LX/7f4;

    const-string v2, "rgb_tex"

    invoke-virtual {v1, v2}, LX/7f4;->a(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1, v4}, Landroid/opengl/GLES20;->glUniform1i(II)V

    goto :goto_1

    .line 1220307
    :cond_2
    const-string v1, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 interp_tc;\n\nuniform samplerExternalOES oes_tex;\n\nvoid main() {\n  gl_FragColor = texture2D(oes_tex, interp_tc);\n}\n"

    if-ne p1, v1, :cond_3

    .line 1220308
    iget-object v1, v0, LX/7f2;->a:LX/7f4;

    const-string v2, "oes_tex"

    invoke-virtual {v1, v2}, LX/7f4;->a(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1, v4}, Landroid/opengl/GLES20;->glUniform1i(II)V

    goto :goto_1

    .line 1220309
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown fragment shader: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
