.class public final LX/7qh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1260425
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1260426
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1260427
    :goto_0
    return v1

    .line 1260428
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1260429
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1260430
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1260431
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1260432
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1260433
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1260434
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1260435
    :cond_2
    const-string v5, "taggable_activity"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1260436
    invoke-static {p0, p1}, LX/7qg;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1260437
    :cond_3
    const-string v5, "taggable_activity_icon"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1260438
    invoke-static {p0, p1}, LX/7qf;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1260439
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1260440
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1260441
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1260442
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1260443
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1260444
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1260445
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1260446
    if-eqz v0, :cond_0

    .line 1260447
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260448
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1260449
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1260450
    if-eqz v0, :cond_1

    .line 1260451
    const-string v1, "taggable_activity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260452
    invoke-static {p0, v0, p2, p3}, LX/7qg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1260453
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1260454
    if-eqz v0, :cond_2

    .line 1260455
    const-string v1, "taggable_activity_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260456
    invoke-static {p0, v0, p2, p3}, LX/7qf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1260457
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1260458
    return-void
.end method
