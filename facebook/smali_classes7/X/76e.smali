.class public final LX/76e;
.super Landroid/preference/PreferenceCategory;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1171198
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1171199
    return-void
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 4

    .prologue
    .line 1171200
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 1171201
    const-string v0, "Promotions - Internal"

    invoke-virtual {p0, v0}, LX/76e;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171202
    invoke-virtual {p0}, LX/76e;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1171203
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1171204
    const-string v2, "Quick Promotion Config"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171205
    const-string v2, "View quick promotion configuration"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1171206
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/facebook/quickpromotion/debug/QuickPromotionSettingsActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 1171207
    invoke-virtual {p0, v1}, LX/76e;->addPreference(Landroid/preference/Preference;)Z

    .line 1171208
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1171209
    const-string v2, "Segues"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1171210
    const-string v2, "View/Launch view intent FB URIs a.k.a. segues"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1171211
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 1171212
    invoke-virtual {p0, v1}, LX/76e;->addPreference(Landroid/preference/Preference;)Z

    .line 1171213
    return-void
.end method
