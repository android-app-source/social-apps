.class public LX/8FX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1317980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1317981
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/view/ViewGroup;I[II)LX/3rL;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/view/ViewGroup;",
            "I[II)",
            "LX/3rL",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1317982
    instance-of v0, p1, Landroid/widget/ListView;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1317983
    check-cast v0, Landroid/widget/ListView;

    .line 1317984
    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v4

    invoke-interface {v4}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v0}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v4

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    invoke-interface {v5}, Landroid/widget/ListAdapter;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne v4, v5, :cond_4

    .line 1317985
    invoke-virtual {v0}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v4

    sub-int/2addr v1, v4

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    move-object v1, p1

    .line 1317986
    :goto_1
    if-eqz v0, :cond_2

    if-ne v0, p0, :cond_2

    .line 1317987
    invoke-virtual {p0, p3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1317988
    aget v0, p3, v3

    if-gt v0, p4, :cond_2

    .line 1317989
    aget v0, p3, v3

    sub-int v0, p4, v0

    .line 1317990
    if-nez p2, :cond_1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v4, 0x2

    if-lt v2, v4, :cond_1

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1317991
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1317992
    aget v1, p3, v3

    if-lez v1, :cond_1

    .line 1317993
    aget v1, p3, v3

    add-int/2addr v0, v1

    move v2, v3

    .line 1317994
    :goto_2
    new-instance v1, LX/3rL;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1

    .line 1317995
    :cond_0
    instance-of v0, p1, Landroid/widget/ScrollView;

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ne v0, v3, :cond_3

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 1317996
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move-object v1, v0

    move-object v0, p0

    goto :goto_1

    :cond_1
    move v2, v3

    goto :goto_2

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/view/ViewGroup;I)V
    .locals 2

    .prologue
    .line 1317997
    sget-object v0, LX/8FW;->IMMEDIATE:LX/8FW;

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, LX/8FX;->b(Landroid/view/ViewGroup;ILX/8FW;I)V

    .line 1317998
    return-void
.end method

.method public static b(Landroid/view/ViewGroup;ILX/8FW;I)V
    .locals 4

    .prologue
    .line 1317999
    if-eqz p0, :cond_0

    const/4 v0, 0x6

    if-le p3, v0, :cond_1

    .line 1318000
    :cond_0
    :goto_0
    return-void

    .line 1318001
    :cond_1
    new-instance v0, Lcom/facebook/pages/common/util/PagesScrollUtils$1;

    invoke-direct {v0, p0, p1, p3}, Lcom/facebook/pages/common/util/PagesScrollUtils$1;-><init>(Landroid/view/ViewGroup;II)V

    .line 1318002
    sget-object v1, LX/8FV;->a:[I

    invoke-virtual {p2}, LX/8FW;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1318003
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1318004
    :pswitch_0
    const-wide/16 v2, 0x21

    invoke-virtual {p0, v0, v2, v3}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
