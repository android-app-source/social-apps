.class public LX/8W7;
.super LX/0D4;
.source ""


# instance fields
.field public a:J

.field public b:LX/8Sx;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/8TD;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0So;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1353766
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/8W7;-><init>(Landroid/content/Context;Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1353767
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1353764
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/8W7;-><init>(Landroid/content/Context;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1353765
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1353759
    invoke-direct {p0, p1, p3, p4}, LX/0D4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1353760
    const-class p1, LX/8W7;

    invoke-static {p1, p0, p2}, LX/8W7;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1353761
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p3, 0x13

    if-lt p1, p3, :cond_0

    invoke-virtual {p0}, LX/8W7;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object p1

    iget p1, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_0

    .line 1353762
    const/4 p1, 0x1

    invoke-static {p1}, Landroid/webkit/WebView;->setWebContentsDebuggingEnabled(Z)V

    .line 1353763
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/8W7;

    const-class v0, LX/8Sx;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/8Sx;

    invoke-static {p0}, LX/8TD;->a(LX/0QB;)LX/8TD;

    move-result-object v1

    check-cast v1, LX/8TD;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object p0

    check-cast p0, LX/0So;

    iput-object v0, p1, LX/8W7;->b:LX/8Sx;

    iput-object v1, p1, LX/8W7;->c:LX/8TD;

    iput-object p0, p1, LX/8W7;->d:LX/0So;

    return-void
.end method

.method public static e(Lorg/json/JSONObject;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 1353756
    const-string v0, "content"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 1353757
    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 1353758
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static f(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1353755
    const-string v0, "content"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "promiseID"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static g(Lorg/json/JSONObject;)I
    .locals 1

    .prologue
    .line 1353743
    const-string v0, "content"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/8W5;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1353747
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1353748
    :try_start_0
    const-string v0, "type"

    invoke-virtual {p1}, LX/8W5;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1353749
    const-string v0, "content"

    invoke-virtual {v1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1353750
    const-string v0, "source"

    const-string v2, "android"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1353751
    :goto_0
    const-string v0, "javascript:e = new Event(\'message\');e.data = %s;window.dispatchEvent(e);"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/8W7;->loadUrl(Ljava/lang/String;)V

    .line 1353752
    return-void

    .line 1353753
    :catch_0
    move-exception v0

    .line 1353754
    iget-object v2, p0, LX/8W7;->c:LX/8TD;

    sget-object v3, LX/8TE;->SEND_MESSAGE_ERROR:LX/8TE;

    const-string v4, "Unexpected exception while constructing JSONObject to be dispatched to Javascript."

    invoke-virtual {v2, v3, v4, v0}, LX/8TD;->a(LX/8TE;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final destroy()V
    .locals 1

    .prologue
    .line 1353744
    const-string v0, "QuicksilverAndroid"

    invoke-virtual {p0, v0}, LX/8W7;->removeJavascriptInterface(Ljava/lang/String;)V

    .line 1353745
    invoke-super {p0}, LX/0D4;->destroy()V

    .line 1353746
    return-void
.end method
