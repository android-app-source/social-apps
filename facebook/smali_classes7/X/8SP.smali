.class public LX/8SP;
.super LX/1Cv;
.source ""


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:LX/8Sa;

.field private final c:Landroid/content/res/Resources;

.field private final d:LX/0wM;

.field private final e:LX/8SR;

.field public f:LX/1oT;

.field public final g:Ljava/lang/String;

.field private final h:LX/8SM;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;LX/8Sa;Landroid/content/res/Resources;LX/0wM;LX/8SR;LX/8SM;Ljava/lang/String;)V
    .locals 1
    .param p5    # LX/8SR;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/8SM;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1346632
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1346633
    iput-object p1, p0, LX/8SP;->a:Landroid/view/LayoutInflater;

    .line 1346634
    iput-object p2, p0, LX/8SP;->b:LX/8Sa;

    .line 1346635
    iput-object p3, p0, LX/8SP;->c:Landroid/content/res/Resources;

    .line 1346636
    iput-object p4, p0, LX/8SP;->d:LX/0wM;

    .line 1346637
    iput-object p5, p0, LX/8SP;->e:LX/8SR;

    .line 1346638
    iput-object p6, p0, LX/8SP;->h:LX/8SM;

    .line 1346639
    iput-object p7, p0, LX/8SP;->g:Ljava/lang/String;

    .line 1346640
    invoke-virtual {p5}, LX/8SR;->b()LX/1oT;

    move-result-object v0

    iput-object v0, p0, LX/8SP;->f:LX/1oT;

    .line 1346641
    return-void
.end method

.method private a(Z)I
    .locals 2

    .prologue
    .line 1346629
    if-eqz p1, :cond_0

    .line 1346630
    iget-object v0, p0, LX/8SP;->c:Landroid/content/res/Resources;

    const v1, 0x7f0a00d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1346631
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/8SP;->c:Landroid/content/res/Resources;

    const v1, 0x7f0a010c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method private a(III)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1346625
    iget-object v0, p0, LX/8SP;->d:LX/0wM;

    iget-object v1, p0, LX/8SP;->c:Landroid/content/res/Resources;

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, p1, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1346626
    iget-object v1, p0, LX/8SP;->c:Landroid/content/res/Resources;

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1346627
    invoke-virtual {v0, v2, v2, v1, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1346628
    return-object v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1346620
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->y_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/8SP;->h:LX/8SM;

    sget-object v3, LX/8SM;->AUDIENCE_SHOW_NONE:LX/8SM;

    if-ne v2, v3, :cond_2

    :cond_0
    move v0, v1

    .line 1346621
    :cond_1
    :goto_0
    return v0

    .line 1346622
    :cond_2
    iget-object v2, p0, LX/8SP;->h:LX/8SM;

    sget-object v3, LX/8SM;->AUDIENCE_SHOW_ALL:LX/8SM;

    if-eq v2, v3, :cond_1

    .line 1346623
    invoke-static {p1}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v2

    .line 1346624
    iget-object v3, p0, LX/8SP;->h:LX/8SM;

    sget-object v4, LX/8SM;->AUDIENCE_SHOW_PUBLIC_FRIENDS_AND_CUSTOM:LX/8SM;

    if-ne v3, v4, :cond_3

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS_EXCEPT:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private b(Z)I
    .locals 2

    .prologue
    .line 1346617
    if-eqz p1, :cond_0

    .line 1346618
    iget-object v0, p0, LX/8SP;->c:Landroid/content/res/Resources;

    const v1, 0x7f0a00d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1346619
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/8SP;->c:Landroid/content/res/Resources;

    const v1, 0x7f0a010e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method private static c(Z)I
    .locals 1

    .prologue
    .line 1346614
    if-eqz p0, :cond_0

    .line 1346615
    const v0, 0x7f0a00d2

    .line 1346616
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0a010d

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/1oT;
    .locals 1

    .prologue
    .line 1346611
    iget-object v0, p0, LX/8SP;->e:LX/8SR;

    .line 1346612
    iget-object p0, v0, LX/8SR;->a:LX/0Px;

    move-object v0, p0

    .line 1346613
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oT;

    return-object v0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1346581
    iget-object v0, p0, LX/8SP;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f03013f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 7

    .prologue
    .line 1346588
    check-cast p2, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1346589
    iget-object v0, p0, LX/8SP;->f:LX/1oT;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 1346590
    invoke-static {v4}, LX/8SP;->c(Z)I

    move-result v5

    .line 1346591
    check-cast p3, Landroid/widget/LinearLayout;

    .line 1346592
    const v0, 0x7f0d060a

    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1346593
    const v1, 0x7f0d0607

    invoke-virtual {p3, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1346594
    const v2, 0x7f0d0608

    invoke-virtual {p3, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    .line 1346595
    const v3, 0x7f0d0609

    invoke-virtual {p3, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/resources/ui/FbTextView;

    .line 1346596
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1346597
    invoke-direct {p0, v4}, LX/8SP;->a(Z)I

    move-result v6

    invoke-virtual {v2, v6}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1346598
    invoke-direct {p0, p2}, LX/8SP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1346599
    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1346600
    :goto_0
    invoke-static {p2}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v3

    sget-object v6, LX/8SZ;->TOKEN:LX/8SZ;

    invoke-static {v3, v6}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v2

    .line 1346601
    const v3, 0x7f0b0ba0

    invoke-direct {p0, v2, v5, v3}, LX/8SP;->a(III)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1346602
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1346603
    if-eqz v4, :cond_1

    .line 1346604
    const v1, 0x7f0207d6

    const v2, 0x7f0b0ba2

    invoke-direct {p0, v1, v5, v2}, LX/8SP;->a(III)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1346605
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1346606
    :goto_1
    return-void

    .line 1346607
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1346608
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->y_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1346609
    invoke-direct {p0, v4}, LX/8SP;->b(Z)I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_0

    .line 1346610
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1346587
    iget-object v0, p0, LX/8SP;->e:LX/8SR;

    iget-object v1, p0, LX/8SP;->f:LX/1oT;

    invoke-virtual {v0, v1}, LX/8SR;->a(LX/1oT;)I

    move-result v0

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1346584
    iget-object v0, p0, LX/8SP;->e:LX/8SR;

    .line 1346585
    iget-object p0, v0, LX/8SR;->a:LX/0Px;

    move-object v0, p0

    .line 1346586
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1346583
    invoke-virtual {p0, p1}, LX/8SP;->a(I)LX/1oT;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1346582
    int-to-long v0, p1

    return-wide v0
.end method
