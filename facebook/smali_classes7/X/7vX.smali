.class public final LX/7vX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsMutationsModels$WatchEventMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public final synthetic b:LX/7vZ;


# direct methods
.method public constructor <init>(LX/7vZ;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 0

    .prologue
    .line 1274688
    iput-object p1, p0, LX/7vX;->b:LX/7vZ;

    iput-object p2, p0, LX/7vX;->a:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1274689
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1274690
    iget-object v0, p0, LX/7vX;->b:LX/7vZ;

    iget-object v0, v0, LX/7vZ;->b:LX/0ie;

    iget-object v1, p0, LX/7vX;->a:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "public_event"

    invoke-virtual {v0, v1, v2}, LX/0ie;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274691
    return-void
.end method
