.class public LX/6jg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;


# instance fields
.field public final messageMetadata:LX/6kn;

.field public final ttl:Ljava/lang/Integer;

.field public final type:Ljava/lang/String;

.field public final untypedData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1131882
    new-instance v0, LX/1sv;

    const-string v1, "DeltaAdminTextMessage"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jg;->b:LX/1sv;

    .line 1131883
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jg;->c:LX/1sw;

    .line 1131884
    new-instance v0, LX/1sw;

    const-string v1, "type"

    const/16 v2, 0xb

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jg;->d:LX/1sw;

    .line 1131885
    new-instance v0, LX/1sw;

    const-string v1, "untypedData"

    const/16 v2, 0xd

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jg;->e:LX/1sw;

    .line 1131886
    new-instance v0, LX/1sw;

    const-string v1, "ttl"

    const/16 v2, 0x8

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jg;->f:LX/1sw;

    .line 1131887
    sput-boolean v4, LX/6jg;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6kn;Ljava/lang/String;Ljava/util/Map;Ljava/lang/Integer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kn;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1131888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1131889
    iput-object p1, p0, LX/6jg;->messageMetadata:LX/6kn;

    .line 1131890
    iput-object p2, p0, LX/6jg;->type:Ljava/lang/String;

    .line 1131891
    iput-object p3, p0, LX/6jg;->untypedData:Ljava/util/Map;

    .line 1131892
    iput-object p4, p0, LX/6jg;->ttl:Ljava/lang/Integer;

    .line 1131893
    return-void
.end method

.method public static a(LX/6jg;)V
    .locals 4

    .prologue
    .line 1131894
    iget-object v0, p0, LX/6jg;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1131895
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jg;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1131896
    :cond_0
    iget-object v0, p0, LX/6jg;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    sget-object v0, LX/6kk;->a:LX/1sn;

    iget-object v1, p0, LX/6jg;->ttl:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1131897
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'ttl\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6jg;->ttl:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1131898
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1131899
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1131900
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v1, v0

    .line 1131901
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1131902
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaAdminTextMessage"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1131903
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131904
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131905
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131906
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131907
    const-string v4, "messageMetadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131908
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131909
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131910
    iget-object v4, p0, LX/6jg;->messageMetadata:LX/6kn;

    if-nez v4, :cond_6

    .line 1131911
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131912
    :goto_3
    iget-object v4, p0, LX/6jg;->type:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 1131913
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131914
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131915
    const-string v4, "type"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131916
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131917
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131918
    iget-object v4, p0, LX/6jg;->type:Ljava/lang/String;

    if-nez v4, :cond_7

    .line 1131919
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131920
    :cond_0
    :goto_4
    iget-object v4, p0, LX/6jg;->untypedData:Ljava/util/Map;

    if-eqz v4, :cond_1

    .line 1131921
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131922
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131923
    const-string v4, "untypedData"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131924
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131925
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131926
    iget-object v4, p0, LX/6jg;->untypedData:Ljava/util/Map;

    if-nez v4, :cond_8

    .line 1131927
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131928
    :cond_1
    :goto_5
    iget-object v4, p0, LX/6jg;->ttl:Ljava/lang/Integer;

    if-eqz v4, :cond_2

    .line 1131929
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131930
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131931
    const-string v4, "ttl"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131932
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131933
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131934
    iget-object v0, p0, LX/6jg;->ttl:Ljava/lang/Integer;

    if-nez v0, :cond_9

    .line 1131935
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131936
    :cond_2
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131937
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131938
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1131939
    :cond_3
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1131940
    :cond_4
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1131941
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1131942
    :cond_6
    iget-object v4, p0, LX/6jg;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1131943
    :cond_7
    iget-object v4, p0, LX/6jg;->type:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1131944
    :cond_8
    iget-object v4, p0, LX/6jg;->untypedData:Ljava/util/Map;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1131945
    :cond_9
    sget-object v0, LX/6kk;->b:Ljava/util/Map;

    iget-object v4, p0, LX/6jg;->ttl:Ljava/lang/Integer;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1131946
    if-eqz v0, :cond_a

    .line 1131947
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131948
    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131949
    :cond_a
    iget-object v4, p0, LX/6jg;->ttl:Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1131950
    if-eqz v0, :cond_2

    .line 1131951
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    const/16 v2, 0xb

    .line 1131952
    invoke-static {p0}, LX/6jg;->a(LX/6jg;)V

    .line 1131953
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1131954
    iget-object v0, p0, LX/6jg;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1131955
    sget-object v0, LX/6jg;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131956
    iget-object v0, p0, LX/6jg;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1131957
    :cond_0
    iget-object v0, p0, LX/6jg;->type:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1131958
    iget-object v0, p0, LX/6jg;->type:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1131959
    sget-object v0, LX/6jg;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131960
    iget-object v0, p0, LX/6jg;->type:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1131961
    :cond_1
    iget-object v0, p0, LX/6jg;->untypedData:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 1131962
    iget-object v0, p0, LX/6jg;->untypedData:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 1131963
    sget-object v0, LX/6jg;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131964
    new-instance v0, LX/7H3;

    iget-object v1, p0, LX/6jg;->untypedData:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v2, v2, v1}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 1131965
    iget-object v0, p0, LX/6jg;->untypedData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1131966
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, LX/1su;->a(Ljava/lang/String;)V

    .line 1131967
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1131968
    :cond_2
    iget-object v0, p0, LX/6jg;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1131969
    iget-object v0, p0, LX/6jg;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1131970
    sget-object v0, LX/6jg;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131971
    iget-object v0, p0, LX/6jg;->ttl:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1131972
    :cond_3
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1131973
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1131974
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1131975
    if-nez p1, :cond_1

    .line 1131976
    :cond_0
    :goto_0
    return v0

    .line 1131977
    :cond_1
    instance-of v1, p1, LX/6jg;

    if-eqz v1, :cond_0

    .line 1131978
    check-cast p1, LX/6jg;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1131979
    if-nez p1, :cond_3

    .line 1131980
    :cond_2
    :goto_1
    move v0, v2

    .line 1131981
    goto :goto_0

    .line 1131982
    :cond_3
    iget-object v0, p0, LX/6jg;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1131983
    :goto_2
    iget-object v3, p1, LX/6jg;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1131984
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1131985
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1131986
    iget-object v0, p0, LX/6jg;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6jg;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1131987
    :cond_5
    iget-object v0, p0, LX/6jg;->type:Ljava/lang/String;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1131988
    :goto_4
    iget-object v3, p1, LX/6jg;->type:Ljava/lang/String;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1131989
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1131990
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1131991
    iget-object v0, p0, LX/6jg;->type:Ljava/lang/String;

    iget-object v3, p1, LX/6jg;->type:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1131992
    :cond_7
    iget-object v0, p0, LX/6jg;->untypedData:Ljava/util/Map;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1131993
    :goto_6
    iget-object v3, p1, LX/6jg;->untypedData:Ljava/util/Map;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1131994
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1131995
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1131996
    iget-object v0, p0, LX/6jg;->untypedData:Ljava/util/Map;

    iget-object v3, p1, LX/6jg;->untypedData:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1131997
    :cond_9
    iget-object v0, p0, LX/6jg;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1131998
    :goto_8
    iget-object v3, p1, LX/6jg;->ttl:Ljava/lang/Integer;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1131999
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1132000
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132001
    iget-object v0, p0, LX/6jg;->ttl:Ljava/lang/Integer;

    iget-object v3, p1, LX/6jg;->ttl:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_b
    move v2, v1

    .line 1132002
    goto :goto_1

    :cond_c
    move v0, v2

    .line 1132003
    goto :goto_2

    :cond_d
    move v3, v2

    .line 1132004
    goto :goto_3

    :cond_e
    move v0, v2

    .line 1132005
    goto :goto_4

    :cond_f
    move v3, v2

    .line 1132006
    goto :goto_5

    :cond_10
    move v0, v2

    .line 1132007
    goto :goto_6

    :cond_11
    move v3, v2

    .line 1132008
    goto :goto_7

    :cond_12
    move v0, v2

    .line 1132009
    goto :goto_8

    :cond_13
    move v3, v2

    .line 1132010
    goto :goto_9
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1132011
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1132012
    sget-boolean v0, LX/6jg;->a:Z

    .line 1132013
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jg;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1132014
    return-object v0
.end method
