.class public LX/86l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/86d;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private a:LX/0fO;


# direct methods
.method public constructor <init>(LX/0fO;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1297709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1297710
    iput-object p1, p0, LX/86l;->a:LX/0fO;

    .line 1297711
    return-void
.end method

.method public static a(LX/0QB;)LX/86l;
    .locals 4

    .prologue
    .line 1297712
    const-class v1, LX/86l;

    monitor-enter v1

    .line 1297713
    :try_start_0
    sget-object v0, LX/86l;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1297714
    sput-object v2, LX/86l;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1297715
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1297716
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1297717
    new-instance p0, LX/86l;

    invoke-static {v0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v3

    check-cast v3, LX/0fO;

    invoke-direct {p0, v3}, LX/86l;-><init>(LX/0fO;)V

    .line 1297718
    move-object v0, p0

    .line 1297719
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1297720
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/86l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1297721
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1297722
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;ZZ)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1297723
    if-eqz p2, :cond_2

    .line 1297724
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getStyleTransfer()Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    move-result-object v2

    if-eqz v2, :cond_1

    if-nez p3, :cond_1

    .line 1297725
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1297726
    goto :goto_0

    .line 1297727
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getStyleTransfer()Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/86l;->a:LX/0fO;

    .line 1297728
    invoke-static {v2}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object p0

    invoke-virtual {p0}, LX/0i8;->f()Z

    move-result p0

    move v2, p0

    .line 1297729
    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
