.class public final LX/7Xb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$FetchZeroTermsConditionsQueryModel;",
        ">;",
        "LX/4pL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/zero/service/FbZeroRequestHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/service/FbZeroRequestHandler;)V
    .locals 0

    .prologue
    .line 1218538
    iput-object p1, p0, LX/7Xb;->a:Lcom/facebook/zero/service/FbZeroRequestHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1218539
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1218540
    new-instance v1, LX/4pL;

    invoke-direct {v1}, LX/4pL;-><init>()V

    .line 1218541
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1218542
    check-cast v0, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$FetchZeroTermsConditionsQueryModel;

    .line 1218543
    invoke-virtual {v0}, Lcom/facebook/zero/protocol/graphql/ZeroTermsConditionsGraphQLModels$FetchZeroTermsConditionsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1218544
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1218545
    if-eqz v0, :cond_0

    .line 1218546
    const/4 v3, 0x3

    invoke-virtual {v2, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 1218547
    iput-object v3, v1, LX/4pL;->a:Ljava/lang/String;

    .line 1218548
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 1218549
    iput-object v3, v1, LX/4pL;->b:Ljava/lang/String;

    .line 1218550
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 1218551
    iput-object v3, v1, LX/4pL;->c:Ljava/lang/String;

    .line 1218552
    const/4 v3, 0x2

    invoke-virtual {v2, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1218553
    iput-object v0, v1, LX/4pL;->d:Ljava/lang/String;

    .line 1218554
    :cond_0
    return-object v1

    .line 1218555
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
