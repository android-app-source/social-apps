.class public LX/8Uw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/8Vo;

.field public final c:LX/8TD;

.field public final d:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(LX/0tX;LX/8Vo;LX/8TD;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1351612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1351613
    iput-object p1, p0, LX/8Uw;->a:LX/0tX;

    .line 1351614
    iput-object p2, p0, LX/8Uw;->b:LX/8Vo;

    .line 1351615
    iput-object p3, p0, LX/8Uw;->c:LX/8TD;

    .line 1351616
    iput-object p4, p0, LX/8Uw;->d:Ljava/util/concurrent/Executor;

    .line 1351617
    return-void
.end method

.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1351618
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1351619
    :cond_0
    const-string v0, ""

    .line 1351620
    :goto_0
    return-object v0

    .line 1351621
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "{"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1351622
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1351623
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1351624
    :cond_2
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1351625
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILjava/lang/String;Ljava/util/List;LX/8Uv;Z)V
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/8Uv;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1351626
    if-eqz p4, :cond_0

    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1351627
    :cond_0
    :goto_0
    return-void

    .line 1351628
    :cond_1
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 1351629
    sget-object v0, LX/8TE;->GAME_ID:LX/8TE;

    iget-object v0, v0, LX/8TE;->value:Ljava/lang/String;

    invoke-interface {v7, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1351630
    sget-object v0, LX/8TE;->EXTRAS_SCORE:LX/8TE;

    iget-object v0, v0, LX/8TE;->value:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1351631
    sget-object v0, LX/8TE;->EXTRAS_SCREENSHOT_IMAGE_HANDLE:LX/8TE;

    iget-object v0, v0, LX/8TE;->value:Ljava/lang/String;

    invoke-interface {v7, v0, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1351632
    sget-object v0, LX/8TE;->EXTRAS_THREAD_IDS:LX/8TE;

    iget-object v0, v0, LX/8TE;->value:Ljava/lang/String;

    invoke-static {p4}, LX/8Uw;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1351633
    sget-object v0, LX/8TE;->EXTRAS_IS_RETRY:LX/8TE;

    iget-object v0, v0, LX/8TE;->value:Ljava/lang/String;

    invoke-static {p6}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1351634
    iget-object v8, p0, LX/8Uw;->d:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move v4, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;-><init>(LX/8Uw;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;LX/8Uv;Ljava/util/Map;)V

    const v1, 0x1ddddbb5

    invoke-static {v8, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method
