.class public final LX/6yK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V
    .locals 0

    .prologue
    .line 1159673
    iput-object p1, p0, LX/6yK;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1159674
    iget-object v0, p0, LX/6yK;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    if-eqz v0, :cond_0

    .line 1159675
    iget-object v0, p0, LX/6yK;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    invoke-virtual {v0}, LX/6y3;->a()V

    .line 1159676
    :cond_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x5

    if-gt v0, v1, :cond_1

    .line 1159677
    iget-object v0, p0, LX/6yK;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    .line 1159678
    :cond_1
    iget-object v0, p0, LX/6yK;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    iget-object v1, p0, LX/6yK;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-virtual {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/6y3;->a(Z)V

    .line 1159679
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1159680
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1159681
    return-void
.end method
