.class public LX/7Om;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Oi;


# instance fields
.field private final a:LX/7Oi;

.field private final b:J


# direct methods
.method public constructor <init>(LX/7Oi;J)V
    .locals 2

    .prologue
    .line 1202108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202109
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1202110
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Oi;

    iput-object v0, p0, LX/7Om;->a:LX/7Oi;

    .line 1202111
    iput-wide p2, p0, LX/7Om;->b:J

    .line 1202112
    return-void

    .line 1202113
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2WF;Ljava/io/OutputStream;)J
    .locals 8

    .prologue
    .line 1202114
    iget-wide v0, p1, LX/2WF;->a:J

    .line 1202115
    :goto_0
    iget-wide v2, p1, LX/2WF;->b:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 1202116
    new-instance v2, LX/2WF;

    iget-wide v4, p0, LX/7Om;->b:J

    add-long/2addr v4, v0

    iget-wide v6, p1, LX/2WF;->b:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-direct {v2, v0, v1, v4, v5}, LX/2WF;-><init>(JJ)V

    .line 1202117
    iget-object v3, p0, LX/7Om;->a:LX/7Oi;

    invoke-interface {v3, v2, p2}, LX/7Oi;->a(LX/2WF;Ljava/io/OutputStream;)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 1202118
    goto :goto_0

    .line 1202119
    :cond_0
    invoke-virtual {p1}, LX/2WF;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a()LX/3Dd;
    .locals 1

    .prologue
    .line 1202120
    iget-object v0, p0, LX/7Om;->a:LX/7Oi;

    invoke-interface {v0}, LX/7Oi;->a()LX/3Dd;

    move-result-object v0

    return-object v0
.end method
