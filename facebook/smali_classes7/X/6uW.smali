.class public final enum LX/6uW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6uW;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6uW;

.field public static final enum EVENT_TICKETING:LX/6uW;

.field public static final enum INSTANT_WORKFLOWS:LX/6uW;

.field public static final enum JS_BASED:LX/6uW;

.field public static final enum M:LX/6uW;

.field public static final enum MESSENGER_COMMERCE:LX/6uW;

.field public static final enum PAGES_COMMERCE:LX/6uW;

.field public static final enum SIMPLE:LX/6uW;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1155913
    new-instance v0, LX/6uW;

    const-string v1, "EVENT_TICKETING"

    invoke-direct {v0, v1, v3}, LX/6uW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6uW;->EVENT_TICKETING:LX/6uW;

    .line 1155914
    new-instance v0, LX/6uW;

    const-string v1, "INSTANT_WORKFLOWS"

    invoke-direct {v0, v1, v4}, LX/6uW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6uW;->INSTANT_WORKFLOWS:LX/6uW;

    .line 1155915
    new-instance v0, LX/6uW;

    const-string v1, "M"

    invoke-direct {v0, v1, v5}, LX/6uW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6uW;->M:LX/6uW;

    .line 1155916
    new-instance v0, LX/6uW;

    const-string v1, "MESSENGER_COMMERCE"

    invoke-direct {v0, v1, v6}, LX/6uW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6uW;->MESSENGER_COMMERCE:LX/6uW;

    .line 1155917
    new-instance v0, LX/6uW;

    const-string v1, "PAGES_COMMERCE"

    invoke-direct {v0, v1, v7}, LX/6uW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6uW;->PAGES_COMMERCE:LX/6uW;

    .line 1155918
    new-instance v0, LX/6uW;

    const-string v1, "SIMPLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6uW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6uW;->SIMPLE:LX/6uW;

    .line 1155919
    new-instance v0, LX/6uW;

    const-string v1, "JS_BASED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/6uW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6uW;->JS_BASED:LX/6uW;

    .line 1155920
    const/4 v0, 0x7

    new-array v0, v0, [LX/6uW;

    sget-object v1, LX/6uW;->EVENT_TICKETING:LX/6uW;

    aput-object v1, v0, v3

    sget-object v1, LX/6uW;->INSTANT_WORKFLOWS:LX/6uW;

    aput-object v1, v0, v4

    sget-object v1, LX/6uW;->M:LX/6uW;

    aput-object v1, v0, v5

    sget-object v1, LX/6uW;->MESSENGER_COMMERCE:LX/6uW;

    aput-object v1, v0, v6

    sget-object v1, LX/6uW;->PAGES_COMMERCE:LX/6uW;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6uW;->SIMPLE:LX/6uW;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6uW;->JS_BASED:LX/6uW;

    aput-object v2, v0, v1

    sput-object v0, LX/6uW;->$VALUES:[LX/6uW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1155910
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6uW;
    .locals 1

    .prologue
    .line 1155911
    const-class v0, LX/6uW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6uW;

    return-object v0
.end method

.method public static values()[LX/6uW;
    .locals 1

    .prologue
    .line 1155912
    sget-object v0, LX/6uW;->$VALUES:[LX/6uW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6uW;

    return-object v0
.end method
