.class public final enum LX/6f2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6f2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6f2;

.field public static final enum API:LX/6f2;

.field public static final enum C2DM:LX/6f2;

.field public static final enum CALL_LOG:LX/6f2;

.field public static final enum MQTT:LX/6f2;

.field public static final enum SEND:LX/6f2;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1118767
    new-instance v0, LX/6f2;

    const-string v1, "API"

    invoke-direct {v0, v1, v2}, LX/6f2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6f2;->API:LX/6f2;

    .line 1118768
    new-instance v0, LX/6f2;

    const-string v1, "CALL_LOG"

    invoke-direct {v0, v1, v3}, LX/6f2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6f2;->CALL_LOG:LX/6f2;

    .line 1118769
    new-instance v0, LX/6f2;

    const-string v1, "C2DM"

    invoke-direct {v0, v1, v4}, LX/6f2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6f2;->C2DM:LX/6f2;

    .line 1118770
    new-instance v0, LX/6f2;

    const-string v1, "MQTT"

    invoke-direct {v0, v1, v5}, LX/6f2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6f2;->MQTT:LX/6f2;

    .line 1118771
    new-instance v0, LX/6f2;

    const-string v1, "SEND"

    invoke-direct {v0, v1, v6}, LX/6f2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6f2;->SEND:LX/6f2;

    .line 1118772
    const/4 v0, 0x5

    new-array v0, v0, [LX/6f2;

    sget-object v1, LX/6f2;->API:LX/6f2;

    aput-object v1, v0, v2

    sget-object v1, LX/6f2;->CALL_LOG:LX/6f2;

    aput-object v1, v0, v3

    sget-object v1, LX/6f2;->C2DM:LX/6f2;

    aput-object v1, v0, v4

    sget-object v1, LX/6f2;->MQTT:LX/6f2;

    aput-object v1, v0, v5

    sget-object v1, LX/6f2;->SEND:LX/6f2;

    aput-object v1, v0, v6

    sput-object v0, LX/6f2;->$VALUES:[LX/6f2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1118766
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6f2;
    .locals 1

    .prologue
    .line 1118764
    const-class v0, LX/6f2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6f2;

    return-object v0
.end method

.method public static values()[LX/6f2;
    .locals 1

    .prologue
    .line 1118765
    sget-object v0, LX/6f2;->$VALUES:[LX/6f2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6f2;

    return-object v0
.end method
