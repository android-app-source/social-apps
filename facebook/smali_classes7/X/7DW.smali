.class public final LX/7DW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:LX/7DX;


# direct methods
.method public constructor <init>(LX/7DX;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1183022
    iput-object p1, p0, LX/7DW;->b:LX/7DX;

    iput-object p2, p0, LX/7DW;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1183023
    iget-object v0, p0, LX/7DW;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7D7;

    .line 1183024
    iget-object v2, p0, LX/7DW;->b:LX/7DX;

    iget-object v2, v2, LX/7DX;->a:LX/7Da;

    iget-object v2, v2, LX/7Da;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7DZ;

    sget-object v2, LX/7DY;->URI_IN_QUEUE:LX/7DY;

    .line 1183025
    iput-object v2, v0, LX/7DZ;->a:LX/7DY;

    .line 1183026
    goto :goto_0

    .line 1183027
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1183028
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 1183029
    if-eqz p1, :cond_1

    .line 1183030
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1183031
    check-cast v0, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    .line 1183032
    :goto_0
    if-ge v1, v4, :cond_1

    .line 1183033
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$TilesModel;

    .line 1183034
    new-instance v5, LX/7D7;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$TilesModel;->k()I

    move-result v6

    invoke-virtual {v0}, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$TilesModel;->j()I

    move-result v7

    invoke-virtual {v0}, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$TilesModel;->a()I

    move-result v8

    invoke-virtual {v0}, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$TilesModel;->l()I

    move-result v9

    invoke-direct {v5, v6, v7, v8, v9}, LX/7D7;-><init>(IIII)V

    .line 1183035
    invoke-virtual {v0}, Lcom/facebook/api/graphql/spherical/FetchPhotoEncodingGraphQLModels$FetchPhotoEncodingGraphQLModel$TilesModel;->m()Ljava/lang/String;

    move-result-object v6

    .line 1183036
    iget-object v0, p0, LX/7DW;->b:LX/7DX;

    iget-object v0, v0, LX/7DX;->a:LX/7Da;

    iget-object v0, v0, LX/7Da;->a:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7DZ;

    .line 1183037
    if-nez v0, :cond_0

    .line 1183038
    new-instance v0, LX/7DZ;

    invoke-direct {v0}, LX/7DZ;-><init>()V

    .line 1183039
    iget-object v7, p0, LX/7DW;->b:LX/7DX;

    iget-object v7, v7, LX/7DX;->a:LX/7Da;

    iget-object v7, v7, LX/7Da;->a:Ljava/util/Map;

    invoke-interface {v7, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1183040
    :cond_0
    sget-object v7, LX/7DY;->URI_READY:LX/7DY;

    .line 1183041
    iput-object v7, v0, LX/7DZ;->a:LX/7DY;

    .line 1183042
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 1183043
    iput-object v6, v0, LX/7DZ;->b:Landroid/net/Uri;

    .line 1183044
    iget-object v0, p0, LX/7DW;->b:LX/7DX;

    iget-object v0, v0, LX/7DX;->a:LX/7Da;

    invoke-static {v0, v5}, LX/7Da;->c(LX/7Da;LX/7D7;)V

    .line 1183045
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1183046
    :cond_1
    return-void
.end method
