.class public final LX/70b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/70U;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/70U;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1162464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1162465
    iput-object p1, p0, LX/70b;->a:LX/0QB;

    .line 1162466
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1162467
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/70b;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1162468
    packed-switch p2, :pswitch_data_0

    .line 1162469
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1162470
    :pswitch_0
    new-instance v0, LX/J0J;

    invoke-direct {v0}, LX/J0J;-><init>()V

    .line 1162471
    move-object v0, v0

    .line 1162472
    move-object v0, v0

    .line 1162473
    :goto_0
    return-object v0

    .line 1162474
    :pswitch_1
    new-instance v0, LX/J0K;

    invoke-direct {v0}, LX/J0K;-><init>()V

    .line 1162475
    move-object v0, v0

    .line 1162476
    move-object v0, v0

    .line 1162477
    goto :goto_0

    .line 1162478
    :pswitch_2
    new-instance v0, LX/J0L;

    invoke-direct {v0}, LX/J0L;-><init>()V

    .line 1162479
    move-object v0, v0

    .line 1162480
    move-object v0, v0

    .line 1162481
    goto :goto_0

    .line 1162482
    :pswitch_3
    new-instance v0, LX/70V;

    invoke-direct {v0}, LX/70V;-><init>()V

    .line 1162483
    move-object v0, v0

    .line 1162484
    move-object v0, v0

    .line 1162485
    goto :goto_0

    .line 1162486
    :pswitch_4
    new-instance v0, LX/70W;

    invoke-direct {v0}, LX/70W;-><init>()V

    .line 1162487
    move-object v0, v0

    .line 1162488
    move-object v0, v0

    .line 1162489
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1162490
    const/4 v0, 0x5

    return v0
.end method
