.class public final enum LX/74F;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/74F;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/74F;

.field public static final enum ADD_TAG_SUCCESS:LX/74F;

.field public static final enum CLOSE_GALLERY_COMPOSER:LX/74F;

.field public static final enum CLOSE_GALLERY_PICKER:LX/74F;

.field public static final enum ENTER_TAGGING_MODE:LX/74F;

.field public static final enum EXIT_TAGGING_MODE:LX/74F;

.field public static final enum LAUNCH_FRIEND_TAGGER:LX/74F;

.field public static final enum LAUNCH_GALLERY:LX/74F;

.field public static final enum NO_FACE_PHOTO_TAG:LX/74F;

.field public static final enum REMOVE_TAG:LX/74F;

.field public static final enum START_TYPING:LX/74F;

.field public static final enum SWIPE_PHOTOS:LX/74F;

.field public static final enum TAG_ON_FACE_BOX:LX/74F;

.field public static final enum TYPE_AHEAD_OPEN:LX/74F;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1167689
    new-instance v0, LX/74F;

    const-string v1, "LAUNCH_GALLERY"

    const-string v2, "photo_picker_gallery_view_photo"

    invoke-direct {v0, v1, v4, v2}, LX/74F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74F;->LAUNCH_GALLERY:LX/74F;

    .line 1167690
    new-instance v0, LX/74F;

    const-string v1, "CLOSE_GALLERY_PICKER"

    const-string v2, "photo_picker_gallery_return_to_grid"

    invoke-direct {v0, v1, v5, v2}, LX/74F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74F;->CLOSE_GALLERY_PICKER:LX/74F;

    .line 1167691
    new-instance v0, LX/74F;

    const-string v1, "CLOSE_GALLERY_COMPOSER"

    const-string v2, "photo_picker_gallery_return_to_composer"

    invoke-direct {v0, v1, v6, v2}, LX/74F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74F;->CLOSE_GALLERY_COMPOSER:LX/74F;

    .line 1167692
    new-instance v0, LX/74F;

    const-string v1, "ENTER_TAGGING_MODE"

    const-string v2, "intent_photo_tag"

    invoke-direct {v0, v1, v7, v2}, LX/74F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74F;->ENTER_TAGGING_MODE:LX/74F;

    .line 1167693
    new-instance v0, LX/74F;

    const-string v1, "EXIT_TAGGING_MODE"

    const-string v2, "photo_picker_gallery_exiting_tagging_mode"

    invoke-direct {v0, v1, v8, v2}, LX/74F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74F;->EXIT_TAGGING_MODE:LX/74F;

    .line 1167694
    new-instance v0, LX/74F;

    const-string v1, "TAG_ON_FACE_BOX"

    const/4 v2, 0x5

    const-string v3, "found_face_photo_tag"

    invoke-direct {v0, v1, v2, v3}, LX/74F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74F;->TAG_ON_FACE_BOX:LX/74F;

    .line 1167695
    new-instance v0, LX/74F;

    const-string v1, "TYPE_AHEAD_OPEN"

    const/4 v2, 0x6

    const-string v3, "photo_picker_type_ahead_open"

    invoke-direct {v0, v1, v2, v3}, LX/74F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74F;->TYPE_AHEAD_OPEN:LX/74F;

    .line 1167696
    new-instance v0, LX/74F;

    const-string v1, "NO_FACE_PHOTO_TAG"

    const/4 v2, 0x7

    const-string v3, "no_face_photo_tag"

    invoke-direct {v0, v1, v2, v3}, LX/74F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74F;->NO_FACE_PHOTO_TAG:LX/74F;

    .line 1167697
    new-instance v0, LX/74F;

    const-string v1, "ADD_TAG_SUCCESS"

    const/16 v2, 0x8

    const-string v3, "add_photo_tag"

    invoke-direct {v0, v1, v2, v3}, LX/74F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74F;->ADD_TAG_SUCCESS:LX/74F;

    .line 1167698
    new-instance v0, LX/74F;

    const-string v1, "REMOVE_TAG"

    const/16 v2, 0x9

    const-string v3, "photo_picker_gallery_remove_tag"

    invoke-direct {v0, v1, v2, v3}, LX/74F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74F;->REMOVE_TAG:LX/74F;

    .line 1167699
    new-instance v0, LX/74F;

    const-string v1, "START_TYPING"

    const/16 v2, 0xa

    const-string v3, "photo_picker_start_typing"

    invoke-direct {v0, v1, v2, v3}, LX/74F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74F;->START_TYPING:LX/74F;

    .line 1167700
    new-instance v0, LX/74F;

    const-string v1, "LAUNCH_FRIEND_TAGGER"

    const/16 v2, 0xb

    const-string v3, "composer_friend_tagger"

    invoke-direct {v0, v1, v2, v3}, LX/74F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74F;->LAUNCH_FRIEND_TAGGER:LX/74F;

    .line 1167701
    new-instance v0, LX/74F;

    const-string v1, "SWIPE_PHOTOS"

    const/16 v2, 0xc

    const-string v3, "photo_picker_galley_swipe_photos"

    invoke-direct {v0, v1, v2, v3}, LX/74F;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74F;->SWIPE_PHOTOS:LX/74F;

    .line 1167702
    const/16 v0, 0xd

    new-array v0, v0, [LX/74F;

    sget-object v1, LX/74F;->LAUNCH_GALLERY:LX/74F;

    aput-object v1, v0, v4

    sget-object v1, LX/74F;->CLOSE_GALLERY_PICKER:LX/74F;

    aput-object v1, v0, v5

    sget-object v1, LX/74F;->CLOSE_GALLERY_COMPOSER:LX/74F;

    aput-object v1, v0, v6

    sget-object v1, LX/74F;->ENTER_TAGGING_MODE:LX/74F;

    aput-object v1, v0, v7

    sget-object v1, LX/74F;->EXIT_TAGGING_MODE:LX/74F;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/74F;->TAG_ON_FACE_BOX:LX/74F;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/74F;->TYPE_AHEAD_OPEN:LX/74F;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/74F;->NO_FACE_PHOTO_TAG:LX/74F;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/74F;->ADD_TAG_SUCCESS:LX/74F;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/74F;->REMOVE_TAG:LX/74F;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/74F;->START_TYPING:LX/74F;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/74F;->LAUNCH_FRIEND_TAGGER:LX/74F;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/74F;->SWIPE_PHOTOS:LX/74F;

    aput-object v2, v0, v1

    sput-object v0, LX/74F;->$VALUES:[LX/74F;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1167703
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1167704
    iput-object p3, p0, LX/74F;->name:Ljava/lang/String;

    .line 1167705
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/74F;
    .locals 1

    .prologue
    .line 1167706
    const-class v0, LX/74F;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/74F;

    return-object v0
.end method

.method public static values()[LX/74F;
    .locals 1

    .prologue
    .line 1167707
    sget-object v0, LX/74F;->$VALUES:[LX/74F;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/74F;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1167708
    iget-object v0, p0, LX/74F;->name:Ljava/lang/String;

    return-object v0
.end method
