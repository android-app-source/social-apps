.class public final enum LX/6pK;
.super LX/6pJ;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1149245
    invoke-direct {p0, p1, p2}, LX/6pJ;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final getFragment(LX/6pM;Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;Landroid/content/res/Resources;I)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 1149246
    iget-object v0, p2, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1149247
    invoke-static {v0, p3, p1}, LX/6pJ;->getHeaderText(Ljava/lang/String;Landroid/content/res/Resources;LX/6pM;)Ljava/lang/String;

    move-result-object v0

    .line 1149248
    iget v1, p2, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->g:F

    move v1, v1

    .line 1149249
    iget-boolean v2, p1, LX/6pM;->mShowForgotLink:Z

    .line 1149250
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 1149251
    const-string p1, "savedHeaderText"

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1149252
    const-string p1, "savedHeaderTextSizePx"

    invoke-virtual {p0, p1, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 1149253
    const-string p1, "savedTag"

    invoke-virtual {p0, p1, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1149254
    const-string p1, "forgetLink"

    invoke-virtual {p0, p1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1149255
    new-instance p1, Lcom/facebook/payments/auth/pin/EnterPinFragment;

    invoke-direct {p1}, Lcom/facebook/payments/auth/pin/EnterPinFragment;-><init>()V

    .line 1149256
    invoke-virtual {p1, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1149257
    move-object v0, p1

    .line 1149258
    return-object v0
.end method
