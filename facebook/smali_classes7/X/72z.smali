.class public LX/72z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6wI",
        "<",
        "Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerRunTimeData;",
        "LX/72x;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1165037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1165038
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;LX/0Px;)LX/0Px;
    .locals 11

    .prologue
    .line 1165039
    check-cast p1, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerRunTimeData;

    .line 1165040
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1165041
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/72x;

    .line 1165042
    sget-object v4, LX/72y;->a:[I

    invoke-virtual {v0}, LX/72x;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1165043
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled section type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1165044
    :pswitch_0
    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;

    .line 1165045
    iget-object v5, v0, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;->b:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v4, v0

    :goto_1
    if-ge v4, v6, :cond_0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/ShippingOption;

    .line 1165046
    new-instance v7, LX/72s;

    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->f()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-result-object v8

    iget-object v8, v8, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingOption;->b()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingOption;->a()Ljava/lang/String;

    move-result-object v10

    sget-object p0, LX/72x;->SHIPPING_OPTIONS:LX/72x;

    invoke-virtual {p1, p0}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a(LX/6vZ;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingOption;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v8, v9, v10, v0}, LX/72s;-><init>(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v2, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1165047
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 1165048
    :cond_0
    invoke-static {v2}, LX/714;->a(LX/0Pz;)V

    .line 1165049
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1165050
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
