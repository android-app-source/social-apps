.class public LX/7V4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7V5;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/7Ux;

.field private final c:LX/7V8;

.field public final d:LX/0w3;

.field private final e:LX/7V9;

.field private final f:LX/0Sh;

.field private final g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field public h:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/7WE;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Landroid/widget/PopupWindow;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z


# direct methods
.method public constructor <init>(LX/0Ot;LX/7Ux;LX/7V8;LX/0w3;LX/7V9;LX/0Sh;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7V5;",
            ">;",
            "LX/7Ux;",
            "LX/7V8;",
            "LX/0w3;",
            "LX/7V9;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1213997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1213998
    iput-object p1, p0, LX/7V4;->a:LX/0Ot;

    .line 1213999
    iput-object p2, p0, LX/7V4;->b:LX/7Ux;

    .line 1214000
    iput-object p3, p0, LX/7V4;->c:LX/7V8;

    .line 1214001
    iput-object p4, p0, LX/7V4;->d:LX/0w3;

    .line 1214002
    iput-object p5, p0, LX/7V4;->e:LX/7V9;

    .line 1214003
    iput-object p6, p0, LX/7V4;->f:LX/0Sh;

    .line 1214004
    new-instance v0, LX/7V3;

    invoke-direct {v0, p0}, LX/7V3;-><init>(LX/7V4;)V

    iput-object v0, p0, LX/7V4;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1214005
    return-void
.end method

.method public static d(LX/7V4;)[I
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1214006
    iget-object v0, p0, LX/7V4;->h:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1214007
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    .line 1214008
    :goto_0
    return-object v0

    .line 1214009
    :cond_0
    new-array v0, v1, [I

    .line 1214010
    iget-object v1, p0, LX/7V4;->h:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method
