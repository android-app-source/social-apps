.class public final LX/8Dd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1312864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)LX/8Dd;
    .locals 0

    .prologue
    .line 1312865
    iput p1, p0, LX/8Dd;->a:I

    .line 1312866
    return-object p0
.end method

.method public final a(LX/0Px;)LX/8Dd;
    .locals 0
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;",
            ">;)",
            "LX/8Dd;"
        }
    .end annotation

    .prologue
    .line 1312867
    iput-object p1, p0, LX/8Dd;->b:LX/0Px;

    .line 1312868
    return-object p0
.end method

.method public final a()Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 1312869
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1312870
    iget-object v1, p0, LX/8Dd;->b:LX/0Px;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1312871
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1312872
    iget v3, p0, LX/8Dd;->a:I

    invoke-virtual {v0, v5, v3, v5}, LX/186;->a(III)V

    .line 1312873
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1312874
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1312875
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1312876
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1312877
    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1312878
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1312879
    new-instance v1, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel;

    invoke-direct {v1, v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel;-><init>(LX/15i;)V

    .line 1312880
    return-object v1
.end method
