.class public LX/78A;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/13N;

.field public final b:LX/13P;

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/0SG;

.field private final e:LX/2mJ;

.field public final f:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public final g:Ljava/lang/String;

.field public final h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

.field public final i:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# direct methods
.method public constructor <init>(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/interstitial/manager/InterstitialTrigger;LX/13N;LX/13P;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;LX/2mJ;)V
    .locals 0
    .param p1    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/interstitial/manager/InterstitialTrigger;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1172465
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1172466
    iput-object p1, p0, LX/78A;->f:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 1172467
    iput-object p2, p0, LX/78A;->g:Ljava/lang/String;

    .line 1172468
    iput-object p3, p0, LX/78A;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    .line 1172469
    iput-object p4, p0, LX/78A;->i:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1172470
    iput-object p5, p0, LX/78A;->a:LX/13N;

    .line 1172471
    iput-object p6, p0, LX/78A;->b:LX/13P;

    .line 1172472
    iput-object p7, p0, LX/78A;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1172473
    iput-object p8, p0, LX/78A;->d:LX/0SG;

    .line 1172474
    iput-object p9, p0, LX/78A;->e:LX/2mJ;

    .line 1172475
    return-void
.end method

.method public static a(LX/78A;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)V
    .locals 2

    .prologue
    .line 1172449
    invoke-static {p1}, LX/78A;->c(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1172450
    iget-object v0, p0, LX/78A;->i:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/78A;->i:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    iget-object v0, v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;->a:Lcom/facebook/interstitial/manager/InterstitialTriggerContext;

    .line 1172451
    :goto_0
    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->url:Ljava/lang/String;

    invoke-static {v1, v0}, LX/6Xz;->a(Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTriggerContext;)Ljava/lang/String;

    move-result-object v0

    .line 1172452
    iget-object v1, p0, LX/78A;->e:LX/2mJ;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2mJ;->a(Landroid/net/Uri;)V

    .line 1172453
    :cond_0
    return-void

    .line 1172454
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;LX/77m;LX/2fy;)V
    .locals 6

    .prologue
    .line 1172455
    invoke-static {p1}, LX/78A;->c(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1172456
    iget-object v0, p0, LX/78A;->i:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/78A;->i:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    iget-object v0, v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;->a:Lcom/facebook/interstitial/manager/InterstitialTriggerContext;

    .line 1172457
    :goto_0
    iget-object v1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->url:Ljava/lang/String;

    invoke-static {v1, v0}, LX/6Xz;->a(Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTriggerContext;)Ljava/lang/String;

    move-result-object v0

    .line 1172458
    iget-object v1, p0, LX/78A;->e:LX/2mJ;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2mJ;->a(Landroid/net/Uri;)V

    .line 1172459
    :cond_0
    iget-object v0, p0, LX/78A;->b:LX/13P;

    iget-object v3, p0, LX/78A;->f:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v4, p0, LX/78A;->g:Ljava/lang/String;

    iget-object v5, p0, LX/78A;->i:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, LX/13P;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;LX/77m;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 1172460
    iget-object v0, p0, LX/78A;->a:LX/13N;

    iget-object v1, p0, LX/78A;->f:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0, v1, p3}, LX/13N;->e(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)V

    .line 1172461
    invoke-static {p1}, LX/78A;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1172462
    invoke-static {p0}, LX/78A;->k(LX/78A;)V

    .line 1172463
    :cond_1
    return-void

    .line 1172464
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)Z
    .locals 1
    .param p0    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1172480
    if-eqz p0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->dismissPromotion:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)Z
    .locals 1
    .param p0    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1172476
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(LX/78A;)V
    .locals 4

    .prologue
    .line 1172477
    iget-object v0, p0, LX/78A;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, LX/78A;->g:Ljava/lang/String;

    invoke-static {v1}, LX/2fw;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    iget-object v2, p0, LX/78A;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1172478
    iget-object v0, p0, LX/78A;->a:LX/13N;

    iget-object v1, p0, LX/78A;->f:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    sget-object v2, LX/2fy;->DISMISSAL:LX/2fy;

    invoke-virtual {v0, v1, v2}, LX/13N;->e(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)V

    .line 1172479
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1172429
    iget-object v0, p0, LX/78A;->a:LX/13N;

    iget-object v1, p0, LX/78A;->f:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    sget-object v2, LX/2fy;->IMPRESSION:LX/2fy;

    invoke-virtual {v0, v1, v2}, LX/13N;->e(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)V

    .line 1172430
    return-void
.end method

.method public final a(LX/77n;)V
    .locals 4

    .prologue
    .line 1172431
    iget-object v0, p0, LX/78A;->b:LX/13P;

    iget-object v1, p0, LX/78A;->f:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v2, p0, LX/78A;->i:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1172432
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "view"

    invoke-direct {v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1172433
    invoke-static {v3}, LX/13P;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1172434
    invoke-static {v0, v3, v1, v2}, LX/13P;->a(LX/13P;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 1172435
    invoke-static {v0, v3}, LX/13P;->b(LX/13P;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1172436
    if-eqz p1, :cond_4

    .line 1172437
    iget-object p0, p1, LX/77n;->a:Ljava/lang/String;

    if-eqz p0, :cond_0

    .line 1172438
    const-string p0, "title_truncated"

    iget-object v1, p1, LX/77n;->a:Ljava/lang/String;

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1172439
    :cond_0
    iget-object p0, p1, LX/77n;->b:Ljava/lang/String;

    if-eqz p0, :cond_1

    .line 1172440
    const-string p0, "content_truncated"

    iget-object v1, p1, LX/77n;->b:Ljava/lang/String;

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1172441
    :cond_1
    iget-object p0, p1, LX/77n;->c:Ljava/lang/String;

    if-eqz p0, :cond_2

    .line 1172442
    const-string p0, "primary_action_truncated"

    iget-object v1, p1, LX/77n;->c:Ljava/lang/String;

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1172443
    :cond_2
    iget-object p0, p1, LX/77n;->d:Ljava/lang/String;

    if-eqz p0, :cond_3

    .line 1172444
    const-string p0, "secondary_action_truncated"

    iget-object v1, p1, LX/77n;->d:Ljava/lang/String;

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1172445
    :cond_3
    iget-object p0, p1, LX/77n;->e:Ljava/lang/String;

    if-eqz p0, :cond_4

    .line 1172446
    const-string p0, "social_context_truncated"

    iget-object v1, p1, LX/77n;->e:Ljava/lang/String;

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1172447
    :cond_4
    iget-object p0, v0, LX/13P;->a:LX/0Zb;

    invoke-interface {p0, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1172448
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1172414
    iget-object v0, p0, LX/78A;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    sget-object v1, LX/77m;->PRIMARY_ACTION:LX/77m;

    sget-object v2, LX/2fy;->PRIMARY_ACTION:LX/2fy;

    invoke-direct {p0, v0, v1, v2}, LX/78A;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;LX/77m;LX/2fy;)V

    .line 1172415
    return-void
.end method

.method public final c()V
    .locals 9

    .prologue
    .line 1172416
    iget-object v0, p0, LX/78A;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    sget-object v1, LX/77m;->PRIMARY_ACTION:LX/77m;

    sget-object v2, LX/2fy;->PRIMARY_ACTION:LX/2fy;

    .line 1172417
    iget-object v3, p0, LX/78A;->b:LX/13P;

    iget-object v6, p0, LX/78A;->f:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v7, p0, LX/78A;->g:Ljava/lang/String;

    iget-object v8, p0, LX/78A;->i:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    move-object v4, v0

    move-object v5, v1

    invoke-virtual/range {v3 .. v8}, LX/13P;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;LX/77m;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    .line 1172418
    iget-object v3, p0, LX/78A;->a:LX/13N;

    iget-object v4, p0, LX/78A;->f:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v3, v4, v2}, LX/13N;->e(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;LX/2fy;)V

    .line 1172419
    invoke-static {v0}, LX/78A;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1172420
    invoke-static {p0}, LX/78A;->k(LX/78A;)V

    .line 1172421
    :cond_0
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1172422
    iget-object v0, p0, LX/78A;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-static {v0}, LX/78A;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)Z

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 1172423
    iget-object v0, p0, LX/78A;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    sget-object v1, LX/77m;->SECONDARY_ACTION:LX/77m;

    sget-object v2, LX/2fy;->SECONDARY_ACTION:LX/2fy;

    invoke-direct {p0, v0, v1, v2}, LX/78A;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;LX/77m;LX/2fy;)V

    .line 1172424
    return-void
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1172428
    iget-object v0, p0, LX/78A;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-static {v0}, LX/78A;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)Z

    move-result v0

    return v0
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 1172425
    iget-object v0, p0, LX/78A;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    sget-object v1, LX/77m;->DISMISS_ACTION:LX/77m;

    sget-object v2, LX/2fy;->DISMISS_ACTION:LX/2fy;

    invoke-direct {p0, v0, v1, v2}, LX/78A;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;LX/77m;LX/2fy;)V

    .line 1172426
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 1172427
    iget-object v0, p0, LX/78A;->h:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-static {v0}, LX/78A;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)Z

    move-result v0

    return v0
.end method
