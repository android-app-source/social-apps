.class public LX/71Y;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/facebook/payments/selector/model/SelectorRow;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/71f;

.field public b:LX/6qh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/71f;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1163435
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 1163436
    iput-object p2, p0, LX/71Y;->a:LX/71f;

    .line 1163437
    return-void
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 1163438
    const/4 v0, 0x0

    return v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1163439
    invoke-virtual {p0, p1}, LX/71Y;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/selector/model/SelectorRow;

    invoke-interface {v0}, Lcom/facebook/payments/selector/model/SelectorRow;->a()LX/71l;

    move-result-object v0

    invoke-virtual {v0}, LX/71l;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1163440
    iget-object v1, p0, LX/71Y;->a:LX/71f;

    iget-object v2, p0, LX/71Y;->b:LX/6qh;

    invoke-virtual {p0, p1}, LX/71Y;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/selector/model/SelectorRow;

    invoke-virtual {v1, v2, v0, p2, p3}, LX/71f;->a(LX/6qh;Lcom/facebook/payments/selector/model/SelectorRow;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1163441
    invoke-static {}, LX/71l;->values()[LX/71l;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 1163442
    invoke-virtual {p0, p1}, LX/71Y;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/selector/model/SelectorRow;

    invoke-interface {v0}, Lcom/facebook/payments/selector/model/SelectorRow;->a()LX/71l;

    move-result-object v0

    invoke-virtual {v0}, LX/71l;->isSelectable()Z

    move-result v0

    return v0
.end method
