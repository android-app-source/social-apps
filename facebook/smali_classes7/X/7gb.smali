.class public final enum LX/7gb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7gb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7gb;

.field public static final enum INSPIRATION_GROUP_ID:LX/7gb;

.field public static final enum PROMPT_ID:LX/7gb;

.field public static final enum SHARESHEET_SESSION_ID:LX/7gb;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1224600
    new-instance v0, LX/7gb;

    const-string v1, "PROMPT_ID"

    const-string v2, "prompt_id"

    invoke-direct {v0, v1, v3, v2}, LX/7gb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gb;->PROMPT_ID:LX/7gb;

    .line 1224601
    new-instance v0, LX/7gb;

    const-string v1, "SHARESHEET_SESSION_ID"

    const-string v2, "sharesheet_session_id"

    invoke-direct {v0, v1, v4, v2}, LX/7gb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gb;->SHARESHEET_SESSION_ID:LX/7gb;

    .line 1224602
    new-instance v0, LX/7gb;

    const-string v1, "INSPIRATION_GROUP_ID"

    const-string v2, "inspiration_group_id"

    invoke-direct {v0, v1, v5, v2}, LX/7gb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7gb;->INSPIRATION_GROUP_ID:LX/7gb;

    .line 1224603
    const/4 v0, 0x3

    new-array v0, v0, [LX/7gb;

    sget-object v1, LX/7gb;->PROMPT_ID:LX/7gb;

    aput-object v1, v0, v3

    sget-object v1, LX/7gb;->SHARESHEET_SESSION_ID:LX/7gb;

    aput-object v1, v0, v4

    sget-object v1, LX/7gb;->INSPIRATION_GROUP_ID:LX/7gb;

    aput-object v1, v0, v5

    sput-object v0, LX/7gb;->$VALUES:[LX/7gb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1224597
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1224598
    iput-object p3, p0, LX/7gb;->mName:Ljava/lang/String;

    .line 1224599
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7gb;
    .locals 1

    .prologue
    .line 1224596
    const-class v0, LX/7gb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7gb;

    return-object v0
.end method

.method public static values()[LX/7gb;
    .locals 1

    .prologue
    .line 1224594
    sget-object v0, LX/7gb;->$VALUES:[LX/7gb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7gb;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1224595
    iget-object v0, p0, LX/7gb;->mName:Ljava/lang/String;

    return-object v0
.end method
