.class public LX/7OF;
.super LX/16T;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static e:LX/0hs;

.field private static volatile f:LX/7OF;


# instance fields
.field private final a:LX/3Gg;

.field public final b:LX/0iA;

.field public final c:LX/0wM;

.field public d:Z


# direct methods
.method public constructor <init>(LX/3Gg;LX/0iA;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1201242
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 1201243
    iput-object p1, p0, LX/7OF;->a:LX/3Gg;

    .line 1201244
    iput-object p2, p0, LX/7OF;->b:LX/0iA;

    .line 1201245
    iput-object p3, p0, LX/7OF;->c:LX/0wM;

    .line 1201246
    return-void
.end method

.method public static a(LX/0QB;)LX/7OF;
    .locals 6

    .prologue
    .line 1201247
    sget-object v0, LX/7OF;->f:LX/7OF;

    if-nez v0, :cond_1

    .line 1201248
    const-class v1, LX/7OF;

    monitor-enter v1

    .line 1201249
    :try_start_0
    sget-object v0, LX/7OF;->f:LX/7OF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1201250
    if-eqz v2, :cond_0

    .line 1201251
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1201252
    new-instance p0, LX/7OF;

    invoke-static {v0}, LX/3Gg;->b(LX/0QB;)LX/3Gg;

    move-result-object v3

    check-cast v3, LX/3Gg;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v4

    check-cast v4, LX/0iA;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    invoke-direct {p0, v3, v4, v5}, LX/7OF;-><init>(LX/3Gg;LX/0iA;LX/0wM;)V

    .line 1201253
    move-object v0, p0

    .line 1201254
    sput-object v0, LX/7OF;->f:LX/7OF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1201255
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1201256
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1201257
    :cond_1
    sget-object v0, LX/7OF;->f:LX/7OF;

    return-object v0

    .line 1201258
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1201259
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 2

    .prologue
    .line 1201260
    iget-boolean v0, p0, LX/7OF;->d:Z

    if-eqz v0, :cond_0

    .line 1201261
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 1201262
    :goto_0
    return-object v0

    .line 1201263
    :cond_0
    iget-object v0, p0, LX/7OF;->a:LX/3Gg;

    .line 1201264
    iget-object v1, v0, LX/3Gg;->a:LX/0ad;

    sget-short p0, LX/0ws;->dL:S

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 1201265
    if-eqz v0, :cond_1

    .line 1201266
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0

    .line 1201267
    :cond_1
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1201268
    const-string v0, "4566"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1201269
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_QUALITY_LABEL_INLINE_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
