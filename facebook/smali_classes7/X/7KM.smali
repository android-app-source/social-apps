.class public LX/7KM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2q7;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

.field private final b:LX/2q5;

.field private c:Lcom/facebook/video/engine/VideoPlayerParams;

.field private d:Ljava/lang/String;

.field private e:LX/04D;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/2p8;

.field private h:I

.field public final i:Ljava/lang/Object;

.field public j:LX/2qD;

.field public k:LX/2qD;

.field public l:LX/2oi;


# direct methods
.method public constructor <init>(Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;LX/2q5;)V
    .locals 3

    .prologue
    .line 1195635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1195636
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    iput-object v0, p0, LX/7KM;->e:LX/04D;

    .line 1195637
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7KM;->f:Ljava/util/List;

    .line 1195638
    const/4 v0, -0x1

    iput v0, p0, LX/7KM;->h:I

    .line 1195639
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/7KM;->i:Ljava/lang/Object;

    .line 1195640
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    iput-object v0, p0, LX/7KM;->j:LX/2qD;

    .line 1195641
    sget-object v0, LX/2qD;->STATE_IDLE:LX/2qD;

    iput-object v0, p0, LX/7KM;->k:LX/2qD;

    .line 1195642
    iput-object p1, p0, LX/7KM;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    .line 1195643
    iput-object p2, p0, LX/7KM;->b:LX/2q5;

    .line 1195644
    iget-object v0, p0, LX/7KM;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    new-instance v1, LX/7KL;

    invoke-direct {v1, p0}, LX/7KL;-><init>(LX/7KM;)V

    invoke-virtual {v0, v1}, LX/2q6;->a(LX/2pf;)V

    .line 1195645
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    .line 1195646
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$13;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$13;-><init>(LX/7KM;F)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195647
    return-void
.end method

.method public final a(ILX/04g;)V
    .locals 2

    .prologue
    .line 1195648
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$9;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$9;-><init>(LX/7KM;ILX/04g;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195649
    return-void
.end method

.method public final a(LX/04D;)V
    .locals 2

    .prologue
    .line 1195650
    iget-object v1, p0, LX/7KM;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 1195651
    :try_start_0
    iput-object p1, p0, LX/7KM;->e:LX/04D;

    .line 1195652
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1195653
    iget-object v0, p0, LX/7KM;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-virtual {v0, p1}, LX/2q6;->a(LX/04D;)V

    .line 1195654
    return-void

    .line 1195655
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/04G;)V
    .locals 2

    .prologue
    .line 1195656
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$17;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$17;-><init>(LX/7KM;LX/04G;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195657
    return-void
.end method

.method public final a(LX/04H;)V
    .locals 2

    .prologue
    .line 1195658
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$18;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$18;-><init>(LX/7KM;LX/04H;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195659
    return-void
.end method

.method public final a(LX/04g;)V
    .locals 2

    .prologue
    .line 1195660
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$5;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$5;-><init>(LX/7KM;LX/04g;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195661
    return-void
.end method

.method public final a(LX/04g;LX/7K4;)V
    .locals 2

    .prologue
    .line 1195662
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$6;-><init>(LX/7KM;LX/04g;LX/7K4;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195663
    return-void
.end method

.method public final a(LX/2oi;LX/04g;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1195664
    iget-object v1, p0, LX/7KM;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 1195665
    :try_start_0
    iput-object p3, p0, LX/7KM;->d:Ljava/lang/String;

    .line 1195666
    iput-object p1, p0, LX/7KM;->l:LX/2oi;

    .line 1195667
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1195668
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$14;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$14;-><init>(LX/7KM;LX/2oi;LX/04g;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195669
    return-void

    .line 1195670
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/2p8;)V
    .locals 2

    .prologue
    .line 1195671
    iget-object v1, p0, LX/7KM;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 1195672
    :try_start_0
    iput-object p1, p0, LX/7KM;->g:LX/2p8;

    .line 1195673
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1195674
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$4;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$4;-><init>(LX/7KM;LX/2p8;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195675
    return-void

    .line 1195676
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/2qG;)V
    .locals 2

    .prologue
    .line 1195679
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$26;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$26;-><init>(LX/7KM;LX/2qG;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195680
    return-void
.end method

.method public final a(LX/7K7;Ljava/lang/String;LX/04g;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1195677
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$15;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$15;-><init>(LX/7KM;LX/7K7;Ljava/lang/String;LX/04g;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195678
    return-void
.end method

.method public final a(LX/7QP;)V
    .locals 2

    .prologue
    .line 1195693
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$2;-><init>(LX/7KM;LX/7QP;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195694
    return-void
.end method

.method public final a(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 1195691
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$25;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$25;-><init>(LX/7KM;Landroid/graphics/RectF;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195692
    return-void
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;)V
    .locals 2

    .prologue
    .line 1195689
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$10;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$10;-><init>(LX/7KM;Lcom/facebook/exoplayer/ipc/DeviceOrientationFrame;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195690
    return-void
.end method

.method public final a(Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;)V
    .locals 2

    .prologue
    .line 1195687
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$11;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$11;-><init>(LX/7KM;Lcom/facebook/exoplayer/ipc/SpatialAudioFocusParams;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195688
    return-void
.end method

.method public final a(Lcom/facebook/video/engine/VideoPlayerParams;LX/2qi;Z)V
    .locals 2
    .param p2    # LX/2qi;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1195695
    iget-object v1, p0, LX/7KM;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 1195696
    :try_start_0
    iput-object p1, p0, LX/7KM;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1195697
    iget v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    iput v0, p0, LX/7KM;->h:I

    .line 1195698
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1195699
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$1;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$1;-><init>(LX/7KM;Lcom/facebook/video/engine/VideoPlayerParams;ZLX/2qi;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195700
    return-void

    .line 1195701
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(ZLX/04g;)V
    .locals 2

    .prologue
    .line 1195685
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$12;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$12;-><init>(LX/7KM;ZLX/04g;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195686
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1195684
    iget-object v0, p0, LX/7KM;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-virtual {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->a()Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1195683
    iget-object v0, p0, LX/7KM;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-virtual {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->b()I

    move-result v0

    return v0
.end method

.method public final b(LX/04g;)V
    .locals 2

    .prologue
    .line 1195681
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$7;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$7;-><init>(LX/7KM;LX/04g;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195682
    return-void
.end method

.method public final c()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1195630
    iget-object v1, p0, LX/7KM;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 1195631
    :try_start_0
    iget-object v0, p0, LX/7KM;->f:Ljava/util/List;

    monitor-exit v1

    return-object v0

    .line 1195632
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(LX/04g;)V
    .locals 2

    .prologue
    .line 1195633
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$8;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$8;-><init>(LX/7KM;LX/04g;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195634
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1195585
    iget-object v1, p0, LX/7KM;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 1195586
    :try_start_0
    iget-object v0, p0, LX/7KM;->d:Ljava/lang/String;

    monitor-exit v1

    return-object v0

    .line 1195587
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d(LX/04g;)V
    .locals 2

    .prologue
    .line 1195588
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$19;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$19;-><init>(LX/7KM;LX/04g;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195589
    return-void
.end method

.method public final e()LX/2oi;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1195590
    iget-object v1, p0, LX/7KM;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 1195591
    :try_start_0
    iget-object v0, p0, LX/7KM;->l:LX/2oi;

    monitor-exit v1

    return-object v0

    .line 1195592
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final e(LX/04g;)V
    .locals 2

    .prologue
    .line 1195593
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$24;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$24;-><init>(LX/7KM;LX/04g;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195594
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1195595
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$16;

    invoke-direct {v1, p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$16;-><init>(LX/7KM;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195596
    return-void
.end method

.method public final g()LX/04D;
    .locals 2

    .prologue
    .line 1195597
    iget-object v1, p0, LX/7KM;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 1195598
    :try_start_0
    iget-object v0, p0, LX/7KM;->e:LX/04D;

    monitor-exit v1

    return-object v0

    .line 1195599
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final h()Z
    .locals 3

    .prologue
    .line 1195600
    iget-object v1, p0, LX/7KM;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 1195601
    :try_start_0
    iget-object v0, p0, LX/7KM;->j:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PREPARING:LX/2qD;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, LX/7KM;->k:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1195602
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final i()Z
    .locals 3

    .prologue
    .line 1195603
    iget-object v1, p0, LX/7KM;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 1195604
    :try_start_0
    iget-object v0, p0, LX/7KM;->k:LX/2qD;

    sget-object v2, LX/2qD;->STATE_PLAYING:LX/2qD;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1195605
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 1195606
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$21;

    invoke-direct {v1, p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$21;-><init>(LX/7KM;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195607
    return-void
.end method

.method public final k()Landroid/view/View;
    .locals 2

    .prologue
    .line 1195608
    iget-object v1, p0, LX/7KM;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 1195609
    :try_start_0
    iget-object v0, p0, LX/7KM;->g:LX/2p8;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    iget-object v0, p0, LX/7KM;->g:LX/2p8;

    .line 1195610
    iget-object p0, v0, LX/2p8;->i:Landroid/view/TextureView;

    move-object v0, p0

    .line 1195611
    goto :goto_0

    .line 1195612
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final l()I
    .locals 2

    .prologue
    .line 1195613
    iget-object v1, p0, LX/7KM;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 1195614
    :try_start_0
    iget v0, p0, LX/7KM;->h:I

    monitor-exit v1

    return v0

    .line 1195615
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 1195616
    iget-object v0, p0, LX/7KM;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-virtual {v0}, LX/2q6;->m()I

    move-result v0

    return v0
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 1195617
    iget-object v0, p0, LX/7KM;->b:LX/2q5;

    new-instance v1, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$22;

    invoke-direct {v1, p0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerAsyncClient$22;-><init>(LX/7KM;)V

    invoke-virtual {v0, v1}, LX/2q5;->a(Ljava/lang/Runnable;)V

    .line 1195618
    return-void
.end method

.method public final o()LX/7QP;
    .locals 1

    .prologue
    .line 1195619
    iget-object v0, p0, LX/7KM;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-virtual {v0}, LX/2q6;->o()LX/7QP;

    move-result-object v0

    return-object v0
.end method

.method public final p()LX/7IE;
    .locals 1

    .prologue
    .line 1195620
    iget-object v0, p0, LX/7KM;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-virtual {v0}, LX/2q6;->p()LX/7IE;

    move-result-object v0

    return-object v0
.end method

.method public final q()LX/16V;
    .locals 1

    .prologue
    .line 1195621
    iget-object v0, p0, LX/7KM;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-virtual {v0}, LX/2q6;->q()LX/16V;

    move-result-object v0

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1195622
    iget-object v0, p0, LX/7KM;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-virtual {v0}, Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 1195623
    iget-object v0, p0, LX/7KM;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-virtual {v0}, LX/2q6;->s()I

    move-result v0

    return v0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 1195624
    iget-object v0, p0, LX/7KM;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-virtual {v0}, LX/2q6;->t()I

    move-result v0

    return v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 1195625
    iget-object v0, p0, LX/7KM;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-virtual {v0}, LX/2q6;->u()Z

    move-result v0

    return v0
.end method

.method public final v()J
    .locals 2

    .prologue
    .line 1195626
    iget-object v0, p0, LX/7KM;->a:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayerClient;

    invoke-virtual {v0}, LX/2q6;->v()J

    move-result-wide v0

    return-wide v0
.end method

.method public final w()Lcom/facebook/video/engine/VideoPlayerParams;
    .locals 2

    .prologue
    .line 1195627
    iget-object v1, p0, LX/7KM;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 1195628
    :try_start_0
    iget-object v0, p0, LX/7KM;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    monitor-exit v1

    return-object v0

    .line 1195629
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
