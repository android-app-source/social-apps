.class public final LX/8cW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;",
        ">;",
        "LX/7By;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3Qd;


# direct methods
.method public constructor <init>(LX/3Qd;)V
    .locals 0

    .prologue
    .line 1374560
    iput-object p1, p0, LX/8cW;->a:LX/3Qd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1374561
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const-wide/16 v6, 0x3e8

    const/4 v4, 0x0

    .line 1374562
    const-wide/16 v2, 0x0

    .line 1374563
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 1374564
    new-instance v9, LX/0Pz;

    invoke-direct {v9}, LX/0Pz;-><init>()V

    .line 1374565
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1374566
    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;->a()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesEdgeFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1374567
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1374568
    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;->a()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesEdgeFragmentModel;

    move-result-object v0

    .line 1374569
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesEdgeFragmentModel;->j()I

    move-result v1

    int-to-long v2, v1

    mul-long/2addr v2, v6

    .line 1374570
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesEdgeFragmentModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v5, v4

    .line 1374571
    :goto_0
    if-ge v5, v7, :cond_8

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesEdgeFragmentModel$EdgesModel;

    .line 1374572
    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesEdgeFragmentModel$EdgesModel;->a()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1374573
    :cond_0
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_BOOTSTRAP_SUGGESTION:LX/3Ql;

    const-string v10, "null edge or node"

    invoke-direct {v0, v1, v10}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch LX/7C4; {:try_start_0 .. :try_end_0} :catch_0

    .line 1374574
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1374575
    iget-object v0, p0, LX/8cW;->a:LX/3Qd;

    iget-object v0, v0, LX/3Qd;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sc;

    invoke-virtual {v0, v1}, LX/2Sc;->a(LX/7C4;)V

    .line 1374576
    :goto_1
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 1374577
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/8cW;->a:LX/3Qd;

    iget-object v1, v1, LX/3Qd;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8cA;

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesEdgeFragmentModel$EdgesModel;->a()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8cA;->a(LX/8bp;)LX/7C0;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catch LX/7C4; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1374578
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1374579
    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;->j()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1374580
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1374581
    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;->j()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel;

    move-result-object v0

    .line 1374582
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel;->k()I

    move-result v1

    int-to-long v2, v1

    mul-long/2addr v6, v2

    .line 1374583
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel;->j()Z

    move-result v2

    .line 1374584
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    .line 1374585
    :goto_2
    if-ge v4, v5, :cond_7

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel;

    .line 1374586
    if-eqz v0, :cond_3

    :try_start_2
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel;->a()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;

    move-result-object v1

    if-nez v1, :cond_5

    .line 1374587
    :cond_3
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_BOOTSTRAP_SUGGESTION:LX/3Ql;

    const-string v10, "null edge or node"

    invoke-direct {v0, v1, v10}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch LX/7C4; {:try_start_2 .. :try_end_2} :catch_1

    .line 1374588
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 1374589
    iget-object v0, p0, LX/8cW;->a:LX/3Qd;

    iget-object v0, v0, LX/3Qd;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sc;

    invoke-virtual {v0, v1}, LX/2Sc;->a(LX/7C4;)V

    .line 1374590
    :cond_4
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1374591
    :cond_5
    :try_start_3
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel;->a()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->j()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1374592
    iget-object v1, p0, LX/8cW;->a:LX/3Qd;

    iget-object v1, v1, LX/3Qd;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8cA;

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel;->a()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8cA;->a(LX/8bp;)LX/7C0;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 1374593
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel;->a()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel;->a()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1374594
    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel;->a()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_3
    .catch LX/7C4; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3

    :cond_7
    move v4, v2

    move-wide v2, v6

    .line 1374595
    :cond_8
    new-instance v1, LX/7By;

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, LX/7By;-><init>(JZLX/0Px;LX/0Px;)V

    return-object v1
.end method
