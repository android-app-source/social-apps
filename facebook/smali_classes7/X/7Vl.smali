.class public final LX/7Vl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/zero/sdk/request/ZeroOptinResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V
    .locals 0

    .prologue
    .line 1215228
    iput-object p1, p0, LX/7Vl;->a:Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/zero/sdk/request/ZeroOptinResult;)V
    .locals 2

    .prologue
    .line 1215229
    invoke-virtual {p1}, Lcom/facebook/zero/sdk/request/ZeroOptinResult;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "optin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1215230
    iget-object v0, p0, LX/7Vl;->a:Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;

    invoke-static {v0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->t(Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;)V

    .line 1215231
    :cond_0
    iget-object v0, p0, LX/7Vl;->a:Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;

    invoke-virtual {v0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->finish()V

    .line 1215232
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1215233
    iget-object v0, p0, LX/7Vl;->a:Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;

    invoke-virtual {v0}, Lcom/facebook/zero/activity/NativeOptinInterstitialActivity;->finish()V

    .line 1215234
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1215235
    check-cast p1, Lcom/facebook/zero/sdk/request/ZeroOptinResult;

    invoke-direct {p0, p1}, LX/7Vl;->a(Lcom/facebook/zero/sdk/request/ZeroOptinResult;)V

    return-void
.end method
