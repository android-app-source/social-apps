.class public LX/6jd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final callId:Ljava/lang/String;

.field public final isVoicemail:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1131662
    new-instance v0, LX/1sv;

    const-string v1, "AudioMetadata"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jd;->b:LX/1sv;

    .line 1131663
    new-instance v0, LX/1sw;

    const-string v1, "isVoicemail"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jd;->c:LX/1sw;

    .line 1131664
    new-instance v0, LX/1sw;

    const-string v1, "callId"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jd;->d:LX/1sw;

    .line 1131665
    sput-boolean v3, LX/6jd;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1131658
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1131659
    iput-object p1, p0, LX/6jd;->isVoicemail:Ljava/lang/Boolean;

    .line 1131660
    iput-object p2, p0, LX/6jd;->callId:Ljava/lang/String;

    .line 1131661
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1131626
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1131627
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1131628
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1131629
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "AudioMetadata"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1131630
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131631
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131632
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131633
    const/4 v1, 0x1

    .line 1131634
    iget-object v5, p0, LX/6jd;->isVoicemail:Ljava/lang/Boolean;

    if-eqz v5, :cond_0

    .line 1131635
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131636
    const-string v1, "isVoicemail"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131637
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131638
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131639
    iget-object v1, p0, LX/6jd;->isVoicemail:Ljava/lang/Boolean;

    if-nez v1, :cond_6

    .line 1131640
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131641
    :goto_3
    const/4 v1, 0x0

    .line 1131642
    :cond_0
    iget-object v5, p0, LX/6jd;->callId:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 1131643
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131644
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131645
    const-string v1, "callId"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131646
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131647
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131648
    iget-object v0, p0, LX/6jd;->callId:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 1131649
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131650
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131651
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131652
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1131653
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1131654
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1131655
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1131656
    :cond_6
    iget-object v1, p0, LX/6jd;->isVoicemail:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1131657
    :cond_7
    iget-object v0, p0, LX/6jd;->callId:Ljava/lang/String;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1131666
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1131667
    iget-object v0, p0, LX/6jd;->isVoicemail:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1131668
    iget-object v0, p0, LX/6jd;->isVoicemail:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1131669
    sget-object v0, LX/6jd;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131670
    iget-object v0, p0, LX/6jd;->isVoicemail:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1131671
    :cond_0
    iget-object v0, p0, LX/6jd;->callId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1131672
    iget-object v0, p0, LX/6jd;->callId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1131673
    sget-object v0, LX/6jd;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131674
    iget-object v0, p0, LX/6jd;->callId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1131675
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1131676
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1131677
    return-void
.end method

.method public final a(LX/6jd;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1131609
    if-nez p1, :cond_1

    .line 1131610
    :cond_0
    :goto_0
    return v2

    .line 1131611
    :cond_1
    iget-object v0, p0, LX/6jd;->isVoicemail:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    move v0, v1

    .line 1131612
    :goto_1
    iget-object v3, p1, LX/6jd;->isVoicemail:Ljava/lang/Boolean;

    if-eqz v3, :cond_7

    move v3, v1

    .line 1131613
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 1131614
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131615
    iget-object v0, p0, LX/6jd;->isVoicemail:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6jd;->isVoicemail:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131616
    :cond_3
    iget-object v0, p0, LX/6jd;->callId:Ljava/lang/String;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1131617
    :goto_3
    iget-object v3, p1, LX/6jd;->callId:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1131618
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1131619
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131620
    iget-object v0, p0, LX/6jd;->callId:Ljava/lang/String;

    iget-object v3, p1, LX/6jd;->callId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_5
    move v2, v1

    .line 1131621
    goto :goto_0

    :cond_6
    move v0, v2

    .line 1131622
    goto :goto_1

    :cond_7
    move v3, v2

    .line 1131623
    goto :goto_2

    :cond_8
    move v0, v2

    .line 1131624
    goto :goto_3

    :cond_9
    move v3, v2

    .line 1131625
    goto :goto_4
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1131605
    if-nez p1, :cond_1

    .line 1131606
    :cond_0
    :goto_0
    return v0

    .line 1131607
    :cond_1
    instance-of v1, p1, LX/6jd;

    if-eqz v1, :cond_0

    .line 1131608
    check-cast p1, LX/6jd;

    invoke-virtual {p0, p1}, LX/6jd;->a(LX/6jd;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1131604
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1131601
    sget-boolean v0, LX/6jd;->a:Z

    .line 1131602
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jd;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1131603
    return-object v0
.end method
