.class public final LX/7CH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1180351
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1180352
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1180353
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1180354
    const/4 v9, 0x1

    const-wide/16 v7, 0x0

    const/4 v4, 0x0

    .line 1180355
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_6

    .line 1180356
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1180357
    :goto_1
    move v1, v4

    .line 1180358
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1180359
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1180360
    :cond_1
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_4

    .line 1180361
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1180362
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1180363
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1180364
    const-string v12, "grammar_cost"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1180365
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v5

    move v3, v9

    goto :goto_2

    .line 1180366
    :cond_2
    const-string v12, "grammar_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1180367
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_2

    .line 1180368
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 1180369
    :cond_4
    const/4 v11, 0x2

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1180370
    if-eqz v3, :cond_5

    move-object v3, p1

    .line 1180371
    invoke-virtual/range {v3 .. v8}, LX/186;->a(IDD)V

    .line 1180372
    :cond_5
    invoke-virtual {p1, v9, v10}, LX/186;->b(II)V

    .line 1180373
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_1

    :cond_6
    move v3, v4

    move v10, v4

    move-wide v5, v7

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1180374
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1180375
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1180376
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const-wide/16 v4, 0x0

    .line 1180377
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1180378
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1180379
    cmpl-double v4, v2, v4

    if-eqz v4, :cond_0

    .line 1180380
    const-string v4, "grammar_cost"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1180381
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 1180382
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1180383
    if-eqz v2, :cond_1

    .line 1180384
    const-string v3, "grammar_type"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1180385
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1180386
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1180387
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1180388
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1180389
    return-void
.end method
