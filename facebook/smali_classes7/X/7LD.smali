.class public final LX/7LD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/FullScreenVideoPlayer;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/394;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V
    .locals 1

    .prologue
    .line 1196868
    iput-object p1, p0, LX/7LD;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1196869
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, LX/7LD;->b:Ljava/util/Set;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/player/FullScreenVideoPlayer;B)V
    .locals 0

    .prologue
    .line 1196870
    invoke-direct {p0, p1}, LX/7LD;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    return-void
.end method


# virtual methods
.method public final a(LX/04g;LX/7Jv;)V
    .locals 2

    .prologue
    .line 1196871
    iget-object v0, p0, LX/7LD;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/394;

    .line 1196872
    invoke-interface {v0, p1, p2}, LX/394;->a(LX/04g;LX/7Jv;)V

    goto :goto_0

    .line 1196873
    :cond_0
    iget-object v0, p0, LX/7LD;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ar:LX/394;

    if-eqz v0, :cond_1

    .line 1196874
    iget-object v0, p0, LX/7LD;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ar:LX/394;

    invoke-interface {v0, p1, p2}, LX/394;->a(LX/04g;LX/7Jv;)V

    .line 1196875
    :cond_1
    return-void
.end method
