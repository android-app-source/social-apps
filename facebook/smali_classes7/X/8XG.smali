.class public final LX/8XG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;)V
    .locals 0

    .prologue
    .line 1354703
    iput-object p1, p0, LX/8XG;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 1354704
    iget-object v0, p0, LX/8XG;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    iget v0, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 1354705
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1354706
    const-string v2, "score"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1354707
    iget-object v0, p0, LX/8XG;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 1354708
    const-string v0, "score_screenshot_handle"

    iget-object v2, p0, LX/8XG;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    iget-object v2, v2, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->w:LX/8Vg;

    iget-object v3, p0, LX/8XG;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, LX/8XG;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    iget-object v4, v4, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3, v4}, LX/8Vg;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1354709
    :cond_0
    iget-object v0, p0, LX/8XG;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->a$redex0(Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;Z)V

    .line 1354710
    iget-object v0, p0, LX/8XG;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->r:Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    iget-object v3, p0, LX/8XG;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Lcom/facebook/katana/games/quicksilver/QuicksilverFB4ADataProvider;->a(ILandroid/app/Activity;Ljava/util/Map;)LX/8Tp;

    move-result-object v1

    .line 1354711
    iget-object v0, p0, LX/8XG;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    iget-object v0, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8T9;

    invoke-virtual {v0, v1}, LX/8T9;->a(LX/8Tp;)V

    .line 1354712
    const/4 v0, 0x1

    return v0
.end method
