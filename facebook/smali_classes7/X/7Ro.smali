.class public LX/7Ro;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/7Ro;


# instance fields
.field public final a:LX/0W3;


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1207866
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1207867
    iput-object p1, p0, LX/7Ro;->a:LX/0W3;

    .line 1207868
    return-void
.end method

.method public static a(LX/0QB;)LX/7Ro;
    .locals 4

    .prologue
    .line 1207869
    sget-object v0, LX/7Ro;->b:LX/7Ro;

    if-nez v0, :cond_1

    .line 1207870
    const-class v1, LX/7Ro;

    monitor-enter v1

    .line 1207871
    :try_start_0
    sget-object v0, LX/7Ro;->b:LX/7Ro;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1207872
    if-eqz v2, :cond_0

    .line 1207873
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1207874
    new-instance p0, LX/7Ro;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/7Ro;-><init>(LX/0W3;)V

    .line 1207875
    move-object v0, p0

    .line 1207876
    sput-object v0, LX/7Ro;->b:LX/7Ro;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1207877
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1207878
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1207879
    :cond_1
    sget-object v0, LX/7Ro;->b:LX/7Ro;

    return-object v0

    .line 1207880
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1207881
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
