.class public final LX/8YT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1uy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1uy",
        "<",
        "LX/8Ya;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/richdocument/fonts/FetchFontExecutor;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field private final d:LX/8Yb;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/fonts/FetchFontExecutor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1356225
    iput-object p1, p0, LX/8YT;->a:Lcom/facebook/richdocument/fonts/FetchFontExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1356226
    iput-object p2, p0, LX/8YT;->b:Ljava/lang/String;

    .line 1356227
    iput-object p3, p0, LX/8YT;->c:Ljava/lang/String;

    .line 1356228
    new-instance v0, LX/8Yb;

    invoke-direct {v0, p2, p3}, LX/8Yb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/8YT;->d:LX/8Yb;

    .line 1356229
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/io/InputStream;)Z
    .locals 3

    .prologue
    .line 1356230
    new-instance v0, LX/8YY;

    iget-object v1, p0, LX/8YT;->b:Ljava/lang/String;

    iget-object v2, p0, LX/8YT;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1}, LX/8YY;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 1356231
    iget-object v0, p0, LX/8YT;->a:Lcom/facebook/richdocument/fonts/FetchFontExecutor;

    iget-object v0, v0, Lcom/facebook/richdocument/fonts/FetchFontExecutor;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8YX;

    invoke-virtual {v0, v1, p2}, LX/2Vx;->a(LX/2WG;Ljava/io/InputStream;)LX/1gI;

    move-result-object v0

    check-cast v0, LX/1gH;

    .line 1356232
    iget-object v2, v0, LX/1gH;->a:Ljava/io/File;

    move-object v0, v2

    .line 1356233
    if-eqz v0, :cond_0

    .line 1356234
    :try_start_0
    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/io/File;)Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1356235
    const/4 v2, 0x1

    .line 1356236
    :goto_0
    move v0, v2

    .line 1356237
    if-eqz v0, :cond_0

    .line 1356238
    const/4 v0, 0x1

    .line 1356239
    :goto_1
    return v0

    .line 1356240
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Invalid font file: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1356241
    iget-object v0, p0, LX/8YT;->a:Lcom/facebook/richdocument/fonts/FetchFontExecutor;

    iget-object v0, v0, Lcom/facebook/richdocument/fonts/FetchFontExecutor;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, Lcom/facebook/richdocument/fonts/FetchFontExecutor;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1356242
    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1356243
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1356244
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1356245
    const/16 v0, 0x20

    invoke-virtual {v3, v0}, Ljava/io/BufferedInputStream;->mark(I)V

    .line 1356246
    new-instance v4, Ljava/util/zip/ZipInputStream;

    invoke-direct {v4, v3}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1356247
    invoke-virtual {v4}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    .line 1356248
    :goto_0
    if-eqz v1, :cond_3

    .line 1356249
    :cond_0
    :goto_1
    if-eqz v0, :cond_5

    .line 1356250
    invoke-virtual {v0}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1356251
    invoke-virtual {v0}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1t3;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1356252
    invoke-direct {p0, v0, v4}, LX/8YT;->a(Ljava/lang/String;Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1356253
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1356254
    :cond_1
    invoke-virtual {v4}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1356255
    invoke-virtual {v4}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v0

    goto :goto_1

    .line 1356256
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 1356257
    :cond_3
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->reset()V

    .line 1356258
    iget-object v0, p0, LX/8YT;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v3}, LX/8YT;->a(Ljava/lang/String;Ljava/io/InputStream;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1356259
    iget-object v0, p0, LX/8YT;->b:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1356260
    :cond_4
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 1356261
    :cond_5
    invoke-virtual {v4}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1356262
    sget-object v0, Lcom/facebook/richdocument/fonts/FetchFontExecutor;->b:Ljava/util/Map;

    iget-object v1, p0, LX/8YT;->d:LX/8Yb;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356263
    new-instance v0, LX/8Ya;

    iget-object v1, p0, LX/8YT;->b:Ljava/lang/String;

    iget-object v3, p0, LX/8YT;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v3, v2}, LX/8Ya;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method
