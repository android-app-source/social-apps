.class public LX/7K5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements LX/7Jq;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/media/MediaPlayer;

.field private final c:J

.field private final d:LX/0So;

.field private final e:Landroid/net/Uri;

.field private f:LX/2qD;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1194456
    const-class v0, LX/7K5;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7K5;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/media/MediaPlayer;Landroid/content/Context;Landroid/net/Uri;LX/0So;)V
    .locals 2

    .prologue
    .line 1194457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194458
    sget-object v0, LX/2qD;->STATE_PREPARING:LX/2qD;

    iput-object v0, p0, LX/7K5;->f:LX/2qD;

    .line 1194459
    iput-object p1, p0, LX/7K5;->b:Landroid/media/MediaPlayer;

    .line 1194460
    iput-object p4, p0, LX/7K5;->d:LX/0So;

    .line 1194461
    iget-object v0, p0, LX/7K5;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p2, p3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1194462
    iget-object v0, p0, LX/7K5;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 1194463
    invoke-virtual {p1, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 1194464
    iget-object v0, p0, LX/7K5;->b:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 1194465
    iget-object v0, p0, LX/7K5;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/7K5;->c:J

    .line 1194466
    iput-object p3, p0, LX/7K5;->e:Landroid/net/Uri;

    .line 1194467
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1194468
    return-void
.end method

.method public final b()Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 1194469
    iget-object v0, p0, LX/7K5;->b:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method public final c()LX/2qD;
    .locals 1

    .prologue
    .line 1194470
    iget-object v0, p0, LX/7K5;->f:LX/2qD;

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 1194471
    iget-wide v0, p0, LX/7K5;->c:J

    return-wide v0
.end method

.method public final onError(Landroid/media/MediaPlayer;II)Z
    .locals 1

    .prologue
    .line 1194472
    sget-object v0, LX/2qD;->STATE_ERROR:LX/2qD;

    iput-object v0, p0, LX/7K5;->f:LX/2qD;

    .line 1194473
    const/4 v0, 0x1

    return v0
.end method

.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1

    .prologue
    .line 1194474
    iget-object v0, p0, LX/7K5;->b:Landroid/media/MediaPlayer;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1194475
    iget-object v0, p0, LX/7K5;->d:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    .line 1194476
    sget-object v0, LX/2qD;->STATE_PREPARED:LX/2qD;

    iput-object v0, p0, LX/7K5;->f:LX/2qD;

    .line 1194477
    return-void

    .line 1194478
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
