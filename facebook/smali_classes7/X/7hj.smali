.class public final LX/7hj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;


# direct methods
.method public constructor <init>(Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;)V
    .locals 0

    .prologue
    .line 1226494
    iput-object p1, p0, LX/7hj;->a:Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x7f360281

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1226495
    iget-object v1, p0, LX/7hj;->a:Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;

    const/4 p1, 0x0

    const/16 p0, 0x8

    .line 1226496
    iget-object v3, v1, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->emailText:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1226497
    iget-object v3, v1, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->emailText:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1226498
    iget-object v3, v1, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->userPhoto:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1226499
    iget-object v3, v1, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->userName:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1226500
    iget-object v3, v1, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->notYouLink:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1226501
    iget-object v3, v1, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->signupButton:Landroid/widget/Button;

    if-eqz v3, :cond_0

    .line 1226502
    iget-object v3, v1, Lcom/facebook/auth/login/ui/GenericPasswordCredentialsViewGroup;->signupButton:Landroid/widget/Button;

    invoke-virtual {v3, p1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1226503
    :cond_0
    const v1, -0x507a6864

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
