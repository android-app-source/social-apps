.class public final LX/78v;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public final synthetic a:LX/78w;


# direct methods
.method public constructor <init>(LX/78w;)V
    .locals 0

    .prologue
    .line 1173649
    iput-object p1, p0, LX/78v;->a:LX/78w;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/78w;B)V
    .locals 0

    .prologue
    .line 1173642
    invoke-direct {p0, p1}, LX/78v;-><init>(LX/78w;)V

    return-void
.end method

.method private a(I)Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;
    .locals 1

    .prologue
    .line 1173650
    iget-object v0, p0, LX/78v;->a:LX/78w;

    iget-object v0, v0, LX/78w;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    return-object v0
.end method

.method private static a(Landroid/widget/RadioButton;Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1173643
    iget-object v0, p1, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1173644
    invoke-virtual {p0, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 1173645
    iget v0, p1, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->c:I

    move v0, v0

    .line 1173646
    invoke-virtual {p0, v0, v1, v1, v1}, Landroid/widget/RadioButton;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 1173647
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 1173648
    iget-object v0, p0, LX/78v;->a:LX/78w;

    iget-object v0, v0, LX/78w;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1173634
    invoke-direct {p0, p1}, LX/78v;->a(I)Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 4

    .prologue
    .line 1173635
    iget-object v0, p0, LX/78v;->a:LX/78w;

    iget-object v0, v0, LX/78w;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    .line 1173636
    iget-wide v2, v0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->b:J

    move-wide v0, v2

    .line 1173637
    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1173638
    if-nez p2, :cond_0

    .line 1173639
    iget-object v0, p0, LX/78v;->a:LX/78w;

    invoke-virtual {v0}, LX/78w;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030202

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 1173640
    check-cast v0, Landroid/widget/RadioButton;

    invoke-direct {p0, p1}, LX/78v;->a(I)Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    move-result-object v2

    invoke-static {v0, v2}, LX/78v;->a(Landroid/widget/RadioButton;Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;)V

    .line 1173641
    return-object v1

    :cond_0
    move-object v1, p2

    goto :goto_0
.end method
