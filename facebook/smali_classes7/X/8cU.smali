.class public LX/8cU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1374514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 12

    .prologue
    .line 1374515
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 1374516
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Strings must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1374517
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 1374518
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 1374519
    if-nez v3, :cond_3

    move v0, v2

    .line 1374520
    :cond_2
    :goto_0
    return v0

    .line 1374521
    :cond_3
    if-nez v2, :cond_4

    move v0, v3

    .line 1374522
    goto :goto_0

    .line 1374523
    :cond_4
    add-int/lit8 v0, v3, 0x1

    new-array v7, v0, [I

    .line 1374524
    add-int/lit8 v0, v3, 0x1

    new-array v1, v0, [I

    .line 1374525
    const/4 v0, 0x0

    :goto_1
    if-gt v0, v3, :cond_5

    .line 1374526
    aput v0, v7, v0

    .line 1374527
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1374528
    :cond_5
    const/4 v0, 0x1

    move v5, v0

    move-object v6, v1

    :goto_2
    if-gt v5, v2, :cond_9

    .line 1374529
    add-int/lit8 v0, v5, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v8

    .line 1374530
    const/4 v0, 0x0

    aput v5, v6, v0

    .line 1374531
    add-int/lit8 v1, p2, 0x1

    .line 1374532
    const/4 v0, 0x1

    move v4, v0

    :goto_3
    if-gt v4, v3, :cond_7

    .line 1374533
    add-int/lit8 v0, v4, -0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v8, :cond_6

    const/4 v0, 0x0

    .line 1374534
    :goto_4
    add-int/lit8 v9, v4, -0x1

    aget v9, v6, v9

    add-int/lit8 v9, v9, 0x1

    aget v10, v7, v4

    add-int/lit8 v10, v10, 0x1

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    add-int/lit8 v10, v4, -0x1

    aget v10, v7, v10

    add-int/2addr v0, v10

    invoke-static {v9, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    aput v0, v6, v4

    .line 1374535
    aget v0, v6, v4

    if-le v1, v0, :cond_b

    .line 1374536
    aget v0, v6, v4

    .line 1374537
    :goto_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_3

    .line 1374538
    :cond_6
    const/4 v0, 0x1

    goto :goto_4

    .line 1374539
    :cond_7
    if-ne v5, v2, :cond_a

    .line 1374540
    aget v0, v6, v3

    if-le v0, p2, :cond_8

    add-int/lit8 v0, p2, 0x1

    .line 1374541
    :goto_6
    if-gt v0, p2, :cond_2

    .line 1374542
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move-object v11, v7

    move-object v7, v6

    move-object v6, v11

    goto :goto_2

    .line 1374543
    :cond_8
    aget v0, v6, v3

    goto :goto_6

    .line 1374544
    :cond_9
    aget v0, v7, v3

    goto :goto_0

    :cond_a
    move v0, v1

    goto :goto_6

    :cond_b
    move v0, v1

    goto :goto_5
.end method
