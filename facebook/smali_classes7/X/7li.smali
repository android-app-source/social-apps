.class public final LX/7li;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public final b:J

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public e:I

.field public f:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
    .end annotation
.end field

.field public h:Lcom/facebook/ipc/media/MediaItem;

.field public i:I


# direct methods
.method public constructor <init>(JLcom/facebook/graphql/model/GraphQLPrivacyOption;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p3    # Lcom/facebook/graphql/model/GraphQLPrivacyOption;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param

    .prologue
    .line 1235436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1235437
    iput-wide p1, p0, LX/7li;->b:J

    .line 1235438
    iput-object p3, p0, LX/7li;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1235439
    iput p4, p0, LX/7li;->e:I

    .line 1235440
    iput-object p5, p0, LX/7li;->f:Ljava/lang/String;

    .line 1235441
    iput-object p6, p0, LX/7li;->g:Ljava/lang/String;

    .line 1235442
    return-void
.end method

.method public static a(Lcom/facebook/composer/protocol/PostReviewParams;)LX/7li;
    .locals 8

    .prologue
    .line 1235443
    new-instance v1, LX/7li;

    iget-wide v2, p0, Lcom/facebook/composer/protocol/PostReviewParams;->b:J

    iget-object v4, p0, Lcom/facebook/composer/protocol/PostReviewParams;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget v5, p0, Lcom/facebook/composer/protocol/PostReviewParams;->e:I

    iget-object v6, p0, Lcom/facebook/composer/protocol/PostReviewParams;->f:Ljava/lang/String;

    iget-object v7, p0, Lcom/facebook/composer/protocol/PostReviewParams;->g:Ljava/lang/String;

    invoke-direct/range {v1 .. v7}, LX/7li;-><init>(JLcom/facebook/graphql/model/GraphQLPrivacyOption;ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/facebook/composer/protocol/PostReviewParams;->a:Ljava/lang/String;

    .line 1235444
    iput-object v0, v1, LX/7li;->a:Ljava/lang/String;

    .line 1235445
    move-object v0, v1

    .line 1235446
    iget-object v1, p0, Lcom/facebook/composer/protocol/PostReviewParams;->c:Ljava/lang/String;

    .line 1235447
    iput-object v1, v0, LX/7li;->c:Ljava/lang/String;

    .line 1235448
    move-object v0, v0

    .line 1235449
    iget-object v1, p0, Lcom/facebook/composer/protocol/PostReviewParams;->h:Lcom/facebook/ipc/media/MediaItem;

    .line 1235450
    iput-object v1, v0, LX/7li;->h:Lcom/facebook/ipc/media/MediaItem;

    .line 1235451
    move-object v0, v0

    .line 1235452
    iget v1, p0, Lcom/facebook/composer/protocol/PostReviewParams;->i:I

    .line 1235453
    iput v1, v0, LX/7li;->i:I

    .line 1235454
    move-object v0, v0

    .line 1235455
    return-object v0
.end method


# virtual methods
.method public final a(I)LX/7li;
    .locals 0

    .prologue
    .line 1235456
    iput p1, p0, LX/7li;->i:I

    .line 1235457
    return-object p0
.end method

.method public final a()Lcom/facebook/composer/protocol/PostReviewParams;
    .locals 1

    .prologue
    .line 1235458
    new-instance v0, Lcom/facebook/composer/protocol/PostReviewParams;

    invoke-direct {v0, p0}, Lcom/facebook/composer/protocol/PostReviewParams;-><init>(LX/7li;)V

    return-object v0
.end method
