.class public final LX/8XB;
.super LX/2s5;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;

.field public b:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

.field public c:Lcom/facebook/quicksilver/views/GamesTopScoresFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;LX/0gc;)V
    .locals 0

    .prologue
    .line 1354661
    iput-object p1, p0, LX/8XB;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;

    .line 1354662
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 1354663
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 1354664
    packed-switch p1, :pswitch_data_0

    .line 1354665
    iget-object v0, p0, LX/8XB;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;

    const v1, 0x7f08234d

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1354666
    :pswitch_0
    iget-object v0, p0, LX/8XB;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;

    const v1, 0x7f08234c

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 1354667
    packed-switch p1, :pswitch_data_0

    .line 1354668
    iget-object v0, p0, LX/8XB;->c:Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    if-nez v0, :cond_0

    .line 1354669
    new-instance v0, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    invoke-direct {v0}, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;-><init>()V

    iput-object v0, p0, LX/8XB;->c:Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    .line 1354670
    iget-object v0, p0, LX/8XB;->c:Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    new-instance v1, LX/8XA;

    invoke-direct {v1, p0}, LX/8XA;-><init>(LX/8XB;)V

    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->a(LX/8VT;)V

    .line 1354671
    iget-object v0, p0, LX/8XB;->c:Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->b()V

    .line 1354672
    :cond_0
    iget-object v0, p0, LX/8XB;->c:Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    :goto_0
    return-object v0

    .line 1354673
    :pswitch_0
    iget-object v0, p0, LX/8XB;->b:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    if-nez v0, :cond_1

    .line 1354674
    new-instance v0, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    invoke-direct {v0}, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;-><init>()V

    iput-object v0, p0, LX/8XB;->b:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    .line 1354675
    iget-object v0, p0, LX/8XB;->b:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    new-instance v1, LX/8X9;

    invoke-direct {v1, p0}, LX/8X9;-><init>(LX/8XB;)V

    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->a(LX/8VT;)V

    .line 1354676
    iget-object v0, p0, LX/8XB;->b:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->b()V

    .line 1354677
    :cond_1
    iget-object v0, p0, LX/8XB;->b:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1354678
    const/4 v0, 0x2

    return v0
.end method
