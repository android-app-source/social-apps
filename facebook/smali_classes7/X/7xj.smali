.class public LX/7xj;
.super LX/1OM;
.source ""

# interfaces
.implements LX/7xU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/62U;",
        ">;",
        "LX/7xU;",
        "Lcom/facebook/events/tickets/modal/views/EventSelectTicketsSeatSelectionNoteViewHolder$OnCrossGlyphClickListener;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:Z

.field private l:LX/7wd;

.field private final m:LX/7xW;

.field private final n:LX/7xg;

.field public final o:LX/0iA;

.field public final p:LX/3kp;


# direct methods
.method public constructor <init>(LX/7wd;LX/0iA;LX/3kp;)V
    .locals 1
    .param p1    # LX/7wd;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1277808
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1277809
    new-instance v0, LX/7xf;

    invoke-direct {v0, p0}, LX/7xf;-><init>(LX/7xj;)V

    iput-object v0, p0, LX/7xj;->m:LX/7xW;

    .line 1277810
    new-instance v0, LX/7xg;

    invoke-direct {v0, p0}, LX/7xg;-><init>(LX/7xj;)V

    iput-object v0, p0, LX/7xj;->n:LX/7xg;

    .line 1277811
    iput-object p1, p0, LX/7xj;->l:LX/7wd;

    .line 1277812
    iput-object p2, p0, LX/7xj;->o:LX/0iA;

    .line 1277813
    iput-object p3, p0, LX/7xj;->p:LX/3kp;

    .line 1277814
    return-void
.end method

.method public static g(LX/7xj;)V
    .locals 6

    .prologue
    .line 1277815
    iget-object v0, p0, LX/7xj;->l:LX/7wd;

    if-eqz v0, :cond_0

    .line 1277816
    iget-object v0, p0, LX/7xj;->l:LX/7wd;

    iget v1, p0, LX/7xj;->d:I

    iget v2, p0, LX/7xj;->e:I

    iget v3, p0, LX/7xj;->c:I

    .line 1277817
    iget-object v4, v0, LX/7wd;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    iget-object v5, v0, LX/7wd;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    iget-object v5, v5, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    invoke-virtual {v5}, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->a()LX/7wo;

    move-result-object v5

    const/4 p0, 0x0

    invoke-interface {v5, v3, p0}, LX/7wo;->a(II)LX/7wo;

    move-result-object v5

    invoke-interface {v5, v1, v2}, LX/7wo;->a(II)LX/7wo;

    move-result-object v5

    invoke-interface {v5}, LX/7wo;->a()Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    move-result-object v5

    .line 1277818
    iput-object v5, v4, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1277819
    iget-object v4, v0, LX/7wd;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    iget-object v5, v0, LX/7wd;->a:Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;

    .line 1277820
    iget-object p0, v5, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->g:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object p0, p0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    .line 1277821
    iget v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    iget v2, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->h:I

    if-lt v0, v2, :cond_1

    iget v0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    iget p0, p0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->g:I

    if-gt v0, p0, :cond_1

    const/4 p0, 0x1

    :goto_0
    move v5, p0

    .line 1277822
    invoke-static {v4, v5}, Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;->a(Lcom/facebook/events/tickets/modal/fragments/EventStartSelectTicketsFragment;Z)V

    .line 1277823
    :cond_0
    return-void

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 1277824
    const/4 v3, 0x0

    .line 1277825
    sget-object v0, LX/7xh;->a:[I

    invoke-static {}, LX/7xi;->values()[LX/7xi;

    move-result-object v1

    aget-object v1, v1, p2

    invoke-virtual {v1}, LX/7xi;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1277826
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1277827
    :pswitch_0
    new-instance v0, LX/7xR;

    new-instance v1, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/7xR;-><init>(Landroid/view/View;)V

    .line 1277828
    :goto_0
    return-object v0

    .line 1277829
    :pswitch_1
    new-instance v0, LX/7xe;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030516

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7xe;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1277830
    :pswitch_2
    new-instance v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030516

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1277831
    :pswitch_3
    new-instance v0, LX/7xb;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030517

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7xb;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1277832
    :pswitch_4
    new-instance v0, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsSeatMapViewHolder;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030518

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsSeatMapViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1277833
    :pswitch_5
    new-instance v0, LX/7xd;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030519

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7xd;-><init>(Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 1277754
    check-cast p1, LX/62U;

    .line 1277755
    sget-object v0, LX/7xh;->a:[I

    invoke-static {}, LX/7xi;->values()[LX/7xi;

    move-result-object v1

    .line 1277756
    iget v2, p1, LX/1a1;->e:I

    move v2, v2

    .line 1277757
    aget-object v1, v1, v2

    invoke-virtual {v1}, LX/7xi;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1277758
    :goto_0
    return-void

    .line 1277759
    :pswitch_0
    check-cast p1, LX/7xR;

    iget-object v0, p0, LX/7xj;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1277760
    iget-object v1, p1, LX/7xR;->m:Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/tickets/modal/views/EventTicketingInfoRowView;->a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V

    .line 1277761
    goto :goto_0

    .line 1277762
    :pswitch_1
    check-cast p1, LX/7xe;

    iget v1, p0, LX/7xj;->e:I

    iget-object v0, p0, LX/7xj;->b:LX/0Px;

    iget v2, p0, LX/7xj;->d:I

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    iget-object v2, p0, LX/7xj;->m:LX/7xW;

    .line 1277763
    iget-object v3, p1, LX/7xe;->p:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    .line 1277764
    iput-object v2, v3, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->i:LX/7xW;

    .line 1277765
    iget-object v3, p1, LX/7xe;->p:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    iget v4, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->h:I

    invoke-virtual {v3, v4}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->setMinimumQuantity(I)V

    .line 1277766
    iget-object v3, p1, LX/7xe;->p:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    iget v4, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->g:I

    invoke-virtual {v3, v4}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->setMaximumQuantity(I)V

    .line 1277767
    iget-object v3, p1, LX/7xe;->p:Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;

    invoke-virtual {v3, v1}, Lcom/facebook/events/tickets/modal/views/EventTicketingQuantityPicker;->setCurrentQuantity(I)V

    .line 1277768
    goto :goto_0

    :pswitch_2
    move-object v0, p1

    .line 1277769
    check-cast v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;

    iget v1, p0, LX/7xj;->d:I

    iget v2, p0, LX/7xj;->g:I

    sub-int v2, p2, v2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_1
    iget v2, p0, LX/7xj;->g:I

    sub-int v2, p2, v2

    iget v3, p0, LX/7xj;->e:I

    iget-object v4, p0, LX/7xj;->b:LX/0Px;

    iget v5, p0, LX/7xj;->g:I

    sub-int v5, p2, v5

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    iget-object v5, p0, LX/7xj;->n:LX/7xg;

    .line 1277770
    iget-object p0, v4, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->d:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;->PRE_SALE:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    if-ne p0, p1, :cond_1

    .line 1277771
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->r:Lcom/facebook/resources/ui/FbRadioButton;

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbRadioButton;->setVisibility(I)V

    .line 1277772
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->p:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1277773
    :goto_2
    iget-object p0, v4, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->c:Ljava/lang/String;

    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 1277774
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-object p1, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->x:LX/7xH;

    iget-object v2, v4, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->i:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v2}, LX/7xH;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277775
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->o:Lcom/facebook/resources/ui/FbTextView;

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277776
    :goto_3
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->x:LX/7xH;

    invoke-virtual {p0, v1, v3, v4}, LX/7xH;->a(ZILcom/facebook/events/tickets/modal/model/EventTicketTierModel;)Ljava/lang/CharSequence;

    move-result-object p0

    .line 1277777
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 1277778
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1277779
    :goto_4
    iget-object p0, v4, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->j:Ljava/lang/String;

    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1277780
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->t:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setVisibility(I)V

    .line 1277781
    :goto_5
    iget-object p0, v4, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->k:Ljava/lang/String;

    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1277782
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1277783
    :goto_6
    goto/16 :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 1277784
    :pswitch_3
    check-cast p1, LX/7xb;

    iget-object v0, p0, LX/7xj;->b:LX/0Px;

    iget v1, p0, LX/7xj;->g:I

    sub-int v1, p2, v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    invoke-virtual {p1, v0}, LX/7xb;->a(Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;)V

    goto/16 :goto_0

    .line 1277785
    :pswitch_4
    check-cast p1, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsSeatMapViewHolder;

    iget-object v0, p0, LX/7xj;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->r:Ljava/lang/String;

    .line 1277786
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1277787
    iget-object v1, p1, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsSeatMapViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1277788
    :goto_7
    goto/16 :goto_0

    .line 1277789
    :pswitch_5
    check-cast p1, LX/7xd;

    iget-object v0, p0, LX/7xj;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->s:Ljava/lang/String;

    .line 1277790
    iget-object v1, p1, LX/7xd;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277791
    iput-object p0, p1, LX/7xd;->o:LX/7xj;

    .line 1277792
    goto/16 :goto_0

    .line 1277793
    :cond_1
    iput v2, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->v:I

    .line 1277794
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->r:Lcom/facebook/resources/ui/FbRadioButton;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbRadioButton;->setVisibility(I)V

    .line 1277795
    iput-object v5, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->u:LX/7xg;

    .line 1277796
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->p:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iget-object p1, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->w:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1277797
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->r:Lcom/facebook/resources/ui/FbRadioButton;

    invoke-virtual {p0, v1}, Lcom/facebook/resources/ui/FbRadioButton;->setChecked(Z)V

    goto/16 :goto_2

    .line 1277798
    :cond_2
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-object p1, v4, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->c:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277799
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->o:Lcom/facebook/resources/ui/FbTextView;

    iget-object p1, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->x:LX/7xH;

    iget-object v2, v4, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->i:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v2}, LX/7xH;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 1277800
    :cond_3
    iget-object p1, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1, p0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277801
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->q:Lcom/facebook/resources/ui/FbTextView;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 1277802
    :cond_4
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->t:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    iget-object p1, v4, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->j:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1277803
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->t:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 1277804
    :cond_5
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object p1, v4, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->k:Ljava/lang/String;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    sget-object v1, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1277805
    iget-object p0, v0, Lcom/facebook/events/tickets/modal/views/EventSingleTierSelectTicketsViewHolder;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_6

    .line 1277806
    :cond_6
    iget-object v1, p1, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsSeatMapViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsSeatMapViewHolder;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1277807
    iget-object v1, p1, Lcom/facebook/events/tickets/modal/views/EventSelectTicketsSeatMapViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto/16 :goto_7

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1277715
    iput-object p1, p0, LX/7xj;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    .line 1277716
    iget-object v0, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->q:LX/0Px;

    iput-object v0, p0, LX/7xj;->b:LX/0Px;

    .line 1277717
    iput v3, p0, LX/7xj;->f:I

    .line 1277718
    iput v3, p0, LX/7xj;->g:I

    .line 1277719
    iput v2, p0, LX/7xj;->h:I

    .line 1277720
    iput v1, p0, LX/7xj;->i:I

    .line 1277721
    iput v2, p0, LX/7xj;->j:I

    .line 1277722
    iput v1, p0, LX/7xj;->c:I

    .line 1277723
    const/4 v2, 0x0

    .line 1277724
    move v1, v2

    :goto_0
    iget-object v0, p0, LX/7xj;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1277725
    iget-object v0, p0, LX/7xj;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    iget v0, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    if-lez v0, :cond_2

    .line 1277726
    iput v1, p0, LX/7xj;->d:I

    .line 1277727
    iget-object v0, p0, LX/7xj;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    iget v0, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    iput v0, p0, LX/7xj;->e:I

    .line 1277728
    :goto_1
    iget-object v0, p0, LX/7xj;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->r:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1277729
    iget v0, p0, LX/7xj;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7xj;->f:I

    .line 1277730
    iget v0, p0, LX/7xj;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7xj;->g:I

    .line 1277731
    iget v0, p0, LX/7xj;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7xj;->h:I

    .line 1277732
    :cond_0
    const/4 v2, 0x1

    .line 1277733
    iget-object v0, p0, LX/7xj;->p:LX/3kp;

    .line 1277734
    iput v2, v0, LX/3kp;->b:I

    .line 1277735
    iget-object v0, p0, LX/7xj;->p:LX/3kp;

    sget-object v1, LX/7vb;->l:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 1277736
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7xj;->k:Z

    .line 1277737
    iget-object v0, p0, LX/7xj;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->s:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/7xj;->p:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1277738
    iget-object v0, p0, LX/7xj;->o:LX/0iA;

    sget-object v1, LX/3lP;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/3lP;

    invoke-virtual {v0, v1, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3lP;

    .line 1277739
    if-eqz v0, :cond_4

    invoke-virtual {v0}, LX/3lP;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "4535"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 1277740
    if-eqz v0, :cond_1

    .line 1277741
    iget-object v0, p0, LX/7xj;->p:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    .line 1277742
    iget-object v0, p0, LX/7xj;->o:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    const-string v1, "4535"

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1277743
    iput-boolean v2, p0, LX/7xj;->k:Z

    .line 1277744
    iget v0, p0, LX/7xj;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7xj;->f:I

    .line 1277745
    iget v0, p0, LX/7xj;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7xj;->g:I

    .line 1277746
    iget v0, p0, LX/7xj;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7xj;->h:I

    .line 1277747
    iget v0, p0, LX/7xj;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7xj;->i:I

    .line 1277748
    iget v0, p0, LX/7xj;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7xj;->j:I

    .line 1277749
    :cond_1
    invoke-static {p0}, LX/7xj;->g(LX/7xj;)V

    .line 1277750
    return-void

    .line 1277751
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 1277752
    :cond_3
    iput v2, p0, LX/7xj;->d:I

    .line 1277753
    iget-object v0, p0, LX/7xj;->b:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    iget v0, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->h:I

    iput v0, p0, LX/7xj;->e:I

    goto/16 :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 1277702
    iget-boolean v0, p0, LX/7xj;->k:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 1277703
    sget-object v0, LX/7xi;->SEAT_SELECTION_NOTE:LX/7xi;

    invoke-virtual {v0}, LX/7xi;->ordinal()I

    move-result v0

    .line 1277704
    :goto_0
    return v0

    .line 1277705
    :cond_0
    iget v0, p0, LX/7xj;->i:I

    if-ne p1, v0, :cond_1

    .line 1277706
    sget-object v0, LX/7xi;->EVENT_INFO_HEADER:LX/7xi;

    invoke-virtual {v0}, LX/7xi;->ordinal()I

    move-result v0

    goto :goto_0

    .line 1277707
    :cond_1
    iget-object v0, p0, LX/7xj;->a:Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->r:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, LX/7xj;->j:I

    if-ne p1, v0, :cond_2

    .line 1277708
    sget-object v0, LX/7xi;->SEAT_MAP_IMAGE:LX/7xi;

    invoke-virtual {v0}, LX/7xi;->ordinal()I

    move-result v0

    goto :goto_0

    .line 1277709
    :cond_2
    iget v0, p0, LX/7xj;->h:I

    if-ne p1, v0, :cond_3

    .line 1277710
    sget-object v0, LX/7xi;->QUANTITY_SELECT_HEADER:LX/7xi;

    invoke-virtual {v0}, LX/7xi;->ordinal()I

    move-result v0

    goto :goto_0

    .line 1277711
    :cond_3
    iget-object v0, p0, LX/7xj;->b:LX/0Px;

    iget v1, p0, LX/7xj;->g:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->d:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;->POST_SALE:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, LX/7xj;->b:LX/0Px;

    iget v1, p0, LX/7xj;->g:I

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    iget-object v0, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->d:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;->SOLD_OUT:Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    if-ne v0, v1, :cond_5

    .line 1277712
    :cond_4
    sget-object v0, LX/7xi;->TICKET_TIER_NOT_AVAILABLE_ITEM:LX/7xi;

    invoke-virtual {v0}, LX/7xi;->ordinal()I

    move-result v0

    goto :goto_0

    .line 1277713
    :cond_5
    sget-object v0, LX/7xi;->TICKET_TIER_ITEM:LX/7xi;

    invoke-virtual {v0}, LX/7xi;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 1277714
    iget-object v0, p0, LX/7xj;->b:LX/0Px;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/7xj;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget v1, p0, LX/7xj;->f:I

    add-int/2addr v0, v1

    goto :goto_0
.end method
