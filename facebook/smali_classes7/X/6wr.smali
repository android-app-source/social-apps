.class public LX/6wr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/Integer;)V
    .locals 0
    .param p1    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/payments/decorator/PaymentsAppThemeResourceId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1158045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158046
    iput-object p1, p0, LX/6wr;->a:Ljava/lang/Integer;

    .line 1158047
    return-void
.end method

.method public static a(Landroid/app/Activity;LX/6ws;)V
    .locals 3

    .prologue
    .line 1158048
    sget-object v0, LX/6wq;->a:[I

    invoke-virtual {p1}, LX/6ws;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1158049
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal animation seen: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1158050
    :pswitch_0
    const v0, 0x7f040034

    const v1, 0x7f0400c6

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 1158051
    :goto_0
    return-void

    .line 1158052
    :pswitch_1
    const v0, 0x7f040035

    const v1, 0x7f0400c6

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/support/v7/widget/RecyclerView;Z)V
    .locals 2

    .prologue
    .line 1158053
    if-nez p1, :cond_0

    .line 1158054
    new-instance v0, LX/62V;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/62V;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1158055
    :goto_0
    return-void

    .line 1158056
    :cond_0
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;LX/0am;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1158057
    if-nez p2, :cond_0

    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1158058
    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setMinimumHeight(I)V

    .line 1158059
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/6wr;
    .locals 2

    .prologue
    .line 1158060
    new-instance v1, LX/6wr;

    .line 1158061
    invoke-static {}, LX/0eG;->i()Ljava/lang/Integer;

    move-result-object v0

    move-object v0, v0

    .line 1158062
    check-cast v0, Ljava/lang/Integer;

    invoke-direct {v1, v0}, LX/6wr;-><init>(Ljava/lang/Integer;)V

    .line 1158063
    return-object v1
.end method

.method public static b(Landroid/app/Activity;LX/6ws;)V
    .locals 3

    .prologue
    .line 1158064
    sget-object v0, LX/6wq;->a:[I

    invoke-virtual {p1}, LX/6ws;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1158065
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal animation seen: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1158066
    :pswitch_0
    const v0, 0x7f0400c5

    const v1, 0x7f040036

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 1158067
    :goto_0
    return-void

    .line 1158068
    :pswitch_1
    const v0, 0x7f0400c5

    const v1, 0x7f040037

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Landroid/app/Activity;ZLX/73i;)V
    .locals 4

    .prologue
    .line 1158069
    if-nez p1, :cond_0

    sget-object v0, LX/73i;->PAYMENTS_WHITE:LX/73i;

    if-ne p2, v0, :cond_0

    .line 1158070
    const v0, 0x7f0d002f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1158071
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1158072
    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1158073
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1158074
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;ZLX/73i;)V
    .locals 3

    .prologue
    .line 1158075
    invoke-virtual {p1}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    iget-object v1, p0, LX/6wr;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 1158076
    sget-object v0, LX/73i;->PAYMENTS_WHITE:LX/73i;

    if-ne p3, v0, :cond_1

    .line 1158077
    if-nez p2, :cond_0

    .line 1158078
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 p0, 0x2

    .line 1158079
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1158080
    invoke-virtual {v0, p0, p0}, Landroid/view/Window;->setFlags(II)V

    .line 1158081
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 1158082
    const v2, 0x3f19999a    # 0.6f

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 1158083
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1158084
    :cond_0
    if-nez p2, :cond_2

    const v0, 0x106000d

    .line 1158085
    :goto_0
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1158086
    invoke-static {p1, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-static {v1, v2}, LX/470;->a(Landroid/view/Window;I)V

    .line 1158087
    :cond_1
    return-void

    .line 1158088
    :cond_2
    const v0, 0x7f0a0672

    goto :goto_0
.end method

.method public final b(Landroid/app/Activity;LX/73i;)V
    .locals 1

    .prologue
    .line 1158089
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, p2}, LX/6wr;->a(Landroid/app/Activity;ZLX/73i;)V

    .line 1158090
    return-void
.end method
