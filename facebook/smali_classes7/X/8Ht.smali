.class public final LX/8Ht;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field private a:J

.field private b:Lcom/facebook/photos/imageprocessing/FiltersEngine;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/facebook/photos/imageprocessing/FiltersEngine;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 1321632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321633
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/8Ht;->a:J

    .line 1321634
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8Ht;->c:Z

    .line 1321635
    iput-object p1, p0, LX/8Ht;->b:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    .line 1321636
    invoke-static {p2}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->b(Landroid/graphics/Bitmap;)J

    move-result-wide v0

    iput-wide v0, p0, LX/8Ht;->a:J

    .line 1321637
    return-void
.end method


# virtual methods
.method public final declared-synchronized a([Landroid/graphics/RectF;)V
    .locals 4

    .prologue
    .line 1321626
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/8Ht;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1321627
    iget-wide v0, p0, LX/8Ht;->a:J

    .line 1321628
    invoke-static {p1, v0, v1}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->preprocess([Landroid/graphics/RectF;J)V

    .line 1321629
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8Ht;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1321630
    :cond_0
    monitor-exit p0

    return-void

    .line 1321631
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/graphics/Bitmap;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 1321619
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/8Ht;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/8Ht;->c:Z

    if-eqz v0, :cond_0

    .line 1321620
    invoke-static {p2}, LX/5iL;->getValue(Ljava/lang/String;)LX/5iL;

    move-result-object v0

    invoke-virtual {v0}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v0

    .line 1321621
    iget-wide v2, p0, LX/8Ht;->a:J

    .line 1321622
    invoke-static {v2, v3, v0, p1}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a(JLjava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1321623
    const/4 v0, 0x1

    .line 1321624
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1321625
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final close()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1321614
    iget-wide v0, p0, LX/8Ht;->a:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1321615
    iget-wide v0, p0, LX/8Ht;->a:J

    .line 1321616
    invoke-static {v0, v1}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->releaseSession(J)V

    .line 1321617
    iput-wide v2, p0, LX/8Ht;->a:J

    .line 1321618
    :cond_0
    return-void
.end method
