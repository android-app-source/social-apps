.class public final LX/8Zu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 54

    .prologue
    .line 1365644
    const/16 v48, 0x0

    .line 1365645
    const/16 v47, 0x0

    .line 1365646
    const/16 v46, 0x0

    .line 1365647
    const/16 v45, 0x0

    .line 1365648
    const/16 v44, 0x0

    .line 1365649
    const/16 v43, 0x0

    .line 1365650
    const/16 v42, 0x0

    .line 1365651
    const/16 v41, 0x0

    .line 1365652
    const/16 v40, 0x0

    .line 1365653
    const/16 v39, 0x0

    .line 1365654
    const/16 v38, 0x0

    .line 1365655
    const/16 v37, 0x0

    .line 1365656
    const/16 v36, 0x0

    .line 1365657
    const/16 v35, 0x0

    .line 1365658
    const/16 v34, 0x0

    .line 1365659
    const/16 v33, 0x0

    .line 1365660
    const/16 v32, 0x0

    .line 1365661
    const/16 v31, 0x0

    .line 1365662
    const/16 v30, 0x0

    .line 1365663
    const/16 v29, 0x0

    .line 1365664
    const/16 v28, 0x0

    .line 1365665
    const/16 v27, 0x0

    .line 1365666
    const/16 v26, 0x0

    .line 1365667
    const-wide/16 v24, 0x0

    .line 1365668
    const-wide/16 v22, 0x0

    .line 1365669
    const/16 v21, 0x0

    .line 1365670
    const/16 v20, 0x0

    .line 1365671
    const/16 v19, 0x0

    .line 1365672
    const/16 v18, 0x0

    .line 1365673
    const/16 v17, 0x0

    .line 1365674
    const/16 v16, 0x0

    .line 1365675
    const/4 v15, 0x0

    .line 1365676
    const/4 v14, 0x0

    .line 1365677
    const/4 v13, 0x0

    .line 1365678
    const/4 v12, 0x0

    .line 1365679
    const/4 v11, 0x0

    .line 1365680
    const/4 v10, 0x0

    .line 1365681
    const/4 v9, 0x0

    .line 1365682
    const/4 v8, 0x0

    .line 1365683
    const/4 v7, 0x0

    .line 1365684
    const/4 v6, 0x0

    .line 1365685
    const/4 v5, 0x0

    .line 1365686
    const/4 v4, 0x0

    .line 1365687
    const/4 v3, 0x0

    .line 1365688
    const/4 v2, 0x0

    .line 1365689
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v49

    sget-object v50, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v49

    move-object/from16 v1, v50

    if-eq v0, v1, :cond_30

    .line 1365690
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1365691
    const/4 v2, 0x0

    .line 1365692
    :goto_0
    return v2

    .line 1365693
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v50, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v50

    if-eq v2, v0, :cond_23

    .line 1365694
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1365695
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1365696
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v50

    sget-object v51, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v50

    move-object/from16 v1, v51

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 1365697
    const-string v50, "__type__"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-nez v50, :cond_1

    const-string v50, "__typename"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_2

    .line 1365698
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move/from16 v49, v2

    goto :goto_1

    .line 1365699
    :cond_2
    const-string v50, "can_viewer_like"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_3

    .line 1365700
    const/4 v2, 0x1

    .line 1365701
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    move/from16 v48, v16

    move/from16 v16, v2

    goto :goto_1

    .line 1365702
    :cond_3
    const-string v50, "does_viewer_like"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_4

    .line 1365703
    const/4 v2, 0x1

    .line 1365704
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    move/from16 v47, v15

    move v15, v2

    goto :goto_1

    .line 1365705
    :cond_4
    const-string v50, "external_url"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_5

    .line 1365706
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v46, v2

    goto :goto_1

    .line 1365707
    :cond_5
    const-string v50, "height"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_6

    .line 1365708
    const/4 v2, 0x1

    .line 1365709
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    move/from16 v45, v14

    move v14, v2

    goto/16 :goto_1

    .line 1365710
    :cond_6
    const-string v50, "id"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_7

    .line 1365711
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v44, v2

    goto/16 :goto_1

    .line 1365712
    :cond_7
    const-string v50, "image"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_8

    .line 1365713
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v43, v2

    goto/16 :goto_1

    .line 1365714
    :cond_8
    const-string v50, "initial_view_heading_degrees"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_9

    .line 1365715
    const/4 v2, 0x1

    .line 1365716
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    move/from16 v42, v13

    move v13, v2

    goto/16 :goto_1

    .line 1365717
    :cond_9
    const-string v50, "initial_view_pitch_degrees"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_a

    .line 1365718
    const/4 v2, 0x1

    .line 1365719
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    move/from16 v41, v12

    move v12, v2

    goto/16 :goto_1

    .line 1365720
    :cond_a
    const-string v50, "initial_view_roll_degrees"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_b

    .line 1365721
    const/4 v2, 0x1

    .line 1365722
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    move/from16 v40, v11

    move v11, v2

    goto/16 :goto_1

    .line 1365723
    :cond_b
    const-string v50, "intermediate_image"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_c

    .line 1365724
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v39, v2

    goto/16 :goto_1

    .line 1365725
    :cond_c
    const-string v50, "is_spherical"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_d

    .line 1365726
    const/4 v2, 0x1

    .line 1365727
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v38, v7

    move v7, v2

    goto/16 :goto_1

    .line 1365728
    :cond_d
    const-string v50, "message"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_e

    .line 1365729
    invoke-static/range {p0 .. p1}, LX/8ZU;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v37, v2

    goto/16 :goto_1

    .line 1365730
    :cond_e
    const-string v50, "name"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_f

    .line 1365731
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v36, v2

    goto/16 :goto_1

    .line 1365732
    :cond_f
    const-string v50, "photo_encodings"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_10

    .line 1365733
    invoke-static/range {p0 .. p1}, LX/8ZP;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v35, v2

    goto/16 :goto_1

    .line 1365734
    :cond_10
    const-string v50, "playable_duration_in_ms"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_11

    .line 1365735
    const/4 v2, 0x1

    .line 1365736
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v34, v6

    move v6, v2

    goto/16 :goto_1

    .line 1365737
    :cond_11
    const-string v50, "playable_url"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_12

    .line 1365738
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v33, v2

    goto/16 :goto_1

    .line 1365739
    :cond_12
    const-string v50, "playable_url_hd"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_13

    .line 1365740
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v32, v2

    goto/16 :goto_1

    .line 1365741
    :cond_13
    const-string v50, "playable_url_preferred"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_14

    .line 1365742
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 1365743
    :cond_14
    const-string v50, "playlist"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_15

    .line 1365744
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 1365745
    :cond_15
    const-string v50, "preview_payload"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_16

    .line 1365746
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 1365747
    :cond_16
    const-string v50, "profilePicture50"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_17

    .line 1365748
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 1365749
    :cond_17
    const-string v50, "projection_type"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_18

    .line 1365750
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 1365751
    :cond_18
    const-string v50, "sphericalFullscreenAspectRatio"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_19

    .line 1365752
    const/4 v2, 0x1

    .line 1365753
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 1365754
    :cond_19
    const-string v50, "sphericalInlineAspectRatio"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_1a

    .line 1365755
    const/4 v2, 0x1

    .line 1365756
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v26

    move v10, v2

    goto/16 :goto_1

    .line 1365757
    :cond_1a
    const-string v50, "sphericalPlayableUrlHdString"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_1b

    .line 1365758
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 1365759
    :cond_1b
    const-string v50, "sphericalPlayableUrlSdString"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_1c

    .line 1365760
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 1365761
    :cond_1c
    const-string v50, "sphericalPlaylist"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_1d

    .line 1365762
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 1365763
    :cond_1d
    const-string v50, "sphericalPreferredFov"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_1e

    .line 1365764
    const/4 v2, 0x1

    .line 1365765
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    move/from16 v21, v9

    move v9, v2

    goto/16 :goto_1

    .line 1365766
    :cond_1e
    const-string v50, "subscribe_status"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_1f

    .line 1365767
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 1365768
    :cond_1f
    const-string v50, "url"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_20

    .line 1365769
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 1365770
    :cond_20
    const-string v50, "video_preview_image"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v50

    if-eqz v50, :cond_21

    .line 1365771
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 1365772
    :cond_21
    const-string v50, "width"

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 1365773
    const/4 v2, 0x1

    .line 1365774
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    move/from16 v17, v8

    move v8, v2

    goto/16 :goto_1

    .line 1365775
    :cond_22
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1365776
    :cond_23
    const/16 v2, 0x21

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1365777
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365778
    if-eqz v16, :cond_24

    .line 1365779
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1365780
    :cond_24
    if-eqz v15, :cond_25

    .line 1365781
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1365782
    :cond_25
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365783
    if-eqz v14, :cond_26

    .line 1365784
    const/4 v2, 0x4

    const/4 v14, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1, v14}, LX/186;->a(III)V

    .line 1365785
    :cond_26
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365786
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365787
    if-eqz v13, :cond_27

    .line 1365788
    const/4 v2, 0x7

    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1, v13}, LX/186;->a(III)V

    .line 1365789
    :cond_27
    if-eqz v12, :cond_28

    .line 1365790
    const/16 v2, 0x8

    const/4 v12, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1, v12}, LX/186;->a(III)V

    .line 1365791
    :cond_28
    if-eqz v11, :cond_29

    .line 1365792
    const/16 v2, 0x9

    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1, v11}, LX/186;->a(III)V

    .line 1365793
    :cond_29
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365794
    if-eqz v7, :cond_2a

    .line 1365795
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1365796
    :cond_2a
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365797
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365798
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365799
    if-eqz v6, :cond_2b

    .line 1365800
    const/16 v2, 0xf

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1, v6}, LX/186;->a(III)V

    .line 1365801
    :cond_2b
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365802
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365803
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365804
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365805
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365806
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365807
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365808
    if-eqz v3, :cond_2c

    .line 1365809
    const/16 v3, 0x17

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1365810
    :cond_2c
    if-eqz v10, :cond_2d

    .line 1365811
    const/16 v3, 0x18

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v26

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1365812
    :cond_2d
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365813
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365814
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365815
    if-eqz v9, :cond_2e

    .line 1365816
    const/16 v2, 0x1c

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1365817
    :cond_2e
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365818
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365819
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1365820
    if-eqz v8, :cond_2f

    .line 1365821
    const/16 v2, 0x20

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1365822
    :cond_2f
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_30
    move/from16 v49, v48

    move/from16 v48, v47

    move/from16 v47, v46

    move/from16 v46, v45

    move/from16 v45, v44

    move/from16 v44, v43

    move/from16 v43, v42

    move/from16 v42, v41

    move/from16 v41, v40

    move/from16 v40, v39

    move/from16 v39, v38

    move/from16 v38, v37

    move/from16 v37, v36

    move/from16 v36, v35

    move/from16 v35, v34

    move/from16 v34, v33

    move/from16 v33, v32

    move/from16 v32, v31

    move/from16 v31, v30

    move/from16 v30, v29

    move/from16 v29, v28

    move/from16 v28, v27

    move/from16 v52, v26

    move-wide/from16 v26, v22

    move/from16 v23, v20

    move/from16 v22, v19

    move/from16 v20, v17

    move/from16 v19, v16

    move/from16 v17, v14

    move/from16 v16, v13

    move v14, v11

    move v13, v10

    move v11, v8

    move v10, v4

    move v8, v2

    move/from16 v53, v12

    move v12, v9

    move v9, v3

    move v3, v5

    move-wide/from16 v4, v24

    move/from16 v25, v52

    move/from16 v24, v21

    move/from16 v21, v18

    move/from16 v18, v15

    move/from16 v15, v53

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x1d

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1365823
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1365824
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1365825
    if-eqz v0, :cond_0

    .line 1365826
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365827
    invoke-static {p0, p1, v3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1365828
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1365829
    if-eqz v0, :cond_1

    .line 1365830
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365831
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1365832
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1365833
    if-eqz v0, :cond_2

    .line 1365834
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365835
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1365836
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1365837
    if-eqz v0, :cond_3

    .line 1365838
    const-string v1, "external_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365839
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365840
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1365841
    if-eqz v0, :cond_4

    .line 1365842
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365843
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1365844
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1365845
    if-eqz v0, :cond_5

    .line 1365846
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365847
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365848
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1365849
    if-eqz v0, :cond_6

    .line 1365850
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365851
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1365852
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1365853
    if-eqz v0, :cond_7

    .line 1365854
    const-string v1, "initial_view_heading_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365855
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1365856
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1365857
    if-eqz v0, :cond_8

    .line 1365858
    const-string v1, "initial_view_pitch_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365859
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1365860
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1365861
    if-eqz v0, :cond_9

    .line 1365862
    const-string v1, "initial_view_roll_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365863
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1365864
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1365865
    if-eqz v0, :cond_a

    .line 1365866
    const-string v1, "intermediate_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365867
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1365868
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1365869
    if-eqz v0, :cond_b

    .line 1365870
    const-string v1, "is_spherical"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365871
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1365872
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1365873
    if-eqz v0, :cond_c

    .line 1365874
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365875
    invoke-static {p0, v0, p2}, LX/8ZU;->a(LX/15i;ILX/0nX;)V

    .line 1365876
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1365877
    if-eqz v0, :cond_d

    .line 1365878
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365879
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365880
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1365881
    if-eqz v0, :cond_e

    .line 1365882
    const-string v1, "photo_encodings"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365883
    invoke-static {p0, v0, p2, p3}, LX/8ZP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1365884
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1365885
    if-eqz v0, :cond_f

    .line 1365886
    const-string v1, "playable_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365887
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1365888
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1365889
    if-eqz v0, :cond_10

    .line 1365890
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365891
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365892
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1365893
    if-eqz v0, :cond_11

    .line 1365894
    const-string v1, "playable_url_hd"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365895
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365896
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1365897
    if-eqz v0, :cond_12

    .line 1365898
    const-string v1, "playable_url_preferred"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365899
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365900
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1365901
    if-eqz v0, :cond_13

    .line 1365902
    const-string v1, "playlist"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365903
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365904
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1365905
    if-eqz v0, :cond_14

    .line 1365906
    const-string v1, "preview_payload"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365907
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365908
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1365909
    if-eqz v0, :cond_15

    .line 1365910
    const-string v1, "profilePicture50"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365911
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1365912
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1365913
    if-eqz v0, :cond_16

    .line 1365914
    const-string v1, "projection_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365915
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365916
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1365917
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_17

    .line 1365918
    const-string v2, "sphericalFullscreenAspectRatio"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365919
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1365920
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1365921
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_18

    .line 1365922
    const-string v2, "sphericalInlineAspectRatio"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365923
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1365924
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1365925
    if-eqz v0, :cond_19

    .line 1365926
    const-string v1, "sphericalPlayableUrlHdString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365927
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365928
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1365929
    if-eqz v0, :cond_1a

    .line 1365930
    const-string v1, "sphericalPlayableUrlSdString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365931
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365932
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1365933
    if-eqz v0, :cond_1b

    .line 1365934
    const-string v1, "sphericalPlaylist"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365935
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365936
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1365937
    if-eqz v0, :cond_1c

    .line 1365938
    const-string v1, "sphericalPreferredFov"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365939
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1365940
    :cond_1c
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1365941
    if-eqz v0, :cond_1d

    .line 1365942
    const-string v0, "subscribe_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365943
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365944
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1365945
    if-eqz v0, :cond_1e

    .line 1365946
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365947
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1365948
    :cond_1e
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1365949
    if-eqz v0, :cond_1f

    .line 1365950
    const-string v1, "video_preview_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365951
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1365952
    :cond_1f
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1365953
    if-eqz v0, :cond_20

    .line 1365954
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365955
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1365956
    :cond_20
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1365957
    return-void
.end method
