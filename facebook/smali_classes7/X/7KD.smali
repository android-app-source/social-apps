.class public final LX/7KD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:J

.field public c:LX/04g;

.field public d:LX/1C8;

.field private final e:LX/0So;

.field private f:J


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 1194590
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194591
    iput-wide v0, p0, LX/7KD;->a:J

    .line 1194592
    iput-wide v0, p0, LX/7KD;->b:J

    .line 1194593
    sget-object v0, LX/1C8;->ERROR:LX/1C8;

    iput-object v0, p0, LX/7KD;->d:LX/1C8;

    .line 1194594
    iput-object p1, p0, LX/7KD;->e:LX/0So;

    .line 1194595
    return-void
.end method

.method private static a(Lcom/facebook/analytics/logger/HoneyClientEvent;)J
    .locals 4

    .prologue
    .line 1194596
    sget-object v0, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    invoke-static {p0, v0}, LX/7KD;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/04F;)Ljava/lang/String;

    move-result-object v0

    .line 1194597
    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/04F;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1194598
    iget-object v0, p1, LX/04F;->value:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/facebook/analytics/logger/HoneyClientEvent;)J
    .locals 4

    .prologue
    .line 1194599
    sget-object v0, LX/04F;->LAST_START_POSITION_PARAM:LX/04F;

    invoke-static {p0, v0}, LX/7KD;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/04F;)Ljava/lang/String;

    move-result-object v0

    .line 1194600
    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    double-to-long v0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;ZLjava/util/List;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;",
            "Z",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 1194601
    iget-object v0, p1, Lcom/facebook/analytics/HoneyAnalyticsEvent;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1194602
    invoke-static {v0}, LX/04A;->asEvent(Ljava/lang/String;)LX/04A;

    move-result-object v1

    .line 1194603
    sget-object v0, LX/1C7;->g:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 1194604
    if-nez v0, :cond_0

    .line 1194605
    const/4 v0, 0x1

    .line 1194606
    :goto_0
    return v0

    .line 1194607
    :cond_0
    const/4 v0, 0x1

    .line 1194608
    sget-object v2, LX/7KC;->a:[I

    iget-object v3, p0, LX/7KD;->d:LX/1C8;

    invoke-virtual {v3}, LX/1C8;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1194609
    :cond_1
    :goto_1
    invoke-static {v1}, LX/1C7;->f(LX/04A;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1194610
    sget-object v2, LX/1C8;->PLAYING:LX/1C8;

    .line 1194611
    :goto_2
    move-object v2, v2

    .line 1194612
    if-eqz p2, :cond_6

    iget-object v3, p0, LX/7KD;->d:LX/1C8;

    if-eq v2, v3, :cond_6

    .line 1194613
    invoke-static {v1}, LX/1C7;->f(LX/04A;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1194614
    invoke-static {p1}, LX/7KD;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)J

    move-result-wide v4

    .line 1194615
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gez v3, :cond_2

    .line 1194616
    new-instance v0, Landroid/util/Pair;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Invalid metadata: "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v6, "Video time position is negative on play event: %d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v3, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1194617
    const/4 v0, 0x0

    .line 1194618
    :cond_2
    iget-object v3, p0, LX/7KD;->e:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v6

    iput-wide v6, p0, LX/7KD;->f:J

    .line 1194619
    iput-wide v4, p0, LX/7KD;->a:J

    .line 1194620
    iput-wide v4, p0, LX/7KD;->b:J

    .line 1194621
    sget-object v3, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    invoke-static {p1, v3}, LX/7KD;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/04F;)Ljava/lang/String;

    move-result-object v3

    .line 1194622
    if-nez v3, :cond_f

    const/4 v3, 0x0

    :goto_3
    move-object v3, v3

    .line 1194623
    iput-object v3, p0, LX/7KD;->c:LX/04g;

    .line 1194624
    iget-object v3, p0, LX/7KD;->c:LX/04g;

    sget-object v4, LX/04g;->BY_AUTOPLAY:LX/04g;

    if-eq v3, v4, :cond_3

    iget-object v3, p0, LX/7KD;->c:LX/04g;

    sget-object v4, LX/04g;->BY_USER:LX/04g;

    if-eq v3, v4, :cond_3

    .line 1194625
    new-instance v0, Landroid/util/Pair;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid metadata: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/04F;->VIDEO_PLAY_REASON:LX/04F;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Invalid value for Video Play Reason: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, LX/7KD;->c:LX/04g;

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1194626
    const/4 v0, 0x0

    .line 1194627
    :cond_3
    invoke-static {v1}, LX/1C7;->g(LX/04A;)Z

    move-result v3

    if-eqz v3, :cond_b

    sget-object v3, LX/04A;->VIDEO_CANCELLED_REQUESTED_PLAYING:LX/04A;

    if-eq v1, v3, :cond_b

    .line 1194628
    invoke-static {p1}, LX/7KD;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)J

    move-result-wide v4

    iput-wide v4, p0, LX/7KD;->b:J

    .line 1194629
    iget-wide v4, p0, LX/7KD;->b:J

    iget-wide v6, p0, LX/7KD;->a:J

    cmp-long v1, v4, v6

    if-gez v1, :cond_4

    .line 1194630
    new-instance v0, Landroid/util/Pair;

    const-string v1, "Metadata inconcistency: Pause Time < Last Start Time"

    const-string v3, "Pause Time (%d) is smaller than Last Start Time (%d)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, p0, LX/7KD;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-wide v6, p0, LX/7KD;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1194631
    const/4 v0, 0x0

    .line 1194632
    :cond_4
    invoke-static {p1}, LX/7KD;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;)J

    move-result-wide v4

    .line 1194633
    iget-wide v6, p0, LX/7KD;->a:J

    cmp-long v1, v6, v4

    if-eqz v1, :cond_5

    .line 1194634
    new-instance v0, Landroid/util/Pair;

    const-string v1, "Metadata inconcistency: Event LST != State Machine LST"

    const-string v3, "Event Last Start Time (%d) is different than state machine Last Start Time (%d)"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v7

    const/4 v4, 0x1

    iget-wide v8, p0, LX/7KD;->a:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v6, v4

    invoke-static {v3, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1194635
    const/4 v0, 0x0

    .line 1194636
    :cond_5
    iget-object v1, p0, LX/7KD;->e:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    .line 1194637
    iget-wide v6, p0, LX/7KD;->b:J

    iget-wide v8, p0, LX/7KD;->a:J

    sub-long/2addr v6, v8

    long-to-double v6, v6

    const-wide v8, 0x3ff028f5c28f5c29L    # 1.01

    iget-wide v10, p0, LX/7KD;->f:J

    sub-long v10, v4, v10

    long-to-double v10, v10

    mul-double/2addr v8, v10

    cmpl-double v1, v6, v8

    if-lez v1, :cond_b

    .line 1194638
    new-instance v0, Landroid/util/Pair;

    const-string v1, "Metadata inconcistency: Watch time interval > system clock difference"

    const-string v3, "Watch Time interval duration (%d) is greater than system clock difference for the period (%d)"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-wide v8, p0, LX/7KD;->b:J

    iget-wide v10, p0, LX/7KD;->a:J

    sub-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-wide v8, p0, LX/7KD;->f:J

    sub-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v7

    invoke-static {v3, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1194639
    const/4 v0, 0x0

    move v1, v0

    .line 1194640
    :goto_4
    sget-object v0, LX/04F;->PLAYER_ORIGIN:LX/04F;

    invoke-static {p1, v0}, LX/7KD;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/04F;)Ljava/lang/String;

    move-result-object v3

    .line 1194641
    sget-object v0, LX/1C7;->h:Ljava/util/Map;

    sget-object v4, LX/04F;->PLAYER_ORIGIN:LX/04F;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1194642
    new-instance v0, Landroid/util/Pair;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Invalid metadata: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, LX/04F;->PLAYER_ORIGIN:LX/04F;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "Invalid value for Player Origin: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1194643
    const/4 v0, 0x0

    .line 1194644
    :cond_6
    :goto_5
    iput-object v2, p0, LX/7KD;->d:LX/1C8;

    goto/16 :goto_0

    .line 1194645
    :pswitch_0
    invoke-static {v1}, LX/1C7;->h(LX/04A;)Z

    .line 1194646
    invoke-static {v1}, LX/1C7;->g(LX/04A;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, LX/04A;->VIDEO_CANCELLED_REQUESTED_PLAYING:LX/04A;

    if-eq v1, v2, :cond_1

    .line 1194647
    new-instance v0, Landroid/util/Pair;

    const-string v2, "Invalid state transition: REQUESTED_PLAY -> PAUSED"

    const-string v3, "In REQUESTED_PLAY state but received a pause event that is not %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v6, LX/04A;->VIDEO_CANCELLED_REQUESTED_PLAYING:LX/04A;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, v1, LX/04A;->value:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1194648
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1194649
    :pswitch_1
    invoke-static {v1}, LX/1C7;->f(LX/04A;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1194650
    new-instance v0, Landroid/util/Pair;

    const-string v2, "Invalid state transition: PLAYING -> PLAYING"

    const-string v3, "In PLAYING state but received a PLAYING event: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v1, LX/04A;->value:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1194651
    const/4 v0, 0x0

    .line 1194652
    :cond_7
    invoke-static {v1}, LX/1C7;->h(LX/04A;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1194653
    new-instance v0, Landroid/util/Pair;

    const-string v2, "Invalid state transition: PLAYING -> REQUESTED_PLAY"

    const-string v3, "In PLAYING state but received a REQUESTED_PLAY event: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v1, LX/04A;->value:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1194654
    const/4 v0, 0x0

    .line 1194655
    :cond_8
    sget-object v2, LX/1C7;->f:Ljava/util/Map;

    sget-object v3, LX/1C8;->SEEKING:LX/1C8;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    move v2, v2

    .line 1194656
    if-eqz v2, :cond_1

    .line 1194657
    new-instance v0, Landroid/util/Pair;

    const-string v2, "Invalid state transition: PLAYING -> SEEKING"

    const-string v3, "In PLAYING state but received SEEKING event: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v1, LX/04A;->value:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1194658
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1194659
    :pswitch_2
    invoke-static {v1}, LX/1C7;->f(LX/04A;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1194660
    new-instance v0, Landroid/util/Pair;

    const-string v2, "Invalid state transition: PAUSED -> PLAYING"

    const-string v3, "In PAUSED state but received PLAYING event: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v1, LX/04A;->value:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1194661
    const/4 v0, 0x0

    .line 1194662
    :cond_9
    invoke-static {v1}, LX/1C7;->g(LX/04A;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1194663
    new-instance v0, Landroid/util/Pair;

    const-string v2, "Invalid state transition: PAUSED -> PAUSED"

    const-string v3, "In PAUSED state but received another PAUSED event: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v1, LX/04A;->value:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1194664
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_a
    move v0, v1

    goto/16 :goto_5

    :cond_b
    move v1, v0

    goto/16 :goto_4

    .line 1194665
    :cond_c
    invoke-static {v1}, LX/1C7;->g(LX/04A;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1194666
    sget-object v2, LX/1C8;->PAUSED:LX/1C8;

    goto/16 :goto_2

    .line 1194667
    :cond_d
    invoke-static {v1}, LX/1C7;->h(LX/04A;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1194668
    sget-object v2, LX/1C8;->REQUESTED_PLAY:LX/1C8;

    goto/16 :goto_2

    .line 1194669
    :cond_e
    sget-object v2, LX/1C8;->ERROR:LX/1C8;

    goto/16 :goto_2

    :cond_f
    invoke-static {v3}, LX/04g;->asEventTriggerType(Ljava/lang/String;)LX/04g;

    move-result-object v3

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
