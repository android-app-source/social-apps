.class public final LX/6ui;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6uV;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6uV",
        "<",
        "Lcom/facebook/payments/confirmation/SimpleConfirmationData;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1156118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156119
    iput-object p1, p0, LX/6ui;->a:Landroid/content/res/Resources;

    .line 1156120
    return-void
.end method

.method private a(LX/0Pz;LX/6uT;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/6uG;",
            ">;",
            "LX/6uT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1156086
    new-instance v0, LX/6ug;

    invoke-direct {v0, p0, p2}, LX/6ug;-><init>(LX/6ui;LX/6uT;)V

    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1156087
    return-void
.end method

.method public static b(LX/0QB;)LX/6ui;
    .locals 2

    .prologue
    .line 1156088
    new-instance v1, LX/6ui;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/6ui;-><init>(Landroid/content/res/Resources;)V

    .line 1156089
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/confirmation/ConfirmationData;)LX/0Px;
    .locals 6

    .prologue
    .line 1156090
    check-cast p1, Lcom/facebook/payments/confirmation/SimpleConfirmationData;

    const/4 v2, 0x0

    .line 1156091
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1156092
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1156093
    sget-object v1, LX/6uT;->CHECK_MARK:LX/6uT;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1156094
    sget-object v1, LX/6uT;->PRODUCT_PURCHASE_SECTION:LX/6uT;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1156095
    sget-object v1, LX/6uT;->DIVIDER:LX/6uT;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1156096
    sget-object v1, LX/6uT;->SEE_RECEIPT:LX/6uT;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1156097
    invoke-virtual {p1}, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->a()Lcom/facebook/payments/confirmation/ConfirmationParams;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/payments/confirmation/ConfirmationParams;->a()Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    move-result-object v1

    iget-boolean v1, v1, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->b:Z

    if-eqz v1, :cond_0

    .line 1156098
    sget-object v1, LX/6uT;->ACTIVATE_SECURITY_PIN:LX/6uT;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1156099
    :cond_0
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v5, v0

    .line 1156100
    move v1, v2

    .line 1156101
    :goto_0
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1156102
    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6uT;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_1

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {p0, v4, v0, p1, v3}, LX/6ui;->a(LX/0Pz;LX/6uT;Lcom/facebook/payments/confirmation/SimpleConfirmationData;Z)V

    .line 1156103
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v3, v2

    .line 1156104
    goto :goto_1

    .line 1156105
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Pz;LX/6uT;Lcom/facebook/payments/confirmation/SimpleConfirmationData;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/6uG;",
            ">;",
            "LX/6uT;",
            "Lcom/facebook/payments/confirmation/SimpleConfirmationData;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1156106
    sget-object v0, LX/6uh;->a:[I

    invoke-virtual {p2}, LX/6uT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1156107
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1156108
    :pswitch_0
    new-instance v0, LX/6uI;

    iget-object v1, p0, LX/6ui;->a:Landroid/content/res/Resources;

    const v2, 0x7f081e20

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/6uT;->ACTIVATE_SECURITY_PIN:LX/6uT;

    .line 1156109
    iget-object p0, p3, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->a:LX/0Rf;

    invoke-virtual {p0, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result p0

    move v2, p0

    .line 1156110
    invoke-direct {v0, v1, p4, v2}, LX/6uI;-><init>(Ljava/lang/String;ZZ)V

    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1156111
    :goto_0
    return-void

    .line 1156112
    :pswitch_1
    sget-object v0, LX/6uT;->CHECK_MARK:LX/6uT;

    invoke-direct {p0, p1, v0}, LX/6ui;->a(LX/0Pz;LX/6uT;)V

    goto :goto_0

    .line 1156113
    :pswitch_2
    sget-object v0, LX/6uT;->DIVIDER:LX/6uT;

    invoke-direct {p0, p1, v0}, LX/6ui;->a(LX/0Pz;LX/6uT;)V

    goto :goto_0

    .line 1156114
    :pswitch_3
    new-instance v0, LX/6um;

    iget-object v1, p0, LX/6ui;->a:Landroid/content/res/Resources;

    const v2, 0x7f081e21

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6um;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1156115
    goto :goto_0

    .line 1156116
    :pswitch_4
    new-instance v0, LX/6uZ;

    invoke-virtual {p3}, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->a()Lcom/facebook/payments/confirmation/ConfirmationParams;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/payments/confirmation/ConfirmationParams;->a()Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->e:Ljava/lang/String;

    iget-object v2, p0, LX/6ui;->a:Landroid/content/res/Resources;

    const p2, 0x7f081e1f

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p4}, LX/6uZ;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1156117
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
