.class public final LX/6x0;
.super LX/6oO;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/payments/form/model/FormFieldAttributes;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/6x2;


# direct methods
.method public constructor <init>(LX/6x2;ILcom/facebook/payments/form/model/FormFieldAttributes;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1158286
    iput-object p1, p0, LX/6x0;->d:LX/6x2;

    iput p2, p0, LX/6x0;->a:I

    iput-object p3, p0, LX/6x0;->b:Lcom/facebook/payments/form/model/FormFieldAttributes;

    iput-object p4, p0, LX/6x0;->c:Ljava/lang/String;

    invoke-direct {p0}, LX/6oO;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    .line 1158287
    iget-object v0, p0, LX/6x0;->d:LX/6x2;

    iget-object v0, v0, LX/6x2;->b:LX/6x7;

    iget v1, p0, LX/6x0;->a:I

    iget-object v2, p0, LX/6x0;->b:Lcom/facebook/payments/form/model/FormFieldAttributes;

    iget-object v2, v2, Lcom/facebook/payments/form/model/FormFieldAttributes;->d:LX/6xe;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/6x7;->a(ILX/6xe;Ljava/lang/String;)V

    .line 1158288
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1158289
    iget-object v0, p0, LX/6x0;->d:LX/6x2;

    iget-object v0, v0, LX/6x2;->c:Landroid/content/Intent;

    iget-object v1, p0, LX/6x0;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1158290
    :goto_0
    iget-object v0, p0, LX/6x0;->d:LX/6x2;

    iget-object v0, v0, LX/6x2;->d:LX/6x9;

    iget-object v1, p0, LX/6x0;->d:LX/6x2;

    invoke-virtual {v1}, LX/6x2;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/6x9;->a(Z)V

    .line 1158291
    return-void

    .line 1158292
    :cond_0
    iget-object v0, p0, LX/6x0;->d:LX/6x2;

    iget-object v0, v0, LX/6x2;->c:Landroid/content/Intent;

    iget-object v1, p0, LX/6x0;->c:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method
