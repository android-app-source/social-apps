.class public final LX/76x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1171483
    iput-object p1, p0, LX/76x;->c:Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;

    iput-boolean p2, p0, LX/76x;->a:Z

    iput-object p3, p0, LX/76x;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    .line 1171484
    iget-boolean v0, p0, LX/76x;->a:Z

    if-eqz v0, :cond_0

    .line 1171485
    new-instance v0, Landroid/widget/EditText;

    iget-object v1, p0, LX/76x;->c:Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1171486
    iget-object v1, p0, LX/76x;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1171487
    new-instance v1, LX/0ju;

    iget-object v2, p0, LX/76x;->c:Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    const-string v2, "Replace parameters"

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const-string v2, "Ok"

    new-instance v3, LX/76w;

    invoke-direct {v3, p0, v0}, LX/76w;-><init>(LX/76x;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const-string v1, "Cancel"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 1171488
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1171489
    :cond_0
    iget-object v0, p0, LX/76x;->c:Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;

    iget-object v0, v0, Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;->a:LX/17W;

    iget-object v1, p0, LX/76x;->c:Lcom/facebook/quickpromotion/debug/SeguePreviewSettingsActivity;

    iget-object v2, p0, LX/76x;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0
.end method
