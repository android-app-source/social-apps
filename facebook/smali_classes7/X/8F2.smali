.class public final LX/8F2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 1316573
    const/16 v17, 0x0

    .line 1316574
    const/16 v16, 0x0

    .line 1316575
    const/4 v15, 0x0

    .line 1316576
    const/4 v14, 0x0

    .line 1316577
    const/4 v13, 0x0

    .line 1316578
    const/4 v12, 0x0

    .line 1316579
    const/4 v11, 0x0

    .line 1316580
    const/4 v10, 0x0

    .line 1316581
    const/4 v9, 0x0

    .line 1316582
    const/4 v8, 0x0

    .line 1316583
    const/4 v7, 0x0

    .line 1316584
    const/4 v6, 0x0

    .line 1316585
    const/4 v5, 0x0

    .line 1316586
    const/4 v4, 0x0

    .line 1316587
    const/4 v3, 0x0

    .line 1316588
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 1316589
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1316590
    const/4 v3, 0x0

    .line 1316591
    :goto_0
    return v3

    .line 1316592
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1316593
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_c

    .line 1316594
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v18

    .line 1316595
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1316596
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    if-eqz v18, :cond_1

    .line 1316597
    const-string v19, "__typename"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 1316598
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto :goto_1

    .line 1316599
    :cond_2
    const-string v19, "has_services"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 1316600
    const/4 v6, 0x1

    .line 1316601
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto :goto_1

    .line 1316602
    :cond_3
    const-string v19, "is_messaging_enabled"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 1316603
    const/4 v5, 0x1

    .line 1316604
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto :goto_1

    .line 1316605
    :cond_4
    const-string v19, "is_service_enabled"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 1316606
    const/4 v4, 0x1

    .line 1316607
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto :goto_1

    .line 1316608
    :cond_5
    const-string v19, "is_service_published"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 1316609
    const/4 v3, 0x1

    .line 1316610
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto :goto_1

    .line 1316611
    :cond_6
    const-string v19, "messaging_setup_image"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 1316612
    invoke-static/range {p0 .. p1}, LX/8F0;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 1316613
    :cond_7
    const-string v19, "messaging_setup_subtitle"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 1316614
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 1316615
    :cond_8
    const-string v19, "messaging_setup_title"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 1316616
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 1316617
    :cond_9
    const-string v19, "service_setup_image"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 1316618
    invoke-static/range {p0 .. p1}, LX/8F1;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 1316619
    :cond_a
    const-string v19, "service_setup_subtitle"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 1316620
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 1316621
    :cond_b
    const-string v19, "service_setup_title"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1316622
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 1316623
    :cond_c
    const/16 v18, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1316624
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1316625
    if-eqz v6, :cond_d

    .line 1316626
    const/4 v6, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1316627
    :cond_d
    if-eqz v5, :cond_e

    .line 1316628
    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, LX/186;->a(IZ)V

    .line 1316629
    :cond_e
    if-eqz v4, :cond_f

    .line 1316630
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->a(IZ)V

    .line 1316631
    :cond_f
    if-eqz v3, :cond_10

    .line 1316632
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->a(IZ)V

    .line 1316633
    :cond_10
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1316634
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1316635
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1316636
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1316637
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1316638
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1316639
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
