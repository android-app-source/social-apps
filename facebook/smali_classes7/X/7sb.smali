.class public final LX/7sb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const-wide/16 v4, 0x0

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1265340
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1265341
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1265342
    :goto_0
    return v1

    .line 1265343
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_4

    .line 1265344
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1265345
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1265346
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v10, :cond_0

    .line 1265347
    const-string v11, "creation_time"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1265348
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v7

    goto :goto_1

    .line 1265349
    :cond_1
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1265350
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1265351
    :cond_2
    const-string v11, "tickets_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1265352
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    move v8, v6

    move v6, v7

    goto :goto_1

    .line 1265353
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1265354
    :cond_4
    const/4 v10, 0x3

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1265355
    if-eqz v0, :cond_5

    move-object v0, p1

    .line 1265356
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1265357
    :cond_5
    invoke-virtual {p1, v7, v9}, LX/186;->b(II)V

    .line 1265358
    if-eqz v6, :cond_6

    .line 1265359
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8, v1}, LX/186;->a(III)V

    .line 1265360
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v6, v1

    move v0, v1

    move v8, v1

    move v9, v1

    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1265361
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1265362
    invoke-virtual {p0, p1, v3, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1265363
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 1265364
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265365
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1265366
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1265367
    if-eqz v0, :cond_1

    .line 1265368
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265369
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1265370
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1265371
    if-eqz v0, :cond_2

    .line 1265372
    const-string v1, "tickets_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265373
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1265374
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1265375
    return-void
.end method
