.class public LX/7i2;
.super LX/1Bp;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/String;

.field private static volatile d:LX/7i2;


# instance fields
.field private final c:LX/1Bn;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1226905
    const-class v0, LX/7i2;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7i2;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Bn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1226821
    invoke-direct {p0}, LX/1Bp;-><init>()V

    .line 1226822
    iput-object p1, p0, LX/7i2;->c:LX/1Bn;

    .line 1226823
    return-void
.end method

.method public static a(LX/0QB;)LX/7i2;
    .locals 4

    .prologue
    .line 1226824
    sget-object v0, LX/7i2;->d:LX/7i2;

    if-nez v0, :cond_1

    .line 1226825
    const-class v1, LX/7i2;

    monitor-enter v1

    .line 1226826
    :try_start_0
    sget-object v0, LX/7i2;->d:LX/7i2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1226827
    if-eqz v2, :cond_0

    .line 1226828
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1226829
    new-instance p0, LX/7i2;

    invoke-static {v0}, LX/1Bn;->a(LX/0QB;)LX/1Bn;

    move-result-object v3

    check-cast v3, LX/1Bn;

    invoke-direct {p0, v3}, LX/7i2;-><init>(LX/1Bn;)V

    .line 1226830
    move-object v0, p0

    .line 1226831
    sput-object v0, LX/7i2;->d:LX/7i2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1226832
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1226833
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1226834
    :cond_1
    sget-object v0, LX/7i2;->d:LX/7i2;

    return-object v0

    .line 1226835
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1226836
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/3nA;)LX/1Bx;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/16 v7, 0xc8

    .line 1226837
    check-cast p1, LX/7i1;

    .line 1226838
    iget-object v0, p1, LX/7i1;->a:Ljava/net/HttpURLConnection;

    .line 1226839
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    .line 1226840
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v2

    .line 1226841
    const/4 v6, 0x0

    .line 1226842
    new-instance v5, LX/7i0;

    invoke-direct {v5}, LX/7i0;-><init>()V

    .line 1226843
    if-nez v2, :cond_9

    move-object v3, v5

    .line 1226844
    :goto_0
    move-object v5, v3

    .line 1226845
    iget-boolean v2, p1, LX/7i1;->f:Z

    iget-object v3, p1, LX/7i1;->c:LX/1Bt;

    iget-object v4, p1, LX/7i1;->e:Ljava/lang/String;

    iget-object v6, v5, LX/7i0;->d:Ljava/util/List;

    invoke-virtual {p0, v2, v3, v4, v6}, LX/1Bp;->a(ZLX/1Bt;Ljava/lang/String;Ljava/util/List;)V

    .line 1226846
    invoke-static {v1}, LX/3C8;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1226847
    iget-object v2, v5, LX/7i0;->e:Ljava/lang/String;

    .line 1226848
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1226849
    new-instance v0, LX/1Bx;

    invoke-direct {v0, v2, v1}, LX/1Bx;-><init>(Ljava/lang/String;I)V

    .line 1226850
    :cond_0
    :goto_1
    return-object v0

    .line 1226851
    :cond_1
    new-instance v0, Lorg/apache/http/client/ClientProtocolException;

    const-string v1, "Redirect without location"

    invoke-direct {v0, v1}, Lorg/apache/http/client/ClientProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1226852
    :cond_2
    if-eq v1, v7, :cond_3

    .line 1226853
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    .line 1226854
    new-instance v0, LX/1Bx;

    invoke-direct {v0, v1}, LX/1Bx;-><init>(I)V

    .line 1226855
    sget-object v1, LX/1Bu;->ERROR_RESPONSE:LX/1Bu;

    iput-object v1, v0, LX/1Bx;->f:LX/1Bu;

    goto :goto_1

    .line 1226856
    :cond_3
    iget-boolean v1, p1, LX/7i1;->f:Z

    if-eqz v1, :cond_4

    .line 1226857
    const/4 v0, 0x0

    goto :goto_1

    .line 1226858
    :cond_4
    iget-object v1, p1, LX/7i1;->c:LX/1Bt;

    if-nez v1, :cond_5

    .line 1226859
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "mPrefetchRequest can\'t be null for non-click request"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1226860
    :cond_5
    iget-object v1, p1, LX/7i1;->c:LX/1Bt;

    iget-boolean v1, v1, LX/1Bt;->c:Z

    .line 1226861
    if-eqz v1, :cond_6

    iget-object v2, v5, LX/7i0;->a:Ljava/lang/String;

    invoke-static {v2}, LX/3C8;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_6
    if-nez v1, :cond_8

    iget-object v1, v5, LX/7i0;->a:Ljava/lang/String;

    invoke-static {v1}, LX/3C8;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1226862
    :cond_7
    new-instance v0, LX/1Bx;

    invoke-direct {v0, v7}, LX/1Bx;-><init>(I)V

    .line 1226863
    iput-object v5, v0, LX/1Bx;->c:LX/7i0;

    .line 1226864
    sget-object v1, LX/1Bu;->NOT_HTML:LX/1Bu;

    iput-object v1, v0, LX/1Bx;->f:LX/1Bu;

    goto :goto_1

    .line 1226865
    :cond_8
    const v1, 0x34e0d012

    invoke-static {v0, v1}, LX/04e;->b(Ljava/net/URLConnection;I)Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p1, LX/7i1;->b:Ljava/io/InputStream;

    .line 1226866
    iget-object v0, p0, LX/7i2;->c:LX/1Bn;

    iget-object v1, p1, LX/7i1;->c:LX/1Bt;

    iget-object v2, p1, LX/7i1;->d:Ljava/lang/String;

    iget-object v3, p1, LX/7i1;->e:Ljava/lang/String;

    iget-object v4, p1, LX/7i1;->b:Ljava/io/InputStream;

    invoke-virtual/range {v0 .. v5}, LX/1Bn;->a(LX/1Bt;Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;LX/7i0;)LX/1By;

    move-result-object v1

    .line 1226867
    new-instance v0, LX/1Bx;

    iget-object v2, p1, LX/7i1;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, LX/1Bx;-><init>(LX/1By;Ljava/lang/String;)V

    .line 1226868
    iput-object v5, v0, LX/1Bx;->c:LX/7i0;

    .line 1226869
    if-nez v1, :cond_0

    .line 1226870
    sget-object v1, LX/1Bu;->STORE_ERROR:LX/1Bu;

    iput-object v1, v0, LX/1Bx;->f:LX/1Bu;

    goto :goto_1

    .line 1226871
    :cond_9
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_a
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 1226872
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1226873
    if-eqz v4, :cond_a

    .line 1226874
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 1226875
    if-eqz v3, :cond_a

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_a

    .line 1226876
    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    const/4 v4, -0x1

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_b
    :goto_3
    packed-switch v4, :pswitch_data_0

    goto :goto_2

    .line 1226877
    :pswitch_0
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3, v5}, LX/3C8;->b(Ljava/lang/String;LX/7i0;)V

    goto :goto_2

    .line 1226878
    :sswitch_0
    const-string v10, "content-type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    move v4, v6

    goto :goto_3

    :sswitch_1
    const-string v10, "cache-control"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    const/4 v4, 0x1

    goto :goto_3

    :sswitch_2
    const-string v10, "set-cookie"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    const/4 v4, 0x2

    goto :goto_3

    :sswitch_3
    const-string v10, "location"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    const/4 v4, 0x3

    goto :goto_3

    .line 1226879
    :pswitch_1
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3, v5}, LX/3C8;->c(Ljava/lang/String;LX/7i0;)V

    goto :goto_2

    .line 1226880
    :pswitch_2
    iput-object v3, v5, LX/7i0;->d:Ljava/util/List;

    goto :goto_2

    .line 1226881
    :pswitch_3
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3, v5}, LX/3C8;->a(Ljava/lang/String;LX/7i0;)V

    goto :goto_2

    :cond_c
    move-object v3, v5

    .line 1226882
    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0xc71a9ee -> :sswitch_1
        0x2ed4600e -> :sswitch_0
        0x49be662f -> :sswitch_2
        0x714f9fb5 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/1Bt;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;)LX/3nA;
    .locals 4
    .param p1    # LX/1Bt;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Bt;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/3nA;"
        }
    .end annotation

    .prologue
    const/16 v2, 0x2710

    const/4 v1, 0x0

    .line 1226883
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1226884
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 1226885
    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 1226886
    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 1226887
    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 1226888
    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 1226889
    invoke-interface {p5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1226890
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1226891
    :cond_0
    new-instance v1, LX/7i1;

    invoke-direct {v1, v0}, LX/7i1;-><init>(Ljava/net/HttpURLConnection;)V

    .line 1226892
    iput-object p1, v1, LX/7i1;->c:LX/1Bt;

    .line 1226893
    iput-object p2, v1, LX/7i1;->d:Ljava/lang/String;

    .line 1226894
    iput-object p3, v1, LX/7i1;->e:Ljava/lang/String;

    .line 1226895
    iput-boolean p4, v1, LX/7i1;->f:Z

    .line 1226896
    return-object v1
.end method

.method public final b(LX/3nA;)V
    .locals 1

    .prologue
    .line 1226897
    check-cast p1, LX/7i1;

    .line 1226898
    :try_start_0
    iget-object v0, p1, LX/7i1;->b:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 1226899
    iget-object v0, p1, LX/7i1;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 1226900
    :cond_0
    iget-object v0, p1, LX/7i1;->a:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_1

    .line 1226901
    iget-object v0, p1, LX/7i1;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1226902
    :cond_1
    :goto_0
    return-void

    .line 1226903
    :catch_0
    move-exception v0

    .line 1226904
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    goto :goto_0
.end method
