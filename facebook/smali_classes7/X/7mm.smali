.class public final enum LX/7mm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7mm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7mm;

.field public static final enum COVER_PHOTO:LX/7mm;

.field public static final enum POST:LX/7mm;

.field public static final enum PROFILE_PIC:LX/7mm;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1237458
    new-instance v0, LX/7mm;

    const-string v1, "POST"

    invoke-direct {v0, v1, v2}, LX/7mm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7mm;->POST:LX/7mm;

    .line 1237459
    new-instance v0, LX/7mm;

    const-string v1, "PROFILE_PIC"

    invoke-direct {v0, v1, v3}, LX/7mm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7mm;->PROFILE_PIC:LX/7mm;

    .line 1237460
    new-instance v0, LX/7mm;

    const-string v1, "COVER_PHOTO"

    invoke-direct {v0, v1, v4}, LX/7mm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7mm;->COVER_PHOTO:LX/7mm;

    .line 1237461
    const/4 v0, 0x3

    new-array v0, v0, [LX/7mm;

    sget-object v1, LX/7mm;->POST:LX/7mm;

    aput-object v1, v0, v2

    sget-object v1, LX/7mm;->PROFILE_PIC:LX/7mm;

    aput-object v1, v0, v3

    sget-object v1, LX/7mm;->COVER_PHOTO:LX/7mm;

    aput-object v1, v0, v4

    sput-object v0, LX/7mm;->$VALUES:[LX/7mm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1237457
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7mm;
    .locals 1

    .prologue
    .line 1237456
    const-class v0, LX/7mm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7mm;

    return-object v0
.end method

.method public static values()[LX/7mm;
    .locals 1

    .prologue
    .line 1237455
    sget-object v0, LX/7mm;->$VALUES:[LX/7mm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7mm;

    return-object v0
.end method
