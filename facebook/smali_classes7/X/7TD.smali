.class public LX/7TD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/io/File;

.field public final b:J

.field public final c:J

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:J

.field public final m:LX/7TE;


# direct methods
.method private constructor <init>(Ljava/io/File;JJIIIIJLX/7Sx;LX/7TE;)V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 1210188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1210189
    iput-object p1, p0, LX/7TD;->a:Ljava/io/File;

    .line 1210190
    iput-wide p2, p0, LX/7TD;->b:J

    .line 1210191
    iput-wide p4, p0, LX/7TD;->c:J

    .line 1210192
    iput p6, p0, LX/7TD;->d:I

    .line 1210193
    iput p7, p0, LX/7TD;->e:I

    .line 1210194
    iput p8, p0, LX/7TD;->f:I

    .line 1210195
    iput p9, p0, LX/7TD;->g:I

    .line 1210196
    iput-wide p10, p0, LX/7TD;->l:J

    .line 1210197
    if-nez p12, :cond_0

    .line 1210198
    iput v0, p0, LX/7TD;->h:I

    .line 1210199
    iput v0, p0, LX/7TD;->i:I

    .line 1210200
    iput v0, p0, LX/7TD;->j:I

    .line 1210201
    iput v0, p0, LX/7TD;->k:I

    .line 1210202
    :goto_0
    iput-object p13, p0, LX/7TD;->m:LX/7TE;

    .line 1210203
    return-void

    .line 1210204
    :cond_0
    iget v0, p12, LX/7Sx;->d:I

    iput v0, p0, LX/7TD;->h:I

    .line 1210205
    iget v0, p12, LX/7Sx;->e:I

    iput v0, p0, LX/7TD;->i:I

    .line 1210206
    iget v0, p12, LX/7Sx;->j:I

    iput v0, p0, LX/7TD;->j:I

    .line 1210207
    iget v0, p12, LX/7Sx;->n:I

    iput v0, p0, LX/7TD;->k:I

    goto :goto_0
.end method

.method public static a(Ljava/io/File;JJIIIIJLX/7Sx;LX/7TE;)LX/7TD;
    .locals 15

    .prologue
    .line 1210208
    new-instance v0, LX/7TD;

    move-object v1, p0

    move-wide/from16 v2, p1

    move-wide/from16 v4, p3

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-wide/from16 v10, p9

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, LX/7TD;-><init>(Ljava/io/File;JJIIIIJLX/7Sx;LX/7TE;)V

    return-object v0
.end method
