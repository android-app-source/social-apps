.class public final LX/7LE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16Y;
.implements LX/2pz;
.implements LX/2q0;
.implements LX/2q2;


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/FullScreenVideoPlayer;

.field public b:Z

.field private c:I


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V
    .locals 0

    .prologue
    .line 1196888
    iput-object p1, p0, LX/7LE;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/player/FullScreenVideoPlayer;B)V
    .locals 0

    .prologue
    .line 1196887
    invoke-direct {p0, p1}, LX/7LE;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1196876
    iget-boolean v0, p0, LX/7LE;->b:Z

    if-nez v0, :cond_0

    .line 1196877
    :goto_0
    return-void

    .line 1196878
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7LE;->b:Z

    .line 1196879
    iget-object v0, p0, LX/7LE;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    iget-object v1, p0, LX/7LE;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->au:LX/04g;

    iget v2, p0, LX/7LE;->c:I

    invoke-virtual {v0, v1, v2}, LX/7Kr;->a(LX/04g;I)V

    goto :goto_0
.end method

.method public final a(LX/2qK;)V
    .locals 4

    .prologue
    .line 1196883
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7LE;->b:Z

    .line 1196884
    iget v0, p1, LX/2qK;->b:I

    iput v0, p0, LX/7LE;->c:I

    .line 1196885
    iget-object v0, p0, LX/7LE;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    iget-object v1, p0, LX/7LE;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->au:LX/04g;

    iget v2, p1, LX/2qK;->b:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/7Kr;->a(LX/04g;ILcom/facebook/video/engine/VideoPlayerParams;)V

    .line 1196886
    return-void
.end method

.method public final a(LX/2qN;)V
    .locals 3

    .prologue
    .line 1196880
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7LE;->b:Z

    .line 1196881
    iget-object v0, p0, LX/7LE;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    iget-object v1, p0, LX/7LE;->a:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->au:LX/04g;

    iget v2, p1, LX/2qN;->b:I

    invoke-virtual {v0, v1, v2}, LX/7Kr;->b(LX/04g;I)V

    .line 1196882
    return-void
.end method
