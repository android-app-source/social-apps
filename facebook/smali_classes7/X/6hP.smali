.class public final enum LX/6hP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6hP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6hP;

.field public static final enum DATA_SAVER_MODE_ENABLED:LX/6hP;

.field public static final enum EMOJI_COLOR_PREF:LX/6hP;

.field public static final enum MONTAGE_NOTIFICATIONS_ENABLED:LX/6hP;

.field public static final enum OMNI_M_SUGGESTION_ENABLED_PREF:LX/6hP;

.field public static final enum OMNI_M_SUGGESTION_MODE_PREF:LX/6hP;


# instance fields
.field public final keyString:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1128163
    new-instance v0, LX/6hP;

    const-string v1, "DATA_SAVER_MODE_ENABLED"

    const-string v2, "data_saver_mode_enabled"

    invoke-direct {v0, v1, v3, v2}, LX/6hP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6hP;->DATA_SAVER_MODE_ENABLED:LX/6hP;

    .line 1128164
    new-instance v0, LX/6hP;

    const-string v1, "EMOJI_COLOR_PREF"

    const-string v2, "emoji_color_pref"

    invoke-direct {v0, v1, v4, v2}, LX/6hP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6hP;->EMOJI_COLOR_PREF:LX/6hP;

    .line 1128165
    new-instance v0, LX/6hP;

    const-string v1, "MONTAGE_NOTIFICATIONS_ENABLED"

    const-string v2, "montage_notifications_enabled"

    invoke-direct {v0, v1, v5, v2}, LX/6hP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6hP;->MONTAGE_NOTIFICATIONS_ENABLED:LX/6hP;

    .line 1128166
    new-instance v0, LX/6hP;

    const-string v1, "OMNI_M_SUGGESTION_ENABLED_PREF"

    const-string v2, "omni_m_suggestion_enabled"

    invoke-direct {v0, v1, v6, v2}, LX/6hP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6hP;->OMNI_M_SUGGESTION_ENABLED_PREF:LX/6hP;

    .line 1128167
    new-instance v0, LX/6hP;

    const-string v1, "OMNI_M_SUGGESTION_MODE_PREF"

    const-string v2, "omni_m_suggestion_mode_pref"

    invoke-direct {v0, v1, v7, v2}, LX/6hP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6hP;->OMNI_M_SUGGESTION_MODE_PREF:LX/6hP;

    .line 1128168
    const/4 v0, 0x5

    new-array v0, v0, [LX/6hP;

    sget-object v1, LX/6hP;->DATA_SAVER_MODE_ENABLED:LX/6hP;

    aput-object v1, v0, v3

    sget-object v1, LX/6hP;->EMOJI_COLOR_PREF:LX/6hP;

    aput-object v1, v0, v4

    sget-object v1, LX/6hP;->MONTAGE_NOTIFICATIONS_ENABLED:LX/6hP;

    aput-object v1, v0, v5

    sget-object v1, LX/6hP;->OMNI_M_SUGGESTION_ENABLED_PREF:LX/6hP;

    aput-object v1, v0, v6

    sget-object v1, LX/6hP;->OMNI_M_SUGGESTION_MODE_PREF:LX/6hP;

    aput-object v1, v0, v7

    sput-object v0, LX/6hP;->$VALUES:[LX/6hP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1128169
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1128170
    iput-object p3, p0, LX/6hP;->keyString:Ljava/lang/String;

    .line 1128171
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6hP;
    .locals 1

    .prologue
    .line 1128172
    const-class v0, LX/6hP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6hP;

    return-object v0
.end method

.method public static values()[LX/6hP;
    .locals 1

    .prologue
    .line 1128173
    sget-object v0, LX/6hP;->$VALUES:[LX/6hP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6hP;

    return-object v0
.end method
