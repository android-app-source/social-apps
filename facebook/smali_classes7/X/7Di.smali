.class public final enum LX/7Di;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Di;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Di;

.field public static final enum AVG_FRAME_RENDER_TIME:LX/7Di;

.field public static final enum AVG_TEXTURE_TO_SCREEN_RATIO:LX/7Di;

.field public static final enum COMPOSER_SESSION_ID:LX/7Di;

.field public static final enum DEVICE_PIXEL_RATIO:LX/7Di;

.field public static final enum DT:LX/7Di;

.field public static final enum MAX_GPU_MEMORY_USED:LX/7Di;

.field public static final enum MAX_TILE_LEVEL:LX/7Di;

.field public static final enum PHOTO_ID:LX/7Di;

.field public static final enum RENDER_METHOD:LX/7Di;

.field public static final enum SURFACE:LX/7Di;

.field public static final enum TOTAL_DATA_LOADED:LX/7Di;

.field public static final enum VIEWPORT_HEIGHT:LX/7Di;

.field public static final enum VIEWPORT_WIDTH:LX/7Di;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1183533
    new-instance v0, LX/7Di;

    const-string v1, "PHOTO_ID"

    const-string v2, "photo_id"

    invoke-direct {v0, v1, v4, v2}, LX/7Di;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Di;->PHOTO_ID:LX/7Di;

    .line 1183534
    new-instance v0, LX/7Di;

    const-string v1, "SURFACE"

    const-string v2, "surface"

    invoke-direct {v0, v1, v5, v2}, LX/7Di;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Di;->SURFACE:LX/7Di;

    .line 1183535
    new-instance v0, LX/7Di;

    const-string v1, "COMPOSER_SESSION_ID"

    const-string v2, "composer_session_id"

    invoke-direct {v0, v1, v6, v2}, LX/7Di;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Di;->COMPOSER_SESSION_ID:LX/7Di;

    .line 1183536
    new-instance v0, LX/7Di;

    const-string v1, "DT"

    const-string v2, "dt"

    invoke-direct {v0, v1, v7, v2}, LX/7Di;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Di;->DT:LX/7Di;

    .line 1183537
    new-instance v0, LX/7Di;

    const-string v1, "RENDER_METHOD"

    const-string v2, "render_method"

    invoke-direct {v0, v1, v8, v2}, LX/7Di;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Di;->RENDER_METHOD:LX/7Di;

    .line 1183538
    new-instance v0, LX/7Di;

    const-string v1, "AVG_FRAME_RENDER_TIME"

    const/4 v2, 0x5

    const-string v3, "avg_frame_render_time"

    invoke-direct {v0, v1, v2, v3}, LX/7Di;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Di;->AVG_FRAME_RENDER_TIME:LX/7Di;

    .line 1183539
    new-instance v0, LX/7Di;

    const-string v1, "AVG_TEXTURE_TO_SCREEN_RATIO"

    const/4 v2, 0x6

    const-string v3, "avg_texture_to_screen_ratio2"

    invoke-direct {v0, v1, v2, v3}, LX/7Di;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Di;->AVG_TEXTURE_TO_SCREEN_RATIO:LX/7Di;

    .line 1183540
    new-instance v0, LX/7Di;

    const-string v1, "DEVICE_PIXEL_RATIO"

    const/4 v2, 0x7

    const-string v3, "device_pixel_ratio"

    invoke-direct {v0, v1, v2, v3}, LX/7Di;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Di;->DEVICE_PIXEL_RATIO:LX/7Di;

    .line 1183541
    new-instance v0, LX/7Di;

    const-string v1, "MAX_GPU_MEMORY_USED"

    const/16 v2, 0x8

    const-string v3, "max_gpu_memory_used"

    invoke-direct {v0, v1, v2, v3}, LX/7Di;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Di;->MAX_GPU_MEMORY_USED:LX/7Di;

    .line 1183542
    new-instance v0, LX/7Di;

    const-string v1, "MAX_TILE_LEVEL"

    const/16 v2, 0x9

    const-string v3, "max_tile_level"

    invoke-direct {v0, v1, v2, v3}, LX/7Di;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Di;->MAX_TILE_LEVEL:LX/7Di;

    .line 1183543
    new-instance v0, LX/7Di;

    const-string v1, "TOTAL_DATA_LOADED"

    const/16 v2, 0xa

    const-string v3, "total_data_loaded"

    invoke-direct {v0, v1, v2, v3}, LX/7Di;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Di;->TOTAL_DATA_LOADED:LX/7Di;

    .line 1183544
    new-instance v0, LX/7Di;

    const-string v1, "VIEWPORT_HEIGHT"

    const/16 v2, 0xb

    const-string v3, "viewport_height"

    invoke-direct {v0, v1, v2, v3}, LX/7Di;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Di;->VIEWPORT_HEIGHT:LX/7Di;

    .line 1183545
    new-instance v0, LX/7Di;

    const-string v1, "VIEWPORT_WIDTH"

    const/16 v2, 0xc

    const-string v3, "viewport_width"

    invoke-direct {v0, v1, v2, v3}, LX/7Di;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Di;->VIEWPORT_WIDTH:LX/7Di;

    .line 1183546
    const/16 v0, 0xd

    new-array v0, v0, [LX/7Di;

    sget-object v1, LX/7Di;->PHOTO_ID:LX/7Di;

    aput-object v1, v0, v4

    sget-object v1, LX/7Di;->SURFACE:LX/7Di;

    aput-object v1, v0, v5

    sget-object v1, LX/7Di;->COMPOSER_SESSION_ID:LX/7Di;

    aput-object v1, v0, v6

    sget-object v1, LX/7Di;->DT:LX/7Di;

    aput-object v1, v0, v7

    sget-object v1, LX/7Di;->RENDER_METHOD:LX/7Di;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7Di;->AVG_FRAME_RENDER_TIME:LX/7Di;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7Di;->AVG_TEXTURE_TO_SCREEN_RATIO:LX/7Di;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7Di;->DEVICE_PIXEL_RATIO:LX/7Di;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7Di;->MAX_GPU_MEMORY_USED:LX/7Di;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/7Di;->MAX_TILE_LEVEL:LX/7Di;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/7Di;->TOTAL_DATA_LOADED:LX/7Di;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/7Di;->VIEWPORT_HEIGHT:LX/7Di;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/7Di;->VIEWPORT_WIDTH:LX/7Di;

    aput-object v2, v0, v1

    sput-object v0, LX/7Di;->$VALUES:[LX/7Di;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1183547
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1183548
    iput-object p3, p0, LX/7Di;->value:Ljava/lang/String;

    .line 1183549
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Di;
    .locals 1

    .prologue
    .line 1183550
    const-class v0, LX/7Di;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Di;

    return-object v0
.end method

.method public static values()[LX/7Di;
    .locals 1

    .prologue
    .line 1183551
    sget-object v0, LX/7Di;->$VALUES:[LX/7Di;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Di;

    return-object v0
.end method
