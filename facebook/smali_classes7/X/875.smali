.class public final enum LX/875;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/875;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/875;

.field public static final enum CAMERA_ROLL:LX/875;

.field public static final enum CAPTURE:LX/875;

.field public static final enum UNKNOWN:LX/875;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1298676
    new-instance v0, LX/875;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/875;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/875;->UNKNOWN:LX/875;

    .line 1298677
    new-instance v0, LX/875;

    const-string v1, "CAMERA_ROLL"

    invoke-direct {v0, v1, v3}, LX/875;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/875;->CAMERA_ROLL:LX/875;

    .line 1298678
    new-instance v0, LX/875;

    const-string v1, "CAPTURE"

    invoke-direct {v0, v1, v4}, LX/875;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/875;->CAPTURE:LX/875;

    .line 1298679
    const/4 v0, 0x3

    new-array v0, v0, [LX/875;

    sget-object v1, LX/875;->UNKNOWN:LX/875;

    aput-object v1, v0, v2

    sget-object v1, LX/875;->CAMERA_ROLL:LX/875;

    aput-object v1, v0, v3

    sget-object v1, LX/875;->CAPTURE:LX/875;

    aput-object v1, v0, v4

    sput-object v0, LX/875;->$VALUES:[LX/875;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1298680
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/875;
    .locals 1

    .prologue
    .line 1298675
    const-class v0, LX/875;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/875;

    return-object v0
.end method

.method public static values()[LX/875;
    .locals 1

    .prologue
    .line 1298674
    sget-object v0, LX/875;->$VALUES:[LX/875;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/875;

    return-object v0
.end method
