.class public final LX/8Zs;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1365579
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1365580
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1365581
    :goto_0
    return v1

    .line 1365582
    :cond_0
    const-string v8, "length"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1365583
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v5, v3

    move v3, v2

    .line 1365584
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 1365585
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1365586
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1365587
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 1365588
    const-string v8, "entity"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1365589
    invoke-static {p0, p1}, LX/8Zu;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1365590
    :cond_2
    const-string v8, "offset"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1365591
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 1365592
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1365593
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1365594
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1365595
    if-eqz v3, :cond_5

    .line 1365596
    invoke-virtual {p1, v2, v5, v1}, LX/186;->a(III)V

    .line 1365597
    :cond_5
    if-eqz v0, :cond_6

    .line 1365598
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 1365599
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1365564
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1365565
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1365566
    if-eqz v0, :cond_0

    .line 1365567
    const-string v1, "entity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365568
    invoke-static {p0, v0, p2, p3}, LX/8Zu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1365569
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1365570
    if-eqz v0, :cond_1

    .line 1365571
    const-string v1, "length"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365572
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1365573
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1365574
    if-eqz v0, :cond_2

    .line 1365575
    const-string v1, "offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1365576
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1365577
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1365578
    return-void
.end method
