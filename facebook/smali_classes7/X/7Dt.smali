.class public LX/7Dt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1184002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184003
    return-void
.end method

.method public static a(Ljava/util/List;I)Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7Dw;",
            ">;I)",
            "Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;"
        }
    .end annotation

    .prologue
    .line 1184004
    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 1184005
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 1184006
    const/16 v1, 0x7dd

    move/from16 v0, p1

    if-gt v0, v1, :cond_0

    .line 1184007
    const/16 v2, 0x300

    .line 1184008
    const/16 v1, 0x300

    move v3, v2

    move v2, v1

    .line 1184009
    :goto_0
    const/4 v6, 0x0

    .line 1184010
    const/4 v5, 0x0

    .line 1184011
    const/4 v4, 0x0

    .line 1184012
    const/4 v1, 0x1

    .line 1184013
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    move-object v13, v11

    move-object v11, v10

    move-object v10, v9

    move-object v9, v8

    move-object v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v1

    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Dw;

    .line 1184014
    sget-object v12, LX/19o;->CUBESTRIP:LX/19o;

    invoke-interface {v1}, LX/7Dw;->b()LX/19o;

    move-result-object v14

    if-ne v12, v14, :cond_c

    .line 1184015
    invoke-interface {v1}, LX/7Dw;->c()Ljava/lang/String;

    move-result-object v12

    .line 1184016
    invoke-interface {v1}, LX/7Dw;->d()I

    move-result v14

    .line 1184017
    if-eqz v4, :cond_1

    .line 1184018
    sub-int v1, v14, v3

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v7

    .line 1184019
    sub-int v1, v14, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 1184020
    const/4 v4, 0x0

    move v1, v4

    move v6, v7

    move-object v8, v12

    move-object v9, v12

    move v4, v14

    move-object v7, v12

    .line 1184021
    :goto_2
    const/16 v10, 0x7dd

    move/from16 v0, p1

    if-gt v0, v10, :cond_3

    .line 1184022
    const/16 v10, 0x300

    if-ne v14, v10, :cond_9

    move-object v10, v12

    .line 1184023
    :goto_3
    const/16 v13, 0x300

    if-ne v14, v13, :cond_7

    move-object v11, v12

    move-object v13, v10

    move-object v10, v9

    move-object v9, v8

    move-object v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v1

    .line 1184024
    goto :goto_1

    .line 1184025
    :cond_0
    const/16 v2, 0x400

    .line 1184026
    const/16 v1, 0x600

    move v3, v2

    move v2, v1

    goto :goto_0

    .line 1184027
    :cond_1
    sub-int v1, v14, v3

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, v7, :cond_2

    .line 1184028
    sub-int v1, v14, v3

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v7

    move-object v9, v12

    .line 1184029
    :cond_2
    sub-int v1, v14, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, v6, :cond_b

    .line 1184030
    sub-int v1, v14, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    move-object v6, v12

    .line 1184031
    :goto_4
    if-le v14, v5, :cond_a

    move v5, v1

    move-object v8, v9

    move v1, v4

    move-object v9, v12

    move v4, v14

    move-object/from16 v16, v6

    move v6, v7

    move-object/from16 v7, v16

    .line 1184032
    goto :goto_2

    .line 1184033
    :cond_3
    const/16 v10, 0x400

    if-ne v14, v10, :cond_8

    move-object v10, v12

    .line 1184034
    :goto_5
    const/16 v13, 0x600

    if-ne v14, v13, :cond_7

    move-object v11, v10

    move-object v10, v12

    :goto_6
    move-object v13, v11

    move-object v11, v10

    move-object v10, v9

    move-object v9, v8

    move-object v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v1

    .line 1184035
    goto/16 :goto_1

    .line 1184036
    :cond_4
    if-nez v13, :cond_6

    .line 1184037
    :goto_7
    if-nez v11, :cond_5

    .line 1184038
    :goto_8
    new-instance v1, Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;

    invoke-direct {v1, v9, v8, v10}, Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_5
    move-object v8, v11

    goto :goto_8

    :cond_6
    move-object v9, v13

    goto :goto_7

    :cond_7
    move-object/from16 v16, v11

    move-object v11, v10

    move-object/from16 v10, v16

    goto :goto_6

    :cond_8
    move-object v10, v13

    goto :goto_5

    :cond_9
    move-object v10, v13

    goto :goto_3

    :cond_a
    move-object v8, v9

    move-object v9, v10

    move/from16 v16, v1

    move v1, v4

    move v4, v5

    move/from16 v5, v16

    move-object/from16 v17, v6

    move v6, v7

    move-object/from16 v7, v17

    goto/16 :goto_2

    :cond_b
    move v1, v6

    move-object v6, v8

    goto :goto_4

    :cond_c
    move v1, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v13

    goto :goto_6
.end method
