.class public final enum LX/8A0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8A0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8A0;

.field public static final enum CLOSED:LX/8A0;

.field public static final enum MESSAGES:LX/8A0;

.field public static final enum NEW_LIKES:LX/8A0;

.field public static final enum NOTIFICATIONS:LX/8A0;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1305559
    new-instance v0, LX/8A0;

    const-string v1, "NEW_LIKES"

    invoke-direct {v0, v1, v2}, LX/8A0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A0;->NEW_LIKES:LX/8A0;

    .line 1305560
    new-instance v0, LX/8A0;

    const-string v1, "MESSAGES"

    invoke-direct {v0, v1, v3}, LX/8A0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A0;->MESSAGES:LX/8A0;

    .line 1305561
    new-instance v0, LX/8A0;

    const-string v1, "NOTIFICATIONS"

    invoke-direct {v0, v1, v4}, LX/8A0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A0;->NOTIFICATIONS:LX/8A0;

    .line 1305562
    new-instance v0, LX/8A0;

    const-string v1, "CLOSED"

    invoke-direct {v0, v1, v5}, LX/8A0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8A0;->CLOSED:LX/8A0;

    .line 1305563
    const/4 v0, 0x4

    new-array v0, v0, [LX/8A0;

    sget-object v1, LX/8A0;->NEW_LIKES:LX/8A0;

    aput-object v1, v0, v2

    sget-object v1, LX/8A0;->MESSAGES:LX/8A0;

    aput-object v1, v0, v3

    sget-object v1, LX/8A0;->NOTIFICATIONS:LX/8A0;

    aput-object v1, v0, v4

    sget-object v1, LX/8A0;->CLOSED:LX/8A0;

    aput-object v1, v0, v5

    sput-object v0, LX/8A0;->$VALUES:[LX/8A0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1305564
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8A0;
    .locals 1

    .prologue
    .line 1305558
    const-class v0, LX/8A0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8A0;

    return-object v0
.end method

.method public static values()[LX/8A0;
    .locals 1

    .prologue
    .line 1305557
    sget-object v0, LX/8A0;->$VALUES:[LX/8A0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8A0;

    return-object v0
.end method
