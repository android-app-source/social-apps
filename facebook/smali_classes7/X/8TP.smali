.class public final LX/8TP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/8Ta;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/8Ta;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1348565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1348566
    iput-object p1, p0, LX/8TP;->a:LX/8Ta;

    .line 1348567
    iput-object p2, p0, LX/8TP;->b:Ljava/lang/String;

    .line 1348568
    return-void
.end method


# virtual methods
.method public final d()Z
    .locals 2

    .prologue
    .line 1348571
    iget-object v0, p0, LX/8TP;->a:LX/8Ta;

    sget-object v1, LX/8Ta;->Thread:LX/8Ta;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1348572
    instance-of v0, p1, LX/8TP;

    if-nez v0, :cond_0

    move v0, v1

    .line 1348573
    :goto_0
    return v0

    .line 1348574
    :cond_0
    if-ne p1, p0, :cond_1

    move v0, v2

    .line 1348575
    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 1348576
    check-cast v0, LX/8TP;

    iget-object v0, v0, LX/8TP;->b:Ljava/lang/String;

    iget-object v3, p0, LX/8TP;->b:Ljava/lang/String;

    invoke-static {v0, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    check-cast p1, LX/8TP;

    iget-object v0, p1, LX/8TP;->a:LX/8Ta;

    iget-object v3, p0, LX/8TP;->a:LX/8Ta;

    invoke-static {v0, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1348570
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/8TP;->a:LX/8Ta;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/8TP;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1348569
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "context: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/8TP;->a:LX/8Ta;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\ncontext_id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/8TP;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
