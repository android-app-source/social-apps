.class public LX/6qC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/6qC;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1150154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150155
    return-void
.end method

.method public static a(LX/0QB;)LX/6qC;
    .locals 3

    .prologue
    .line 1150156
    sget-object v0, LX/6qC;->a:LX/6qC;

    if-nez v0, :cond_1

    .line 1150157
    const-class v1, LX/6qC;

    monitor-enter v1

    .line 1150158
    :try_start_0
    sget-object v0, LX/6qC;->a:LX/6qC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1150159
    if-eqz v2, :cond_0

    .line 1150160
    :try_start_1
    new-instance v0, LX/6qC;

    invoke-direct {v0}, LX/6qC;-><init>()V

    .line 1150161
    move-object v0, v0

    .line 1150162
    sput-object v0, LX/6qC;->a:LX/6qC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1150163
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1150164
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1150165
    :cond_1
    sget-object v0, LX/6qC;->a:LX/6qC;

    return-object v0

    .line 1150166
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1150167
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1150168
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1150169
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "q"

    const-string v3, "viewer(){peer_to_peer_payment_pin{id}}"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1150170
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "fetchPaymentPin"

    .line 1150171
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1150172
    move-object v1, v1

    .line 1150173
    const-string v2, "GET"

    .line 1150174
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1150175
    move-object v1, v1

    .line 1150176
    const-string v2, "graphql"

    .line 1150177
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1150178
    move-object v1, v1

    .line 1150179
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1150180
    move-object v0, v1

    .line 1150181
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1150182
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1150183
    move-object v0, v0

    .line 1150184
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1150185
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1150186
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "viewer"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v1, "peer_to_peer_payment_pin"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 1150187
    const-string v0, "id"

    invoke-virtual {v1, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    const-string v2, "id"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->D()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/facebook/payments/auth/pin/model/PaymentPin;-><init>(J)V

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    goto :goto_0
.end method
