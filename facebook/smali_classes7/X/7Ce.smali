.class public abstract LX/7Ce;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/WindowManager;

.field public final h:Ljava/util/concurrent/locks/Lock;

.field public final i:[F

.field public j:LX/7Cp;

.field public final k:LX/7Cp;

.field public final l:LX/7Cp;

.field public m:F

.field public n:F

.field public o:F

.field public p:F

.field public q:F

.field public r:Z

.field public s:I


# direct methods
.method public constructor <init>(Landroid/view/WindowManager;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1181244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1181245
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    .line 1181246
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, LX/7Ce;->i:[F

    .line 1181247
    invoke-static {v1}, LX/7Cq;->a(I)LX/7Cp;

    move-result-object v0

    iput-object v0, p0, LX/7Ce;->j:LX/7Cp;

    .line 1181248
    new-instance v0, LX/7Cp;

    invoke-direct {v0}, LX/7Cp;-><init>()V

    iput-object v0, p0, LX/7Ce;->k:LX/7Cp;

    .line 1181249
    new-instance v0, LX/7Cp;

    invoke-direct {v0}, LX/7Cp;-><init>()V

    iput-object v0, p0, LX/7Ce;->l:LX/7Cp;

    .line 1181250
    iput-boolean v1, p0, LX/7Ce;->r:Z

    .line 1181251
    const/4 v0, -0x1

    iput v0, p0, LX/7Ce;->s:I

    .line 1181252
    iput-object p1, p0, LX/7Ce;->a:Landroid/view/WindowManager;

    .line 1181253
    return-void
.end method


# virtual methods
.method public abstract a(F)V
.end method

.method public a(FF)V
    .locals 0

    .prologue
    .line 1181233
    iput p1, p0, LX/7Ce;->m:F

    .line 1181234
    iput p2, p0, LX/7Ce;->n:F

    .line 1181235
    return-void
.end method

.method public abstract a(Landroid/hardware/SensorEvent;)V
.end method

.method public abstract a([FLX/3IO;)V
.end method

.method public abstract b(FF)V
.end method

.method public abstract c(FF)V
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1181239
    iget v0, p0, LX/7Ce;->s:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1181240
    iget v0, p0, LX/7Ce;->s:I

    .line 1181241
    :goto_0
    move v0, v0

    .line 1181242
    invoke-static {v0}, LX/7Cq;->a(I)LX/7Cp;

    move-result-object v0

    iput-object v0, p0, LX/7Ce;->j:LX/7Cp;

    .line 1181243
    return-void

    :cond_0
    iget-object v0, p0, LX/7Ce;->a:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    goto :goto_0
.end method

.method public abstract d(FF)V
.end method

.method public e(FF)V
    .locals 0

    .prologue
    .line 1181236
    iput p1, p0, LX/7Ce;->o:F

    .line 1181237
    iput p2, p0, LX/7Ce;->p:F

    .line 1181238
    return-void
.end method
