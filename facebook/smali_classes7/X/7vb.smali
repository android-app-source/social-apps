.class public LX/7vb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;
.implements LX/0dc;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field private static final m:LX/0Tn;

.field private static final n:LX/0Tn;

.field private static final o:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1274835
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "events/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1274836
    sput-object v0, LX/7vb;->m:LX/0Tn;

    const-string v1, "privacy_critical/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7vb;->n:LX/0Tn;

    .line 1274837
    sget-object v0, LX/7vb;->m:LX/0Tn;

    const-string v1, "non_critical/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7vb;->o:LX/0Tn;

    .line 1274838
    sget-object v0, LX/7vb;->n:LX/0Tn;

    const-string v1, "guest_list_invited_seen_state_megaphone"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7vb;->a:LX/0Tn;

    .line 1274839
    sget-object v0, LX/7vb;->n:LX/0Tn;

    const-string v1, "locked_privacy_education"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7vb;->b:LX/0Tn;

    .line 1274840
    sget-object v0, LX/7vb;->o:LX/0Tn;

    const-string v1, "events_cover_photo_selector_theme_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7vb;->c:LX/0Tn;

    .line 1274841
    sget-object v0, LX/7vb;->o:LX/0Tn;

    const-string v1, "events_invite_through_messenger_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7vb;->d:LX/0Tn;

    .line 1274842
    sget-object v0, LX/7vb;->o:LX/0Tn;

    const-string v1, "events_extended_invite_add_contacts_button_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7vb;->e:LX/0Tn;

    .line 1274843
    sget-object v0, LX/7vb;->o:LX/0Tn;

    const-string v1, "events_extended_invite_add_note_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7vb;->f:LX/0Tn;

    .line 1274844
    sget-object v0, LX/7vb;->o:LX/0Tn;

    const-string v1, "events_extended_invite_note_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7vb;->g:LX/0Tn;

    .line 1274845
    sget-object v0, LX/7vb;->o:LX/0Tn;

    const-string v1, "events_extended_invite_lannister_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7vb;->h:LX/0Tn;

    .line 1274846
    sget-object v0, LX/7vb;->o:LX/0Tn;

    const-string v1, "events_extended_invite_entrypoint_fb_nonsync_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7vb;->i:LX/0Tn;

    .line 1274847
    sget-object v0, LX/7vb;->o:LX/0Tn;

    const-string v1, "events_extended_invite_entrypoint_fb_sync_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7vb;->j:LX/0Tn;

    .line 1274848
    sget-object v0, LX/7vb;->o:LX/0Tn;

    const-string v1, "events_extended_invite_entrypoint_off_fb_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7vb;->k:LX/0Tn;

    .line 1274849
    sget-object v0, LX/7vb;->o:LX/0Tn;

    const-string v1, "events_ticketing_seat_selection_note_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/7vb;->l:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1274833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274834
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1274832
    sget-object v0, LX/7vb;->o:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1274831
    sget-object v0, LX/7vb;->n:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
