.class public LX/6x7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/util/SparseBooleanArray;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1158462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158463
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, LX/6x7;->b:Landroid/util/SparseBooleanArray;

    .line 1158464
    iput-object p1, p0, LX/6x7;->a:Landroid/content/Context;

    .line 1158465
    return-void
.end method

.method public static b(LX/0QB;)LX/6x7;
    .locals 2

    .prologue
    .line 1158466
    new-instance v1, LX/6x7;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/6x7;-><init>(Landroid/content/Context;)V

    .line 1158467
    return-object v1
.end method


# virtual methods
.method public final a()LX/73Y;
    .locals 2

    .prologue
    .line 1158468
    new-instance v0, LX/73Y;

    iget-object v1, p0, LX/6x7;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/73Y;-><init>(Landroid/content/Context;)V

    .line 1158469
    const v1, 0x7f081e42

    invoke-virtual {v0, v1}, LX/73Y;->setSecurityInfo(I)V

    .line 1158470
    invoke-virtual {p0}, LX/6x7;->b()I

    move-result v1

    invoke-virtual {v0, v1}, LX/73Y;->setLeftAndRightPaddingForChildViews(I)V

    .line 1158471
    return-object v0
.end method

.method public final a(ILX/6xe;Ljava/lang/String;)V
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 1158472
    sget-object v1, LX/6xe;->REQUIRED:LX/6xe;

    if-ne p2, v1, :cond_1

    .line 1158473
    iget-object v1, p0, LX/6x7;->b:Landroid/util/SparseBooleanArray;

    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1158474
    :goto_1
    return-void

    .line 1158475
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1158476
    :cond_1
    iget-object v1, p0, LX/6x7;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_1
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1158477
    iget-object v0, p0, LX/6x7;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0545

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1158478
    iget-object v0, p0, LX/6x7;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1158479
    move v0, v1

    :goto_0
    iget-object v2, p0, LX/6x7;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1158480
    iget-object v2, p0, LX/6x7;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1158481
    :goto_1
    return v1

    .line 1158482
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1158483
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method
