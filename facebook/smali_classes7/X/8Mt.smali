.class public LX/8Mt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/8Mu;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1335335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1335336
    iput-object p1, p0, LX/8Mt;->a:LX/0SG;

    .line 1335337
    return-void
.end method

.method public static a(LX/0QB;)LX/8Mt;
    .locals 2

    .prologue
    .line 1335338
    new-instance v1, LX/8Mt;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-direct {v1, v0}, LX/8Mt;-><init>(LX/0SG;)V

    .line 1335339
    move-object v0, v1

    .line 1335340
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 10

    .prologue
    .line 1335341
    check-cast p1, LX/8Mu;

    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    .line 1335342
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1335343
    iget-object v1, p1, LX/8Mu;->u:LX/5Rn;

    sget-object v2, LX/5Rn;->NORMAL:LX/5Rn;

    if-ne v1, v2, :cond_12

    .line 1335344
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "published"

    const-string v3, "1"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335345
    :cond_0
    :goto_0
    iget-object v1, p1, LX/8Mu;->b:Ljava/lang/String;

    if-eqz v1, :cond_13

    .line 1335346
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "target_post"

    iget-object v3, p1, LX/8Mu;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335347
    iget-object v1, p1, LX/8Mu;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1335348
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "profile_id"

    iget-object v3, p1, LX/8Mu;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335349
    :cond_1
    :goto_1
    iget-object v1, p1, LX/8Mu;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1335350
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "qn"

    iget-object v3, p1, LX/8Mu;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335351
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "composer_session_id"

    iget-object v3, p1, LX/8Mu;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335352
    :cond_2
    iget-object v1, p1, LX/8Mu;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1335353
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "place"

    iget-object v3, p1, LX/8Mu;->f:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335354
    :cond_3
    iget-object v1, p1, LX/8Mu;->g:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1335355
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "text_only_place"

    iget-object v3, p1, LX/8Mu;->g:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335356
    :cond_4
    iget-object v1, p1, LX/8Mu;->h:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1335357
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tags"

    iget-object v3, p1, LX/8Mu;->h:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335358
    :cond_5
    iget-object v1, p1, LX/8Mu;->i:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 1335359
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "name"

    iget-object v3, p1, LX/8Mu;->i:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335360
    :cond_6
    iget-object v1, p1, LX/8Mu;->e:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1335361
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "idempotence_token"

    iget-object v3, p1, LX/8Mu;->e:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335362
    :cond_7
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_explicit_location"

    iget-boolean v3, p1, LX/8Mu;->r:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335363
    iget-object v1, p1, LX/8Mu;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    if-eqz v1, :cond_8

    .line 1335364
    iget-object v1, p1, LX/8Mu;->j:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1335365
    :cond_8
    iget-object v1, p1, LX/8Mu;->k:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1335366
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "referenced_sticker_id"

    iget-object v3, p1, LX/8Mu;->k:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335367
    :cond_9
    iget-wide v2, p1, LX/8Mu;->s:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_a

    .line 1335368
    iget-object v1, p0, LX/8Mt;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1335369
    iget-wide v4, p1, LX/8Mu;->s:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 1335370
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "time_since_original_post"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335371
    :cond_a
    iget-object v1, p1, LX/8Mu;->l:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1335372
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "stickers"

    iget-object v3, p1, LX/8Mu;->l:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335373
    :cond_b
    iget-object v1, p1, LX/8Mu;->m:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 1335374
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "text_overlay"

    iget-object v3, p1, LX/8Mu;->m:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335375
    :cond_c
    iget-boolean v1, p1, LX/8Mu;->n:Z

    if-eqz v1, :cond_d

    .line 1335376
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_cropped"

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335377
    :cond_d
    iget-boolean v1, p1, LX/8Mu;->o:Z

    if-eqz v1, :cond_e

    .line 1335378
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_rotated"

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335379
    :cond_e
    iget-boolean v1, p1, LX/8Mu;->p:Z

    if-eqz v1, :cond_f

    .line 1335380
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_filtered"

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335381
    :cond_f
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "batch_size"

    iget v3, p1, LX/8Mu;->t:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335382
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "audience_exp"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335383
    iget-boolean v1, p1, LX/8Mu;->w:Z

    if-eqz v1, :cond_10

    .line 1335384
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "post_surfaces_blacklist"

    sget-object v3, LX/7mB;->a:LX/0Px;

    invoke-static {v3}, LX/16N;->b(Ljava/util/List;)LX/162;

    move-result-object v3

    invoke-virtual {v3}, LX/162;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335385
    :cond_10
    iget-boolean v1, p1, LX/8Mu;->x:Z

    if-eqz v1, :cond_11

    .line 1335386
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_member_bio_post"

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335387
    :cond_11
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "attachPhoto"

    .line 1335388
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1335389
    move-object v1, v1

    .line 1335390
    const-string v2, "POST"

    .line 1335391
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1335392
    move-object v1, v1

    .line 1335393
    iget-object v2, p1, LX/8Mu;->a:Ljava/lang/String;

    .line 1335394
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1335395
    move-object v1, v1

    .line 1335396
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1335397
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1335398
    move-object v0, v1

    .line 1335399
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 1335400
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1335401
    move-object v0, v0

    .line 1335402
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1335403
    :cond_12
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "published"

    const-string v3, "0"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335404
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "unpublished_content_type"

    iget-object v3, p1, LX/8Mu;->u:LX/5Rn;

    invoke-virtual {v3}, LX/5Rn;->getContentType()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1335405
    iget-wide v2, p1, LX/8Mu;->v:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_0

    .line 1335406
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "scheduled_publish_time"

    iget-wide v4, p1, LX/8Mu;->v:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 1335407
    :cond_13
    iget-object v1, p1, LX/8Mu;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1335408
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "target"

    iget-object v3, p1, LX/8Mu;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1335409
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1335410
    invoke-virtual {p2}, LX/1pN;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
