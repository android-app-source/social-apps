.class public LX/8WI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/support/v7/widget/RecyclerView;

.field public final c:LX/8VY;

.field private final d:LX/8Th;

.field public final e:LX/8TS;

.field public f:LX/8WH;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;LX/8VY;Landroid/content/Context;LX/8Th;LX/8TS;)V
    .locals 2
    .param p1    # Landroid/support/v7/widget/RecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/8VY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1353912
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1353913
    iput-object p1, p0, LX/8WI;->b:Landroid/support/v7/widget/RecyclerView;

    .line 1353914
    iput-object p3, p0, LX/8WI;->a:Landroid/content/Context;

    .line 1353915
    iput-object p4, p0, LX/8WI;->d:LX/8Th;

    .line 1353916
    instance-of v0, p2, LX/1OM;

    if-eqz v0, :cond_0

    .line 1353917
    iput-object p2, p0, LX/8WI;->c:LX/8VY;

    .line 1353918
    :goto_0
    iput-object p5, p0, LX/8WI;->e:LX/8TS;

    .line 1353919
    return-void

    .line 1353920
    :cond_0
    const-string v0, "CurrentMatchViewController"

    const-string v1, "Provided Quicksilver View Adapter is not an instance of recycler view"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1353921
    const/4 v0, 0x0

    iput-object v0, p0, LX/8WI;->c:LX/8VY;

    goto :goto_0
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 1353922
    iget-object v0, p0, LX/8WI;->c:LX/8VY;

    invoke-interface {v0}, LX/8VX;->b()Ljava/lang/String;

    move-result-object v0

    .line 1353923
    iget-object v1, p0, LX/8WI;->e:LX/8TS;

    .line 1353924
    iget-object p0, v1, LX/8TS;->j:LX/8TP;

    move-object v1, p0

    .line 1353925
    iget-object p0, v1, LX/8TP;->b:Ljava/lang/String;

    move-object v1, p0

    .line 1353926
    if-nez v0, :cond_1

    .line 1353927
    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 1353928
    :goto_0
    return v0

    .line 1353929
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1353930
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1353931
    new-instance v0, LX/62V;

    iget-object v1, p0, LX/8WI;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/62V;-><init>(Landroid/content/Context;)V

    .line 1353932
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 1353933
    iget-object v1, p0, LX/8WI;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1353934
    iget-object v1, p0, LX/8WI;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, p0, LX/8WI;->c:LX/8VY;

    check-cast v0, LX/1OM;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1353935
    return-void
.end method

.method public final b()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1353936
    iget-object v0, p0, LX/8WI;->e:LX/8TS;

    .line 1353937
    iget-object v2, v0, LX/8TS;->j:LX/8TP;

    move-object v0, v2

    .line 1353938
    invoke-virtual {v0}, LX/8TP;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8WI;->e:LX/8TS;

    .line 1353939
    iget-object v2, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v2

    .line 1353940
    if-eqz v0, :cond_1

    .line 1353941
    invoke-direct {p0}, LX/8WI;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1353942
    iget-object v0, p0, LX/8WI;->c:LX/8VY;

    iget-object v2, p0, LX/8WI;->c:LX/8VY;

    invoke-interface {v2}, LX/8VX;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v5, 0x0

    move-object v4, v1

    invoke-interface/range {v0 .. v5}, LX/8VY;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Z)V

    .line 1353943
    iget-object v0, p0, LX/8WI;->e:LX/8TS;

    .line 1353944
    iput-object v1, v0, LX/8TS;->h:LX/8TW;

    .line 1353945
    iget-object v0, p0, LX/8WI;->e:LX/8TS;

    .line 1353946
    iget-object v1, v0, LX/8TS;->f:LX/8Tb;

    move-object v0, v1

    .line 1353947
    iget-object v1, v0, LX/8Tb;->c:LX/8Ta;

    move-object v0, v1

    .line 1353948
    sget-object v1, LX/8Ta;->Thread:LX/8Ta;

    if-eq v0, v1, :cond_0

    .line 1353949
    iget-object v0, p0, LX/8WI;->e:LX/8TS;

    .line 1353950
    iget-object v1, v0, LX/8TS;->f:LX/8Tb;

    move-object v0, v1

    .line 1353951
    new-instance v1, LX/8Tb;

    iget-object v2, p0, LX/8WI;->e:LX/8TS;

    .line 1353952
    iget-object v3, v2, LX/8TS;->j:LX/8TP;

    move-object v2, v3

    .line 1353953
    iget-object v3, v2, LX/8TP;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1353954
    iget-object v3, v0, LX/8Tb;->b:LX/8TZ;

    move-object v0, v3

    .line 1353955
    iget-object v3, p0, LX/8WI;->e:LX/8TS;

    .line 1353956
    iget-object v4, v3, LX/8TS;->j:LX/8TP;

    move-object v3, v4

    .line 1353957
    iget-object v4, v3, LX/8TP;->a:LX/8Ta;

    move-object v3, v4

    .line 1353958
    invoke-direct {v1, v2, v0, v3}, LX/8Tb;-><init>(Ljava/lang/String;LX/8TZ;LX/8Ta;)V

    .line 1353959
    iget-object v0, p0, LX/8WI;->e:LX/8TS;

    invoke-virtual {v0, v1}, LX/8TS;->a(LX/8Tb;)LX/8TS;

    .line 1353960
    :cond_0
    iget-object v0, p0, LX/8WI;->d:LX/8Th;

    new-instance v1, LX/8WG;

    invoke-direct {v1, p0}, LX/8WG;-><init>(LX/8WI;)V

    const/4 p0, 0x1

    const/4 v8, 0x0

    .line 1353961
    iget-object v2, v0, LX/8Th;->f:LX/8TS;

    .line 1353962
    iget-object v3, v2, LX/8TS;->e:LX/8TO;

    move-object v2, v3

    .line 1353963
    iget-object v3, v2, LX/8TO;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1353964
    iget-object v3, v0, LX/8Th;->f:LX/8TS;

    .line 1353965
    iget-object v4, v3, LX/8TS;->j:LX/8TP;

    move-object v3, v4

    .line 1353966
    iget-object v4, v0, LX/8Th;->f:LX/8TS;

    invoke-virtual {v4}, LX/8TS;->c()LX/8Vb;

    .line 1353967
    new-instance v4, LX/4GV;

    invoke-direct {v4}, LX/4GV;-><init>()V

    .line 1353968
    sget-object v5, LX/8Tg;->a:[I

    .line 1353969
    iget-object v6, v3, LX/8TP;->a:LX/8Ta;

    move-object v6, v6

    .line 1353970
    invoke-virtual {v6}, LX/8Ta;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 1353971
    const-string v5, "CurrentMatchDataProvider"

    const-string v6, "Unsupported context %s for CurrentMatchDataProvider load"

    new-array v7, p0, [Ljava/lang/Object;

    invoke-virtual {v3}, LX/8TP;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v7, v8

    invoke-static {v5, v6, v7}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1353972
    :goto_0
    const-string v3, "game_id"

    invoke-virtual {v4, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1353973
    new-instance v2, LX/8UA;

    invoke-direct {v2}, LX/8UA;-><init>()V

    move-object v2, v2

    .line 1353974
    const-string v3, "key"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1353975
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 1353976
    iget-object v3, v0, LX/8Th;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1353977
    iget-object v3, v0, LX/8Th;->e:LX/8V7;

    invoke-virtual {v3}, LX/8V7;->a()LX/0zO;

    move-result-object v3

    .line 1353978
    iget-object v4, v0, LX/8Th;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 1353979
    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v2, v4, v8

    aput-object v3, v4, p0

    invoke-static {v4}, LX/0Vg;->b([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1353980
    new-instance v3, LX/8Tf;

    invoke-direct {v3, v0, v1}, LX/8Tf;-><init>(LX/8Th;LX/8WG;)V

    .line 1353981
    iget-object v4, v0, LX/8Th;->b:LX/1Ck;

    const-string v5, "current_match_data_query"

    invoke-virtual {v4, v5, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1353982
    :cond_1
    return-void

    .line 1353983
    :pswitch_0
    iget-object v5, v3, LX/8TP;->b:Ljava/lang/String;

    move-object v3, v5

    .line 1353984
    const-string v5, "thread_id"

    invoke-virtual {v4, v5, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1353985
    goto :goto_0

    .line 1353986
    :pswitch_1
    iget-object v5, v3, LX/8TP;->b:Ljava/lang/String;

    move-object v3, v5

    .line 1353987
    const-string v5, "group_id"

    invoke-virtual {v4, v5, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1353988
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
