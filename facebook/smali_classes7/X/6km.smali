.class public LX/6km;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;


# instance fields
.field public final accuracyMillimeters:Ljava/lang/Long;

.field public final altitudeAccuracyMillimeters:Ljava/lang/Long;

.field public final altitudeMillimeters:Ljava/lang/Long;

.field public final bearingDegrees:Ljava/lang/Long;

.field public final latitude:Ljava/lang/Long;

.field public final longitude:Ljava/lang/Long;

.field public final speedMillimetersPerSecond:Ljava/lang/Long;

.field public final timestampMilliseconds:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/16 v3, 0xa

    .line 1141147
    new-instance v0, LX/1sv;

    const-string v1, "MessageLiveLocationCoordinate"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6km;->b:LX/1sv;

    .line 1141148
    new-instance v0, LX/1sw;

    const-string v1, "latitude"

    invoke-direct {v0, v1, v3, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6km;->c:LX/1sw;

    .line 1141149
    new-instance v0, LX/1sw;

    const-string v1, "longitude"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6km;->d:LX/1sw;

    .line 1141150
    new-instance v0, LX/1sw;

    const-string v1, "timestampMilliseconds"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6km;->e:LX/1sw;

    .line 1141151
    new-instance v0, LX/1sw;

    const-string v1, "accuracyMillimeters"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6km;->f:LX/1sw;

    .line 1141152
    new-instance v0, LX/1sw;

    const-string v1, "speedMillimetersPerSecond"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6km;->g:LX/1sw;

    .line 1141153
    new-instance v0, LX/1sw;

    const-string v1, "altitudeMillimeters"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6km;->h:LX/1sw;

    .line 1141154
    new-instance v0, LX/1sw;

    const-string v1, "altitudeAccuracyMillimeters"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6km;->i:LX/1sw;

    .line 1141155
    new-instance v0, LX/1sw;

    const-string v1, "bearingDegrees"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6km;->j:LX/1sw;

    .line 1141156
    sput-boolean v4, LX/6km;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1141137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1141138
    iput-object p1, p0, LX/6km;->latitude:Ljava/lang/Long;

    .line 1141139
    iput-object p2, p0, LX/6km;->longitude:Ljava/lang/Long;

    .line 1141140
    iput-object p3, p0, LX/6km;->timestampMilliseconds:Ljava/lang/Long;

    .line 1141141
    iput-object p4, p0, LX/6km;->accuracyMillimeters:Ljava/lang/Long;

    .line 1141142
    iput-object p5, p0, LX/6km;->speedMillimetersPerSecond:Ljava/lang/Long;

    .line 1141143
    iput-object p6, p0, LX/6km;->altitudeMillimeters:Ljava/lang/Long;

    .line 1141144
    iput-object p7, p0, LX/6km;->altitudeAccuracyMillimeters:Ljava/lang/Long;

    .line 1141145
    iput-object p8, p0, LX/6km;->bearingDegrees:Ljava/lang/Long;

    .line 1141146
    return-void
.end method

.method private static a(LX/6km;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1141130
    iget-object v0, p0, LX/6km;->latitude:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 1141131
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'latitude\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6km;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1141132
    :cond_0
    iget-object v0, p0, LX/6km;->longitude:Ljava/lang/Long;

    if-nez v0, :cond_1

    .line 1141133
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'longitude\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6km;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1141134
    :cond_1
    iget-object v0, p0, LX/6km;->timestampMilliseconds:Ljava/lang/Long;

    if-nez v0, :cond_2

    .line 1141135
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'timestampMilliseconds\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6km;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1141136
    :cond_2
    return-void
.end method

.method public static b(LX/1su;)LX/6km;
    .locals 13

    .prologue
    const/4 v8, 0x0

    const/16 v12, 0xa

    .line 1141097
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v7, v8

    move-object v6, v8

    move-object v5, v8

    move-object v4, v8

    move-object v3, v8

    move-object v2, v8

    move-object v1, v8

    .line 1141098
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1141099
    iget-byte v9, v0, LX/1sw;->b:B

    if-eqz v9, :cond_8

    .line 1141100
    iget-short v9, v0, LX/1sw;->c:S

    packed-switch v9, :pswitch_data_0

    .line 1141101
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141102
    :pswitch_0
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_0

    .line 1141103
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 1141104
    :cond_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141105
    :pswitch_1
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_1

    .line 1141106
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_0

    .line 1141107
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141108
    :pswitch_2
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_2

    .line 1141109
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    .line 1141110
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141111
    :pswitch_3
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_3

    .line 1141112
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_0

    .line 1141113
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141114
    :pswitch_4
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_4

    .line 1141115
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_0

    .line 1141116
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1141117
    :pswitch_5
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_5

    .line 1141118
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    goto :goto_0

    .line 1141119
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1141120
    :pswitch_6
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_6

    .line 1141121
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    goto/16 :goto_0

    .line 1141122
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1141123
    :pswitch_7
    iget-byte v9, v0, LX/1sw;->b:B

    if-ne v9, v12, :cond_7

    .line 1141124
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    goto/16 :goto_0

    .line 1141125
    :cond_7
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1141126
    :cond_8
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1141127
    new-instance v0, LX/6km;

    invoke-direct/range {v0 .. v8}, LX/6km;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V

    .line 1141128
    invoke-static {v0}, LX/6km;->a(LX/6km;)V

    .line 1141129
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1141016
    if-eqz p2, :cond_5

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1141017
    :goto_0
    if-eqz p2, :cond_6

    const-string v0, "\n"

    move-object v1, v0

    .line 1141018
    :goto_1
    if-eqz p2, :cond_7

    const-string v0, " "

    .line 1141019
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MessageLiveLocationCoordinate"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1141020
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141021
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141022
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141023
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141024
    const-string v4, "latitude"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141025
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141026
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141027
    iget-object v4, p0, LX/6km;->latitude:Ljava/lang/Long;

    if-nez v4, :cond_8

    .line 1141028
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141029
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141030
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141031
    const-string v4, "longitude"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141032
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141033
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141034
    iget-object v4, p0, LX/6km;->longitude:Ljava/lang/Long;

    if-nez v4, :cond_9

    .line 1141035
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141036
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141037
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141038
    const-string v4, "timestampMilliseconds"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141039
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141040
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141041
    iget-object v4, p0, LX/6km;->timestampMilliseconds:Ljava/lang/Long;

    if-nez v4, :cond_a

    .line 1141042
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141043
    :goto_5
    iget-object v4, p0, LX/6km;->accuracyMillimeters:Ljava/lang/Long;

    if-eqz v4, :cond_0

    .line 1141044
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141045
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141046
    const-string v4, "accuracyMillimeters"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141047
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141048
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141049
    iget-object v4, p0, LX/6km;->accuracyMillimeters:Ljava/lang/Long;

    if-nez v4, :cond_b

    .line 1141050
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141051
    :cond_0
    :goto_6
    iget-object v4, p0, LX/6km;->speedMillimetersPerSecond:Ljava/lang/Long;

    if-eqz v4, :cond_1

    .line 1141052
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141053
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141054
    const-string v4, "speedMillimetersPerSecond"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141055
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141056
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141057
    iget-object v4, p0, LX/6km;->speedMillimetersPerSecond:Ljava/lang/Long;

    if-nez v4, :cond_c

    .line 1141058
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141059
    :cond_1
    :goto_7
    iget-object v4, p0, LX/6km;->altitudeMillimeters:Ljava/lang/Long;

    if-eqz v4, :cond_2

    .line 1141060
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141061
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141062
    const-string v4, "altitudeMillimeters"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141063
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141064
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141065
    iget-object v4, p0, LX/6km;->altitudeMillimeters:Ljava/lang/Long;

    if-nez v4, :cond_d

    .line 1141066
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141067
    :cond_2
    :goto_8
    iget-object v4, p0, LX/6km;->altitudeAccuracyMillimeters:Ljava/lang/Long;

    if-eqz v4, :cond_3

    .line 1141068
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141069
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141070
    const-string v4, "altitudeAccuracyMillimeters"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141071
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141072
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141073
    iget-object v4, p0, LX/6km;->altitudeAccuracyMillimeters:Ljava/lang/Long;

    if-nez v4, :cond_e

    .line 1141074
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141075
    :cond_3
    :goto_9
    iget-object v4, p0, LX/6km;->bearingDegrees:Ljava/lang/Long;

    if-eqz v4, :cond_4

    .line 1141076
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141077
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141078
    const-string v4, "bearingDegrees"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141079
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141080
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141081
    iget-object v0, p0, LX/6km;->bearingDegrees:Ljava/lang/Long;

    if-nez v0, :cond_f

    .line 1141082
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141083
    :cond_4
    :goto_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141084
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141085
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1141086
    :cond_5
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1141087
    :cond_6
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1141088
    :cond_7
    const-string v0, ""

    goto/16 :goto_2

    .line 1141089
    :cond_8
    iget-object v4, p0, LX/6km;->latitude:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1141090
    :cond_9
    iget-object v4, p0, LX/6km;->longitude:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1141091
    :cond_a
    iget-object v4, p0, LX/6km;->timestampMilliseconds:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1141092
    :cond_b
    iget-object v4, p0, LX/6km;->accuracyMillimeters:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1141093
    :cond_c
    iget-object v4, p0, LX/6km;->speedMillimetersPerSecond:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1141094
    :cond_d
    iget-object v4, p0, LX/6km;->altitudeMillimeters:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1141095
    :cond_e
    iget-object v4, p0, LX/6km;->altitudeAccuracyMillimeters:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 1141096
    :cond_f
    iget-object v0, p0, LX/6km;->bearingDegrees:Ljava/lang/Long;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1140915
    invoke-static {p0}, LX/6km;->a(LX/6km;)V

    .line 1140916
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1140917
    iget-object v0, p0, LX/6km;->latitude:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1140918
    sget-object v0, LX/6km;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140919
    iget-object v0, p0, LX/6km;->latitude:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1140920
    :cond_0
    iget-object v0, p0, LX/6km;->longitude:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1140921
    sget-object v0, LX/6km;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140922
    iget-object v0, p0, LX/6km;->longitude:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1140923
    :cond_1
    iget-object v0, p0, LX/6km;->timestampMilliseconds:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1140924
    sget-object v0, LX/6km;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140925
    iget-object v0, p0, LX/6km;->timestampMilliseconds:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1140926
    :cond_2
    iget-object v0, p0, LX/6km;->accuracyMillimeters:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1140927
    iget-object v0, p0, LX/6km;->accuracyMillimeters:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1140928
    sget-object v0, LX/6km;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140929
    iget-object v0, p0, LX/6km;->accuracyMillimeters:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1140930
    :cond_3
    iget-object v0, p0, LX/6km;->speedMillimetersPerSecond:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1140931
    iget-object v0, p0, LX/6km;->speedMillimetersPerSecond:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1140932
    sget-object v0, LX/6km;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140933
    iget-object v0, p0, LX/6km;->speedMillimetersPerSecond:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1140934
    :cond_4
    iget-object v0, p0, LX/6km;->altitudeMillimeters:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 1140935
    iget-object v0, p0, LX/6km;->altitudeMillimeters:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 1140936
    sget-object v0, LX/6km;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140937
    iget-object v0, p0, LX/6km;->altitudeMillimeters:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1140938
    :cond_5
    iget-object v0, p0, LX/6km;->altitudeAccuracyMillimeters:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 1140939
    iget-object v0, p0, LX/6km;->altitudeAccuracyMillimeters:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 1140940
    sget-object v0, LX/6km;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140941
    iget-object v0, p0, LX/6km;->altitudeAccuracyMillimeters:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1140942
    :cond_6
    iget-object v0, p0, LX/6km;->bearingDegrees:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 1140943
    iget-object v0, p0, LX/6km;->bearingDegrees:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 1140944
    sget-object v0, LX/6km;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1140945
    iget-object v0, p0, LX/6km;->bearingDegrees:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1140946
    :cond_7
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1140947
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1140948
    return-void
.end method

.method public final a(LX/6km;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1140957
    if-nez p1, :cond_1

    .line 1140958
    :cond_0
    :goto_0
    return v2

    .line 1140959
    :cond_1
    iget-object v0, p0, LX/6km;->latitude:Ljava/lang/Long;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1140960
    :goto_1
    iget-object v3, p1, LX/6km;->latitude:Ljava/lang/Long;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1140961
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 1140962
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140963
    iget-object v0, p0, LX/6km;->latitude:Ljava/lang/Long;

    iget-object v3, p1, LX/6km;->latitude:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140964
    :cond_3
    iget-object v0, p0, LX/6km;->longitude:Ljava/lang/Long;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1140965
    :goto_3
    iget-object v3, p1, LX/6km;->longitude:Ljava/lang/Long;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1140966
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1140967
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140968
    iget-object v0, p0, LX/6km;->longitude:Ljava/lang/Long;

    iget-object v3, p1, LX/6km;->longitude:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140969
    :cond_5
    iget-object v0, p0, LX/6km;->timestampMilliseconds:Ljava/lang/Long;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1140970
    :goto_5
    iget-object v3, p1, LX/6km;->timestampMilliseconds:Ljava/lang/Long;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1140971
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1140972
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140973
    iget-object v0, p0, LX/6km;->timestampMilliseconds:Ljava/lang/Long;

    iget-object v3, p1, LX/6km;->timestampMilliseconds:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140974
    :cond_7
    iget-object v0, p0, LX/6km;->accuracyMillimeters:Ljava/lang/Long;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1140975
    :goto_7
    iget-object v3, p1, LX/6km;->accuracyMillimeters:Ljava/lang/Long;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1140976
    :goto_8
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1140977
    :cond_8
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140978
    iget-object v0, p0, LX/6km;->accuracyMillimeters:Ljava/lang/Long;

    iget-object v3, p1, LX/6km;->accuracyMillimeters:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140979
    :cond_9
    iget-object v0, p0, LX/6km;->speedMillimetersPerSecond:Ljava/lang/Long;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1140980
    :goto_9
    iget-object v3, p1, LX/6km;->speedMillimetersPerSecond:Ljava/lang/Long;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 1140981
    :goto_a
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1140982
    :cond_a
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140983
    iget-object v0, p0, LX/6km;->speedMillimetersPerSecond:Ljava/lang/Long;

    iget-object v3, p1, LX/6km;->speedMillimetersPerSecond:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140984
    :cond_b
    iget-object v0, p0, LX/6km;->altitudeMillimeters:Ljava/lang/Long;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1140985
    :goto_b
    iget-object v3, p1, LX/6km;->altitudeMillimeters:Ljava/lang/Long;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1140986
    :goto_c
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1140987
    :cond_c
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140988
    iget-object v0, p0, LX/6km;->altitudeMillimeters:Ljava/lang/Long;

    iget-object v3, p1, LX/6km;->altitudeMillimeters:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140989
    :cond_d
    iget-object v0, p0, LX/6km;->altitudeAccuracyMillimeters:Ljava/lang/Long;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 1140990
    :goto_d
    iget-object v3, p1, LX/6km;->altitudeAccuracyMillimeters:Ljava/lang/Long;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 1140991
    :goto_e
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1140992
    :cond_e
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140993
    iget-object v0, p0, LX/6km;->altitudeAccuracyMillimeters:Ljava/lang/Long;

    iget-object v3, p1, LX/6km;->altitudeAccuracyMillimeters:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1140994
    :cond_f
    iget-object v0, p0, LX/6km;->bearingDegrees:Ljava/lang/Long;

    if-eqz v0, :cond_20

    move v0, v1

    .line 1140995
    :goto_f
    iget-object v3, p1, LX/6km;->bearingDegrees:Ljava/lang/Long;

    if-eqz v3, :cond_21

    move v3, v1

    .line 1140996
    :goto_10
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1140997
    :cond_10
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1140998
    iget-object v0, p0, LX/6km;->bearingDegrees:Ljava/lang/Long;

    iget-object v3, p1, LX/6km;->bearingDegrees:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_11
    move v2, v1

    .line 1140999
    goto/16 :goto_0

    :cond_12
    move v0, v2

    .line 1141000
    goto/16 :goto_1

    :cond_13
    move v3, v2

    .line 1141001
    goto/16 :goto_2

    :cond_14
    move v0, v2

    .line 1141002
    goto/16 :goto_3

    :cond_15
    move v3, v2

    .line 1141003
    goto/16 :goto_4

    :cond_16
    move v0, v2

    .line 1141004
    goto/16 :goto_5

    :cond_17
    move v3, v2

    .line 1141005
    goto/16 :goto_6

    :cond_18
    move v0, v2

    .line 1141006
    goto/16 :goto_7

    :cond_19
    move v3, v2

    .line 1141007
    goto/16 :goto_8

    :cond_1a
    move v0, v2

    .line 1141008
    goto/16 :goto_9

    :cond_1b
    move v3, v2

    .line 1141009
    goto/16 :goto_a

    :cond_1c
    move v0, v2

    .line 1141010
    goto :goto_b

    :cond_1d
    move v3, v2

    .line 1141011
    goto :goto_c

    :cond_1e
    move v0, v2

    .line 1141012
    goto :goto_d

    :cond_1f
    move v3, v2

    .line 1141013
    goto :goto_e

    :cond_20
    move v0, v2

    .line 1141014
    goto :goto_f

    :cond_21
    move v3, v2

    .line 1141015
    goto :goto_10
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1140953
    if-nez p1, :cond_1

    .line 1140954
    :cond_0
    :goto_0
    return v0

    .line 1140955
    :cond_1
    instance-of v1, p1, LX/6km;

    if-eqz v1, :cond_0

    .line 1140956
    check-cast p1, LX/6km;

    invoke-virtual {p0, p1}, LX/6km;->a(LX/6km;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1140952
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1140949
    sget-boolean v0, LX/6km;->a:Z

    .line 1140950
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6km;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1140951
    return-object v0
.end method
