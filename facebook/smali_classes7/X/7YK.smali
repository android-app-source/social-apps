.class public LX/7YK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;",
        "Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1219702
    const-class v0, LX/7YK;

    sput-object v0, LX/7YK;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1219703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1219704
    iput-object p1, p0, LX/7YK;->b:Landroid/content/res/Resources;

    .line 1219705
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1219706
    check-cast p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;

    .line 1219707
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1219708
    iget-object v0, p0, LX/7YK;->b:Landroid/content/res/Resources;

    .line 1219709
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 1219710
    iget v2, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->a:I

    move v2, v2

    .line 1219711
    if-eqz v2, :cond_0

    .line 1219712
    const-string v2, "limit"

    .line 1219713
    iget v3, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->a:I

    move v3, v3

    .line 1219714
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1219715
    :cond_0
    const-string v2, "scale"

    .line 1219716
    iget-object v3, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1219717
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1219718
    const-string v2, "refresh"

    .line 1219719
    iget-boolean v3, p1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->c:Z

    move v3, v3

    .line 1219720
    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1219721
    const-string v2, "location"

    invoke-virtual {p1}, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1219722
    const-string v2, "shortcut_icon_px"

    const v3, 0x7f0b06f0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1219723
    const-string v2, "alert_icon_px"

    const v3, 0x7f0b06f1

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1219724
    const-string v2, "interstitial_context"

    invoke-virtual {p1}, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1219725
    const-string v2, "format"

    const-string v3, "json"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1219726
    move-object v0, v1

    .line 1219727
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1219728
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v3, v1, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1219729
    :cond_1
    new-instance v0, LX/14N;

    const-string v1, "zeroGetRecommendedPromo"

    const-string v2, "GET"

    const-string v3, "method/mobile.zeroGetRecommendedPromo"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1219730
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1219731
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1219732
    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->a(Lorg/json/JSONObject;)Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;

    move-result-object v0

    return-object v0
.end method
