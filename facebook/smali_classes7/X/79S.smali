.class public LX/79S;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Z

.field private final b:Z

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:J

.field public final f:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;J)V
    .locals 8

    .prologue
    .line 1174058
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v6, v0

    .line 1174059
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-direct/range {v0 .. v6}, LX/79S;-><init>(ZLjava/lang/String;Ljava/lang/String;JLX/0P1;)V

    .line 1174060
    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;JLX/0P1;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1174061
    move-object v1, p0

    move v2, p1

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move-wide v6, p4

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v8}, LX/79S;-><init>(ZZLjava/lang/String;Ljava/lang/String;JLX/0P1;)V

    .line 1174062
    return-void
.end method

.method private constructor <init>(ZZLjava/lang/String;Ljava/lang/String;JLX/0P1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1174063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1174064
    iput-boolean p1, p0, LX/79S;->a:Z

    .line 1174065
    iput-boolean p2, p0, LX/79S;->b:Z

    .line 1174066
    iput-object p3, p0, LX/79S;->c:Ljava/lang/String;

    .line 1174067
    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, LX/79S;->d:Ljava/lang/String;

    .line 1174068
    iput-wide p5, p0, LX/79S;->e:J

    .line 1174069
    iput-object p7, p0, LX/79S;->f:LX/0P1;

    .line 1174070
    return-void

    .line 1174071
    :cond_0
    const-string p4, ""

    goto :goto_0
.end method
