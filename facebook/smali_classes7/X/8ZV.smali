.class public final LX/8ZV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 41

    .prologue
    .line 1364623
    const/16 v34, 0x0

    .line 1364624
    const/16 v33, 0x0

    .line 1364625
    const/16 v32, 0x0

    .line 1364626
    const/16 v31, 0x0

    .line 1364627
    const/16 v30, 0x0

    .line 1364628
    const/16 v29, 0x0

    .line 1364629
    const/16 v28, 0x0

    .line 1364630
    const/16 v27, 0x0

    .line 1364631
    const/16 v26, 0x0

    .line 1364632
    const/16 v25, 0x0

    .line 1364633
    const/16 v24, 0x0

    .line 1364634
    const/16 v23, 0x0

    .line 1364635
    const/16 v22, 0x0

    .line 1364636
    const-wide/16 v20, 0x0

    .line 1364637
    const-wide/16 v18, 0x0

    .line 1364638
    const/16 v17, 0x0

    .line 1364639
    const/16 v16, 0x0

    .line 1364640
    const/4 v15, 0x0

    .line 1364641
    const/4 v14, 0x0

    .line 1364642
    const/4 v13, 0x0

    .line 1364643
    const/4 v12, 0x0

    .line 1364644
    const/4 v11, 0x0

    .line 1364645
    const/4 v10, 0x0

    .line 1364646
    const/4 v9, 0x0

    .line 1364647
    const/4 v8, 0x0

    .line 1364648
    const/4 v7, 0x0

    .line 1364649
    const/4 v6, 0x0

    .line 1364650
    const/4 v5, 0x0

    .line 1364651
    const/4 v4, 0x0

    .line 1364652
    const/4 v3, 0x0

    .line 1364653
    const/4 v2, 0x0

    .line 1364654
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v35

    sget-object v36, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_21

    .line 1364655
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1364656
    const/4 v2, 0x0

    .line 1364657
    :goto_0
    return v2

    .line 1364658
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v36, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v36

    if-eq v2, v0, :cond_16

    .line 1364659
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1364660
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1364661
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v36

    sget-object v37, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 1364662
    const-string v36, "height"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_1

    .line 1364663
    const/4 v2, 0x1

    .line 1364664
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    move/from16 v35, v14

    move v14, v2

    goto :goto_1

    .line 1364665
    :cond_1
    const-string v36, "id"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_2

    .line 1364666
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v34, v2

    goto :goto_1

    .line 1364667
    :cond_2
    const-string v36, "initial_view_heading_degrees"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_3

    .line 1364668
    const/4 v2, 0x1

    .line 1364669
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    move/from16 v33, v13

    move v13, v2

    goto :goto_1

    .line 1364670
    :cond_3
    const-string v36, "initial_view_pitch_degrees"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_4

    .line 1364671
    const/4 v2, 0x1

    .line 1364672
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    move/from16 v32, v12

    move v12, v2

    goto :goto_1

    .line 1364673
    :cond_4
    const-string v36, "initial_view_roll_degrees"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_5

    .line 1364674
    const/4 v2, 0x1

    .line 1364675
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    move/from16 v31, v11

    move v11, v2

    goto/16 :goto_1

    .line 1364676
    :cond_5
    const-string v36, "is_spherical"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_6

    .line 1364677
    const/4 v2, 0x1

    .line 1364678
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v30, v7

    move v7, v2

    goto/16 :goto_1

    .line 1364679
    :cond_6
    const-string v36, "message"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_7

    .line 1364680
    invoke-static/range {p0 .. p1}, LX/8ZU;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 1364681
    :cond_7
    const-string v36, "playable_duration_in_ms"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_8

    .line 1364682
    const/4 v2, 0x1

    .line 1364683
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v28, v6

    move v6, v2

    goto/16 :goto_1

    .line 1364684
    :cond_8
    const-string v36, "playable_url"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_9

    .line 1364685
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v27, v2

    goto/16 :goto_1

    .line 1364686
    :cond_9
    const-string v36, "playable_url_hd"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_a

    .line 1364687
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 1364688
    :cond_a
    const-string v36, "playable_url_preferred"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_b

    .line 1364689
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 1364690
    :cond_b
    const-string v36, "playlist"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_c

    .line 1364691
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 1364692
    :cond_c
    const-string v36, "projection_type"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_d

    .line 1364693
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 1364694
    :cond_d
    const-string v36, "sphericalFullscreenAspectRatio"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_e

    .line 1364695
    const/4 v2, 0x1

    .line 1364696
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 1364697
    :cond_e
    const-string v36, "sphericalInlineAspectRatio"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_f

    .line 1364698
    const/4 v2, 0x1

    .line 1364699
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v22

    move v10, v2

    goto/16 :goto_1

    .line 1364700
    :cond_f
    const-string v36, "sphericalPlayableUrlHdString"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_10

    .line 1364701
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 1364702
    :cond_10
    const-string v36, "sphericalPlayableUrlSdString"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_11

    .line 1364703
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 1364704
    :cond_11
    const-string v36, "sphericalPlaylist"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_12

    .line 1364705
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 1364706
    :cond_12
    const-string v36, "sphericalPreferredFov"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_13

    .line 1364707
    const/4 v2, 0x1

    .line 1364708
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    move/from16 v17, v9

    move v9, v2

    goto/16 :goto_1

    .line 1364709
    :cond_13
    const-string v36, "video_preview_image"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_14

    .line 1364710
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 1364711
    :cond_14
    const-string v36, "width"

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1364712
    const/4 v2, 0x1

    .line 1364713
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    move v15, v8

    move v8, v2

    goto/16 :goto_1

    .line 1364714
    :cond_15
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1364715
    :cond_16
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1364716
    if-eqz v14, :cond_17

    .line 1364717
    const/4 v2, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1, v14}, LX/186;->a(III)V

    .line 1364718
    :cond_17
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1364719
    if-eqz v13, :cond_18

    .line 1364720
    const/4 v2, 0x2

    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1, v13}, LX/186;->a(III)V

    .line 1364721
    :cond_18
    if-eqz v12, :cond_19

    .line 1364722
    const/4 v2, 0x3

    const/4 v12, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1, v12}, LX/186;->a(III)V

    .line 1364723
    :cond_19
    if-eqz v11, :cond_1a

    .line 1364724
    const/4 v2, 0x4

    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1, v11}, LX/186;->a(III)V

    .line 1364725
    :cond_1a
    if-eqz v7, :cond_1b

    .line 1364726
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1364727
    :cond_1b
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1364728
    if-eqz v6, :cond_1c

    .line 1364729
    const/4 v2, 0x7

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1, v6}, LX/186;->a(III)V

    .line 1364730
    :cond_1c
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1364731
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1364732
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1364733
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1364734
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1364735
    if-eqz v3, :cond_1d

    .line 1364736
    const/16 v3, 0xd

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1364737
    :cond_1d
    if-eqz v10, :cond_1e

    .line 1364738
    const/16 v3, 0xe

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v22

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1364739
    :cond_1e
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1364740
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1364741
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1364742
    if-eqz v9, :cond_1f

    .line 1364743
    const/16 v2, 0x12

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 1364744
    :cond_1f
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1364745
    if-eqz v8, :cond_20

    .line 1364746
    const/16 v2, 0x14

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v3}, LX/186;->a(III)V

    .line 1364747
    :cond_20
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_21
    move/from16 v35, v34

    move/from16 v34, v33

    move/from16 v33, v32

    move/from16 v32, v31

    move/from16 v31, v30

    move/from16 v30, v29

    move/from16 v29, v28

    move/from16 v28, v27

    move/from16 v27, v26

    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v23

    move/from16 v38, v16

    move/from16 v16, v13

    move v13, v10

    move v10, v4

    move-wide/from16 v39, v20

    move/from16 v21, v22

    move/from16 v20, v17

    move/from16 v17, v14

    move-wide/from16 v22, v18

    move/from16 v19, v38

    move v14, v11

    move/from16 v18, v15

    move v11, v8

    move v15, v12

    move v12, v9

    move v8, v2

    move v9, v3

    move v3, v5

    move-wide/from16 v4, v39

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1364748
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1364749
    invoke-virtual {p0, p1, v3, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1364750
    if-eqz v0, :cond_0

    .line 1364751
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364752
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1364753
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1364754
    if-eqz v0, :cond_1

    .line 1364755
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364756
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1364757
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1364758
    if-eqz v0, :cond_2

    .line 1364759
    const-string v1, "initial_view_heading_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364760
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1364761
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1364762
    if-eqz v0, :cond_3

    .line 1364763
    const-string v1, "initial_view_pitch_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364764
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1364765
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1364766
    if-eqz v0, :cond_4

    .line 1364767
    const-string v1, "initial_view_roll_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364768
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1364769
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1364770
    if-eqz v0, :cond_5

    .line 1364771
    const-string v1, "is_spherical"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364772
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1364773
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1364774
    if-eqz v0, :cond_6

    .line 1364775
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364776
    invoke-static {p0, v0, p2}, LX/8ZU;->a(LX/15i;ILX/0nX;)V

    .line 1364777
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1364778
    if-eqz v0, :cond_7

    .line 1364779
    const-string v1, "playable_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364780
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1364781
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1364782
    if-eqz v0, :cond_8

    .line 1364783
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364784
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1364785
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1364786
    if-eqz v0, :cond_9

    .line 1364787
    const-string v1, "playable_url_hd"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364788
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1364789
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1364790
    if-eqz v0, :cond_a

    .line 1364791
    const-string v1, "playable_url_preferred"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364792
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1364793
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1364794
    if-eqz v0, :cond_b

    .line 1364795
    const-string v1, "playlist"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364796
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1364797
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1364798
    if-eqz v0, :cond_c

    .line 1364799
    const-string v1, "projection_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364800
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1364801
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1364802
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_d

    .line 1364803
    const-string v2, "sphericalFullscreenAspectRatio"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364804
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1364805
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1364806
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_e

    .line 1364807
    const-string v2, "sphericalInlineAspectRatio"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364808
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1364809
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1364810
    if-eqz v0, :cond_f

    .line 1364811
    const-string v1, "sphericalPlayableUrlHdString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364812
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1364813
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1364814
    if-eqz v0, :cond_10

    .line 1364815
    const-string v1, "sphericalPlayableUrlSdString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364816
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1364817
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1364818
    if-eqz v0, :cond_11

    .line 1364819
    const-string v1, "sphericalPlaylist"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364820
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1364821
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1364822
    if-eqz v0, :cond_12

    .line 1364823
    const-string v1, "sphericalPreferredFov"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364824
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1364825
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1364826
    if-eqz v0, :cond_13

    .line 1364827
    const-string v1, "video_preview_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364828
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1364829
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1364830
    if-eqz v0, :cond_14

    .line 1364831
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1364832
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1364833
    :cond_14
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1364834
    return-void
.end method
