.class public LX/6jw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final folders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final threadKeys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6l9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xf

    const/4 v3, 0x1

    .line 1133696
    new-instance v0, LX/1sv;

    const-string v1, "DeltaMarkUnread"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jw;->b:LX/1sv;

    .line 1133697
    new-instance v0, LX/1sw;

    const-string v1, "threadKeys"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jw;->c:LX/1sw;

    .line 1133698
    new-instance v0, LX/1sw;

    const-string v1, "folders"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jw;->d:LX/1sw;

    .line 1133699
    sput-boolean v3, LX/6jw;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6l9;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1133774
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1133775
    iput-object p1, p0, LX/6jw;->threadKeys:Ljava/util/List;

    .line 1133776
    iput-object p2, p0, LX/6jw;->folders:Ljava/util/List;

    .line 1133777
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1133726
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1133727
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1133728
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1133729
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "DeltaMarkUnread"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1133730
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133731
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133732
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133733
    const/4 v1, 0x1

    .line 1133734
    iget-object v5, p0, LX/6jw;->threadKeys:Ljava/util/List;

    if-eqz v5, :cond_0

    .line 1133735
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133736
    const-string v1, "threadKeys"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133737
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133738
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133739
    iget-object v1, p0, LX/6jw;->threadKeys:Ljava/util/List;

    if-nez v1, :cond_6

    .line 1133740
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133741
    :goto_3
    const/4 v1, 0x0

    .line 1133742
    :cond_0
    iget-object v5, p0, LX/6jw;->folders:Ljava/util/List;

    if-eqz v5, :cond_2

    .line 1133743
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133744
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133745
    const-string v1, "folders"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133746
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133747
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133748
    iget-object v0, p0, LX/6jw;->folders:Ljava/util/List;

    if-nez v0, :cond_7

    .line 1133749
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133750
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133751
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133752
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1133753
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1133754
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1133755
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1133756
    :cond_6
    iget-object v1, p0, LX/6jw;->threadKeys:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1133757
    :cond_7
    iget-object v0, p0, LX/6jw;->folders:Ljava/util/List;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1133758
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1133759
    iget-object v0, p0, LX/6jw;->threadKeys:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1133760
    iget-object v0, p0, LX/6jw;->threadKeys:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1133761
    sget-object v0, LX/6jw;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133762
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/6jw;->threadKeys:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1133763
    iget-object v0, p0, LX/6jw;->threadKeys:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6l9;

    .line 1133764
    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    goto :goto_0

    .line 1133765
    :cond_0
    iget-object v0, p0, LX/6jw;->folders:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1133766
    iget-object v0, p0, LX/6jw;->folders:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1133767
    sget-object v0, LX/6jw;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133768
    new-instance v0, LX/1u3;

    const/16 v1, 0x8

    iget-object v2, p0, LX/6jw;->folders:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1133769
    iget-object v0, p0, LX/6jw;->folders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1133770
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    goto :goto_1

    .line 1133771
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1133772
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1133773
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1133704
    if-nez p1, :cond_1

    .line 1133705
    :cond_0
    :goto_0
    return v0

    .line 1133706
    :cond_1
    instance-of v1, p1, LX/6jw;

    if-eqz v1, :cond_0

    .line 1133707
    check-cast p1, LX/6jw;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1133708
    if-nez p1, :cond_3

    .line 1133709
    :cond_2
    :goto_1
    move v0, v2

    .line 1133710
    goto :goto_0

    .line 1133711
    :cond_3
    iget-object v0, p0, LX/6jw;->threadKeys:Ljava/util/List;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1133712
    :goto_2
    iget-object v3, p1, LX/6jw;->threadKeys:Ljava/util/List;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1133713
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1133714
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133715
    iget-object v0, p0, LX/6jw;->threadKeys:Ljava/util/List;

    iget-object v3, p1, LX/6jw;->threadKeys:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133716
    :cond_5
    iget-object v0, p0, LX/6jw;->folders:Ljava/util/List;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1133717
    :goto_4
    iget-object v3, p1, LX/6jw;->folders:Ljava/util/List;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1133718
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1133719
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133720
    iget-object v0, p0, LX/6jw;->folders:Ljava/util/List;

    iget-object v3, p1, LX/6jw;->folders:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1133721
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1133722
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1133723
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1133724
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1133725
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1133703
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1133700
    sget-boolean v0, LX/6jw;->a:Z

    .line 1133701
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jw;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1133702
    return-object v0
.end method
