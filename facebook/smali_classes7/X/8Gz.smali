.class public LX/8Gz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/data/method/DeletePhotoAlbumParams;",
        "Lcom/facebook/photos/data/method/DeletePhotoAlbumResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320360
    const-class v0, LX/8Gz;

    sput-object v0, LX/8Gz;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1320358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320359
    return-void
.end method

.method public static a(LX/0QB;)LX/8Gz;
    .locals 1

    .prologue
    .line 1320361
    new-instance v0, LX/8Gz;

    invoke-direct {v0}, LX/8Gz;-><init>()V

    .line 1320362
    move-object v0, v0

    .line 1320363
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1320339
    check-cast p1, Lcom/facebook/photos/data/method/DeletePhotoAlbumParams;

    .line 1320340
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1320341
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320342
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    sget-object v2, LX/8Gz;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1320343
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1320344
    move-object v1, v1

    .line 1320345
    const-string v2, "DELETE"

    .line 1320346
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1320347
    move-object v1, v1

    .line 1320348
    iget-object v2, p1, Lcom/facebook/photos/data/method/DeletePhotoAlbumParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1320349
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1320350
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1320351
    move-object v1, v1

    .line 1320352
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1320353
    move-object v0, v1

    .line 1320354
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1320355
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1320356
    move-object v0, v0

    .line 1320357
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1320336
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1320337
    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1320338
    new-instance v1, Lcom/facebook/photos/data/method/DeletePhotoAlbumResponse;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v1, v0}, Lcom/facebook/photos/data/method/DeletePhotoAlbumResponse;-><init>(Z)V

    return-object v1
.end method
