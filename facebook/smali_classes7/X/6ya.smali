.class public final LX/6ya;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

.field public final synthetic b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

.field public final synthetic c:LX/6ye;


# direct methods
.method public constructor <init>(LX/6ye;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)V
    .locals 0

    .prologue
    .line 1160149
    iput-object p1, p0, LX/6ya;->c:LX/6ye;

    iput-object p2, p0, LX/6ya;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iput-object p3, p0, LX/6ya;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1160150
    iget-object v0, p0, LX/6ya;->c:LX/6ye;

    iget-object v1, p0, LX/6ya;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iget-object v2, p0, LX/6ya;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    .line 1160151
    const-class v3, LX/2Oo;

    invoke-static {p1, v3}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v3

    check-cast v3, LX/2Oo;

    .line 1160152
    if-eqz v3, :cond_0

    .line 1160153
    invoke-virtual {v0}, LX/6ye;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, v3, v1, v2, p0}, LX/6ye;->a(LX/2Oo;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;Ljava/lang/String;)V

    .line 1160154
    :goto_0
    return-void

    .line 1160155
    :cond_0
    invoke-virtual {v0, p1, v1, v2}, LX/6ye;->b(Ljava/lang/Throwable;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1160147
    iget-object v0, p0, LX/6ya;->c:LX/6ye;

    iget-object v1, p0, LX/6ya;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iget-object v2, p0, LX/6ya;->b:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    invoke-virtual {v0, v1, v2}, LX/6ye;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)V

    .line 1160148
    return-void
.end method
