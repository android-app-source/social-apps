.class public LX/6mj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final recipient:Ljava/lang/Long;

.field public final sender:Ljava/lang/Long;

.field public final state:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v3, 0xa

    const/4 v4, 0x1

    .line 1146540
    new-instance v0, LX/1sv;

    const-string v1, "TypingFromClientThrift"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mj;->b:LX/1sv;

    .line 1146541
    new-instance v0, LX/1sw;

    const-string v1, "recipient"

    invoke-direct {v0, v1, v3, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mj;->c:LX/1sw;

    .line 1146542
    new-instance v0, LX/1sw;

    const-string v1, "sender"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mj;->d:LX/1sw;

    .line 1146543
    new-instance v0, LX/1sw;

    const-string v1, "state"

    const/16 v2, 0x8

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mj;->e:LX/1sw;

    .line 1146544
    sput-boolean v4, LX/6mj;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1146441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1146442
    iput-object p1, p0, LX/6mj;->recipient:Ljava/lang/Long;

    .line 1146443
    iput-object p2, p0, LX/6mj;->sender:Ljava/lang/Long;

    .line 1146444
    iput-object p3, p0, LX/6mj;->state:Ljava/lang/Integer;

    .line 1146445
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1146537
    iget-object v0, p0, LX/6mj;->state:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, LX/6ml;->a:LX/1sn;

    iget-object v1, p0, LX/6mj;->state:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1146538
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'state\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6mj;->state:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1146539
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1146494
    if-eqz p2, :cond_2

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1146495
    :goto_0
    if-eqz p2, :cond_3

    const-string v0, "\n"

    move-object v1, v0

    .line 1146496
    :goto_1
    if-eqz p2, :cond_4

    const-string v0, " "

    .line 1146497
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TypingFromClientThrift"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1146498
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146499
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146500
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146501
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146502
    const-string v4, "recipient"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146503
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146504
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146505
    iget-object v4, p0, LX/6mj;->recipient:Ljava/lang/Long;

    if-nez v4, :cond_5

    .line 1146506
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146507
    :goto_3
    iget-object v4, p0, LX/6mj;->sender:Ljava/lang/Long;

    if-eqz v4, :cond_0

    .line 1146508
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146509
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146510
    const-string v4, "sender"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146511
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146512
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146513
    iget-object v4, p0, LX/6mj;->sender:Ljava/lang/Long;

    if-nez v4, :cond_6

    .line 1146514
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146515
    :cond_0
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146516
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146517
    const-string v4, "state"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146518
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146519
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146520
    iget-object v0, p0, LX/6mj;->state:Ljava/lang/Integer;

    if-nez v0, :cond_7

    .line 1146521
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146522
    :cond_1
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146523
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146524
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1146525
    :cond_2
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1146526
    :cond_3
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1146527
    :cond_4
    const-string v0, ""

    goto/16 :goto_2

    .line 1146528
    :cond_5
    iget-object v4, p0, LX/6mj;->recipient:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1146529
    :cond_6
    iget-object v4, p0, LX/6mj;->sender:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1146530
    :cond_7
    sget-object v0, LX/6ml;->b:Ljava/util/Map;

    iget-object v4, p0, LX/6mj;->state:Ljava/lang/Integer;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1146531
    if-eqz v0, :cond_8

    .line 1146532
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146533
    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146534
    :cond_8
    iget-object v4, p0, LX/6mj;->state:Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1146535
    if-eqz v0, :cond_1

    .line 1146536
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1146479
    invoke-direct {p0}, LX/6mj;->a()V

    .line 1146480
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1146481
    iget-object v0, p0, LX/6mj;->recipient:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1146482
    sget-object v0, LX/6mj;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146483
    iget-object v0, p0, LX/6mj;->recipient:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1146484
    :cond_0
    iget-object v0, p0, LX/6mj;->sender:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1146485
    iget-object v0, p0, LX/6mj;->sender:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1146486
    sget-object v0, LX/6mj;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146487
    iget-object v0, p0, LX/6mj;->sender:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1146488
    :cond_1
    iget-object v0, p0, LX/6mj;->state:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1146489
    sget-object v0, LX/6mj;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146490
    iget-object v0, p0, LX/6mj;->state:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1146491
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1146492
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1146493
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1146450
    if-nez p1, :cond_1

    .line 1146451
    :cond_0
    :goto_0
    return v0

    .line 1146452
    :cond_1
    instance-of v1, p1, LX/6mj;

    if-eqz v1, :cond_0

    .line 1146453
    check-cast p1, LX/6mj;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1146454
    if-nez p1, :cond_3

    .line 1146455
    :cond_2
    :goto_1
    move v0, v2

    .line 1146456
    goto :goto_0

    .line 1146457
    :cond_3
    iget-object v0, p0, LX/6mj;->recipient:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1146458
    :goto_2
    iget-object v3, p1, LX/6mj;->recipient:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1146459
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1146460
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146461
    iget-object v0, p0, LX/6mj;->recipient:Ljava/lang/Long;

    iget-object v3, p1, LX/6mj;->recipient:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1146462
    :cond_5
    iget-object v0, p0, LX/6mj;->sender:Ljava/lang/Long;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1146463
    :goto_4
    iget-object v3, p1, LX/6mj;->sender:Ljava/lang/Long;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1146464
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1146465
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146466
    iget-object v0, p0, LX/6mj;->sender:Ljava/lang/Long;

    iget-object v3, p1, LX/6mj;->sender:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1146467
    :cond_7
    iget-object v0, p0, LX/6mj;->state:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1146468
    :goto_6
    iget-object v3, p1, LX/6mj;->state:Ljava/lang/Integer;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1146469
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1146470
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146471
    iget-object v0, p0, LX/6mj;->state:Ljava/lang/Integer;

    iget-object v3, p1, LX/6mj;->state:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 1146472
    goto :goto_1

    :cond_a
    move v0, v2

    .line 1146473
    goto :goto_2

    :cond_b
    move v3, v2

    .line 1146474
    goto :goto_3

    :cond_c
    move v0, v2

    .line 1146475
    goto :goto_4

    :cond_d
    move v3, v2

    .line 1146476
    goto :goto_5

    :cond_e
    move v0, v2

    .line 1146477
    goto :goto_6

    :cond_f
    move v3, v2

    .line 1146478
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1146449
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1146446
    sget-boolean v0, LX/6mj;->a:Z

    .line 1146447
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mj;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1146448
    return-object v0
.end method
