.class public LX/7ye;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/photos/base/photos/LocalPhoto;

.field public final c:Lcom/facebook/photos/base/media/PhotoItem;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/75Q;

.field public final f:LX/75F;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/base/media/PhotoItem;LX/75Q;LX/75F;)V
    .locals 1

    .prologue
    .line 1279906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1279907
    iput-object p1, p0, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    .line 1279908
    iget-object v0, p1, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v0

    .line 1279909
    check-cast v0, Lcom/facebook/photos/base/photos/LocalPhoto;

    iput-object v0, p0, LX/7ye;->b:Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1279910
    iget-object v0, p0, LX/7ye;->b:Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1279911
    iget-object p1, v0, Lcom/facebook/photos/base/photos/LocalPhoto;->d:Ljava/lang/String;

    move-object v0, p1

    .line 1279912
    iput-object v0, p0, LX/7ye;->a:Ljava/lang/String;

    .line 1279913
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7ye;->d:Ljava/util/List;

    .line 1279914
    iput-object p2, p0, LX/7ye;->e:LX/75Q;

    .line 1279915
    iput-object p3, p0, LX/7ye;->f:LX/75F;

    .line 1279916
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 1279917
    iget-object v0, p0, LX/7ye;->f:LX/75F;

    iget-object v1, p0, LX/7ye;->b:Lcom/facebook/photos/base/photos/LocalPhoto;

    invoke-virtual {v0, v1}, LX/75F;->c(LX/74x;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1279918
    iget-object v0, p0, LX/7ye;->f:LX/75F;

    iget-object v1, p0, LX/7ye;->b:Lcom/facebook/photos/base/photos/LocalPhoto;

    invoke-virtual {v0, v1}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1279919
    iget-object v0, p0, LX/7ye;->e:LX/75Q;

    iget-object v1, p0, LX/7ye;->c:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v0, v1}, LX/75Q;->b(Lcom/facebook/photos/base/media/PhotoItem;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1279920
    iget-object v0, p0, LX/7ye;->f:LX/75F;

    iget-object v1, p0, LX/7ye;->b:Lcom/facebook/photos/base/photos/LocalPhoto;

    invoke-virtual {v0, v1}, LX/75F;->d(LX/74x;)Z

    move-result v0

    return v0
.end method

.method public final g()J
    .locals 4

    .prologue
    .line 1279921
    iget-object v0, p0, LX/7ye;->b:Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1279922
    iget-wide v2, v0, LX/74w;->a:J

    move-wide v0, v2

    .line 1279923
    return-wide v0
.end method
