.class public final enum LX/7hc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7hc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7hc;

.field public static final enum LOGO_SLIDE:LX/7hc;

.field public static final enum NONE:LX/7hc;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1226317
    new-instance v0, LX/7hc;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/7hc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7hc;->NONE:LX/7hc;

    .line 1226318
    new-instance v0, LX/7hc;

    const-string v1, "LOGO_SLIDE"

    invoke-direct {v0, v1, v3}, LX/7hc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7hc;->LOGO_SLIDE:LX/7hc;

    .line 1226319
    const/4 v0, 0x2

    new-array v0, v0, [LX/7hc;

    sget-object v1, LX/7hc;->NONE:LX/7hc;

    aput-object v1, v0, v2

    sget-object v1, LX/7hc;->LOGO_SLIDE:LX/7hc;

    aput-object v1, v0, v3

    sput-object v0, LX/7hc;->$VALUES:[LX/7hc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1226320
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7hc;
    .locals 1

    .prologue
    .line 1226321
    const-class v0, LX/7hc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7hc;

    return-object v0
.end method

.method public static values()[LX/7hc;
    .locals 1

    .prologue
    .line 1226322
    sget-object v0, LX/7hc;->$VALUES:[LX/7hc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7hc;

    return-object v0
.end method
