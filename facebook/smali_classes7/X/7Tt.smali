.class public final LX/7Tt;
.super LX/1PB;
.source ""


# instance fields
.field public final synthetic a:LX/7Tu;


# direct methods
.method public constructor <init>(LX/7Tu;)V
    .locals 0

    .prologue
    .line 1211023
    iput-object p1, p0, LX/7Tt;->a:LX/7Tu;

    invoke-direct {p0}, LX/1PB;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 1211024
    iget-object v0, p0, LX/7Tt;->a:LX/7Tu;

    iget-object v0, v0, LX/7Tu;->g:LX/0YU;

    invoke-virtual {v0, p2}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1211025
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move-object v0, v2

    .line 1211026
    :goto_0
    return-object v0

    .line 1211027
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v3, v4

    :goto_1
    if-ge v3, v5, :cond_2

    .line 1211028
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1a1;

    .line 1211029
    invoke-virtual {v1}, LX/1a1;->d()I

    move-result v1

    if-ne v1, p1, :cond_3

    .line 1211030
    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1a1;

    .line 1211031
    iget-object v2, p0, LX/7Tt;->a:LX/7Tu;

    const/4 v3, 0x1

    invoke-static {v2, v1, p1, v3}, LX/7Tu;->a$redex0(LX/7Tu;LX/1a1;IZ)V

    move-object v2, v1

    .line 1211032
    :cond_2
    if-nez v2, :cond_6

    .line 1211033
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v3, v4

    :goto_2
    if-ge v3, v5, :cond_6

    .line 1211034
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1a1;

    .line 1211035
    iget-object v6, p0, LX/7Tt;->a:LX/7Tu;

    iget-object v6, v6, LX/7Tu;->h:Ljava/util/Set;

    invoke-interface {v6, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1211036
    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1a1;

    .line 1211037
    iget-object v2, p0, LX/7Tt;->a:LX/7Tu;

    invoke-static {v2, v1, p1, v4}, LX/7Tu;->a$redex0(LX/7Tu;LX/1a1;IZ)V

    .line 1211038
    :goto_3
    if-nez v1, :cond_5

    .line 1211039
    invoke-interface {v0, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    .line 1211040
    iget-object v1, p0, LX/7Tt;->a:LX/7Tu;

    invoke-static {v1, v0, p1, v4}, LX/7Tu;->a$redex0(LX/7Tu;LX/1a1;IZ)V

    .line 1211041
    :goto_4
    iget-object v1, p0, LX/7Tt;->a:LX/7Tu;

    iget-object v2, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1, v2}, LX/1OR;->f(Landroid/view/View;)V

    .line 1211042
    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    goto :goto_0

    .line 1211043
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 1211044
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_4

    :cond_6
    move-object v1, v2

    goto :goto_3
.end method
