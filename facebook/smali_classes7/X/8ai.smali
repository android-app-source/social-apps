.class public final LX/8ai;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1368461
    const/4 v9, 0x0

    .line 1368462
    const/4 v8, 0x0

    .line 1368463
    const/4 v7, 0x0

    .line 1368464
    const/4 v6, 0x0

    .line 1368465
    const/4 v5, 0x0

    .line 1368466
    const/4 v4, 0x0

    .line 1368467
    const/4 v3, 0x0

    .line 1368468
    const/4 v2, 0x0

    .line 1368469
    const/4 v1, 0x0

    .line 1368470
    const/4 v0, 0x0

    .line 1368471
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 1368472
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1368473
    const/4 v0, 0x0

    .line 1368474
    :goto_0
    return v0

    .line 1368475
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1368476
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 1368477
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1368478
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1368479
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1368480
    const-string v11, "can_viewer_like"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1368481
    const/4 v1, 0x1

    .line 1368482
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 1368483
    :cond_2
    const-string v11, "cover_photo"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1368484
    invoke-static {p0, p1}, LX/8aa;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1368485
    :cond_3
    const-string v11, "does_viewer_like"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1368486
    const/4 v0, 0x1

    .line 1368487
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 1368488
    :cond_4
    const-string v11, "friends_who_like"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1368489
    invoke-static {p0, p1}, LX/8ae;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1368490
    :cond_5
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1368491
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1368492
    :cond_6
    const-string v11, "name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1368493
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1368494
    :cond_7
    const-string v11, "page_likers"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1368495
    invoke-static {p0, p1}, LX/8af;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1368496
    :cond_8
    const-string v11, "profile_photo"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1368497
    invoke-static {p0, p1}, LX/8ah;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1368498
    :cond_9
    const/16 v10, 0x8

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1368499
    if-eqz v1, :cond_a

    .line 1368500
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v9}, LX/186;->a(IZ)V

    .line 1368501
    :cond_a
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1368502
    if-eqz v0, :cond_b

    .line 1368503
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 1368504
    :cond_b
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1368505
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1368506
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1368507
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1368508
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1368509
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1368426
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1368427
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1368428
    if-eqz v0, :cond_0

    .line 1368429
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368430
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1368431
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368432
    if-eqz v0, :cond_1

    .line 1368433
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368434
    invoke-static {p0, v0, p2, p3}, LX/8aa;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368435
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1368436
    if-eqz v0, :cond_2

    .line 1368437
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368438
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1368439
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368440
    if-eqz v0, :cond_3

    .line 1368441
    const-string v1, "friends_who_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368442
    invoke-static {p0, v0, p2, p3}, LX/8ae;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368443
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1368444
    if-eqz v0, :cond_4

    .line 1368445
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368446
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1368447
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1368448
    if-eqz v0, :cond_5

    .line 1368449
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368450
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1368451
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368452
    if-eqz v0, :cond_6

    .line 1368453
    const-string v1, "page_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368454
    invoke-static {p0, v0, p2}, LX/8af;->a(LX/15i;ILX/0nX;)V

    .line 1368455
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368456
    if-eqz v0, :cond_7

    .line 1368457
    const-string v1, "profile_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368458
    invoke-static {p0, v0, p2, p3}, LX/8ah;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368459
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1368460
    return-void
.end method
