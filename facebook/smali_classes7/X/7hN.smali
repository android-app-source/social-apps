.class public LX/7hN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0zF;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0zF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1226102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1226103
    iput-object p1, p0, LX/7hN;->a:Landroid/content/Context;

    .line 1226104
    iput-object p2, p0, LX/7hN;->b:LX/0zF;

    .line 1226105
    return-void
.end method

.method public static b(LX/0QB;)LX/7hN;
    .locals 3

    .prologue
    .line 1226106
    new-instance v2, LX/7hN;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v1

    check-cast v1, LX/0zF;

    invoke-direct {v2, v0, v1}, LX/7hN;-><init>(Landroid/content/Context;LX/0zF;)V

    .line 1226107
    return-object v2
.end method


# virtual methods
.method public final a(IIZLX/107;)Lcom/facebook/ui/titlebar/Fb4aTitleBar;
    .locals 2
    .param p4    # LX/107;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1226108
    iget-object v0, p0, LX/7hN;->b:LX/0zF;

    .line 1226109
    iget-object v1, v0, LX/0zF;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v0, v1

    .line 1226110
    invoke-virtual {v0, p1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 1226111
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    .line 1226112
    const/4 v1, -0x1

    if-eq p2, v1, :cond_0

    .line 1226113
    iget-object v1, p0, LX/7hN;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1226114
    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 1226115
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object p1

    .line 1226116
    iput-object p0, p1, LX/108;->g:Ljava/lang/String;

    .line 1226117
    move-object p1, p1

    .line 1226118
    iput-object p0, p1, LX/108;->j:Ljava/lang/String;

    .line 1226119
    move-object p0, p1

    .line 1226120
    iput-boolean p3, p0, LX/108;->d:Z

    .line 1226121
    move-object p0, p0

    .line 1226122
    const/4 p1, 0x1

    .line 1226123
    iput-boolean p1, p0, LX/108;->q:Z

    .line 1226124
    move-object p0, p0

    .line 1226125
    invoke-virtual {p0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object p0

    move-object v1, p0

    .line 1226126
    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 1226127
    :cond_0
    invoke-virtual {v0, p4}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 1226128
    return-object v0
.end method
