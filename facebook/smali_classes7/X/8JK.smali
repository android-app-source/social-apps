.class public final LX/8JK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/photos/tagging/shared/protocols/FamilyNonUserMemberTagQueryModels$FamilyNonUserMemberTagQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8JL;


# direct methods
.method public constructor <init>(LX/8JL;)V
    .locals 0

    .prologue
    .line 1328354
    iput-object p1, p0, LX/8JK;->a:LX/8JL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1328355
    iget-object v0, p0, LX/8JK;->a:LX/8JL;

    iget-object v0, v0, LX/8JL;->h:LX/03V;

    const-string v1, "familyTagTypeahead"

    const-string v2, "Could not fetch family tags: "

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1328356
    iget-object v0, p0, LX/8JK;->a:LX/8JL;

    const/4 v1, 0x0

    .line 1328357
    iput-boolean v1, v0, LX/8JL;->j:Z

    .line 1328358
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1328346
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1328347
    iget-object v0, p0, LX/8JK;->a:LX/8JL;

    invoke-static {v0, p1}, LX/8JL;->a$redex0(LX/8JL;Lcom/facebook/graphql/executor/GraphQLResult;)Ljava/util/List;

    move-result-object v0

    .line 1328348
    if-eqz v0, :cond_0

    .line 1328349
    iget-object v1, p0, LX/8JK;->a:LX/8JL;

    .line 1328350
    iput-object v0, v1, LX/8JL;->i:Ljava/util/List;

    .line 1328351
    :cond_0
    iget-object v0, p0, LX/8JK;->a:LX/8JL;

    const/4 v1, 0x0

    .line 1328352
    iput-boolean v1, v0, LX/8JL;->j:Z

    .line 1328353
    return-void
.end method
