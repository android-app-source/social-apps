.class public final LX/7yC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/7yF;


# direct methods
.method public constructor <init>(LX/7yF;)V
    .locals 0

    .prologue
    .line 1278789
    iput-object p1, p0, LX/7yC;->a:LX/7yF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1278790
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1278791
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6

    .prologue
    .line 1278792
    iget-object v0, p0, LX/7yC;->a:LX/7yF;

    iget-object v0, v0, LX/7yF;->o:LX/7ws;

    .line 1278793
    iget-object v1, v0, LX/7ws;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1278794
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1278795
    new-instance v1, Lcom/facebook/events/tickets/modal/model/FieldItem;

    invoke-direct {v1, v0}, Lcom/facebook/events/tickets/modal/model/FieldItem;-><init>(Ljava/lang/String;)V

    move-object v4, v1

    .line 1278796
    :goto_0
    iget-object v0, p0, LX/7yC;->a:LX/7yF;

    iget-object v0, v0, LX/7yF;->p:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    iget-object v1, p0, LX/7yC;->a:LX/7yF;

    iget-object v1, v1, LX/7yF;->o:LX/7ws;

    .line 1278797
    iget v2, v1, LX/7ws;->a:I

    move v1, v2

    .line 1278798
    iget-object v2, p0, LX/7yC;->a:LX/7yF;

    iget-object v2, v2, LX/7yF;->o:LX/7ws;

    .line 1278799
    iget-object v3, v2, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v2, v3

    .line 1278800
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->fa_()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v2

    iget-object v3, p0, LX/7yC;->a:LX/7yF;

    iget-object v3, v3, LX/7yF;->o:LX/7ws;

    .line 1278801
    iget-object v5, v3, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v3, v5

    .line 1278802
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->eZ_()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, LX/7yC;->a:LX/7yF;

    invoke-virtual {v5}, LX/1a1;->e()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a(ILcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;Ljava/lang/String;Lcom/facebook/events/tickets/modal/model/FieldItem;I)V

    .line 1278803
    return-void

    .line 1278804
    :cond_0
    iget-object v0, p0, LX/7yC;->a:LX/7yF;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1278805
    iget-object v2, v0, LX/7yF;->p:Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;

    iget-object v3, v0, LX/7yF;->o:LX/7ws;

    .line 1278806
    iget v4, v3, LX/7ws;->a:I

    move v3, v4

    .line 1278807
    iget-object v4, v0, LX/7yF;->o:LX/7ws;

    .line 1278808
    iget-object v5, v4, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v4, v5

    .line 1278809
    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->fa_()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v4

    iget-object v5, v0, LX/7yF;->o:LX/7ws;

    .line 1278810
    iget-object p1, v5, LX/7ws;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    move-object v5, p1

    .line 1278811
    invoke-virtual {v5}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->eZ_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/events/tickets/modal/fragments/EventStartRegistrationFragment;->a(ILcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;Ljava/lang/String;)Lcom/facebook/events/tickets/modal/model/FieldItem;

    move-result-object v2

    .line 1278812
    if-nez v2, :cond_1

    .line 1278813
    new-instance v2, Lcom/facebook/events/tickets/modal/model/FieldItem;

    iget-object v3, v0, LX/7yF;->o:LX/7ws;

    .line 1278814
    iget-object v4, v3, LX/7ws;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1278815
    invoke-static {v3, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/events/tickets/modal/model/FieldItem;-><init>(LX/0P1;)V

    .line 1278816
    :goto_1
    move-object v4, v2

    .line 1278817
    goto :goto_0

    .line 1278818
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    .line 1278819
    iget-object v4, v2, Lcom/facebook/events/tickets/modal/model/FieldItem;->e:LX/0P1;

    move-object v2, v4

    .line 1278820
    invoke-direct {v3, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 1278821
    iget-object v2, v0, LX/7yF;->o:LX/7ws;

    .line 1278822
    iget-object v4, v2, LX/7ws;->d:Ljava/lang/String;

    move-object v2, v4

    .line 1278823
    invoke-interface {v3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1278824
    new-instance v2, Lcom/facebook/events/tickets/modal/model/FieldItem;

    invoke-static {v3}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/events/tickets/modal/model/FieldItem;-><init>(LX/0P1;)V

    goto :goto_1
.end method
