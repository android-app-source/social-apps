.class public final LX/7Th;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/7Tj;


# direct methods
.method public constructor <init>(LX/7Tj;)V
    .locals 0

    .prologue
    .line 1210876
    iput-object p1, p0, LX/7Th;->a:LX/7Tj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1210877
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1210878
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1210879
    iget-object v0, p0, LX/7Th;->a:LX/7Tj;

    iget-object v0, v0, LX/7Tj;->t:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    new-instance v1, LX/7Tg;

    invoke-direct {v1, p0}, LX/7Tg;-><init>(LX/7Th;)V

    invoke-virtual {v0, p1, v1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterListener;)V

    .line 1210880
    return-void
.end method
