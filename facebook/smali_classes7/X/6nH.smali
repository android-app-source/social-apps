.class public interface abstract LX/6nH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/inject/Bindings;
.end annotation


# virtual methods
.method public abstract addAlwaysPersistentGkMqttPersistenceRequirement(LX/2HL;)LX/2HL;
    .annotation build Lcom/facebook/inject/MultiBind;
    .end annotation
.end method

.method public abstract addConnectionStarter(LX/2zl;)LX/2C2;
    .annotation build Lcom/facebook/inject/MultiBind;
    .end annotation
.end method

.method public abstract getOmnistoreMqttPushHandler(LX/2a5;)LX/1fT;
    .annotation build Lcom/facebook/inject/MultiBind;
    .end annotation
.end method

.method public abstract getOmnistoreMqttTopics(LX/2I1;)LX/2AX;
    .annotation build Lcom/facebook/inject/MultiBind;
    .end annotation

    .annotation runtime Lcom/facebook/push/mqtt/service/MqttTopicList;
    .end annotation
.end method
