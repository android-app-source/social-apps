.class public LX/87o;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Landroid/content/Context;)V
    .locals 2
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1300642
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300643
    iput-object p1, p0, LX/87o;->b:LX/0tX;

    .line 1300644
    iput-object p2, p0, LX/87o;->c:Ljava/util/concurrent/Executor;

    .line 1300645
    iput-object p3, p0, LX/87o;->d:Ljava/util/concurrent/Executor;

    .line 1300646
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1855

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/87o;->a:I

    .line 1300647
    return-void
.end method

.method public static a(LX/0Px;)LX/0Px;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1300608
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1300609
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/data/FriendData;

    .line 1300610
    iget-object v4, v0, Lcom/facebook/goodfriends/data/FriendData;->d:LX/87j;

    sget-object v5, LX/87j;->SUGGESTED:LX/87j;

    if-ne v4, v5, :cond_2

    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 1300611
    if-eqz v4, :cond_0

    .line 1300612
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1300613
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1300614
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel;ZI)LX/0Px;
    .locals 13
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel;",
            "ZI)",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1300615
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1300616
    invoke-virtual {p0}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel;->a()Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel;->j()Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    .line 1300617
    :goto_0
    if-ge v2, v5, :cond_2

    .line 1300618
    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;

    .line 1300619
    if-eq v1, p2, :cond_2

    .line 1300620
    const/4 v6, 0x0

    .line 1300621
    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;->j()Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;

    move-result-object v9

    .line 1300622
    invoke-virtual {v9}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;->k()Ljava/lang/String;

    move-result-object v7

    .line 1300623
    invoke-virtual {v9}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;->l()Ljava/lang/String;

    move-result-object v8

    .line 1300624
    invoke-virtual {v9}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;->j()Ljava/lang/String;

    move-result-object v11

    .line 1300625
    invoke-virtual {v9}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;->m()LX/1vs;

    move-result-object v10

    iget v10, v10, LX/1vs;->b:I

    if-nez v10, :cond_4

    move-object v9, v6

    .line 1300626
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;->a()Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 1300627
    sget-object v10, LX/87m;->a:[I

    invoke-virtual {v0}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel;->a()Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/graphql/enums/GraphQLFriendListEditEntryStatus;->ordinal()I

    move-result v12

    aget v10, v10, v12

    packed-switch v10, :pswitch_data_0

    :cond_0
    move-object v10, v6

    .line 1300628
    :goto_2
    if-eqz v7, :cond_1

    if-eqz p1, :cond_5

    sget-object v12, LX/87j;->SELECTED:LX/87j;

    if-eq v10, v12, :cond_5

    .line 1300629
    :cond_1
    :goto_3
    move-object v0, v6

    .line 1300630
    if-eqz v0, :cond_3

    .line 1300631
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1300632
    add-int/lit8 v0, v1, 0x1

    .line 1300633
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1300634
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    goto :goto_4

    .line 1300635
    :cond_4
    invoke-virtual {v9}, Lcom/facebook/goodfriends/protocol/FetchAllFriendsAndGoodFriendsModels$GoodFriendsAudienceQueryModel$FriendListsModel$NodesModel$EditMembersModel$EditMembersNodesModel$UserModel;->m()LX/1vs;

    move-result-object v9

    iget-object v10, v9, LX/1vs;->a:LX/15i;

    iget v9, v9, LX/1vs;->b:I

    .line 1300636
    const/4 v12, 0x0

    invoke-virtual {v10, v9, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    goto :goto_1

    .line 1300637
    :pswitch_0
    sget-object v10, LX/87j;->SELECTED:LX/87j;

    goto :goto_2

    .line 1300638
    :pswitch_1
    sget-object v10, LX/87j;->NOT_SELECTED:LX/87j;

    goto :goto_2

    .line 1300639
    :pswitch_2
    sget-object v10, LX/87j;->SUGGESTED:LX/87j;

    goto :goto_2

    .line 1300640
    :pswitch_3
    sget-object v10, LX/87j;->SUGGESTED:LX/87j;

    goto :goto_2

    .line 1300641
    :cond_5
    new-instance v6, Lcom/facebook/goodfriends/data/FriendData;

    invoke-direct/range {v6 .. v11}, Lcom/facebook/goodfriends/data/FriendData;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/87j;Ljava/lang/String;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(LX/87o;ZZILX/87n;)V
    .locals 9

    .prologue
    .line 1300598
    if-eqz p1, :cond_0

    sget-object v0, LX/0zS;->d:LX/0zS;

    :goto_0
    new-instance v1, LX/87l;

    invoke-direct {v1, p0, p2, p3, p4}, LX/87l;-><init>(LX/87o;ZILX/87n;)V

    .line 1300599
    new-instance v5, LX/87y;

    invoke-direct {v5}, LX/87y;-><init>()V

    move-object v5, v5

    .line 1300600
    const-string v6, "0"

    iget v7, p0, LX/87o;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1300601
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    const-wide/16 v7, 0x384

    invoke-virtual {v5, v7, v8}, LX/0zO;->a(J)LX/0zO;

    move-result-object v5

    const/4 v6, 0x1

    .line 1300602
    iput-boolean v6, v5, LX/0zO;->p:Z

    .line 1300603
    move-object v5, v5

    .line 1300604
    iget-object v6, p0, LX/87o;->b:LX/0tX;

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    move-object v2, v5

    .line 1300605
    new-instance v3, LX/87k;

    invoke-direct {v3, p0, v1}, LX/87k;-><init>(LX/87o;LX/0TF;)V

    iget-object v4, p0, LX/87o;->d:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1300606
    return-void

    .line 1300607
    :cond_0
    sget-object v0, LX/0zS;->a:LX/0zS;

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/87o;
    .locals 5

    .prologue
    .line 1300596
    new-instance v4, LX/87o;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, v0, v1, v2, v3}, LX/87o;-><init>(LX/0tX;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Landroid/content/Context;)V

    .line 1300597
    return-object v4
.end method


# virtual methods
.method public final a(ZLX/87n;)V
    .locals 2

    .prologue
    .line 1300594
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-static {p0, p1, v0, v1, p2}, LX/87o;->a(LX/87o;ZZILX/87n;)V

    .line 1300595
    return-void
.end method
