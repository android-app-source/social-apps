.class public LX/6mf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final qualityOfService:Ljava/lang/Integer;

.field public final topicName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1146241
    new-instance v0, LX/1sv;

    const-string v1, "SubscribeGenericTopic"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mf;->b:LX/1sv;

    .line 1146242
    new-instance v0, LX/1sw;

    const-string v1, "topicName"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mf;->c:LX/1sw;

    .line 1146243
    new-instance v0, LX/1sw;

    const-string v1, "qualityOfService"

    const/16 v2, 0x8

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mf;->d:LX/1sw;

    .line 1146244
    sput-boolean v4, LX/6mf;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1146237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1146238
    iput-object p1, p0, LX/6mf;->topicName:Ljava/lang/String;

    .line 1146239
    iput-object p2, p0, LX/6mf;->qualityOfService:Ljava/lang/Integer;

    .line 1146240
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1146209
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1146210
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 1146211
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 1146212
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SubscribeGenericTopic"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1146213
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146214
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146215
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146216
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146217
    const-string v4, "topicName"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146218
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146219
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146220
    iget-object v4, p0, LX/6mf;->topicName:Ljava/lang/String;

    if-nez v4, :cond_3

    .line 1146221
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146222
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146223
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146224
    const-string v4, "qualityOfService"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146225
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146226
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146227
    iget-object v0, p0, LX/6mf;->qualityOfService:Ljava/lang/Integer;

    if-nez v0, :cond_4

    .line 1146228
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146229
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146230
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146231
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1146232
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1146233
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1146234
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 1146235
    :cond_3
    iget-object v4, p0, LX/6mf;->topicName:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1146236
    :cond_4
    iget-object v0, p0, LX/6mf;->qualityOfService:Ljava/lang/Integer;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1146173
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1146174
    iget-object v0, p0, LX/6mf;->topicName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1146175
    sget-object v0, LX/6mf;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146176
    iget-object v0, p0, LX/6mf;->topicName:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1146177
    :cond_0
    iget-object v0, p0, LX/6mf;->qualityOfService:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1146178
    sget-object v0, LX/6mf;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146179
    iget-object v0, p0, LX/6mf;->qualityOfService:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1146180
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1146181
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1146182
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1146187
    if-nez p1, :cond_1

    .line 1146188
    :cond_0
    :goto_0
    return v0

    .line 1146189
    :cond_1
    instance-of v1, p1, LX/6mf;

    if-eqz v1, :cond_0

    .line 1146190
    check-cast p1, LX/6mf;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1146191
    if-nez p1, :cond_3

    .line 1146192
    :cond_2
    :goto_1
    move v0, v2

    .line 1146193
    goto :goto_0

    .line 1146194
    :cond_3
    iget-object v0, p0, LX/6mf;->topicName:Ljava/lang/String;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1146195
    :goto_2
    iget-object v3, p1, LX/6mf;->topicName:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1146196
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1146197
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146198
    iget-object v0, p0, LX/6mf;->topicName:Ljava/lang/String;

    iget-object v3, p1, LX/6mf;->topicName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1146199
    :cond_5
    iget-object v0, p0, LX/6mf;->qualityOfService:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1146200
    :goto_4
    iget-object v3, p1, LX/6mf;->qualityOfService:Ljava/lang/Integer;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1146201
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1146202
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146203
    iget-object v0, p0, LX/6mf;->qualityOfService:Ljava/lang/Integer;

    iget-object v3, p1, LX/6mf;->qualityOfService:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1146204
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1146205
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1146206
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1146207
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1146208
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1146186
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1146183
    sget-boolean v0, LX/6mf;->a:Z

    .line 1146184
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mf;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1146185
    return-object v0
.end method
