.class public LX/70Y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/70Z;


# direct methods
.method public constructor <init>(LX/70Z;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1162423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1162424
    iput-object p1, p0, LX/70Y;->a:LX/70Z;

    .line 1162425
    return-void
.end method

.method public static b(LX/70Y;LX/0lF;)LX/0Px;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1162398
    const-string v0, "available_payment_options"

    invoke-static {p1, v0}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    .line 1162399
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1162400
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1162401
    const-string v3, "type"

    invoke-virtual {v0, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1162402
    const-string v3, "type"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/6zQ;->forValue(Ljava/lang/String;)LX/6zQ;

    move-result-object v3

    .line 1162403
    sget-object v4, LX/6zQ;->UNKNOWN:LX/6zQ;

    if-eq v3, v4, :cond_0

    .line 1162404
    iget-object v4, p0, LX/70Y;->a:LX/70Z;

    .line 1162405
    iget-object v5, v4, LX/70Z;->b:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/70U;

    .line 1162406
    invoke-interface {v5}, LX/70U;->a()LX/6zQ;

    move-result-object p1

    if-ne p1, v3, :cond_1

    .line 1162407
    :goto_1
    move-object v3, v5

    .line 1162408
    if-eqz v3, :cond_0

    .line 1162409
    invoke-interface {v3, v0}, LX/70U;->a(LX/0lF;)Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;

    move-result-object v0

    .line 1162410
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1162411
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public static c(LX/70Y;LX/0lF;)LX/0Px;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethod;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1162412
    const-string v0, "available_payment_options"

    invoke-static {p1, v0}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    .line 1162413
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1162414
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1162415
    const-string v3, "type"

    invoke-virtual {v0, v3}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1162416
    const-string v3, "type"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/6zU;->forValue(Ljava/lang/String;)LX/6zU;

    move-result-object v3

    .line 1162417
    sget-object v4, LX/6zU;->UNKNOWN:LX/6zU;

    if-eq v3, v4, :cond_0

    .line 1162418
    iget-object v4, p0, LX/70Y;->a:LX/70Z;

    invoke-virtual {v4, v3}, LX/70Z;->a(LX/6zU;)LX/70S;

    move-result-object v3

    .line 1162419
    if-eqz v3, :cond_0

    .line 1162420
    invoke-interface {v3, v0}, LX/70S;->a(LX/0lF;)Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    move-result-object v0

    .line 1162421
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1162422
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
