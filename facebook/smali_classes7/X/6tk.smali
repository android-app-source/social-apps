.class public LX/6tk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private final b:LX/6r9;

.field private final c:LX/23P;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/6r9;LX/23P;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1155155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1155156
    iput-object p1, p0, LX/6tk;->a:Landroid/content/res/Resources;

    .line 1155157
    iput-object p2, p0, LX/6tk;->b:LX/6r9;

    .line 1155158
    iput-object p3, p0, LX/6tk;->c:LX/23P;

    .line 1155159
    return-void
.end method

.method public static a(LX/6tk;Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;)LX/6sw;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1155160
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    .line 1155161
    iget-object v0, p0, LX/6tk;->b:LX/6r9;

    iget-object v4, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v4}, LX/6r9;->b(LX/6qw;)LX/6qd;

    move-result-object v0

    invoke-interface {v0, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Z

    move-result v4

    .line 1155162
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->r()LX/6tr;

    move-result-object v0

    sget-object v5, LX/6tr;->PROCESSING_SEND_PAYMENT:LX/6tr;

    if-ne v0, v5, :cond_1

    move v0, v1

    .line 1155163
    :goto_0
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->r()LX/6tr;

    move-result-object v5

    sget-object v6, LX/6tr;->FINISH:LX/6tr;

    if-ne v5, v6, :cond_2

    .line 1155164
    :goto_1
    iget-boolean v2, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->n:Z

    if-nez v2, :cond_3

    .line 1155165
    sget-object v2, LX/6sv;->INIT:LX/6sv;

    .line 1155166
    if-eqz v4, :cond_5

    .line 1155167
    sget-object v2, LX/6sv;->READY_FOR_PAYMENT:LX/6sv;

    .line 1155168
    :cond_0
    :goto_2
    move-object v0, v2

    .line 1155169
    :goto_3
    iget-object v1, p0, LX/6tk;->c:LX/23P;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1155170
    new-instance v2, LX/6sw;

    invoke-direct {v2, v0, v1}, LX/6sw;-><init>(LX/6sv;Ljava/lang/String;)V

    return-object v2

    :cond_1
    move v0, v2

    .line 1155171
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1155172
    goto :goto_1

    .line 1155173
    :cond_3
    if-nez v4, :cond_4

    if-nez v0, :cond_4

    if-eqz v1, :cond_7

    .line 1155174
    :cond_4
    sget-object v2, LX/6sv;->READY_FOR_PAYMENT:LX/6sv;

    .line 1155175
    :goto_4
    move-object v0, v2

    .line 1155176
    goto :goto_3

    .line 1155177
    :cond_5
    if-eqz v0, :cond_6

    .line 1155178
    sget-object v2, LX/6sv;->PROCESSING_PAYMENT:LX/6sv;

    goto :goto_2

    .line 1155179
    :cond_6
    if-eqz v1, :cond_0

    .line 1155180
    sget-object v2, LX/6sv;->PAYMENT_COMPLETED:LX/6sv;

    goto :goto_2

    :cond_7
    sget-object v2, LX/6sv;->INIT:LX/6sv;

    goto :goto_4
.end method

.method public static b(LX/0QB;)LX/6tk;
    .locals 4

    .prologue
    .line 1155181
    new-instance v3, LX/6tk;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p0}, LX/6r9;->a(LX/0QB;)LX/6r9;

    move-result-object v1

    check-cast v1, LX/6r9;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v2

    check-cast v2, LX/23P;

    invoke-direct {v3, v0, v1, v2}, LX/6tk;-><init>(Landroid/content/res/Resources;LX/6r9;LX/23P;)V

    .line 1155182
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;I)LX/6sw;
    .locals 1
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1155183
    iget-object v0, p0, LX/6tk;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/6tk;->a(LX/6tk;Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;)LX/6sw;

    move-result-object v0

    return-object v0
.end method
