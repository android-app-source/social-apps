.class public LX/7md;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:LX/7ma;

.field private final c:LX/7mf;

.field public final d:Landroid/content/Context;

.field private final e:LX/0Sh;

.field public final f:LX/0ad;


# direct methods
.method public constructor <init>(LX/7ma;LX/7mf;Landroid/content/Context;LX/0Sh;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1237297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1237298
    iput-object p1, p0, LX/7md;->b:LX/7ma;

    .line 1237299
    iput-object p2, p0, LX/7md;->c:LX/7mf;

    .line 1237300
    iput-object p3, p0, LX/7md;->d:Landroid/content/Context;

    .line 1237301
    iput-object p4, p0, LX/7md;->e:LX/0Sh;

    .line 1237302
    iput-object p5, p0, LX/7md;->f:LX/0ad;

    .line 1237303
    return-void
.end method

.method public static a(Lcom/facebook/composer/publish/common/PendingStory;)Z
    .locals 1

    .prologue
    .line 1237296
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    iget v0, v0, Lcom/facebook/composer/publish/common/ErrorDetails;->errorCode:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/7md;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1237282
    iget-object v0, p0, LX/7md;->b:LX/7ma;

    invoke-virtual {v0, p1}, LX/7ma;->b(Ljava/lang/String;)LX/7ml;

    move-result-object v2

    .line 1237283
    iget-object v0, p0, LX/7md;->b:LX/7ma;

    invoke-virtual {v0, p1}, LX/7mV;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1237284
    if-nez v2, :cond_1

    const/4 v0, 0x0

    move-object v1, v0

    .line 1237285
    :goto_0
    if-nez v1, :cond_2

    if-nez p3, :cond_2

    .line 1237286
    :cond_0
    :goto_1
    return-void

    .line 1237287
    :cond_1
    iget-object v0, v2, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v0

    .line 1237288
    move-object v1, v0

    goto :goto_0

    .line 1237289
    :cond_2
    iget-object v3, p0, LX/7md;->c:LX/7mf;

    if-eqz v2, :cond_3

    .line 1237290
    iget-object v0, v2, LX/7ml;->b:LX/7mm;

    move-object v0, v0

    .line 1237291
    :goto_2
    invoke-virtual {v3, v1, p3, p2, v0}, LX/7mf;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;LX/7mm;)V

    .line 1237292
    iget-object v0, p0, LX/7md;->f:LX/0ad;

    sget-short v1, LX/1aO;->aF:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 1237293
    if-eqz v0, :cond_0

    .line 1237294
    iget-object v0, p0, LX/7md;->e:LX/0Sh;

    new-instance v1, Lcom/facebook/compost/store/CompostStoryStoreUtil$1;

    invoke-direct {v1, p0}, Lcom/facebook/compost/store/CompostStoryStoreUtil$1;-><init>(LX/7md;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 1237295
    :cond_3
    sget-object v0, LX/7mm;->POST:LX/7mm;

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1237270
    iget-object v0, p0, LX/7md;->b:LX/7ma;

    invoke-virtual {v0, p1}, LX/7mV;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1237271
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1237274
    iget-object v0, p0, LX/7md;->b:LX/7ma;

    invoke-virtual {v0, p1}, LX/7ma;->b(Ljava/lang/String;)LX/7ml;

    move-result-object v0

    .line 1237275
    if-eqz v0, :cond_0

    .line 1237276
    iget-object v1, v0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1237277
    if-eqz v1, :cond_0

    iget-object v1, p0, LX/7md;->f:LX/0ad;

    .line 1237278
    iget-object v2, v0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v2

    .line 1237279
    invoke-static {v1, v0}, LX/8Nv;->a(LX/0ad;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1237280
    :goto_0
    return-void

    .line 1237281
    :cond_0
    invoke-static {p0, p1, p2, p3}, LX/7md;->c(LX/7md;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1237272
    iget-object v0, p0, LX/7md;->b:LX/7ma;

    invoke-virtual {v0, p1}, LX/7mV;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1237273
    return-void
.end method
