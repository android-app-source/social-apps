.class public final LX/8cZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/7By;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3Qc;


# direct methods
.method public constructor <init>(LX/3Qc;)V
    .locals 0

    .prologue
    .line 1374636
    iput-object p1, p0, LX/8cZ;->a:LX/3Qc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1374666
    iget-object v0, p0, LX/8cZ;->a:LX/3Qc;

    sget-object v1, LX/3Ql;->FETCH_DB_BOOTSTRAP_ENTITY_PRE_FAIL:LX/3Ql;

    .line 1374667
    iput-object v1, v0, LX/3Qc;->p:LX/3Ql;

    .line 1374668
    iget-object v0, p0, LX/8cZ;->a:LX/3Qc;

    iget-object v0, v0, LX/3Qc;->e:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_DB_BOOTSTRAP_ENTITY_FAIL:LX/3Ql;

    invoke-virtual {v0, v1, p1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 1374669
    iget-object v0, p0, LX/8cZ;->a:LX/3Qc;

    invoke-static {v0}, LX/3Qc;->h(LX/3Qc;)V

    .line 1374670
    iget-object v0, p0, LX/8cZ;->a:LX/3Qc;

    iget-object v0, v0, LX/3Qc;->i:LX/3Qe;

    const-string v1, "time_to_load_bootstrap_entities"

    invoke-virtual {v0, v1}, LX/3Qe;->d(Ljava/lang/String;)V

    .line 1374671
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1374637
    check-cast p1, LX/7By;

    .line 1374638
    iget-object v0, p0, LX/8cZ;->a:LX/3Qc;

    iget-object v0, v0, LX/3Qc;->i:LX/3Qe;

    const-string v1, "time_to_write_bootstrap_entities"

    invoke-virtual {v0, v1}, LX/3Qe;->a(Ljava/lang/String;)V

    .line 1374639
    :try_start_0
    iget-object v0, p0, LX/8cZ;->a:LX/3Qc;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/3Qc;->a$redex0(LX/3Qc;I)V

    .line 1374640
    iget-object v0, p0, LX/8cZ;->a:LX/3Qc;

    const-string v1, " "

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/3Qc;->a$redex0(LX/3Qc;Ljava/lang/String;Z)V

    .line 1374641
    iget-object v0, p0, LX/8cZ;->a:LX/3Qc;

    iget-object v0, v0, LX/3Qc;->m:LX/0Sy;

    invoke-virtual {v0}, LX/0Sy;->c()V

    .line 1374642
    iget-boolean v0, p1, LX/7By;->b:Z

    move v0, v0

    .line 1374643
    if-eqz v0, :cond_1

    .line 1374644
    iget-object v0, p0, LX/8cZ;->a:LX/3Qc;

    iget-object v0, v0, LX/3Qc;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Bf;

    invoke-virtual {v0, p1}, LX/7Bf;->b(LX/7By;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1374645
    :goto_0
    iget-object v0, p0, LX/8cZ;->a:LX/3Qc;

    invoke-static {v0}, LX/3Qc;->h(LX/3Qc;)V

    .line 1374646
    iget-object v0, p0, LX/8cZ;->a:LX/3Qc;

    iget-object v0, v0, LX/3Qc;->i:LX/3Qe;

    const-string v1, "time_to_write_bootstrap_entities"

    invoke-virtual {v0, v1}, LX/3Qe;->b(Ljava/lang/String;)V

    .line 1374647
    iget-object v0, p0, LX/8cZ;->a:LX/3Qc;

    iget-object v0, v0, LX/3Qc;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 1374648
    sget-object v1, LX/3Qm;->d:LX/0Tn;

    iget-object v2, p0, LX/8cZ;->a:LX/3Qc;

    invoke-static {v2}, LX/3Qc;->i$redex0(LX/3Qc;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1374649
    iget-wide v6, p1, LX/7By;->a:J

    move-wide v2, v6

    .line 1374650
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 1374651
    sget-object v1, LX/3Qm;->b:LX/0Tn;

    .line 1374652
    iget-wide v6, p1, LX/7By;->a:J

    move-wide v2, v6

    .line 1374653
    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 1374654
    :cond_0
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1374655
    iget-object v0, p0, LX/8cZ;->a:LX/3Qc;

    iget-object v0, v0, LX/3Qc;->i:LX/3Qe;

    const-string v1, "time_to_load_bootstrap_entities"

    invoke-virtual {v0, v1}, LX/3Qe;->c(Ljava/lang/String;)V

    .line 1374656
    :goto_1
    return-void

    .line 1374657
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/8cZ;->a:LX/3Qc;

    iget-object v0, v0, LX/3Qc;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Bf;

    invoke-virtual {v0, p1}, LX/7Bf;->a(LX/7By;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1374658
    :catch_0
    move-exception v0

    .line 1374659
    :try_start_2
    iget-object v1, p0, LX/8cZ;->a:LX/3Qc;

    sget-object v2, LX/3Ql;->INSERT_DB_BOOTSTRAP_ENTITY_FAIL:LX/3Ql;

    .line 1374660
    iput-object v2, v1, LX/3Qc;->p:LX/3Ql;

    .line 1374661
    iget-object v1, p0, LX/8cZ;->a:LX/3Qc;

    iget-object v1, v1, LX/3Qc;->e:LX/2Sc;

    sget-object v2, LX/3Ql;->INSERT_DB_BOOTSTRAP_ENTITY_FAIL:LX/3Ql;

    invoke-virtual {v1, v2, v0}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 1374662
    iget-object v1, p0, LX/8cZ;->a:LX/3Qc;

    iget-object v1, v1, LX/3Qc;->i:LX/3Qe;

    .line 1374663
    iget-boolean v2, p1, LX/7By;->b:Z

    move v2, v2

    .line 1374664
    const-string v3, "time_to_write_bootstrap_entities"

    invoke-virtual {v1, v2, v0, v3}, LX/3Qe;->a(ZLjava/lang/Exception;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1374665
    iget-object v0, p0, LX/8cZ;->a:LX/3Qc;

    invoke-static {v0}, LX/3Qc;->h(LX/3Qc;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/8cZ;->a:LX/3Qc;

    invoke-static {v1}, LX/3Qc;->h(LX/3Qc;)V

    throw v0
.end method
