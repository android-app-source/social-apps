.class public final LX/8cQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/8cI;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8cR;


# direct methods
.method public constructor <init>(LX/8cR;)V
    .locals 0

    .prologue
    .line 1374390
    iput-object p1, p0, LX/8cQ;->a:LX/8cR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 6

    .prologue
    .line 1374391
    check-cast p1, LX/8cI;

    check-cast p2, LX/8cI;

    .line 1374392
    iget-wide v4, p1, LX/8cI;->e:D

    move-wide v0, v4

    .line 1374393
    iget-wide v4, p2, LX/8cI;->e:D

    move-wide v2, v4

    .line 1374394
    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1374395
    :cond_0
    iget-wide v4, p1, LX/8cI;->e:D

    move-wide v0, v4

    .line 1374396
    iget-wide v4, p2, LX/8cI;->e:D

    move-wide v2, v4

    .line 1374397
    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
