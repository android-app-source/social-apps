.class public LX/7xJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1277330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1277331
    return-void
.end method

.method public static a(ILcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/7ws;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1277283
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1277284
    sget-object v0, LX/7wO;->ITEM_HEADING:LX/7wO;

    const/4 v3, 0x0

    .line 1277285
    new-instance v1, LX/7ws;

    invoke-direct {v1, p0, v3, v0, v3}, LX/7ws;-><init>(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)V

    move-object v0, v1

    .line 1277286
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1277287
    iget-object v3, p1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;->v:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    .line 1277288
    const/4 v8, 0x0

    .line 1277289
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1277290
    sget-object v6, LX/7xI;->b:[I

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->e()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 1277291
    :goto_1
    move-object v0, v5

    .line 1277292
    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1277293
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1277294
    :cond_0
    sget-object v0, LX/7wO;->GAP:LX/7wO;

    invoke-static {v0}, LX/7ws;->a(LX/7wO;)LX/7ws;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1277295
    return-object v2

    .line 1277296
    :pswitch_0
    const/4 p1, 0x0

    .line 1277297
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1277298
    sget-object v7, LX/7xI;->c:[I

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->fa_()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_1

    .line 1277299
    :goto_2
    move-object v6, v6

    .line 1277300
    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 1277301
    :pswitch_1
    sget-object v6, LX/7wO;->SEPARATOR:LX/7wO;

    invoke-static {v6}, LX/7ws;->a(LX/7wO;)LX/7ws;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1277302
    :pswitch_2
    sget-object v6, LX/7wO;->IMAGE:LX/7wO;

    invoke-static {p0, v0, v6, v8}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1277303
    :pswitch_3
    sget-object v6, LX/7wO;->PARAGRAPH:LX/7wO;

    invoke-static {p0, v0, v6, v8}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1277304
    :pswitch_4
    invoke-static {v6, p0, v0}, LX/7xJ;->a(Ljava/util/List;ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;)V

    .line 1277305
    sget-object v7, LX/7wO;->FIELD_TEXT:LX/7wO;

    const-string v8, "address1"

    invoke-static {p0, v0, v7, v8}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1277306
    sget-object v7, LX/7wO;->FIELD_TEXT:LX/7wO;

    const-string v8, "address2"

    invoke-static {p0, v0, v7, v8}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1277307
    sget-object v7, LX/7wO;->FIELD_TEXT:LX/7wO;

    const-string v8, "city"

    invoke-static {p0, v0, v7, v8}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1277308
    sget-object v7, LX/7wO;->FIELD_TEXT:LX/7wO;

    const-string v8, "state"

    invoke-static {p0, v0, v7, v8}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1277309
    sget-object v7, LX/7wO;->FIELD_TEXT:LX/7wO;

    const-string v8, "zipcode"

    invoke-static {p0, v0, v7, v8}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1277310
    :pswitch_5
    invoke-static {v6, p0, v0}, LX/7xJ;->a(Ljava/util/List;ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;)V

    .line 1277311
    sget-object v7, LX/7wO;->FIELD_TEXT:LX/7wO;

    const-string v8, "first_name"

    invoke-static {p0, v0, v7, v8}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1277312
    sget-object v7, LX/7wO;->FIELD_TEXT:LX/7wO;

    const-string v8, "last_name"

    invoke-static {p0, v0, v7, v8}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1277313
    sget-object v7, LX/7wO;->FIELD_TEXT:LX/7wO;

    const-string v8, "phone"

    invoke-static {p0, v0, v7, v8}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1277314
    sget-object v7, LX/7wO;->FIELD_TEXT:LX/7wO;

    const-string v8, "email"

    invoke-static {p0, v0, v7, v8}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1277315
    :pswitch_6
    invoke-static {v6, p0, v0}, LX/7xJ;->a(Ljava/util/List;ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;)V

    .line 1277316
    sget-object v7, LX/7wO;->FIELD_TEXT:LX/7wO;

    invoke-static {p0, v0, v7, p1}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1277317
    :pswitch_7
    sget-object v7, LX/7wO;->FIELD_CHECKBOX:LX/7wO;

    invoke-static {p0, v0, v7, p1}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1277318
    :pswitch_8
    invoke-static {v6, p0, v0}, LX/7xJ;->a(Ljava/util/List;ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;)V

    .line 1277319
    const/4 p1, 0x0

    .line 1277320
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->d()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1277321
    sget-object v7, LX/7wO;->FIELD_SELECT_MULTIPLE:LX/7wO;

    invoke-static {p0, v0, v7, p1}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v7

    .line 1277322
    :goto_3
    move-object v7, v7

    .line 1277323
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1277324
    :cond_1
    sget-object v7, LX/7xI;->d:[I

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->k()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_2

    .line 1277325
    sget-object v7, LX/7wO;->FIELD_SELECT_DROPDOWN:LX/7wO;

    invoke-static {p0, v0, v7, p1}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v7

    goto :goto_3

    .line 1277326
    :pswitch_9
    sget-object v7, LX/7wO;->FIELD_SELECT_EXPANDED:LX/7wO;

    invoke-static {p0, v0, v7, p1}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v7

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
    .end packed-switch
.end method

.method public static a(Ljava/util/List;ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7ws;",
            ">;I",
            "Lcom/facebook/events/graphql/EventsGraphQLInterfaces$EventTicketingInfo$RegistrationSettings$Nodes$ScreenElements$;",
            ")V"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1277327
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1277328
    sget-object v0, LX/7wO;->FIELD_HEADING:LX/7wO;

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1}, LX/7ws;->a(ILcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;LX/7wO;Ljava/lang/String;)LX/7ws;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1277329
    :cond_0
    return-void
.end method
