.class public final LX/8Xb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8CF;


# instance fields
.field public final synthetic a:LX/8Xe;


# direct methods
.method public constructor <init>(LX/8Xe;)V
    .locals 0

    .prologue
    .line 1355223
    iput-object p1, p0, LX/8Xb;->a:LX/8Xe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 1355224
    iget-object v0, p0, LX/8Xb;->a:LX/8Xe;

    iget-object v0, v0, LX/8Xe;->e:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/doodle/ColourIndicator;->setColour(I)V

    .line 1355225
    iget-object v0, p0, LX/8Xb;->a:LX/8Xe;

    iget-object v0, v0, LX/8Xe;->e:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0}, Lcom/facebook/messaging/doodle/ColourIndicator;->a()V

    .line 1355226
    iget-object v0, p0, LX/8Xb;->a:LX/8Xe;

    iget-object v0, v0, LX/8Xe;->c:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawingview/DrawingView;->setColour(I)V

    .line 1355227
    return-void
.end method

.method public final a(IFFF)V
    .locals 1

    .prologue
    .line 1355228
    iget-object v0, p0, LX/8Xb;->a:LX/8Xe;

    iget-object v0, v0, LX/8Xe;->e:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/messaging/doodle/ColourIndicator;->a(IFFF)V

    .line 1355229
    iget-object v0, p0, LX/8Xb;->a:LX/8Xe;

    iget-object v0, v0, LX/8Xe;->c:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, p4}, Lcom/facebook/drawingview/DrawingView;->setStrokeWidth(F)V

    .line 1355230
    iget-object v0, p0, LX/8Xb;->a:LX/8Xe;

    iget-object v0, v0, LX/8Xe;->c:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawingview/DrawingView;->setColour(I)V

    .line 1355231
    return-void
.end method
