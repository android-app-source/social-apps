.class public final LX/72R;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

.field public final synthetic b:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

.field public final synthetic c:LX/72T;


# direct methods
.method public constructor <init>(LX/72T;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;)V
    .locals 0

    .prologue
    .line 1164501
    iput-object p1, p0, LX/72R;->c:LX/72T;

    iput-object p2, p0, LX/72R;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object p3, p0, LX/72R;->b:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 1164502
    iget-object v0, p0, LX/72R;->c:LX/72T;

    iget-object v1, p0, LX/72R;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v2, p0, LX/72R;->c:LX/72T;

    iget-object v2, v2, LX/72T;->d:Landroid/content/Context;

    const v3, 0x7f081e63

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, p1, v2}, LX/72T;->a$redex0(LX/72T;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1164503
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1164504
    check-cast p1, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressResult;

    .line 1164505
    iget-object v0, p0, LX/72R;->c:LX/72T;

    iget-object v1, p0, LX/72R;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {p1}, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressResult;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/72R;->b:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    .line 1164506
    iget-object v6, v0, LX/72T;->g:LX/6xb;

    sget-object v7, LX/6xZ;->ADD_SHIPPING_ADDRESS:LX/6xZ;

    const-string p0, "payflows_success"

    invoke-virtual {v6, v1, v7, p0}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/String;)V

    .line 1164507
    goto :goto_1

    .line 1164508
    :goto_0
    return-void

    .line 1164509
    :goto_1
    invoke-static {}, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->newBuilder()LX/72i;

    move-result-object v6

    .line 1164510
    iput-object v2, v6, LX/72i;->a:Ljava/lang/String;

    .line 1164511
    move-object v6, v6

    .line 1164512
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->a:Ljava/lang/String;

    .line 1164513
    iput-object v7, v6, LX/72i;->b:Ljava/lang/String;

    .line 1164514
    move-object v6, v6

    .line 1164515
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->c:Ljava/lang/String;

    .line 1164516
    iput-object v7, v6, LX/72i;->c:Ljava/lang/String;

    .line 1164517
    move-object v6, v6

    .line 1164518
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->d:Ljava/lang/String;

    .line 1164519
    iput-object v7, v6, LX/72i;->d:Ljava/lang/String;

    .line 1164520
    move-object v6, v6

    .line 1164521
    const-string v7, "%s, %s"

    iget-object p0, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->e:Ljava/lang/String;

    iget-object p1, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->f:Ljava/lang/String;

    invoke-static {v7, p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 1164522
    iput-object v7, v6, LX/72i;->e:Ljava/lang/String;

    .line 1164523
    move-object v6, v6

    .line 1164524
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->e:Ljava/lang/String;

    .line 1164525
    iput-object v7, v6, LX/72i;->i:Ljava/lang/String;

    .line 1164526
    move-object v6, v6

    .line 1164527
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->f:Ljava/lang/String;

    .line 1164528
    iput-object v7, v6, LX/72i;->j:Ljava/lang/String;

    .line 1164529
    move-object v6, v6

    .line 1164530
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->g:Ljava/lang/String;

    .line 1164531
    iput-object v7, v6, LX/72i;->f:Ljava/lang/String;

    .line 1164532
    move-object v6, v6

    .line 1164533
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->h:Lcom/facebook/common/locale/Country;

    .line 1164534
    iput-object v7, v6, LX/72i;->g:Lcom/facebook/common/locale/Country;

    .line 1164535
    move-object v6, v6

    .line 1164536
    iget-object v7, v3, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;->b:Ljava/lang/String;

    .line 1164537
    iput-object v7, v6, LX/72i;->h:Ljava/lang/String;

    .line 1164538
    move-object v6, v6

    .line 1164539
    goto :goto_2

    .line 1164540
    :goto_2
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 1164541
    const-string p0, "shipping_address"

    invoke-virtual {v6}, LX/72i;->l()Lcom/facebook/payments/shipping/model/SimpleMailingAddress;

    move-result-object v6

    invoke-virtual {v7, p0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1164542
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1164543
    const-string p0, "extra_activity_result_data"

    invoke-virtual {v6, p0, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1164544
    iget-object v7, v0, LX/72T;->a:LX/6qh;

    new-instance p0, LX/73T;

    sget-object p1, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {p0, p1, v6}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    invoke-virtual {v7, p0}, LX/6qh;->a(LX/73T;)V

    goto :goto_0
.end method
