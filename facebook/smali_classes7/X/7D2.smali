.class public final LX/7D2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/5Pg;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/7Cp;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1182120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/5PR;
    .locals 6

    .prologue
    .line 1182121
    new-instance v0, LX/5PQ;

    const/16 v1, 0x18

    invoke-direct {v0, v1}, LX/5PQ;-><init>(I)V

    const/4 v1, 0x4

    .line 1182122
    iput v1, v0, LX/5PQ;->a:I

    .line 1182123
    move-object v0, v0

    .line 1182124
    const-string v1, "aPosition"

    const/high16 v2, 0x40000000    # 2.0f

    iget-object v3, p0, LX/7D2;->b:LX/7Cp;

    invoke-static {v2, v3}, LX/7D3;->b(FLX/7Cp;)LX/5Pg;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    const/4 v1, 0x0

    .line 1182125
    const/16 v2, 0x24

    new-array v3, v2, [B

    move v2, v1

    .line 1182126
    :goto_0
    const/16 v4, 0x18

    if-ge v1, v4, :cond_0

    .line 1182127
    add-int/lit8 v4, v2, 0x0

    aput-byte v1, v3, v4

    .line 1182128
    add-int/lit8 v4, v2, 0x1

    add-int/lit8 v5, v1, 0x2

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 1182129
    add-int/lit8 v4, v2, 0x2

    add-int/lit8 v5, v1, 0x1

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 1182130
    add-int/lit8 v4, v2, 0x3

    aput-byte v1, v3, v4

    .line 1182131
    add-int/lit8 v4, v2, 0x4

    add-int/lit8 v5, v1, 0x3

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 1182132
    add-int/lit8 v4, v2, 0x5

    add-int/lit8 v5, v1, 0x2

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 1182133
    add-int/lit8 v2, v2, 0x6

    .line 1182134
    add-int/lit8 v1, v1, 0x4

    int-to-byte v1, v1

    goto :goto_0

    .line 1182135
    :cond_0
    new-instance v1, LX/5PY;

    invoke-direct {v1, v3}, LX/5PY;-><init>([B)V

    move-object v1, v1

    .line 1182136
    invoke-virtual {v0, v1}, LX/5PQ;->a(LX/5PY;)LX/5PQ;

    move-result-object v0

    .line 1182137
    iget-object v1, p0, LX/7D2;->a:LX/5Pg;

    if-eqz v1, :cond_1

    .line 1182138
    const-string v1, "aTextureCoord"

    iget-object v2, p0, LX/7D2;->a:LX/5Pg;

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    .line 1182139
    :cond_1
    invoke-virtual {v0}, LX/5PQ;->a()LX/5PR;

    move-result-object v0

    return-object v0
.end method
