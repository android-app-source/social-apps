.class public LX/8Mb;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field private b:LX/7ma;

.field private c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7md;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7mW;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/03V;

.field private f:LX/0qn;

.field private g:LX/0bH;

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1EZ;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/8LV;

.field private j:LX/7m8;

.field private k:LX/0lB;

.field private l:LX/0gd;

.field private final m:LX/0SG;

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0ad;

.field private final p:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/7ma;LX/0Ot;LX/0Ot;LX/03V;LX/0qn;LX/0bH;LX/0Ot;LX/8LV;LX/7m8;LX/0lB;LX/0gd;LX/0SG;LX/0Ot;LX/0ad;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            "LX/7ma;",
            "LX/0Ot",
            "<",
            "LX/7md;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7mW;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0qn;",
            "LX/0bH;",
            "LX/0Ot",
            "<",
            "LX/1EZ;",
            ">;",
            "LX/8LV;",
            "LX/7m8;",
            "LX/0lB;",
            "LX/0gd;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/0ad;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1334812
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1334813
    iput-object p1, p0, LX/8Mb;->a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1334814
    iput-object p2, p0, LX/8Mb;->b:LX/7ma;

    .line 1334815
    iput-object p3, p0, LX/8Mb;->c:LX/0Ot;

    .line 1334816
    iput-object p4, p0, LX/8Mb;->d:LX/0Ot;

    .line 1334817
    iput-object p5, p0, LX/8Mb;->e:LX/03V;

    .line 1334818
    iput-object p6, p0, LX/8Mb;->f:LX/0qn;

    .line 1334819
    iput-object p7, p0, LX/8Mb;->g:LX/0bH;

    .line 1334820
    iput-object p8, p0, LX/8Mb;->h:LX/0Ot;

    .line 1334821
    iput-object p9, p0, LX/8Mb;->i:LX/8LV;

    .line 1334822
    iput-object p10, p0, LX/8Mb;->j:LX/7m8;

    .line 1334823
    iput-object p11, p0, LX/8Mb;->k:LX/0lB;

    .line 1334824
    iput-object p12, p0, LX/8Mb;->l:LX/0gd;

    .line 1334825
    iput-object p13, p0, LX/8Mb;->m:LX/0SG;

    .line 1334826
    iput-object p14, p0, LX/8Mb;->n:LX/0Ot;

    .line 1334827
    move-object/from16 v0, p15

    iput-object v0, p0, LX/8Mb;->o:LX/0ad;

    .line 1334828
    move-object/from16 v0, p16

    iput-object v0, p0, LX/8Mb;->p:Landroid/content/Context;

    .line 1334829
    return-void
.end method

.method public static a(LX/0QB;)LX/8Mb;
    .locals 1

    .prologue
    .line 1334927
    invoke-static {p0}, LX/8Mb;->b(LX/0QB;)LX/8Mb;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;LX/5Ro;Z)Z
    .locals 4

    .prologue
    .line 1334910
    sget-object v0, LX/5Ro;->OFFLINE_POSTING_HEADER:LX/5Ro;

    if-eq p2, v0, :cond_0

    sget-object v0, LX/5Ro;->COMPOST:LX/5Ro;

    if-eq p2, v0, :cond_0

    sget-object v0, LX/5Ro;->NOTIFICATION:LX/5Ro;

    if-ne p2, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 1334911
    :goto_0
    if-eqz v0, :cond_3

    sget-object v0, LX/8Kx;->UserRetry:LX/8Kx;

    move-object v1, v0

    .line 1334912
    :goto_1
    if-eqz p3, :cond_5

    .line 1334913
    iget-object v0, p0, LX/8Mb;->o:LX/0ad;

    sget-short p2, LX/1aO;->aG:S

    const/4 p3, 0x1

    invoke-interface {v0, p2, p3}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 1334914
    if-eqz v0, :cond_4

    .line 1334915
    iget-object v0, p0, LX/8Mb;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    const/4 v3, 0x1

    .line 1334916
    invoke-virtual {v0}, LX/1EZ;->l()Ljava/util/Set;

    move-result-object v2

    .line 1334917
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_6

    .line 1334918
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1334919
    iput-boolean v3, v2, Lcom/facebook/photos/upload/operation/UploadOperation;->I:Z

    .line 1334920
    new-instance p3, LX/8LQ;

    invoke-direct {p3, v2}, LX/8LQ;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    invoke-virtual {p3}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1EZ;->c(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    goto :goto_2

    .line 1334921
    :cond_1
    :goto_3
    iget-object v0, p0, LX/8Mb;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    invoke-virtual {v0, p1, v1}, LX/1EZ;->a(Ljava/lang/String;LX/8Kx;)Z

    move-result v0

    .line 1334922
    :goto_4
    return v0

    .line 1334923
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1334924
    :cond_3
    sget-object v0, LX/8Kx;->AutoRetry:LX/8Kx;

    move-object v1, v0

    goto :goto_1

    .line 1334925
    :cond_4
    iget-object v0, p0, LX/8Mb;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    invoke-virtual {v0, p1, v1}, LX/1EZ;->b(Ljava/lang/String;LX/8Kx;)Z

    move-result v0

    goto :goto_4

    .line 1334926
    :cond_5
    iget-object v0, p0, LX/8Mb;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    invoke-virtual {v0, p1, v1}, LX/1EZ;->a(Ljava/lang/String;LX/8Kx;)Z

    move-result v0

    goto :goto_4

    :cond_6
    goto :goto_3
.end method

.method public static b(LX/0QB;)LX/8Mb;
    .locals 19

    .prologue
    .line 1334908
    new-instance v2, LX/8Mb;

    invoke-static/range {p0 .. p0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static/range {p0 .. p0}, LX/7ma;->a(LX/0QB;)LX/7ma;

    move-result-object v4

    check-cast v4, LX/7ma;

    const/16 v5, 0x19ef

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x19ec

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v8

    check-cast v8, LX/0qn;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v9

    check-cast v9, LX/0bH;

    const/16 v10, 0xf39

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/8LV;->a(LX/0QB;)LX/8LV;

    move-result-object v11

    check-cast v11, LX/8LV;

    invoke-static/range {p0 .. p0}, LX/7m8;->a(LX/0QB;)LX/7m8;

    move-result-object v12

    check-cast v12, LX/7m8;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v13

    check-cast v13, LX/0lB;

    invoke-static/range {p0 .. p0}, LX/0gd;->a(LX/0QB;)LX/0gd;

    move-result-object v14

    check-cast v14, LX/0gd;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v15

    check-cast v15, LX/0SG;

    const/16 v16, 0x19c6

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v17

    check-cast v17, LX/0ad;

    const-class v18, Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/content/Context;

    invoke-direct/range {v2 .. v18}, LX/8Mb;-><init>(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/7ma;LX/0Ot;LX/0Ot;LX/03V;LX/0qn;LX/0bH;LX/0Ot;LX/8LV;LX/7m8;LX/0lB;LX/0gd;LX/0SG;LX/0Ot;LX/0ad;Landroid/content/Context;)V

    .line 1334909
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 11

    .prologue
    .line 1334864
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1334865
    iget-object v0, p0, LX/8Mb;->b:LX/7ma;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7ma;->b(Ljava/lang/String;)LX/7ml;

    move-result-object v1

    .line 1334866
    if-nez v1, :cond_0

    .line 1334867
    iget-object v0, p0, LX/8Mb;->e:LX/03V;

    const-string v1, "offline_post_delete_missing_story"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "couldn\'t find story, sessionId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1334868
    :goto_0
    return-void

    .line 1334869
    :cond_0
    iget-object v0, v1, LX/7ml;->b:LX/7mm;

    move-object v0, v0

    .line 1334870
    sget-object v2, LX/7mm;->COVER_PHOTO:LX/7mm;

    if-ne v0, v2, :cond_1

    .line 1334871
    iget-object v0, p0, LX/8Mb;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    iget-object v1, p0, LX/8Mb;->i:LX/8LV;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/8LS;->COVER_PHOTO:LX/8LS;

    invoke-virtual {v1, v2, v3}, LX/8LV;->a(Ljava/lang/String;LX/8LS;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v1

    const-string v2, "compost"

    invoke-virtual {v0, v1, v2}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    goto :goto_0

    .line 1334872
    :cond_1
    iget-object v0, v1, LX/7ml;->b:LX/7mm;

    move-object v0, v0

    .line 1334873
    sget-object v2, LX/7mm;->PROFILE_PIC:LX/7mm;

    if-ne v0, v2, :cond_2

    .line 1334874
    iget-object v0, p0, LX/8Mb;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    iget-object v1, p0, LX/8Mb;->i:LX/8LV;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/8LS;->PROFILE_PIC:LX/8LS;

    invoke-virtual {v1, v2, v3}, LX/8LV;->a(Ljava/lang/String;LX/8LS;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v1

    const-string v2, "compost"

    invoke-virtual {v0, v1, v2}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    goto :goto_0

    .line 1334875
    :cond_2
    iget-object v0, v1, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v0, v0

    .line 1334876
    invoke-virtual {p0, v0}, LX/8Mb;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1334877
    iget-object v0, p0, LX/8Mb;->p:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f082058

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1334878
    :cond_3
    iget-object v0, v1, LX/7ml;->b:LX/7mm;

    move-object v0, v0

    .line 1334879
    sget-object v2, LX/7mm;->POST:LX/7mm;

    if-ne v0, v2, :cond_5

    const/4 v0, 0x1

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "can\'t cancel a non post story, sessionId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1334880
    iget-object v0, v1, LX/7ml;->a:Lcom/facebook/composer/publish/common/PendingStory;

    move-object v9, v0

    .line 1334881
    iget-object v0, p0, LX/8Mb;->f:LX/0qn;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->DELETED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v0, p1, v1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 1334882
    new-instance v0, LX/1Nd;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/1Nd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    .line 1334883
    iget-object v1, p0, LX/8Mb;->g:LX/0bH;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b7;)V

    .line 1334884
    iget-object v0, p0, LX/8Mb;->g:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1334885
    iget-object v0, p0, LX/8Mb;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7md;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7md;->a(Ljava/lang/String;)V

    .line 1334886
    iget-object v0, p0, LX/8Mb;->a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(Ljava/lang/String;)V

    .line 1334887
    invoke-virtual {v9}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v10

    .line 1334888
    invoke-virtual {v10}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1334889
    iget-object v0, p0, LX/8Mb;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    iget-object v1, p0, LX/8Mb;->i:LX/8LV;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v2

    .line 1334890
    sget-object v3, LX/8LS;->TARGET:LX/8LS;

    invoke-virtual {v1, v2, v3}, LX/8LV;->a(Ljava/lang/String;LX/8LS;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v3

    move-object v1, v3

    .line 1334891
    const-string v2, "Compost"

    invoke-virtual {v0, v1, v2}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    .line 1334892
    :cond_4
    iget-object v1, p0, LX/8Mb;->j:LX/7m8;

    sget-object v2, LX/7m7;->CANCELLED:LX/7m7;

    invoke-virtual {v10}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->h()Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v0

    if-nez v0, :cond_6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v3

    :goto_2
    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->c()J

    move-result-wide v6

    new-instance v0, LX/2rc;

    invoke-direct {v0}, LX/2rc;-><init>()V

    const/4 v8, 0x0

    .line 1334893
    iput-boolean v8, v0, LX/2rc;->a:Z

    .line 1334894
    move-object v0, v0

    .line 1334895
    invoke-virtual {v0}, LX/2rc;->a()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v8

    invoke-virtual/range {v1 .. v8}, LX/7m8;->a(LX/7m7;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;JLcom/facebook/composer/publish/common/ErrorDetails;)V

    .line 1334896
    const-string v3, ""

    .line 1334897
    :try_start_0
    iget-object v0, p0, LX/8Mb;->k:LX/0lB;

    invoke-virtual {v0, v10}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 1334898
    :goto_3
    iget-object v0, p0, LX/8Mb;->l:LX/0gd;

    invoke-virtual {v10}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->e()LX/2rt;

    move-result-object v2

    invoke-virtual {v9}, Lcom/facebook/composer/publish/common/PendingStory;->i()I

    move-result v4

    invoke-virtual {v10}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->f()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    const/4 v5, 0x1

    .line 1334899
    :goto_4
    sget-object v6, LX/0ge;->COMPOSER_POST_CANCEL:LX/0ge;

    invoke-static {v6, v1}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v6

    iget-object v7, v0, LX/0gd;->b:LX/0kb;

    invoke-virtual {v7}, LX/0kb;->p()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/324;->g(Ljava/lang/String;)LX/324;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/324;->a(LX/2rt;)LX/324;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/324;->c(Ljava/lang/String;)LX/324;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/324;->a(I)LX/324;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/324;->i(Z)LX/324;

    move-result-object v6

    .line 1334900
    iget-object v7, v6, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v6, v7

    .line 1334901
    iget-object v7, v0, LX/0gd;->a:LX/0Zb;

    invoke-interface {v7, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1334902
    goto/16 :goto_0

    .line 1334903
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1334904
    :cond_6
    const/4 v3, 0x0

    goto :goto_2

    .line 1334905
    :catch_0
    move-exception v0

    .line 1334906
    iget-object v1, p0, LX/8Mb;->e:LX/03V;

    const-string v2, "publish_offline_post_header_param_json_failed"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 1334907
    :cond_7
    const/4 v5, 0x0

    goto :goto_4
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/5Ro;Z)V
    .locals 5
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1334831
    if-nez p1, :cond_1

    .line 1334832
    :cond_0
    :goto_0
    return-void

    .line 1334833
    :cond_1
    iget-object v0, p0, LX/8Mb;->o:LX/0ad;

    sget-short v1, LX/1aO;->ax:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 1334834
    if-nez v0, :cond_2

    .line 1334835
    const/4 p3, 0x0

    .line 1334836
    :cond_2
    iget-object v0, p0, LX/8Mb;->b:LX/7ma;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7ma;->b(Ljava/lang/String;)LX/7ml;

    move-result-object v0

    .line 1334837
    if-nez v0, :cond_3

    .line 1334838
    iget-object v0, p0, LX/8Mb;->e:LX/03V;

    const-string v1, "offline_post_retry_missing_story"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "couldn\'t find story, sessionId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1334839
    :cond_3
    iget-object v1, v0, LX/7ml;->b:LX/7mm;

    move-object v1, v1

    .line 1334840
    sget-object v2, LX/7mm;->COVER_PHOTO:LX/7mm;

    if-eq v1, v2, :cond_4

    .line 1334841
    iget-object v1, v0, LX/7ml;->b:LX/7mm;

    move-object v1, v1

    .line 1334842
    sget-object v2, LX/7mm;->PROFILE_PIC:LX/7mm;

    if-ne v1, v2, :cond_5

    .line 1334843
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, LX/8Mb;->a(Ljava/lang/String;LX/5Ro;Z)Z

    goto :goto_0

    .line 1334844
    :cond_5
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, LX/8Mb;->removeMessages(ILjava/lang/Object;)V

    .line 1334845
    if-nez p3, :cond_6

    iget-object v1, p0, LX/8Mb;->f:LX/0qn;

    invoke-virtual {v1, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v1, v2, :cond_0

    .line 1334846
    :cond_6
    iget-object v1, v0, LX/7ml;->a:Lcom/facebook/composer/publish/common/PendingStory;

    move-object v0, v1

    .line 1334847
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1334848
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v1

    .line 1334849
    iget-object v2, p0, LX/8Mb;->a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->c()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->a(Lcom/facebook/composer/publish/common/PublishAttemptInfo;)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    move-result-object v4

    invoke-virtual {v4, p2}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->setRetrySource(LX/5Ro;)Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->a()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v4

    .line 1334850
    invoke-static {v2, v1, v3, v4}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->b(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    .line 1334851
    iget-object v2, p0, LX/8Mb;->m:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {p1}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/facebook/composer/publish/common/PendingStory;->b(JZ)V

    .line 1334852
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1334853
    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->i()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 1334854
    const-string v3, "publishPostParams"

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->i()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1334855
    :goto_1
    iget-object v3, p0, LX/8Mb;->f:LX/0qn;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v3, p1, v4}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 1334856
    iget-object v3, p0, LX/8Mb;->a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->c(Ljava/lang/String;)V

    .line 1334857
    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->d()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1334858
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p2, p3}, LX/8Mb;->a(Ljava/lang/String;LX/5Ro;Z)Z

    move-result v1

    .line 1334859
    if-eqz v1, :cond_7

    .line 1334860
    iget-object v1, p0, LX/8Mb;->m:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v3, v1}, Lcom/facebook/composer/publish/common/PendingStory;->b(JZ)V

    .line 1334861
    :cond_7
    :goto_2
    iget-object v0, p0, LX/8Mb;->g:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto/16 :goto_0

    .line 1334862
    :cond_8
    const-string v3, "publishEditPostParamsKey"

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->h()Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_1

    .line 1334863
    :cond_9
    iget-object v0, p0, LX/8Mb;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, v2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_2
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1334830
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/8Mb;->o:LX/0ad;

    invoke-static {v0, p1}, LX/8Nv;->a(LX/0ad;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8Mb;->f:LX/0qn;

    invoke-virtual {v0, p1}, LX/0qn;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
