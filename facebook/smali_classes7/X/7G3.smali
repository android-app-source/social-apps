.class public LX/7G3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/7G3;


# instance fields
.field public final a:LX/03V;

.field public final b:LX/6Po;


# direct methods
.method public constructor <init>(LX/03V;LX/6Po;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1188699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1188700
    iput-object p1, p0, LX/7G3;->a:LX/03V;

    .line 1188701
    iput-object p2, p0, LX/7G3;->b:LX/6Po;

    .line 1188702
    return-void
.end method

.method public static a(LX/0QB;)LX/7G3;
    .locals 5

    .prologue
    .line 1188703
    sget-object v0, LX/7G3;->c:LX/7G3;

    if-nez v0, :cond_1

    .line 1188704
    const-class v1, LX/7G3;

    monitor-enter v1

    .line 1188705
    :try_start_0
    sget-object v0, LX/7G3;->c:LX/7G3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1188706
    if-eqz v2, :cond_0

    .line 1188707
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1188708
    new-instance p0, LX/7G3;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/6Po;->a(LX/0QB;)LX/6Po;

    move-result-object v4

    check-cast v4, LX/6Po;

    invoke-direct {p0, v3, v4}, LX/7G3;-><init>(LX/03V;LX/6Po;)V

    .line 1188709
    move-object v0, p0

    .line 1188710
    sput-object v0, LX/7G3;->c:LX/7G3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1188711
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1188712
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1188713
    :cond_1
    sget-object v0, LX/7G3;->c:LX/7G3;

    return-object v0

    .line 1188714
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1188715
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(LX/7GT;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 1188716
    iget-object v0, p0, LX/7G3;->b:LX/6Po;

    sget-object v1, LX/7GY;->b:LX/6Pr;

    const-string v2, "Sync exception in cache on %s queue - %s"

    iget-object v3, p1, LX/7GT;->apiString:Ljava/lang/String;

    invoke-static {v2, v3, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 1188717
    iget-object v0, p0, LX/7G3;->a:LX/03V;

    const-string v1, "sync_delta_handling_cache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sync exception in cache. queue_type = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, LX/7GT;->apiString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1188718
    return-void
.end method
