.class public LX/6mo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0AM;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/76B;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1146661
    const-class v0, LX/6mo;

    sput-object v0, LX/6mo;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/76B;)V
    .locals 1

    .prologue
    .line 1146658
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1146659
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/76B;

    iput-object v0, p0, LX/6mo;->b:LX/76B;

    .line 1146660
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1146654
    :try_start_0
    iget-object v0, p0, LX/6mo;->b:LX/76B;

    invoke-interface {v0}, LX/76B;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1146655
    :goto_0
    return-void

    .line 1146656
    :catch_0
    move-exception v0

    .line 1146657
    sget-object v1, LX/6mo;->a:Ljava/lang/Class;

    const-string v2, "Failed to deliver onFailure"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 1146650
    :try_start_0
    iget-object v0, p0, LX/6mo;->b:LX/76B;

    invoke-interface {v0, p1, p2}, LX/76B;->a(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1146651
    :goto_0
    return-void

    .line 1146652
    :catch_0
    move-exception v0

    .line 1146653
    sget-object v1, LX/6mo;->a:Ljava/lang/Class;

    const-string v2, "Failed to deliver onSuccess"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
