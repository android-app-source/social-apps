.class public final enum LX/8FW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8FW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8FW;

.field public static final enum DELAY:LX/8FW;

.field public static final enum IMMEDIATE:LX/8FW;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1317974
    new-instance v0, LX/8FW;

    const-string v1, "IMMEDIATE"

    invoke-direct {v0, v1, v2}, LX/8FW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8FW;->IMMEDIATE:LX/8FW;

    .line 1317975
    new-instance v0, LX/8FW;

    const-string v1, "DELAY"

    invoke-direct {v0, v1, v3}, LX/8FW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8FW;->DELAY:LX/8FW;

    .line 1317976
    const/4 v0, 0x2

    new-array v0, v0, [LX/8FW;

    sget-object v1, LX/8FW;->IMMEDIATE:LX/8FW;

    aput-object v1, v0, v2

    sget-object v1, LX/8FW;->DELAY:LX/8FW;

    aput-object v1, v0, v3

    sput-object v0, LX/8FW;->$VALUES:[LX/8FW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1317977
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8FW;
    .locals 1

    .prologue
    .line 1317978
    const-class v0, LX/8FW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8FW;

    return-object v0
.end method

.method public static values()[LX/8FW;
    .locals 1

    .prologue
    .line 1317979
    sget-object v0, LX/8FW;->$VALUES:[LX/8FW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8FW;

    return-object v0
.end method
