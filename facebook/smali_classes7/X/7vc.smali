.class public final LX/7vc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/events/common/EventAnalyticsParams;

.field public final synthetic c:Lcom/facebook/events/common/ActionMechanism;

.field public final synthetic d:LX/1nP;

.field public e:Z


# direct methods
.method public constructor <init>(LX/1nP;Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)V
    .locals 0

    .prologue
    .line 1274854
    iput-object p1, p0, LX/7vc;->d:LX/1nP;

    iput-object p2, p0, LX/7vc;->a:Ljava/lang/String;

    iput-object p3, p0, LX/7vc;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iput-object p4, p0, LX/7vc;->c:Lcom/facebook/events/common/ActionMechanism;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    const v0, 0x26c18f9a

    invoke-static {v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1274855
    iget-boolean v1, p0, LX/7vc;->e:Z

    if-eqz v1, :cond_0

    .line 1274856
    const v1, 0xe772741    # 3.0464E-30f

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1274857
    :goto_0
    return-void

    .line 1274858
    :cond_0
    iput-boolean v3, p0, LX/7vc;->e:Z

    .line 1274859
    iget-object v1, p0, LX/7vc;->a:Ljava/lang/String;

    iget-object v2, p0, LX/7vc;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    iget-object v3, p0, LX/7vc;->c:Lcom/facebook/events/common/ActionMechanism;

    .line 1274860
    new-instance v4, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v1, v2, v3}, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)V

    move-object v1, v4

    .line 1274861
    iget-object v2, p0, LX/7vc;->d:LX/1nP;

    iget-object v2, v2, LX/1nP;->a:LX/1nQ;

    .line 1274862
    iget-object v3, v2, LX/1nQ;->i:LX/0Zb;

    const-string v4, "event_buy_tickets_button_tapped"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 1274863
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1274864
    const-string v4, "event_permalink"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    iget-object v4, v2, LX/1nQ;->j:LX/0kv;

    iget-object v5, v2, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v4, v5}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "Event"

    invoke-virtual {v3, v4}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    iget-object v4, v1, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "mechanism"

    iget-object v5, v1, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->d:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v3

    const-string v4, "event_id"

    iget-object v5, v1, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "session_id"

    iget-object v5, v1, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "ref_module"

    iget-object v5, v1, Lcom/facebook/events/logging/BuyTicketsLoggingInfo;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "source_module"

    const-string v5, "event_ticketing"

    invoke-virtual {v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3}, LX/0oG;->d()V

    .line 1274865
    :cond_1
    iget-object v2, p0, LX/7vc;->d:LX/1nP;

    iget-object v3, p0, LX/7vc;->a:Ljava/lang/String;

    iget-object v4, p0, LX/7vc;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1274866
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3, v1, v4}, Lcom/facebook/events/tickets/modal/EventStartSelectTicketsActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/events/logging/BuyTicketsLoggingInfo;Lcom/facebook/events/common/EventAnalyticsParams;)Landroid/content/Intent;

    move-result-object v6

    .line 1274867
    iget-object v7, v2, LX/1nP;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const-class v8, Landroid/app/Activity;

    invoke-static {v5, v8}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-interface {v7, v6, v5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1274868
    new-instance v1, Lcom/facebook/events/tickets/EventBuyTicketsFlowLauncher$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/events/tickets/EventBuyTicketsFlowLauncher$1$1;-><init>(LX/7vc;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 1274869
    const v1, 0x1cbd933c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto/16 :goto_0
.end method
