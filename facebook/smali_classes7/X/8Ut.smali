.class public final LX/8Ut;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;)V
    .locals 0

    .prologue
    .line 1351577
    iput-object p1, p0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 1351559
    iget-object v0, p0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget-object v0, v0, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->g:LX/8Uw;

    iget-object v1, p0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget-object v1, v1, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->b:Ljava/lang/String;

    iget-object v2, p0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget v2, v2, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->c:I

    const/4 v3, 0x0

    iget-object v4, p0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget-object v4, v4, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->d:Ljava/util/List;

    iget-object v5, p0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget-object v5, v5, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->e:LX/8Uv;

    iget-object v6, p0, LX/8Ut;->a:Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;

    iget-object v6, v6, Lcom/facebook/quicksilver/graphql/QuicksilverMutations$2;->f:Ljava/util/Map;

    .line 1351560
    sget-object v7, LX/8TE;->EXTRAS_SCREENSHOT_IMAGE_FBID:LX/8TE;

    iget-object v7, v7, LX/8TE;->value:Ljava/lang/String;

    invoke-interface {v6, v7, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1351561
    new-instance v7, LX/4GY;

    invoke-direct {v7}, LX/4GY;-><init>()V

    .line 1351562
    const-string v8, "thread_id"

    invoke-virtual {v7, v8, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1351563
    move-object v7, v7

    .line 1351564
    const-string v8, "game_id"

    invoke-virtual {v7, v8, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1351565
    move-object v7, v7

    .line 1351566
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 1351567
    const-string p0, "score"

    invoke-virtual {v7, p0, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1351568
    move-object v7, v7

    .line 1351569
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1351570
    const-string v8, "screenshot"

    invoke-virtual {v7, v8, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1351571
    :cond_0
    new-instance v8, LX/8Ue;

    invoke-direct {v8}, LX/8Ue;-><init>()V

    move-object v8, v8

    .line 1351572
    const-string p0, "input"

    invoke-virtual {v8, p0, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1351573
    invoke-static {v8}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    .line 1351574
    iget-object v8, v0, LX/8Uw;->a:LX/0tX;

    invoke-virtual {v8, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 1351575
    new-instance v8, LX/8Uu;

    invoke-direct {v8, v0, v6, v5}, LX/8Uu;-><init>(LX/8Uw;Ljava/util/Map;LX/8Uv;)V

    iget-object p0, v0, LX/8Uw;->d:Ljava/util/concurrent/Executor;

    invoke-static {v7, v8, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1351576
    return-void
.end method
