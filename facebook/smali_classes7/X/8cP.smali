.class public final LX/8cP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0Px",
        "<",
        "LX/8cI;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:I

.field public final synthetic c:I

.field public final synthetic d:LX/8cT;


# direct methods
.method public constructor <init>(LX/8cT;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 1374381
    iput-object p1, p0, LX/8cP;->d:LX/8cT;

    iput-object p2, p0, LX/8cP;->a:Ljava/lang/String;

    iput p3, p0, LX/8cP;->b:I

    iput p4, p0, LX/8cP;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1374382
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1374383
    iget-object v0, p0, LX/8cP;->d:LX/8cT;

    iget-object v0, v0, LX/8cT;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8cF;

    iget-object v2, p0, LX/8cP;->a:Ljava/lang/String;

    iget v3, p0, LX/8cP;->b:I

    invoke-virtual {v0, v2, v3}, LX/8cF;->c(Ljava/lang/String;I)LX/8cG;

    move-result-object v0

    .line 1374384
    iget-object v2, p0, LX/8cP;->d:LX/8cT;

    invoke-static {v2, v0}, LX/8cT;->a$redex0(LX/8cT;LX/2TZ;)Ljava/util/List;

    move-result-object v2

    .line 1374385
    iget v0, p0, LX/8cP;->c:I

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-le v0, v3, :cond_0

    .line 1374386
    iget-object v0, p0, LX/8cP;->d:LX/8cT;

    iget-object v3, p0, LX/8cP;->a:Ljava/lang/String;

    iget v4, p0, LX/8cP;->c:I

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v0, v3, v4}, LX/8cT;->d(LX/8cT;Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    .line 1374387
    :goto_0
    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1374388
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1374389
    goto :goto_0
.end method
