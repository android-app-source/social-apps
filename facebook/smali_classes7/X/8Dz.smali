.class public final enum LX/8Dz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Dz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Dz;

.field public static final enum CLOSED:LX/8Dz;

.field public static final enum NOT_CLOSED:LX/8Dz;

.field public static final enum NOT_PUBLIC:LX/8Dz;

.field public static final enum OFFENSIVE:LX/8Dz;


# instance fields
.field public final mArgVal:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1313301
    new-instance v0, LX/8Dz;

    const-string v1, "OFFENSIVE"

    const-string v2, "offensive"

    invoke-direct {v0, v1, v3, v2}, LX/8Dz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Dz;->OFFENSIVE:LX/8Dz;

    .line 1313302
    new-instance v0, LX/8Dz;

    const-string v1, "NOT_PUBLIC"

    const-string v2, "not_public"

    invoke-direct {v0, v1, v4, v2}, LX/8Dz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Dz;->NOT_PUBLIC:LX/8Dz;

    .line 1313303
    new-instance v0, LX/8Dz;

    const-string v1, "NOT_CLOSED"

    const-string v2, "not_closed"

    invoke-direct {v0, v1, v5, v2}, LX/8Dz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Dz;->NOT_CLOSED:LX/8Dz;

    .line 1313304
    new-instance v0, LX/8Dz;

    const-string v1, "CLOSED"

    const-string v2, "closed"

    invoke-direct {v0, v1, v6, v2}, LX/8Dz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/8Dz;->CLOSED:LX/8Dz;

    .line 1313305
    const/4 v0, 0x4

    new-array v0, v0, [LX/8Dz;

    sget-object v1, LX/8Dz;->OFFENSIVE:LX/8Dz;

    aput-object v1, v0, v3

    sget-object v1, LX/8Dz;->NOT_PUBLIC:LX/8Dz;

    aput-object v1, v0, v4

    sget-object v1, LX/8Dz;->NOT_CLOSED:LX/8Dz;

    aput-object v1, v0, v5

    sget-object v1, LX/8Dz;->CLOSED:LX/8Dz;

    aput-object v1, v0, v6

    sput-object v0, LX/8Dz;->$VALUES:[LX/8Dz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1313306
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1313307
    iput-object p3, p0, LX/8Dz;->mArgVal:Ljava/lang/String;

    .line 1313308
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Dz;
    .locals 1

    .prologue
    .line 1313309
    const-class v0, LX/8Dz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Dz;

    return-object v0
.end method

.method public static values()[LX/8Dz;
    .locals 1

    .prologue
    .line 1313310
    sget-object v0, LX/8Dz;->$VALUES:[LX/8Dz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Dz;

    return-object v0
.end method
