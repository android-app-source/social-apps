.class public final LX/8GR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/util/List",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;",
        "LX/1FJ",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/362;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:Lcom/facebook/photos/imageprocessing/FiltersEngine;

.field private final e:Ljava/lang/String;

.field private final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Z

.field private final h:Landroid/net/Uri;

.field private final i:LX/1HI;

.field private final j:LX/1FZ;


# direct methods
.method public constructor <init>(LX/0Px;LX/0Px;ILcom/facebook/photos/imageprocessing/FiltersEngine;Ljava/lang/String;LX/0Px;Landroid/net/Uri;ZLX/1HI;LX/1FZ;)V
    .locals 3
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/362;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;I",
            "Lcom/facebook/photos/imageprocessing/FiltersEngine;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Landroid/graphics/RectF;",
            ">;",
            "Landroid/net/Uri;",
            "Z",
            "LX/1HI;",
            "LX/1FZ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1319347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1319348
    iput-object p1, p0, LX/8GR;->a:LX/0Px;

    .line 1319349
    iput-object p2, p0, LX/8GR;->b:LX/0Px;

    .line 1319350
    iput p3, p0, LX/8GR;->c:I

    .line 1319351
    iput-object p4, p0, LX/8GR;->d:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    .line 1319352
    iput-object p5, p0, LX/8GR;->e:Ljava/lang/String;

    .line 1319353
    iget-object v0, p0, LX/8GR;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8GR;->e:Ljava/lang/String;

    invoke-static {v0}, LX/5iL;->isFilter(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid filter: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/8GR;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1319354
    iput-object p6, p0, LX/8GR;->f:LX/0Px;

    .line 1319355
    iput-boolean p8, p0, LX/8GR;->g:Z

    .line 1319356
    iput-object p7, p0, LX/8GR;->h:Landroid/net/Uri;

    .line 1319357
    iput-object p9, p0, LX/8GR;->i:LX/1HI;

    .line 1319358
    iput-object p10, p0, LX/8GR;->j:LX/1FZ;

    .line 1319359
    return-void

    .line 1319360
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;LX/5i8;)V
    .locals 7

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 1319361
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 1319362
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-interface {p5}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->left:F

    invoke-interface {p5}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    div-float/2addr v3, v5

    add-float/2addr v2, v3

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 1319363
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-interface {p5}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/RectF;->top:F

    invoke-interface {p5}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1319364
    invoke-interface {p5}, LX/362;->c()F

    move-result v3

    int-to-float v4, v1

    int-to-float v5, v2

    invoke-virtual {p1, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1319365
    invoke-interface {p5}, LX/5i8;->i()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p5}, LX/5i8;->h()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1319366
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    int-to-float v1, v1

    int-to-float v2, v2

    invoke-virtual {p1, v3, v4, v1, v2}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 1319367
    :cond_0
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-interface {p5}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->left:F

    mul-float/2addr v1, v2

    .line 1319368
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-interface {p5}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/RectF;->top:F

    mul-float/2addr v2, v3

    .line 1319369
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-interface {p5}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/RectF;->left:F

    invoke-interface {p5}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    add-float/2addr v4, v5

    mul-float/2addr v3, v4

    .line 1319370
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-interface {p5}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-interface {p5}, LX/362;->a()Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    add-float/2addr v5, v6

    mul-float/2addr v4, v5

    .line 1319371
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1319372
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 1319373
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 1319374
    iget v3, p0, LX/8GR;->c:I

    int-to-float v3, v3

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    move-result v6

    invoke-virtual {v2, v3, v4, v6}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 1319375
    invoke-virtual {v2, v1, v5}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1319376
    const/4 v2, 0x0

    invoke-virtual {p1, p4, v2, v1, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1319377
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1319378
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 12

    .prologue
    .line 1319379
    check-cast p1, Ljava/util/List;

    const/4 v1, 0x0

    const/4 v9, 0x0

    .line 1319380
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1319381
    :cond_0
    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1319382
    :goto_0
    return-object v0

    .line 1319383
    :cond_1
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    .line 1319384
    instance-of v2, v0, LX/1lm;

    if-nez v2, :cond_5

    .line 1319385
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1319386
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1319387
    if-eqz v0, :cond_2

    .line 1319388
    invoke-virtual {v0}, LX/1FJ;->close()V

    goto :goto_1

    .line 1319389
    :cond_3
    iget-boolean v0, p0, LX/8GR;->g:Z

    if-eqz v0, :cond_4

    .line 1319390
    iget-object v0, p0, LX/8GR;->i:LX/1HI;

    iget-object v2, p0, LX/8GR;->h:Landroid/net/Uri;

    invoke-virtual {v0, v2}, LX/1HI;->a(Landroid/net/Uri;)V

    :cond_4
    move-object v0, v1

    goto :goto_0

    .line 1319391
    :cond_5
    :try_start_1
    iget-object v2, p0, LX/8GR;->e:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    sget-object v2, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v2}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/8GR;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 1319392
    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1319393
    iget-object v2, p0, LX/8GR;->j:LX/1FZ;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sget-object v5, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v4, v5}, LX/1FZ;->a(IILjava/lang/Object;)LX/1FJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 1319394
    :try_start_2
    iget-object v2, p0, LX/8GR;->d:Lcom/facebook/photos/imageprocessing/FiltersEngine;

    invoke-virtual {v2, v0}, Lcom/facebook/photos/imageprocessing/FiltersEngine;->a(Landroid/graphics/Bitmap;)LX/8Ht;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v2

    .line 1319395
    :try_start_3
    iget-object v0, p0, LX/8GR;->f:LX/0Px;

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/8GR;->f:LX/0Px;

    const-class v4, Landroid/graphics/RectF;

    invoke-static {v0, v4}, LX/0Ph;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/graphics/RectF;

    :goto_2
    invoke-virtual {v2, v0}, LX/8Ht;->a([Landroid/graphics/RectF;)V

    .line 1319396
    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iget-object v4, p0, LX/8GR;->e:Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, LX/8Ht;->a(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    .line 1319397
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1319398
    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-object v6, v2

    move-object v7, v3

    move-object v3, v0

    .line 1319399
    :goto_3
    :try_start_4
    iget-object v0, p0, LX/8GR;->j:LX/1FZ;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sget-object v5, Lcom/facebook/photos/creativeediting/utilities/CreativeEditingBitmapHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v4, v5}, LX/1FZ;->a(IILjava/lang/Object;)LX/1FJ;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-result-object v8

    .line 1319400
    :try_start_5
    new-instance v1, Landroid/graphics/Canvas;

    invoke-virtual {v8}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1319401
    new-instance v2, Landroid/graphics/Paint;

    const/4 v0, 0x7

    invoke-direct {v2, v0}, Landroid/graphics/Paint;-><init>(I)V

    .line 1319402
    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v0, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    move v10, v9

    .line 1319403
    :goto_4
    iget-object v0, p0, LX/8GR;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v10, v0, :cond_9

    .line 1319404
    add-int/lit8 v0, v10, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    .line 1319405
    instance-of v4, v0, LX/1lm;

    if-eqz v4, :cond_6

    .line 1319406
    iget-object v4, p0, LX/8GR;->a:LX/0Px;

    invoke-virtual {v4, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, LX/5i8;

    if-eqz v4, :cond_6

    .line 1319407
    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1319408
    iget-object v0, p0, LX/8GR;->a:LX/0Px;

    invoke-virtual {v0, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/5i8;

    move-object v0, p0

    .line 1319409
    invoke-direct/range {v0 .. v5}, LX/8GR;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;LX/5i8;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 1319410
    :cond_6
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_4

    .line 1319411
    :cond_7
    const/4 v0, 0x0

    :try_start_6
    new-array v0, v0, [Landroid/graphics/RectF;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto/16 :goto_2

    .line 1319412
    :cond_8
    :try_start_7
    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v3

    move-object v6, v1

    move-object v7, v1

    goto :goto_3

    .line 1319413
    :cond_9
    :goto_5
    :try_start_8
    iget-object v0, p0, LX/8GR;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v9, v0, :cond_b

    .line 1319414
    add-int/lit8 v0, v9, 0x1

    iget-object v4, p0, LX/8GR;->a:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    add-int/2addr v0, v4

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    .line 1319415
    instance-of v4, v0, LX/1lm;

    if-eqz v4, :cond_a

    .line 1319416
    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1319417
    iget-object v0, p0, LX/8GR;->b:LX/0Px;

    invoke-virtual {v0, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/5i8;

    move-object v0, p0

    .line 1319418
    invoke-direct/range {v0 .. v5}, LX/8GR;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;LX/5i8;)V

    .line 1319419
    :cond_a
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_5

    .line 1319420
    :cond_b
    invoke-virtual {v8}, LX/1FJ;->b()LX/1FJ;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    move-result-object v1

    .line 1319421
    if-eqz v6, :cond_c

    .line 1319422
    :try_start_9
    invoke-virtual {v6}, LX/8Ht;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0

    .line 1319423
    :cond_c
    if-eqz v8, :cond_d

    .line 1319424
    invoke-virtual {v8}, LX/1FJ;->close()V

    .line 1319425
    :cond_d
    if-eqz v7, :cond_e

    .line 1319426
    invoke-virtual {v7}, LX/1FJ;->close()V

    .line 1319427
    :cond_e
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_f
    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1319428
    if-eqz v0, :cond_f

    .line 1319429
    invoke-virtual {v0}, LX/1FJ;->close()V

    goto :goto_6

    .line 1319430
    :catch_0
    move-exception v0

    .line 1319431
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1319432
    :cond_10
    iget-boolean v0, p0, LX/8GR;->g:Z

    if-eqz v0, :cond_11

    .line 1319433
    iget-object v0, p0, LX/8GR;->i:LX/1HI;

    iget-object v2, p0, LX/8GR;->h:Landroid/net/Uri;

    invoke-virtual {v0, v2}, LX/1HI;->a(Landroid/net/Uri;)V

    :cond_11
    move-object v0, v1

    goto/16 :goto_0

    .line 1319434
    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    move-object v11, v0

    move-object v0, v1

    move-object v1, v11

    .line 1319435
    :goto_7
    if-eqz v0, :cond_12

    .line 1319436
    :try_start_a
    invoke-virtual {v0}, LX/8Ht;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1

    .line 1319437
    :cond_12
    if-eqz v3, :cond_13

    .line 1319438
    invoke-virtual {v3}, LX/1FJ;->close()V

    .line 1319439
    :cond_13
    if-eqz v2, :cond_14

    .line 1319440
    invoke-virtual {v2}, LX/1FJ;->close()V

    .line 1319441
    :cond_14
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_15
    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1319442
    if-eqz v0, :cond_15

    .line 1319443
    invoke-virtual {v0}, LX/1FJ;->close()V

    goto :goto_8

    .line 1319444
    :catch_1
    move-exception v0

    .line 1319445
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1319446
    :cond_16
    iget-boolean v0, p0, LX/8GR;->g:Z

    if-eqz v0, :cond_17

    .line 1319447
    iget-object v0, p0, LX/8GR;->i:LX/1HI;

    iget-object v2, p0, LX/8GR;->h:Landroid/net/Uri;

    invoke-virtual {v0, v2}, LX/1HI;->a(Landroid/net/Uri;)V

    :cond_17
    throw v1

    .line 1319448
    :catchall_1
    move-exception v0

    move-object v2, v3

    move-object v3, v1

    move-object v11, v0

    move-object v0, v1

    move-object v1, v11

    goto :goto_7

    :catchall_2
    move-exception v0

    move-object v11, v0

    move-object v0, v2

    move-object v2, v3

    move-object v3, v1

    move-object v1, v11

    goto :goto_7

    :catchall_3
    move-exception v0

    move-object v2, v7

    move-object v3, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_7

    :catchall_4
    move-exception v0

    move-object v1, v0

    move-object v2, v7

    move-object v3, v8

    move-object v0, v6

    goto :goto_7
.end method
