.class public LX/7mA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/composer/publish/common/PollUploadParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1236390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 1236391
    check-cast p1, Lcom/facebook/composer/publish/common/PollUploadParams;

    .line 1236392
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1236393
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "id"

    .line 1236394
    iget-object v3, p1, Lcom/facebook/composer/publish/common/PollUploadParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1236395
    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1236396
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "question"

    .line 1236397
    iget-object v3, p1, Lcom/facebook/composer/publish/common/PollUploadParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1236398
    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1236399
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "allow_new_options"

    .line 1236400
    iget-boolean v3, p1, Lcom/facebook/composer/publish/common/PollUploadParams;->d:Z

    move v3, v3

    .line 1236401
    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1236402
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "choose_multiple_options"

    .line 1236403
    iget-boolean v3, p1, Lcom/facebook/composer/publish/common/PollUploadParams;->e:Z

    move v3, v3

    .line 1236404
    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1236405
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "published"

    .line 1236406
    iget-boolean v3, p1, Lcom/facebook/composer/publish/common/PollUploadParams;->f:Z

    move v3, v3

    .line 1236407
    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1236408
    iget-object v0, p1, Lcom/facebook/composer/publish/common/PollUploadParams;->c:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v3, v0

    .line 1236409
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1236410
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "options[]"

    invoke-direct {v5, v6, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1236411
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1236412
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1236413
    iget-object v1, p1, Lcom/facebook/composer/publish/common/PollUploadParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1236414
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/questions"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1236415
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v3, "uploadPoll"

    .line 1236416
    iput-object v3, v1, LX/14O;->b:Ljava/lang/String;

    .line 1236417
    move-object v1, v1

    .line 1236418
    const-string v3, "POST"

    .line 1236419
    iput-object v3, v1, LX/14O;->c:Ljava/lang/String;

    .line 1236420
    move-object v1, v1

    .line 1236421
    iput-object v0, v1, LX/14O;->d:Ljava/lang/String;

    .line 1236422
    move-object v0, v1

    .line 1236423
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1236424
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1236425
    move-object v0, v0

    .line 1236426
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1236427
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1236428
    move-object v0, v0

    .line 1236429
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1236430
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1236431
    :cond_0
    const/4 v0, 0x0

    .line 1236432
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
