.class public final LX/86v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1298108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1298110
    new-instance v0, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    invoke-direct {v0, p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1298109
    new-array v0, p1, [Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    return-object v0
.end method
