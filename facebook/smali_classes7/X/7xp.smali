.class public final LX/7xp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/7og;

.field public final synthetic b:Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;LX/7og;)V
    .locals 0

    .prologue
    .line 1277879
    iput-object p1, p0, LX/7xp;->b:Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;

    iput-object p2, p0, LX/7xp;->a:LX/7og;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    const v0, 0x53d668cc

    invoke-static {v4, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1277880
    iget-object v1, p0, LX/7xp;->b:Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;

    iget-object v1, v1, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->j:LX/1nQ;

    iget-object v2, p0, LX/7xp;->a:LX/7og;

    invoke-interface {v2}, LX/7of;->eY_()Ljava/lang/String;

    move-result-object v2

    .line 1277881
    iget-object v3, v1, LX/1nQ;->i:LX/0Zb;

    const-string v6, "event_ticket_order_action_link_clicked"

    const/4 p1, 0x0

    invoke-interface {v3, v6, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 1277882
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1277883
    const-string v6, "event_ticketing"

    invoke-virtual {v3, v6}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    iget-object v6, v1, LX/1nQ;->j:LX/0kv;

    iget-object p1, v1, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v6, p1}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v6, "EventTicketOrder"

    invoke-virtual {v3, v6}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3}, LX/0oG;->d()V

    .line 1277884
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1277885
    iget-object v2, p0, LX/7xp;->a:LX/7og;

    invoke-interface {v2}, LX/7og;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1277886
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1277887
    const-string v2, "force_in_app_browser"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1277888
    iget-object v2, p0, LX/7xp;->b:Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;

    iget-object v2, v2, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->l:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/7xp;->b:Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;

    invoke-virtual {v3}, Lcom/facebook/events/tickets/modal/views/EventTicketOrderDetailView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1277889
    const v1, -0x526cd7f2

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
