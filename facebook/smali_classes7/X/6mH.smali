.class public LX/6mH;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/6lq;


# instance fields
.field public a:LX/6mF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/6ll;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1143455
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1143456
    const-class v0, LX/6mH;

    invoke-static {v0, p0}, LX/6mH;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1143457
    iget-object v0, p0, LX/6mH;->a:LX/6mF;

    new-instance p1, LX/6mG;

    invoke-direct {p1, p0}, LX/6mG;-><init>(LX/6mH;)V

    .line 1143458
    iput-object p1, v0, LX/6mF;->b:LX/6m1;

    .line 1143459
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/6mH;

    invoke-static {p0}, LX/6mF;->b(LX/0QB;)LX/6mF;

    move-result-object p0

    check-cast p0, LX/6mF;

    iput-object p0, p1, LX/6mH;->a:LX/6mF;

    return-void
.end method


# virtual methods
.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1143460
    iget-object v0, p0, LX/6mH;->a:LX/6mF;

    invoke-virtual {v0, p1}, LX/6mF;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x475c2fb2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1143453
    iget-object v1, p0, LX/6mH;->a:LX/6mF;

    invoke-virtual {v1, p1}, LX/6mF;->b(Landroid/view/MotionEvent;)V

    .line 1143454
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, 0xe86c80f

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setXMACallback(LX/6ll;)V
    .locals 0
    .param p1    # LX/6ll;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1143451
    iput-object p1, p0, LX/6mH;->b:LX/6ll;

    .line 1143452
    return-void
.end method
