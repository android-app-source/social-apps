.class public final LX/7an;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

.field private b:LX/7ax;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    iput-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/model/CircleOptions;)LX/7c8;
    .locals 2

    :try_start_0
    new-instance v0, LX/7c8;

    iget-object v1, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    invoke-interface {v1, p1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(Lcom/google/android/gms/maps/model/CircleOptions;)LX/7cJ;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7c8;-><init>(LX/7cJ;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/maps/model/MarkerOptions;)LX/7cA;
    .locals 2

    :try_start_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)LX/7cS;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, LX/7cA;

    invoke-direct {v0, v1}, LX/7cA;-><init>(LX/7cS;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a()Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 2

    :try_start_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a()Lcom/google/android/gms/maps/model/CameraPosition;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(IIII)V
    .locals 2

    :try_start_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(IIII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(LX/6aP;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(LX/7aY;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    new-instance v1, LX/7aa;

    invoke-direct {v1, p0, p1}, LX/7aa;-><init>(LX/7an;LX/6aP;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(LX/7aY;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(LX/6aS;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(LX/7ab;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    new-instance v1, LX/7ad;

    invoke-direct {v1, p0, p1}, LX/7ad;-><init>(LX/7an;LX/6aS;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(LX/7ab;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(LX/6aV;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(LX/7ae;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    new-instance v1, LX/7ag;

    invoke-direct {v1, p0, p1}, LX/7ag;-><init>(LX/7an;LX/6aV;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(LX/7ae;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(LX/6ab;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(LX/7ah;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    new-instance v1, LX/7aj;

    invoke-direct {v1, p0, p1}, LX/7aj;-><init>(LX/7an;LX/6ab;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(LX/7ah;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(LX/6ae;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(LX/7aV;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    new-instance v1, LX/7aX;

    invoke-direct {v1, p0, p1}, LX/7aX;-><init>(LX/7an;LX/6ae;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(LX/7aV;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(LX/6ag;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(LX/7aS;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    new-instance v1, LX/7aU;

    invoke-direct {v1, p0, p1}, LX/7aU;-><init>(LX/7an;LX/6ag;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(LX/7aS;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(LX/7aQ;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    iget-object v1, p1, LX/7aQ;->a:LX/1ot;

    move-object v1, v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(LX/1ot;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(LX/7aQ;ILX/6aZ;)V
    .locals 3

    :try_start_0
    iget-object v1, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    iget-object v0, p1, LX/7aQ;->a:LX/1ot;

    move-object v2, v0

    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v2, p2, v0}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->a(LX/1ot;ILX/7ak;)V

    return-void

    :cond_0
    new-instance v0, LX/7am;

    invoke-direct {v0, p3}, LX/7am;-><init>(LX/6aZ;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Z)V
    .locals 2
    .annotation build Landroid/support/annotation/RequiresPermission;
    .end annotation

    :try_start_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->c(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final b()V
    .locals 2

    :try_start_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final c()Landroid/location/Location;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    :try_start_0
    iget-object v0, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->j()Landroid/location/Location;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final d()LX/7ax;
    .locals 2

    :try_start_0
    iget-object v0, p0, LX/7an;->b:LX/7ax;

    if-nez v0, :cond_0

    new-instance v0, LX/7ax;

    iget-object v1, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    invoke-interface {v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->k()Lcom/google/android/gms/maps/internal/IUiSettingsDelegate;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7ax;-><init>(Lcom/google/android/gms/maps/internal/IUiSettingsDelegate;)V

    iput-object v0, p0, LX/7an;->b:LX/7ax;

    :cond_0
    iget-object v0, p0, LX/7an;->b:LX/7ax;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final e()LX/7aw;
    .locals 2

    :try_start_0
    new-instance v0, LX/7aw;

    iget-object v1, p0, LX/7an;->a:Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;

    invoke-interface {v1}, Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;->l()Lcom/google/android/gms/maps/internal/IProjectionDelegate;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7aw;-><init>(Lcom/google/android/gms/maps/internal/IProjectionDelegate;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, LX/7cB;

    invoke-direct {v1, v0}, LX/7cB;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
