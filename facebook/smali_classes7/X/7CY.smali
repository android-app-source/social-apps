.class public LX/7CY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field private static volatile n:LX/7CY;


# instance fields
.field public final d:Landroid/hardware/SensorManager;

.field public final e:LX/0Sh;

.field public final f:Landroid/os/Handler;

.field public final g:Landroid/hardware/Sensor;

.field public final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/sensor/ProximitySensor$ProximitySensorListener;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/lang/Runnable;

.field public j:LX/7CX;

.field public k:Z

.field public l:F

.field public m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1181039
    const-class v0, LX/7CY;

    sput-object v0, LX/7CY;->a:Ljava/lang/Class;

    .line 1181040
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/7CY;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_missing_sensor"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7CY;->b:Ljava/lang/String;

    .line 1181041
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/7CY;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_multiple_sensors"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7CY;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Landroid/hardware/SensorManager;LX/0Sh;LX/03V;)V
    .locals 2
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1181042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1181043
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/7CY;->h:Ljava/util/Set;

    .line 1181044
    new-instance v0, Lcom/facebook/sensor/ProximitySensor$RunnableReportFirstEvent;

    invoke-direct {v0, p0}, Lcom/facebook/sensor/ProximitySensor$RunnableReportFirstEvent;-><init>(LX/7CY;)V

    iput-object v0, p0, LX/7CY;->i:Ljava/lang/Runnable;

    .line 1181045
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, LX/7CY;->f:Landroid/os/Handler;

    .line 1181046
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, LX/7CY;->d:Landroid/hardware/SensorManager;

    .line 1181047
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iput-object v0, p0, LX/7CY;->e:LX/0Sh;

    .line 1181048
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1181049
    iget-object v0, p0, LX/7CY;->d:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, LX/7CY;->g:Landroid/hardware/Sensor;

    .line 1181050
    const/16 p0, 0x8

    .line 1181051
    invoke-virtual {p2, p0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1181052
    sget-object v0, LX/7CY;->b:Ljava/lang/String;

    invoke-static {p2}, LX/7CY;->a(Landroid/hardware/SensorManager;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1181053
    :cond_0
    invoke-virtual {p2, p0}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 1181054
    sget-object v0, LX/7CY;->c:Ljava/lang/String;

    invoke-static {p2}, LX/7CY;->a(Landroid/hardware/SensorManager;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1181055
    :cond_1
    return-void
.end method

.method public static a(LX/0QB;)LX/7CY;
    .locals 7

    .prologue
    .line 1181056
    sget-object v0, LX/7CY;->n:LX/7CY;

    if-nez v0, :cond_1

    .line 1181057
    const-class v1, LX/7CY;

    monitor-enter v1

    .line 1181058
    :try_start_0
    sget-object v0, LX/7CY;->n:LX/7CY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1181059
    if-eqz v2, :cond_0

    .line 1181060
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1181061
    new-instance p0, LX/7CY;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-static {v0}, LX/2Fq;->b(LX/0QB;)Landroid/hardware/SensorManager;

    move-result-object v4

    check-cast v4, Landroid/hardware/SensorManager;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct {p0, v3, v4, v5, v6}, LX/7CY;-><init>(Landroid/os/Handler;Landroid/hardware/SensorManager;LX/0Sh;LX/03V;)V

    .line 1181062
    move-object v0, p0

    .line 1181063
    sput-object v0, LX/7CY;->n:LX/7CY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1181064
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1181065
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1181066
    :cond_1
    sget-object v0, LX/7CY;->n:LX/7CY;

    return-object v0

    .line 1181067
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1181068
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/hardware/SensorManager;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1181069
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1181070
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Sensor;

    .line 1181071
    const-string v3, "sensor "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " vendor="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " maxRange="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " power="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getPower()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " minDelay="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getMinDelay()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1181072
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/7CY;)Z
    .locals 3

    .prologue
    .line 1181073
    iget v0, p0, LX/7CY;->l:F

    const/high16 v1, 0x40a00000    # 5.0f

    iget-object v2, p0, LX/7CY;->g:Landroid/hardware/Sensor;

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/7CY;)V
    .locals 6

    .prologue
    .line 1181074
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7CY;->k:Z

    .line 1181075
    invoke-static {p0}, LX/7CY;->b(LX/7CY;)Z

    move-result v2

    .line 1181076
    iget-object v0, p0, LX/7CY;->h:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ivd;

    .line 1181077
    iget-object v5, p0, LX/7CY;->h:Ljava/util/Set;

    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1181078
    invoke-virtual {v0, v2}, LX/Ivd;->a(Z)V

    .line 1181079
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1181080
    :cond_1
    return-void
.end method


# virtual methods
.method public final b(LX/Ivd;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1181081
    iget-object v1, p0, LX/7CY;->e:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 1181082
    iget-object v1, p0, LX/7CY;->h:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/7CY;->h:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 1181083
    iput-boolean v0, p0, LX/7CY;->k:Z

    .line 1181084
    iget-object v1, p0, LX/7CY;->f:Landroid/os/Handler;

    iget-object v2, p0, LX/7CY;->i:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1181085
    iget-object v1, p0, LX/7CY;->j:LX/7CX;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1181086
    iget-object v0, p0, LX/7CY;->d:Landroid/hardware/SensorManager;

    iget-object v1, p0, LX/7CY;->j:LX/7CX;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 1181087
    iget-object v0, p0, LX/7CY;->j:LX/7CX;

    invoke-static {v0}, LX/7CX;->a$redex0(LX/7CX;)V

    .line 1181088
    const/4 v0, 0x0

    iput-object v0, p0, LX/7CY;->j:LX/7CX;

    .line 1181089
    :cond_1
    return-void
.end method
