.class public LX/7vP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1274531
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274532
    iput-object p1, p0, LX/7vP;->a:LX/0tX;

    .line 1274533
    return-void
.end method

.method public static b(LX/0QB;)LX/7vP;
    .locals 2

    .prologue
    .line 1274534
    new-instance v1, LX/7vP;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-direct {v1, v0}, LX/7vP;-><init>(LX/0tX;)V

    .line 1274535
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsMutationsModels$CancelEventMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1274536
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    .line 1274537
    iget-object v1, p2, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1274538
    new-instance v1, LX/4EG;

    invoke-direct {v1}, LX/4EG;-><init>()V

    .line 1274539
    iget-object v2, p2, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1274540
    invoke-virtual {v1, p3}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1274541
    new-instance v2, LX/4EL;

    invoke-direct {v2}, LX/4EL;-><init>()V

    .line 1274542
    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 1274543
    new-instance v0, LX/4EJ;

    invoke-direct {v0}, LX/4EJ;-><init>()V

    .line 1274544
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274545
    move-object v0, v0

    .line 1274546
    const-string v1, "context"

    invoke-virtual {v0, v1, v2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1274547
    move-object v0, v0

    .line 1274548
    new-instance v1, LX/7uA;

    invoke-direct {v1}, LX/7uA;-><init>()V

    move-object v1, v1

    .line 1274549
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/7uA;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1274550
    iget-object v1, p0, LX/7vP;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
