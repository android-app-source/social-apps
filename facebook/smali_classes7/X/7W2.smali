.class public final LX/7W2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Landroid/widget/TextView;

.field public final synthetic b:Landroid/widget/TextView;

.field public final synthetic c:Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 1215721
    iput-object p1, p0, LX/7W2;->c:Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;

    iput-object p2, p0, LX/7W2;->a:Landroid/widget/TextView;

    iput-object p3, p0, LX/7W2;->b:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    .line 1215722
    iget-object v0, p0, LX/7W2;->c:Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->c:LX/0Px;

    if-nez v0, :cond_0

    .line 1215723
    :goto_0
    return-void

    .line 1215724
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1215725
    iget-object v0, p0, LX/7W2;->c:Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    iget-object v0, p0, LX/7W2;->c:Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;

    iget-object v0, v0, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;

    .line 1215726
    invoke-virtual {v0, v2}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1215727
    iget-object v1, p0, LX/7W2;->a:Landroid/widget/TextView;

    const/high16 v3, -0x10000

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1215728
    iget-object v1, p0, LX/7W2;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215729
    iget-object v1, p0, LX/7W2;->c:Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;

    iget-object v2, p0, LX/7W2;->b:Landroid/widget/TextView;

    iget-object v3, p0, LX/7W2;->c:Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;

    iget-object v3, v3, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->c:LX/0Px;

    .line 1215730
    invoke-static {v1, v2, v0, v3}, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a$redex0(Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;Landroid/widget/TextView;Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;LX/0Px;)V

    .line 1215731
    goto :goto_0

    .line 1215732
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1215733
    :cond_2
    iget-object v0, p0, LX/7W2;->a:Landroid/widget/TextView;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1215734
    iget-object v0, p0, LX/7W2;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215735
    iget-object v0, p0, LX/7W2;->c:Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;

    iget-object v1, p0, LX/7W2;->b:Landroid/widget/TextView;

    const/4 v2, 0x0

    iget-object v3, p0, LX/7W2;->c:Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;

    iget-object v3, v3, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->c:LX/0Px;

    .line 1215736
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;->a$redex0(Lcom/facebook/zero/activity/ZeroInternStatusActivity$TokenFragment;Landroid/widget/TextView;Lcom/facebook/zero/sdk/rewrite/ZeroUrlRewriteRule;LX/0Px;)V

    .line 1215737
    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1215738
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1215739
    return-void
.end method
