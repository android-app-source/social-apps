.class public final LX/7O8;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/7Lw;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7N3;


# direct methods
.method public constructor <init>(LX/7N3;)V
    .locals 0

    .prologue
    .line 1201169
    iput-object p1, p0, LX/7O8;->a:LX/7N3;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/7Lw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1201170
    const-class v0, LX/7Lw;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 1201171
    check-cast p1, LX/7Lw;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1201172
    iget-object v0, p0, LX/7O8;->a:LX/7N3;

    iget-object v1, p1, LX/7Lw;->a:LX/7MP;

    iput-object v1, v0, LX/7N3;->d:LX/7MP;

    .line 1201173
    iget-object v0, p0, LX/7O8;->a:LX/7N3;

    iget-object v0, v0, LX/7N3;->d:LX/7MP;

    sget-object v1, LX/7MP;->AUTO:LX/7MP;

    if-ne v0, v1, :cond_2

    .line 1201174
    iget-object v0, p0, LX/7O8;->a:LX/7N3;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/7O8;->a:LX/7N3;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    .line 1201175
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1201176
    :goto_0
    iget-object v1, p0, LX/7O8;->a:LX/7N3;

    iget-boolean v1, v1, LX/7N3;->c:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1201177
    iget-object v0, p0, LX/7O8;->a:LX/7N3;

    iget-object v0, v0, LX/7N3;->q:LX/7O7;

    const/4 v1, 0x2

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, LX/7O7;->sendEmptyMessageDelayed(IJ)Z

    .line 1201178
    :cond_0
    :goto_1
    return-void

    .line 1201179
    :cond_1
    sget-object v0, LX/2qV;->UNPREPARED:LX/2qV;

    goto :goto_0

    .line 1201180
    :cond_2
    iget-object v0, p0, LX/7O8;->a:LX/7N3;

    iget-object v0, v0, LX/7N3;->d:LX/7MP;

    sget-object v1, LX/7MP;->ALWAYS_HIDDEN:LX/7MP;

    if-ne v0, v1, :cond_3

    .line 1201181
    iget-object v0, p0, LX/7O8;->a:LX/7N3;

    iget-object v0, v0, LX/7N3;->q:LX/7O7;

    invoke-virtual {v0, v3}, LX/7O7;->removeMessages(I)V

    .line 1201182
    iget-object v0, p0, LX/7O8;->a:LX/7N3;

    invoke-virtual {v0, v2}, LX/7N3;->c(I)V

    goto :goto_1

    .line 1201183
    :cond_3
    iget-object v0, p0, LX/7O8;->a:LX/7N3;

    iget-object v0, v0, LX/7N3;->d:LX/7MP;

    sget-object v1, LX/7MP;->ALWAYS_VISIBLE:LX/7MP;

    if-ne v0, v1, :cond_0

    .line 1201184
    iget-object v0, p0, LX/7O8;->a:LX/7N3;

    iget-object v0, v0, LX/7N3;->q:LX/7O7;

    invoke-virtual {v0, v3}, LX/7O7;->removeMessages(I)V

    .line 1201185
    iget-object v0, p0, LX/7O8;->a:LX/7N3;

    invoke-virtual {v0, v2}, LX/7N3;->d(I)V

    goto :goto_1
.end method
