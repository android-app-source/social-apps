.class public final enum LX/7B5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7B5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7B5;

.field public static final enum PILL:LX/7B5;

.field public static final enum SCOPED_ONLY:LX/7B5;

.field public static final enum SINGLE_STATE:LX/7B5;

.field public static final enum TAB:LX/7B5;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1177746
    new-instance v0, LX/7B5;

    const-string v1, "SINGLE_STATE"

    invoke-direct {v0, v1, v2}, LX/7B5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7B5;->SINGLE_STATE:LX/7B5;

    .line 1177747
    new-instance v0, LX/7B5;

    const-string v1, "TAB"

    invoke-direct {v0, v1, v3}, LX/7B5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7B5;->TAB:LX/7B5;

    .line 1177748
    new-instance v0, LX/7B5;

    const-string v1, "PILL"

    invoke-direct {v0, v1, v4}, LX/7B5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7B5;->PILL:LX/7B5;

    .line 1177749
    new-instance v0, LX/7B5;

    const-string v1, "SCOPED_ONLY"

    invoke-direct {v0, v1, v5}, LX/7B5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7B5;->SCOPED_ONLY:LX/7B5;

    .line 1177750
    const/4 v0, 0x4

    new-array v0, v0, [LX/7B5;

    sget-object v1, LX/7B5;->SINGLE_STATE:LX/7B5;

    aput-object v1, v0, v2

    sget-object v1, LX/7B5;->TAB:LX/7B5;

    aput-object v1, v0, v3

    sget-object v1, LX/7B5;->PILL:LX/7B5;

    aput-object v1, v0, v4

    sget-object v1, LX/7B5;->SCOPED_ONLY:LX/7B5;

    aput-object v1, v0, v5

    sput-object v0, LX/7B5;->$VALUES:[LX/7B5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1177751
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7B5;
    .locals 1

    .prologue
    .line 1177752
    const-class v0, LX/7B5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7B5;

    return-object v0
.end method

.method public static values()[LX/7B5;
    .locals 1

    .prologue
    .line 1177753
    sget-object v0, LX/7B5;->$VALUES:[LX/7B5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7B5;

    return-object v0
.end method
