.class public final LX/6v5;
.super LX/6qh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V
    .locals 0

    .prologue
    .line 1156475
    iput-object p1, p0, LX/6v5;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    invoke-direct {p0}, LX/6qh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/73T;)V
    .locals 3

    .prologue
    .line 1156476
    iget-object v0, p0, LX/6v5;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    const/4 p0, -0x1

    .line 1156477
    sget-object v1, LX/6vB;->b:[I

    .line 1156478
    iget-object v2, p1, LX/73T;->a:LX/73S;

    move-object v2, v2

    .line 1156479
    invoke-virtual {v2}, LX/73S;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1156480
    :cond_0
    :goto_0
    return-void

    .line 1156481
    :pswitch_0
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    .line 1156482
    if-eqz v2, :cond_0

    .line 1156483
    const-string v1, "extra_activity_result_data"

    invoke-virtual {p1, v1}, LX/73T;->a(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 1156484
    if-eqz v1, :cond_1

    .line 1156485
    invoke-virtual {v2, p0, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1156486
    :goto_1
    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 1156487
    :cond_1
    invoke-virtual {v2, p0}, Landroid/app/Activity;->setResult(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V
    .locals 2

    .prologue
    .line 1156488
    iget-object v0, p0, LX/6v5;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "payments_component_dialog_fragment"

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1156489
    return-void
.end method
