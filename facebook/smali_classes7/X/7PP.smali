.class public final LX/7PP;
.super Ljava/io/OutputStream;
.source ""


# instance fields
.field public final synthetic a:LX/1Lu;

.field private final b:Ljava/io/OutputStream;

.field private final c:Ljava/lang/Thread;

.field private final d:Ljava/lang/Object;

.field private e:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private f:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Lu;Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 1202846
    iput-object p1, p0, LX/7PP;->a:LX/1Lu;

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 1202847
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/7PP;->d:Ljava/lang/Object;

    .line 1202848
    iput-object p2, p0, LX/7PP;->b:Ljava/io/OutputStream;

    .line 1202849
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, LX/7PP;->c:Ljava/lang/Thread;

    .line 1202850
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1202815
    iget-object v1, p0, LX/7PP;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1202816
    :try_start_0
    iget-object v0, p0, LX/7PP;->a:LX/1Lu;

    iget-object v0, v0, LX/1Lu;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/7PP;->f:J

    .line 1202817
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7PP;->e:Z

    .line 1202818
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1202819
    iget-object v1, p0, LX/7PP;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1202820
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/7PP;->e:Z

    .line 1202821
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(J)V
    .locals 7

    .prologue
    .line 1202822
    iget-object v1, p0, LX/7PP;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1202823
    :try_start_0
    iget-object v0, p0, LX/7PP;->c:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1202824
    iget-boolean v0, p0, LX/7PP;->e:Z

    if-nez v0, :cond_1

    .line 1202825
    monitor-exit v1

    .line 1202826
    :goto_1
    return-void

    .line 1202827
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1202828
    :cond_1
    iget-object v0, p0, LX/7PP;->a:LX/1Lu;

    iget-object v0, v0, LX/1Lu;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/7PP;->f:J

    sub-long/2addr v2, v4

    cmp-long v0, v2, p1

    if-lez v0, :cond_2

    .line 1202829
    iget-object v0, p0, LX/7PP;->c:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 1202830
    :cond_2
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final write(I)V
    .locals 1

    .prologue
    .line 1202831
    :try_start_0
    invoke-direct {p0}, LX/7PP;->a()V

    .line 1202832
    iget-object v0, p0, LX/7PP;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1202833
    invoke-direct {p0}, LX/7PP;->b()V

    .line 1202834
    return-void

    .line 1202835
    :catchall_0
    move-exception v0

    invoke-direct {p0}, LX/7PP;->b()V

    throw v0
.end method

.method public final write([B)V
    .locals 1

    .prologue
    .line 1202836
    :try_start_0
    invoke-direct {p0}, LX/7PP;->a()V

    .line 1202837
    iget-object v0, p0, LX/7PP;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1202838
    invoke-direct {p0}, LX/7PP;->b()V

    .line 1202839
    return-void

    .line 1202840
    :catchall_0
    move-exception v0

    invoke-direct {p0}, LX/7PP;->b()V

    throw v0
.end method

.method public final write([BII)V
    .locals 1

    .prologue
    .line 1202841
    :try_start_0
    invoke-direct {p0}, LX/7PP;->a()V

    .line 1202842
    iget-object v0, p0, LX/7PP;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1202843
    invoke-direct {p0}, LX/7PP;->b()V

    .line 1202844
    return-void

    .line 1202845
    :catchall_0
    move-exception v0

    invoke-direct {p0}, LX/7PP;->b()V

    throw v0
.end method
