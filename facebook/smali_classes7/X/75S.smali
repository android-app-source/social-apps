.class public LX/75S;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Z0;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/ContentResolver;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:LX/75Q;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/75Q;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/6Z0;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/content/ContentResolver;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/75Q;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1169610
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169611
    iput-object v0, p0, LX/75S;->d:Ljava/util/List;

    .line 1169612
    iput-object v0, p0, LX/75S;->e:Ljava/util/Map;

    .line 1169613
    iput-object p1, p0, LX/75S;->a:LX/0Ot;

    .line 1169614
    iput-object p2, p0, LX/75S;->b:LX/0Ot;

    .line 1169615
    iput-object p3, p0, LX/75S;->c:LX/0Ot;

    .line 1169616
    iput-object p4, p0, LX/75S;->f:LX/75Q;

    .line 1169617
    return-void
.end method

.method public static a(LX/0QB;)LX/75S;
    .locals 7

    .prologue
    .line 1169599
    const-class v1, LX/75S;

    monitor-enter v1

    .line 1169600
    :try_start_0
    sget-object v0, LX/75S;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1169601
    sput-object v2, LX/75S;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1169602
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1169603
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1169604
    new-instance v4, LX/75S;

    const/16 v3, 0x2587

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x13

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x2e3

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/75Q;->a(LX/0QB;)LX/75Q;

    move-result-object v3

    check-cast v3, LX/75Q;

    invoke-direct {v4, v5, v6, p0, v3}, LX/75S;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/75Q;)V

    .line 1169605
    move-object v0, v4

    .line 1169606
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1169607
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/75S;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1169608
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1169609
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/75S;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1169523
    iget-object v0, p0, LX/75S;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1169524
    iget-object v0, p0, LX/75S;->d:Ljava/util/List;

    .line 1169525
    :goto_0
    return-object v0

    .line 1169526
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1169527
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1169528
    const-string v0, "tag_prefill_completed"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1169529
    const-string v0, " = 1"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1169530
    iget-object v0, p0, LX/75S;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iget-object v1, p0, LX/75S;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Z0;

    iget-object v1, v1, LX/6Z0;->e:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "photo_hash"

    aput-object v7, v2, v5

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1169531
    if-eqz v1, :cond_3

    .line 1169532
    :try_start_0
    sget v0, LX/6Yx;->a:I

    .line 1169533
    :cond_1
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1169534
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1169535
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1169536
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1169537
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1169538
    :cond_3
    iput-object v6, p0, LX/75S;->d:Ljava/util/List;

    move-object v0, v6

    .line 1169539
    goto :goto_0
.end method

.method public static e(LX/75S;)Ljava/util/Map;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1169573
    iget-object v0, p0, LX/75S;->e:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 1169574
    iget-object v0, p0, LX/75S;->e:Ljava/util/Map;

    .line 1169575
    :goto_0
    return-object v0

    .line 1169576
    :cond_0
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 1169577
    iget-object v0, p0, LX/75S;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iget-object v1, p0, LX/75S;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Z0;

    iget-object v1, v1, LX/6Z0;->d:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1169578
    if-eqz v9, :cond_4

    .line 1169579
    :goto_1
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1169580
    const/16 v0, 0xa

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1169581
    invoke-interface {v8, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1169582
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1169583
    :cond_1
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1169584
    new-instance v3, Landroid/graphics/RectF;

    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    const/4 v2, 0x2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    const/4 v6, 0x3

    invoke-interface {v9, v6}, Landroid/database/Cursor;->getFloat(I)F

    move-result v6

    const/4 v7, 0x4

    invoke-interface {v9, v7}, Landroid/database/Cursor;->getFloat(I)F

    move-result v7

    invoke-direct {v3, v1, v2, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1169585
    const/4 v1, 0x5

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 1169586
    invoke-static {}, LX/7Gr;->values()[LX/7Gr;

    move-result-object v2

    aget-object v6, v2, v1

    .line 1169587
    const/4 v1, 0x6

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    const/4 v7, 0x1

    .line 1169588
    :goto_2
    const/4 v1, 0x7

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 1169589
    const/16 v1, 0x8

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1169590
    const/16 v1, 0x9

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1169591
    new-instance v1, Lcom/facebook/photos/base/tagging/Tag;

    new-instance v2, Lcom/facebook/photos/base/tagging/FaceBox;

    invoke-direct {v2, v3}, Lcom/facebook/photos/base/tagging/FaceBox;-><init>(Landroid/graphics/RectF;)V

    new-instance v3, Lcom/facebook/user/model/Name;

    const/4 v14, 0x0

    invoke-direct {v3, v13, v14, v12}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct/range {v1 .. v7}, Lcom/facebook/photos/base/tagging/Tag;-><init>(Lcom/facebook/photos/base/tagging/TagTarget;Lcom/facebook/user/model/Name;JLX/7Gr;Z)V

    .line 1169592
    invoke-virtual {v1, v10, v11}, Lcom/facebook/photos/base/tagging/Tag;->a(J)V

    .line 1169593
    invoke-interface {v8, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1169594
    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1169595
    :cond_2
    const/4 v7, 0x0

    goto :goto_2

    .line 1169596
    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1169597
    :cond_4
    iput-object v8, p0, LX/75S;->e:Ljava/util/Map;

    move-object v0, v8

    .line 1169598
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/base/media/PhotoItem;)V
    .locals 6

    .prologue
    .line 1169545
    iget-object v0, p0, LX/75S;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/75S;->e:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 1169546
    :cond_0
    :goto_0
    return-void

    .line 1169547
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->j()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/75E;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 1169548
    invoke-static {v0, p1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    .line 1169549
    invoke-static {p0}, LX/75S;->d(LX/75S;)Ljava/util/List;

    move-result-object v1

    .line 1169550
    if-eqz v1, :cond_3

    .line 1169551
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1169552
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/media/MediaItem;

    .line 1169553
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->l()LX/4gF;

    move-result-object v2

    sget-object v4, LX/4gF;->PHOTO:LX/4gF;

    if-ne v2, v4, :cond_2

    move-object v2, v1

    check-cast v2, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1169554
    iget-object v4, v2, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v2, v4

    .line 1169555
    instance-of v2, v2, Lcom/facebook/photos/base/photos/LocalPhoto;

    if-eqz v2, :cond_2

    .line 1169556
    check-cast v1, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1169557
    iget-object v2, v1, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v1, v2

    .line 1169558
    check-cast v1, Lcom/facebook/photos/base/photos/LocalPhoto;

    const/4 v2, 0x1

    .line 1169559
    iput-boolean v2, v1, Lcom/facebook/photos/base/photos/LocalPhoto;->f:Z

    .line 1169560
    goto :goto_1

    .line 1169561
    :cond_3
    invoke-static {p0}, LX/75S;->e(LX/75S;)Ljava/util/Map;

    move-result-object v3

    .line 1169562
    if-nez v3, :cond_4

    .line 1169563
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    .line 1169564
    :goto_2
    goto :goto_0

    .line 1169565
    :cond_4
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1169566
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/media/MediaItem;

    .line 1169567
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->l()LX/4gF;

    move-result-object v5

    sget-object p1, LX/4gF;->PHOTO:LX/4gF;

    if-ne v5, p1, :cond_5

    .line 1169568
    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 1169569
    iget-object v5, p0, LX/75S;->f:LX/75Q;

    check-cast v2, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1169570
    iget-object p1, v2, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v2, p1

    .line 1169571
    check-cast v2, LX/74x;

    invoke-virtual {v5, v2, v1}, LX/75Q;->a(LX/74x;Ljava/util/List;)V

    goto :goto_3

    .line 1169572
    :cond_6
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_2
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1169540
    iget-object v0, p0, LX/75S;->d:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1169541
    invoke-static {p0}, LX/75S;->d(LX/75S;)Ljava/util/List;

    .line 1169542
    :cond_0
    iget-object v0, p0, LX/75S;->e:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 1169543
    invoke-static {p0}, LX/75S;->e(LX/75S;)Ljava/util/Map;

    .line 1169544
    :cond_1
    return-void
.end method
