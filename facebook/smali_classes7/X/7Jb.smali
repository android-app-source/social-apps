.class public final enum LX/7Jb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Jb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Jb;

.field public static final enum CACHE_EVICTION:LX/7Jb;

.field public static final enum FEATURE_DISABLED:LX/7Jb;

.field public static final enum NOT_SAVABLE_OFFLINE:LX/7Jb;

.field public static final enum NOT_VIEWABLE:LX/7Jb;

.field public static final enum SAVE_STATE_CHANGED:LX/7Jb;

.field public static final enum USER_ARCHIVED:LX/7Jb;

.field public static final enum USER_INITIATED:LX/7Jb;

.field public static final enum USER_LOGGED_OUT:LX/7Jb;

.field public static final enum VIDEO_EXPIRED:LX/7Jb;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1193935
    new-instance v0, LX/7Jb;

    const-string v1, "USER_INITIATED"

    const-string v2, "user_initiated"

    invoke-direct {v0, v1, v4, v2}, LX/7Jb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jb;->USER_INITIATED:LX/7Jb;

    .line 1193936
    new-instance v0, LX/7Jb;

    const-string v1, "USER_ARCHIVED"

    const-string v2, "user_archived"

    invoke-direct {v0, v1, v5, v2}, LX/7Jb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jb;->USER_ARCHIVED:LX/7Jb;

    .line 1193937
    new-instance v0, LX/7Jb;

    const-string v1, "NOT_VIEWABLE"

    const-string v2, "not_viewable"

    invoke-direct {v0, v1, v6, v2}, LX/7Jb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jb;->NOT_VIEWABLE:LX/7Jb;

    .line 1193938
    new-instance v0, LX/7Jb;

    const-string v1, "SAVE_STATE_CHANGED"

    const-string v2, "save_state_changed"

    invoke-direct {v0, v1, v7, v2}, LX/7Jb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jb;->SAVE_STATE_CHANGED:LX/7Jb;

    .line 1193939
    new-instance v0, LX/7Jb;

    const-string v1, "NOT_SAVABLE_OFFLINE"

    const-string v2, "not_savable_offline"

    invoke-direct {v0, v1, v8, v2}, LX/7Jb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jb;->NOT_SAVABLE_OFFLINE:LX/7Jb;

    .line 1193940
    new-instance v0, LX/7Jb;

    const-string v1, "VIDEO_EXPIRED"

    const/4 v2, 0x5

    const-string v3, "video_expired"

    invoke-direct {v0, v1, v2, v3}, LX/7Jb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jb;->VIDEO_EXPIRED:LX/7Jb;

    .line 1193941
    new-instance v0, LX/7Jb;

    const-string v1, "FEATURE_DISABLED"

    const/4 v2, 0x6

    const-string v3, "feature_disabled"

    invoke-direct {v0, v1, v2, v3}, LX/7Jb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jb;->FEATURE_DISABLED:LX/7Jb;

    .line 1193942
    new-instance v0, LX/7Jb;

    const-string v1, "USER_LOGGED_OUT"

    const/4 v2, 0x7

    const-string v3, "user_logged_out"

    invoke-direct {v0, v1, v2, v3}, LX/7Jb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jb;->USER_LOGGED_OUT:LX/7Jb;

    .line 1193943
    new-instance v0, LX/7Jb;

    const-string v1, "CACHE_EVICTION"

    const/16 v2, 0x8

    const-string v3, "cache_eviction"

    invoke-direct {v0, v1, v2, v3}, LX/7Jb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7Jb;->CACHE_EVICTION:LX/7Jb;

    .line 1193944
    const/16 v0, 0x9

    new-array v0, v0, [LX/7Jb;

    sget-object v1, LX/7Jb;->USER_INITIATED:LX/7Jb;

    aput-object v1, v0, v4

    sget-object v1, LX/7Jb;->USER_ARCHIVED:LX/7Jb;

    aput-object v1, v0, v5

    sget-object v1, LX/7Jb;->NOT_VIEWABLE:LX/7Jb;

    aput-object v1, v0, v6

    sget-object v1, LX/7Jb;->SAVE_STATE_CHANGED:LX/7Jb;

    aput-object v1, v0, v7

    sget-object v1, LX/7Jb;->NOT_SAVABLE_OFFLINE:LX/7Jb;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7Jb;->VIDEO_EXPIRED:LX/7Jb;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7Jb;->FEATURE_DISABLED:LX/7Jb;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7Jb;->USER_LOGGED_OUT:LX/7Jb;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/7Jb;->CACHE_EVICTION:LX/7Jb;

    aput-object v2, v0, v1

    sput-object v0, LX/7Jb;->$VALUES:[LX/7Jb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1193946
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1193947
    iput-object p3, p0, LX/7Jb;->value:Ljava/lang/String;

    .line 1193948
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Jb;
    .locals 1

    .prologue
    .line 1193949
    const-class v0, LX/7Jb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Jb;

    return-object v0
.end method

.method public static values()[LX/7Jb;
    .locals 1

    .prologue
    .line 1193945
    sget-object v0, LX/7Jb;->$VALUES:[LX/7Jb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Jb;

    return-object v0
.end method
