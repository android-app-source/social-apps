.class public final LX/7JZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/7Jg;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/19w;


# direct methods
.method public constructor <init>(LX/19w;)V
    .locals 0

    .prologue
    .line 1193927
    iput-object p1, p0, LX/7JZ;->a:LX/19w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 6

    .prologue
    .line 1193928
    check-cast p1, LX/7Jg;

    check-cast p2, LX/7Jg;

    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 1193929
    iget-object v2, p1, LX/7Jg;->l:LX/1A0;

    sget-object v3, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-ne v2, v3, :cond_1

    iget-object v2, p2, LX/7Jg;->l:LX/1A0;

    sget-object v3, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-eq v2, v3, :cond_1

    .line 1193930
    :cond_0
    :goto_0
    return v0

    .line 1193931
    :cond_1
    iget-object v2, p1, LX/7Jg;->l:LX/1A0;

    sget-object v3, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-eq v2, v3, :cond_2

    iget-object v2, p2, LX/7Jg;->l:LX/1A0;

    sget-object v3, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-ne v2, v3, :cond_2

    move v0, v1

    .line 1193932
    goto :goto_0

    .line 1193933
    :cond_2
    iget-wide v2, p1, LX/7Jg;->p:J

    iget-wide v4, p2, LX/7Jg;->p:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    iget-wide v2, p1, LX/7Jg;->p:J

    iget-wide v4, p2, LX/7Jg;->p:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
