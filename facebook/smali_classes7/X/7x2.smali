.class public final LX/7x2;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7x3;


# direct methods
.method public constructor <init>(LX/7x3;)V
    .locals 0

    .prologue
    .line 1276911
    iput-object p1, p0, LX/7x2;->a:LX/7x3;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1276912
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1276913
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    .line 1276914
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1276915
    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 1276916
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null GraphQL result"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/7x2;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1276917
    :goto_1
    return-void

    .line 1276918
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1276919
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1276920
    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1276921
    :cond_2
    iget-object v0, p0, LX/7x2;->a:LX/7x3;

    iget-object v0, v0, LX/7x3;->c:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 1276922
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1276923
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$EventTicketProviderModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1276924
    iget-object v0, p0, LX/7x2;->a:LX/7x3;

    iget-object v1, v0, LX/7x3;->a:Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;

    .line 1276925
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1276926
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel;->k()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$EventTicketProviderModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$EventTicketProviderModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    .line 1276927
    const/4 v5, 0x0

    .line 1276928
    if-eqz v0, :cond_3

    .line 1276929
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030528

    iget-object v4, v1, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1276930
    iget-object v3, v1, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->j:LX/7x9;

    invoke-virtual {v3, v2, v0}, LX/7x9;->a(Landroid/view/View;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)V

    .line 1276931
    iget-object v3, v1, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->h:LX/1ON;

    const/4 v4, 0x1

    new-array v4, v4, [Landroid/view/View;

    aput-object v2, v4, v5

    invoke-static {v4}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/1ON;->b(Ljava/util/ArrayList;)V

    .line 1276932
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1276933
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel;->l()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v2, p0, LX/7x2;->a:LX/7x3;

    const/4 v5, 0x1

    .line 1276934
    const/4 v3, 0x0

    const-class v4, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel;

    invoke-virtual {v1, v0, v3, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    move-object v4, v3

    .line 1276935
    :goto_2
    const-class v3, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {v1, v0, v5, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    if-eqz v3, :cond_7

    const-class v3, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {v1, v0, v5, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    :goto_3
    iput-object v3, v2, LX/7x3;->c:Ljava/lang/String;

    .line 1276936
    iget-object v3, v2, LX/7x3;->c:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 1276937
    iput-boolean v5, v2, LX/7x3;->b:Z

    .line 1276938
    :cond_4
    iget-object v3, v2, LX/7x3;->a:Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;

    const/4 v0, 0x0

    .line 1276939
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1276940
    :cond_5
    :goto_4
    goto/16 :goto_1

    .line 1276941
    :cond_6
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 1276942
    move-object v4, v3

    goto :goto_2

    .line 1276943
    :cond_7
    const/4 v3, 0x0

    goto :goto_3

    .line 1276944
    :cond_8
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    sget-object v2, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->a:LX/0QK;

    invoke-static {v4, v2}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v5

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 1276945
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v2

    const/4 v1, 0x1

    if-ne v2, v1, :cond_9

    .line 1276946
    invoke-virtual {v5, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;

    invoke-virtual {v5}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrdersQueryModel$PurchasedTicketOrdersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 1276947
    const-class v5, LX/7wM;

    invoke-virtual {v3, v5}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/7wM;

    .line 1276948
    if-eqz v5, :cond_5

    .line 1276949
    invoke-interface {v5, v2}, LX/7wM;->c(Ljava/lang/String;)V

    goto :goto_4

    .line 1276950
    :cond_9
    iget-object v2, v3, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1276951
    iget-object v2, v3, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 1276952
    iget-object v2, v3, Lcom/facebook/events/tickets/modal/fragments/EventTicketsOrdersListFragment;->g:LX/7wI;

    .line 1276953
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1276954
    :goto_5
    goto :goto_4

    .line 1276955
    :cond_a
    iget-object v1, v2, LX/7wI;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 1276956
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iget-object v3, v2, LX/7wI;->d:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/7wI;->d:LX/0Px;

    .line 1276957
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {v2, v1, v0}, LX/1OM;->c(II)V

    goto :goto_5
.end method
