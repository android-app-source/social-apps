.class public LX/8CZ;
.super LX/1OM;
.source ""


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/8Cc;

.field public c:LX/8CX;

.field public d:LX/0Px;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1311057
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1311058
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8CZ;->a:Ljava/util/Map;

    .line 1311059
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1311060
    iput-object v0, p0, LX/8CZ;->d:LX/0Px;

    .line 1311061
    return-void
.end method

.method public static d(LX/8CZ;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1311051
    iget-object v1, p0, LX/8CZ;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 1311052
    iget-object v1, p0, LX/8CZ;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, LX/8CZ;->d:LX/0Px;

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 1311053
    iget-object v4, p0, LX/8CZ;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1311054
    add-int/lit8 v1, v1, 0x1

    .line 1311055
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1311056
    :cond_0
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 1311048
    iget-object v0, p0, LX/8CZ;->c:LX/8CX;

    if-nez v0, :cond_0

    .line 1311049
    const-wide/16 v0, -0x1

    .line 1311050
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/8CZ;->c:LX/8CX;

    iget-object v1, p0, LX/8CZ;->d:LX/0Px;

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, LX/8CX;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1311041
    iget-object v0, p0, LX/8CZ;->c:LX/8CX;

    invoke-interface {v0, p1, p2}, LX/8CX;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v1

    .line 1311042
    iget-object v0, p0, LX/8CZ;->c:LX/8CX;

    invoke-interface {v0}, LX/8CX;->a()I

    move-result v0

    .line 1311043
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 1311044
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    iget-object v2, p0, LX/8CZ;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    div-int/2addr v0, v2

    .line 1311045
    :cond_0
    iget-object v2, v1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setMinimumWidth(I)V

    .line 1311046
    iget-object v0, v1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setMinimumHeight(I)V

    .line 1311047
    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 1311036
    iget-object v0, p0, LX/8CZ;->d:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1311037
    iget-object v1, p0, LX/8CZ;->c:LX/8CX;

    iget-object v2, p0, LX/8CZ;->d:LX/0Px;

    invoke-virtual {v2, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, p1, v2}, LX/8CX;->a(LX/1a1;Ljava/lang/Object;)V

    .line 1311038
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v2, LX/8CY;

    invoke-direct {v2, p0, v0, p1}, LX/8CY;-><init>(LX/8CZ;Ljava/lang/Object;LX/1a1;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1311039
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    iget-object v1, p0, LX/8CZ;->e:Ljava/lang/String;

    iget-object v2, p0, LX/8CZ;->c:LX/8CX;

    iget-object v3, p0, LX/8CZ;->d:LX/0Px;

    invoke-virtual {v3, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, LX/8CX;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1311040
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 1311032
    iget-object v0, p0, LX/8CZ;->c:LX/8CX;

    if-eqz v0, :cond_0

    .line 1311033
    iget-object v0, p0, LX/8CZ;->c:LX/8CX;

    iget-object v1, p0, LX/8CZ;->d:LX/0Px;

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, LX/8CX;->c(Ljava/lang/Object;)I

    move-result v0

    .line 1311034
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1311035
    iget-object v0, p0, LX/8CZ;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
