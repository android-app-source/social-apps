.class public LX/8TY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1348750
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1348751
    iput-object p1, p0, LX/8TY;->a:LX/0Uh;

    .line 1348752
    return-void
.end method

.method public static a(LX/0QB;)LX/8TY;
    .locals 4

    .prologue
    .line 1348753
    const-class v1, LX/8TY;

    monitor-enter v1

    .line 1348754
    :try_start_0
    sget-object v0, LX/8TY;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1348755
    sput-object v2, LX/8TY;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1348756
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1348757
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1348758
    new-instance p0, LX/8TY;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/8TY;-><init>(LX/0Uh;)V

    .line 1348759
    move-object v0, p0

    .line 1348760
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1348761
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8TY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1348762
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1348763
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Z
    .locals 3

    .prologue
    .line 1348764
    iget-object v0, p0, LX/8TY;->a:LX/0Uh;

    const/16 v1, 0x4c5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 1348765
    iget-object v0, p0, LX/8TY;->a:LX/0Uh;

    const/16 v1, 0x4c2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
