.class public final enum LX/79k;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/79k;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/79k;

.field public static final enum PAST_MONTH:LX/79k;

.field public static final enum PAST_WEEK:LX/79k;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1174391
    new-instance v0, LX/79k;

    const-string v1, "PAST_WEEK"

    invoke-direct {v0, v1, v2}, LX/79k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79k;->PAST_WEEK:LX/79k;

    .line 1174392
    new-instance v0, LX/79k;

    const-string v1, "PAST_MONTH"

    invoke-direct {v0, v1, v3}, LX/79k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/79k;->PAST_MONTH:LX/79k;

    .line 1174393
    const/4 v0, 0x2

    new-array v0, v0, [LX/79k;

    sget-object v1, LX/79k;->PAST_WEEK:LX/79k;

    aput-object v1, v0, v2

    sget-object v1, LX/79k;->PAST_MONTH:LX/79k;

    aput-object v1, v0, v3

    sput-object v0, LX/79k;->$VALUES:[LX/79k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1174394
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static convertString(Ljava/lang/String;)LX/79k;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1174395
    if-eqz p0, :cond_0

    :try_start_0
    invoke-static {p0}, LX/79k;->valueOf(Ljava/lang/String;)LX/79k;

    move-result-object v0

    .line 1174396
    :goto_0
    return-object v0

    .line 1174397
    :cond_0
    sget-object v0, LX/79k;->PAST_WEEK:LX/79k;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1174398
    :catch_0
    sget-object v0, LX/79k;->PAST_WEEK:LX/79k;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/79k;
    .locals 1

    .prologue
    .line 1174399
    const-class v0, LX/79k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/79k;

    return-object v0
.end method

.method public static values()[LX/79k;
    .locals 1

    .prologue
    .line 1174400
    sget-object v0, LX/79k;->$VALUES:[LX/79k;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/79k;

    return-object v0
.end method
