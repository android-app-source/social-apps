.class public LX/7Ra;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/1Er;

.field private c:LX/61h;

.field public d:Ljava/io/File;

.field public e:LX/7RV;

.field public f:LX/7RV;

.field public g:Z

.field public h:J

.field public i:Z

.field private volatile j:Z

.field private volatile k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1207053
    const-class v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Ra;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/61h;LX/1Er;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1207054
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1207055
    const/4 v0, 0x0

    iput-object v0, p0, LX/7Ra;->d:Ljava/io/File;

    .line 1207056
    iput-boolean v2, p0, LX/7Ra;->g:Z

    .line 1207057
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/7Ra;->h:J

    .line 1207058
    iput-boolean v2, p0, LX/7Ra;->i:Z

    .line 1207059
    iput-boolean v2, p0, LX/7Ra;->j:Z

    .line 1207060
    iput-boolean v2, p0, LX/7Ra;->k:Z

    .line 1207061
    iput-object p1, p0, LX/7Ra;->c:LX/61h;

    .line 1207062
    iput-object p2, p0, LX/7Ra;->b:LX/1Er;

    .line 1207063
    return-void
.end method

.method private static declared-synchronized a(LX/7Ra;LX/614;)V
    .locals 3

    .prologue
    .line 1207064
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/7Ra;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 1207065
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1207066
    :cond_1
    :try_start_1
    iget-boolean v0, p0, LX/7Ra;->g:Z

    if-nez v0, :cond_2

    .line 1207067
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7Ra;->j:Z

    .line 1207068
    invoke-static {p0}, LX/7Ra;->d(LX/7Ra;)V

    .line 1207069
    :cond_2
    iget-boolean v0, p0, LX/7Ra;->g:Z

    if-eqz v0, :cond_0

    .line 1207070
    invoke-virtual {p1}, LX/614;->b()Landroid/media/MediaCodec$BufferInfo;

    move-result-object v0

    iget v0, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    .line 1207071
    :try_start_2
    iget-object v0, p0, LX/7Ra;->c:LX/61h;

    invoke-interface {v0, p1}, LX/61h;->a(LX/60z;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1207072
    :catch_0
    move-exception v0

    .line 1207073
    const/4 v1, 0x1

    :try_start_3
    iput-boolean v1, p0, LX/7Ra;->i:Z

    .line 1207074
    sget-object v1, LX/7Ra;->a:Ljava/lang/String;

    const-string v2, "LiveStreamMux Error writing Audio samples "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1207075
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/7Ra;LX/614;)V
    .locals 3

    .prologue
    .line 1207076
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/7Ra;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 1207077
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1207078
    :cond_1
    :try_start_1
    iget-boolean v0, p0, LX/7Ra;->g:Z

    if-nez v0, :cond_2

    .line 1207079
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7Ra;->k:Z

    .line 1207080
    invoke-static {p0}, LX/7Ra;->d(LX/7Ra;)V

    .line 1207081
    :cond_2
    iget-boolean v0, p0, LX/7Ra;->g:Z

    if-eqz v0, :cond_0

    .line 1207082
    invoke-virtual {p1}, LX/614;->b()Landroid/media/MediaCodec$BufferInfo;

    move-result-object v0

    iget v0, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    .line 1207083
    :try_start_2
    iget-object v0, p0, LX/7Ra;->c:LX/61h;

    invoke-interface {v0, p1}, LX/61h;->b(LX/60z;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1207084
    :catch_0
    move-exception v0

    .line 1207085
    const/4 v1, 0x1

    :try_start_3
    iput-boolean v1, p0, LX/7Ra;->i:Z

    .line 1207086
    sget-object v1, LX/7Ra;->a:Ljava/lang/String;

    const-string v2, "LiveStreamMux Error writing Video samples "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1207087
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static c(Ljava/nio/ByteBuffer;IIIIILandroid/media/MediaCodec$BufferInfo;)LX/614;
    .locals 7

    .prologue
    .line 1207088
    new-instance v1, LX/614;

    invoke-direct {v1, p0, p5, p6}, LX/614;-><init>(Ljava/nio/ByteBuffer;ILandroid/media/MediaCodec$BufferInfo;)V

    .line 1207089
    mul-int/lit16 v0, p3, 0x3e8

    .line 1207090
    int-to-long v4, v0

    move v2, p1

    move v3, p2

    move v6, p4

    invoke-virtual/range {v1 .. v6}, LX/614;->a(IIJI)V

    .line 1207091
    return-object v1
.end method

.method private static d(LX/7Ra;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1207092
    iget-object v0, p0, LX/7Ra;->d:Ljava/io/File;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207093
    :try_start_0
    iget-boolean v0, p0, LX/7Ra;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/7Ra;->j:Z

    if-nez v0, :cond_1

    .line 1207094
    :cond_0
    :goto_0
    return-void

    .line 1207095
    :cond_1
    iget-object v0, p0, LX/7Ra;->c:LX/61h;

    iget-object v1, p0, LX/7Ra;->d:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/61h;->a(Ljava/lang/String;)V

    .line 1207096
    iget-object v0, p0, LX/7Ra;->f:LX/7RV;

    .line 1207097
    iget-object v1, v0, LX/7RV;->o:Landroid/media/MediaCodec;

    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207098
    iget-object v1, v0, LX/7RV;->x:Landroid/media/MediaFormat;

    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207099
    iget-object v1, v0, LX/7RV;->x:Landroid/media/MediaFormat;

    move-object v0, v1

    .line 1207100
    iget-object v1, p0, LX/7Ra;->c:LX/61h;

    invoke-interface {v1, v0}, LX/61h;->b(Landroid/media/MediaFormat;)V

    .line 1207101
    iget-object v0, p0, LX/7Ra;->c:LX/61h;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/61h;->a(I)V

    .line 1207102
    iget-object v0, p0, LX/7Ra;->e:LX/7RV;

    .line 1207103
    iget-object v1, v0, LX/7RV;->g:Landroid/media/MediaCodec;

    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207104
    iget-object v1, v0, LX/7RV;->y:Landroid/media/MediaFormat;

    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207105
    iget-object v1, v0, LX/7RV;->y:Landroid/media/MediaFormat;

    move-object v0, v1

    .line 1207106
    iget-object v1, p0, LX/7Ra;->c:LX/61h;

    invoke-interface {v1, v0}, LX/61h;->a(Landroid/media/MediaFormat;)V

    .line 1207107
    iget-object v0, p0, LX/7Ra;->c:LX/61h;

    invoke-interface {v0}, LX/61h;->a()V

    .line 1207108
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7Ra;->g:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1207109
    :catch_0
    move-exception v0

    .line 1207110
    iput-boolean v2, p0, LX/7Ra;->i:Z

    .line 1207111
    sget-object v1, LX/7Ra;->a:Ljava/lang/String;

    const-string v2, "LiveStreamMux Error adding tracks and starting muxer "

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1207112
    iput-boolean v3, p0, LX/7Ra;->g:Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;IIIIILandroid/media/MediaCodec$BufferInfo;)V
    .locals 6

    .prologue
    .line 1207113
    invoke-static/range {p1 .. p7}, LX/7Ra;->c(Ljava/nio/ByteBuffer;IIIIILandroid/media/MediaCodec$BufferInfo;)LX/614;

    move-result-object v0

    .line 1207114
    iget-wide v2, p7, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iget-wide v4, p0, LX/7Ra;->h:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 1207115
    sget-object v0, LX/7Ra;->a:Ljava/lang/String;

    const-string v1, "LiveStreamMux Audio PTS OutOfOrder CurPresentationTime %d Last PresentationTime %d "

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p7, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, LX/7Ra;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1207116
    :goto_0
    return-void

    .line 1207117
    :cond_0
    iget-wide v2, p7, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iput-wide v2, p0, LX/7Ra;->h:J

    .line 1207118
    invoke-static {p0, v0}, LX/7Ra;->a(LX/7Ra;LX/614;)V

    goto :goto_0
.end method

.method public final b(Ljava/nio/ByteBuffer;IIIIILandroid/media/MediaCodec$BufferInfo;)V
    .locals 1

    .prologue
    .line 1207119
    invoke-static/range {p1 .. p7}, LX/7Ra;->c(Ljava/nio/ByteBuffer;IIIIILandroid/media/MediaCodec$BufferInfo;)LX/614;

    move-result-object v0

    .line 1207120
    invoke-static {p0, v0}, LX/7Ra;->b(LX/7Ra;LX/614;)V

    .line 1207121
    return-void
.end method

.method public final c()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1207122
    iget-boolean v2, p0, LX/7Ra;->g:Z

    if-eqz v2, :cond_0

    .line 1207123
    :try_start_0
    iget-object v2, p0, LX/7Ra;->c:LX/61h;

    invoke-interface {v2}, LX/61h;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1207124
    :goto_0
    iput-boolean v1, p0, LX/7Ra;->j:Z

    .line 1207125
    iput-boolean v1, p0, LX/7Ra;->k:Z

    .line 1207126
    iput-boolean v1, p0, LX/7Ra;->g:Z

    .line 1207127
    iget-boolean v2, p0, LX/7Ra;->i:Z

    if-nez v2, :cond_1

    :goto_1
    return v0

    .line 1207128
    :catch_0
    move-exception v2

    .line 1207129
    iput-boolean v0, p0, LX/7Ra;->i:Z

    .line 1207130
    sget-object v3, LX/7Ra;->a:Ljava/lang/String;

    const-string v4, "LiveStreamMux Error stopping muxer "

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1207131
    :cond_0
    sget-object v2, LX/7Ra;->a:Ljava/lang/String;

    const-string v3, "LiveStreamMux Never started muxer...Nothing to stop "

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1207132
    goto :goto_1
.end method
