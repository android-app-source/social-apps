.class public final enum LX/8Pr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Pr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Pr;

.field public static final enum DEFAULT:LX/8Pr;

.field public static final enum STICKY:LX/8Pr;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1342196
    new-instance v0, LX/8Pr;

    const-string v1, "STICKY"

    invoke-direct {v0, v1, v2}, LX/8Pr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Pr;->STICKY:LX/8Pr;

    .line 1342197
    new-instance v0, LX/8Pr;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v3}, LX/8Pr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Pr;->DEFAULT:LX/8Pr;

    .line 1342198
    const/4 v0, 0x2

    new-array v0, v0, [LX/8Pr;

    sget-object v1, LX/8Pr;->STICKY:LX/8Pr;

    aput-object v1, v0, v2

    sget-object v1, LX/8Pr;->DEFAULT:LX/8Pr;

    aput-object v1, v0, v3

    sput-object v0, LX/8Pr;->$VALUES:[LX/8Pr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1342199
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Pr;
    .locals 1

    .prologue
    .line 1342200
    const-class v0, LX/8Pr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Pr;

    return-object v0
.end method

.method public static values()[LX/8Pr;
    .locals 1

    .prologue
    .line 1342201
    sget-object v0, LX/8Pr;->$VALUES:[LX/8Pr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Pr;

    return-object v0
.end method
