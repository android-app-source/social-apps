.class public LX/8Vt;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/8Vp;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8Vb;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8Vb;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/8Vx;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1353430
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1353431
    new-instance v0, LX/8Vq;

    invoke-direct {v0, p0}, LX/8Vq;-><init>(LX/8Vt;)V

    iput-object v0, p0, LX/8Vt;->b:LX/8Vp;

    .line 1353432
    iput-object p1, p0, LX/8Vt;->a:Landroid/content/Context;

    .line 1353433
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 1353434
    return-void
.end method

.method private e(I)LX/8Vb;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1353435
    const/4 v0, 0x0

    .line 1353436
    iget-object v2, p0, LX/8Vt;->c:LX/0Px;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/8Vt;->c:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1353437
    iget-object v0, p0, LX/8Vt;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-gt p1, v0, :cond_1

    .line 1353438
    if-nez p1, :cond_0

    move-object v0, v1

    .line 1353439
    :goto_0
    return-object v0

    .line 1353440
    :cond_0
    iget-object v0, p0, LX/8Vt;->c:LX/0Px;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Vb;

    goto :goto_0

    .line 1353441
    :cond_1
    iget-object v0, p0, LX/8Vt;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1353442
    :cond_2
    sub-int v0, p1, v0

    .line 1353443
    iget-object v2, p0, LX/8Vt;->d:LX/0Px;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/8Vt;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1353444
    if-nez v0, :cond_3

    move-object v0, v1

    .line 1353445
    goto :goto_0

    .line 1353446
    :cond_3
    iget-object v1, p0, LX/8Vt;->d:LX/0Px;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Vb;

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 1353447
    goto :goto_0
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 1353448
    invoke-virtual {p0, p1}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 1353449
    packed-switch v0, :pswitch_data_0

    .line 1353450
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1353451
    :pswitch_0
    const-wide/16 v0, -0x1

    .line 1353452
    :goto_0
    return-wide v0

    :pswitch_1
    invoke-direct {p0, p1}, LX/8Vt;->e(I)LX/8Vb;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1353453
    packed-switch p2, :pswitch_data_0

    .line 1353454
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "incorrect view creation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1353455
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030780

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1353456
    new-instance v0, LX/8Vs;

    invoke-direct {v0, p0, v1}, LX/8Vs;-><init>(LX/8Vt;Landroid/view/View;)V

    .line 1353457
    :goto_0
    return-object v0

    .line 1353458
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03077e

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1353459
    new-instance v0, LX/8W2;

    invoke-direct {v0, v1}, LX/8W2;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1353460
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030780

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1353461
    new-instance v0, LX/8Vs;

    invoke-direct {v0, p0, v1}, LX/8Vs;-><init>(LX/8Vt;Landroid/view/View;)V

    goto :goto_0

    .line 1353462
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03077e

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1353463
    new-instance v0, LX/8W2;

    invoke-direct {v0, v1}, LX/8W2;-><init>(Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 1353464
    move-object v0, p1

    check-cast v0, LX/8Vr;

    .line 1353465
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v2

    invoke-direct {p0, p2}, LX/8Vt;->e(I)LX/8Vb;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, LX/8Vt;->b:LX/8Vp;

    move v1, p2

    invoke-interface/range {v0 .. v5}, LX/8Vr;->a(IILX/8Vb;ZLX/8Vp;)V

    .line 1353466
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1353467
    iget-object v0, p0, LX/8Vt;->c:LX/0Px;

    if-eqz v0, :cond_0

    move v1, v2

    .line 1353468
    :goto_0
    iget-object v0, p0, LX/8Vt;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1353469
    iget-object v0, p0, LX/8Vt;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Vb;

    invoke-virtual {v0}, LX/8Vb;->a()V

    .line 1353470
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1353471
    :cond_0
    iget-object v0, p0, LX/8Vt;->d:LX/0Px;

    if-eqz v0, :cond_1

    .line 1353472
    :goto_1
    iget-object v0, p0, LX/8Vt;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1353473
    iget-object v0, p0, LX/8Vt;->d:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Vb;

    invoke-virtual {v0}, LX/8Vb;->a()V

    .line 1353474
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1353475
    :cond_1
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 1353476
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1353477
    iget-object v1, p0, LX/8Vt;->c:LX/0Px;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/8Vt;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1353478
    iget-object v1, p0, LX/8Vt;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-gt p1, v1, :cond_1

    .line 1353479
    if-nez p1, :cond_0

    .line 1353480
    :goto_0
    return v0

    .line 1353481
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 1353482
    :cond_1
    iget-object v0, p0, LX/8Vt;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1353483
    :cond_2
    sub-int v0, p1, v0

    .line 1353484
    iget-object v1, p0, LX/8Vt;->d:LX/0Px;

    if-eqz v1, :cond_4

    iget-object v1, p0, LX/8Vt;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1353485
    if-nez v0, :cond_3

    .line 1353486
    const/4 v0, 0x1

    goto :goto_0

    .line 1353487
    :cond_3
    const/4 v0, 0x3

    goto :goto_0

    .line 1353488
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Incorrect view type calculation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 1353489
    const/4 v0, 0x0

    .line 1353490
    iget-object v1, p0, LX/8Vt;->c:LX/0Px;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/8Vt;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1353491
    iget-object v0, p0, LX/8Vt;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 1353492
    :cond_0
    iget-object v1, p0, LX/8Vt;->d:LX/0Px;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/8Vt;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1353493
    iget-object v1, p0, LX/8Vt;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1353494
    :cond_1
    return v0
.end method
