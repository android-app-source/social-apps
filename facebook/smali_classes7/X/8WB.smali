.class public LX/8WB;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/8Vp;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8Vb;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8Vb;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/8Vz;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1353842
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1353843
    new-instance v0, LX/8W8;

    invoke-direct {v0, p0}, LX/8W8;-><init>(LX/8WB;)V

    iput-object v0, p0, LX/8WB;->b:LX/8Vp;

    .line 1353844
    iput-object p1, p0, LX/8WB;->a:Landroid/content/Context;

    .line 1353845
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 1353846
    return-void
.end method

.method private e(I)LX/8Vb;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1353827
    iget-object v0, p0, LX/8WB;->c:LX/0Px;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/8WB;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1353828
    iget-object v0, p0, LX/8WB;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-gt p1, v0, :cond_1

    .line 1353829
    if-nez p1, :cond_0

    move-object v0, v1

    .line 1353830
    :goto_0
    return-object v0

    .line 1353831
    :cond_0
    iget-object v0, p0, LX/8WB;->c:LX/0Px;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Vb;

    goto :goto_0

    .line 1353832
    :cond_1
    iget-object v0, p0, LX/8WB;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1353833
    :goto_1
    sub-int v0, p1, v0

    .line 1353834
    iget-object v2, p0, LX/8WB;->d:LX/0Px;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/8WB;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1353835
    if-nez v0, :cond_4

    move-object v0, v1

    .line 1353836
    goto :goto_0

    .line 1353837
    :cond_2
    if-nez p1, :cond_3

    move-object v0, v1

    .line 1353838
    goto :goto_0

    .line 1353839
    :cond_3
    const/4 v0, 0x1

    goto :goto_1

    .line 1353840
    :cond_4
    iget-object v1, p0, LX/8WB;->d:LX/0Px;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Vb;

    goto :goto_0

    :cond_5
    move-object v0, v1

    .line 1353841
    goto :goto_0
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 1353822
    invoke-virtual {p0, p1}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 1353823
    packed-switch v0, :pswitch_data_0

    .line 1353824
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1353825
    :pswitch_0
    const-wide/16 v0, -0x1

    .line 1353826
    :goto_0
    return-wide v0

    :pswitch_1
    invoke-direct {p0, p1}, LX/8WB;->e(I)LX/8Vb;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1353847
    packed-switch p2, :pswitch_data_0

    .line 1353848
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "incorrect view creation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1353849
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030780

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1353850
    new-instance v0, LX/8WA;

    invoke-direct {v0, p0, v1}, LX/8WA;-><init>(LX/8WB;Landroid/view/View;)V

    .line 1353851
    :goto_0
    return-object v0

    .line 1353852
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03077e

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1353853
    new-instance v0, LX/8W2;

    invoke-direct {v0, v1}, LX/8W2;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1353854
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030780

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1353855
    new-instance v0, LX/8WA;

    invoke-direct {v0, p0, v1}, LX/8WA;-><init>(LX/8WB;Landroid/view/View;)V

    goto :goto_0

    .line 1353856
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03077e

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1353857
    new-instance v0, LX/8W2;

    invoke-direct {v0, v1}, LX/8W2;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1353858
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030766

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1353859
    new-instance v0, LX/8W9;

    invoke-direct {v0, p0, v1}, LX/8W9;-><init>(LX/8WB;Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 1353819
    move-object v0, p1

    check-cast v0, LX/8Vr;

    .line 1353820
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v2

    invoke-direct {p0, p2}, LX/8WB;->e(I)LX/8Vb;

    move-result-object v3

    const/4 v4, 0x1

    iget-object v5, p0, LX/8WB;->b:LX/8Vp;

    move v1, p2

    invoke-interface/range {v0 .. v5}, LX/8Vr;->a(IILX/8Vb;ZLX/8Vp;)V

    .line 1353821
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1353809
    iget-object v0, p0, LX/8WB;->c:LX/0Px;

    if-eqz v0, :cond_0

    move v1, v2

    .line 1353810
    :goto_0
    iget-object v0, p0, LX/8WB;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1353811
    iget-object v0, p0, LX/8WB;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Vb;

    invoke-virtual {v0}, LX/8Vb;->a()V

    .line 1353812
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1353813
    :cond_0
    iget-object v0, p0, LX/8WB;->d:LX/0Px;

    if-eqz v0, :cond_1

    .line 1353814
    :goto_1
    iget-object v0, p0, LX/8WB;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1353815
    iget-object v0, p0, LX/8WB;->d:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Vb;

    invoke-virtual {v0}, LX/8Vb;->a()V

    .line 1353816
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1353817
    :cond_1
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 1353818
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1353795
    iget-object v1, p0, LX/8WB;->c:LX/0Px;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/8WB;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1353796
    iget-object v1, p0, LX/8WB;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-gt p1, v1, :cond_1

    .line 1353797
    if-nez p1, :cond_0

    .line 1353798
    const/4 v0, 0x0

    .line 1353799
    :cond_0
    :goto_0
    return v0

    .line 1353800
    :cond_1
    iget-object v0, p0, LX/8WB;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1353801
    :cond_2
    sub-int v0, p1, v0

    .line 1353802
    iget-object v1, p0, LX/8WB;->d:LX/0Px;

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/8WB;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1353803
    if-nez v0, :cond_4

    .line 1353804
    const/4 v0, 0x3

    goto :goto_0

    .line 1353805
    :cond_3
    if-nez p1, :cond_2

    .line 1353806
    const/4 v0, 0x2

    goto :goto_0

    .line 1353807
    :cond_4
    const/4 v0, 0x4

    goto :goto_0

    .line 1353808
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Incorrect view type calculation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 1353789
    iget-object v0, p0, LX/8WB;->c:LX/0Px;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8WB;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1353790
    iget-object v0, p0, LX/8WB;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 1353791
    :goto_0
    iget-object v1, p0, LX/8WB;->d:LX/0Px;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/8WB;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1353792
    iget-object v1, p0, LX/8WB;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1353793
    :cond_0
    return v0

    .line 1353794
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
