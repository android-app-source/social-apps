.class public final LX/72G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;)V
    .locals 0

    .prologue
    .line 1164073
    iput-object p1, p0, LX/72G;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1164074
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p0, LX/72G;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v1, v1, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->t:LX/73H;

    invoke-virtual {v1}, LX/73H;->a()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 1164075
    iget-object v0, p0, LX/72G;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    .line 1164076
    :cond_0
    iget-object v0, p0, LX/72G;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->d:LX/72M;

    iget-object v1, p0, LX/72G;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    invoke-virtual {v1}, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/72M;->a(Z)V

    .line 1164077
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1164078
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1164079
    return-void
.end method
