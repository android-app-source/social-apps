.class public LX/8Lq;
.super LX/1OM;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/compost/story/CompostStory;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8K6;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/8Ma;

.field private final e:LX/8Lk;

.field private final f:LX/8Lb;

.field private final g:LX/8Lo;

.field public h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/photos/upload/progresspage/CompostRecyclerViewAdapter$CompostStoryListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/8Ma;LX/8Lk;LX/8Lb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1334032
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1334033
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8Lq;->a:Ljava/util/List;

    .line 1334034
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8Lq;->b:Ljava/util/List;

    .line 1334035
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8Lq;->c:Ljava/util/List;

    .line 1334036
    new-instance v0, LX/8Lo;

    invoke-direct {v0, p0}, LX/8Lo;-><init>(LX/8Lq;)V

    iput-object v0, p0, LX/8Lq;->g:LX/8Lo;

    .line 1334037
    iput-object p1, p0, LX/8Lq;->d:LX/8Ma;

    .line 1334038
    iput-object p2, p0, LX/8Lq;->e:LX/8Lk;

    .line 1334039
    iput-object p3, p0, LX/8Lq;->f:LX/8Lb;

    .line 1334040
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/8Lq;->h:LX/0am;

    .line 1334041
    return-void
.end method

.method public static a(LX/8Lq;LX/8K6;)I
    .locals 2

    .prologue
    .line 1334027
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/8Lq;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1334028
    iget-object v1, p0, LX/8Lq;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 1334029
    :goto_1
    return v0

    .line 1334030
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1334031
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static a(LX/8Lq;LX/8Lp;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1334022
    iget v0, p1, LX/8Lp;->a:I

    if-ltz v0, :cond_0

    iget v0, p1, LX/8Lp;->a:I

    iget-object v2, p0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 1334023
    :goto_0
    return v0

    .line 1334024
    :cond_1
    iget v0, p1, LX/8Lp;->b:I

    if-ltz v0, :cond_2

    iget v2, p1, LX/8Lp;->b:I

    iget-object v0, p0, LX/8Lq;->b:Ljava/util/List;

    iget v3, p1, LX/8Lp;->a:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-lt v2, v0, :cond_3

    :cond_2
    move v0, v1

    .line 1334025
    goto :goto_0

    .line 1334026
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(LX/8Lq;LX/8Lp;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1334014
    move v1, v0

    move v2, v0

    .line 1334015
    :goto_0
    iget-object v0, p0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v3, p1, LX/8Lp;->a:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1334016
    iget-object v0, p0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1334017
    iget-object v0, p0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 1334018
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1334019
    :cond_1
    iget-object v0, p0, LX/8Lq;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1334020
    add-int/lit8 v2, v2, 0x2

    goto :goto_1

    .line 1334021
    :cond_2
    iget v0, p1, LX/8Lp;->b:I

    add-int/2addr v0, v2

    return v0
.end method

.method public static f(LX/8Lq;)V
    .locals 2

    .prologue
    .line 1334006
    invoke-static {p0}, LX/8Lq;->h(LX/8Lq;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/8Lq;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1334007
    iget-object v0, p0, LX/8Lq;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Ld;

    .line 1334008
    iget-object v1, v0, LX/8Ld;->a:Lcom/facebook/photos/upload/progresspage/CompostFragment;

    const/16 v0, 0x8

    .line 1334009
    iget-object p0, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1334010
    iget-object p0, v1, Lcom/facebook/photos/upload/progresspage/CompostFragment;->m:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 1334011
    const/4 p0, 0x0

    invoke-static {v1, p0}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->a(Lcom/facebook/photos/upload/progresspage/CompostFragment;I)V

    .line 1334012
    invoke-static {v1, v0}, Lcom/facebook/photos/upload/progresspage/CompostFragment;->d(Lcom/facebook/photos/upload/progresspage/CompostFragment;I)V

    .line 1334013
    :cond_0
    return-void
.end method

.method public static g(LX/8Lq;I)LX/8Lp;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1333992
    add-int/lit8 v1, p1, 0x0

    .line 1333993
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1333994
    iget-object v0, p0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1333995
    iget-object v0, p0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1333996
    :goto_1
    if-gez v2, :cond_1

    .line 1333997
    new-instance v0, LX/8Lp;

    invoke-direct {v0, v3, v3}, LX/8Lp;-><init>(II)V

    .line 1333998
    :goto_2
    return-object v0

    .line 1333999
    :cond_0
    iget-object v0, p0, LX/8Lq;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1334000
    const/4 v0, 0x2

    goto :goto_1

    .line 1334001
    :cond_1
    if-ge v2, v0, :cond_2

    .line 1334002
    new-instance v0, LX/8Lp;

    invoke-direct {v0, v1, v2}, LX/8Lp;-><init>(II)V

    goto :goto_2

    .line 1334003
    :cond_2
    sub-int/2addr v2, v0

    .line 1334004
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1334005
    :cond_4
    new-instance v0, LX/8Lp;

    invoke-direct {v0, v3, v3}, LX/8Lp;-><init>(II)V

    goto :goto_2
.end method

.method public static h(LX/8Lq;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1333987
    move v1, v2

    :goto_0
    iget-object v0, p0, LX/8Lq;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1333988
    iget-object v0, p0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1333989
    const/4 v2, 0x1

    .line 1333990
    :cond_0
    return v2

    .line 1333991
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 1333977
    packed-switch p2, :pswitch_data_0

    .line 1333978
    :pswitch_0
    iget-object v0, p0, LX/8Lq;->d:LX/8Ma;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03154f

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/8Ma;->a(Landroid/content/Context;Landroid/view/View;)Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1333979
    :pswitch_1
    iget-object v0, p0, LX/8Lq;->e:LX/8Lk;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03154c

    invoke-virtual {v1, v2, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1333980
    new-instance v5, LX/8Lj;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v7

    check-cast v7, LX/0kb;

    invoke-static {v0}, LX/1RW;->b(LX/0QB;)LX/1RW;

    move-result-object v8

    check-cast v8, LX/1RW;

    invoke-static {v0}, LX/0YQ;->a(LX/0QB;)LX/0YQ;

    move-result-object v9

    check-cast v9, LX/0YR;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    move-object v6, v1

    invoke-direct/range {v5 .. v10}, LX/8Lj;-><init>(Landroid/view/View;LX/0kb;LX/1RW;LX/0YR;LX/0ad;)V

    .line 1333981
    move-object v0, v5

    .line 1333982
    goto :goto_0

    .line 1333983
    :pswitch_2
    iget-object v0, p0, LX/8Lq;->f:LX/8Lb;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03154b

    invoke-virtual {v1, v2, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1333984
    new-instance v3, LX/8La;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-direct {v3, v1, v2}, LX/8La;-><init>(Landroid/view/View;LX/0ad;)V

    .line 1333985
    move-object v0, v3

    .line 1333986
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/1a1;)V
    .locals 3

    .prologue
    .line 1333962
    iget v0, p1, LX/1a1;->e:I

    move v0, v0

    .line 1333963
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1333964
    check-cast p1, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    .line 1333965
    iget-object v0, p1, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->J:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 1333966
    iget-object v0, p1, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1333967
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p0, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8MA;

    .line 1333968
    invoke-interface {v0}, LX/8MA;->a()V

    .line 1333969
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1333970
    :cond_0
    iget-object v0, p1, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->L:LX/8K6;

    sget-object v1, LX/8K6;->PENDING_SECTION:LX/8K6;

    if-ne v0, v1, :cond_1

    iget-boolean v0, p1, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->M:Z

    if-eqz v0, :cond_1

    .line 1333971
    iget-object v0, p1, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->N:LX/8MZ;

    .line 1333972
    iget-boolean v1, v0, LX/8MZ;->b:Z

    if-eqz v1, :cond_1

    .line 1333973
    iget-object v1, v0, LX/8MZ;->d:LX/0b3;

    iget-object v2, v0, LX/8MZ;->c:LX/8KR;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 1333974
    :cond_1
    iget-object v0, p1, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->B:Landroid/os/Handler;

    iget-object v1, p1, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->O:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1333975
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a(LX/8Lo;)V

    .line 1333976
    :cond_2
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 11

    .prologue
    .line 1333856
    iget v0, p1, LX/1a1;->e:I

    move v1, v0

    .line 1333857
    invoke-static {p0, p2}, LX/8Lq;->g(LX/8Lq;I)LX/8Lp;

    move-result-object v2

    .line 1333858
    iget-object v0, p0, LX/8Lq;->c:Ljava/util/List;

    iget v3, v2, LX/8Lp;->a:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8K6;

    .line 1333859
    packed-switch v1, :pswitch_data_0

    .line 1333860
    :cond_0
    :goto_0
    return-void

    .line 1333861
    :pswitch_0
    if-nez p2, :cond_1

    iget-object v1, p0, LX/8Lq;->b:Ljava/util/List;

    iget v2, v2, LX/8Lp;->a:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 1333862
    :goto_1
    check-cast p1, LX/8Lj;

    const/4 v10, 0x0

    .line 1333863
    if-eqz v1, :cond_4

    sget-object v4, LX/8K6;->UPLOADED_SECTION:LX/8K6;

    if-eq v0, v4, :cond_4

    .line 1333864
    iget-object v4, p1, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4, v10}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1333865
    :goto_2
    sget-object v4, LX/8Li;->a:[I

    invoke-virtual {v0}, LX/8K6;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 1333866
    :goto_3
    goto :goto_0

    .line 1333867
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 1333868
    :pswitch_1
    check-cast p1, LX/8La;

    .line 1333869
    iget-object v0, p1, LX/8La;->l:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p1, LX/8La;->m:LX/0ad;

    sget-char v2, LX/1aO;->J:C

    iget-object v3, p1, LX/8La;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbTextView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f082008

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1333870
    goto :goto_0

    .line 1333871
    :pswitch_2
    check-cast p1, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    .line 1333872
    invoke-static {p0, p2}, LX/8Lq;->g(LX/8Lq;I)LX/8Lp;

    move-result-object v2

    .line 1333873
    invoke-static {p0, v2}, LX/8Lq;->a(LX/8Lq;LX/8Lp;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, v2, LX/8Lp;->b:I

    if-nez v1, :cond_f

    .line 1333874
    :cond_2
    const/4 v1, 0x0

    .line 1333875
    :goto_4
    move-object v1, v1

    .line 1333876
    if-eqz v1, :cond_0

    .line 1333877
    invoke-virtual {p1, v1, v0}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a(LX/7mi;LX/8K6;)V

    .line 1333878
    sget-object v1, LX/8K6;->DRAFT_SECTION:LX/8K6;

    if-eq v0, v1, :cond_3

    sget-object v1, LX/8K6;->PENDING_SECTION:LX/8K6;

    if-eq v0, v1, :cond_3

    sget-object v1, LX/8K6;->SCHEDULED_SECTION:LX/8K6;

    if-ne v0, v1, :cond_0

    .line 1333879
    :cond_3
    iget-object v0, p0, LX/8Lq;->g:LX/8Lo;

    invoke-virtual {p1, v0}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a(LX/8Lo;)V

    goto :goto_0

    .line 1333880
    :cond_4
    iget-object v4, p1, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_2

    .line 1333881
    :pswitch_3
    const v4, 0x7f02031d

    invoke-static {p1, v4}, LX/8Lj;->c(LX/8Lj;I)V

    .line 1333882
    iget-object v4, p1, LX/8Lj;->o:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    const v5, 0x7f08200b

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setText(I)V

    .line 1333883
    iget-object v4, p1, LX/8Lj;->m:LX/0ad;

    sget-short v5, LX/1aO;->ae:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 1333884
    if-nez v4, :cond_8

    .line 1333885
    iget-object v4, p1, LX/8Lj;->l:LX/0kb;

    invoke-virtual {v4}, LX/0kb;->d()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1333886
    iget-object v4, p1, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    const v5, 0x7f08200a

    invoke-virtual {v4, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1333887
    :goto_5
    iget-object v4, p1, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    if-eqz v4, :cond_5

    iget-object v4, p1, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1333888
    :cond_5
    iget-object v4, p1, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1333889
    :cond_6
    goto/16 :goto_3

    .line 1333890
    :pswitch_4
    const v4, 0x7f02031e

    invoke-static {p1, v4}, LX/8Lj;->c(LX/8Lj;I)V

    .line 1333891
    iget-object v4, p1, LX/8Lj;->o:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    const v5, 0x7f08200c

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setText(I)V

    .line 1333892
    iget-object v4, p1, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 1333893
    :pswitch_5
    const v4, 0x7f02031b

    invoke-static {p1, v4}, LX/8Lj;->c(LX/8Lj;I)V

    .line 1333894
    iget-object v4, p1, LX/8Lj;->o:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    const v5, 0x7f08200e

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setText(I)V

    .line 1333895
    iget-object v4, p1, LX/8Lj;->m:LX/0ad;

    sget-wide v6, LX/1aO;->m:J

    const-wide/16 v8, 0x3

    invoke-interface {v4, v6, v7, v8, v9}, LX/0ad;->a(JJ)J

    move-result-wide v4

    long-to-int v4, v4

    .line 1333896
    iget-object v5, p1, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    iget-object v6, p1, LX/8Lj;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f00fb

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v4, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 1333897
    :pswitch_6
    const v4, 0x7f02031e

    invoke-static {p1, v4}, LX/8Lj;->c(LX/8Lj;I)V

    .line 1333898
    iget-object v4, p1, LX/8Lj;->o:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 1333899
    iget-object v5, p1, LX/8Lj;->m:LX/0ad;

    sget-char v6, LX/1aO;->aB:C

    iget-object v7, p1, LX/8Lj;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f082010

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 1333900
    invoke-virtual {v4, v5}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1333901
    iget-object v4, p1, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 1333902
    iget-object v5, p1, LX/8Lj;->m:LX/0ad;

    sget-char v6, LX/1aO;->aA:C

    iget-object v7, p1, LX/8Lj;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f082011

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 1333903
    invoke-virtual {v4, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 1333904
    :pswitch_7
    const v4, 0x7f02031c

    invoke-static {p1, v4}, LX/8Lj;->c(LX/8Lj;I)V

    .line 1333905
    iget-object v4, p1, LX/8Lj;->o:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 1333906
    iget-object v5, p1, LX/8Lj;->m:LX/0ad;

    sget-char v6, LX/1aO;->aq:C

    iget-object v7, p1, LX/8Lj;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f082012

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 1333907
    invoke-virtual {v4, v5}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1333908
    iget-object v4, p1, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 1333909
    iget-object v5, p1, LX/8Lj;->m:LX/0ad;

    sget-char v6, LX/1aO;->ap:C

    iget-object v7, p1, LX/8Lj;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f082013

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 1333910
    invoke-virtual {v4, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 1333911
    :cond_7
    iget-object v4, p1, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    const v5, 0x7f082015

    invoke-virtual {v4, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1333912
    iget-object v4, p1, LX/8Lj;->q:LX/1RW;

    const-string v5, "no_network"

    invoke-virtual {v4, v5}, LX/1RW;->k(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1333913
    :cond_8
    iget-object v4, p1, LX/8Lj;->l:LX/0kb;

    invoke-virtual {v4}, LX/0kb;->d()Z

    move-result v4

    if-nez v4, :cond_9

    .line 1333914
    invoke-static {p1}, LX/8Lj;->D(LX/8Lj;)V

    goto/16 :goto_5

    .line 1333915
    :cond_9
    iget-object v4, p1, LX/8Lj;->r:Ljava/lang/String;

    const/4 v6, 0x0

    .line 1333916
    const-string v5, "excellent"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 1333917
    iget-object v5, p1, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1333918
    iget-object v5, p1, LX/8Lj;->q:LX/1RW;

    const-string v6, "excellent_network"

    invoke-virtual {v5, v6}, LX/1RW;->k(Ljava/lang/String;)V

    .line 1333919
    :cond_a
    :goto_6
    goto/16 :goto_5

    .line 1333920
    :cond_b
    const-string v5, "good"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 1333921
    iget-object v5, p1, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1333922
    iget-object v5, p1, LX/8Lj;->q:LX/1RW;

    const-string v6, "good_network"

    invoke-virtual {v5, v6}, LX/1RW;->k(Ljava/lang/String;)V

    goto :goto_6

    .line 1333923
    :cond_c
    const-string v5, "moderate"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 1333924
    iget-object v5, p1, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    const v6, 0x7f08200a

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1333925
    iget-object v5, p1, LX/8Lj;->q:LX/1RW;

    const-string v6, "moderate_network"

    invoke-virtual {v5, v6}, LX/1RW;->k(Ljava/lang/String;)V

    goto :goto_6

    .line 1333926
    :cond_d
    const-string v5, "poor"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 1333927
    iget-object v5, p1, LX/8Lj;->p:Lcom/facebook/resources/ui/FbTextView;

    const v6, 0x7f08200a

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1333928
    iget-object v5, p1, LX/8Lj;->q:LX/1RW;

    const-string v6, "poor_network"

    invoke-virtual {v5, v6}, LX/1RW;->k(Ljava/lang/String;)V

    goto :goto_6

    .line 1333929
    :cond_e
    const-string v5, "unknown"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1333930
    invoke-static {p1}, LX/8Lj;->D(LX/8Lj;)V

    goto :goto_6

    :cond_f
    iget-object v1, p0, LX/8Lq;->b:Ljava/util/List;

    iget v3, v2, LX/8Lp;->a:I

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget v2, v2, LX/8Lp;->b:I

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7mi;

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final a(LX/8K6;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/compost/story/CompostStory;",
            ">(",
            "LX/8K6;",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1333952
    invoke-static {p0, p1}, LX/8Lq;->a(LX/8Lq;LX/8K6;)I

    move-result v2

    .line 1333953
    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1333954
    iget-object v0, p0, LX/8Lq;->b:Ljava/util/List;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1333955
    new-instance v0, LX/8Lp;

    invoke-direct {v0, v2, v1}, LX/8Lp;-><init>(II)V

    invoke-static {p0, v0}, LX/8Lq;->b(LX/8Lq;LX/8Lp;)I

    move-result v1

    .line 1333956
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8Lq;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1333957
    const/4 v0, 0x2

    invoke-virtual {p0, v1, v0}, LX/1OM;->c(II)V

    .line 1333958
    :goto_1
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    invoke-virtual {p0, v1, v0}, LX/1OM;->a(II)V

    .line 1333959
    return-void

    :cond_0
    move v0, v1

    .line 1333960
    goto :goto_0

    .line 1333961
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, v1, v0}, LX/1OM;->c(II)V

    goto :goto_1
.end method

.method public final a(LX/8Ld;)V
    .locals 1
    .param p1    # LX/8Ld;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1333950
    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/8Lq;->h:LX/0am;

    .line 1333951
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 1333942
    invoke-static {p0, p1}, LX/8Lq;->g(LX/8Lq;I)LX/8Lp;

    move-result-object v0

    iget v0, v0, LX/8Lp;->b:I

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1333943
    if-eqz v0, :cond_0

    .line 1333944
    const/4 v0, 0x0

    .line 1333945
    :goto_1
    return v0

    .line 1333946
    :cond_0
    invoke-static {p0, p1}, LX/8Lq;->g(LX/8Lq;I)LX/8Lp;

    move-result-object v0

    iget v1, v0, LX/8Lp;->a:I

    .line 1333947
    iget-object v0, p0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8Lq;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1333948
    const/4 v0, 0x2

    goto :goto_1

    .line 1333949
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1333931
    move v2, v0

    move v3, v0

    .line 1333932
    :goto_0
    iget-object v0, p0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1333933
    iget-object v0, p0, LX/8Lq;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1333934
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/8Lq;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1333935
    add-int/lit8 v1, v3, 0x2

    .line 1333936
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v1

    goto :goto_0

    .line 1333937
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1333938
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int v1, v3, v0

    goto :goto_1

    .line 1333939
    :cond_1
    if-nez v3, :cond_2

    .line 1333940
    const/4 v3, 0x1

    .line 1333941
    :cond_2
    return v3

    :cond_3
    move v1, v3

    goto :goto_1
.end method
