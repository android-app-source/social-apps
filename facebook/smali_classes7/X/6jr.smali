.class public LX/6jr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final threadDescription:Ljava/lang/String;

.field public final threadKey:LX/6l9;

.field public final timestamp:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1133185
    new-instance v0, LX/1sv;

    const-string v1, "DeltaGroupThreadDescription"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jr;->b:LX/1sv;

    .line 1133186
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jr;->c:LX/1sw;

    .line 1133187
    new-instance v0, LX/1sw;

    const-string v1, "timestamp"

    const/16 v2, 0xa

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jr;->d:LX/1sw;

    .line 1133188
    new-instance v0, LX/1sw;

    const-string v1, "threadDescription"

    const/16 v2, 0xb

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jr;->e:LX/1sw;

    .line 1133189
    sput-boolean v4, LX/6jr;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6l9;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1133280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1133281
    iput-object p1, p0, LX/6jr;->threadKey:LX/6l9;

    .line 1133282
    iput-object p2, p0, LX/6jr;->timestamp:Ljava/lang/Long;

    .line 1133283
    iput-object p3, p0, LX/6jr;->threadDescription:Ljava/lang/String;

    .line 1133284
    return-void
.end method

.method public static a(LX/6jr;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1133275
    iget-object v0, p0, LX/6jr;->threadKey:LX/6l9;

    if-nez v0, :cond_0

    .line 1133276
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'threadKey\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jr;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1133277
    :cond_0
    iget-object v0, p0, LX/6jr;->timestamp:Ljava/lang/Long;

    if-nez v0, :cond_1

    .line 1133278
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'timestamp\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jr;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1133279
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1133238
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1133239
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 1133240
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1133241
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaGroupThreadDescription"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1133242
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133243
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133244
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133245
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133246
    const-string v4, "threadKey"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133247
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133248
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133249
    iget-object v4, p0, LX/6jr;->threadKey:LX/6l9;

    if-nez v4, :cond_4

    .line 1133250
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133251
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133252
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133253
    const-string v4, "timestamp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133254
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133255
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133256
    iget-object v4, p0, LX/6jr;->timestamp:Ljava/lang/Long;

    if-nez v4, :cond_5

    .line 1133257
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133258
    :goto_4
    iget-object v4, p0, LX/6jr;->threadDescription:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 1133259
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133260
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133261
    const-string v4, "threadDescription"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133262
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133263
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133264
    iget-object v0, p0, LX/6jr;->threadDescription:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 1133265
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133266
    :cond_0
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133267
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133268
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1133269
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1133270
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1133271
    :cond_3
    const-string v0, ""

    goto/16 :goto_2

    .line 1133272
    :cond_4
    iget-object v4, p0, LX/6jr;->threadKey:LX/6l9;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1133273
    :cond_5
    iget-object v4, p0, LX/6jr;->timestamp:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1133274
    :cond_6
    iget-object v0, p0, LX/6jr;->threadDescription:Ljava/lang/String;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1133223
    invoke-static {p0}, LX/6jr;->a(LX/6jr;)V

    .line 1133224
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1133225
    iget-object v0, p0, LX/6jr;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1133226
    sget-object v0, LX/6jr;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133227
    iget-object v0, p0, LX/6jr;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1133228
    :cond_0
    iget-object v0, p0, LX/6jr;->timestamp:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1133229
    sget-object v0, LX/6jr;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133230
    iget-object v0, p0, LX/6jr;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1133231
    :cond_1
    iget-object v0, p0, LX/6jr;->threadDescription:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1133232
    iget-object v0, p0, LX/6jr;->threadDescription:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1133233
    sget-object v0, LX/6jr;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1133234
    iget-object v0, p0, LX/6jr;->threadDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1133235
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1133236
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1133237
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1133194
    if-nez p1, :cond_1

    .line 1133195
    :cond_0
    :goto_0
    return v0

    .line 1133196
    :cond_1
    instance-of v1, p1, LX/6jr;

    if-eqz v1, :cond_0

    .line 1133197
    check-cast p1, LX/6jr;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1133198
    if-nez p1, :cond_3

    .line 1133199
    :cond_2
    :goto_1
    move v0, v2

    .line 1133200
    goto :goto_0

    .line 1133201
    :cond_3
    iget-object v0, p0, LX/6jr;->threadKey:LX/6l9;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1133202
    :goto_2
    iget-object v3, p1, LX/6jr;->threadKey:LX/6l9;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1133203
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1133204
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133205
    iget-object v0, p0, LX/6jr;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6jr;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133206
    :cond_5
    iget-object v0, p0, LX/6jr;->timestamp:Ljava/lang/Long;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1133207
    :goto_4
    iget-object v3, p1, LX/6jr;->timestamp:Ljava/lang/Long;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1133208
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1133209
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133210
    iget-object v0, p0, LX/6jr;->timestamp:Ljava/lang/Long;

    iget-object v3, p1, LX/6jr;->timestamp:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133211
    :cond_7
    iget-object v0, p0, LX/6jr;->threadDescription:Ljava/lang/String;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1133212
    :goto_6
    iget-object v3, p1, LX/6jr;->threadDescription:Ljava/lang/String;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1133213
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1133214
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133215
    iget-object v0, p0, LX/6jr;->threadDescription:Ljava/lang/String;

    iget-object v3, p1, LX/6jr;->threadDescription:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 1133216
    goto :goto_1

    :cond_a
    move v0, v2

    .line 1133217
    goto :goto_2

    :cond_b
    move v3, v2

    .line 1133218
    goto :goto_3

    :cond_c
    move v0, v2

    .line 1133219
    goto :goto_4

    :cond_d
    move v3, v2

    .line 1133220
    goto :goto_5

    :cond_e
    move v0, v2

    .line 1133221
    goto :goto_6

    :cond_f
    move v3, v2

    .line 1133222
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1133193
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1133190
    sget-boolean v0, LX/6jr;->a:Z

    .line 1133191
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jr;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1133192
    return-object v0
.end method
