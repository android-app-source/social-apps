.class public final LX/6p0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QR",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/6p6;


# direct methods
.method public constructor <init>(LX/6p6;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1148740
    iput-object p1, p0, LX/6p0;->c:LX/6p6;

    iput-wide p2, p0, LX/6p0;->a:J

    iput-object p4, p0, LX/6p0;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1148741
    iget-object v0, p0, LX/6p0;->c:LX/6p6;

    iget-object v0, v0, LX/6p6;->i:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    iget-wide v2, p0, LX/6p0;->a:J

    iget-object v1, p0, LX/6p0;->b:Ljava/lang/String;

    .line 1148742
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1148743
    sget-object v5, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;->a:Ljava/lang/String;

    new-instance p0, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;

    invoke-direct {p0, v2, v3, v1}, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;-><init>(JLjava/lang/String;)V

    invoke-virtual {v4, v5, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1148744
    const-string v5, "check_payment_pin"

    invoke-static {v0, v4, v5}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1148745
    invoke-static {v0, v4}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 1148746
    return-object v0
.end method
