.class public LX/8NB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/upload/protocol/UploadPhotoParams;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1336256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1336257
    iput-object p1, p0, LX/8NB;->a:LX/0SG;

    .line 1336258
    return-void
.end method

.method public static a(LX/0QB;)LX/8NB;
    .locals 2

    .prologue
    .line 1336259
    new-instance v1, LX/8NB;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-direct {v1, v0}, LX/8NB;-><init>(LX/0SG;)V

    .line 1336260
    move-object v0, v1

    .line 1336261
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 14

    .prologue
    .line 1336262
    check-cast p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    const/4 v11, 0x1

    const/4 v10, 0x0

    const-wide/16 v8, 0x0

    .line 1336263
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1336264
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->p:Ljava/lang/String;

    move-object v1, v1

    .line 1336265
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1336266
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "qn"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336267
    :cond_0
    iget-wide v12, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->F:J

    move-wide v2, v12

    .line 1336268
    cmp-long v1, v2, v8

    if-eqz v1, :cond_1

    .line 1336269
    iget-object v1, p0, LX/8NB;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 1336270
    sub-long v2, v4, v2

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 1336271
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "time_since_original_post"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336272
    :cond_1
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "scaled_crop_rect"

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "x"

    invoke-virtual {v3, v4, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "y"

    invoke-virtual {v3, v4, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "width"

    invoke-virtual {v3, v4, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "height"

    invoke-virtual {v3, v4, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336273
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "profile_pic_method"

    .line 1336274
    iget-object v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->R:Ljava/lang/String;

    move-object v3, v3

    .line 1336275
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336276
    iget-wide v12, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->S:J

    move-wide v2, v12

    .line 1336277
    cmp-long v1, v2, v8

    if-eqz v1, :cond_2

    .line 1336278
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "expiration_time"

    .line 1336279
    iget-wide v12, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->S:J

    move-wide v4, v12

    .line 1336280
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336281
    :cond_2
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->T:Ljava/lang/String;

    move-object v1, v1

    .line 1336282
    if-eqz v1, :cond_3

    .line 1336283
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "sticker_id"

    .line 1336284
    iget-object v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->T:Ljava/lang/String;

    move-object v3, v3

    .line 1336285
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336286
    :cond_3
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->j:Ljava/lang/String;

    move-object v1, v1

    .line 1336287
    if-eqz v1, :cond_4

    .line 1336288
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "caption"

    .line 1336289
    iget-object v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->j:Ljava/lang/String;

    move-object v3, v3

    .line 1336290
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336291
    :cond_4
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->v:Lcom/facebook/share/model/ComposerAppAttribution;

    move-object v1, v1

    .line 1336292
    if-eqz v1, :cond_5

    .line 1336293
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "proxied_app_id"

    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336294
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "android_key_hash"

    invoke-virtual {v1}, Lcom/facebook/share/model/ComposerAppAttribution;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336295
    :cond_5
    iget-object v1, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->z:Ljava/lang/String;

    move-object v1, v1

    .line 1336296
    if-eqz v1, :cond_6

    .line 1336297
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "msqrd_mask_id"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336298
    :cond_6
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "has_umg"

    .line 1336299
    iget-boolean v3, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->U:Z

    move v3, v3

    .line 1336300
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1336301
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/picture/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1336302
    iget-wide v12, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    move-wide v2, v12

    .line 1336303
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1336304
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "publish-photo"

    .line 1336305
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 1336306
    move-object v2, v2

    .line 1336307
    const-string v3, "POST"

    .line 1336308
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 1336309
    move-object v2, v2

    .line 1336310
    iput-object v1, v2, LX/14O;->d:Ljava/lang/String;

    .line 1336311
    move-object v1, v2

    .line 1336312
    sget-object v2, LX/14S;->STRING:LX/14S;

    .line 1336313
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1336314
    move-object v1, v1

    .line 1336315
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1336316
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1336317
    move-object v0, v1

    .line 1336318
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1336319
    check-cast p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    .line 1336320
    iget-wide v2, p1, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->C:J

    move-wide v0, v2

    .line 1336321
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
