.class public final LX/8Xg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;)V
    .locals 0

    .prologue
    .line 1355287
    iput-object p1, p0, LX/8Xg;->a:Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 15

    .prologue
    .line 1355262
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 1355263
    iget-object v0, p0, LX/8Xg;->a:Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->b:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1355264
    :goto_0
    return-void

    .line 1355265
    :cond_0
    iget-object v0, p0, LX/8Xg;->a:Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;

    .line 1355266
    const/4 v14, 0x3

    const/4 v13, 0x1

    const/4 v12, 0x2

    const/4 v11, 0x0

    .line 1355267
    new-array v3, v14, [I

    sget-object v4, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->b:[I

    aget v4, v4, v11

    aput v4, v3, v11

    sget-object v4, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->b:[I

    aget v4, v4, v11

    aput v4, v3, v13

    sget-object v4, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->b:[I

    aget v4, v4, v11

    aput v4, v3, v12

    .line 1355268
    new-instance v4, Landroid/graphics/drawable/GradientDrawable;

    sget-object v5, Landroid/graphics/drawable/GradientDrawable$Orientation;->BL_TR:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-direct {v4, v5, v3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 1355269
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v5, v0, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->d:Landroid/animation/AnimatorSet;

    .line 1355270
    sget-object v5, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->b:[I

    invoke-static {v5}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v5

    .line 1355271
    new-instance v6, Landroid/animation/ArgbEvaluator;

    invoke-direct {v6}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 1355272
    sget-object v6, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->b:[I

    array-length v6, v6

    mul-int/lit16 v6, v6, 0x1f4

    int-to-long v7, v6

    invoke-virtual {v5, v7, v8}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1355273
    invoke-virtual {v5, v12}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 1355274
    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 1355275
    new-instance v6, LX/8Xh;

    invoke-direct {v6, v0, v4, v3}, LX/8Xh;-><init>(Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;Landroid/graphics/drawable/GradientDrawable;[I)V

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1355276
    invoke-virtual {v5}, Landroid/animation/ValueAnimator;->clone()Landroid/animation/ValueAnimator;

    move-result-object v6

    .line 1355277
    const-wide/16 v7, 0x1f4

    invoke-virtual {v6, v7, v8}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 1355278
    invoke-virtual {v6}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 1355279
    new-instance v7, LX/8Xi;

    invoke-direct {v7, v0, v3}, LX/8Xi;-><init>(Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;[I)V

    invoke-virtual {v6, v7}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1355280
    invoke-virtual {v5}, Landroid/animation/ValueAnimator;->clone()Landroid/animation/ValueAnimator;

    move-result-object v7

    .line 1355281
    const-wide/16 v9, 0x3e8

    invoke-virtual {v7, v9, v10}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 1355282
    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 1355283
    new-instance v8, LX/8Xj;

    invoke-direct {v8, v0, v3}, LX/8Xj;-><init>(Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;[I)V

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1355284
    iget-object v3, v0, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->d:Landroid/animation/AnimatorSet;

    new-array v8, v14, [Landroid/animation/Animator;

    aput-object v5, v8, v11

    aput-object v6, v8, v13

    aput-object v7, v8, v12

    invoke-virtual {v3, v8}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1355285
    invoke-virtual {v0, v4}, Lcom/facebook/quicksilver/views/loading/BaseQuicksilverLoadingView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1355286
    goto/16 :goto_0
.end method
