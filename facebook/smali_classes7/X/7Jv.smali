.class public LX/7Jv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:I

.field public final d:I

.field public final e:Z

.field public final f:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:LX/7QP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/04g;

.field public final i:LX/7DJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7Ju;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1194226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194227
    iget v0, p1, LX/7Ju;->c:I

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1194228
    iget v0, p1, LX/7Ju;->d:I

    if-ltz v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1194229
    iget-object v0, p1, LX/7Ju;->h:LX/04g;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1194230
    iget-boolean v0, p1, LX/7Ju;->a:Z

    iput-boolean v0, p0, LX/7Jv;->a:Z

    .line 1194231
    iget-object v0, p1, LX/7Ju;->f:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, LX/7Jv;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1194232
    iget-boolean v0, p1, LX/7Ju;->b:Z

    iput-boolean v0, p0, LX/7Jv;->b:Z

    .line 1194233
    iget v0, p1, LX/7Ju;->c:I

    iput v0, p0, LX/7Jv;->c:I

    .line 1194234
    iget v0, p1, LX/7Ju;->d:I

    iput v0, p0, LX/7Jv;->d:I

    .line 1194235
    iget-boolean v0, p1, LX/7Ju;->e:Z

    iput-boolean v0, p0, LX/7Jv;->e:Z

    .line 1194236
    iget-object v0, p1, LX/7Ju;->g:LX/7QP;

    iput-object v0, p0, LX/7Jv;->g:LX/7QP;

    .line 1194237
    iget-object v0, p1, LX/7Ju;->h:LX/04g;

    iput-object v0, p0, LX/7Jv;->h:LX/04g;

    .line 1194238
    iget-object v0, p1, LX/7Ju;->i:LX/7DJ;

    iput-object v0, p0, LX/7Jv;->i:LX/7DJ;

    .line 1194239
    return-void

    :cond_0
    move v0, v2

    .line 1194240
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1194241
    goto :goto_1
.end method
