.class public final LX/7ow;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:J

.field public e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Z

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:J

.field public l:J

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Z

.field public p:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1242374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;
    .locals 13

    .prologue
    .line 1242375
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1242376
    iget-object v1, p0, LX/7ow;->b:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1242377
    iget-object v2, p0, LX/7ow;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1242378
    iget-object v3, p0, LX/7ow;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1242379
    iget-object v3, p0, LX/7ow;->f:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1242380
    iget-object v3, p0, LX/7ow;->g:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1242381
    iget-object v3, p0, LX/7ow;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1242382
    iget-object v3, p0, LX/7ow;->m:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1242383
    iget-object v3, p0, LX/7ow;->n:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 1242384
    iget-object v3, p0, LX/7ow;->p:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    .line 1242385
    const/16 v3, 0x10

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1242386
    const/4 v3, 0x0

    iget-boolean v4, p0, LX/7ow;->a:Z

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1242387
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1242388
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1242389
    const/4 v1, 0x3

    iget-wide v2, p0, LX/7ow;->d:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1242390
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1242391
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1242392
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1242393
    const/4 v1, 0x7

    iget-boolean v2, p0, LX/7ow;->h:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1242394
    const/16 v1, 0x8

    iget-boolean v2, p0, LX/7ow;->i:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1242395
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1242396
    const/16 v1, 0xa

    iget-wide v2, p0, LX/7ow;->k:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1242397
    const/16 v1, 0xb

    iget-wide v2, p0, LX/7ow;->l:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1242398
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1242399
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1242400
    const/16 v1, 0xe

    iget-boolean v2, p0, LX/7ow;->o:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 1242401
    const/16 v1, 0xf

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 1242402
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1242403
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1242404
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1242405
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1242406
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1242407
    new-instance v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;

    invoke-direct {v1, v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel;-><init>(LX/15i;)V

    .line 1242408
    return-object v1
.end method
