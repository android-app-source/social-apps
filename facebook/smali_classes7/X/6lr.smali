.class public LX/6lr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "LX/6ls;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1142988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142989
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/6lr;->a:Ljava/util/WeakHashMap;

    .line 1142990
    return-void
.end method

.method public static a(LX/0QB;)LX/6lr;
    .locals 3

    .prologue
    .line 1142991
    const-class v1, LX/6lr;

    monitor-enter v1

    .line 1142992
    :try_start_0
    sget-object v0, LX/6lr;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1142993
    sput-object v2, LX/6lr;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1142994
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1142995
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1142996
    new-instance v0, LX/6lr;

    invoke-direct {v0}, LX/6lr;-><init>()V

    .line 1142997
    move-object v0, v0

    .line 1142998
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1142999
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6lr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1143000
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1143001
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
