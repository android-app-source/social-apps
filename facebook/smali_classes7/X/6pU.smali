.class public final LX/6pU;
.super LX/6nn;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6nn",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

.field public final synthetic b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/6pZ;


# direct methods
.method public constructor <init>(LX/6pZ;Lcom/facebook/payments/auth/pin/EnterPinFragment;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1149458
    iput-object p1, p0, LX/6pU;->d:LX/6pZ;

    iput-object p2, p0, LX/6pU;->a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    iput-object p3, p0, LX/6pU;->b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iput-object p4, p0, LX/6pU;->c:Ljava/lang/String;

    invoke-direct {p0}, LX/6nn;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1149445
    iget-object v0, p0, LX/6pU;->a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/EnterPinFragment;->d()V

    .line 1149446
    return-void
.end method

.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1149456
    iget-object v0, p0, LX/6pU;->b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iget-object v1, p0, LX/6pU;->a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(Lcom/facebook/fbservice/service/ServiceException;Lcom/facebook/payments/auth/pin/EnterPinFragment;Z)V

    .line 1149457
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1149447
    iget-object v0, p0, LX/6pU;->d:LX/6pZ;

    iget-object v0, v0, LX/6pZ;->c:LX/6o9;

    invoke-virtual {v0}, LX/6o9;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1149448
    iget-object v0, p0, LX/6pU;->d:LX/6pZ;

    iget-object v1, p0, LX/6pU;->b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iget-object v2, p0, LX/6pU;->a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    iget-object v3, p0, LX/6pU;->c:Ljava/lang/String;

    .line 1149449
    iget-object p0, v0, LX/6pZ;->b:LX/6p6;

    new-instance p1, LX/6pV;

    invoke-direct {p1, v0, v1, v2, v3}, LX/6pV;-><init>(LX/6pZ;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V

    .line 1149450
    new-instance v0, LX/6p2;

    invoke-direct {v0, p0}, LX/6p2;-><init>(LX/6p6;)V

    .line 1149451
    iget-object v1, p0, LX/6p6;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1149452
    sget-object v2, LX/6p5;->a:LX/6p5;

    move-object v2, v2

    .line 1149453
    invoke-static {p0, v1, v0, v2, p1}, LX/6p6;->a(LX/6p6;Lcom/google/common/util/concurrent/ListenableFuture;LX/0QR;LX/6p5;LX/6nn;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6p6;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1149454
    :goto_0
    return-void

    .line 1149455
    :cond_0
    iget-object v0, p0, LX/6pU;->b:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iget-object v1, p0, LX/6pU;->a:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    iget-object v2, p0, LX/6pU;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/6pZ;->f(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V

    goto :goto_0
.end method
