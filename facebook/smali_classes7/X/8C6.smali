.class public LX/8C6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2MA;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/8C6;


# instance fields
.field private final a:LX/1MZ;

.field private final b:LX/0kb;

.field private final c:LX/0Xl;

.field private final d:LX/2EL;

.field private final e:Ljava/util/concurrent/ScheduledExecutorService;

.field private f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/2MB;",
            ">;"
        }
    .end annotation
.end field

.field private g:Landroid/content/ContentResolver;

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/8Bk;",
            "LX/2MB;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/8Bk;",
            "LX/0am",
            "<",
            "LX/2MB;",
            ">;>;"
        }
    .end annotation
.end field

.field public final j:LX/8Bz;

.field private k:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1MZ;LX/0kb;LX/0Xl;LX/2EL;Ljava/util/concurrent/ScheduledExecutorService;Landroid/content/ContentResolver;LX/8Bz;)V
    .locals 3
    .param p3    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1310178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1310179
    iput-object p1, p0, LX/8C6;->a:LX/1MZ;

    .line 1310180
    iput-object p2, p0, LX/8C6;->b:LX/0kb;

    .line 1310181
    iput-object p3, p0, LX/8C6;->c:LX/0Xl;

    .line 1310182
    iput-object p4, p0, LX/8C6;->d:LX/2EL;

    .line 1310183
    iput-object p5, p0, LX/8C6;->e:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1310184
    iput-object p6, p0, LX/8C6;->g:Landroid/content/ContentResolver;

    .line 1310185
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8C6;->h:Ljava/util/Map;

    .line 1310186
    iget-object v0, p0, LX/8C6;->h:Ljava/util/Map;

    sget-object v1, LX/8Bk;->MQTT:LX/8Bk;

    sget-object v2, LX/2MB;->CONNECTED:LX/2MB;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1310187
    iget-object v0, p0, LX/8C6;->h:Ljava/util/Map;

    sget-object v1, LX/8Bk;->HTTP:LX/8Bk;

    sget-object v2, LX/2MB;->CONNECTED:LX/2MB;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1310188
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8C6;->i:Ljava/util/Map;

    .line 1310189
    iget-object v0, p0, LX/8C6;->i:Ljava/util/Map;

    sget-object v1, LX/8Bk;->MQTT:LX/8Bk;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1310190
    iget-object v0, p0, LX/8C6;->i:Ljava/util/Map;

    sget-object v1, LX/8Bk;->HTTP:LX/8Bk;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1310191
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/8C6;->f:LX/0am;

    .line 1310192
    iput-object p7, p0, LX/8C6;->j:LX/8Bz;

    .line 1310193
    return-void
.end method

.method public static a(LX/0QB;)LX/8C6;
    .locals 11

    .prologue
    .line 1310194
    sget-object v0, LX/8C6;->l:LX/8C6;

    if-nez v0, :cond_1

    .line 1310195
    const-class v1, LX/8C6;

    monitor-enter v1

    .line 1310196
    :try_start_0
    sget-object v0, LX/8C6;->l:LX/8C6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1310197
    if-eqz v2, :cond_0

    .line 1310198
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1310199
    new-instance v3, LX/8C6;

    invoke-static {v0}, LX/1MZ;->a(LX/0QB;)LX/1MZ;

    move-result-object v4

    check-cast v4, LX/1MZ;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v5

    check-cast v5, LX/0kb;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v6

    check-cast v6, LX/0Xl;

    invoke-static {v0}, LX/2EL;->a(LX/0QB;)LX/2EL;

    move-result-object v7

    check-cast v7, LX/2EL;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v9

    check-cast v9, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/8Bz;->a(LX/0QB;)LX/8Bz;

    move-result-object v10

    check-cast v10, LX/8Bz;

    invoke-direct/range {v3 .. v10}, LX/8C6;-><init>(LX/1MZ;LX/0kb;LX/0Xl;LX/2EL;Ljava/util/concurrent/ScheduledExecutorService;Landroid/content/ContentResolver;LX/8Bz;)V

    .line 1310200
    move-object v0, v3

    .line 1310201
    sput-object v0, LX/8C6;->l:LX/8C6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1310202
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1310203
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1310204
    :cond_1
    sget-object v0, LX/8C6;->l:LX/8C6;

    return-object v0

    .line 1310205
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1310206
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized d(LX/8Bk;)Z
    .locals 2

    .prologue
    .line 1310207
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8C6;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/2MB;->CONNECTED:LX/2MB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e(LX/8Bk;)Z
    .locals 2

    .prologue
    .line 1310208
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8C6;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8C6;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/2MB;->CONNECTED:LX/2MB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized g(LX/8C6;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1310209
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x200

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1310210
    const-string v0, "FbNetworkManager.activeNetwork: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1310211
    iget-object v0, p0, LX/8C6;->b:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->b()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 1310212
    const-string v2, "NetworkInfo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1310213
    if-nez v0, :cond_3

    .line 1310214
    const-string v2, "null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1310215
    :goto_0
    move-object v0, v1

    .line 1310216
    const-string v2, ", FbNetworkManager.isConnected: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/8C6;->b:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", ChannelConnectivityTracker.getConnectionState: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/8C6;->a:LX/1MZ;

    invoke-virtual {v2}, LX/1MZ;->b()LX/1Mb;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", NetChecker.getNetCheckState: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/8C6;->d:LX/2EL;

    .line 1310217
    iget-object v3, v2, LX/2EL;->k:LX/2EQ;

    move-object v2, v3

    .line 1310218
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", CurrentState (mqtt): "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/8C6;->h:Ljava/util/Map;

    sget-object v3, LX/8Bk;->MQTT:LX/8Bk;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", CurrentState (http): "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/8C6;->h:Ljava/util/Map;

    sget-object v3, LX/8Bk;->HTTP:LX/8Bk;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", PreviousState (mqtt): "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, LX/8C6;->i:Ljava/util/Map;

    sget-object v3, LX/8Bk;->MQTT:LX/8Bk;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8C6;->i:Ljava/util/Map;

    sget-object v3, LX/8Bk;->MQTT:LX/8Bk;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", PreviousState (http): "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, LX/8C6;->i:Ljava/util/Map;

    sget-object v3, LX/8Bk;->HTTP:LX/8Bk;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8C6;->i:Ljava/util/Map;

    sget-object v3, LX/8Bk;->HTTP:LX/8Bk;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", PreviousState: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, LX/8C6;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/8C6;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1310219
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 1310220
    :cond_0
    :try_start_1
    const-string v0, "n/a"

    goto :goto_1

    :cond_1
    const-string v0, "n/a"

    goto :goto_2

    :cond_2
    const-string v0, "n/a"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 1310221
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1310222
    :cond_3
    const-string v2, "type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isAvailable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isConnected: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isConnectedOrConnecting: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method public static declared-synchronized h(LX/8C6;)V
    .locals 5

    .prologue
    .line 1310223
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8C6;->k:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 1310224
    iget-object v0, p0, LX/8C6;->k:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 1310225
    :cond_0
    invoke-virtual {p0}, LX/8C6;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/8C6;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1310226
    new-instance v0, Lcom/facebook/messaging/connectivity/SimpleConnectionStatusMonitor$2;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/connectivity/SimpleConnectionStatusMonitor$2;-><init>(LX/8C6;)V

    .line 1310227
    iget-object v1, p0, LX/8C6;->e:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x1388

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v0, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/8C6;->k:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1310228
    :goto_0
    monitor-exit p0

    return-void

    .line 1310229
    :cond_1
    :try_start_1
    invoke-static {p0}, LX/8C6;->i(LX/8C6;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1310230
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized i(LX/8C6;)V
    .locals 7

    .prologue
    .line 1310151
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8C6;->h:Ljava/util/Map;

    sget-object v1, LX/8Bk;->MQTT:LX/8Bk;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2MB;

    .line 1310152
    iget-object v1, p0, LX/8C6;->h:Ljava/util/Map;

    sget-object v2, LX/8Bk;->HTTP:LX/8Bk;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2MB;

    .line 1310153
    iget-object v2, p0, LX/8C6;->b:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, LX/8C6;->a:LX/1MZ;

    invoke-virtual {v2}, LX/1MZ;->d()Z

    move-result v2

    if-nez v2, :cond_8

    .line 1310154
    sget-object v3, LX/2MB;->NO_INTERNET:LX/2MB;

    .line 1310155
    sget-object v2, LX/2MB;->NO_INTERNET:LX/2MB;

    move-object v4, v3

    move-object v3, v2

    .line 1310156
    :goto_0
    if-ne v4, v0, :cond_0

    iget-object v2, p0, LX/8C6;->i:Ljava/util/Map;

    sget-object v5, LX/8Bk;->MQTT:LX/8Bk;

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1310157
    :cond_0
    iget-object v2, p0, LX/8C6;->i:Ljava/util/Map;

    sget-object v5, LX/8Bk;->MQTT:LX/8Bk;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1310158
    :cond_1
    if-ne v3, v1, :cond_2

    iget-object v2, p0, LX/8C6;->i:Ljava/util/Map;

    sget-object v5, LX/8Bk;->HTTP:LX/8Bk;

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1310159
    :cond_2
    iget-object v2, p0, LX/8C6;->i:Ljava/util/Map;

    sget-object v5, LX/8Bk;->HTTP:LX/8Bk;

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1310160
    :cond_3
    if-ne v0, v4, :cond_4

    if-ne v3, v1, :cond_4

    iget-object v2, p0, LX/8C6;->f:LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1310161
    :cond_4
    invoke-virtual {p0}, LX/8C6;->c()Z

    move-result v2

    if-eqz v2, :cond_b

    sget-object v2, LX/2MB;->CONNECTED:LX/2MB;

    :goto_1
    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    iput-object v2, p0, LX/8C6;->f:LX/0am;

    .line 1310162
    :cond_5
    iget-object v2, p0, LX/8C6;->h:Ljava/util/Map;

    sget-object v5, LX/8Bk;->MQTT:LX/8Bk;

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1310163
    iget-object v2, p0, LX/8C6;->h:Ljava/util/Map;

    sget-object v5, LX/8Bk;->HTTP:LX/8Bk;

    invoke-interface {v2, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1310164
    if-ne v3, v1, :cond_6

    if-eq v4, v0, :cond_7

    .line 1310165
    :cond_6
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.orca.CONNECTIVITY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1310166
    iget-object v1, p0, LX/8C6;->c:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1310167
    :cond_7
    monitor-exit p0

    return-void

    .line 1310168
    :cond_8
    :try_start_1
    iget-object v2, p0, LX/8C6;->d:LX/2EL;

    .line 1310169
    iget-object v3, v2, LX/2EL;->k:LX/2EQ;

    move-object v2, v3

    .line 1310170
    sget-object v3, LX/2EQ;->CAPTIVE_PORTAL:LX/2EQ;

    if-ne v2, v3, :cond_9

    .line 1310171
    sget-object v2, LX/2MB;->CONNECTED_CAPTIVE_PORTAL:LX/2MB;

    .line 1310172
    :goto_2
    iget-object v3, p0, LX/8C6;->a:LX/1MZ;

    invoke-virtual {v3}, LX/1MZ;->d()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1310173
    sget-object v3, LX/2MB;->CONNECTED:LX/2MB;

    move-object v4, v3

    move-object v3, v2

    goto/16 :goto_0

    .line 1310174
    :cond_9
    sget-object v2, LX/2MB;->CONNECTED:LX/2MB;

    goto :goto_2

    .line 1310175
    :cond_a
    sget-object v3, LX/2MB;->NO_INTERNET:LX/2MB;

    move-object v4, v3

    move-object v3, v2

    goto/16 :goto_0

    .line 1310176
    :cond_b
    sget-object v2, LX/2MB;->NO_INTERNET:LX/2MB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1310177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()LX/2MB;
    .locals 2

    .prologue
    .line 1310231
    iget-object v0, p0, LX/8C6;->a:LX/1MZ;

    invoke-virtual {v0}, LX/1MZ;->b()LX/1Mb;

    move-result-object v0

    sget-object v1, LX/1Mb;->CONNECTED:LX/1Mb;

    if-eq v0, v1, :cond_0

    sget-object v0, LX/8Bk;->HTTP:LX/8Bk;

    invoke-virtual {p0, v0}, LX/8C6;->a(LX/8Bk;)LX/2MB;

    move-result-object v0

    sget-object v1, LX/2MB;->CONNECTED_CAPTIVE_PORTAL:LX/2MB;

    if-ne v0, v1, :cond_0

    .line 1310232
    sget-object v0, LX/2MB;->CONNECTED_CAPTIVE_PORTAL:LX/2MB;

    .line 1310233
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/8C6;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/2MB;->CONNECTED:LX/2MB;

    goto :goto_0

    :cond_1
    sget-object v0, LX/2MB;->NO_INTERNET:LX/2MB;

    goto :goto_0
.end method

.method public final declared-synchronized a(LX/8Bk;)LX/2MB;
    .locals 1

    .prologue
    .line 1310150
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8C6;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2MB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1310149
    iget-object v0, p0, LX/8C6;->a:LX/1MZ;

    invoke-virtual {v0}, LX/1MZ;->f()Z

    move-result v0

    return v0
.end method

.method public final b(LX/8Bk;)Z
    .locals 1

    .prologue
    .line 1310148
    invoke-direct {p0, p1}, LX/8C6;->d(LX/8Bk;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1310147
    sget-object v0, LX/8Bk;->MQTT:LX/8Bk;

    invoke-direct {p0, v0}, LX/8C6;->d(LX/8Bk;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/8Bk;->HTTP:LX/8Bk;

    invoke-direct {p0, v0}, LX/8C6;->d(LX/8Bk;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(LX/8Bk;)Z
    .locals 1

    .prologue
    .line 1310135
    invoke-direct {p0, p1}, LX/8C6;->e(LX/8Bk;)Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized d()Z
    .locals 2

    .prologue
    .line 1310146
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8C6;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8C6;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/2MB;->CONNECTED:LX/2MB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1310141
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-ge v2, v3, :cond_2

    .line 1310142
    iget-object v2, p0, LX/8C6;->g:Landroid/content/ContentResolver;

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    .line 1310143
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1310144
    goto :goto_0

    .line 1310145
    :cond_2
    iget-object v2, p0, LX/8C6;->g:Landroid/content/ContentResolver;

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 1310136
    invoke-static {p0}, LX/8C6;->i(LX/8C6;)V

    .line 1310137
    iget-object v0, p0, LX/8C6;->j:LX/8Bz;

    const-string v1, "init"

    invoke-static {p0}, LX/8C6;->g(LX/8C6;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/8Bz;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1310138
    new-instance v0, LX/8C5;

    invoke-direct {v0, p0}, LX/8C5;-><init>(LX/8C6;)V

    .line 1310139
    iget-object v1, p0, LX/8C6;->c:LX/0Xl;

    invoke-interface {v1}, LX/0Xl;->a()LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    const-string v2, "com.facebook.common.netchecker.ACTION_NETCHECK_STATE_CHANGED"

    invoke-interface {v1, v2, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 1310140
    return-void
.end method
