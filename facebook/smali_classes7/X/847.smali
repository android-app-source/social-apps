.class public final LX/847;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/2na;

.field public final synthetic c:LX/2hA;

.field public final synthetic d:LX/3UJ;


# direct methods
.method public constructor <init>(LX/3UJ;JLX/2na;LX/2hA;)V
    .locals 0

    .prologue
    .line 1290802
    iput-object p1, p0, LX/847;->d:LX/3UJ;

    iput-wide p2, p0, LX/847;->a:J

    iput-object p4, p0, LX/847;->b:LX/2na;

    iput-object p5, p0, LX/847;->c:LX/2hA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1290803
    iget-object v0, p0, LX/847;->d:LX/3UJ;

    iget-object v0, v0, LX/3UJ;->a:LX/2dj;

    iget-wide v2, p0, LX/847;->a:J

    iget-object v1, p0, LX/847;->b:LX/2na;

    iget-object v4, p0, LX/847;->c:LX/2hA;

    invoke-virtual {v0, v2, v3, v1, v4}, LX/2dj;->a(JLX/2na;LX/2hA;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
