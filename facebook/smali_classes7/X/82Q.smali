.class public LX/82Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1dp;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# instance fields
.field private final a:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "LX/1X9;",
            "LX/1dq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1288191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1288192
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, LX/1X9;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 1288193
    sget-object v1, LX/1X9;->TOP:LX/1X9;

    new-instance v2, LX/1dq;

    const v3, 0x7f020a73

    const v4, 0x7f020a6e

    const v5, 0x7f0219e6

    const v6, 0x7f0219e7

    invoke-direct {v2, v3, v4, v5, v6}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288194
    sget-object v1, LX/1X9;->DIVIDER_TOP:LX/1X9;

    new-instance v2, LX/1dq;

    const v3, 0x7f020a72

    const v4, 0x7f020a72

    const v5, 0x7f0219e7

    const v6, 0x7f0219e7

    invoke-direct {v2, v3, v4, v5, v6}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288195
    sget-object v1, LX/1X9;->DIVIDER_BOTTOM:LX/1X9;

    new-instance v2, LX/1dq;

    const v3, 0x7f020a62

    const v4, 0x7f020aed

    const v5, 0x7f0219e7

    const v6, 0x7f0219e7

    invoke-direct {v2, v3, v4, v5, v6}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288196
    sget-object v1, LX/1X9;->DIVIDER_BOTTOM_NON_TOP:LX/1X9;

    new-instance v2, LX/1dq;

    const v3, 0x7f020aed

    const v4, 0x7f020aed

    const v5, 0x7f0219e7

    const v6, 0x7f0219e7

    invoke-direct {v2, v3, v4, v5, v6}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288197
    sget-object v1, LX/1X9;->MIDDLE:LX/1X9;

    new-instance v2, LX/1dq;

    const v3, 0x7f020a70

    const v4, 0x7f020a6d

    const v5, 0x7f0219e7

    const v6, 0x7f0219e7

    invoke-direct {v2, v3, v4, v5, v6}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288198
    sget-object v1, LX/1X9;->BOX:LX/1X9;

    new-instance v2, LX/1dq;

    const v3, 0x7f020a66

    const v4, 0x7f020a6b

    const v5, 0x7f0219e7

    const v6, 0x7f0219e7

    invoke-direct {v2, v3, v4, v5, v6}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288199
    sget-object v1, LX/1X9;->BOTTOM:LX/1X9;

    new-instance v2, LX/1dq;

    const v3, 0x7f020a64

    const v4, 0x7f020a6a

    const v5, 0x7f0219e4

    const v6, 0x7f0219e7

    invoke-direct {v2, v3, v4, v5, v6}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288200
    sget-object v1, LX/1X9;->FOLLOW_UP:LX/1X9;

    new-instance v2, LX/1dq;

    const v3, 0x7f020a70

    const v4, 0x7f020a70

    const v5, 0x7f0219e7

    const v6, 0x7f0219e7

    invoke-direct {v2, v3, v4, v5, v6}, LX/1dq;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1288201
    move-object v0, v0

    .line 1288202
    iput-object v0, p0, LX/82Q;->a:Ljava/util/EnumMap;

    .line 1288203
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;LX/1X9;ILcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1288204
    iget-object v0, p0, LX/82Q;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dq;

    invoke-virtual {v0, p1, p3, p4}, LX/1dq;->a(Landroid/content/res/Resources;ILcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
