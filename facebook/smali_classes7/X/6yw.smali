.class public LX/6yw;
.super LX/6u5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6u5",
        "<",
        "Lcom/facebook/payments/paymentmethods/cardform/protocol/model/EditCreditCardParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1160612
    invoke-direct {p0, p1}, LX/6u5;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 1160613
    return-void
.end method

.method public static b(LX/0QB;)LX/6yw;
    .locals 2

    .prologue
    .line 1160610
    new-instance v1, LX/6yw;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v1, v0}, LX/6yw;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 1160611
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 2

    .prologue
    .line 1160592
    check-cast p1, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/EditCreditCardParams;

    .line 1160593
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "edit_credit_card"

    .line 1160594
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 1160595
    move-object v0, v0

    .line 1160596
    const-string v1, "POST"

    .line 1160597
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 1160598
    move-object v0, v0

    .line 1160599
    invoke-virtual {p1}, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->a()Ljava/util/List;

    move-result-object v1

    .line 1160600
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1160601
    move-object v0, v0

    .line 1160602
    iget-object v1, p1, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/EditCreditCardParams;->b:Ljava/lang/String;

    .line 1160603
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 1160604
    move-object v0, v0

    .line 1160605
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1160606
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1160607
    move-object v0, v0

    .line 1160608
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1160609
    const-string v0, "Edit_credit_card"

    return-object v0
.end method
