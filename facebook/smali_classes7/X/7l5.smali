.class public final LX/7l5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

.field public n:Z

.field public o:Z

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Z

.field public s:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1233289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1233290
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1233291
    iput-object v0, p0, LX/7l5;->k:LX/0Px;

    .line 1233292
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1233293
    iput-object v0, p0, LX/7l5;->l:LX/0Px;

    .line 1233294
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;)V
    .locals 1

    .prologue
    .line 1233268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1233269
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->composerSessionId:Ljava/lang/String;

    iput-object v0, p0, LX/7l5;->a:Ljava/lang/String;

    .line 1233270
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->userId:Ljava/lang/String;

    iput-object v0, p0, LX/7l5;->b:Ljava/lang/String;

    .line 1233271
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->iconId:Ljava/lang/String;

    iput-object v0, p0, LX/7l5;->c:Ljava/lang/String;

    .line 1233272
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->description:Ljava/lang/String;

    iput-object v0, p0, LX/7l5;->d:Ljava/lang/String;

    .line 1233273
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->story:Ljava/lang/String;

    iput-object v0, p0, LX/7l5;->e:Ljava/lang/String;

    .line 1233274
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->startDate:Ljava/lang/String;

    iput-object v0, p0, LX/7l5;->f:Ljava/lang/String;

    .line 1233275
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->endDate:Ljava/lang/String;

    iput-object v0, p0, LX/7l5;->g:Ljava/lang/String;

    .line 1233276
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->surface:Ljava/lang/String;

    iput-object v0, p0, LX/7l5;->h:Ljava/lang/String;

    .line 1233277
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->place:Ljava/lang/String;

    iput-object v0, p0, LX/7l5;->i:Ljava/lang/String;

    .line 1233278
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->privacy:Ljava/lang/String;

    iput-object v0, p0, LX/7l5;->j:Ljava/lang/String;

    .line 1233279
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->tags:LX/0Px;

    iput-object v0, p0, LX/7l5;->k:LX/0Px;

    .line 1233280
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->photoFbids:LX/0Px;

    iput-object v0, p0, LX/7l5;->l:LX/0Px;

    .line 1233281
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->lifeEventType:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    iput-object v0, p0, LX/7l5;->m:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 1233282
    iget-boolean v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->shouldUpdateRelationshipStatus:Z

    iput-boolean v0, p0, LX/7l5;->n:Z

    .line 1233283
    iget-boolean v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->isGraduated:Z

    iput-boolean v0, p0, LX/7l5;->o:Z

    .line 1233284
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->schoolType:Ljava/lang/String;

    iput-object v0, p0, LX/7l5;->p:Ljava/lang/String;

    .line 1233285
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->schoolHubId:Ljava/lang/String;

    iput-object v0, p0, LX/7l5;->q:Ljava/lang/String;

    .line 1233286
    iget-boolean v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->isCurrent:Z

    iput-boolean v0, p0, LX/7l5;->r:Z

    .line 1233287
    iget-object v0, p1, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->employerHubId:Ljava/lang/String;

    iput-object v0, p0, LX/7l5;->s:Ljava/lang/String;

    .line 1233288
    return-void
.end method

.method public static c(Lcom/facebook/uicontrib/datepicker/Date;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1233295
    if-nez p0, :cond_0

    .line 1233296
    const/4 v0, 0x0

    .line 1233297
    :goto_0
    return-object v0

    .line 1233298
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{\"year\":"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/Date;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1233299
    const-string v0, ",\"month\":"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/Date;->b()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "null"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1233300
    const-string v0, ",\"day\":"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/Date;->c()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "null"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1233301
    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1233302
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/Date;->b()Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1233303
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/Date;->c()Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;
    .locals 2

    .prologue
    .line 1233260
    iget-object v0, p0, LX/7l5;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1233261
    iget-object v0, p0, LX/7l5;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1233262
    iget-object v0, p0, LX/7l5;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1233263
    iget-object v0, p0, LX/7l5;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1233264
    iget-object v0, p0, LX/7l5;->h:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1233265
    iget-object v0, p0, LX/7l5;->j:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1233266
    iget-object v0, p0, LX/7l5;->m:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1233267
    new-instance v0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    invoke-direct {v0, p0}, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;-><init>(LX/7l5;)V

    return-object v0
.end method
