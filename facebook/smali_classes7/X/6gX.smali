.class public final LX/6gX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1125645
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1125646
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125647
    :goto_0
    return v1

    .line 1125648
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125649
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1125650
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1125651
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1125652
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1125653
    const-string v5, "filename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1125654
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1125655
    :cond_2
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1125656
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1125657
    :cond_3
    const-string v5, "uri"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1125658
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1125659
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1125660
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1125661
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1125662
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1125663
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 1125664
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1125665
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1125666
    if-eqz v0, :cond_0

    .line 1125667
    const-string v1, "filename"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125668
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1125669
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1125670
    if-eqz v0, :cond_1

    .line 1125671
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125672
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1125673
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1125674
    if-eqz v0, :cond_2

    .line 1125675
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125676
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1125677
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1125678
    return-void
.end method
