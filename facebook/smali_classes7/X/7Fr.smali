.class public final LX/7Fr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 1188471
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1188472
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1188473
    if-eqz v0, :cond_0

    .line 1188474
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188475
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1188476
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1188477
    if-eqz v0, :cond_1

    .line 1188478
    const-string v1, "__typename"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188479
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1188480
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1188481
    if-eqz v0, :cond_2

    .line 1188482
    const-string v1, "key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188483
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1188484
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1188485
    if-eqz v0, :cond_3

    .line 1188486
    const-string v1, "match_value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188487
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1188488
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1188489
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_4

    .line 1188490
    const-string v2, "max_value"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188491
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1188492
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1188493
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_5

    .line 1188494
    const-string v2, "min_value"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1188495
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1188496
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1188497
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1188498
    const/4 v9, 0x0

    .line 1188499
    const/4 v8, 0x0

    .line 1188500
    const/4 v7, 0x0

    .line 1188501
    const/4 v6, 0x0

    .line 1188502
    const-wide/16 v4, 0x0

    .line 1188503
    const-wide/16 v2, 0x0

    .line 1188504
    const/4 v1, 0x0

    .line 1188505
    const/4 v0, 0x0

    .line 1188506
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 1188507
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1188508
    const/4 v0, 0x0

    .line 1188509
    :goto_0
    return v0

    .line 1188510
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_8

    .line 1188511
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 1188512
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1188513
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 1188514
    const-string v4, "__type__"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "__typename"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1188515
    :cond_1
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v0

    move v12, v0

    goto :goto_1

    .line 1188516
    :cond_2
    const-string v4, "__typename"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1188517
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v11, v0

    goto :goto_1

    .line 1188518
    :cond_3
    const-string v4, "key"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1188519
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v10, v0

    goto :goto_1

    .line 1188520
    :cond_4
    const-string v4, "match_value"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1188521
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v7, v0

    goto :goto_1

    .line 1188522
    :cond_5
    const-string v4, "max_value"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1188523
    const/4 v0, 0x1

    .line 1188524
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 1188525
    :cond_6
    const-string v4, "min_value"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1188526
    const/4 v0, 0x1

    .line 1188527
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v6, v0

    move-wide v8, v4

    goto/16 :goto_1

    .line 1188528
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1188529
    :cond_8
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1188530
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 1188531
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1188532
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1188533
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1188534
    if-eqz v1, :cond_9

    .line 1188535
    const/4 v1, 0x4

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1188536
    :cond_9
    if-eqz v6, :cond_a

    .line 1188537
    const/4 v1, 0x5

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1188538
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_b
    move v10, v7

    move v11, v8

    move v12, v9

    move v7, v6

    move-wide v8, v2

    move v6, v0

    move-wide v2, v4

    goto/16 :goto_1
.end method
