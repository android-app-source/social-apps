.class public final LX/81s;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionAddVoteMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1286669
    const-class v1, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionAddVoteMutationModel;

    const v0, 0x1c6c40a5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "QuestionAddVoteMutation"

    const-string v6, "230939405a038d6e4099976cb831ea15"

    const-string v7, "question_add_vote"

    const-string v8, "0"

    const-string v9, "10155069968781729"

    const/4 v10, 0x0

    .line 1286670
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1286671
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1286672
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1286673
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1286674
    sparse-switch v0, :sswitch_data_0

    .line 1286675
    :goto_0
    return-object p1

    .line 1286676
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1286677
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1286678
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1286679
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3619b3d2 -> :sswitch_1
        -0x736e985 -> :sswitch_3
        0x5fb57ca -> :sswitch_0
        0x410878b1 -> :sswitch_2
    .end sparse-switch
.end method
