.class public final LX/7gs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Z

.field public d:Lcom/facebook/audience/model/AudienceControlData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/model/Reply;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/audience/model/Reply;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1225000
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225001
    const-string v0, ""

    iput-object v0, p0, LX/7gs;->a:Ljava/lang/String;

    .line 1225002
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1225003
    iput-object v0, p0, LX/7gs;->e:LX/0Px;

    .line 1225004
    return-void
.end method

.method public constructor <init>(Lcom/facebook/audience/model/ReplyThread;)V
    .locals 1

    .prologue
    .line 1224977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1224978
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1224979
    instance-of v0, p1, Lcom/facebook/audience/model/ReplyThread;

    if-eqz v0, :cond_0

    .line 1224980
    check-cast p1, Lcom/facebook/audience/model/ReplyThread;

    .line 1224981
    iget-object v0, p1, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    iput-object v0, p0, LX/7gs;->a:Ljava/lang/String;

    .line 1224982
    iget-boolean v0, p1, Lcom/facebook/audience/model/ReplyThread;->b:Z

    iput-boolean v0, p0, LX/7gs;->b:Z

    .line 1224983
    iget-boolean v0, p1, Lcom/facebook/audience/model/ReplyThread;->c:Z

    iput-boolean v0, p0, LX/7gs;->c:Z

    .line 1224984
    iget-object v0, p1, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    iput-object v0, p0, LX/7gs;->d:Lcom/facebook/audience/model/AudienceControlData;

    .line 1224985
    iget-object v0, p1, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    iput-object v0, p0, LX/7gs;->e:LX/0Px;

    .line 1224986
    iget-object v0, p1, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    iput-object v0, p0, LX/7gs;->f:Lcom/facebook/audience/model/Reply;

    .line 1224987
    :goto_0
    return-void

    .line 1224988
    :cond_0
    iget-object v0, p1, Lcom/facebook/audience/model/ReplyThread;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1224989
    iput-object v0, p0, LX/7gs;->a:Ljava/lang/String;

    .line 1224990
    iget-boolean v0, p1, Lcom/facebook/audience/model/ReplyThread;->b:Z

    move v0, v0

    .line 1224991
    iput-boolean v0, p0, LX/7gs;->b:Z

    .line 1224992
    iget-boolean v0, p1, Lcom/facebook/audience/model/ReplyThread;->c:Z

    move v0, v0

    .line 1224993
    iput-boolean v0, p0, LX/7gs;->c:Z

    .line 1224994
    iget-object v0, p1, Lcom/facebook/audience/model/ReplyThread;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v0, v0

    .line 1224995
    iput-object v0, p0, LX/7gs;->d:Lcom/facebook/audience/model/AudienceControlData;

    .line 1224996
    iget-object v0, p1, Lcom/facebook/audience/model/ReplyThread;->e:LX/0Px;

    move-object v0, v0

    .line 1224997
    iput-object v0, p0, LX/7gs;->e:LX/0Px;

    .line 1224998
    iget-object v0, p1, Lcom/facebook/audience/model/ReplyThread;->f:Lcom/facebook/audience/model/Reply;

    move-object v0, v0

    .line 1224999
    iput-object v0, p0, LX/7gs;->f:Lcom/facebook/audience/model/Reply;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/audience/model/ReplyThread;
    .locals 2

    .prologue
    .line 1224976
    new-instance v0, Lcom/facebook/audience/model/ReplyThread;

    invoke-direct {v0, p0}, Lcom/facebook/audience/model/ReplyThread;-><init>(LX/7gs;)V

    return-object v0
.end method
