.class public final LX/8XV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;)V
    .locals 0

    .prologue
    .line 1355092
    iput-object p1, p0, LX/8XV;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 5

    .prologue
    .line 1355093
    iget-object v0, p0, LX/8XV;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;

    const/4 p0, 0x0

    .line 1355094
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 1355095
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1811

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 1355096
    iget-object v3, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->f:LX/8Wb;

    iget-object v3, v3, LX/8Wb;->a:LX/8Wa;

    iget-object v3, v3, LX/8Wa;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    iget-object v4, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->d:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v3, v4

    mul-int/lit8 v4, v2, 0x2

    add-int/2addr v3, v4

    sub-int/2addr v1, v3

    .line 1355097
    iget-object v3, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->d:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v3

    .line 1355098
    iget-object v3, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverGLEndgameLeaderboardFragment;->k:Landroid/view/ViewGroup;

    invoke-virtual {v3, p0, v1, p0, v2}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 1355099
    return-void
.end method
