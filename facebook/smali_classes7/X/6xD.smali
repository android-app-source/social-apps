.class public LX/6xD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/CustomLinearLayout;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/CustomLinearLayout;)V
    .locals 0

    .prologue
    .line 1158545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158546
    iput-object p1, p0, LX/6xD;->a:Lcom/facebook/widget/CustomLinearLayout;

    .line 1158547
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 1158548
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, LX/6xD;->a:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1}, Lcom/facebook/widget/CustomLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, LX/6xD;->a:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, LX/6xD;->a([Landroid/view/View;)V

    .line 1158549
    return-void
.end method

.method public final varargs a([Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1158550
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1158551
    array-length v0, p1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 1158552
    iget-object v0, p0, LX/6xD;->a:Lcom/facebook/widget/CustomLinearLayout;

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    .line 1158553
    :goto_0
    return-void

    .line 1158554
    :cond_0
    iget-object v0, p0, LX/6xD;->a:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030686

    iget-object v3, p0, LX/6xD;->a:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v2, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    .line 1158555
    array-length v2, p1

    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v3, p1, v1

    .line 1158556
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, 0x0

    const/4 v6, -0x2

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v4, v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    move-object v4, v4

    .line 1158557
    invoke-virtual {v0, v3, v4}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1158558
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1158559
    :cond_1
    iget-object v1, p0, LX/6xD;->a:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
