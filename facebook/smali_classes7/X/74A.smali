.class public final enum LX/74A;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/74A;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/74A;

.field public static final enum FBUPLOAD_FILE_UPLOAD_FAILED:LX/74A;

.field public static final enum FBUPLOAD_FILE_UPLOAD_STARTED:LX/74A;

.field public static final enum FBUPLOAD_FILE_UPLOAD_SUCCEEDED:LX/74A;

.field public static final enum RESUMABLE_UPLOAD_FAILED:LX/74A;

.field public static final enum RESUMABLE_UPLOAD_SUCCEEDED:LX/74A;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1167545
    new-instance v0, LX/74A;

    const-string v1, "FBUPLOAD_FILE_UPLOAD_STARTED"

    const-string v2, "fbupload_file_upload_started"

    invoke-direct {v0, v1, v3, v2}, LX/74A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74A;->FBUPLOAD_FILE_UPLOAD_STARTED:LX/74A;

    .line 1167546
    new-instance v0, LX/74A;

    const-string v1, "FBUPLOAD_FILE_UPLOAD_SUCCEEDED"

    const-string v2, "fbupload_file_upload_succeeded"

    invoke-direct {v0, v1, v4, v2}, LX/74A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74A;->FBUPLOAD_FILE_UPLOAD_SUCCEEDED:LX/74A;

    .line 1167547
    new-instance v0, LX/74A;

    const-string v1, "FBUPLOAD_FILE_UPLOAD_FAILED"

    const-string v2, "fbupload_file_upload_failed"

    invoke-direct {v0, v1, v5, v2}, LX/74A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74A;->FBUPLOAD_FILE_UPLOAD_FAILED:LX/74A;

    .line 1167548
    new-instance v0, LX/74A;

    const-string v1, "RESUMABLE_UPLOAD_SUCCEEDED"

    const-string v2, "resumable_upload_succeeded"

    invoke-direct {v0, v1, v6, v2}, LX/74A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74A;->RESUMABLE_UPLOAD_SUCCEEDED:LX/74A;

    .line 1167549
    new-instance v0, LX/74A;

    const-string v1, "RESUMABLE_UPLOAD_FAILED"

    const-string v2, "resumable_upload_failed"

    invoke-direct {v0, v1, v7, v2}, LX/74A;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/74A;->RESUMABLE_UPLOAD_FAILED:LX/74A;

    .line 1167550
    const/4 v0, 0x5

    new-array v0, v0, [LX/74A;

    sget-object v1, LX/74A;->FBUPLOAD_FILE_UPLOAD_STARTED:LX/74A;

    aput-object v1, v0, v3

    sget-object v1, LX/74A;->FBUPLOAD_FILE_UPLOAD_SUCCEEDED:LX/74A;

    aput-object v1, v0, v4

    sget-object v1, LX/74A;->FBUPLOAD_FILE_UPLOAD_FAILED:LX/74A;

    aput-object v1, v0, v5

    sget-object v1, LX/74A;->RESUMABLE_UPLOAD_SUCCEEDED:LX/74A;

    aput-object v1, v0, v6

    sget-object v1, LX/74A;->RESUMABLE_UPLOAD_FAILED:LX/74A;

    aput-object v1, v0, v7

    sput-object v0, LX/74A;->$VALUES:[LX/74A;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1167551
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1167552
    iput-object p3, p0, LX/74A;->mName:Ljava/lang/String;

    .line 1167553
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/74A;
    .locals 1

    .prologue
    .line 1167554
    const-class v0, LX/74A;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/74A;

    return-object v0
.end method

.method public static values()[LX/74A;
    .locals 1

    .prologue
    .line 1167555
    sget-object v0, LX/74A;->$VALUES:[LX/74A;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/74A;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1167556
    iget-object v0, p0, LX/74A;->mName:Ljava/lang/String;

    return-object v0
.end method
