.class public LX/7Cp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:F

.field public b:F

.field public c:F

.field public d:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1181865
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1181866
    invoke-virtual {p0}, LX/7Cp;->a()V

    .line 1181867
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0

    .prologue
    .line 1181862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1181863
    invoke-virtual {p0, p1, p2, p3, p4}, LX/7Cp;->a(FFFF)V

    .line 1181864
    return-void
.end method

.method public constructor <init>([F)V
    .locals 0

    .prologue
    .line 1181859
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1181860
    invoke-virtual {p0, p1}, LX/7Cp;->a([F)V

    .line 1181861
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1181856
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/7Cp;->a:F

    .line 1181857
    const/4 v0, 0x0

    iput v0, p0, LX/7Cp;->d:F

    iput v0, p0, LX/7Cp;->c:F

    iput v0, p0, LX/7Cp;->b:F

    .line 1181858
    return-void
.end method

.method public final a(FFFF)V
    .locals 4

    .prologue
    .line 1181849
    const/high16 v0, 0x3f000000    # 0.5f

    mul-float/2addr v0, p1

    .line 1181850
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 1181851
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v0, v2

    iput v0, p0, LX/7Cp;->a:F

    .line 1181852
    mul-float v0, p2, v1

    iput v0, p0, LX/7Cp;->b:F

    .line 1181853
    mul-float v0, p3, v1

    iput v0, p0, LX/7Cp;->c:F

    .line 1181854
    mul-float v0, p4, v1

    iput v0, p0, LX/7Cp;->d:F

    .line 1181855
    return-void
.end method

.method public final a(LX/7Cp;)V
    .locals 1

    .prologue
    .line 1181760
    iget v0, p1, LX/7Cp;->b:F

    iput v0, p0, LX/7Cp;->b:F

    .line 1181761
    iget v0, p1, LX/7Cp;->c:F

    iput v0, p0, LX/7Cp;->c:F

    .line 1181762
    iget v0, p1, LX/7Cp;->d:F

    iput v0, p0, LX/7Cp;->d:F

    .line 1181763
    iget v0, p1, LX/7Cp;->a:F

    iput v0, p0, LX/7Cp;->a:F

    .line 1181764
    return-void
.end method

.method public final a(LX/7Cp;LX/7Cp;F)V
    .locals 10

    .prologue
    .line 1181824
    iget v0, p1, LX/7Cp;->a:F

    iget v1, p2, LX/7Cp;->a:F

    mul-float/2addr v0, v1

    iget v1, p1, LX/7Cp;->b:F

    iget v2, p2, LX/7Cp;->b:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p1, LX/7Cp;->c:F

    iget v2, p2, LX/7Cp;->c:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p1, LX/7Cp;->d:F

    iget v2, p2, LX/7Cp;->d:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    move v0, v0

    .line 1181825
    float-to-double v0, v0

    .line 1181826
    const/4 v2, 0x4

    new-array v4, v2, [F

    .line 1181827
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    .line 1181828
    neg-double v0, v0

    .line 1181829
    const/4 v2, 0x0

    iget v3, p2, LX/7Cp;->a:F

    neg-float v3, v3

    aput v3, v4, v2

    .line 1181830
    const/4 v2, 0x1

    iget v3, p2, LX/7Cp;->b:F

    neg-float v3, v3

    aput v3, v4, v2

    .line 1181831
    const/4 v2, 0x2

    iget v3, p2, LX/7Cp;->c:F

    neg-float v3, v3

    aput v3, v4, v2

    .line 1181832
    const/4 v2, 0x3

    iget v3, p2, LX/7Cp;->d:F

    neg-float v3, v3

    aput v3, v4, v2

    .line 1181833
    :goto_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, v0

    const-wide v6, 0x3f847ae147ae147bL    # 0.01

    cmpl-double v2, v2, v6

    if-lez v2, :cond_1

    .line 1181834
    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v0

    double-to-float v0, v0

    float-to-double v0, v0

    .line 1181835
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    .line 1181836
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    float-to-double v8, p3

    sub-double/2addr v2, v8

    mul-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    div-double/2addr v2, v6

    .line 1181837
    float-to-double v8, p3

    mul-double/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    div-double/2addr v0, v6

    .line 1181838
    :goto_1
    iget v5, p1, LX/7Cp;->a:F

    float-to-double v6, v5

    mul-double/2addr v6, v2

    const/4 v5, 0x0

    aget v5, v4, v5

    float-to-double v8, v5

    mul-double/2addr v8, v0

    add-double/2addr v6, v8

    double-to-float v5, v6

    iput v5, p0, LX/7Cp;->a:F

    .line 1181839
    iget v5, p1, LX/7Cp;->b:F

    float-to-double v6, v5

    mul-double/2addr v6, v2

    const/4 v5, 0x1

    aget v5, v4, v5

    float-to-double v8, v5

    mul-double/2addr v8, v0

    add-double/2addr v6, v8

    double-to-float v5, v6

    iput v5, p0, LX/7Cp;->b:F

    .line 1181840
    iget v5, p1, LX/7Cp;->c:F

    float-to-double v6, v5

    mul-double/2addr v6, v2

    const/4 v5, 0x2

    aget v5, v4, v5

    float-to-double v8, v5

    mul-double/2addr v8, v0

    add-double/2addr v6, v8

    double-to-float v5, v6

    iput v5, p0, LX/7Cp;->c:F

    .line 1181841
    iget v5, p1, LX/7Cp;->d:F

    float-to-double v6, v5

    mul-double/2addr v2, v6

    const/4 v5, 0x3

    aget v4, v4, v5

    float-to-double v4, v4

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, LX/7Cp;->d:F

    .line 1181842
    return-void

    .line 1181843
    :cond_0
    const/4 v2, 0x0

    iget v3, p2, LX/7Cp;->a:F

    aput v3, v4, v2

    .line 1181844
    const/4 v2, 0x1

    iget v3, p2, LX/7Cp;->b:F

    aput v3, v4, v2

    .line 1181845
    const/4 v2, 0x2

    iget v3, p2, LX/7Cp;->c:F

    aput v3, v4, v2

    .line 1181846
    const/4 v2, 0x3

    iget v3, p2, LX/7Cp;->d:F

    aput v3, v4, v2

    goto :goto_0

    .line 1181847
    :cond_1
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    float-to-double v2, p3

    sub-double v2, v0, v2

    .line 1181848
    float-to-double v0, p3

    goto :goto_1
.end method

.method public final a([F)V
    .locals 1

    .prologue
    .line 1181819
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, LX/7Cp;->a:F

    .line 1181820
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, LX/7Cp;->b:F

    .line 1181821
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, LX/7Cp;->c:F

    .line 1181822
    const/4 v0, 0x3

    aget v0, p1, v0

    iput v0, p0, LX/7Cp;->d:F

    .line 1181823
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1181815
    iget v0, p0, LX/7Cp;->b:F

    neg-float v0, v0

    iput v0, p0, LX/7Cp;->b:F

    .line 1181816
    iget v0, p0, LX/7Cp;->c:F

    neg-float v0, v0

    iput v0, p0, LX/7Cp;->c:F

    .line 1181817
    iget v0, p0, LX/7Cp;->d:F

    neg-float v0, v0

    iput v0, p0, LX/7Cp;->d:F

    .line 1181818
    return-void
.end method

.method public final b(LX/7Cp;)V
    .locals 6

    .prologue
    .line 1181806
    iget v0, p1, LX/7Cp;->a:F

    iget v1, p0, LX/7Cp;->a:F

    mul-float/2addr v0, v1

    iget v1, p1, LX/7Cp;->b:F

    iget v2, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p1, LX/7Cp;->c:F

    iget v2, p0, LX/7Cp;->c:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p1, LX/7Cp;->d:F

    iget v2, p0, LX/7Cp;->d:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 1181807
    iget v1, p1, LX/7Cp;->a:F

    iget v2, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v2

    iget v2, p1, LX/7Cp;->b:F

    iget v3, p0, LX/7Cp;->a:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p1, LX/7Cp;->c:F

    iget v3, p0, LX/7Cp;->d:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p1, LX/7Cp;->d:F

    iget v3, p0, LX/7Cp;->c:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    .line 1181808
    iget v2, p1, LX/7Cp;->a:F

    iget v3, p0, LX/7Cp;->c:F

    mul-float/2addr v2, v3

    iget v3, p1, LX/7Cp;->b:F

    iget v4, p0, LX/7Cp;->d:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p1, LX/7Cp;->c:F

    iget v4, p0, LX/7Cp;->a:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p1, LX/7Cp;->d:F

    iget v4, p0, LX/7Cp;->b:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 1181809
    iget v3, p1, LX/7Cp;->a:F

    iget v4, p0, LX/7Cp;->d:F

    mul-float/2addr v3, v4

    iget v4, p1, LX/7Cp;->b:F

    iget v5, p0, LX/7Cp;->c:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, LX/7Cp;->c:F

    iget v5, p0, LX/7Cp;->b:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p1, LX/7Cp;->d:F

    iget v5, p0, LX/7Cp;->a:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    .line 1181810
    iput v0, p0, LX/7Cp;->a:F

    .line 1181811
    iput v1, p0, LX/7Cp;->b:F

    .line 1181812
    iput v2, p0, LX/7Cp;->c:F

    .line 1181813
    iput v3, p0, LX/7Cp;->d:F

    .line 1181814
    return-void
.end method

.method public final b([F)V
    .locals 10

    .prologue
    const/high16 v9, -0x40000000    # -2.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/high16 v7, 0x40000000    # 2.0f

    .line 1181798
    iget v0, p0, LX/7Cp;->a:F

    iget v1, p0, LX/7Cp;->b:F

    mul-float/2addr v0, v1

    iget v1, p0, LX/7Cp;->c:F

    iget v2, p0, LX/7Cp;->d:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 1181799
    mul-float/2addr v0, v7

    .line 1181800
    const/high16 v1, -0x40800000    # -1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1181801
    invoke-static {v0, v8}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1181802
    const/4 v1, 0x0

    iget v2, p0, LX/7Cp;->b:F

    mul-float/2addr v2, v9

    iget v3, p0, LX/7Cp;->c:F

    mul-float/2addr v2, v3

    iget v3, p0, LX/7Cp;->a:F

    mul-float/2addr v3, v7

    iget v4, p0, LX/7Cp;->d:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    iget v4, p0, LX/7Cp;->b:F

    mul-float/2addr v4, v7

    iget v5, p0, LX/7Cp;->b:F

    mul-float/2addr v4, v5

    sub-float v4, v8, v4

    iget v5, p0, LX/7Cp;->d:F

    mul-float/2addr v5, v7

    iget v6, p0, LX/7Cp;->d:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    double-to-float v2, v2

    aput v2, p1, v1

    .line 1181803
    const/4 v1, 0x1

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->asin(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    double-to-float v0, v2

    aput v0, p1, v1

    .line 1181804
    const/4 v0, 0x2

    iget v1, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v9

    iget v2, p0, LX/7Cp;->d:F

    mul-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->a:F

    mul-float/2addr v2, v7

    iget v3, p0, LX/7Cp;->c:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-double v2, v1

    iget v1, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v7

    iget v4, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v4

    sub-float v1, v8, v1

    iget v4, p0, LX/7Cp;->c:F

    mul-float/2addr v4, v7

    iget v5, p0, LX/7Cp;->c:F

    mul-float/2addr v4, v5

    sub-float/2addr v1, v4

    float-to-double v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    double-to-float v1, v2

    aput v1, p1, v0

    .line 1181805
    return-void
.end method

.method public final c(LX/7Cp;)V
    .locals 6

    .prologue
    .line 1181789
    iget v0, p0, LX/7Cp;->a:F

    iget v1, p1, LX/7Cp;->a:F

    mul-float/2addr v0, v1

    iget v1, p0, LX/7Cp;->b:F

    iget v2, p1, LX/7Cp;->b:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, LX/7Cp;->c:F

    iget v2, p1, LX/7Cp;->c:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, LX/7Cp;->d:F

    iget v2, p1, LX/7Cp;->d:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 1181790
    iget v1, p0, LX/7Cp;->a:F

    iget v2, p1, LX/7Cp;->b:F

    mul-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->b:F

    iget v3, p1, LX/7Cp;->a:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->c:F

    iget v3, p1, LX/7Cp;->d:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->d:F

    iget v3, p1, LX/7Cp;->c:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    .line 1181791
    iget v2, p0, LX/7Cp;->a:F

    iget v3, p1, LX/7Cp;->c:F

    mul-float/2addr v2, v3

    iget v3, p0, LX/7Cp;->b:F

    iget v4, p1, LX/7Cp;->d:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, LX/7Cp;->c:F

    iget v4, p1, LX/7Cp;->a:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, LX/7Cp;->d:F

    iget v4, p1, LX/7Cp;->b:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 1181792
    iget v3, p0, LX/7Cp;->a:F

    iget v4, p1, LX/7Cp;->d:F

    mul-float/2addr v3, v4

    iget v4, p0, LX/7Cp;->b:F

    iget v5, p1, LX/7Cp;->c:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, LX/7Cp;->c:F

    iget v5, p1, LX/7Cp;->b:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, LX/7Cp;->d:F

    iget v5, p1, LX/7Cp;->a:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    .line 1181793
    iput v0, p0, LX/7Cp;->a:F

    .line 1181794
    iput v1, p0, LX/7Cp;->b:F

    .line 1181795
    iput v2, p0, LX/7Cp;->c:F

    .line 1181796
    iput v3, p0, LX/7Cp;->d:F

    .line 1181797
    return-void
.end method

.method public final c([F)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    .line 1181772
    const/4 v0, 0x0

    iget v1, p0, LX/7Cp;->c:F

    mul-float/2addr v1, v4

    iget v2, p0, LX/7Cp;->c:F

    mul-float/2addr v1, v2

    sub-float v1, v6, v1

    iget v2, p0, LX/7Cp;->d:F

    mul-float/2addr v2, v4

    iget v3, p0, LX/7Cp;->d:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    aput v1, p1, v0

    .line 1181773
    const/4 v0, 0x1

    iget v1, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v4

    iget v2, p0, LX/7Cp;->c:F

    mul-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->a:F

    mul-float/2addr v2, v4

    iget v3, p0, LX/7Cp;->d:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    aput v1, p1, v0

    .line 1181774
    const/4 v0, 0x2

    iget v1, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v4

    iget v2, p0, LX/7Cp;->d:F

    mul-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->a:F

    mul-float/2addr v2, v4

    iget v3, p0, LX/7Cp;->c:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    aput v1, p1, v0

    .line 1181775
    const/4 v0, 0x3

    aput v5, p1, v0

    .line 1181776
    const/4 v0, 0x4

    iget v1, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v4

    iget v2, p0, LX/7Cp;->c:F

    mul-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->a:F

    mul-float/2addr v2, v4

    iget v3, p0, LX/7Cp;->d:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    aput v1, p1, v0

    .line 1181777
    const/4 v0, 0x5

    iget v1, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v4

    iget v2, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v2

    sub-float v1, v6, v1

    iget v2, p0, LX/7Cp;->d:F

    mul-float/2addr v2, v4

    iget v3, p0, LX/7Cp;->d:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    aput v1, p1, v0

    .line 1181778
    const/4 v0, 0x6

    iget v1, p0, LX/7Cp;->c:F

    mul-float/2addr v1, v4

    iget v2, p0, LX/7Cp;->d:F

    mul-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->a:F

    mul-float/2addr v2, v4

    iget v3, p0, LX/7Cp;->b:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    aput v1, p1, v0

    .line 1181779
    const/4 v0, 0x7

    aput v5, p1, v0

    .line 1181780
    const/16 v0, 0x8

    iget v1, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v4

    iget v2, p0, LX/7Cp;->d:F

    mul-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->a:F

    mul-float/2addr v2, v4

    iget v3, p0, LX/7Cp;->c:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    aput v1, p1, v0

    .line 1181781
    const/16 v0, 0x9

    iget v1, p0, LX/7Cp;->c:F

    mul-float/2addr v1, v4

    iget v2, p0, LX/7Cp;->d:F

    mul-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->a:F

    mul-float/2addr v2, v4

    iget v3, p0, LX/7Cp;->b:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    aput v1, p1, v0

    .line 1181782
    const/16 v0, 0xa

    iget v1, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v4

    iget v2, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v2

    sub-float v1, v6, v1

    iget v2, p0, LX/7Cp;->c:F

    mul-float/2addr v2, v4

    iget v3, p0, LX/7Cp;->c:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    aput v1, p1, v0

    .line 1181783
    const/16 v0, 0xb

    aput v5, p1, v0

    .line 1181784
    const/16 v0, 0xc

    aput v5, p1, v0

    .line 1181785
    const/16 v0, 0xd

    aput v5, p1, v0

    .line 1181786
    const/16 v0, 0xe

    aput v5, p1, v0

    .line 1181787
    const/16 v0, 0xf

    aput v6, p1, v0

    .line 1181788
    return-void
.end method

.method public final d([F)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    .line 1181765
    iget v0, p0, LX/7Cp;->a:F

    iget v1, p0, LX/7Cp;->a:F

    mul-float/2addr v0, v1

    aget v1, p1, v6

    mul-float/2addr v0, v1

    iget v1, p0, LX/7Cp;->c:F

    mul-float/2addr v1, v5

    iget v2, p0, LX/7Cp;->a:F

    mul-float/2addr v1, v2

    aget v2, p1, v8

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, LX/7Cp;->d:F

    mul-float/2addr v1, v5

    iget v2, p0, LX/7Cp;->a:F

    mul-float/2addr v1, v2

    aget v2, p1, v7

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, LX/7Cp;->b:F

    iget v2, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v2

    aget v2, p1, v6

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, LX/7Cp;->c:F

    mul-float/2addr v1, v5

    iget v2, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v2

    aget v2, p1, v7

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, LX/7Cp;->d:F

    mul-float/2addr v1, v5

    iget v2, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v2

    aget v2, p1, v8

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, LX/7Cp;->d:F

    iget v2, p0, LX/7Cp;->d:F

    mul-float/2addr v1, v2

    aget v2, p1, v6

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, LX/7Cp;->c:F

    iget v2, p0, LX/7Cp;->c:F

    mul-float/2addr v1, v2

    aget v2, p1, v6

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 1181766
    iget v1, p0, LX/7Cp;->b:F

    mul-float/2addr v1, v5

    iget v2, p0, LX/7Cp;->c:F

    mul-float/2addr v1, v2

    aget v2, p1, v6

    mul-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->c:F

    iget v3, p0, LX/7Cp;->c:F

    mul-float/2addr v2, v3

    aget v3, p1, v7

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->d:F

    mul-float/2addr v2, v5

    iget v3, p0, LX/7Cp;->c:F

    mul-float/2addr v2, v3

    aget v3, p1, v8

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->a:F

    mul-float/2addr v2, v5

    iget v3, p0, LX/7Cp;->d:F

    mul-float/2addr v2, v3

    aget v3, p1, v6

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->d:F

    iget v3, p0, LX/7Cp;->d:F

    mul-float/2addr v2, v3

    aget v3, p1, v7

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->a:F

    iget v3, p0, LX/7Cp;->a:F

    mul-float/2addr v2, v3

    aget v3, p1, v7

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->b:F

    mul-float/2addr v2, v5

    iget v3, p0, LX/7Cp;->a:F

    mul-float/2addr v2, v3

    aget v3, p1, v8

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, LX/7Cp;->b:F

    iget v3, p0, LX/7Cp;->b:F

    mul-float/2addr v2, v3

    aget v3, p1, v7

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    .line 1181767
    iget v2, p0, LX/7Cp;->b:F

    mul-float/2addr v2, v5

    iget v3, p0, LX/7Cp;->d:F

    mul-float/2addr v2, v3

    aget v3, p1, v6

    mul-float/2addr v2, v3

    iget v3, p0, LX/7Cp;->c:F

    mul-float/2addr v3, v5

    iget v4, p0, LX/7Cp;->d:F

    mul-float/2addr v3, v4

    aget v4, p1, v7

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, LX/7Cp;->d:F

    iget v4, p0, LX/7Cp;->d:F

    mul-float/2addr v3, v4

    aget v4, p1, v8

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, LX/7Cp;->a:F

    mul-float/2addr v3, v5

    iget v4, p0, LX/7Cp;->c:F

    mul-float/2addr v3, v4

    aget v4, p1, v6

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, LX/7Cp;->c:F

    iget v4, p0, LX/7Cp;->c:F

    mul-float/2addr v3, v4

    aget v4, p1, v8

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, LX/7Cp;->a:F

    mul-float/2addr v3, v5

    iget v4, p0, LX/7Cp;->b:F

    mul-float/2addr v3, v4

    aget v4, p1, v7

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, LX/7Cp;->b:F

    iget v4, p0, LX/7Cp;->b:F

    mul-float/2addr v3, v4

    aget v4, p1, v8

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, LX/7Cp;->a:F

    iget v4, p0, LX/7Cp;->a:F

    mul-float/2addr v3, v4

    aget v4, p1, v8

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 1181768
    aput v0, p1, v6

    .line 1181769
    aput v1, p1, v7

    .line 1181770
    aput v2, p1, v8

    .line 1181771
    return-void
.end method
