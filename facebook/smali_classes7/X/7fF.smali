.class public final enum LX/7fF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7fF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7fF;

.field public static final enum CAPTURING:LX/7fF;

.field public static final enum STOPPED:LX/7fF;

.field public static final enum WAITING_FOR_SURFACE:LX/7fF;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1221161
    new-instance v0, LX/7fF;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v2}, LX/7fF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7fF;->STOPPED:LX/7fF;

    .line 1221162
    new-instance v0, LX/7fF;

    const-string v1, "WAITING_FOR_SURFACE"

    invoke-direct {v0, v1, v3}, LX/7fF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7fF;->WAITING_FOR_SURFACE:LX/7fF;

    .line 1221163
    new-instance v0, LX/7fF;

    const-string v1, "CAPTURING"

    invoke-direct {v0, v1, v4}, LX/7fF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7fF;->CAPTURING:LX/7fF;

    .line 1221164
    const/4 v0, 0x3

    new-array v0, v0, [LX/7fF;

    sget-object v1, LX/7fF;->STOPPED:LX/7fF;

    aput-object v1, v0, v2

    sget-object v1, LX/7fF;->WAITING_FOR_SURFACE:LX/7fF;

    aput-object v1, v0, v3

    sget-object v1, LX/7fF;->CAPTURING:LX/7fF;

    aput-object v1, v0, v4

    sput-object v0, LX/7fF;->$VALUES:[LX/7fF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1221165
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7fF;
    .locals 1

    .prologue
    .line 1221166
    const-class v0, LX/7fF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7fF;

    return-object v0
.end method

.method public static values()[LX/7fF;
    .locals 1

    .prologue
    .line 1221167
    sget-object v0, LX/7fF;->$VALUES:[LX/7fF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7fF;

    return-object v0
.end method
