.class public LX/87c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MT;


# instance fields
.field private final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1300460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300461
    iput-object p1, p0, LX/87c;->a:LX/0Uh;

    .line 1300462
    return-void
.end method


# virtual methods
.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1300463
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1300464
    iget-object v1, p0, LX/87c;->a:LX/0Uh;

    invoke-virtual {v1}, LX/0Uh;->a()Ljava/util/SortedMap;

    move-result-object v1

    .line 1300465
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1300466
    const-string v2, "gatekeeper_pairs.txt"

    invoke-static {p1, v2, v1}, LX/4l9;->a(Ljava/io/File;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1300467
    :cond_0
    return-object v0
.end method
