.class public final enum LX/6fR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6fR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6fR;

.field public static final enum PAYMENT:LX/6fR;

.field public static final enum SHARE:LX/6fR;

.field private static final VALUE_MAP:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/6fR;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final DBSerialValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1119907
    new-instance v1, LX/6fR;

    const-string v2, "SHARE"

    const-string v3, "share"

    invoke-direct {v1, v2, v0, v3}, LX/6fR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/6fR;->SHARE:LX/6fR;

    .line 1119908
    new-instance v1, LX/6fR;

    const-string v2, "PAYMENT"

    const-string v3, "payment"

    invoke-direct {v1, v2, v4, v3}, LX/6fR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/6fR;->PAYMENT:LX/6fR;

    .line 1119909
    const/4 v1, 0x2

    new-array v1, v1, [LX/6fR;

    sget-object v2, LX/6fR;->SHARE:LX/6fR;

    aput-object v2, v1, v0

    sget-object v2, LX/6fR;->PAYMENT:LX/6fR;

    aput-object v2, v1, v4

    sput-object v1, LX/6fR;->$VALUES:[LX/6fR;

    .line 1119910
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1119911
    invoke-static {}, LX/6fR;->values()[LX/6fR;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1119912
    iget-object v5, v4, LX/6fR;->DBSerialValue:Ljava/lang/String;

    invoke-virtual {v1, v5, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1119913
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1119914
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/6fR;->VALUE_MAP:LX/0P1;

    .line 1119915
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1119904
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1119905
    iput-object p3, p0, LX/6fR;->DBSerialValue:Ljava/lang/String;

    .line 1119906
    return-void
.end method

.method public static fromDBSerialValue(Ljava/lang/String;)LX/6fR;
    .locals 3

    .prologue
    .line 1119899
    sget-object v0, LX/6fR;->VALUE_MAP:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1119900
    sget-object v0, LX/6fR;->VALUE_MAP:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6fR;

    return-object v0

    .line 1119901
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported Type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6fR;
    .locals 1

    .prologue
    .line 1119903
    const-class v0, LX/6fR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6fR;

    return-object v0
.end method

.method public static values()[LX/6fR;
    .locals 1

    .prologue
    .line 1119902
    sget-object v0, LX/6fR;->$VALUES:[LX/6fR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6fR;

    return-object v0
.end method
