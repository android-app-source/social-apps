.class public LX/7wq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7wo;


# instance fields
.field public A:Ljava/lang/String;

.field public B:Ljava/lang/String;

.field public C:Ljava/lang/String;

.field public D:Ljava/lang/String;

.field public E:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public F:Ljava/lang/String;

.field public G:Ljava/lang/String;

.field public H:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

.field public I:I

.field public J:Ljava/lang/String;

.field public K:Ljava/lang/String;

.field public L:Z

.field private M:Z

.field public N:LX/7wp;

.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

.field public h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

.field public i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

.field public j:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

.field public p:Z

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

.field public v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLInterfaces$EventTicketingInfo$RegistrationSettings$Nodes$ScreenElements$;",
            ">;"
        }
    .end annotation
.end field

.field public w:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;",
            ">;"
        }
    .end annotation
.end field

.field public x:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

.field private z:Lcom/facebook/payments/auth/pin/model/PaymentPin;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1276607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1276608
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7wq;->x:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a(II)LX/7wo;
    .locals 3

    .prologue
    .line 1276609
    iget-object v0, p0, LX/7wq;->q:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    .line 1276610
    iget v1, p0, LX/7wq;->I:I

    iget v2, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    sub-int v2, p2, v2

    add-int/2addr v1, v2

    iput v1, p0, LX/7wq;->I:I

    .line 1276611
    iput p2, v0, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->l:I

    .line 1276612
    return-object p0
.end method

.method public final a(ILcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;Ljava/lang/String;Lcom/facebook/events/tickets/modal/model/FieldItem;)LX/7wo;
    .locals 3

    .prologue
    .line 1276613
    iget-object v0, p0, LX/7wq;->w:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;

    .line 1276614
    iget-object v1, v0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->c:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->c:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 1276615
    :goto_0
    invoke-interface {v1, p3, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1276616
    iget-object v2, v0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->c:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->name()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1276617
    invoke-virtual {p4}, Lcom/facebook/events/tickets/modal/model/FieldItem;->g()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1276618
    iget-object v1, v0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->d:Ljava/util/Set;

    invoke-interface {v1, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1276619
    :goto_1
    return-object p0

    .line 1276620
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto :goto_0

    .line 1276621
    :cond_1
    iget-object v1, v0, Lcom/facebook/events/tickets/modal/model/EventRegistrationStoredData;->d:Ljava/util/Set;

    invoke-interface {v1, p3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final synthetic a(LX/0Px;)LX/7wo;
    .locals 1

    .prologue
    .line 1276622
    iput-object p1, p0, LX/7wq;->w:LX/0Px;

    .line 1276623
    move-object v0, p0

    .line 1276624
    return-object v0
.end method

.method public final synthetic a(LX/7wp;)LX/7wo;
    .locals 1

    .prologue
    .line 1276625
    iput-object p1, p0, LX/7wq;->N:LX/7wp;

    .line 1276626
    move-object v0, p0

    .line 1276627
    return-object v0
.end method

.method public final synthetic a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)LX/7wo;
    .locals 1

    .prologue
    .line 1276628
    iput-object p1, p0, LX/7wq;->H:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276629
    move-object v0, p0

    .line 1276630
    return-object v0
.end method

.method public final synthetic a(Ljava/lang/String;)LX/7wo;
    .locals 1

    .prologue
    .line 1276631
    iput-object p1, p0, LX/7wq;->a:Ljava/lang/String;

    .line 1276632
    move-object v0, p0

    .line 1276633
    return-object v0
.end method

.method public final a(Ljava/lang/String;I)LX/7wo;
    .locals 2

    .prologue
    .line 1276634
    iget-object v0, p0, LX/7wq;->x:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1276635
    return-object p0
.end method

.method public final synthetic a(Z)LX/7wo;
    .locals 1

    .prologue
    .line 1276636
    iput-boolean p1, p0, LX/7wq;->L:Z

    .line 1276637
    move-object v0, p0

    .line 1276638
    return-object v0
.end method

.method public final a(I)LX/7wq;
    .locals 0

    .prologue
    .line 1276642
    iput p1, p0, LX/7wq;->c:I

    .line 1276643
    return-object p0
.end method

.method public final a(LX/2uF;)LX/7wq;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "setRegistrationTargetTypeFromNodesModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1276639
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1276640
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x1

    const-class v3, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    invoke-virtual {v1, v0, v2, v3, v4}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    iput-object v0, p0, LX/7wq;->u:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    .line 1276641
    :cond_0
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/7wq;
    .locals 0

    .prologue
    .line 1276657
    iput-object p1, p0, LX/7wq;->f:Landroid/net/Uri;

    .line 1276658
    return-object p0
.end method

.method public final a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel$NodesModel;)LX/7wq;
    .locals 1

    .prologue
    .line 1276654
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel$NodesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel$NodesModel$SeatingMapImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1276655
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel$NodesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel$NodesModel$SeatingMapImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketSettingsModel$NodesModel$SeatingMapImageModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7wq;->r:Ljava/lang/String;

    .line 1276656
    :cond_0
    return-object p0
.end method

.method public final a(Lcom/facebook/events/logging/BuyTicketsLoggingInfo;)LX/7wq;
    .locals 0

    .prologue
    .line 1276652
    iput-object p1, p0, LX/7wq;->o:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    .line 1276653
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/7wq;
    .locals 0

    .prologue
    .line 1276659
    iput-object p1, p0, LX/7wq;->E:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1276660
    return-object p0
.end method

.method public final a(Lcom/facebook/payments/auth/pin/model/PaymentPin;)LX/7wq;
    .locals 0
    .param p1    # Lcom/facebook/payments/auth/pin/model/PaymentPin;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1276650
    iput-object p1, p0, LX/7wq;->z:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1276651
    return-object p0
.end method

.method public final a()Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;
    .locals 42

    .prologue
    .line 1276649
    new-instance v1, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7wq;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7wq;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, LX/7wq;->c:I

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7wq;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/7wq;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7wq;->f:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7wq;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/7wq;->h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/7wq;->i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/7wq;->j:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/7wq;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/7wq;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/7wq;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, LX/7wq;->n:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->o:Lcom/facebook/events/logging/BuyTicketsLoggingInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/7wq;->p:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->q:LX/0Px;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->r:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->s:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->t:LX/0Px;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->u:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->v:LX/0Px;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->w:LX/0Px;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->x:Ljava/util/Map;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->y:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->A:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->B:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->C:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->D:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->E:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/7wq;->I:I

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->J:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->K:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/7wq;->L:Z

    move/from16 v35, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/7wq;->M:Z

    move/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->F:Ljava/lang/String;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->G:Ljava/lang/String;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->H:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->N:LX/7wp;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7wq;->z:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    move-object/from16 v41, v0

    invoke-direct/range {v1 .. v41}, Lcom/facebook/events/tickets/modal/model/EventBuyTicketsModel;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/logging/BuyTicketsLoggingInfo;ZLX/0Px;Ljava/lang/String;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;LX/0Px;LX/0Px;Ljava/util/Map;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;ILjava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;LX/7wp;Lcom/facebook/payments/auth/pin/model/PaymentPin;)V

    return-object v1
.end method

.method public final synthetic b(Ljava/lang/String;)LX/7wo;
    .locals 1

    .prologue
    .line 1276646
    iput-object p1, p0, LX/7wq;->b:Ljava/lang/String;

    .line 1276647
    move-object v0, p0

    .line 1276648
    return-object v0
.end method

.method public final b(I)LX/7wq;
    .locals 0

    .prologue
    .line 1276644
    iput p1, p0, LX/7wq;->I:I

    .line 1276645
    return-object p0
.end method

.method public final b(LX/0Px;)LX/7wq;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;",
            ">;)",
            "LX/7wq;"
        }
    .end annotation

    .prologue
    .line 1276582
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 1276583
    invoke-virtual/range {p1 .. p1}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    const/4 v2, 0x1

    move v3, v2

    .line 1276584
    :goto_0
    invoke-virtual/range {p1 .. p1}, LX/0Px;->size()I

    move-result v10

    const/4 v2, 0x0

    move v8, v2

    :goto_1
    if-ge v8, v10, :cond_5

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;

    .line 1276585
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->r()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    sget v4, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->a:I

    int-to-long v4, v4

    .line 1276586
    :goto_2
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->m()J

    move-result-wide v6

    const-wide/16 v12, 0x0

    cmp-long v6, v6, v12

    if-nez v6, :cond_2

    sget v6, Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;->a:I

    int-to-long v6, v6

    .line 1276587
    :goto_3
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->b()LX/1vs;

    move-result-object v11

    iget-object v12, v11, LX/1vs;->a:LX/15i;

    iget v11, v11, LX/1vs;->b:I

    .line 1276588
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->b()LX/1vs;

    move-result-object v13

    iget-object v14, v13, LX/1vs;->a:LX/15i;

    iget v13, v13, LX/1vs;->b:I

    .line 1276589
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->b()LX/1vs;

    move-result-object v15

    iget-object v0, v15, LX/1vs;->a:LX/15i;

    move-object/from16 v16, v0

    iget v15, v15, LX/1vs;->b:I

    .line 1276590
    new-instance v17, LX/7wu;

    invoke-direct/range {v17 .. v17}, LX/7wu;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->n()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, LX/7wu;->a(Ljava/lang/String;)LX/7wu;

    move-result-object v17

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->o()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, LX/7wu;->b(Ljava/lang/String;)LX/7wu;

    move-result-object v17

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->p()Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, LX/7wu;->a(Lcom/facebook/graphql/enums/GraphQLTicketTierSaleStatus;)LX/7wu;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, LX/7wu;->a(J)LX/7wu;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, LX/7wu;->b(J)LX/7wu;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->k()I

    move-result v5

    invoke-virtual {v4, v5}, LX/7wu;->a(I)LX/7wu;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->c()I

    move-result v5

    invoke-virtual {v4, v5}, LX/7wu;->b(I)LX/7wu;

    move-result-object v5

    if-eqz v3, :cond_3

    const/4 v4, 0x0

    :goto_4
    invoke-virtual {v5, v4}, LX/7wu;->c(I)LX/7wu;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v12, v11, v5}, LX/15i;->j(II)I

    move-result v5

    const/4 v6, 0x3

    invoke-virtual {v14, v13, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v15, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, LX/7xE;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/7wu;->a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/7wu;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/7wu;->c(Ljava/lang/String;)LX/7wu;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel$SeatMapThumbnailModel;

    move-result-object v4

    if-nez v4, :cond_4

    const/4 v4, 0x0

    :goto_5
    invoke-virtual {v5, v4}, LX/7wu;->d(Ljava/lang/String;)LX/7wu;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/7wu;->a(LX/0Px;)LX/7wu;

    move-result-object v2

    .line 1276591
    invoke-virtual {v2}, LX/7wu;->a()Lcom/facebook/events/tickets/modal/model/EventTicketTierModel;

    move-result-object v2

    .line 1276592
    invoke-virtual {v9, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1276593
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto/16 :goto_1

    .line 1276594
    :cond_0
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_0

    .line 1276595
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->r()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    goto/16 :goto_2

    .line 1276596
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->m()J

    move-result-wide v6

    const-wide/16 v12, 0x3e8

    mul-long/2addr v6, v12

    goto/16 :goto_3

    .line 1276597
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->c()I

    move-result v4

    goto :goto_4

    :cond_4
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel;->q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel$SeatMapThumbnailModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketTierFragmentModel$TicketTiersModel$NodesModel$SeatMapThumbnailModel;->a()Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    .line 1276598
    :cond_5
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, LX/7wq;->q:LX/0Px;

    .line 1276599
    return-object p0
.end method

.method public final b(LX/2uF;)LX/7wq;
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "setScreenElementsFromNodesModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1276600
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1276601
    invoke-virtual {p1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1276602
    const/4 v4, 0x0

    const-class v5, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketingInfoModel$RegistrationSettingsModel$NodesModel$ScreenElementsModel;

    invoke-virtual {v3, v0, v4, v5}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_0

    .line 1276603
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1276604
    goto :goto_1

    .line 1276605
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/7wq;->v:LX/0Px;

    .line 1276606
    return-object p0
.end method

.method public final b(LX/7wp;)LX/7wq;
    .locals 0

    .prologue
    .line 1276560
    iput-object p1, p0, LX/7wq;->N:LX/7wp;

    .line 1276561
    return-object p0
.end method

.method public final b(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)LX/7wq;
    .locals 0

    .prologue
    .line 1276540
    iput-object p1, p0, LX/7wq;->g:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276541
    return-object p0
.end method

.method public final b(Z)LX/7wq;
    .locals 0

    .prologue
    .line 1276558
    iput-boolean p1, p0, LX/7wq;->p:Z

    .line 1276559
    return-object p0
.end method

.method public final synthetic c(Ljava/lang/String;)LX/7wo;
    .locals 1

    .prologue
    .line 1276555
    iput-object p1, p0, LX/7wq;->F:Ljava/lang/String;

    .line 1276556
    move-object v0, p0

    .line 1276557
    return-object v0
.end method

.method public final c(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)LX/7wq;
    .locals 0

    .prologue
    .line 1276553
    iput-object p1, p0, LX/7wq;->h:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276554
    return-object p0
.end method

.method public final synthetic d(Ljava/lang/String;)LX/7wo;
    .locals 1

    .prologue
    .line 1276550
    iput-object p1, p0, LX/7wq;->G:Ljava/lang/String;

    .line 1276551
    move-object v0, p0

    .line 1276552
    return-object v0
.end method

.method public final d(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)LX/7wq;
    .locals 0

    .prologue
    .line 1276548
    iput-object p1, p0, LX/7wq;->i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276549
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/7wq;
    .locals 0

    .prologue
    .line 1276546
    iput-object p1, p0, LX/7wq;->d:Ljava/lang/String;

    .line 1276547
    return-object p0
.end method

.method public final f(LX/0Px;)LX/7wq;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketAdditionalChargeFragmentModel;",
            ">;)",
            "LX/7wq;"
        }
    .end annotation

    .prologue
    .line 1276544
    iput-object p1, p0, LX/7wq;->t:LX/0Px;

    .line 1276545
    return-object p0
.end method

.method public final f(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;)LX/7wq;
    .locals 0

    .prologue
    .line 1276542
    iput-object p1, p0, LX/7wq;->y:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1276543
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/7wq;
    .locals 0

    .prologue
    .line 1276562
    iput-object p1, p0, LX/7wq;->e:Ljava/lang/String;

    .line 1276563
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/7wq;
    .locals 0

    .prologue
    .line 1276564
    iput-object p1, p0, LX/7wq;->k:Ljava/lang/String;

    .line 1276565
    return-object p0
.end method

.method public final h(Ljava/lang/String;)LX/7wq;
    .locals 0

    .prologue
    .line 1276566
    iput-object p1, p0, LX/7wq;->l:Ljava/lang/String;

    .line 1276567
    return-object p0
.end method

.method public final i(Ljava/lang/String;)LX/7wq;
    .locals 0

    .prologue
    .line 1276568
    iput-object p1, p0, LX/7wq;->m:Ljava/lang/String;

    .line 1276569
    return-object p0
.end method

.method public final j(Ljava/lang/String;)LX/7wq;
    .locals 0

    .prologue
    .line 1276570
    iput-object p1, p0, LX/7wq;->n:Ljava/lang/String;

    .line 1276571
    return-object p0
.end method

.method public final l(Ljava/lang/String;)LX/7wq;
    .locals 0

    .prologue
    .line 1276572
    iput-object p1, p0, LX/7wq;->s:Ljava/lang/String;

    .line 1276573
    return-object p0
.end method

.method public final m(Ljava/lang/String;)LX/7wq;
    .locals 0

    .prologue
    .line 1276574
    iput-object p1, p0, LX/7wq;->A:Ljava/lang/String;

    .line 1276575
    return-object p0
.end method

.method public final n(Ljava/lang/String;)LX/7wq;
    .locals 0

    .prologue
    .line 1276576
    iput-object p1, p0, LX/7wq;->B:Ljava/lang/String;

    .line 1276577
    return-object p0
.end method

.method public final o(Ljava/lang/String;)LX/7wq;
    .locals 0

    .prologue
    .line 1276578
    iput-object p1, p0, LX/7wq;->C:Ljava/lang/String;

    .line 1276579
    return-object p0
.end method

.method public final p(Ljava/lang/String;)LX/7wq;
    .locals 0

    .prologue
    .line 1276580
    iput-object p1, p0, LX/7wq;->D:Ljava/lang/String;

    .line 1276581
    return-object p0
.end method
