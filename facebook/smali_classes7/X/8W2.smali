.class public LX/8W2;
.super LX/1a1;
.source ""

# interfaces
.implements LX/8Vr;


# instance fields
.field private final l:Lcom/facebook/widget/text/BetterTextView;

.field private final m:Lcom/facebook/widget/tiles/ThreadTileView;

.field private final n:Lcom/facebook/widget/text/BetterTextView;

.field private final o:Lcom/facebook/widget/text/BetterTextView;

.field private final p:Lcom/facebook/widget/text/BetterTextView;

.field private final q:Lcom/facebook/resources/ui/FbTextView;

.field private r:LX/8Vd;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1353702
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1353703
    const v0, 0x7f0d1406

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/8W2;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 1353704
    const v0, 0x7f0d0946

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, LX/8W2;->m:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 1353705
    const v0, 0x7f0d13f6

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/8W2;->n:Lcom/facebook/widget/text/BetterTextView;

    .line 1353706
    const v0, 0x7f0d13f7

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/8W2;->o:Lcom/facebook/widget/text/BetterTextView;

    .line 1353707
    const v0, 0x7f0d1403

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/8W2;->p:Lcom/facebook/widget/text/BetterTextView;

    .line 1353708
    const v0, 0x7f0d1407

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/8W2;->q:Lcom/facebook/resources/ui/FbTextView;

    .line 1353709
    return-void
.end method


# virtual methods
.method public final a(IILX/8Vb;ZLX/8Vp;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1353677
    iget-object v0, p3, LX/8Vb;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p4, :cond_3

    .line 1353678
    :cond_0
    iget-object v0, p0, LX/8W2;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1353679
    :goto_0
    iget-object v0, p0, LX/8W2;->r:LX/8Vd;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/8W2;->r:LX/8Vd;

    iget-object v1, p3, LX/8Vb;->i:LX/8Vd;

    if-eq v0, v1, :cond_2

    .line 1353680
    :cond_1
    iget-object v0, p0, LX/8W2;->m:Lcom/facebook/widget/tiles/ThreadTileView;

    iget-object v1, p3, LX/8Vb;->i:LX/8Vd;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 1353681
    iget-object v0, p3, LX/8Vb;->i:LX/8Vd;

    iput-object v0, p0, LX/8W2;->r:LX/8Vd;

    .line 1353682
    :cond_2
    iget-object v0, p0, LX/8W2;->n:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p3, LX/8Vb;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1353683
    iget-object v0, p3, LX/8Vb;->n:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1353684
    iget-object v0, p0, LX/8W2;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1353685
    :goto_1
    iget-object v0, p3, LX/8Vb;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1353686
    iget-object v0, p0, LX/8W2;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1353687
    :goto_2
    iget-object v0, p3, LX/8Vb;->h:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1353688
    iget-object v0, p0, LX/8W2;->q:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1353689
    :goto_3
    return-void

    .line 1353690
    :cond_3
    iget-object v0, p0, LX/8W2;->l:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p3, LX/8Vb;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1353691
    iget-object v0, p0, LX/8W2;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    .line 1353692
    :cond_4
    iget-object v0, p0, LX/8W2;->o:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p3, LX/8Vb;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1353693
    iget-object v0, p0, LX/8W2;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_1

    .line 1353694
    :cond_5
    iget-object v0, p0, LX/8W2;->p:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p3, LX/8Vb;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1353695
    iget-object v0, p0, LX/8W2;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_2

    .line 1353696
    :cond_6
    iget-object v0, p0, LX/8W2;->q:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p3, LX/8Vb;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1353697
    iget-boolean v0, p3, LX/8Vb;->k:Z

    if-eqz v0, :cond_7

    .line 1353698
    iget-object v0, p0, LX/8W2;->q:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    .line 1353699
    iget-object v0, p0, LX/8W2;->q:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setClickable(Z)V

    .line 1353700
    :cond_7
    iget-object v0, p0, LX/8W2;->q:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1353701
    iget-object v0, p0, LX/8W2;->q:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/8W1;

    invoke-direct {v1, p0, p5, p3, p1}, LX/8W1;-><init>(LX/8W2;LX/8Vp;LX/8Vb;I)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3
.end method
