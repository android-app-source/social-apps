.class public LX/6mv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/proxygen/ByteEventLogger;


# instance fields
.field public volatile a:LX/075;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1146830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onBytesReceived(Ljava/lang/String;J)V
    .locals 4

    .prologue
    .line 1146818
    iget-object v0, p0, LX/6mv;->a:LX/075;

    if-eqz v0, :cond_0

    .line 1146819
    long-to-int v0, p2

    .line 1146820
    iget-object v1, p0, LX/6mv;->a:LX/075;

    invoke-virtual {v1, v0}, LX/075;->b(I)V

    .line 1146821
    const-string v1, "PUBLISH_"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1146822
    iget-object v1, p0, LX/6mv;->a:LX/075;

    const-string v2, "PUBLISH"

    const/16 v3, 0x8

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/075;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1146823
    :cond_0
    :goto_0
    return-void

    .line 1146824
    :cond_1
    iget-object v1, p0, LX/6mv;->a:LX/075;

    const-string v2, ""

    invoke-virtual {v1, p1, v2, v0}, LX/075;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final onBytesSent(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 1146825
    iget-object v0, p0, LX/6mv;->a:LX/075;

    if-eqz v0, :cond_0

    .line 1146826
    long-to-int v0, p2

    .line 1146827
    iget-object v1, p0, LX/6mv;->a:LX/075;

    invoke-virtual {v1, v0}, LX/075;->a(I)V

    .line 1146828
    iget-object v1, p0, LX/6mv;->a:LX/075;

    invoke-virtual {v1, p1, v0}, LX/075;->a(Ljava/lang/String;I)V

    .line 1146829
    :cond_0
    return-void
.end method
