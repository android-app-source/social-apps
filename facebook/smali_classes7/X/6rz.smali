.class public LX/6rz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1152834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152835
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1152836
    const-string v0, "title"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152837
    const-string v0, "title"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->a(Ljava/lang/String;)LX/6rU;

    move-result-object v0

    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 1152838
    iput-object v1, v0, LX/6rU;->b:Ljava/lang/String;

    .line 1152839
    move-object v0, v0

    .line 1152840
    const-string v1, "subsubtitle"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 1152841
    iput-object v1, v0, LX/6rU;->c:Ljava/lang/String;

    .line 1152842
    move-object v0, v0

    .line 1152843
    const-string v1, "merchant_name"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 1152844
    iput-object v1, v0, LX/6rU;->d:Ljava/lang/String;

    .line 1152845
    move-object v0, v0

    .line 1152846
    const-string v1, "item_image_url"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 1152847
    iput-object v1, v0, LX/6rU;->e:Ljava/lang/String;

    .line 1152848
    move-object v0, v0

    .line 1152849
    invoke-virtual {v0}, LX/6rU;->a()Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;

    move-result-object v0

    return-object v0
.end method
