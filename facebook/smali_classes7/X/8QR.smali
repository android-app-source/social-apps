.class public final enum LX/8QR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8QR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8QR;

.field public static final enum ALL_FRIENDS:LX/8QR;

.field public static final enum FRIENDS_OF_FRIENDS:LX/8QR;

.field public static final enum SOME_FRIENDS:LX/8QR;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1343074
    new-instance v0, LX/8QR;

    const-string v1, "SOME_FRIENDS"

    invoke-direct {v0, v1, v2}, LX/8QR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8QR;->SOME_FRIENDS:LX/8QR;

    .line 1343075
    new-instance v0, LX/8QR;

    const-string v1, "ALL_FRIENDS"

    invoke-direct {v0, v1, v3}, LX/8QR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8QR;->ALL_FRIENDS:LX/8QR;

    .line 1343076
    new-instance v0, LX/8QR;

    const-string v1, "FRIENDS_OF_FRIENDS"

    invoke-direct {v0, v1, v4}, LX/8QR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8QR;->FRIENDS_OF_FRIENDS:LX/8QR;

    .line 1343077
    const/4 v0, 0x3

    new-array v0, v0, [LX/8QR;

    sget-object v1, LX/8QR;->SOME_FRIENDS:LX/8QR;

    aput-object v1, v0, v2

    sget-object v1, LX/8QR;->ALL_FRIENDS:LX/8QR;

    aput-object v1, v0, v3

    sget-object v1, LX/8QR;->FRIENDS_OF_FRIENDS:LX/8QR;

    aput-object v1, v0, v4

    sput-object v0, LX/8QR;->$VALUES:[LX/8QR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1343078
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8QR;
    .locals 1

    .prologue
    .line 1343079
    const-class v0, LX/8QR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8QR;

    return-object v0
.end method

.method public static values()[LX/8QR;
    .locals 1

    .prologue
    .line 1343080
    sget-object v0, LX/8QR;->$VALUES:[LX/8QR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8QR;

    return-object v0
.end method
