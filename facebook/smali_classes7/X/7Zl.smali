.class public final LX/7Zl;
.super LX/2wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2wH",
        "<",
        "LX/7Zr;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;LX/2wA;LX/1qf;LX/1qg;)V
    .locals 7

    const/16 v3, 0x36

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/2wH;-><init>(Landroid/content/Context;Landroid/os/Looper;ILX/2wA;LX/1qf;LX/1qg;)V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/7Zl;->d:J

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.nearby.internal.connection.INearbyConnectionService"

    invoke-interface {p1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of p0, v0, LX/7Zr;

    if-eqz p0, :cond_1

    check-cast v0, LX/7Zr;

    goto :goto_0

    :cond_1
    new-instance v0, LX/7Zs;

    invoke-direct {v0, p1}, LX/7Zs;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.nearby.connection.service.START"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.nearby.internal.connection.INearbyConnectionService"

    return-object v0
.end method

.method public final f()V
    .locals 4

    invoke-virtual {p0}, LX/2wI;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, LX/2wI;->m()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, LX/7Zr;

    iget-wide v2, p0, LX/7Zl;->d:J

    invoke-interface {v0, v2, v3}, LX/7Zr;->c(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-super {p0}, LX/2wH;->f()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "NearbyConnectionsClient"

    const-string v2, "Failed to notify client disconnect."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final j()Landroid/os/Bundle;
    .locals 4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "clientId"

    iget-wide v2, p0, LX/7Zl;->d:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-object v0
.end method
