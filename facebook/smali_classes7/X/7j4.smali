.class public LX/7j4;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/util/Locale;

.field public final c:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1229112
    const-class v0, LX/7j4;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7j4;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1229108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1229109
    iput-object p1, p0, LX/7j4;->b:Ljava/util/Locale;

    .line 1229110
    iput-object p2, p0, LX/7j4;->c:Landroid/content/Context;

    .line 1229111
    return-void
.end method

.method private static a(I)I
    .locals 3

    .prologue
    .line 1229101
    sparse-switch p0, :sswitch_data_0

    .line 1229102
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid offset value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Supported are [1,100,1000,10000]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1229103
    :sswitch_0
    const/4 v0, 0x0

    .line 1229104
    :goto_0
    return v0

    .line 1229105
    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 1229106
    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 1229107
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x64 -> :sswitch_1
        0x3e8 -> :sswitch_2
        0x2710 -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/0P1;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1229099
    const-string v0, "offset"

    invoke-virtual {p0, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1229100
    const-string v0, "offset"

    invoke-virtual {p0, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, LX/7j4;->a(I)I

    move-result v0

    return v0
.end method

.method public static a(LX/0QB;)LX/7j4;
    .locals 1

    .prologue
    .line 1229098
    invoke-static {p0}, LX/7j4;->b(LX/0QB;)LX/7j4;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1229095
    if-nez p0, :cond_0

    .line 1229096
    const/4 v0, 0x0

    .line 1229097
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;->a()I

    move-result v1

    invoke-static {v0, v1}, LX/7j4;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1229090
    sget-object v0, LX/5fx;->a:LX/0P1;

    move-object v0, v0

    .line 1229091
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    .line 1229092
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/7j4;->a(LX/0P1;)I

    move-result v0

    .line 1229093
    :goto_0
    int-to-long v2, p1

    invoke-static {p0, v2, v3, v0}, LX/47f;->a(Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1229094
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/7j4;
    .locals 3

    .prologue
    .line 1229088
    new-instance v2, LX/7j4;

    invoke-static {p0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v0, v1}, LX/7j4;-><init>(Ljava/util/Locale;Landroid/content/Context;)V

    .line 1229089
    return-object v2
.end method

.method public static b(Ljava/lang/String;)Ljava/util/Currency;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1229047
    :try_start_0
    invoke-static {p0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1229048
    :goto_0
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/Currency;Ljava/lang/String;)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1229049
    sget-object v0, LX/5fx;->a:LX/0P1;

    move-object v0, v0

    .line 1229050
    invoke-virtual {p1}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    .line 1229051
    if-nez v0, :cond_0

    move-object v0, v1

    .line 1229052
    :goto_0
    return-object v0

    .line 1229053
    :cond_0
    const/4 v9, 0x0

    .line 1229054
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    move-object v7, v9

    .line 1229055
    :goto_1
    move-object v0, v7

    .line 1229056
    if-nez v0, :cond_1

    move-object v0, v1

    .line 1229057
    goto :goto_0

    .line 1229058
    :cond_1
    :try_start_0
    new-instance v2, Ljava/math/BigDecimal;

    const/16 v3, 0x64

    invoke-direct {v2, v3}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    .line 1229059
    const-wide/32 v4, 0x7fffffff

    cmp-long v0, v2, v4

    if-ltz v0, :cond_2

    .line 1229060
    const-class v0, LX/7j4;

    const-string v4, "Price causes integer overflow: %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-static {v0, v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 1229061
    goto :goto_0

    .line 1229062
    :cond_2
    long-to-int v0, v2

    .line 1229063
    new-instance v1, LX/7ig;

    invoke-direct {v1}, LX/7ig;-><init>()V

    invoke-virtual {p1}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v2

    .line 1229064
    iput-object v2, v1, LX/7ig;->b:Ljava/lang/String;

    .line 1229065
    move-object v1, v1

    .line 1229066
    iput v0, v1, LX/7ig;->a:I

    .line 1229067
    move-object v0, v1

    .line 1229068
    invoke-virtual {v0}, LX/7ig;->a()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v0

    goto :goto_0

    .line 1229069
    :catch_0
    move-exception v0

    .line 1229070
    const-class v2, LX/7j4;

    const-string v3, "Can\'t convert from BigDecimal to Integer"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 1229071
    goto :goto_0

    .line 1229072
    :cond_3
    invoke-static {v0}, LX/7j4;->a(LX/0P1;)I

    move-result v10

    .line 1229073
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 1229074
    invoke-virtual {p1}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v8

    const-string v11, ""

    invoke-virtual {v7, v8, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 1229075
    iget-object v8, p0, LX/7j4;->b:Ljava/util/Locale;

    invoke-virtual {p1, v8}, Ljava/util/Currency;->getSymbol(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    .line 1229076
    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_5

    .line 1229077
    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v8, v11}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    const-string v11, ""

    invoke-virtual {v7, v8, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    move-object v8, v7

    .line 1229078
    :goto_2
    const-string v7, "symbol"

    invoke-virtual {v0, v7}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 1229079
    invoke-static {v7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 1229080
    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v11}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    const-string v11, ""

    invoke-virtual {v8, v7, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    .line 1229081
    :cond_4
    const-string v7, "\\s"

    const-string v11, ""

    invoke-virtual {v8, v7, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1229082
    iget-object v8, p0, LX/7j4;->b:Ljava/util/Locale;

    invoke-static {v8}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v8

    .line 1229083
    invoke-virtual {v8, p1}, Ljava/text/NumberFormat;->setCurrency(Ljava/util/Currency;)V

    .line 1229084
    :try_start_1
    new-instance v11, Ljava/math/BigDecimal;

    invoke-virtual {v8, v7}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v7

    invoke-direct {v11, v7, v8}, Ljava/math/BigDecimal;-><init>(D)V

    sget-object v7, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v11, v10, v7}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v7

    goto/16 :goto_1

    .line 1229085
    :catch_1
    move-exception v7

    .line 1229086
    sget-object v8, LX/7j4;->a:Ljava/lang/String;

    const-string v10, "Could not parse input: %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object p2, v11, v12

    invoke-static {v8, v7, v10, v11}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v7, v9

    .line 1229087
    goto/16 :goto_1

    :cond_5
    move-object v8, v7

    goto :goto_2
.end method
