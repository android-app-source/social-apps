.class public LX/7T2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/5Pc;

.field public final b:LX/5PR;

.field public c:LX/5Pb;

.field private final d:LX/60w;

.field public final e:[F

.field public final f:[F

.field public final g:[F

.field public h:I

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/61B;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/5Pf;


# direct methods
.method public constructor <init>(LX/5Pc;LX/7Sx;LX/60w;)V
    .locals 10

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v9, 0x3f000000    # 0.5f

    const/high16 v8, -0x41000000    # -0.5f

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 1209575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1209576
    new-instance v0, LX/5PQ;

    const/4 v2, 0x4

    invoke-direct {v0, v2}, LX/5PQ;-><init>(I)V

    const/4 v2, 0x5

    .line 1209577
    iput v2, v0, LX/5PQ;->a:I

    .line 1209578
    move-object v0, v0

    .line 1209579
    const-string v2, "aPosition"

    new-instance v4, LX/5Pg;

    const/16 v6, 0x8

    new-array v6, v6, [F

    fill-array-data v6, :array_0

    const/4 v7, 0x2

    invoke-direct {v4, v6, v7}, LX/5Pg;-><init>([FI)V

    invoke-virtual {v0, v2, v4}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    const-string v2, "aTextureCoord"

    new-instance v4, LX/5Pg;

    const/16 v6, 0x8

    new-array v6, v6, [F

    fill-array-data v6, :array_1

    const/4 v7, 0x2

    invoke-direct {v4, v6, v7}, LX/5Pg;-><init>([FI)V

    invoke-virtual {v0, v2, v4}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    invoke-virtual {v0}, LX/5PQ;->a()LX/5PR;

    move-result-object v0

    iput-object v0, p0, LX/7T2;->b:LX/5PR;

    .line 1209580
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LX/7T2;->e:[F

    .line 1209581
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LX/7T2;->f:[F

    .line 1209582
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LX/7T2;->g:[F

    .line 1209583
    const/16 v0, -0x3039

    iput v0, p0, LX/7T2;->h:I

    .line 1209584
    iput-object p3, p0, LX/7T2;->d:LX/60w;

    .line 1209585
    iput-object p1, p0, LX/7T2;->a:LX/5Pc;

    .line 1209586
    iget-object v0, p2, LX/7Sx;->q:Ljava/util/List;

    iput-object v0, p0, LX/7T2;->i:Ljava/util/List;

    .line 1209587
    iget-object v0, p0, LX/7T2;->e:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1209588
    iget-object v0, p0, LX/7T2;->f:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1209589
    iget-object v0, p0, LX/7T2;->g:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1209590
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v0, v2, :cond_0

    .line 1209591
    iget-object v0, p0, LX/7T2;->f:[F

    invoke-static {v0, v1, v9, v9, v3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1209592
    iget-object v0, p0, LX/7T2;->f:[F

    iget v2, p2, LX/7Sx;->c:I

    int-to-float v2, v2

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 1209593
    iget-object v0, p0, LX/7T2;->f:[F

    invoke-static {v0, v1, v8, v8, v3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1209594
    :cond_0
    iget-object v0, p0, LX/7T2;->f:[F

    iget-object v2, p2, LX/7Sx;->h:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v4, p2, LX/7Sx;->h:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    invoke-static {v0, v1, v2, v4, v3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1209595
    iget-object v0, p0, LX/7T2;->f:[F

    iget-object v2, p2, LX/7Sx;->h:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v4, p2, LX/7Sx;->h:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-static {v0, v1, v2, v4, v5}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 1209596
    iget-object v0, p0, LX/7T2;->f:[F

    invoke-static {v0, v1, v9, v9, v3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1209597
    iget-object v0, p2, LX/7Sx;->i:LX/7Sv;

    sget-object v2, LX/7Sv;->MIRROR_HORIZONTALLY:LX/7Sv;

    if-ne v0, v2, :cond_1

    .line 1209598
    iget-object v0, p0, LX/7T2;->f:[F

    const/high16 v2, -0x40800000    # -1.0f

    invoke-static {v0, v1, v2, v5, v5}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 1209599
    :cond_1
    iget-object v0, p0, LX/7T2;->f:[F

    iget v2, p2, LX/7Sx;->f:I

    int-to-float v2, v2

    const/high16 v5, -0x40800000    # -1.0f

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 1209600
    iget-object v0, p0, LX/7T2;->f:[F

    invoke-static {v0, v1, v8, v8, v3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 1209601
    return-void

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1209602
    iget-object v0, p0, LX/7T2;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1209603
    iget-object v0, p0, LX/7T2;->j:LX/5Pf;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1209604
    iget-object v0, p0, LX/7T2;->j:LX/5Pf;

    iget v0, v0, LX/5Pf;->b:I

    .line 1209605
    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/7T2;->h:I

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    const v3, 0x8d65

    .line 1209606
    invoke-static {v0, v1, v0, v1}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 1209607
    iget-object v0, p0, LX/7T2;->d:LX/60w;

    sget-object v1, LX/60w;->RGBA:LX/60w;

    if-ne v0, v1, :cond_1

    .line 1209608
    iget-object v0, p0, LX/7T2;->a:LX/5Pc;

    const v1, 0x7f070096

    const v2, 0x7f070095

    invoke-interface {v0, v1, v2}, LX/5Pc;->a(II)LX/5Pb;

    move-result-object v0

    iput-object v0, p0, LX/7T2;->c:LX/5Pb;

    .line 1209609
    :goto_0
    iget-object v0, p0, LX/7T2;->i:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 1209610
    const v3, 0x812f

    const/16 v2, 0x2601

    .line 1209611
    new-instance v0, LX/5Pe;

    invoke-direct {v0}, LX/5Pe;-><init>()V

    const v1, 0x8d65

    .line 1209612
    iput v1, v0, LX/5Pe;->a:I

    .line 1209613
    move-object v0, v0

    .line 1209614
    const/16 v1, 0x2801

    invoke-virtual {v0, v1, v2}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v1, 0x2800

    invoke-virtual {v0, v1, v2}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v1, 0x2802

    invoke-virtual {v0, v1, v3}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v1, 0x2803

    invoke-virtual {v0, v1, v3}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    invoke-virtual {v0}, LX/5Pe;->a()LX/5Pf;

    move-result-object v0

    iput-object v0, p0, LX/7T2;->j:LX/5Pf;

    .line 1209615
    iget-object v0, p0, LX/7T2;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/61B;

    .line 1209616
    iget-object v2, p0, LX/7T2;->a:LX/5Pc;

    invoke-interface {v0, v2}, LX/61B;->a(LX/5Pc;)V

    goto :goto_1

    .line 1209617
    :cond_0
    const-string v0, "video texture"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1209618
    :goto_2
    return-void

    .line 1209619
    :cond_1
    iget-object v0, p0, LX/7T2;->a:LX/5Pc;

    const v1, 0x7f070096

    const v2, 0x7f070094

    invoke-interface {v0, v1, v2}, LX/5Pc;->a(II)LX/5Pb;

    move-result-object v0

    iput-object v0, p0, LX/7T2;->c:LX/5Pb;

    goto :goto_0

    .line 1209620
    :cond_2
    new-array v0, v5, [I

    .line 1209621
    invoke-static {v5, v0, v4}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 1209622
    aget v0, v0, v4

    iput v0, p0, LX/7T2;->h:I

    .line 1209623
    iget v0, p0, LX/7T2;->h:I

    invoke-static {v3, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1209624
    const-string v0, "glBindTexture mTextureID"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1209625
    const/16 v0, 0x2801

    const/high16 v1, 0x46180000    # 9728.0f

    invoke-static {v3, v0, v1}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1209626
    const/16 v0, 0x2800

    const v1, 0x46180400    # 9729.0f

    invoke-static {v3, v0, v1}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1209627
    const/16 v0, 0x2802

    const v1, 0x812f

    invoke-static {v3, v0, v1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 1209628
    const/16 v0, 0x2803

    const v1, 0x812f

    invoke-static {v3, v0, v1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 1209629
    const-string v0, "glTexParameter"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    goto :goto_2
.end method
