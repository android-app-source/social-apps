.class public LX/6kY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;


# instance fields
.field public final errorCode:Ljava/lang/Integer;

.field public final errorMessage:Ljava/lang/String;

.field public final isRetryable:Ljava/lang/Boolean;

.field public final offlineThreadingId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 1139555
    new-instance v0, LX/1sv;

    const-string v1, "FailedSend"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kY;->b:LX/1sv;

    .line 1139556
    new-instance v0, LX/1sw;

    const-string v1, "offlineThreadingId"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kY;->c:LX/1sw;

    .line 1139557
    new-instance v0, LX/1sw;

    const-string v1, "errorMessage"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kY;->d:LX/1sw;

    .line 1139558
    new-instance v0, LX/1sw;

    const-string v1, "isRetryable"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kY;->e:LX/1sw;

    .line 1139559
    new-instance v0, LX/1sw;

    const-string v1, "errorCode"

    const/16 v2, 0x8

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kY;->f:LX/1sw;

    .line 1139560
    sput-boolean v4, LX/6kY;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 1139561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1139562
    iput-object p1, p0, LX/6kY;->offlineThreadingId:Ljava/lang/Long;

    .line 1139563
    iput-object p2, p0, LX/6kY;->errorMessage:Ljava/lang/String;

    .line 1139564
    iput-object p3, p0, LX/6kY;->isRetryable:Ljava/lang/Boolean;

    .line 1139565
    iput-object p4, p0, LX/6kY;->errorCode:Ljava/lang/Integer;

    .line 1139566
    return-void
.end method

.method public static b(LX/1su;)LX/6kY;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1139567
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v1, v0

    move-object v2, v0

    move-object v3, v0

    .line 1139568
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v4

    .line 1139569
    iget-byte v5, v4, LX/1sw;->b:B

    if-eqz v5, :cond_4

    .line 1139570
    iget-short v5, v4, LX/1sw;->c:S

    packed-switch v5, :pswitch_data_0

    .line 1139571
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1139572
    :pswitch_0
    iget-byte v5, v4, LX/1sw;->b:B

    const/16 v6, 0xa

    if-ne v5, v6, :cond_0

    .line 1139573
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    .line 1139574
    :cond_0
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1139575
    :pswitch_1
    iget-byte v5, v4, LX/1sw;->b:B

    const/16 v6, 0xb

    if-ne v5, v6, :cond_1

    .line 1139576
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1139577
    :cond_1
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1139578
    :pswitch_2
    iget-byte v5, v4, LX/1sw;->b:B

    const/4 v6, 0x2

    if-ne v5, v6, :cond_2

    .line 1139579
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 1139580
    :cond_2
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1139581
    :pswitch_3
    iget-byte v5, v4, LX/1sw;->b:B

    const/16 v6, 0x8

    if-ne v5, v6, :cond_3

    .line 1139582
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 1139583
    :cond_3
    iget-byte v4, v4, LX/1sw;->b:B

    invoke-static {p0, v4}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1139584
    :cond_4
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1139585
    new-instance v4, LX/6kY;

    invoke-direct {v4, v3, v2, v1, v0}, LX/6kY;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;)V

    .line 1139586
    return-object v4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1139587
    if-eqz p2, :cond_6

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1139588
    :goto_0
    if-eqz p2, :cond_7

    const-string v0, "\n"

    move-object v3, v0

    .line 1139589
    :goto_1
    if-eqz p2, :cond_8

    const-string v0, " "

    .line 1139590
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "FailedSend"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1139591
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139592
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139593
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139594
    const/4 v1, 0x1

    .line 1139595
    iget-object v6, p0, LX/6kY;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v6, :cond_0

    .line 1139596
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139597
    const-string v1, "offlineThreadingId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139598
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139599
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139600
    iget-object v1, p0, LX/6kY;->offlineThreadingId:Ljava/lang/Long;

    if-nez v1, :cond_9

    .line 1139601
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 1139602
    :cond_0
    iget-object v6, p0, LX/6kY;->errorMessage:Ljava/lang/String;

    if-eqz v6, :cond_2

    .line 1139603
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139604
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139605
    const-string v1, "errorMessage"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139606
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139607
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139608
    iget-object v1, p0, LX/6kY;->errorMessage:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 1139609
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 1139610
    :cond_2
    iget-object v6, p0, LX/6kY;->isRetryable:Ljava/lang/Boolean;

    if-eqz v6, :cond_d

    .line 1139611
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139612
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139613
    const-string v1, "isRetryable"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139614
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139615
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139616
    iget-object v1, p0, LX/6kY;->isRetryable:Ljava/lang/Boolean;

    if-nez v1, :cond_b

    .line 1139617
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139618
    :goto_5
    iget-object v1, p0, LX/6kY;->errorCode:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 1139619
    if-nez v2, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139620
    :cond_4
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139621
    const-string v1, "errorCode"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139622
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139623
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139624
    iget-object v0, p0, LX/6kY;->errorCode:Ljava/lang/Integer;

    if-nez v0, :cond_c

    .line 1139625
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139626
    :cond_5
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139627
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139628
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1139629
    :cond_6
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1139630
    :cond_7
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1139631
    :cond_8
    const-string v0, ""

    goto/16 :goto_2

    .line 1139632
    :cond_9
    iget-object v1, p0, LX/6kY;->offlineThreadingId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1139633
    :cond_a
    iget-object v1, p0, LX/6kY;->errorMessage:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1139634
    :cond_b
    iget-object v1, p0, LX/6kY;->isRetryable:Ljava/lang/Boolean;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1139635
    :cond_c
    iget-object v0, p0, LX/6kY;->errorCode:Ljava/lang/Integer;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    :cond_d
    move v2, v1

    goto/16 :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1139636
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1139637
    iget-object v0, p0, LX/6kY;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1139638
    iget-object v0, p0, LX/6kY;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1139639
    sget-object v0, LX/6kY;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1139640
    iget-object v0, p0, LX/6kY;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1139641
    :cond_0
    iget-object v0, p0, LX/6kY;->errorMessage:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1139642
    iget-object v0, p0, LX/6kY;->errorMessage:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1139643
    sget-object v0, LX/6kY;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1139644
    iget-object v0, p0, LX/6kY;->errorMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1139645
    :cond_1
    iget-object v0, p0, LX/6kY;->isRetryable:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1139646
    iget-object v0, p0, LX/6kY;->isRetryable:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1139647
    sget-object v0, LX/6kY;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1139648
    iget-object v0, p0, LX/6kY;->isRetryable:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1139649
    :cond_2
    iget-object v0, p0, LX/6kY;->errorCode:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1139650
    iget-object v0, p0, LX/6kY;->errorCode:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1139651
    sget-object v0, LX/6kY;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1139652
    iget-object v0, p0, LX/6kY;->errorCode:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1139653
    :cond_3
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1139654
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1139655
    return-void
.end method

.method public final a(LX/6kY;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1139656
    if-nez p1, :cond_1

    .line 1139657
    :cond_0
    :goto_0
    return v2

    .line 1139658
    :cond_1
    iget-object v0, p0, LX/6kY;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1139659
    :goto_1
    iget-object v3, p1, LX/6kY;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1139660
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 1139661
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1139662
    iget-object v0, p0, LX/6kY;->offlineThreadingId:Ljava/lang/Long;

    iget-object v3, p1, LX/6kY;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1139663
    :cond_3
    iget-object v0, p0, LX/6kY;->errorMessage:Ljava/lang/String;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1139664
    :goto_3
    iget-object v3, p1, LX/6kY;->errorMessage:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1139665
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1139666
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1139667
    iget-object v0, p0, LX/6kY;->errorMessage:Ljava/lang/String;

    iget-object v3, p1, LX/6kY;->errorMessage:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1139668
    :cond_5
    iget-object v0, p0, LX/6kY;->isRetryable:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1139669
    :goto_5
    iget-object v3, p1, LX/6kY;->isRetryable:Ljava/lang/Boolean;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1139670
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1139671
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1139672
    iget-object v0, p0, LX/6kY;->isRetryable:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6kY;->isRetryable:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1139673
    :cond_7
    iget-object v0, p0, LX/6kY;->errorCode:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1139674
    :goto_7
    iget-object v3, p1, LX/6kY;->errorCode:Ljava/lang/Integer;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1139675
    :goto_8
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1139676
    :cond_8
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1139677
    iget-object v0, p0, LX/6kY;->errorCode:Ljava/lang/Integer;

    iget-object v3, p1, LX/6kY;->errorCode:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_9
    move v2, v1

    .line 1139678
    goto :goto_0

    :cond_a
    move v0, v2

    .line 1139679
    goto :goto_1

    :cond_b
    move v3, v2

    .line 1139680
    goto :goto_2

    :cond_c
    move v0, v2

    .line 1139681
    goto :goto_3

    :cond_d
    move v3, v2

    .line 1139682
    goto :goto_4

    :cond_e
    move v0, v2

    .line 1139683
    goto :goto_5

    :cond_f
    move v3, v2

    .line 1139684
    goto :goto_6

    :cond_10
    move v0, v2

    .line 1139685
    goto :goto_7

    :cond_11
    move v3, v2

    .line 1139686
    goto :goto_8
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1139687
    if-nez p1, :cond_1

    .line 1139688
    :cond_0
    :goto_0
    return v0

    .line 1139689
    :cond_1
    instance-of v1, p1, LX/6kY;

    if-eqz v1, :cond_0

    .line 1139690
    check-cast p1, LX/6kY;

    invoke-virtual {p0, p1}, LX/6kY;->a(LX/6kY;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1139691
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1139692
    sget-boolean v0, LX/6kY;->a:Z

    .line 1139693
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kY;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1139694
    return-object v0
.end method
