.class public abstract LX/6tx;
.super LX/6sU;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "LX/6sU",
        "<",
        "Landroid/os/Parcelable;",
        "TRESU",
        "LT;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/common/PaymentNetworkOperationHelper;",
            "Ljava/lang/Class",
            "<TRESU",
            "LT;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1155426
    invoke-direct {p0, p1, p2}, LX/6sU;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 1155427
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 1

    .prologue
    .line 1155428
    invoke-virtual {p0}, LX/6tx;->b()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1155429
    const/4 v0, 0x0

    invoke-super {p0, v0}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(LX/1pN;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1pN;",
            ")TRESU",
            "LT;"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1155430
    invoke-virtual {p0, p2}, LX/6tx;->a(LX/1pN;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract b()LX/14N;
.end method
