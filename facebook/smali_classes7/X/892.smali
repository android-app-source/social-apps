.class public final LX/892;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;",
        ">;",
        "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/21D;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/1nC;


# direct methods
.method public constructor <init>(LX/1nC;JLX/21D;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1304097
    iput-object p1, p0, LX/892;->d:LX/1nC;

    iput-wide p2, p0, LX/892;->a:J

    iput-object p4, p0, LX/892;->b:LX/21D;

    iput-object p5, p0, LX/892;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;",
            ">;)",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;"
        }
    .end annotation

    .prologue
    .line 1304083
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1304084
    :cond_0
    const/4 v2, 0x0

    .line 1304085
    :goto_0
    return-object v2

    .line 1304086
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;

    .line 1304087
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->l()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, LX/15i;->g(II)I

    move-result v3

    .line 1304088
    new-instance v5, LX/89I;

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/892;->a:J

    sget-object v8, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v5, v6, v7, v8}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/89I;->a(Ljava/lang/String;)LX/89I;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v3, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/89I;->b(Ljava/lang/String;)LX/89I;

    move-result-object v3

    invoke-virtual {v3}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v4

    .line 1304089
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->k()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    sget-object v6, LX/893;->a:[I

    const/4 v7, 0x0

    const-class v8, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v5, v3, v7, v8, v9}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->ordinal()I

    move-result v3

    aget v3, v6, v3

    packed-switch v3, :pswitch_data_0

    .line 1304090
    const/4 v2, 0x0

    goto :goto_0

    .line 1304091
    :pswitch_0
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->k()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v6, v3, LX/1vs;->b:I

    .line 1304092
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->k()LX/1vs;

    move-result-object v3

    iget-object v7, v3, LX/1vs;->a:LX/15i;

    iget v8, v3, LX/1vs;->b:I

    .line 1304093
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->k()LX/1vs;

    move-result-object v3

    iget-object v9, v3, LX/1vs;->a:LX/15i;

    iget v10, v3, LX/1vs;->b:I

    .line 1304094
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->a()LX/1vs;

    move-result-object v3

    iget-object v11, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const/4 v12, 0x0

    const-class v13, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v11, v3, v12, v13}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    check-cast v3, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel$CoverPhotoModel$PhotoModel;->a()LX/1vs;

    move-result-object v3

    iget-object v11, v3, LX/1vs;->a:LX/15i;

    iget v12, v3, LX/1vs;->b:I

    .line 1304095
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->l()LX/1vs;

    move-result-object v3

    iget-object v13, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const/4 v14, 0x0

    invoke-virtual {v13, v3, v14}, LX/15i;->g(II)I

    move-result v14

    .line 1304096
    move-object/from16 v0, p0

    iget-object v15, v0, LX/892;->b:LX/21D;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/892;->c:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;

    move-result-object v17

    const/4 v3, 0x0

    const-class v18, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v19, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v6, v3, v0, v1}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->name()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->setCallToActionType(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->setTitle(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;

    move-result-object v3

    const/4 v5, 0x3

    invoke-virtual {v7, v8, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->setLabel(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;

    move-result-object v3

    const/4 v5, 0x1

    invoke-virtual {v9, v10, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->setLink(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v11, v12, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->setLinkImage(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerCallToAction$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-static {v15, v0, v3}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/model/ComposerCallToAction;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAllowTargetSelection(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/graphql/FetchPageCTAComposerPreviewModels$FetchPageCTAComposerPreviewModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v13, v14, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, LX/892;->d:LX/1nC;

    iget-object v4, v4, LX/1nC;->b:LX/0SI;

    invoke-interface {v4}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1304082
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/892;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    return-object v0
.end method
