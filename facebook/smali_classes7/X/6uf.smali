.class public LX/6uf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6uU;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1156065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156066
    return-void
.end method

.method public static a(LX/0QB;)LX/6uf;
    .locals 1

    .prologue
    .line 1156067
    new-instance v0, LX/6uf;

    invoke-direct {v0}, LX/6uf;-><init>()V

    .line 1156068
    move-object v0, v0

    .line 1156069
    return-object v0
.end method

.method private static d(Landroid/view/ViewGroup;)LX/6E8;
    .locals 4

    .prologue
    .line 1156070
    new-instance v1, LX/6uX;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03165e

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;

    invoke-direct {v1, v0}, LX/6uX;-><init>(Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;)V

    return-object v1
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;LX/6uT;)LX/6E8;
    .locals 3

    .prologue
    .line 1156071
    sget-object v0, LX/6ue;->a:[I

    invoke-virtual {p2}, LX/6uT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1156072
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled row : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1156073
    :pswitch_0
    invoke-static {p1}, LX/6uf;->d(Landroid/view/ViewGroup;)LX/6E8;

    move-result-object v0

    .line 1156074
    :goto_0
    return-object v0

    .line 1156075
    :pswitch_1
    new-instance v1, LX/6uK;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f031631

    const/4 p0, 0x0

    invoke-virtual {v0, v2, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/confirmation/ConfirmationCheckMarkRowView;

    invoke-direct {v1, v0}, LX/6uK;-><init>(Lcom/facebook/payments/confirmation/ConfirmationCheckMarkRowView;)V

    move-object v0, v1

    .line 1156076
    goto :goto_0

    .line 1156077
    :pswitch_2
    new-instance v0, LX/73W;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/73W;-><init>(Landroid/content/Context;)V

    .line 1156078
    new-instance v1, LX/73X;

    invoke-direct {v1, v0}, LX/73X;-><init>(LX/73W;)V

    move-object v0, v1

    .line 1156079
    goto :goto_0

    .line 1156080
    :pswitch_3
    new-instance v1, LX/6un;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f031661

    const/4 p0, 0x0

    invoke-virtual {v0, v2, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/confirmation/SimpleProductPurchaseRowView;

    invoke-direct {v1, v0}, LX/6un;-><init>(Lcom/facebook/payments/confirmation/SimpleProductPurchaseRowView;)V

    move-object v0, v1

    .line 1156081
    goto :goto_0

    .line 1156082
    :pswitch_4
    invoke-static {p1}, LX/6uf;->d(Landroid/view/ViewGroup;)LX/6E8;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
