.class public final enum LX/71I;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/71I;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/71I;

.field public static final enum ADD_CARD:LX/71I;

.field public static final enum ADD_CONTACT_INFORMATION:LX/71I;

.field public static final enum ADD_PAYPAL:LX/71I;

.field public static final enum ADD_SHIPPING_ADDRESS:LX/71I;

.field public static final enum AVAILABLE_PAYMENT_PROVIDER:LX/71I;

.field public static final enum COMMERCE_COMMERCE_SIMPLE_ADD_PAYMENT_METHOD:LX/71I;

.field public static final enum CONTACT_INFORMATION:LX/71I;

.field public static final enum COUNTRY_SELECTOR:LX/71I;

.field public static final enum EXISTING_PAYMENT_METHOD:LX/71I;

.field public static final enum HEADER:LX/71I;

.field public static final enum LOADING_MORE:LX/71I;

.field public static final enum PAYMENTS_PICKER_OPTION:LX/71I;

.field public static final enum PAYMENT_HISTORY:LX/71I;

.field public static final enum PAYMENT_HISTORY_NUX_ROW:LX/71I;

.field public static final enum PAYMENT_SETTINGS_ACTION:LX/71I;

.field public static final enum PIN:LX/71I;

.field public static final enum SECURITY_FOOTER:LX/71I;

.field public static final enum SHIPPING_ADDRESS:LX/71I;

.field public static final enum SHIPPING_OPTION:LX/71I;

.field public static final enum SINGLE_ROW_DIVIDER:LX/71I;

.field public static final enum SPACED_DOUBLE_ROW_DIVIDER:LX/71I;


# instance fields
.field private final mSelectable:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1163212
    new-instance v0, LX/71I;

    const-string v1, "COUNTRY_SELECTOR"

    invoke-direct {v0, v1, v4}, LX/71I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71I;->COUNTRY_SELECTOR:LX/71I;

    .line 1163213
    new-instance v0, LX/71I;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v3}, LX/71I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71I;->HEADER:LX/71I;

    .line 1163214
    new-instance v0, LX/71I;

    const-string v1, "EXISTING_PAYMENT_METHOD"

    invoke-direct {v0, v1, v5, v3}, LX/71I;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/71I;->EXISTING_PAYMENT_METHOD:LX/71I;

    .line 1163215
    new-instance v0, LX/71I;

    const-string v1, "SINGLE_ROW_DIVIDER"

    invoke-direct {v0, v1, v6}, LX/71I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71I;->SINGLE_ROW_DIVIDER:LX/71I;

    .line 1163216
    new-instance v0, LX/71I;

    const-string v1, "SPACED_DOUBLE_ROW_DIVIDER"

    invoke-direct {v0, v1, v7}, LX/71I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71I;->SPACED_DOUBLE_ROW_DIVIDER:LX/71I;

    .line 1163217
    new-instance v0, LX/71I;

    const-string v1, "ADD_CARD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/71I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71I;->ADD_CARD:LX/71I;

    .line 1163218
    new-instance v0, LX/71I;

    const-string v1, "ADD_PAYPAL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/71I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71I;->ADD_PAYPAL:LX/71I;

    .line 1163219
    new-instance v0, LX/71I;

    const-string v1, "ADD_SHIPPING_ADDRESS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/71I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71I;->ADD_SHIPPING_ADDRESS:LX/71I;

    .line 1163220
    new-instance v0, LX/71I;

    const-string v1, "ADD_CONTACT_INFORMATION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/71I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71I;->ADD_CONTACT_INFORMATION:LX/71I;

    .line 1163221
    new-instance v0, LX/71I;

    const-string v1, "AVAILABLE_PAYMENT_PROVIDER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/71I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71I;->AVAILABLE_PAYMENT_PROVIDER:LX/71I;

    .line 1163222
    new-instance v0, LX/71I;

    const-string v1, "COMMERCE_COMMERCE_SIMPLE_ADD_PAYMENT_METHOD"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3}, LX/71I;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/71I;->COMMERCE_COMMERCE_SIMPLE_ADD_PAYMENT_METHOD:LX/71I;

    .line 1163223
    new-instance v0, LX/71I;

    const-string v1, "PIN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v3}, LX/71I;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/71I;->PIN:LX/71I;

    .line 1163224
    new-instance v0, LX/71I;

    const-string v1, "SHIPPING_ADDRESS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, LX/71I;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/71I;->SHIPPING_ADDRESS:LX/71I;

    .line 1163225
    new-instance v0, LX/71I;

    const-string v1, "SHIPPING_OPTION"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v3}, LX/71I;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/71I;->SHIPPING_OPTION:LX/71I;

    .line 1163226
    new-instance v0, LX/71I;

    const-string v1, "CONTACT_INFORMATION"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2, v3}, LX/71I;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/71I;->CONTACT_INFORMATION:LX/71I;

    .line 1163227
    new-instance v0, LX/71I;

    const-string v1, "PAYMENT_HISTORY_NUX_ROW"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/71I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71I;->PAYMENT_HISTORY_NUX_ROW:LX/71I;

    .line 1163228
    new-instance v0, LX/71I;

    const-string v1, "PAYMENT_HISTORY"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2, v3}, LX/71I;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/71I;->PAYMENT_HISTORY:LX/71I;

    .line 1163229
    new-instance v0, LX/71I;

    const-string v1, "PAYMENT_SETTINGS_ACTION"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2, v3}, LX/71I;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/71I;->PAYMENT_SETTINGS_ACTION:LX/71I;

    .line 1163230
    new-instance v0, LX/71I;

    const-string v1, "PAYMENTS_PICKER_OPTION"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2, v3}, LX/71I;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/71I;->PAYMENTS_PICKER_OPTION:LX/71I;

    .line 1163231
    new-instance v0, LX/71I;

    const-string v1, "SECURITY_FOOTER"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/71I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71I;->SECURITY_FOOTER:LX/71I;

    .line 1163232
    new-instance v0, LX/71I;

    const-string v1, "LOADING_MORE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/71I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/71I;->LOADING_MORE:LX/71I;

    .line 1163233
    const/16 v0, 0x15

    new-array v0, v0, [LX/71I;

    sget-object v1, LX/71I;->COUNTRY_SELECTOR:LX/71I;

    aput-object v1, v0, v4

    sget-object v1, LX/71I;->HEADER:LX/71I;

    aput-object v1, v0, v3

    sget-object v1, LX/71I;->EXISTING_PAYMENT_METHOD:LX/71I;

    aput-object v1, v0, v5

    sget-object v1, LX/71I;->SINGLE_ROW_DIVIDER:LX/71I;

    aput-object v1, v0, v6

    sget-object v1, LX/71I;->SPACED_DOUBLE_ROW_DIVIDER:LX/71I;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/71I;->ADD_CARD:LX/71I;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/71I;->ADD_PAYPAL:LX/71I;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/71I;->ADD_SHIPPING_ADDRESS:LX/71I;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/71I;->ADD_CONTACT_INFORMATION:LX/71I;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/71I;->AVAILABLE_PAYMENT_PROVIDER:LX/71I;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/71I;->COMMERCE_COMMERCE_SIMPLE_ADD_PAYMENT_METHOD:LX/71I;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/71I;->PIN:LX/71I;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/71I;->SHIPPING_ADDRESS:LX/71I;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/71I;->SHIPPING_OPTION:LX/71I;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/71I;->CONTACT_INFORMATION:LX/71I;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/71I;->PAYMENT_HISTORY_NUX_ROW:LX/71I;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/71I;->PAYMENT_HISTORY:LX/71I;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/71I;->PAYMENT_SETTINGS_ACTION:LX/71I;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/71I;->PAYMENTS_PICKER_OPTION:LX/71I;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/71I;->SECURITY_FOOTER:LX/71I;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/71I;->LOADING_MORE:LX/71I;

    aput-object v2, v0, v1

    sput-object v0, LX/71I;->$VALUES:[LX/71I;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1163209
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1163210
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/71I;->mSelectable:Z

    .line 1163211
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 1163234
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1163235
    iput-boolean p3, p0, LX/71I;->mSelectable:Z

    .line 1163236
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/71I;
    .locals 1

    .prologue
    .line 1163208
    const-class v0, LX/71I;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/71I;

    return-object v0
.end method

.method public static values()[LX/71I;
    .locals 1

    .prologue
    .line 1163207
    sget-object v0, LX/71I;->$VALUES:[LX/71I;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/71I;

    return-object v0
.end method


# virtual methods
.method public final isSelectable()Z
    .locals 1

    .prologue
    .line 1163206
    iget-boolean v0, p0, LX/71I;->mSelectable:Z

    return v0
.end method
