.class public LX/7Jr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Jq;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/36s;

.field public final c:Landroid/media/MediaPlayer;

.field private final d:LX/1m0;

.field public final e:Landroid/content/Context;

.field public final f:Landroid/net/Uri;

.field private final g:LX/0So;

.field private final h:J

.field private final i:LX/3FK;

.field private final j:LX/7Q0;

.field public final k:LX/0TD;

.field public l:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1194172
    const-class v0, LX/7Jr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Jr;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/36s;Landroid/media/MediaPlayer;LX/1m0;Landroid/content/Context;Landroid/net/Uri;LX/0So;LX/3FK;LX/7Q0;LX/0TD;)V
    .locals 2
    .param p9    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/VideoPerformanceExecutor;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1194173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194174
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/7Jr;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1194175
    iput-boolean v1, p0, LX/7Jr;->m:Z

    .line 1194176
    iput-object p6, p0, LX/7Jr;->g:LX/0So;

    .line 1194177
    iput-boolean v1, p0, LX/7Jr;->m:Z

    .line 1194178
    iput-object p1, p0, LX/7Jr;->b:LX/36s;

    .line 1194179
    iput-object p2, p0, LX/7Jr;->c:Landroid/media/MediaPlayer;

    .line 1194180
    iput-object p3, p0, LX/7Jr;->d:LX/1m0;

    .line 1194181
    iput-object p4, p0, LX/7Jr;->e:Landroid/content/Context;

    .line 1194182
    iput-object p5, p0, LX/7Jr;->f:Landroid/net/Uri;

    .line 1194183
    iget-object v0, p0, LX/7Jr;->g:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/7Jr;->h:J

    .line 1194184
    iput-object p7, p0, LX/7Jr;->i:LX/3FK;

    .line 1194185
    iput-object p8, p0, LX/7Jr;->j:LX/7Q0;

    .line 1194186
    iput-object p9, p0, LX/7Jr;->k:LX/0TD;

    .line 1194187
    const/4 p1, 0x0

    .line 1194188
    iget-object v0, p0, LX/7Jr;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1194189
    :goto_0
    return-void

    .line 1194190
    :cond_0
    iput-boolean p1, p0, LX/7Jr;->m:Z

    .line 1194191
    iget-object v0, p0, LX/7Jr;->k:LX/0TD;

    new-instance v1, Lcom/facebook/video/engine/DirectPlayMediaPlayer$1;

    invoke-direct {v1, p0}, Lcom/facebook/video/engine/DirectPlayMediaPlayer$1;-><init>(LX/7Jr;)V

    const p1, 0x5f6d7454

    invoke-static {v0, v1, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public static a$redex0(LX/7Jr;Landroid/media/MediaPlayer;Landroid/net/Uri;)V
    .locals 18

    .prologue
    .line 1194192
    invoke-static/range {p2 .. p2}, LX/1m0;->f(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static/range {p2 .. p2}, LX/1m0;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    move-object v9, v2

    .line 1194193
    :goto_0
    invoke-static/range {p2 .. p2}, LX/1m0;->g(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v10

    .line 1194194
    invoke-static/range {p2 .. p2}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v11

    .line 1194195
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7Jr;->g:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Landroid/net/Uri;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".mp4"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1194196
    move-object/from16 v0, p0

    iget-object v12, v0, LX/7Jr;->d:LX/1m0;

    const-wide/16 v14, 0x0

    const-wide/16 v16, -0x1

    new-instance v2, LX/7Jp;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/7Jr;->j:LX/7Q0;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7Jr;->i:LX/3FK;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7Jr;->b:LX/36s;

    move-object/from16 v3, p0

    move-object/from16 v8, p1

    invoke-direct/range {v2 .. v8}, LX/7Jp;-><init>(LX/7Jr;LX/7Q0;LX/3FK;Ljava/lang/String;LX/36s;Landroid/media/MediaPlayer;)V

    move-object v4, v12

    move-object v5, v9

    move-wide v6, v14

    move-wide/from16 v8, v16

    move-object v12, v2

    invoke-virtual/range {v4 .. v12}, LX/1m0;->a(Landroid/net/Uri;JJLjava/lang/String;ILX/3Di;)V

    .line 1194197
    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Jr;->j:LX/7Q0;

    invoke-virtual {v2}, LX/7Q0;->U_()V

    .line 1194198
    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Jr;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1194199
    return-void

    :cond_0
    move-object/from16 v9, p2

    .line 1194200
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1194201
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7Jr;->m:Z

    .line 1194202
    return-void
.end method

.method public final b()Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 1194203
    iget-object v0, p0, LX/7Jr;->c:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method public final c()LX/2qD;
    .locals 1

    .prologue
    .line 1194204
    sget-object v0, LX/2qD;->STATE_PREPARING:LX/2qD;

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 1194205
    iget-wide v0, p0, LX/7Jr;->h:J

    return-wide v0
.end method
