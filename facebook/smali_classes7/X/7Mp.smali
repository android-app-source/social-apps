.class public final LX/7Mp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V
    .locals 0

    .prologue
    .line 1199038
    iput-object p1, p0, LX/7Mp;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;B)V
    .locals 0

    .prologue
    .line 1199037
    invoke-direct {p0, p1}, LX/7Mp;-><init>(Lcom/facebook/video/player/plugins/FullScreenCastPlugin;)V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2

    .prologue
    .line 1199027
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1199028
    if-nez p3, :cond_0

    .line 1199029
    :goto_0
    return-void

    .line 1199030
    :cond_0
    iget-object v0, p0, LX/7Mp;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->a:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getMax()I

    move-result v0

    .line 1199031
    iget-object v1, p0, LX/7Mp;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iget v1, v1, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->B:I

    mul-int/2addr v1, p2

    div-int v0, v1, v0

    iput v0, p0, LX/7Mp;->b:I

    goto :goto_0
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 1199036
    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4

    .prologue
    .line 1199032
    iget-object v0, p0, LX/7Mp;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->I:LX/7J3;

    const-string v1, "plugin.seek"

    iget v2, p0, LX/7Mp;->b:I

    invoke-virtual {v0, v1, v2}, LX/7J3;->b(Ljava/lang/String;I)V

    .line 1199033
    iget-object v0, p0, LX/7Mp;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->R:LX/37Y;

    iget v1, p0, LX/7Mp;->b:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, LX/37Y;->a(J)V

    .line 1199034
    iget-object v0, p0, LX/7Mp;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iget v1, p0, LX/7Mp;->b:I

    iget-object v2, p0, LX/7Mp;->a:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iget v2, v2, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->B:I

    invoke-virtual {v0, v1, v2}, LX/7Mr;->b(II)V

    .line 1199035
    return-void
.end method
