.class public LX/7Dx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Dw;


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLPhotoEncoding;


# direct methods
.method private constructor <init>(Lcom/facebook/graphql/model/GraphQLPhotoEncoding;)V
    .locals 0

    .prologue
    .line 1184084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184085
    iput-object p1, p0, LX/7Dx;->a:Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    .line 1184086
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLMedia;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/7Dw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1184087
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1184088
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    .line 1184089
    new-instance v5, LX/7Dx;

    invoke-direct {v5, v0}, LX/7Dx;-><init>(Lcom/facebook/graphql/model/GraphQLPhotoEncoding;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184090
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1184091
    :cond_0
    return-object v2
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1184092
    iget-object v0, p0, LX/7Dx;->a:Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/19o;
    .locals 1

    .prologue
    .line 1184093
    iget-object v0, p0, LX/7Dx;->a:Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/19o;->fromString(Ljava/lang/String;)LX/19o;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1184094
    iget-object v0, p0, LX/7Dx;->a:Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1184095
    iget-object v0, p0, LX/7Dx;->a:Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->n()I

    move-result v0

    return v0
.end method

.method public final e()LX/7Dy;
    .locals 2

    .prologue
    .line 1184096
    iget-object v0, p0, LX/7Dx;->a:Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->m()Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;

    move-result-object v0

    .line 1184097
    new-instance v1, LX/7Dz;

    invoke-direct {v1, v0}, LX/7Dz;-><init>(Lcom/facebook/graphql/model/GraphQLPhotosphereMetadata;)V

    return-object v1
.end method

.method public final f()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/spherical/photo/model/PhotoTile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1184098
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1184099
    iget-object v0, p0, LX/7Dx;->a:Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoEncoding;->o()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhotoTile;

    .line 1184100
    new-instance v5, LX/7Dm;

    invoke-direct {v5}, LX/7Dm;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoTile;->k()I

    move-result v6

    .line 1184101
    iput v6, v5, LX/7Dm;->a:I

    .line 1184102
    move-object v5, v5

    .line 1184103
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoTile;->j()I

    move-result v6

    .line 1184104
    iput v6, v5, LX/7Dm;->b:I

    .line 1184105
    move-object v5, v5

    .line 1184106
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoTile;->a()I

    move-result v6

    .line 1184107
    iput v6, v5, LX/7Dm;->c:I

    .line 1184108
    move-object v5, v5

    .line 1184109
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoTile;->l()I

    move-result v6

    .line 1184110
    iput v6, v5, LX/7Dm;->d:I

    .line 1184111
    move-object v5, v5

    .line 1184112
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhotoTile;->m()Ljava/lang/String;

    move-result-object v0

    .line 1184113
    iput-object v0, v5, LX/7Dm;->e:Ljava/lang/String;

    .line 1184114
    move-object v0, v5

    .line 1184115
    invoke-virtual {v0}, LX/7Dm;->a()Lcom/facebook/spherical/photo/model/PhotoTile;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1184116
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1184117
    :cond_0
    return-object v2
.end method
