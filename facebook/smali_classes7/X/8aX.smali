.class public final LX/8aX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 35

    .prologue
    .line 1367945
    const/16 v31, 0x0

    .line 1367946
    const/16 v30, 0x0

    .line 1367947
    const/16 v29, 0x0

    .line 1367948
    const/16 v28, 0x0

    .line 1367949
    const/16 v27, 0x0

    .line 1367950
    const/16 v26, 0x0

    .line 1367951
    const/16 v25, 0x0

    .line 1367952
    const/16 v24, 0x0

    .line 1367953
    const/16 v23, 0x0

    .line 1367954
    const/16 v22, 0x0

    .line 1367955
    const/16 v21, 0x0

    .line 1367956
    const/16 v20, 0x0

    .line 1367957
    const/16 v19, 0x0

    .line 1367958
    const/16 v18, 0x0

    .line 1367959
    const/16 v17, 0x0

    .line 1367960
    const/16 v16, 0x0

    .line 1367961
    const/4 v15, 0x0

    .line 1367962
    const/4 v14, 0x0

    .line 1367963
    const/4 v13, 0x0

    .line 1367964
    const/4 v12, 0x0

    .line 1367965
    const/4 v11, 0x0

    .line 1367966
    const/4 v10, 0x0

    .line 1367967
    const/4 v9, 0x0

    .line 1367968
    const/4 v8, 0x0

    .line 1367969
    const/4 v7, 0x0

    .line 1367970
    const/4 v6, 0x0

    .line 1367971
    const/4 v5, 0x0

    .line 1367972
    const/4 v4, 0x0

    .line 1367973
    const/4 v3, 0x0

    .line 1367974
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v32

    sget-object v33, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-eq v0, v1, :cond_1

    .line 1367975
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1367976
    const/4 v3, 0x0

    .line 1367977
    :goto_0
    return v3

    .line 1367978
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1367979
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v32

    sget-object v33, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    if-eq v0, v1, :cond_1d

    .line 1367980
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v32

    .line 1367981
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1367982
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v33

    sget-object v34, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    if-eq v0, v1, :cond_1

    if-eqz v32, :cond_1

    .line 1367983
    const-string v33, "background_color"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_2

    .line 1367984
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v31

    goto :goto_1

    .line 1367985
    :cond_2
    const-string v33, "block_quote_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_3

    .line 1367986
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v30

    goto :goto_1

    .line 1367987
    :cond_3
    const-string v33, "body_text_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_4

    .line 1367988
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v29

    goto :goto_1

    .line 1367989
    :cond_4
    const-string v33, "byline"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_5

    .line 1367990
    invoke-static/range {p0 .. p1}, LX/8Zq;->a(LX/15w;LX/186;)I

    move-result v28

    goto :goto_1

    .line 1367991
    :cond_5
    const-string v33, "byline_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_6

    .line 1367992
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v27

    goto :goto_1

    .line 1367993
    :cond_6
    const-string v33, "caption_credit_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_7

    .line 1367994
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v26

    goto :goto_1

    .line 1367995
    :cond_7
    const-string v33, "caption_description_extra_large_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_8

    .line 1367996
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 1367997
    :cond_8
    const-string v33, "caption_description_large_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_9

    .line 1367998
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 1367999
    :cond_9
    const-string v33, "caption_description_medium_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_a

    .line 1368000
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 1368001
    :cond_a
    const-string v33, "caption_description_small_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_b

    .line 1368002
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 1368003
    :cond_b
    const-string v33, "caption_title_extra_large_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_c

    .line 1368004
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 1368005
    :cond_c
    const-string v33, "caption_title_large_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_d

    .line 1368006
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 1368007
    :cond_d
    const-string v33, "caption_title_medium_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_e

    .line 1368008
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 1368009
    :cond_e
    const-string v33, "caption_title_small_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_f

    .line 1368010
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 1368011
    :cond_f
    const-string v33, "custom_fonts"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_10

    .line 1368012
    invoke-static/range {p0 .. p1}, LX/8a1;->b(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 1368013
    :cond_10
    const-string v33, "end_credits_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_11

    .line 1368014
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 1368015
    :cond_11
    const-string v33, "fallback_article_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_12

    .line 1368016
    invoke-static/range {p0 .. p1}, LX/8aW;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1368017
    :cond_12
    const-string v33, "header_one_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_13

    .line 1368018
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 1368019
    :cond_13
    const-string v33, "header_two_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_14

    .line 1368020
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1368021
    :cond_14
    const-string v33, "include_end_credits"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_15

    .line 1368022
    const/4 v3, 0x1

    .line 1368023
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 1368024
    :cond_15
    const-string v33, "kicker_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_16

    .line 1368025
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1368026
    :cond_16
    const-string v33, "link_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_17

    .line 1368027
    invoke-static/range {p0 .. p1}, LX/8a7;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1368028
    :cond_17
    const-string v33, "logo"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_18

    .line 1368029
    invoke-static/range {p0 .. p1}, LX/8aC;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 1368030
    :cond_18
    const-string v33, "pull_quote_attribution_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_19

    .line 1368031
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1368032
    :cond_19
    const-string v33, "pull_quote_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_1a

    .line 1368033
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 1368034
    :cond_1a
    const-string v33, "related_articles_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_1b

    .line 1368035
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1368036
    :cond_1b
    const-string v33, "subtitle_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-eqz v33, :cond_1c

    .line 1368037
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1368038
    :cond_1c
    const-string v33, "title_style"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_0

    .line 1368039
    invoke-static/range {p0 .. p1}, LX/8Zw;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1368040
    :cond_1d
    const/16 v32, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1368041
    const/16 v32, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v32

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368042
    const/16 v31, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368043
    const/16 v30, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368044
    const/16 v29, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v29

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368045
    const/16 v28, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368046
    const/16 v27, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368047
    const/16 v26, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368048
    const/16 v25, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368049
    const/16 v24, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368050
    const/16 v23, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368051
    const/16 v22, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368052
    const/16 v21, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368053
    const/16 v20, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368054
    const/16 v19, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368055
    const/16 v18, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368056
    const/16 v17, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1368057
    const/16 v16, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1368058
    const/16 v15, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1368059
    const/16 v14, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1368060
    if-eqz v3, :cond_1e

    .line 1368061
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->a(IZ)V

    .line 1368062
    :cond_1e
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1368063
    const/16 v3, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1368064
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1368065
    const/16 v3, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1368066
    const/16 v3, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1368067
    const/16 v3, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1368068
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1368069
    const/16 v3, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1368070
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1368071
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1368072
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1368073
    if-eqz v0, :cond_0

    .line 1368074
    const-string v1, "background_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368075
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1368076
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368077
    if-eqz v0, :cond_1

    .line 1368078
    const-string v1, "block_quote_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368079
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368080
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368081
    if-eqz v0, :cond_2

    .line 1368082
    const-string v1, "body_text_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368083
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368084
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368085
    if-eqz v0, :cond_3

    .line 1368086
    const-string v1, "byline"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368087
    invoke-static {p0, v0, p2}, LX/8Zq;->a(LX/15i;ILX/0nX;)V

    .line 1368088
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368089
    if-eqz v0, :cond_4

    .line 1368090
    const-string v1, "byline_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368091
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368092
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368093
    if-eqz v0, :cond_5

    .line 1368094
    const-string v1, "caption_credit_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368095
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368096
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368097
    if-eqz v0, :cond_6

    .line 1368098
    const-string v1, "caption_description_extra_large_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368099
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368100
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368101
    if-eqz v0, :cond_7

    .line 1368102
    const-string v1, "caption_description_large_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368103
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368104
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368105
    if-eqz v0, :cond_8

    .line 1368106
    const-string v1, "caption_description_medium_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368107
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368108
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368109
    if-eqz v0, :cond_9

    .line 1368110
    const-string v1, "caption_description_small_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368111
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368112
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368113
    if-eqz v0, :cond_a

    .line 1368114
    const-string v1, "caption_title_extra_large_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368115
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368116
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368117
    if-eqz v0, :cond_b

    .line 1368118
    const-string v1, "caption_title_large_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368119
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368120
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368121
    if-eqz v0, :cond_c

    .line 1368122
    const-string v1, "caption_title_medium_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368123
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368124
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368125
    if-eqz v0, :cond_d

    .line 1368126
    const-string v1, "caption_title_small_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368127
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368128
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368129
    if-eqz v0, :cond_e

    .line 1368130
    const-string v1, "custom_fonts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368131
    invoke-static {p0, v0, p2, p3}, LX/8a1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368132
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368133
    if-eqz v0, :cond_f

    .line 1368134
    const-string v1, "end_credits_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368135
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368136
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368137
    if-eqz v0, :cond_10

    .line 1368138
    const-string v1, "fallback_article_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368139
    invoke-static {p0, v0, p2, p3}, LX/8aW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368140
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368141
    if-eqz v0, :cond_11

    .line 1368142
    const-string v1, "header_one_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368143
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368144
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368145
    if-eqz v0, :cond_12

    .line 1368146
    const-string v1, "header_two_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368147
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368148
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1368149
    if-eqz v0, :cond_13

    .line 1368150
    const-string v1, "include_end_credits"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368151
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1368152
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368153
    if-eqz v0, :cond_14

    .line 1368154
    const-string v1, "kicker_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368155
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368156
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368157
    if-eqz v0, :cond_15

    .line 1368158
    const-string v1, "link_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368159
    invoke-static {p0, v0, p2}, LX/8a7;->a(LX/15i;ILX/0nX;)V

    .line 1368160
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368161
    if-eqz v0, :cond_16

    .line 1368162
    const-string v1, "logo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368163
    invoke-static {p0, v0, p2}, LX/8aC;->a(LX/15i;ILX/0nX;)V

    .line 1368164
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368165
    if-eqz v0, :cond_17

    .line 1368166
    const-string v1, "pull_quote_attribution_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368167
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368168
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368169
    if-eqz v0, :cond_18

    .line 1368170
    const-string v1, "pull_quote_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368171
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368172
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368173
    if-eqz v0, :cond_19

    .line 1368174
    const-string v1, "related_articles_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368175
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368176
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368177
    if-eqz v0, :cond_1a

    .line 1368178
    const-string v1, "subtitle_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368179
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368180
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1368181
    if-eqz v0, :cond_1b

    .line 1368182
    const-string v1, "title_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1368183
    invoke-static {p0, v0, p2, p3}, LX/8Zw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1368184
    :cond_1b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1368185
    return-void
.end method
