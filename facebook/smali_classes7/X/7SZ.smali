.class public LX/7SZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Lcom/facebook/common/callercontext/CallerContext;

.field private final c:LX/BVF;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/1HI;

.field public final f:Ljava/util/concurrent/locks/Lock;

.field private g:LX/5Pf;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field public h:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;LX/BVF;Ljava/util/concurrent/ExecutorService;LX/1HI;)V
    .locals 1
    .param p1    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BVF;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1208976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1208977
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, LX/7SZ;->f:Ljava/util/concurrent/locks/Lock;

    .line 1208978
    iput-object p1, p0, LX/7SZ;->a:Landroid/net/Uri;

    .line 1208979
    iput-object p2, p0, LX/7SZ;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 1208980
    iput-object p3, p0, LX/7SZ;->c:LX/BVF;

    .line 1208981
    iput-object p4, p0, LX/7SZ;->d:Ljava/util/concurrent/ExecutorService;

    .line 1208982
    iput-object p5, p0, LX/7SZ;->e:LX/1HI;

    .line 1208983
    return-void
.end method


# virtual methods
.method public final a(LX/5Pa;)V
    .locals 4

    .prologue
    .line 1208984
    iget-object v0, p0, LX/7SZ;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1208985
    :goto_0
    return-void

    .line 1208986
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/7SZ;->g:LX/5Pf;

    if-nez v0, :cond_3

    .line 1208987
    iget-object v0, p0, LX/7SZ;->h:LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 1208988
    iget-object v0, p0, LX/7SZ;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 1208989
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/7SZ;->h:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1208990
    if-eqz v0, :cond_2

    .line 1208991
    new-instance v1, LX/5Pe;

    invoke-direct {v1}, LX/5Pe;-><init>()V

    .line 1208992
    iput-object v0, v1, LX/5Pe;->c:Landroid/graphics/Bitmap;

    .line 1208993
    move-object v1, v1

    .line 1208994
    invoke-virtual {v1}, LX/5Pe;->a()LX/5Pf;

    move-result-object v1

    iput-object v1, p0, LX/7SZ;->g:LX/5Pf;

    .line 1208995
    iget-object v1, p0, LX/7SZ;->c:LX/BVF;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 1208996
    invoke-static {v1}, LX/BVF;->g(LX/BVF;)Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;->setTextureSize(II)V

    .line 1208997
    :cond_2
    iget-object v0, p0, LX/7SZ;->h:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1208998
    const/4 v0, 0x0

    iput-object v0, p0, LX/7SZ;->h:LX/1FJ;

    .line 1208999
    :cond_3
    const-string v0, "uTexture"

    iget-object v1, p0, LX/7SZ;->g:LX/5Pf;

    invoke-virtual {p1, v0, v1}, LX/5Pa;->a(Ljava/lang/String;LX/5Pf;)LX/5Pa;

    move-result-object v0

    const-string v1, "uBirthAnimationNumberOfSprites"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/5Pa;->a(Ljava/lang/String;F)LX/5Pa;

    move-result-object v0

    const-string v1, "uDeathAnimationNumberOfSprites"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/5Pa;->a(Ljava/lang/String;F)LX/5Pa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1209000
    iget-object v0, p0, LX/7SZ;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/7SZ;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1209001
    iget-object v0, p0, LX/7SZ;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1209002
    :try_start_0
    iget-object v0, p0, LX/7SZ;->g:LX/5Pf;

    if-eqz v0, :cond_0

    .line 1209003
    iget-object v0, p0, LX/7SZ;->g:LX/5Pf;

    invoke-virtual {v0}, LX/5Pf;->a()V

    .line 1209004
    const/4 v0, 0x0

    iput-object v0, p0, LX/7SZ;->g:LX/5Pf;

    .line 1209005
    :cond_0
    iget-object v0, p0, LX/7SZ;->h:LX/1FJ;

    if-eqz v0, :cond_1

    .line 1209006
    iget-object v0, p0, LX/7SZ;->h:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1209007
    const/4 v0, 0x0

    iput-object v0, p0, LX/7SZ;->h:LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1209008
    :cond_1
    iget-object v0, p0, LX/7SZ;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1209009
    return-void

    .line 1209010
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/7SZ;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
