.class public final LX/7TT;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/Runnable;

.field public final synthetic b:LX/2Tm;


# direct methods
.method public constructor <init>(LX/2Tm;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1210693
    iput-object p1, p0, LX/7TT;->b:LX/2Tm;

    iput-object p2, p0, LX/7TT;->a:Ljava/lang/Runnable;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1210673
    const/4 v0, 0x0

    .line 1210674
    iget-object v1, p0, LX/7TT;->b:LX/2Tm;

    iget-object v1, v1, LX/2Tm;->g:LX/2Hu;

    invoke-virtual {v1}, LX/2Hu;->a()LX/2gV;

    move-result-object v2

    .line 1210675
    :try_start_0
    iget-object v1, p0, LX/7TT;->b:LX/2Tm;

    iget-object v1, v1, LX/2Tm;->e:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    const-wide/16 v6, 0x2710

    add-long/2addr v4, v6

    .line 1210676
    :cond_0
    invoke-virtual {v2}, LX/2gV;->b()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 1210677
    const-wide/16 v6, 0x32

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1210678
    :goto_0
    :try_start_2
    iget-object v1, p0, LX/7TT;->b:LX/2Tm;

    iget-object v1, v1, LX/2Tm;->e:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v6

    cmp-long v1, v6, v4

    if-lez v1, :cond_0

    .line 1210679
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 1210680
    invoke-virtual {v2}, LX/2gV;->f()V

    .line 1210681
    :goto_1
    return-object v0

    .line 1210682
    :cond_1
    const/4 v0, 0x1

    .line 1210683
    invoke-virtual {v2}, LX/2gV;->f()V

    .line 1210684
    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 1210685
    :catch_0
    move-exception v1

    .line 1210686
    :try_start_3
    sget-object v3, LX/2Tm;->a:Ljava/lang/Class;

    const-string v4, "Exception waiting for mqtt to connect"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1210687
    invoke-virtual {v2}, LX/2gV;->f()V

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/2gV;->f()V

    throw v0

    :catch_1
    goto :goto_0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1210688
    check-cast p1, Ljava/lang/Boolean;

    .line 1210689
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1210690
    sget-object v0, LX/2Tm;->a:Ljava/lang/Class;

    const-string v1, "Could not connect to MQTT service in %d ms"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-wide/16 v4, 0x2710

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1210691
    :cond_0
    iget-object v0, p0, LX/7TT;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1210692
    return-void
.end method
