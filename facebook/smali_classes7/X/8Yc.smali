.class public LX/8Yc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/8Yc;


# instance fields
.field public final b:Ljava/io/File;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "LX/8Yb;",
            "LX/8Ya;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1356415
    const-class v0, LX/8Yc;

    sput-object v0, LX/8Yc;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/0lC;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1356416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1356417
    iput-object p2, p0, LX/8Yc;->c:LX/0Ot;

    .line 1356418
    iput-object p3, p0, LX/8Yc;->d:LX/0Ot;

    .line 1356419
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, LX/8Yc;->e:Landroid/util/LruCache;

    .line 1356420
    if-eqz p1, :cond_1

    .line 1356421
    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 1356422
    new-instance v1, Ljava/io/File;

    const-string v2, "fontResourceCache.json"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, LX/8Yc;->b:Ljava/io/File;

    .line 1356423
    :goto_0
    iget-object v0, p0, LX/8Yc;->e:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 1356424
    :try_start_0
    iget-object v0, p0, LX/8Yc;->b:Ljava/io/File;

    move-object v1, v0

    .line 1356425
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1356426
    iget-object v0, p0, LX/8Yc;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lC;

    const-class v2, LX/8YZ;

    invoke-virtual {v0, v1, v2}, LX/0lC;->a(Ljava/io/File;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8YZ;

    .line 1356427
    if-eqz v0, :cond_0

    .line 1356428
    iget-object v1, v0, LX/8YZ;->mResources:Ljava/util/List;

    move-object v0, v1

    .line 1356429
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Ya;

    .line 1356430
    new-instance v2, LX/8Yb;

    .line 1356431
    iget-object p1, v0, LX/8Ya;->mName:Ljava/lang/String;

    move-object p1, p1

    .line 1356432
    iget-object p2, v0, LX/8Ya;->mVersion:Ljava/lang/String;

    move-object p2, p2

    .line 1356433
    invoke-direct {v2, p1, p2}, LX/8Yb;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1356434
    iget-object p1, p0, LX/8Yc;->e:Landroid/util/LruCache;

    invoke-virtual {p1, v2, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1356435
    :catch_0
    move-exception v0

    .line 1356436
    sget-object v1, LX/8Yc;->a:Ljava/lang/Class;

    const-string v2, "Failed to load font resource cache file %s"

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    const-string p3, "fontResourceCache.json"

    aput-object p3, p1, p2

    invoke-static {v1, v0, v2, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1356437
    :cond_0
    return-void

    .line 1356438
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/8Yc;->b:Ljava/io/File;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/8Yc;
    .locals 6

    .prologue
    .line 1356402
    sget-object v0, LX/8Yc;->f:LX/8Yc;

    if-nez v0, :cond_1

    .line 1356403
    const-class v1, LX/8Yc;

    monitor-enter v1

    .line 1356404
    :try_start_0
    sget-object v0, LX/8Yc;->f:LX/8Yc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1356405
    if-eqz v2, :cond_0

    .line 1356406
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1356407
    new-instance v4, LX/8Yc;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v5, 0x2ba

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, LX/8Yc;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;)V

    .line 1356408
    move-object v0, v4

    .line 1356409
    sput-object v0, LX/8Yc;->f:LX/8Yc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1356410
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1356411
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1356412
    :cond_1
    sget-object v0, LX/8Yc;->f:LX/8Yc;

    return-object v0

    .line 1356413
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1356414
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/8Yb;)LX/8Ya;
    .locals 1

    .prologue
    .line 1356401
    iget-object v0, p0, LX/8Yc;->e:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Ya;

    return-object v0
.end method
