.class public final enum LX/8SM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8SM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8SM;

.field public static final enum AUDIENCE_SHOW_ALL:LX/8SM;

.field public static final enum AUDIENCE_SHOW_NONE:LX/8SM;

.field public static final enum AUDIENCE_SHOW_PUBLIC_FRIENDS_AND_CUSTOM:LX/8SM;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1346474
    new-instance v0, LX/8SM;

    const-string v1, "AUDIENCE_SHOW_ALL"

    invoke-direct {v0, v1, v2}, LX/8SM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8SM;->AUDIENCE_SHOW_ALL:LX/8SM;

    .line 1346475
    new-instance v0, LX/8SM;

    const-string v1, "AUDIENCE_SHOW_PUBLIC_FRIENDS_AND_CUSTOM"

    invoke-direct {v0, v1, v3}, LX/8SM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8SM;->AUDIENCE_SHOW_PUBLIC_FRIENDS_AND_CUSTOM:LX/8SM;

    .line 1346476
    new-instance v0, LX/8SM;

    const-string v1, "AUDIENCE_SHOW_NONE"

    invoke-direct {v0, v1, v4}, LX/8SM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8SM;->AUDIENCE_SHOW_NONE:LX/8SM;

    .line 1346477
    const/4 v0, 0x3

    new-array v0, v0, [LX/8SM;

    sget-object v1, LX/8SM;->AUDIENCE_SHOW_ALL:LX/8SM;

    aput-object v1, v0, v2

    sget-object v1, LX/8SM;->AUDIENCE_SHOW_PUBLIC_FRIENDS_AND_CUSTOM:LX/8SM;

    aput-object v1, v0, v3

    sget-object v1, LX/8SM;->AUDIENCE_SHOW_NONE:LX/8SM;

    aput-object v1, v0, v4

    sput-object v0, LX/8SM;->$VALUES:[LX/8SM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1346480
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8SM;
    .locals 1

    .prologue
    .line 1346479
    const-class v0, LX/8SM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8SM;

    return-object v0
.end method

.method public static values()[LX/8SM;
    .locals 1

    .prologue
    .line 1346478
    sget-object v0, LX/8SM;->$VALUES:[LX/8SM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8SM;

    return-object v0
.end method
