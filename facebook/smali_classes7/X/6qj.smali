.class public final LX/6qj;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/checkout/CheckoutFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/checkout/CheckoutFragment;)V
    .locals 0

    .prologue
    .line 1151057
    iput-object p1, p0, LX/6qj;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1151058
    iget-object v0, p0, LX/6qj;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    .line 1151059
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    .line 1151060
    if-eqz v1, :cond_0

    .line 1151061
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1151062
    :cond_0
    iget-object v0, p0, LX/6qj;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    invoke-static {v0}, Lcom/facebook/payments/checkout/CheckoutFragment;->z(Lcom/facebook/payments/checkout/CheckoutFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1151063
    iget-object v0, p0, LX/6qj;->a:Lcom/facebook/payments/checkout/CheckoutFragment;

    invoke-static {v0}, Lcom/facebook/payments/checkout/CheckoutFragment;->D(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    .line 1151064
    :cond_1
    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 0

    .prologue
    .line 1151065
    invoke-direct {p0}, LX/6qj;->a()V

    .line 1151066
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1151053
    invoke-direct {p0}, LX/6qj;->a()V

    .line 1151054
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1151055
    invoke-direct {p0}, LX/6qj;->a()V

    .line 1151056
    return-void
.end method
