.class public LX/7OG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/7OG;


# instance fields
.field public a:LX/0Tn;

.field public b:LX/0Tn;

.field public c:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1201272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1201273
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "quality_selector/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/7OG;->a:LX/0Tn;

    .line 1201274
    iget-object v0, p0, LX/7OG;->a:LX/0Tn;

    const-string v1, "user_selected_quality_label_key"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/7OG;->b:LX/0Tn;

    .line 1201275
    iput-object p1, p0, LX/7OG;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1201276
    return-void
.end method

.method public static a(LX/0QB;)LX/7OG;
    .locals 4

    .prologue
    .line 1201277
    sget-object v0, LX/7OG;->d:LX/7OG;

    if-nez v0, :cond_1

    .line 1201278
    const-class v1, LX/7OG;

    monitor-enter v1

    .line 1201279
    :try_start_0
    sget-object v0, LX/7OG;->d:LX/7OG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1201280
    if-eqz v2, :cond_0

    .line 1201281
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1201282
    new-instance p0, LX/7OG;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/7OG;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1201283
    move-object v0, p0

    .line 1201284
    sput-object v0, LX/7OG;->d:LX/7OG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1201285
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1201286
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1201287
    :cond_1
    sget-object v0, LX/7OG;->d:LX/7OG;

    return-object v0

    .line 1201288
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1201289
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1201270
    iget-object v1, p0, LX/7OG;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1201271
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/7OG;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, LX/7OG;->b:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
