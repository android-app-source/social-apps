.class public LX/8Cf;
.super LX/0gG;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0gG;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Sg;

.field private c:LX/8CX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/8CX",
            "<TT;>;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/8Ce",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private f:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1311453
    const-class v0, LX/8Cf;

    sput-object v0, LX/8Cf;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Sg;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1311446
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 1311447
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1311448
    iput-object v0, p0, LX/8Cf;->d:LX/0Px;

    .line 1311449
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/8Cf;->e:Ljava/util/Queue;

    .line 1311450
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/8Cf;->f:LX/0YU;

    .line 1311451
    iput-object p1, p0, LX/8Cf;->b:LX/0Sg;

    .line 1311452
    return-void
.end method

.method private a(Ljava/lang/Object;Landroid/view/ViewGroup;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1311435
    iget-object v0, p0, LX/8Cf;->c:LX/8CX;

    invoke-interface {v0, p1}, LX/8CX;->b(Ljava/lang/Object;)I

    move-result v0

    .line 1311436
    const/4 v2, -0x1

    if-eq v0, v2, :cond_3

    .line 1311437
    iget-object v2, p0, LX/8Cf;->f:LX/0YU;

    invoke-virtual {v2, v0}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1311438
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 1311439
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1311440
    :cond_0
    :goto_0
    iget-object v3, p0, LX/8Cf;->c:LX/8CX;

    iget-object v2, p0, LX/8Cf;->g:Ljava/lang/Object;

    if-ne p1, v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    invoke-interface {v3, p1, v1, p2, v2}, LX/8CX;->a(Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1311441
    if-eqz v1, :cond_1

    if-eq v2, v1, :cond_1

    .line 1311442
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1311443
    :cond_1
    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1311444
    return-void

    .line 1311445
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/8Cf;
    .locals 2

    .prologue
    .line 1311393
    new-instance v1, LX/8Cf;

    invoke-static {p0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v0

    check-cast v0, LX/0Sg;

    invoke-direct {v1, v0}, LX/8Cf;-><init>(LX/0Sg;)V

    .line 1311394
    return-object v1
.end method

.method private e()V
    .locals 5

    .prologue
    .line 1311432
    iget-object v0, p0, LX/8Cf;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1311433
    :goto_0
    return-void

    .line 1311434
    :cond_0
    iget-object v0, p0, LX/8Cf;->b:LX/0Sg;

    const-string v1, "TabbedPagerAdapter"

    new-instance v2, Lcom/facebook/messaging/tabbedpager/TabbedPagerAdapter$1;

    invoke-direct {v2, p0}, Lcom/facebook/messaging/tabbedpager/TabbedPagerAdapter$1;-><init>(LX/8Cf;)V

    sget-object v3, LX/0VZ;->APPLICATION_LOADED_HIGH_PRIORITY:LX/0VZ;

    sget-object v4, LX/0Vm;->UI:LX/0Vm;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    goto :goto_0
.end method

.method public static f(LX/8Cf;)V
    .locals 3

    .prologue
    .line 1311427
    iget-object v0, p0, LX/8Cf;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1311428
    :goto_0
    return-void

    .line 1311429
    :cond_0
    iget-object v0, p0, LX/8Cf;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Ce;

    .line 1311430
    iget-object v1, p0, LX/8Cf;->d:LX/0Px;

    iget v2, v0, LX/8Ce;->a:I

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v0, v0, LX/8Ce;->b:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v0}, LX/8Cf;->a(Ljava/lang/Object;Landroid/view/ViewGroup;)V

    .line 1311431
    invoke-direct {p0}, LX/8Cf;->e()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 1311423
    check-cast p1, LX/8Ce;

    .line 1311424
    iget-object v0, p0, LX/8Cf;->d:LX/0Px;

    iget-object v1, p1, LX/8Ce;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1311425
    if-ltz v0, :cond_0

    .line 1311426
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 1311454
    iget-object v0, p0, LX/8Cf;->c:LX/8CX;

    if-nez v0, :cond_1

    move v0, v1

    .line 1311455
    :cond_0
    :goto_0
    return v0

    .line 1311456
    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, LX/8Cf;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1311457
    iget-object v2, p0, LX/8Cf;->d:LX/0Px;

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 1311458
    iget-object v3, p0, LX/8Cf;->c:LX/8CX;

    invoke-interface {v3, v2}, LX/8CX;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1311459
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 1311460
    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1311417
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1311418
    new-instance v1, LX/8Ce;

    iget-object v2, p0, LX/8Cf;->d:LX/0Px;

    invoke-virtual {v2, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v1, p2, v0, v2}, LX/8Ce;-><init>(ILandroid/view/ViewGroup;Ljava/lang/Object;)V

    .line 1311419
    iget-object v2, p0, LX/8Cf;->e:Ljava/util/Queue;

    invoke-interface {v2, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 1311420
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1311421
    invoke-direct {p0}, LX/8Cf;->e()V

    .line 1311422
    return-object v1
.end method

.method public final a(LX/8CX;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8CX",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1311414
    iput-object p1, p0, LX/8Cf;->c:LX/8CX;

    .line 1311415
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 1311416
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 1311399
    check-cast p3, LX/8Ce;

    .line 1311400
    iget-object v0, p3, LX/8Ce;->b:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1311401
    iget-object v0, p0, LX/8Cf;->e:Ljava/util/Queue;

    invoke-interface {v0, p3}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 1311402
    iget-object v0, p3, LX/8Ce;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 1311403
    iget-object v0, p0, LX/8Cf;->c:LX/8CX;

    iget-object v1, p3, LX/8Ce;->c:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/8CX;->b(Ljava/lang/Object;)I

    move-result v1

    .line 1311404
    const/4 v0, -0x1

    if-eq v1, v0, :cond_1

    .line 1311405
    iget-object v0, p0, LX/8Cf;->f:LX/0YU;

    invoke-virtual {v0, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1311406
    if-nez v0, :cond_0

    .line 1311407
    invoke-static {v3}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 1311408
    iget-object v2, p0, LX/8Cf;->f:LX/0YU;

    invoke-virtual {v2, v1, v0}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 1311409
    :cond_0
    iget-object v1, p3, LX/8Ce;->b:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1311410
    iget-object v2, p3, LX/8Ce;->b:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1311411
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v2, v3, :cond_1

    .line 1311412
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1311413
    :cond_1
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1311395
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1311396
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/8Cf;->d:LX/0Px;

    .line 1311397
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 1311398
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1311391
    check-cast p2, LX/8Ce;

    .line 1311392
    iget-object v0, p2, LX/8Ce;->b:Landroid/view/ViewGroup;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1311390
    iget-object v0, p0, LX/8Cf;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1311383
    if-nez p3, :cond_1

    .line 1311384
    :cond_0
    :goto_0
    return-void

    .line 1311385
    :cond_1
    check-cast p3, LX/8Ce;

    .line 1311386
    iget-object v0, p3, LX/8Ce;->c:Ljava/lang/Object;

    iput-object v0, p0, LX/8Cf;->g:Ljava/lang/Object;

    .line 1311387
    iget-object v0, p0, LX/8Cf;->e:Ljava/util/Queue;

    invoke-interface {v0, p3}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 1311388
    if-eqz v0, :cond_0

    .line 1311389
    iget-object v0, p0, LX/8Cf;->d:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p3, LX/8Ce;->b:Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v1}, LX/8Cf;->a(Ljava/lang/Object;Landroid/view/ViewGroup;)V

    goto :goto_0
.end method
