.class public final LX/7Cw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private a:LX/3II;


# direct methods
.method public constructor <init>(LX/3II;)V
    .locals 0

    .prologue
    .line 1182028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1182029
    iput-object p1, p0, LX/7Cw;->a:LX/3II;

    .line 1182030
    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 1182031
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v0

    .line 1182032
    iget-object v1, p0, LX/7Cw;->a:LX/3II;

    invoke-interface {v1}, LX/3II;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1182033
    iget-object v1, p0, LX/7Cw;->a:LX/3II;

    invoke-interface {v1}, LX/3II;->get360TextureView()LX/2qW;

    move-result-object v1

    .line 1182034
    iget-object p0, v1, LX/2qW;->c:LX/7Cy;

    if-eqz p0, :cond_0

    iget-object p0, v1, LX/2qW;->c:LX/7Cy;

    iget-object p0, p0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    if-eqz p0, :cond_0

    .line 1182035
    iget-object p0, v1, LX/2qW;->c:LX/7Cy;

    iget-object p0, p0, LX/7Cy;->c:Lcom/facebook/spherical/GlMediaRenderThread;

    .line 1182036
    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v1, v1, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1182037
    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    invoke-virtual {v1, v0}, LX/7Ce;->a(F)V

    .line 1182038
    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v1, v1, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1182039
    :cond_0
    return-void
.end method
