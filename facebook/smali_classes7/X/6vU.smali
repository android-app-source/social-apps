.class public final LX/6vU;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

.field public final synthetic b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

.field public final synthetic c:LX/6vJ;


# direct methods
.method public constructor <init>(LX/6vJ;Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;)V
    .locals 0

    .prologue
    .line 1157002
    iput-object p1, p0, LX/6vU;->c:LX/6vJ;

    iput-object p2, p0, LX/6vU;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    iput-object p3, p0, LX/6vU;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1157003
    iget-object v0, p0, LX/6vU;->c:LX/6vJ;

    iget-object v1, p0, LX/6vU;->c:LX/6vJ;

    iget-object v1, v1, LX/6vJ;->a:Landroid/content/Context;

    const v2, 0x7f081e37

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, LX/6vJ;->a$redex0(LX/6vJ;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1157004
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1157005
    check-cast p1, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;

    const/4 v4, 0x0

    .line 1157006
    iget-object v0, p0, LX/6vU;->c:LX/6vJ;

    iget-object v1, p0, LX/6vU;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    iget-object v2, p0, LX/6vU;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    invoke-virtual {p1}, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;->a()Ljava/lang/String;

    move-result-object v3

    move v5, v4

    .line 1157007
    if-nez v4, :cond_0

    if-eqz v5, :cond_1

    .line 1157008
    :cond_0
    iget-object v6, v0, LX/6vJ;->c:LX/6qh;

    new-instance v7, LX/73T;

    sget-object p0, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v7, p0}, LX/73T;-><init>(LX/73S;)V

    invoke-virtual {v6, v7}, LX/6qh;->a(LX/73T;)V

    .line 1157009
    :goto_0
    return-void

    .line 1157010
    :cond_1
    move-object v6, v1

    .line 1157011
    iget-object v6, v6, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->a:LX/6vY;

    invoke-static {v3, v2, v6}, LX/6vJ;->a(Ljava/lang/String;Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;LX/6vY;)Lcom/facebook/payments/contactinfo/model/ContactInfo;

    move-result-object v6

    .line 1157012
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 1157013
    const-string p0, "contact_info"

    invoke-virtual {v7, p0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1157014
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1157015
    const-string p0, "extra_activity_result_data"

    invoke-virtual {v6, p0, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1157016
    iget-object v7, v0, LX/6vJ;->c:LX/6qh;

    new-instance p0, LX/73T;

    sget-object p1, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {p0, p1, v6}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    invoke-virtual {v7, p0}, LX/6qh;->a(LX/73T;)V

    goto :goto_0
.end method
