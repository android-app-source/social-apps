.class public LX/8A4;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8A3;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1305608
    invoke-static {}, LX/8A3;->values()[LX/8A3;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/8A4;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1305605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305606
    new-instance v0, Ljava/util/BitSet;

    invoke-static {}, LX/8A3;->values()[LX/8A3;

    move-result-object v1

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/8A4;->b:Ljava/util/BitSet;

    .line 1305607
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1305596
    invoke-direct {p0}, LX/8A4;-><init>()V

    .line 1305597
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1305598
    :try_start_0
    invoke-static {v0}, LX/8A3;->valueOf(Ljava/lang/String;)LX/8A3;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1305599
    iget-object v2, p0, LX/8A4;->b:Ljava/util/BitSet;

    invoke-virtual {v0}, LX/8A3;->getBit()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->set(I)V

    goto :goto_0

    .line 1305600
    :catch_0
    goto :goto_0

    .line 1305601
    :cond_0
    return-void
.end method

.method public static a(LX/0Px;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1305604
    new-instance v0, LX/8A4;

    invoke-direct {v0, p0}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object v1, LX/8A3;->BASIC_ADMIN:LX/8A3;

    invoke-virtual {v0, v1}, LX/8A4;->a(LX/8A3;)Z

    move-result v0

    return v0
.end method

.method public static c(LX/0Px;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1305603
    new-instance v0, LX/8A4;

    invoke-direct {v0, p0}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object v1, LX/8A3;->CREATE_CONTENT:LX/8A3;

    invoke-virtual {v0, v1}, LX/8A4;->a(LX/8A3;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/8A3;)Z
    .locals 2

    .prologue
    .line 1305602
    iget-object v0, p0, LX/8A4;->b:Ljava/util/BitSet;

    invoke-virtual {p1}, LX/8A3;->getBit()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method
