.class public LX/7Kh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/7Kh;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/37Y;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1196185
    const-class v0, LX/7Kh;

    sput-object v0, LX/7Kh;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Landroid/content/Context;LX/37Y;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1196186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1196187
    iput-object p2, p0, LX/7Kh;->b:Landroid/content/Context;

    .line 1196188
    iput-object p1, p0, LX/7Kh;->c:Ljava/util/concurrent/Executor;

    .line 1196189
    iput-object p3, p0, LX/7Kh;->d:LX/37Y;

    .line 1196190
    return-void
.end method

.method public static a(LX/0QB;)LX/7Kh;
    .locals 6

    .prologue
    .line 1196191
    sget-object v0, LX/7Kh;->e:LX/7Kh;

    if-nez v0, :cond_1

    .line 1196192
    const-class v1, LX/7Kh;

    monitor-enter v1

    .line 1196193
    :try_start_0
    sget-object v0, LX/7Kh;->e:LX/7Kh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1196194
    if-eqz v2, :cond_0

    .line 1196195
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1196196
    new-instance p0, LX/7Kh;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/37Y;->a(LX/0QB;)LX/37Y;

    move-result-object v5

    check-cast v5, LX/37Y;

    invoke-direct {p0, v3, v4, v5}, LX/7Kh;-><init>(Ljava/util/concurrent/Executor;Landroid/content/Context;LX/37Y;)V

    .line 1196197
    move-object v0, p0

    .line 1196198
    sput-object v0, LX/7Kh;->e:LX/7Kh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1196199
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1196200
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1196201
    :cond_1
    sget-object v0, LX/7Kh;->e:LX/7Kh;

    return-object v0

    .line 1196202
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1196203
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static i(LX/7Kh;)LX/7JN;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1196204
    iget-object v0, p0, LX/7Kh;->d:LX/37Y;

    .line 1196205
    iget-object v1, v0, LX/37Y;->r:LX/7JN;

    move-object v0, v1

    .line 1196206
    if-nez v0, :cond_0

    .line 1196207
    iget-object v0, p0, LX/7Kh;->d:LX/37Y;

    .line 1196208
    iget-object v1, v0, LX/37Y;->t:LX/7JN;

    move-object v0, v1

    .line 1196209
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final g()LX/1bf;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1196210
    invoke-static {p0}, LX/7Kh;->i(LX/7Kh;)LX/7JN;

    move-result-object v0

    .line 1196211
    if-nez v0, :cond_0

    .line 1196212
    const/4 v0, 0x0

    .line 1196213
    :goto_0
    return-object v0

    .line 1196214
    :cond_0
    iget-object p0, v0, LX/7JN;->l:LX/1bf;

    move-object v0, p0

    .line 1196215
    goto :goto_0
.end method
