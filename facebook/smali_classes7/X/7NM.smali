.class public LX/7NM;
.super LX/2oy;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/text/BetterTextView;

.field private b:LX/2pO;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1200192
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7NM;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1200193
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1200194
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7NM;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200195
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1200196
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200197
    sget-object v0, LX/2pO;->DEFAULT:LX/2pO;

    iput-object v0, p0, LX/7NM;->b:LX/2pO;

    .line 1200198
    const v0, 0x7f0312bd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1200199
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7NL;

    invoke-direct {v1, p0}, LX/7NL;-><init>(LX/7NM;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1200200
    const v0, 0x7f0d2ba7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/7NM;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1200201
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 6

    .prologue
    .line 1200202
    if-eqz p2, :cond_1

    .line 1200203
    sget-object v0, LX/2pO;->DEFAULT:LX/2pO;

    iput-object v0, p0, LX/7NM;->b:LX/2pO;

    .line 1200204
    iget-object v0, p0, LX/7NM;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1200205
    const/4 v3, 0x0

    .line 1200206
    iget-object v2, p1, LX/2pa;->b:LX/0P1;

    if-eqz v2, :cond_0

    iget-object v2, p1, LX/2pa;->b:LX/0P1;

    const-string v4, "GraphQLStoryProps"

    invoke-virtual {v2, v4}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_0
    move-object v2, v3

    .line 1200207
    :goto_0
    move-object v2, v2

    .line 1200208
    if-nez v2, :cond_3

    .line 1200209
    const/4 v2, 0x0

    .line 1200210
    :goto_1
    move-object v2, v2

    .line 1200211
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1200212
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->O()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, LX/7NM;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080d4d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_2
    move-object v1, v2

    .line 1200213
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1200214
    :cond_1
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1200215
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1200216
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1200217
    invoke-virtual {p0, v0}, LX/7NM;->a(LX/2qV;)V

    .line 1200218
    return-void

    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->aR()I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, LX/7NJ;->a(J)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_3
    invoke-static {v2}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    goto :goto_1

    .line 1200219
    :cond_4
    iget-object v2, p1, LX/2pa;->b:LX/0P1;

    const-string v4, "GraphQLStoryProps"

    invoke-virtual {v2, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 1200220
    instance-of v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 1200221
    if-nez v2, :cond_5

    move-object v2, v3

    .line 1200222
    goto :goto_0

    .line 1200223
    :cond_5
    check-cast v2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1200224
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 1200225
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0
.end method

.method public final a(LX/2qV;)V
    .locals 2

    .prologue
    .line 1200226
    sget-object v0, LX/7NK;->a:[I

    iget-object v1, p0, LX/7NM;->b:LX/2pO;

    invoke-virtual {v1}, LX/2pO;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1200227
    :goto_0
    return-void

    .line 1200228
    :pswitch_0
    invoke-virtual {p1}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1200229
    iget-object v0, p0, LX/7NM;->a:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    .line 1200230
    :cond_0
    :pswitch_1
    iget-object v0, p0, LX/7NM;->a:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
