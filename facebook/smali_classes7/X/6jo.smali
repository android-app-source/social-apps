.class public LX/6jo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final isLazy:Ljava/lang/Boolean;

.field public final messageId:Ljava/lang/String;

.field public final threadKey:LX/6l9;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1132930
    new-instance v0, LX/1sv;

    const-string v1, "DeltaForcedFetch"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jo;->b:LX/1sv;

    .line 1132931
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jo;->c:LX/1sw;

    .line 1132932
    new-instance v0, LX/1sw;

    const-string v1, "messageId"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jo;->d:LX/1sw;

    .line 1132933
    new-instance v0, LX/1sw;

    const-string v1, "isLazy"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jo;->e:LX/1sw;

    .line 1132934
    sput-boolean v3, LX/6jo;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6l9;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1132935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1132936
    iput-object p1, p0, LX/6jo;->threadKey:LX/6l9;

    .line 1132937
    iput-object p2, p0, LX/6jo;->messageId:Ljava/lang/String;

    .line 1132938
    iput-object p3, p0, LX/6jo;->isLazy:Ljava/lang/Boolean;

    .line 1132939
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1132940
    if-eqz p2, :cond_4

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1132941
    :goto_0
    if-eqz p2, :cond_5

    const-string v0, "\n"

    move-object v3, v0

    .line 1132942
    :goto_1
    if-eqz p2, :cond_6

    const-string v0, " "

    .line 1132943
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "DeltaForcedFetch"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1132944
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132945
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132946
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132947
    const/4 v1, 0x1

    .line 1132948
    iget-object v6, p0, LX/6jo;->threadKey:LX/6l9;

    if-eqz v6, :cond_0

    .line 1132949
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132950
    const-string v1, "threadKey"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132951
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132952
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132953
    iget-object v1, p0, LX/6jo;->threadKey:LX/6l9;

    if-nez v1, :cond_7

    .line 1132954
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 1132955
    :cond_0
    iget-object v6, p0, LX/6jo;->messageId:Ljava/lang/String;

    if-eqz v6, :cond_a

    .line 1132956
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132957
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132958
    const-string v1, "messageId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132959
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132960
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132961
    iget-object v1, p0, LX/6jo;->messageId:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 1132962
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132963
    :goto_4
    iget-object v1, p0, LX/6jo;->isLazy:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 1132964
    if-nez v2, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132965
    :cond_2
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132966
    const-string v1, "isLazy"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132967
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132968
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132969
    iget-object v0, p0, LX/6jo;->isLazy:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    .line 1132970
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132971
    :cond_3
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132972
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132973
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1132974
    :cond_4
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1132975
    :cond_5
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1132976
    :cond_6
    const-string v0, ""

    goto/16 :goto_2

    .line 1132977
    :cond_7
    iget-object v1, p0, LX/6jo;->threadKey:LX/6l9;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1132978
    :cond_8
    iget-object v1, p0, LX/6jo;->messageId:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1132979
    :cond_9
    iget-object v0, p0, LX/6jo;->isLazy:Ljava/lang/Boolean;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_a
    move v2, v1

    goto/16 :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1132980
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1132981
    iget-object v0, p0, LX/6jo;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1132982
    iget-object v0, p0, LX/6jo;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1132983
    sget-object v0, LX/6jo;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132984
    iget-object v0, p0, LX/6jo;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1132985
    :cond_0
    iget-object v0, p0, LX/6jo;->messageId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1132986
    iget-object v0, p0, LX/6jo;->messageId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1132987
    sget-object v0, LX/6jo;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132988
    iget-object v0, p0, LX/6jo;->messageId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1132989
    :cond_1
    iget-object v0, p0, LX/6jo;->isLazy:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1132990
    iget-object v0, p0, LX/6jo;->isLazy:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1132991
    sget-object v0, LX/6jo;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132992
    iget-object v0, p0, LX/6jo;->isLazy:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1132993
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1132994
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1132995
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1132996
    if-nez p1, :cond_1

    .line 1132997
    :cond_0
    :goto_0
    return v0

    .line 1132998
    :cond_1
    instance-of v1, p1, LX/6jo;

    if-eqz v1, :cond_0

    .line 1132999
    check-cast p1, LX/6jo;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1133000
    if-nez p1, :cond_3

    .line 1133001
    :cond_2
    :goto_1
    move v0, v2

    .line 1133002
    goto :goto_0

    .line 1133003
    :cond_3
    iget-object v0, p0, LX/6jo;->threadKey:LX/6l9;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1133004
    :goto_2
    iget-object v3, p1, LX/6jo;->threadKey:LX/6l9;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1133005
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1133006
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133007
    iget-object v0, p0, LX/6jo;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6jo;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133008
    :cond_5
    iget-object v0, p0, LX/6jo;->messageId:Ljava/lang/String;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1133009
    :goto_4
    iget-object v3, p1, LX/6jo;->messageId:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1133010
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1133011
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133012
    iget-object v0, p0, LX/6jo;->messageId:Ljava/lang/String;

    iget-object v3, p1, LX/6jo;->messageId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133013
    :cond_7
    iget-object v0, p0, LX/6jo;->isLazy:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1133014
    :goto_6
    iget-object v3, p1, LX/6jo;->isLazy:Ljava/lang/Boolean;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1133015
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1133016
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1133017
    iget-object v0, p0, LX/6jo;->isLazy:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6jo;->isLazy:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 1133018
    goto :goto_1

    :cond_a
    move v0, v2

    .line 1133019
    goto :goto_2

    :cond_b
    move v3, v2

    .line 1133020
    goto :goto_3

    :cond_c
    move v0, v2

    .line 1133021
    goto :goto_4

    :cond_d
    move v3, v2

    .line 1133022
    goto :goto_5

    :cond_e
    move v0, v2

    .line 1133023
    goto :goto_6

    :cond_f
    move v3, v2

    .line 1133024
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1133025
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1133026
    sget-boolean v0, LX/6jo;->a:Z

    .line 1133027
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jo;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1133028
    return-object v0
.end method
