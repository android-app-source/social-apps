.class public abstract LX/8QK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "LISTENER::Lcom/facebook/widget/tokenizedtypeahead/model/Token$OnTokenClickedListener;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Z

.field public b:LX/8Re;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "T",
            "LISTENER;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1342882
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/8QK;-><init>(Z)V

    .line 1342883
    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .prologue
    .line 1342876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1342877
    iput-boolean p1, p0, LX/8QK;->a:Z

    .line 1342878
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    .prologue
    .line 1342879
    iget-object v0, p0, LX/8QK;->b:LX/8Re;

    if-nez v0, :cond_0

    .line 1342880
    :goto_0
    return-void

    .line 1342881
    :cond_0
    iget-object v0, p0, LX/8QK;->b:LX/8Re;

    invoke-interface {v0, p0}, LX/8Re;->a(LX/8QK;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 1342884
    iget-boolean v0, p0, LX/8QK;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 1342874
    iget-boolean v0, p0, LX/8QK;->c:Z

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 1342875
    iget-boolean v0, p0, LX/8QK;->a:Z

    return v0
.end method
