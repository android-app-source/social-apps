.class public LX/8Nw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;
.implements LX/4B8;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile p:LX/8Nw;


# instance fields
.field private final b:Z

.field private final c:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

.field private final d:LX/8Om;

.field private final e:LX/8OS;

.field private final f:Ljava/util/concurrent/ExecutorService;

.field public final g:LX/03V;

.field private final h:LX/1EZ;

.field private final i:LX/0cX;

.field private final j:LX/8Oq;

.field public k:LX/8OM;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public volatile l:Z

.field public m:Ljava/util/concurrent/CountDownLatch;

.field private final n:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final o:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1338081
    const-class v0, LX/8Nw;

    sput-object v0, LX/8Nw;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;LX/8Om;Ljava/util/concurrent/ExecutorService;LX/03V;LX/1EZ;LX/8Oq;LX/8OS;LX/0cX;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1338082
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1338083
    const-string v0, "MediaUpload"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, LX/8Nw;->b:Z

    .line 1338084
    const/4 v0, 0x0

    iput-object v0, p0, LX/8Nw;->k:LX/8OM;

    .line 1338085
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/8Nw;->n:Ljava/util/HashSet;

    .line 1338086
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/8Nw;->o:Ljava/util/HashSet;

    .line 1338087
    iput-object p1, p0, LX/8Nw;->c:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    .line 1338088
    iput-object p2, p0, LX/8Nw;->d:LX/8Om;

    .line 1338089
    iput-object p3, p0, LX/8Nw;->f:Ljava/util/concurrent/ExecutorService;

    .line 1338090
    iput-object p4, p0, LX/8Nw;->g:LX/03V;

    .line 1338091
    iput-object p5, p0, LX/8Nw;->h:LX/1EZ;

    .line 1338092
    iput-object p6, p0, LX/8Nw;->j:LX/8Oq;

    .line 1338093
    iput-object p7, p0, LX/8Nw;->e:LX/8OS;

    .line 1338094
    iput-object p8, p0, LX/8Nw;->i:LX/0cX;

    .line 1338095
    return-void
.end method

.method public static a(LX/0QB;)LX/8Nw;
    .locals 12

    .prologue
    .line 1338096
    sget-object v0, LX/8Nw;->p:LX/8Nw;

    if-nez v0, :cond_1

    .line 1338097
    const-class v1, LX/8Nw;

    monitor-enter v1

    .line 1338098
    :try_start_0
    sget-object v0, LX/8Nw;->p:LX/8Nw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1338099
    if-eqz v2, :cond_0

    .line 1338100
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1338101
    new-instance v3, LX/8Nw;

    invoke-static {v0}, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->b(LX/0QB;)Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    invoke-static {v0}, LX/8Om;->b(LX/0QB;)LX/8Om;

    move-result-object v5

    check-cast v5, LX/8Om;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v8

    check-cast v8, LX/1EZ;

    invoke-static {v0}, LX/8Oq;->a(LX/0QB;)LX/8Oq;

    move-result-object v9

    check-cast v9, LX/8Oq;

    invoke-static {v0}, LX/8OS;->b(LX/0QB;)LX/8OS;

    move-result-object v10

    check-cast v10, LX/8OS;

    invoke-static {v0}, LX/0cX;->a(LX/0QB;)LX/0cX;

    move-result-object v11

    check-cast v11, LX/0cX;

    invoke-direct/range {v3 .. v11}, LX/8Nw;-><init>(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;LX/8Om;Ljava/util/concurrent/ExecutorService;LX/03V;LX/1EZ;LX/8Oq;LX/8OS;LX/0cX;)V

    .line 1338102
    move-object v0, v3

    .line 1338103
    sput-object v0, LX/8Nw;->p:LX/8Nw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1338104
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1338105
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1338106
    :cond_1
    sget-object v0, LX/8Nw;->p:LX/8Nw;

    return-object v0

    .line 1338107
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1338108
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/8OM;LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1338109
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/8OT; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1338110
    :try_start_1
    iget-object v0, p2, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1338111
    const-string v2, "uploadOp"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1338112
    iget-object v2, p0, LX/8Nw;->k:LX/8OM;

    if-eqz v2, :cond_2

    .line 1338113
    iget-object v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->G:Lcom/facebook/photos/upload/operation/UploadRecords;

    move-object v0, v2

    .line 1338114
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/photos/upload/operation/UploadRecords;->a()LX/0P1;

    move-result-object v0

    .line 1338115
    :goto_0
    if-nez v0, :cond_0

    .line 1338116
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 1338117
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "PhotosUploadServiceHandler re-entrance!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 1338118
    new-instance v2, LX/8OT;

    invoke-static {v1}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v1

    invoke-direct {v2, v1, v0}, LX/8OT;-><init>(LX/73z;Ljava/util/Map;)V

    throw v2

    .line 1338119
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_0
    .catch LX/8OT; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1338120
    :catch_0
    move-exception v0

    .line 1338121
    :try_start_3
    sget-object v1, LX/8Nw;->a:Ljava/lang/Class;

    const-string v2, "CancellationException in %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-direct {p0}, LX/8Nw;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1338122
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1338123
    :catchall_1
    move-exception v0

    monitor-enter p0

    .line 1338124
    const/4 v1, 0x0

    :try_start_4
    iput-object v1, p0, LX/8Nw;->k:LX/8OM;

    .line 1338125
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    throw v0

    :cond_1
    move-object v0, v1

    .line 1338126
    goto :goto_0

    .line 1338127
    :cond_2
    :try_start_5
    iget-object v1, p0, LX/8Nw;->h:LX/1EZ;

    .line 1338128
    iget-object v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v2, v2

    .line 1338129
    invoke-virtual {v1, v2}, LX/1EZ;->g(Ljava/lang/String;)V

    .line 1338130
    const-string v1, "photo_upload_op"

    .line 1338131
    iget-object v2, p2, LX/1qK;->mType:Ljava/lang/String;

    move-object v2, v2

    .line 1338132
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/8Nw;->o:Ljava/util/HashSet;

    .line 1338133
    iget-object v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v2, v2

    .line 1338134
    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1338135
    iget-object v1, p0, LX/8Nw;->g:LX/03V;

    sget-object v2, LX/8Nw;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Repeating successful upload "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1338136
    iget-object v4, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v4, v4

    .line 1338137
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1338138
    :cond_3
    iput-object p1, p0, LX/8Nw;->k:LX/8OM;

    .line 1338139
    iget-boolean v1, p0, LX/8Nw;->b:Z

    if-eqz v1, :cond_4

    .line 1338140
    iget-object v1, p0, LX/8Nw;->k:LX/8OM;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 1338141
    :cond_4
    iget-object v1, p0, LX/8Nw;->k:LX/8OM;

    invoke-interface {v1}, LX/8OM;->a()V

    .line 1338142
    iget-object v1, p0, LX/8Nw;->n:Ljava/util/HashSet;

    .line 1338143
    iget-object v2, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v2, v2

    .line 1338144
    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1338145
    new-instance v0, Ljava/util/concurrent/CancellationException;

    const-string v1, "Cancelled prior to starting"

    invoke-direct {v0, v1}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1338146
    :cond_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1338147
    :try_start_6
    invoke-interface {p1, v0}, LX/8OM;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 1338148
    iget-boolean v2, p0, LX/8Nw;->b:Z

    if-eqz v2, :cond_6

    .line 1338149
    invoke-direct {p0}, LX/8Nw;->a()Ljava/lang/String;

    .line 1338150
    :cond_6
    const-string v2, "photo_upload_op"

    .line 1338151
    iget-object v3, p2, LX/1qK;->mType:Ljava/lang/String;

    move-object v3, v3

    .line 1338152
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1338153
    iget-object v2, p0, LX/8Nw;->o:Ljava/util/HashSet;

    .line 1338154
    iget-object v3, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v0, v3

    .line 1338155
    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/util/concurrent/CancellationException; {:try_start_6 .. :try_end_6} :catch_0
    .catch LX/8OT; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1338156
    :cond_7
    monitor-enter p0

    .line 1338157
    const/4 v0, 0x0

    :try_start_7
    iput-object v0, p0, LX/8Nw;->k:LX/8OM;

    .line 1338158
    monitor-exit p0

    .line 1338159
    return-object v1

    .line 1338160
    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v0

    .line 1338161
    :catch_1
    move-exception v0

    .line 1338162
    :try_start_8
    sget-object v1, LX/8Nw;->a:Ljava/lang/Class;

    const-string v2, "PartialUploadException in %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-direct {p0}, LX/8Nw;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1338163
    throw v0

    .line 1338164
    :catch_2
    move-exception v0

    .line 1338165
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, LX/8Nw;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " got an unexpected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1338166
    sget-object v2, LX/8Nw;->a:Ljava/lang/Class;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v0, v1, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1338167
    iget-object v2, p0, LX/8Nw;->g:LX/03V;

    sget-object v3, LX/8Nw;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1338168
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/8Nw;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " caught "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1338169
    new-instance v2, LX/8OT;

    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v3}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v0

    invoke-direct {v2, v0}, LX/8OT;-><init>(LX/73z;)V

    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1338170
    :catchall_3
    move-exception v0

    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v0
.end method

.method private declared-synchronized a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1338171
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/8Nw;->k:LX/8OM;

    if-nez v0, :cond_0

    const-string v0, "<no media uploader>"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/8Nw;->k:LX/8OM;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized cancelOperation(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1338172
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LX/8Nw;->k:LX/8OM;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1338173
    :goto_0
    monitor-exit p0

    return v0

    .line 1338174
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, LX/8Nw;->l:Z

    .line 1338175
    iget-boolean v1, p0, LX/8Nw;->b:Z

    if-eqz v1, :cond_1

    .line 1338176
    iget-object v1, p0, LX/8Nw;->k:LX/8OM;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 1338177
    :cond_1
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, LX/8Nw;->m:Ljava/util/concurrent/CountDownLatch;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1338178
    :try_start_2
    iget-object v1, p0, LX/8Nw;->f:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/photos/upload/service/PhotosUploadServiceHandler$1;

    invoke-direct {v2, p0}, Lcom/facebook/photos/upload/service/PhotosUploadServiceHandler$1;-><init>(LX/8Nw;)V

    const v3, -0x5d74276d

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1338179
    iget-object v1, p0, LX/8Nw;->m:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1338180
    :try_start_3
    iget-boolean v0, p0, LX/8Nw;->b:Z

    if-eqz v0, :cond_2

    .line 1338181
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Did we cancel "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/8Nw;->k:LX/8OM;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, LX/8Nw;->l:Z

    if-eqz v0, :cond_3

    const-string v0, "Yes!"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1338182
    :cond_2
    iget-boolean v0, p0, LX/8Nw;->l:Z

    goto :goto_0

    .line 1338183
    :cond_3
    const-string v0, "No!"
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 1338184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1338185
    :catch_0
    goto :goto_0
.end method

.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 1338186
    iget-object v0, p0, LX/8Nw;->j:LX/8Oq;

    invoke-virtual {v0}, LX/8Oq;->a()V

    .line 1338187
    :try_start_0
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1338188
    const-string v1, "photo_upload_op"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1338189
    iget-object v0, p0, LX/8Nw;->c:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    invoke-direct {p0, v0, p1}, LX/8Nw;->a(LX/8OM;LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1338190
    iget-object v1, p0, LX/8Nw;->j:LX/8Oq;

    invoke-virtual {v1}, LX/8Oq;->b()V

    :goto_0
    return-object v0

    .line 1338191
    :cond_0
    :try_start_1
    const-string v1, "video_upload_op"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1338192
    iget-object v0, p0, LX/8Nw;->d:LX/8Om;

    invoke-direct {p0, v0, p1}, LX/8Nw;->a(LX/8OM;LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1338193
    iget-object v1, p0, LX/8Nw;->j:LX/8Oq;

    invoke-virtual {v1}, LX/8Oq;->b()V

    goto :goto_0

    .line 1338194
    :cond_1
    :try_start_2
    const-string v1, "multimedia_upload_op"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1338195
    iget-object v0, p0, LX/8Nw;->e:LX/8OS;

    invoke-direct {p0, v0, p1}, LX/8Nw;->a(LX/8OM;LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 1338196
    iget-object v1, p0, LX/8Nw;->j:LX/8Oq;

    invoke-virtual {v1}, LX/8Oq;->b()V

    goto :goto_0

    .line 1338197
    :cond_2
    :try_start_3
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot handle operation "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1338198
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/8Nw;->j:LX/8Oq;

    invoke-virtual {v1}, LX/8Oq;->b()V

    throw v0
.end method
