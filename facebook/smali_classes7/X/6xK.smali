.class public final enum LX/6xK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6xK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6xK;

.field public static final enum AMOUNT_FORM_CONTROLLER:LX/6xK;

.field public static final enum ITEM_FORM_CONTROLLER:LX/6xK;

.field public static final enum NOTE_FORM_CONTROLLER:LX/6xK;

.field public static final enum SHIPPING_METHOD_FORM_CONTROLLER:LX/6xK;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1158685
    new-instance v0, LX/6xK;

    const-string v1, "AMOUNT_FORM_CONTROLLER"

    invoke-direct {v0, v1, v2}, LX/6xK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xK;->AMOUNT_FORM_CONTROLLER:LX/6xK;

    .line 1158686
    new-instance v0, LX/6xK;

    const-string v1, "ITEM_FORM_CONTROLLER"

    invoke-direct {v0, v1, v3}, LX/6xK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xK;->ITEM_FORM_CONTROLLER:LX/6xK;

    .line 1158687
    new-instance v0, LX/6xK;

    const-string v1, "NOTE_FORM_CONTROLLER"

    invoke-direct {v0, v1, v4}, LX/6xK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xK;->NOTE_FORM_CONTROLLER:LX/6xK;

    .line 1158688
    new-instance v0, LX/6xK;

    const-string v1, "SHIPPING_METHOD_FORM_CONTROLLER"

    invoke-direct {v0, v1, v5}, LX/6xK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xK;->SHIPPING_METHOD_FORM_CONTROLLER:LX/6xK;

    .line 1158689
    const/4 v0, 0x4

    new-array v0, v0, [LX/6xK;

    sget-object v1, LX/6xK;->AMOUNT_FORM_CONTROLLER:LX/6xK;

    aput-object v1, v0, v2

    sget-object v1, LX/6xK;->ITEM_FORM_CONTROLLER:LX/6xK;

    aput-object v1, v0, v3

    sget-object v1, LX/6xK;->NOTE_FORM_CONTROLLER:LX/6xK;

    aput-object v1, v0, v4

    sget-object v1, LX/6xK;->SHIPPING_METHOD_FORM_CONTROLLER:LX/6xK;

    aput-object v1, v0, v5

    sput-object v0, LX/6xK;->$VALUES:[LX/6xK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1158690
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6xK;
    .locals 1

    .prologue
    .line 1158691
    const-class v0, LX/6xK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xK;

    return-object v0
.end method

.method public static values()[LX/6xK;
    .locals 1

    .prologue
    .line 1158692
    sget-object v0, LX/6xK;->$VALUES:[LX/6xK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6xK;

    return-object v0
.end method
