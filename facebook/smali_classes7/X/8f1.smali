.class public final LX/8f1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1384058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1384059
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1384060
    iget-object v1, p0, LX/8f1;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1384061
    iget-object v2, p0, LX/8f1;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1384062
    const/4 v3, 0x3

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1384063
    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1384064
    invoke-virtual {v0, v8, v2}, LX/186;->b(II)V

    .line 1384065
    const/4 v1, 0x2

    iget-wide v2, p0, LX/8f1;->c:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1384066
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1384067
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1384068
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1384069
    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1384070
    new-instance v0, LX/15i;

    move-object v2, v6

    move-object v3, v6

    move v4, v8

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1384071
    new-instance v1, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    invoke-direct {v1, v0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;-><init>(LX/15i;)V

    .line 1384072
    iget-object v0, p0, LX/8f1;->b:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 1384073
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;->b()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iget-object v2, p0, LX/8f1;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->L_()LX/0x2;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/model/GraphQLStory;->a(LX/0x2;)V

    .line 1384074
    :cond_0
    return-object v1
.end method
