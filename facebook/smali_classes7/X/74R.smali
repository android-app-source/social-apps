.class public final enum LX/74R;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/74R;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/74R;

.field public static final enum ALL_SELECTED_BUTTON:LX/74R;

.field public static final enum ANDROID_PHOTOS_CONSUMPTION:LX/74R;

.field public static final enum CAMERA_CLOSING:LX/74R;

.field public static final enum CAMERA_DONE_LOADING:LX/74R;

.field public static final enum CAMERA_EXCEPTION:LX/74R;

.field public static final enum CAMERA_FLASH:LX/74R;

.field public static final enum CAMERA_FLOW:LX/74R;

.field public static final enum CAMERA_LOADING:LX/74R;

.field public static final enum CAMERA_REVIEW:LX/74R;

.field public static final enum CAMERA_REVIEW_ACCEPT:LX/74R;

.field public static final enum CAMERA_REVIEW_CANCEL:LX/74R;

.field public static final enum CAMERA_REVIEW_REJECT:LX/74R;

.field public static final enum CAMERA_SOURCE_SELECT:LX/74R;

.field public static final enum CAMERA_START:LX/74R;

.field public static final enum CAMERA_STOPPED:LX/74R;

.field public static final enum CAMERA_TAKE_PHOTO:LX/74R;

.field public static final enum CAMERA_VIDEO_RECORDER_STOPPED:LX/74R;

.field public static final enum CANCELLED_MULTIPICKER:LX/74R;

.field public static final enum COMPOSER_ADD_ATTACHMENT_BUTTON_PRESSED:LX/74R;

.field public static final enum COMPOSER_ADD_MEDIA:LX/74R;

.field public static final enum COMPOSER_ATTACHMENT_REMOVED:LX/74R;

.field public static final enum COMPOSER_BUTTON_CLICKED:LX/74R;

.field public static final enum COMPOSER_EDIT_TAG_SUGGESTION:LX/74R;

.field public static final enum COMPOSER_MEDIA_UPLOAD_INIT:LX/74R;

.field public static final enum COMPOSER_NUM_UNIQUE_CAPTIONS:LX/74R;

.field public static final enum COMPOSER_REORDER_MODE_CLOSED:LX/74R;

.field public static final enum COMPOSER_REORDER_MODE_OPENED:LX/74R;

.field public static final enum COMPOSER_REORDER_NUX_SHOWN:LX/74R;

.field public static final enum COMPOSER_REORDER_OCCURRED:LX/74R;

.field public static final enum COMPOSER_SUBMIT_VIDEO:LX/74R;

.field public static final enum COMPOSER_TAGGING_CANCEL:LX/74R;

.field public static final enum COMPOSER_TAGGING_DONE:LX/74R;

.field public static final enum COMPOSER_TAGGING_LAUNCHED:LX/74R;

.field public static final enum COMPOSER_TAGS_TOGGLED:LX/74R;

.field public static final enum COMPOSER_UPDATE_ATTACHMENT_VIDEO:LX/74R;

.field public static final enum ENTER_GALLERY:LX/74R;

.field public static final enum ENTER_GRID:LX/74R;

.field public static final enum EXTERNAL_PHOTO:LX/74R;

.field public static final enum EXTERNAL_VIDEO:LX/74R;

.field public static final enum LAUNCHED_MULTIPICKER:LX/74R;

.field public static final enum LAUNCH_EXTERNAL_PICKER:LX/74R;

.field public static final enum LOAD_MEDIA:LX/74R;

.field public static final enum MEDIA_POST_FAILURE:LX/74R;

.field public static final enum MEDIA_POST_START:LX/74R;

.field public static final enum MEDIA_POST_SUCCESS:LX/74R;

.field public static final enum MEDIA_PUBLISH_FAILURE:LX/74R;

.field public static final enum MEDIA_PUBLISH_RECEIVED:LX/74R;

.field public static final enum MEDIA_PUBLISH_SENT:LX/74R;

.field public static final enum MEDIA_PUBLISH_START:LX/74R;

.field public static final enum MEDIA_PUBLISH_SUCCESS:LX/74R;

.field public static final enum MEDIA_UPLOAD_ATTEMPT_FAILURE:LX/74R;

.field public static final enum MEDIA_UPLOAD_ATTEMPT_GET_METADATA_FAILURE:LX/74R;

.field public static final enum MEDIA_UPLOAD_ATTEMPT_INCOMPLETE:LX/74R;

.field public static final enum MEDIA_UPLOAD_ATTEMPT_PRESERVE_SPHERICAL_METADATA:LX/74R;

.field public static final enum MEDIA_UPLOAD_ATTEMPT_RETRY:LX/74R;

.field public static final enum MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_FAILURE:LX/74R;

.field public static final enum MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_SKIPPED:LX/74R;

.field public static final enum MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_START:LX/74R;

.field public static final enum MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_SUCCESS:LX/74R;

.field public static final enum MEDIA_UPLOAD_BATCH_CANCEL:LX/74R;

.field public static final enum MEDIA_UPLOAD_BATCH_FAILURE:LX/74R;

.field public static final enum MEDIA_UPLOAD_BATCH_START:LX/74R;

.field public static final enum MEDIA_UPLOAD_BATCH_SUCCESS:LX/74R;

.field public static final enum MEDIA_UPLOAD_CANCEL:LX/74R;

.field public static final enum MEDIA_UPLOAD_CANCEL_REQUEST_FAILURE:LX/74R;

.field public static final enum MEDIA_UPLOAD_CANCEL_REQUEST_START:LX/74R;

.field public static final enum MEDIA_UPLOAD_CANCEL_REQUEST_SUCCESS:LX/74R;

.field public static final enum MEDIA_UPLOAD_CANCEL_SURVEY:LX/74R;

.field public static final enum MEDIA_UPLOAD_CHECKPOINT_FAILURE:LX/74R;

.field public static final enum MEDIA_UPLOAD_CHUNK_TRANSFER_CANCEL:LX/74R;

.field public static final enum MEDIA_UPLOAD_CHUNK_TRANSFER_FAILURE:LX/74R;

.field public static final enum MEDIA_UPLOAD_CHUNK_TRANSFER_RESPONSE:LX/74R;

.field public static final enum MEDIA_UPLOAD_CHUNK_TRANSFER_START:LX/74R;

.field public static final enum MEDIA_UPLOAD_CHUNK_TRANSFER_SUCCESS:LX/74R;

.field public static final enum MEDIA_UPLOAD_DIAGNOSTIC:LX/74R;

.field public static final enum MEDIA_UPLOAD_FAILURE:LX/74R;

.field public static final enum MEDIA_UPLOAD_FLOW_CANCEL:LX/74R;

.field public static final enum MEDIA_UPLOAD_FLOW_FATAL:LX/74R;

.field public static final enum MEDIA_UPLOAD_FLOW_GIVEUP:LX/74R;

.field public static final enum MEDIA_UPLOAD_FLOW_START:LX/74R;

.field public static final enum MEDIA_UPLOAD_FLOW_SUCCESS:LX/74R;

.field public static final enum MEDIA_UPLOAD_HASH_COMPUTE_END:LX/74R;

.field public static final enum MEDIA_UPLOAD_HASH_COMPUTE_SKIPPED:LX/74R;

.field public static final enum MEDIA_UPLOAD_HASH_COMPUTE_START:LX/74R;

.field public static final enum MEDIA_UPLOAD_INIT_CANCEL:LX/74R;

.field public static final enum MEDIA_UPLOAD_INIT_CONTEXT:LX/74R;

.field public static final enum MEDIA_UPLOAD_INIT_FAILURE:LX/74R;

.field public static final enum MEDIA_UPLOAD_INIT_START:LX/74R;

.field public static final enum MEDIA_UPLOAD_INIT_SUCCESS:LX/74R;

.field public static final enum MEDIA_UPLOAD_MISSING_ORIGINAL_MEDIA_FILE:LX/74R;

.field public static final enum MEDIA_UPLOAD_PROCESS_CANCEL:LX/74R;

.field public static final enum MEDIA_UPLOAD_PROCESS_CHECKPOINT_PERSISTED_TRANSCODE_INFO:LX/74R;

.field public static final enum MEDIA_UPLOAD_PROCESS_ENHANCEMENT:LX/74R;

.field public static final enum MEDIA_UPLOAD_PROCESS_FAILURE:LX/74R;

.field public static final enum MEDIA_UPLOAD_PROCESS_FOUND_EXISTING_FILE:LX/74R;

.field public static final enum MEDIA_UPLOAD_PROCESS_MISSING_EXISTING_FILE:LX/74R;

.field public static final enum MEDIA_UPLOAD_PROCESS_OMITTED_PREVIOUS_ATTEMPT:LX/74R;

.field public static final enum MEDIA_UPLOAD_PROCESS_OMITTED_TOO_MANY_FAILURES:LX/74R;

.field public static final enum MEDIA_UPLOAD_PROCESS_READ_PERSISTED_TRANSCODE_INFO:LX/74R;

.field public static final enum MEDIA_UPLOAD_PROCESS_SKIPPED:LX/74R;

.field public static final enum MEDIA_UPLOAD_PROCESS_START:LX/74R;

.field public static final enum MEDIA_UPLOAD_PROCESS_SUCCESS:LX/74R;

.field public static final enum MEDIA_UPLOAD_RESTART:LX/74R;

.field public static final enum MEDIA_UPLOAD_RESTART_FAILURE:LX/74R;

.field public static final enum MEDIA_UPLOAD_RETRY_QUEUE_CHECK:LX/74R;

.field public static final enum MEDIA_UPLOAD_SEGMENTED_TRANSCODE_FAILURE:LX/74R;

.field public static final enum MEDIA_UPLOAD_SEGMENTED_TRANSCODE_START:LX/74R;

.field public static final enum MEDIA_UPLOAD_SEGMENTED_TRANSCODE_SUCCESS:LX/74R;

.field public static final enum MEDIA_UPLOAD_START:LX/74R;

.field public static final enum MEDIA_UPLOAD_SUCCESS:LX/74R;

.field public static final enum MEDIA_UPLOAD_TRANSFER_CANCEL:LX/74R;

.field public static final enum MEDIA_UPLOAD_TRANSFER_FAILURE:LX/74R;

.field public static final enum MEDIA_UPLOAD_TRANSFER_SKIP:LX/74R;

.field public static final enum MEDIA_UPLOAD_TRANSFER_START:LX/74R;

.field public static final enum MEDIA_UPLOAD_TRANSFER_SUCCESS:LX/74R;

.field public static final enum PHOTOS_OF_LOADING_SECTIONS:LX/74R;

.field public static final enum PHOTOS_TAB_NAV:LX/74R;

.field public static final enum PHOTOS_UPLOADED_LOADING:LX/74R;

.field public static final enum PHOTO_GALLERY_MENU_SHOWN:LX/74R;

.field public static final enum PHOTO_SAVE_SUCCEEDED:LX/74R;

.field public static final enum PHOTO_SHARE_EXTERNALLY:LX/74R;

.field public static final enum PHOTO_SWIPE_ACTION:LX/74R;

.field public static final enum PICKED_MEDIA:LX/74R;

.field public static final enum PICKED_MEDIA_IMPLICIT:LX/74R;

.field public static final enum POST_POST_CLOSE_PRESSED:LX/74R;

.field public static final enum POST_POST_DIALOG_CANCELLED:LX/74R;

.field public static final enum POST_POST_DIALOG_FAILED_LAUNCH:LX/74R;

.field public static final enum POST_POST_DIALOG_LAUNCHED:LX/74R;

.field public static final enum ROTATE:LX/74R;

.field public static final enum SHOW_USB_ERROR:LX/74R;

.field public static final enum STARTED_FLOW_MULTIPICKER:LX/74R;

.field public static final enum TAGS_DELETE_FAILED:LX/74R;

.field public static final enum TAGS_DELETE_SUCCEEDED:LX/74R;

.field public static final enum TAGS_UPLOAD_FAILED:LX/74R;

.field public static final enum TAGS_UPLOAD_SUCCEEDED:LX/74R;

.field public static final enum TAG_CANCELLED:LX/74R;

.field public static final enum TAG_CREATED:LX/74R;

.field public static final enum TAG_DELETED:LX/74R;

.field public static final enum TAG_INITIATED:LX/74R;

.field public static final enum TAG_PHOTO:LX/74R;

.field public static final enum TAG_START_TYPING:LX/74R;

.field public static final enum TAG_TYPEAHEAD_OPEN:LX/74R;

.field public static final enum UPLOAD_EXCEPTION:LX/74R;

.field public static final enum VIDEO_PLAYED_IN_REVIEW:LX/74R;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1167877
    new-instance v0, LX/74R;

    const-string v1, "STARTED_FLOW_MULTIPICKER"

    invoke-direct {v0, v1, v3}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->STARTED_FLOW_MULTIPICKER:LX/74R;

    .line 1167878
    new-instance v0, LX/74R;

    const-string v1, "LAUNCHED_MULTIPICKER"

    invoke-direct {v0, v1, v4}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->LAUNCHED_MULTIPICKER:LX/74R;

    .line 1167879
    new-instance v0, LX/74R;

    const-string v1, "CANCELLED_MULTIPICKER"

    invoke-direct {v0, v1, v5}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CANCELLED_MULTIPICKER:LX/74R;

    .line 1167880
    new-instance v0, LX/74R;

    const-string v1, "LAUNCH_EXTERNAL_PICKER"

    invoke-direct {v0, v1, v6}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->LAUNCH_EXTERNAL_PICKER:LX/74R;

    .line 1167881
    new-instance v0, LX/74R;

    const-string v1, "LOAD_MEDIA"

    invoke-direct {v0, v1, v7}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->LOAD_MEDIA:LX/74R;

    .line 1167882
    new-instance v0, LX/74R;

    const-string v1, "SHOW_USB_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->SHOW_USB_ERROR:LX/74R;

    .line 1167883
    new-instance v0, LX/74R;

    const-string v1, "ALL_SELECTED_BUTTON"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->ALL_SELECTED_BUTTON:LX/74R;

    .line 1167884
    new-instance v0, LX/74R;

    const-string v1, "ENTER_GRID"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->ENTER_GRID:LX/74R;

    .line 1167885
    new-instance v0, LX/74R;

    const-string v1, "ENTER_GALLERY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->ENTER_GALLERY:LX/74R;

    .line 1167886
    new-instance v0, LX/74R;

    const-string v1, "EXTERNAL_VIDEO"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->EXTERNAL_VIDEO:LX/74R;

    .line 1167887
    new-instance v0, LX/74R;

    const-string v1, "EXTERNAL_PHOTO"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->EXTERNAL_PHOTO:LX/74R;

    .line 1167888
    new-instance v0, LX/74R;

    const-string v1, "ROTATE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->ROTATE:LX/74R;

    .line 1167889
    new-instance v0, LX/74R;

    const-string v1, "PICKED_MEDIA"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->PICKED_MEDIA:LX/74R;

    .line 1167890
    new-instance v0, LX/74R;

    const-string v1, "PICKED_MEDIA_IMPLICIT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->PICKED_MEDIA_IMPLICIT:LX/74R;

    .line 1167891
    new-instance v0, LX/74R;

    const-string v1, "PHOTO_SWIPE_ACTION"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->PHOTO_SWIPE_ACTION:LX/74R;

    .line 1167892
    new-instance v0, LX/74R;

    const-string v1, "UPLOAD_EXCEPTION"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->UPLOAD_EXCEPTION:LX/74R;

    .line 1167893
    new-instance v0, LX/74R;

    const-string v1, "TAG_PHOTO"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->TAG_PHOTO:LX/74R;

    .line 1167894
    new-instance v0, LX/74R;

    const-string v1, "CAMERA_FLOW"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CAMERA_FLOW:LX/74R;

    .line 1167895
    new-instance v0, LX/74R;

    const-string v1, "CAMERA_START"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CAMERA_START:LX/74R;

    .line 1167896
    new-instance v0, LX/74R;

    const-string v1, "CAMERA_FLASH"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CAMERA_FLASH:LX/74R;

    .line 1167897
    new-instance v0, LX/74R;

    const-string v1, "CAMERA_SOURCE_SELECT"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CAMERA_SOURCE_SELECT:LX/74R;

    .line 1167898
    new-instance v0, LX/74R;

    const-string v1, "CAMERA_TAKE_PHOTO"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CAMERA_TAKE_PHOTO:LX/74R;

    .line 1167899
    new-instance v0, LX/74R;

    const-string v1, "CAMERA_LOADING"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CAMERA_LOADING:LX/74R;

    .line 1167900
    new-instance v0, LX/74R;

    const-string v1, "CAMERA_DONE_LOADING"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CAMERA_DONE_LOADING:LX/74R;

    .line 1167901
    new-instance v0, LX/74R;

    const-string v1, "CAMERA_EXCEPTION"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CAMERA_EXCEPTION:LX/74R;

    .line 1167902
    new-instance v0, LX/74R;

    const-string v1, "CAMERA_STOPPED"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CAMERA_STOPPED:LX/74R;

    .line 1167903
    new-instance v0, LX/74R;

    const-string v1, "CAMERA_VIDEO_RECORDER_STOPPED"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CAMERA_VIDEO_RECORDER_STOPPED:LX/74R;

    .line 1167904
    new-instance v0, LX/74R;

    const-string v1, "CAMERA_CLOSING"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CAMERA_CLOSING:LX/74R;

    .line 1167905
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_MEDIA_UPLOAD_INIT"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_MEDIA_UPLOAD_INIT:LX/74R;

    .line 1167906
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_ADD_MEDIA"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_ADD_MEDIA:LX/74R;

    .line 1167907
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_BUTTON_CLICKED"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_BUTTON_CLICKED:LX/74R;

    .line 1167908
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_UPDATE_ATTACHMENT_VIDEO"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_UPDATE_ATTACHMENT_VIDEO:LX/74R;

    .line 1167909
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_SUBMIT_VIDEO"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_SUBMIT_VIDEO:LX/74R;

    .line 1167910
    new-instance v0, LX/74R;

    const-string v1, "TAG_INITIATED"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->TAG_INITIATED:LX/74R;

    .line 1167911
    new-instance v0, LX/74R;

    const-string v1, "TAG_START_TYPING"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->TAG_START_TYPING:LX/74R;

    .line 1167912
    new-instance v0, LX/74R;

    const-string v1, "TAG_CREATED"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->TAG_CREATED:LX/74R;

    .line 1167913
    new-instance v0, LX/74R;

    const-string v1, "TAGS_UPLOAD_SUCCEEDED"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->TAGS_UPLOAD_SUCCEEDED:LX/74R;

    .line 1167914
    new-instance v0, LX/74R;

    const-string v1, "TAGS_UPLOAD_FAILED"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->TAGS_UPLOAD_FAILED:LX/74R;

    .line 1167915
    new-instance v0, LX/74R;

    const-string v1, "TAGS_DELETE_SUCCEEDED"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->TAGS_DELETE_SUCCEEDED:LX/74R;

    .line 1167916
    new-instance v0, LX/74R;

    const-string v1, "TAGS_DELETE_FAILED"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->TAGS_DELETE_FAILED:LX/74R;

    .line 1167917
    new-instance v0, LX/74R;

    const-string v1, "TAG_DELETED"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->TAG_DELETED:LX/74R;

    .line 1167918
    new-instance v0, LX/74R;

    const-string v1, "TAG_CANCELLED"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->TAG_CANCELLED:LX/74R;

    .line 1167919
    new-instance v0, LX/74R;

    const-string v1, "TAG_TYPEAHEAD_OPEN"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->TAG_TYPEAHEAD_OPEN:LX/74R;

    .line 1167920
    new-instance v0, LX/74R;

    const-string v1, "PHOTOS_UPLOADED_LOADING"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->PHOTOS_UPLOADED_LOADING:LX/74R;

    .line 1167921
    new-instance v0, LX/74R;

    const-string v1, "PHOTOS_TAB_NAV"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->PHOTOS_TAB_NAV:LX/74R;

    .line 1167922
    new-instance v0, LX/74R;

    const-string v1, "PHOTOS_OF_LOADING_SECTIONS"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->PHOTOS_OF_LOADING_SECTIONS:LX/74R;

    .line 1167923
    new-instance v0, LX/74R;

    const-string v1, "POST_POST_DIALOG_LAUNCHED"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->POST_POST_DIALOG_LAUNCHED:LX/74R;

    .line 1167924
    new-instance v0, LX/74R;

    const-string v1, "POST_POST_DIALOG_FAILED_LAUNCH"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->POST_POST_DIALOG_FAILED_LAUNCH:LX/74R;

    .line 1167925
    new-instance v0, LX/74R;

    const-string v1, "POST_POST_CLOSE_PRESSED"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->POST_POST_CLOSE_PRESSED:LX/74R;

    .line 1167926
    new-instance v0, LX/74R;

    const-string v1, "POST_POST_DIALOG_CANCELLED"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->POST_POST_DIALOG_CANCELLED:LX/74R;

    .line 1167927
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_FLOW_START"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_FLOW_START:LX/74R;

    .line 1167928
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_FLOW_SUCCESS"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_FLOW_SUCCESS:LX/74R;

    .line 1167929
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_FLOW_CANCEL"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_FLOW_CANCEL:LX/74R;

    .line 1167930
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_FLOW_GIVEUP"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_FLOW_GIVEUP:LX/74R;

    .line 1167931
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_FLOW_FATAL"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_FLOW_FATAL:LX/74R;

    .line 1167932
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_ATTEMPT_FAILURE"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_ATTEMPT_FAILURE:LX/74R;

    .line 1167933
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_ATTEMPT_INCOMPLETE"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_ATTEMPT_INCOMPLETE:LX/74R;

    .line 1167934
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_ATTEMPT_RETRY"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_ATTEMPT_RETRY:LX/74R;

    .line 1167935
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_RETRY_QUEUE_CHECK"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_RETRY_QUEUE_CHECK:LX/74R;

    .line 1167936
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_DIAGNOSTIC"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_DIAGNOSTIC:LX/74R;

    .line 1167937
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_CANCEL_SURVEY"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_CANCEL_SURVEY:LX/74R;

    .line 1167938
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_BATCH_START"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_BATCH_START:LX/74R;

    .line 1167939
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_START"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_START:LX/74R;

    .line 1167940
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_MISSING_ORIGINAL_MEDIA_FILE"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_MISSING_ORIGINAL_MEDIA_FILE:LX/74R;

    .line 1167941
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_HASH_COMPUTE_START"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_HASH_COMPUTE_START:LX/74R;

    .line 1167942
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_HASH_COMPUTE_END"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_HASH_COMPUTE_END:LX/74R;

    .line 1167943
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_HASH_COMPUTE_SKIPPED"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_HASH_COMPUTE_SKIPPED:LX/74R;

    .line 1167944
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_ATTEMPT_PRESERVE_SPHERICAL_METADATA"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_ATTEMPT_PRESERVE_SPHERICAL_METADATA:LX/74R;

    .line 1167945
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_ATTEMPT_GET_METADATA_FAILURE"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_ATTEMPT_GET_METADATA_FAILURE:LX/74R;

    .line 1167946
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_START"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_START:LX/74R;

    .line 1167947
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_SUCCESS"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_SUCCESS:LX/74R;

    .line 1167948
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_FAILURE"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_FAILURE:LX/74R;

    .line 1167949
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_SKIPPED"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_SKIPPED:LX/74R;

    .line 1167950
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_PROCESS_START"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_PROCESS_START:LX/74R;

    .line 1167951
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_PROCESS_SUCCESS"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_PROCESS_SUCCESS:LX/74R;

    .line 1167952
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_PROCESS_FAILURE"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_PROCESS_FAILURE:LX/74R;

    .line 1167953
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_PROCESS_CANCEL"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_PROCESS_CANCEL:LX/74R;

    .line 1167954
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_PROCESS_SKIPPED"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_PROCESS_SKIPPED:LX/74R;

    .line 1167955
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_PROCESS_OMITTED_TOO_MANY_FAILURES"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_PROCESS_OMITTED_TOO_MANY_FAILURES:LX/74R;

    .line 1167956
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_PROCESS_OMITTED_PREVIOUS_ATTEMPT"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_PROCESS_OMITTED_PREVIOUS_ATTEMPT:LX/74R;

    .line 1167957
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_PROCESS_FOUND_EXISTING_FILE"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_PROCESS_FOUND_EXISTING_FILE:LX/74R;

    .line 1167958
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_PROCESS_MISSING_EXISTING_FILE"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_PROCESS_MISSING_EXISTING_FILE:LX/74R;

    .line 1167959
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_PROCESS_READ_PERSISTED_TRANSCODE_INFO"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_PROCESS_READ_PERSISTED_TRANSCODE_INFO:LX/74R;

    .line 1167960
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_PROCESS_CHECKPOINT_PERSISTED_TRANSCODE_INFO"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_PROCESS_CHECKPOINT_PERSISTED_TRANSCODE_INFO:LX/74R;

    .line 1167961
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_PROCESS_ENHANCEMENT"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_PROCESS_ENHANCEMENT:LX/74R;

    .line 1167962
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_SEGMENTED_TRANSCODE_START"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_SEGMENTED_TRANSCODE_START:LX/74R;

    .line 1167963
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_SEGMENTED_TRANSCODE_FAILURE"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_SEGMENTED_TRANSCODE_FAILURE:LX/74R;

    .line 1167964
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_SEGMENTED_TRANSCODE_SUCCESS"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_SEGMENTED_TRANSCODE_SUCCESS:LX/74R;

    .line 1167965
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_TRANSFER_SKIP"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_TRANSFER_SKIP:LX/74R;

    .line 1167966
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_TRANSFER_START"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_TRANSFER_START:LX/74R;

    .line 1167967
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_TRANSFER_FAILURE"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_TRANSFER_FAILURE:LX/74R;

    .line 1167968
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_TRANSFER_SUCCESS"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_TRANSFER_SUCCESS:LX/74R;

    .line 1167969
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_TRANSFER_CANCEL"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_TRANSFER_CANCEL:LX/74R;

    .line 1167970
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_INIT_START"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_INIT_START:LX/74R;

    .line 1167971
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_INIT_SUCCESS"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_INIT_SUCCESS:LX/74R;

    .line 1167972
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_INIT_FAILURE"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_INIT_FAILURE:LX/74R;

    .line 1167973
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_INIT_CANCEL"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_INIT_CANCEL:LX/74R;

    .line 1167974
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_CHUNK_TRANSFER_START"

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_START:LX/74R;

    .line 1167975
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_CHUNK_TRANSFER_FAILURE"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_FAILURE:LX/74R;

    .line 1167976
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_CHUNK_TRANSFER_SUCCESS"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_SUCCESS:LX/74R;

    .line 1167977
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_CHUNK_TRANSFER_CANCEL"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_CANCEL:LX/74R;

    .line 1167978
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_CHUNK_TRANSFER_RESPONSE"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_RESPONSE:LX/74R;

    .line 1167979
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_CHECKPOINT_FAILURE"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_CHECKPOINT_FAILURE:LX/74R;

    .line 1167980
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_POST_START"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_POST_START:LX/74R;

    .line 1167981
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_POST_SUCCESS"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_POST_SUCCESS:LX/74R;

    .line 1167982
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_POST_FAILURE"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_POST_FAILURE:LX/74R;

    .line 1167983
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_CANCEL_REQUEST_START"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_CANCEL_REQUEST_START:LX/74R;

    .line 1167984
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_CANCEL_REQUEST_SUCCESS"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_CANCEL_REQUEST_SUCCESS:LX/74R;

    .line 1167985
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_CANCEL_REQUEST_FAILURE"

    const/16 v2, 0x6c

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_CANCEL_REQUEST_FAILURE:LX/74R;

    .line 1167986
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_SUCCESS"

    const/16 v2, 0x6d

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_SUCCESS:LX/74R;

    .line 1167987
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_FAILURE"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_FAILURE:LX/74R;

    .line 1167988
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_CANCEL"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_CANCEL:LX/74R;

    .line 1167989
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_RESTART"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_RESTART:LX/74R;

    .line 1167990
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_RESTART_FAILURE"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_RESTART_FAILURE:LX/74R;

    .line 1167991
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_INIT_CONTEXT"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_INIT_CONTEXT:LX/74R;

    .line 1167992
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_BATCH_SUCCESS"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_BATCH_SUCCESS:LX/74R;

    .line 1167993
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_BATCH_FAILURE"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_BATCH_FAILURE:LX/74R;

    .line 1167994
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_UPLOAD_BATCH_CANCEL"

    const/16 v2, 0x75

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_UPLOAD_BATCH_CANCEL:LX/74R;

    .line 1167995
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_PUBLISH_START"

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_PUBLISH_START:LX/74R;

    .line 1167996
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_PUBLISH_SENT"

    const/16 v2, 0x77

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_PUBLISH_SENT:LX/74R;

    .line 1167997
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_PUBLISH_RECEIVED"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_PUBLISH_RECEIVED:LX/74R;

    .line 1167998
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_PUBLISH_SUCCESS"

    const/16 v2, 0x79

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_PUBLISH_SUCCESS:LX/74R;

    .line 1167999
    new-instance v0, LX/74R;

    const-string v1, "MEDIA_PUBLISH_FAILURE"

    const/16 v2, 0x7a

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->MEDIA_PUBLISH_FAILURE:LX/74R;

    .line 1168000
    new-instance v0, LX/74R;

    const-string v1, "CAMERA_REVIEW"

    const/16 v2, 0x7b

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CAMERA_REVIEW:LX/74R;

    .line 1168001
    new-instance v0, LX/74R;

    const-string v1, "VIDEO_PLAYED_IN_REVIEW"

    const/16 v2, 0x7c

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->VIDEO_PLAYED_IN_REVIEW:LX/74R;

    .line 1168002
    new-instance v0, LX/74R;

    const-string v1, "CAMERA_REVIEW_ACCEPT"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CAMERA_REVIEW_ACCEPT:LX/74R;

    .line 1168003
    new-instance v0, LX/74R;

    const-string v1, "CAMERA_REVIEW_REJECT"

    const/16 v2, 0x7e

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CAMERA_REVIEW_REJECT:LX/74R;

    .line 1168004
    new-instance v0, LX/74R;

    const-string v1, "CAMERA_REVIEW_CANCEL"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->CAMERA_REVIEW_CANCEL:LX/74R;

    .line 1168005
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_EDIT_TAG_SUGGESTION"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_EDIT_TAG_SUGGESTION:LX/74R;

    .line 1168006
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_TAGGING_DONE"

    const/16 v2, 0x81

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_TAGGING_DONE:LX/74R;

    .line 1168007
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_TAGGING_CANCEL"

    const/16 v2, 0x82

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_TAGGING_CANCEL:LX/74R;

    .line 1168008
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_TAGGING_LAUNCHED"

    const/16 v2, 0x83

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_TAGGING_LAUNCHED:LX/74R;

    .line 1168009
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_REORDER_MODE_OPENED"

    const/16 v2, 0x84

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_REORDER_MODE_OPENED:LX/74R;

    .line 1168010
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_REORDER_OCCURRED"

    const/16 v2, 0x85

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_REORDER_OCCURRED:LX/74R;

    .line 1168011
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_REORDER_MODE_CLOSED"

    const/16 v2, 0x86

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_REORDER_MODE_CLOSED:LX/74R;

    .line 1168012
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_ATTACHMENT_REMOVED"

    const/16 v2, 0x87

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_ATTACHMENT_REMOVED:LX/74R;

    .line 1168013
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_ADD_ATTACHMENT_BUTTON_PRESSED"

    const/16 v2, 0x88

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_ADD_ATTACHMENT_BUTTON_PRESSED:LX/74R;

    .line 1168014
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_REORDER_NUX_SHOWN"

    const/16 v2, 0x89

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_REORDER_NUX_SHOWN:LX/74R;

    .line 1168015
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_NUM_UNIQUE_CAPTIONS"

    const/16 v2, 0x8a

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_NUM_UNIQUE_CAPTIONS:LX/74R;

    .line 1168016
    new-instance v0, LX/74R;

    const-string v1, "COMPOSER_TAGS_TOGGLED"

    const/16 v2, 0x8b

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->COMPOSER_TAGS_TOGGLED:LX/74R;

    .line 1168017
    new-instance v0, LX/74R;

    const-string v1, "ANDROID_PHOTOS_CONSUMPTION"

    const/16 v2, 0x8c

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->ANDROID_PHOTOS_CONSUMPTION:LX/74R;

    .line 1168018
    new-instance v0, LX/74R;

    const-string v1, "PHOTO_GALLERY_MENU_SHOWN"

    const/16 v2, 0x8d

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->PHOTO_GALLERY_MENU_SHOWN:LX/74R;

    .line 1168019
    new-instance v0, LX/74R;

    const-string v1, "PHOTO_SAVE_SUCCEEDED"

    const/16 v2, 0x8e

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->PHOTO_SAVE_SUCCEEDED:LX/74R;

    .line 1168020
    new-instance v0, LX/74R;

    const-string v1, "PHOTO_SHARE_EXTERNALLY"

    const/16 v2, 0x8f

    invoke-direct {v0, v1, v2}, LX/74R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74R;->PHOTO_SHARE_EXTERNALLY:LX/74R;

    .line 1168021
    const/16 v0, 0x90

    new-array v0, v0, [LX/74R;

    sget-object v1, LX/74R;->STARTED_FLOW_MULTIPICKER:LX/74R;

    aput-object v1, v0, v3

    sget-object v1, LX/74R;->LAUNCHED_MULTIPICKER:LX/74R;

    aput-object v1, v0, v4

    sget-object v1, LX/74R;->CANCELLED_MULTIPICKER:LX/74R;

    aput-object v1, v0, v5

    sget-object v1, LX/74R;->LAUNCH_EXTERNAL_PICKER:LX/74R;

    aput-object v1, v0, v6

    sget-object v1, LX/74R;->LOAD_MEDIA:LX/74R;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/74R;->SHOW_USB_ERROR:LX/74R;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/74R;->ALL_SELECTED_BUTTON:LX/74R;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/74R;->ENTER_GRID:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/74R;->ENTER_GALLERY:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/74R;->EXTERNAL_VIDEO:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/74R;->EXTERNAL_PHOTO:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/74R;->ROTATE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/74R;->PICKED_MEDIA:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/74R;->PICKED_MEDIA_IMPLICIT:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/74R;->PHOTO_SWIPE_ACTION:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/74R;->UPLOAD_EXCEPTION:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/74R;->TAG_PHOTO:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/74R;->CAMERA_FLOW:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/74R;->CAMERA_START:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/74R;->CAMERA_FLASH:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/74R;->CAMERA_SOURCE_SELECT:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/74R;->CAMERA_TAKE_PHOTO:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/74R;->CAMERA_LOADING:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/74R;->CAMERA_DONE_LOADING:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/74R;->CAMERA_EXCEPTION:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/74R;->CAMERA_STOPPED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/74R;->CAMERA_VIDEO_RECORDER_STOPPED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/74R;->CAMERA_CLOSING:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/74R;->COMPOSER_MEDIA_UPLOAD_INIT:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/74R;->COMPOSER_ADD_MEDIA:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/74R;->COMPOSER_BUTTON_CLICKED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/74R;->COMPOSER_UPDATE_ATTACHMENT_VIDEO:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/74R;->COMPOSER_SUBMIT_VIDEO:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/74R;->TAG_INITIATED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/74R;->TAG_START_TYPING:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/74R;->TAG_CREATED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/74R;->TAGS_UPLOAD_SUCCEEDED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/74R;->TAGS_UPLOAD_FAILED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/74R;->TAGS_DELETE_SUCCEEDED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/74R;->TAGS_DELETE_FAILED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/74R;->TAG_DELETED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/74R;->TAG_CANCELLED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/74R;->TAG_TYPEAHEAD_OPEN:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/74R;->PHOTOS_UPLOADED_LOADING:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/74R;->PHOTOS_TAB_NAV:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, LX/74R;->PHOTOS_OF_LOADING_SECTIONS:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, LX/74R;->POST_POST_DIALOG_LAUNCHED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, LX/74R;->POST_POST_DIALOG_FAILED_LAUNCH:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, LX/74R;->POST_POST_CLOSE_PRESSED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, LX/74R;->POST_POST_DIALOG_CANCELLED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, LX/74R;->MEDIA_UPLOAD_FLOW_START:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, LX/74R;->MEDIA_UPLOAD_FLOW_SUCCESS:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, LX/74R;->MEDIA_UPLOAD_FLOW_CANCEL:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, LX/74R;->MEDIA_UPLOAD_FLOW_GIVEUP:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, LX/74R;->MEDIA_UPLOAD_FLOW_FATAL:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, LX/74R;->MEDIA_UPLOAD_ATTEMPT_FAILURE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, LX/74R;->MEDIA_UPLOAD_ATTEMPT_INCOMPLETE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, LX/74R;->MEDIA_UPLOAD_ATTEMPT_RETRY:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, LX/74R;->MEDIA_UPLOAD_RETRY_QUEUE_CHECK:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, LX/74R;->MEDIA_UPLOAD_DIAGNOSTIC:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, LX/74R;->MEDIA_UPLOAD_CANCEL_SURVEY:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, LX/74R;->MEDIA_UPLOAD_BATCH_START:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, LX/74R;->MEDIA_UPLOAD_START:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, LX/74R;->MEDIA_UPLOAD_MISSING_ORIGINAL_MEDIA_FILE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, LX/74R;->MEDIA_UPLOAD_HASH_COMPUTE_START:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, LX/74R;->MEDIA_UPLOAD_HASH_COMPUTE_END:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, LX/74R;->MEDIA_UPLOAD_HASH_COMPUTE_SKIPPED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, LX/74R;->MEDIA_UPLOAD_ATTEMPT_PRESERVE_SPHERICAL_METADATA:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, LX/74R;->MEDIA_UPLOAD_ATTEMPT_GET_METADATA_FAILURE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, LX/74R;->MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_START:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, LX/74R;->MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_SUCCESS:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, LX/74R;->MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_FAILURE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, LX/74R;->MEDIA_UPLOAD_ATTEMPT_VIDEO_RESIZE_CHECK_SKIPPED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, LX/74R;->MEDIA_UPLOAD_PROCESS_START:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, LX/74R;->MEDIA_UPLOAD_PROCESS_SUCCESS:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, LX/74R;->MEDIA_UPLOAD_PROCESS_FAILURE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, LX/74R;->MEDIA_UPLOAD_PROCESS_CANCEL:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, LX/74R;->MEDIA_UPLOAD_PROCESS_SKIPPED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, LX/74R;->MEDIA_UPLOAD_PROCESS_OMITTED_TOO_MANY_FAILURES:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, LX/74R;->MEDIA_UPLOAD_PROCESS_OMITTED_PREVIOUS_ATTEMPT:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, LX/74R;->MEDIA_UPLOAD_PROCESS_FOUND_EXISTING_FILE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, LX/74R;->MEDIA_UPLOAD_PROCESS_MISSING_EXISTING_FILE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, LX/74R;->MEDIA_UPLOAD_PROCESS_READ_PERSISTED_TRANSCODE_INFO:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, LX/74R;->MEDIA_UPLOAD_PROCESS_CHECKPOINT_PERSISTED_TRANSCODE_INFO:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, LX/74R;->MEDIA_UPLOAD_PROCESS_ENHANCEMENT:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, LX/74R;->MEDIA_UPLOAD_SEGMENTED_TRANSCODE_START:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, LX/74R;->MEDIA_UPLOAD_SEGMENTED_TRANSCODE_FAILURE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, LX/74R;->MEDIA_UPLOAD_SEGMENTED_TRANSCODE_SUCCESS:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, LX/74R;->MEDIA_UPLOAD_TRANSFER_SKIP:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, LX/74R;->MEDIA_UPLOAD_TRANSFER_START:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, LX/74R;->MEDIA_UPLOAD_TRANSFER_FAILURE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, LX/74R;->MEDIA_UPLOAD_TRANSFER_SUCCESS:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, LX/74R;->MEDIA_UPLOAD_TRANSFER_CANCEL:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, LX/74R;->MEDIA_UPLOAD_INIT_START:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, LX/74R;->MEDIA_UPLOAD_INIT_SUCCESS:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, LX/74R;->MEDIA_UPLOAD_INIT_FAILURE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, LX/74R;->MEDIA_UPLOAD_INIT_CANCEL:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_START:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_FAILURE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_SUCCESS:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_CANCEL:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, LX/74R;->MEDIA_UPLOAD_CHUNK_TRANSFER_RESPONSE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, LX/74R;->MEDIA_UPLOAD_CHECKPOINT_FAILURE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, LX/74R;->MEDIA_POST_START:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, LX/74R;->MEDIA_POST_SUCCESS:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, LX/74R;->MEDIA_POST_FAILURE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, LX/74R;->MEDIA_UPLOAD_CANCEL_REQUEST_START:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, LX/74R;->MEDIA_UPLOAD_CANCEL_REQUEST_SUCCESS:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, LX/74R;->MEDIA_UPLOAD_CANCEL_REQUEST_FAILURE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, LX/74R;->MEDIA_UPLOAD_SUCCESS:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, LX/74R;->MEDIA_UPLOAD_FAILURE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, LX/74R;->MEDIA_UPLOAD_CANCEL:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, LX/74R;->MEDIA_UPLOAD_RESTART:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, LX/74R;->MEDIA_UPLOAD_RESTART_FAILURE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, LX/74R;->MEDIA_UPLOAD_INIT_CONTEXT:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, LX/74R;->MEDIA_UPLOAD_BATCH_SUCCESS:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, LX/74R;->MEDIA_UPLOAD_BATCH_FAILURE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, LX/74R;->MEDIA_UPLOAD_BATCH_CANCEL:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, LX/74R;->MEDIA_PUBLISH_START:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, LX/74R;->MEDIA_PUBLISH_SENT:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, LX/74R;->MEDIA_PUBLISH_RECEIVED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, LX/74R;->MEDIA_PUBLISH_SUCCESS:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, LX/74R;->MEDIA_PUBLISH_FAILURE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, LX/74R;->CAMERA_REVIEW:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, LX/74R;->VIDEO_PLAYED_IN_REVIEW:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, LX/74R;->CAMERA_REVIEW_ACCEPT:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, LX/74R;->CAMERA_REVIEW_REJECT:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, LX/74R;->CAMERA_REVIEW_CANCEL:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, LX/74R;->COMPOSER_EDIT_TAG_SUGGESTION:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, LX/74R;->COMPOSER_TAGGING_DONE:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, LX/74R;->COMPOSER_TAGGING_CANCEL:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, LX/74R;->COMPOSER_TAGGING_LAUNCHED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, LX/74R;->COMPOSER_REORDER_MODE_OPENED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, LX/74R;->COMPOSER_REORDER_OCCURRED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, LX/74R;->COMPOSER_REORDER_MODE_CLOSED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x87

    sget-object v2, LX/74R;->COMPOSER_ATTACHMENT_REMOVED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x88

    sget-object v2, LX/74R;->COMPOSER_ADD_ATTACHMENT_BUTTON_PRESSED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x89

    sget-object v2, LX/74R;->COMPOSER_REORDER_NUX_SHOWN:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    sget-object v2, LX/74R;->COMPOSER_NUM_UNIQUE_CAPTIONS:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    sget-object v2, LX/74R;->COMPOSER_TAGS_TOGGLED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    sget-object v2, LX/74R;->ANDROID_PHOTOS_CONSUMPTION:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    sget-object v2, LX/74R;->PHOTO_GALLERY_MENU_SHOWN:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    sget-object v2, LX/74R;->PHOTO_SAVE_SUCCEEDED:LX/74R;

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    sget-object v2, LX/74R;->PHOTO_SHARE_EXTERNALLY:LX/74R;

    aput-object v2, v0, v1

    sput-object v0, LX/74R;->$VALUES:[LX/74R;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1168022
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/74R;
    .locals 1

    .prologue
    .line 1168023
    const-class v0, LX/74R;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/74R;

    return-object v0
.end method

.method public static values()[LX/74R;
    .locals 1

    .prologue
    .line 1168024
    sget-object v0, LX/74R;->$VALUES:[LX/74R;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/74R;

    return-object v0
.end method


# virtual methods
.method public final isFinalFunnelEvent()Z
    .locals 1

    .prologue
    .line 1168025
    sget-object v0, LX/74R;->MEDIA_UPLOAD_FLOW_CANCEL:LX/74R;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/74R;->MEDIA_UPLOAD_FLOW_FATAL:LX/74R;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/74R;->MEDIA_UPLOAD_FLOW_GIVEUP:LX/74R;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/74R;->MEDIA_UPLOAD_FLOW_SUCCESS:LX/74R;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/74R;->MEDIA_UPLOAD_CANCEL:LX/74R;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isStartFunnelEvent()Z
    .locals 1

    .prologue
    .line 1168026
    sget-object v0, LX/74R;->MEDIA_UPLOAD_FLOW_START:LX/74R;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isUploadEvent()Z
    .locals 2

    .prologue
    .line 1168027
    invoke-virtual {p0}, LX/74R;->ordinal()I

    move-result v0

    .line 1168028
    sget-object v1, LX/74R;->MEDIA_UPLOAD_FLOW_START:LX/74R;

    invoke-virtual {v1}, LX/74R;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, LX/74R;->MEDIA_PUBLISH_FAILURE:LX/74R;

    invoke-virtual {v1}, LX/74R;->ordinal()I

    move-result v1

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
