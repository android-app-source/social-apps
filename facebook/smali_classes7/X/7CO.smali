.class public LX/7CO;
.super Landroid/preference/PreferenceCategory;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1180498
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1180499
    return-void
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 2

    .prologue
    .line 1180500
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 1180501
    const-string v0, "Graph Search - internal"

    invoke-virtual {p0, v0}, LX/7CO;->setTitle(Ljava/lang/CharSequence;)V

    .line 1180502
    new-instance v0, LX/4ok;

    invoke-virtual {p0}, LX/7CO;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 1180503
    const-string v1, "Toast all Graph Search errors"

    invoke-virtual {v0, v1}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 1180504
    const-string v1, "Any Graph Search errors encountered (network, unexpected conditions, etc.) will appear in a toast"

    invoke-virtual {v0, v1}, LX/4ok;->setSummary(Ljava/lang/CharSequence;)V

    .line 1180505
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 1180506
    sget-object v1, LX/7C7;->a:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 1180507
    invoke-virtual {p0, v0}, LX/7CO;->addPreference(Landroid/preference/Preference;)Z

    .line 1180508
    return-void
.end method
