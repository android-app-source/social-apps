.class public LX/8GO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:I

.field public final d:LX/8GN;


# direct methods
.method public constructor <init>(IILX/8GN;)V
    .locals 1
    .param p1    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1319336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1319337
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/8GO;->a:Ljava/util/LinkedHashMap;

    .line 1319338
    iput p1, p0, LX/8GO;->b:I

    .line 1319339
    iput p2, p0, LX/8GO;->c:I

    .line 1319340
    iput-object p3, p0, LX/8GO;->d:LX/8GN;

    .line 1319341
    return-void
.end method

.method public static b(LX/8GO;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 2

    .prologue
    .line 1319332
    iget-object v0, p0, LX/8GO;->a:Ljava/util/LinkedHashMap;

    .line 1319333
    iget-object v1, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1319334
    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319335
    return-void
.end method


# virtual methods
.method public final a()LX/8GO;
    .locals 1

    .prologue
    .line 1319330
    iget-object v0, p0, LX/8GO;->d:LX/8GN;

    invoke-virtual {v0}, LX/8GN;->b()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    invoke-static {p0, v0}, LX/8GO;->b(LX/8GO;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V

    .line 1319331
    return-object p0
.end method

.method public final a(LX/0Px;)LX/8GO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;)",
            "LX/8GO;"
        }
    .end annotation

    .prologue
    .line 1319315
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319316
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1319317
    invoke-static {p0, v0}, LX/8GO;->b(LX/8GO;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V

    .line 1319318
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1319319
    :cond_0
    return-object p0
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;Ljava/lang/String;)LX/8GO;
    .locals 7

    .prologue
    .line 1319326
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 1319327
    iget-object v1, p0, LX/8GO;->d:LX/8GN;

    iget v3, p0, LX/8GO;->b:I

    iget v4, p0, LX/8GO;->c:I

    move-object v2, p1

    move-object v5, p2

    move-object v6, v0

    invoke-virtual/range {v1 .. v6}, LX/8GN;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;IILjava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v1

    invoke-static {p0, v1}, LX/8GO;->b(LX/8GO;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V

    .line 1319328
    move-object v0, p0

    .line 1319329
    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1319325
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iget-object v1, p0, LX/8GO;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/0Px;)LX/8GO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;)",
            "LX/8GO;"
        }
    .end annotation

    .prologue
    .line 1319320
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1319321
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1319322
    const-string v3, ""

    invoke-virtual {p0, v0, v3}, LX/8GO;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;Ljava/lang/String;)LX/8GO;

    .line 1319323
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1319324
    :cond_0
    return-object p0
.end method
