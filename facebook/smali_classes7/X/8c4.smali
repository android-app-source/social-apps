.class public final LX/8c4;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$FetchBootstrapKeywordsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1373641
    const-class v1, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$FetchBootstrapKeywordsModel;

    const v0, -0x77eb85f4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchBootstrapKeywords"

    const-string v6, "ec70b9ea3b324793efe1e2f3878cf6ba"

    const-string v7, "viewer"

    const-string v8, "10155069964561729"

    const-string v9, "10155259087166729"

    .line 1373642
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1373643
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1373644
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1373649
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1373650
    sparse-switch v0, :sswitch_data_0

    .line 1373651
    :goto_0
    return-object p1

    .line 1373652
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1373653
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x79c9061b -> :sswitch_1
        0x288d2ee6 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1373645
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1373646
    :goto_1
    return v0

    .line 1373647
    :pswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1373648
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
