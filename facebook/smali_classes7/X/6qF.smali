.class public LX/6qF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1150272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150273
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 1150274
    check-cast p1, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;

    .line 1150275
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1150276
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "old_pin"

    .line 1150277
    iget-object v3, p1, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1150278
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1150279
    iget-object v1, p1, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1150280
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1150281
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "new_pin"

    .line 1150282
    iget-object v3, p1, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1150283
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1150284
    :cond_0
    iget-object v1, p1, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->e:LX/03R;

    move-object v1, v1

    .line 1150285
    if-eqz v1, :cond_1

    .line 1150286
    iget-object v1, p1, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->e:LX/03R;

    move-object v1, v1

    .line 1150287
    invoke-virtual {v1}, LX/03R;->isSet()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1150288
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "payments_protected"

    .line 1150289
    iget-object v3, p1, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->e:LX/03R;

    move-object v3, v3

    .line 1150290
    invoke-virtual {v3}, LX/03R;->asBooleanObject()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1150291
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->e()LX/0P1;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->e()LX/0P1;

    move-result-object v1

    invoke-virtual {v1}, LX/0P1;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1150292
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "thread_profiles_protected"

    new-instance v3, Lorg/json/JSONObject;

    invoke-virtual {p1}, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->e()LX/0P1;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1150293
    :cond_2
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1150294
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "update_payment_pin_status"

    .line 1150295
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1150296
    move-object v1, v1

    .line 1150297
    const-string v2, "POST"

    .line 1150298
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1150299
    move-object v1, v1

    .line 1150300
    const-string v2, "%d"

    .line 1150301
    iget-wide v6, p1, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->b:J

    move-wide v4, v6

    .line 1150302
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1150303
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1150304
    move-object v1, v1

    .line 1150305
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1150306
    move-object v0, v1

    .line 1150307
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1150308
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1150309
    move-object v0, v0

    .line 1150310
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1150311
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1150312
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
