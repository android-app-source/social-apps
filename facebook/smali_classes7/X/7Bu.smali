.class public final LX/7Bu;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1179656
    sget-object v0, LX/7Bt;->a:LX/0U1;

    sget-object v1, LX/7Bt;->b:LX/0U1;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/7Bu;->a:LX/0Px;

    .line 1179657
    new-instance v0, LX/2Qn;

    sget-object v1, LX/7Bt;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    const-string v2, "keywords"

    sget-object v3, LX/7Bv;->a:LX/0U1;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/2Qn;-><init>(LX/0Px;Ljava/lang/String;LX/0Px;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/7Bu;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 1179658
    const-string v0, "keywords_data"

    sget-object v1, LX/7Bu;->a:LX/0Px;

    sget-object v2, LX/7Bu;->b:LX/0Px;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 1179659
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    .line 1179660
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1179661
    const-string v0, "keywords_data"

    const-string v1, "KEYWORDS_DATA_INDEX"

    sget-object v2, LX/7Bt;->b:LX/0U1;

    sget-object v3, LX/7Bt;->a:LX/0U1;

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x53b8126b

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x24c09b97

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1179662
    return-void
.end method
