.class public final LX/760;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public bypassProxyDomains:Ljava/lang/String;

.field public connectTimeout:I

.field public pingRespTimeout:I

.field public proxyAddress:Ljava/lang/String;

.field public proxyFallbackEnabled:Z

.field public proxyPort:I

.field public proxyUserAgent:Ljava/lang/String;

.field public secureProxyAddress:Ljava/lang/String;

.field public secureProxyPort:I

.field public verifyCertificates:Z

.field public zlibCompression:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1170413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1170414
    iput v1, p0, LX/760;->pingRespTimeout:I

    .line 1170415
    const/16 v0, 0xbb8

    iput v0, p0, LX/760;->connectTimeout:I

    .line 1170416
    iput-boolean v2, p0, LX/760;->verifyCertificates:Z

    .line 1170417
    iput-boolean v1, p0, LX/760;->zlibCompression:Z

    .line 1170418
    const-string v0, ""

    iput-object v0, p0, LX/760;->proxyAddress:Ljava/lang/String;

    .line 1170419
    iput v1, p0, LX/760;->proxyPort:I

    .line 1170420
    const-string v0, ""

    iput-object v0, p0, LX/760;->secureProxyAddress:Ljava/lang/String;

    .line 1170421
    iput v1, p0, LX/760;->secureProxyPort:I

    .line 1170422
    const-string v0, ""

    iput-object v0, p0, LX/760;->bypassProxyDomains:Ljava/lang/String;

    .line 1170423
    const-string v0, "WhistleClient"

    iput-object v0, p0, LX/760;->proxyUserAgent:Ljava/lang/String;

    .line 1170424
    iput-boolean v2, p0, LX/760;->proxyFallbackEnabled:Z

    .line 1170425
    return-void
.end method


# virtual methods
.method public build()LX/761;
    .locals 12

    .prologue
    .line 1170426
    new-instance v0, LX/761;

    iget v1, p0, LX/760;->pingRespTimeout:I

    iget v2, p0, LX/760;->connectTimeout:I

    iget-boolean v3, p0, LX/760;->verifyCertificates:Z

    iget-boolean v4, p0, LX/760;->zlibCompression:Z

    iget-object v5, p0, LX/760;->proxyAddress:Ljava/lang/String;

    iget v6, p0, LX/760;->proxyPort:I

    iget-object v7, p0, LX/760;->secureProxyAddress:Ljava/lang/String;

    iget v8, p0, LX/760;->secureProxyPort:I

    iget-object v9, p0, LX/760;->bypassProxyDomains:Ljava/lang/String;

    iget-object v10, p0, LX/760;->proxyUserAgent:Ljava/lang/String;

    iget-boolean v11, p0, LX/760;->proxyFallbackEnabled:Z

    invoke-direct/range {v0 .. v11}, LX/761;-><init>(IIZZLjava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public setProxyFallbackEnabled(Z)LX/760;
    .locals 0

    .prologue
    .line 1170427
    iput-boolean p1, p0, LX/760;->proxyFallbackEnabled:Z

    .line 1170428
    return-object p0
.end method

.method public setProxyUserAgent(Ljava/lang/String;)LX/760;
    .locals 0

    .prologue
    .line 1170429
    iput-object p1, p0, LX/760;->proxyUserAgent:Ljava/lang/String;

    .line 1170430
    return-object p0
.end method
