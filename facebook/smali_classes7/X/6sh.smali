.class public LX/6sh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6sf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6sf",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1153664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153665
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)LX/0Px;
    .locals 1

    .prologue
    .line 1153667
    check-cast p1, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    .line 1153668
    invoke-static {p1}, LX/6si;->a(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/6zU;
    .locals 1

    .prologue
    .line 1153666
    sget-object v0, LX/6zU;->PAYPAL_BILLING_AGREEMENT:LX/6zU;

    return-object v0
.end method
