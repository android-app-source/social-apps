.class public final enum LX/6nM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6nM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6nM;

.field public static final enum DeserializationFailed:LX/6nM;

.field public static final enum Deserialized:LX/6nM;

.field public static final enum NotIncluded:LX/6nM;

.field public static final enum SerializationFailed:LX/6nM;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1147289
    new-instance v0, LX/6nM;

    const-string v1, "Deserialized"

    invoke-direct {v0, v1, v2}, LX/6nM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6nM;->Deserialized:LX/6nM;

    .line 1147290
    new-instance v0, LX/6nM;

    const-string v1, "SerializationFailed"

    invoke-direct {v0, v1, v3}, LX/6nM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6nM;->SerializationFailed:LX/6nM;

    .line 1147291
    new-instance v0, LX/6nM;

    const-string v1, "DeserializationFailed"

    invoke-direct {v0, v1, v4}, LX/6nM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6nM;->DeserializationFailed:LX/6nM;

    .line 1147292
    new-instance v0, LX/6nM;

    const-string v1, "NotIncluded"

    invoke-direct {v0, v1, v5}, LX/6nM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6nM;->NotIncluded:LX/6nM;

    .line 1147293
    const/4 v0, 0x4

    new-array v0, v0, [LX/6nM;

    sget-object v1, LX/6nM;->Deserialized:LX/6nM;

    aput-object v1, v0, v2

    sget-object v1, LX/6nM;->SerializationFailed:LX/6nM;

    aput-object v1, v0, v3

    sget-object v1, LX/6nM;->DeserializationFailed:LX/6nM;

    aput-object v1, v0, v4

    sget-object v1, LX/6nM;->NotIncluded:LX/6nM;

    aput-object v1, v0, v5

    sput-object v0, LX/6nM;->$VALUES:[LX/6nM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1147286
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6nM;
    .locals 1

    .prologue
    .line 1147287
    const-class v0, LX/6nM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6nM;

    return-object v0
.end method

.method public static values()[LX/6nM;
    .locals 1

    .prologue
    .line 1147288
    sget-object v0, LX/6nM;->$VALUES:[LX/6nM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6nM;

    return-object v0
.end method
