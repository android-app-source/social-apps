.class public final enum LX/6xe;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6xe;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6xe;

.field public static final enum HIDDEN:LX/6xe;

.field public static final enum OPTIONAL:LX/6xe;

.field public static final enum REQUIRED:LX/6xe;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1159075
    new-instance v0, LX/6xe;

    const-string v1, "REQUIRED"

    invoke-direct {v0, v1, v2}, LX/6xe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xe;->REQUIRED:LX/6xe;

    .line 1159076
    new-instance v0, LX/6xe;

    const-string v1, "OPTIONAL"

    invoke-direct {v0, v1, v3}, LX/6xe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xe;->OPTIONAL:LX/6xe;

    .line 1159077
    new-instance v0, LX/6xe;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v4}, LX/6xe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6xe;->HIDDEN:LX/6xe;

    .line 1159078
    const/4 v0, 0x3

    new-array v0, v0, [LX/6xe;

    sget-object v1, LX/6xe;->REQUIRED:LX/6xe;

    aput-object v1, v0, v2

    sget-object v1, LX/6xe;->OPTIONAL:LX/6xe;

    aput-object v1, v0, v3

    sget-object v1, LX/6xe;->HIDDEN:LX/6xe;

    aput-object v1, v0, v4

    sput-object v0, LX/6xe;->$VALUES:[LX/6xe;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1159074
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static forValue(Ljava/lang/String;)LX/6xe;
    .locals 2

    .prologue
    .line 1159073
    const-class v0, LX/6xe;

    sget-object v1, LX/6xe;->REQUIRED:LX/6xe;

    invoke-static {v0, p0, v1}, LX/47c;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xe;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6xe;
    .locals 1

    .prologue
    .line 1159072
    const-class v0, LX/6xe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xe;

    return-object v0
.end method

.method public static values()[LX/6xe;
    .locals 1

    .prologue
    .line 1159071
    sget-object v0, LX/6xe;->$VALUES:[LX/6xe;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6xe;

    return-object v0
.end method
