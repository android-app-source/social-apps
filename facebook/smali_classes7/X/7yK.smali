.class public LX/7yK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/6BW;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6BW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1279017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1279018
    iput-object p1, p0, LX/7yK;->a:Landroid/content/Context;

    .line 1279019
    iput-object p2, p0, LX/7yK;->b:LX/6BW;

    .line 1279020
    return-void
.end method

.method public static b(LX/0QB;)LX/7yK;
    .locals 3

    .prologue
    .line 1279015
    new-instance v2, LX/7yK;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/6BW;->a(LX/0QB;)LX/6BW;

    move-result-object v1

    check-cast v1, LX/6BW;

    invoke-direct {v2, v0, v1}, LX/7yK;-><init>(Landroid/content/Context;LX/6BW;)V

    .line 1279016
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1278999
    new-instance v2, Ljava/util/zip/ZipInputStream;

    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v3, p0, LX/7yK;->a:Landroid/content/Context;

    const-string v4, "assets.zip"

    invoke-virtual {v3, v4}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v0}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1279000
    :cond_0
    :try_start_0
    invoke-virtual {v2}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1279001
    invoke-virtual {v0}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1279002
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1279003
    const/16 v0, 0x400

    new-array v0, v0, [B

    .line 1279004
    :goto_0
    invoke-virtual {v2, v0}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    .line 1279005
    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 1279006
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1279007
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v1, :cond_3

    :try_start_2
    invoke-virtual {v2}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    throw v0

    .line 1279008
    :cond_1
    :try_start_3
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 1279009
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1279010
    invoke-virtual {v2}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1279011
    :goto_3
    return-object v0

    .line 1279012
    :cond_2
    invoke-virtual {v2}, Ljava/util/zip/ZipInputStream;->close()V

    move-object v0, v1

    .line 1279013
    goto :goto_3

    .line 1279014
    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v2}, Ljava/util/zip/ZipInputStream;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public final a()V
    .locals 13

    .prologue
    .line 1278961
    iget-object v0, p0, LX/7yK;->a:Landroid/content/Context;

    const-string v1, "assets.zip"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1278962
    new-instance v0, LX/6BT;

    const-string v1, "FaceDetectionAssets"

    invoke-direct {v0, v1}, LX/6BT;-><init>(Ljava/lang/String;)V

    const-string v1, "https://www.facebook.com/mobileassets/facedetection"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1278963
    iput-object v1, v0, LX/6BT;->b:Landroid/net/Uri;

    .line 1278964
    move-object v0, v0

    .line 1278965
    const-string v1, "FaceDetectionAssets"

    .line 1278966
    iput-object v1, v0, LX/6BT;->h:Ljava/lang/String;

    .line 1278967
    move-object v0, v0

    .line 1278968
    const/16 v1, -0x14

    .line 1278969
    iput v1, v0, LX/6BT;->c:I

    .line 1278970
    move-object v0, v0

    .line 1278971
    sget-object v1, LX/6BU;->CAN_BE_ANY:LX/6BU;

    .line 1278972
    iput-object v1, v0, LX/6BT;->d:LX/6BU;

    .line 1278973
    move-object v0, v0

    .line 1278974
    sget-object v1, LX/6BV;->CAN_BE_EXTERNAL:LX/6BV;

    .line 1278975
    iput-object v1, v0, LX/6BT;->e:LX/6BV;

    .line 1278976
    move-object v0, v0

    .line 1278977
    new-instance v3, Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    iget-object v4, v0, LX/6BT;->a:Ljava/lang/String;

    iget-object v5, v0, LX/6BT;->b:Landroid/net/Uri;

    iget v6, v0, LX/6BT;->c:I

    iget-object v7, v0, LX/6BT;->d:LX/6BU;

    iget-object v8, v0, LX/6BT;->e:LX/6BV;

    iget-object v9, v0, LX/6BT;->f:Ljava/lang/String;

    iget-object v10, v0, LX/6BT;->g:Ljava/io/File;

    iget-object v11, v0, LX/6BT;->h:Ljava/lang/String;

    iget-object v12, v0, LX/6BT;->i:Ljava/util/Map;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/assetdownload/AssetDownloadConfiguration;-><init>(Ljava/lang/String;Landroid/net/Uri;ILX/6BU;LX/6BV;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Ljava/util/Map;)V

    move-object v0, v3

    .line 1278978
    iget-object v1, p0, LX/7yK;->b:LX/6BW;

    const/4 v2, 0x1

    .line 1278979
    iget-object v3, v1, LX/6BW;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_1

    if-nez v2, :cond_1

    .line 1278980
    :goto_0
    iget-object v0, p0, LX/7yK;->b:LX/6BW;

    .line 1278981
    iget-object v1, v0, LX/6BW;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Bb;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/6Bb;->a(LX/2Jy;)Z

    .line 1278982
    :cond_0
    return-void

    .line 1278983
    :cond_1
    iget-object v3, v1, LX/6BW;->d:LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->b()V

    .line 1278984
    iget-object v3, v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    move-object v3, v3

    .line 1278985
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1278986
    const/4 v4, 0x0

    .line 1278987
    :goto_1
    move v3, v4

    .line 1278988
    const-string v4, "The identifier of the given configuration is not valid"

    invoke-static {v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1278989
    invoke-virtual {v0}, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->a()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "The source of the configuration must not be null"

    invoke-static {v3, v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1278990
    iget-object v3, v1, LX/6BW;->a:LX/2IT;

    const/4 v4, 0x1

    .line 1278991
    iget-object v1, v3, LX/2IT;->a:LX/2IU;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1278992
    if-eqz v4, :cond_3

    const/4 v1, 0x5

    .line 1278993
    :goto_2
    invoke-static {v3, v2, v1, v0}, LX/2IT;->a(LX/2IT;Landroid/database/sqlite/SQLiteDatabase;ILcom/facebook/assetdownload/AssetDownloadConfiguration;)Z

    .line 1278994
    goto :goto_0

    .line 1278995
    :cond_2
    invoke-static {v1}, LX/6BW;->b(LX/6BW;)Ljava/util/regex/Pattern;

    move-result-object v4

    .line 1278996
    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 1278997
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    goto :goto_1

    .line 1278998
    :cond_3
    const/4 v1, 0x3

    goto :goto_2
.end method

.method public final a(Ljava/io/File;)V
    .locals 7

    .prologue
    .line 1278950
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v1, 0x0

    .line 1278951
    :try_start_1
    iget-object v0, p0, LX/7yK;->a:Landroid/content/Context;

    const-string v3, "assets.zip"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    .line 1278952
    const/16 v3, 0x400

    new-array v3, v3, [B

    .line 1278953
    :goto_0
    invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    .line 1278954
    const/4 v5, 0x0

    invoke-virtual {v0, v3, v5, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 1278955
    :catch_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1278956
    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v1, :cond_1

    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :goto_2
    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception v0

    .line 1278957
    const-string v1, "FaceDetectionAssetDownloader"

    const-string v2, "Couldn\'t read facedetection assets"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1278958
    :goto_3
    return-void

    .line 1278959
    :cond_0
    :try_start_5
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1278960
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    goto :goto_3

    :catch_2
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method
