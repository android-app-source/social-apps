.class public final LX/6g1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/6g2;

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1120876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120877
    sget-object v0, LX/6g2;->UNKNOWN:LX/6g2;

    iput-object v0, p0, LX/6g1;->a:LX/6g2;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/6g1;
    .locals 5

    .prologue
    .line 1120870
    sget-object v0, LX/6g2;->UNKNOWN:LX/6g2;

    iput-object v0, p0, LX/6g1;->a:LX/6g2;

    .line 1120871
    invoke-static {}, LX/6g2;->values()[LX/6g2;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1120872
    invoke-virtual {v3, p1}, LX/6g2;->equalsName(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1120873
    iput-object v3, p0, LX/6g1;->a:LX/6g2;

    .line 1120874
    :cond_0
    return-object p0

    .line 1120875
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)LX/6g1;
    .locals 0

    .prologue
    .line 1120868
    iput-object p1, p0, LX/6g1;->b:Ljava/lang/String;

    .line 1120869
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/6g1;
    .locals 0

    .prologue
    .line 1120866
    iput-object p1, p0, LX/6g1;->c:Ljava/lang/String;

    .line 1120867
    return-object p0
.end method

.method public final d()Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;
    .locals 1

    .prologue
    .line 1120865
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;-><init>(LX/6g1;)V

    return-object v0
.end method
