.class public final LX/7QT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2qI;
.implements LX/2pw;


# instance fields
.field private final a:Lcom/facebook/video/subtitles/views/FbSubtitleView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/subtitles/views/FbSubtitleView;)V
    .locals 1

    .prologue
    .line 1204296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1204297
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1204298
    iput-object p1, p0, LX/7QT;->a:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    .line 1204299
    return-void

    .line 1204300
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1204292
    iget-object v0, p0, LX/7QT;->a:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-virtual {v0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->getMediaTimeMs()I

    move-result v0

    return v0
.end method

.method public final a(LX/7QO;)V
    .locals 1

    .prologue
    .line 1204293
    if-eqz p1, :cond_0

    .line 1204294
    iget-object v0, p0, LX/7QT;->a:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-virtual {v0, p1}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->a(LX/7QO;)V

    .line 1204295
    :cond_0
    return-void
.end method
