.class public LX/6q9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1150087
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150088
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 1150059
    check-cast p1, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;

    .line 1150060
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1150061
    iget-boolean v1, p1, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->d:Z

    move v1, v1

    .line 1150062
    if-eqz v1, :cond_0

    .line 1150063
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "pin"

    .line 1150064
    iget-object v3, p1, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1150065
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1150066
    :goto_0
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1150067
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "delete_payment_pins"

    .line 1150068
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1150069
    move-object v1, v1

    .line 1150070
    const-string v2, "DELETE"

    .line 1150071
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1150072
    move-object v1, v1

    .line 1150073
    const-string v2, "%d"

    .line 1150074
    iget-wide v6, p1, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->c:J

    move-wide v4, v6

    .line 1150075
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1150076
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1150077
    move-object v1, v1

    .line 1150078
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1150079
    move-object v0, v1

    .line 1150080
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1150081
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1150082
    move-object v0, v0

    .line 1150083
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1150084
    :cond_0
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "password"

    .line 1150085
    iget-object v3, p1, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1150086
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1150057
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1150058
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
