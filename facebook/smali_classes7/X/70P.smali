.class public LX/70P;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/70Z;


# direct methods
.method public constructor <init>(LX/70Z;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1162287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1162288
    iput-object p1, p0, LX/70P;->a:LX/70Z;

    .line 1162289
    return-void
.end method

.method private static a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1162290
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Tp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/70P;LX/0lF;)LX/0Px;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1162291
    const-string v0, "available_payment_options"

    invoke-static {p1, v0}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    .line 1162292
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1162293
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1162294
    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6zQ;->forValue(Ljava/lang/String;)LX/6zQ;

    move-result-object v0

    .line 1162295
    iget-object v3, p0, LX/70P;->a:LX/70Z;

    .line 1162296
    iget-object v4, v3, LX/70Z;->c:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/70M;

    .line 1162297
    invoke-interface {v4}, LX/70M;->a()LX/6zQ;

    move-result-object v6

    if-ne v6, v0, :cond_1

    .line 1162298
    :goto_1
    move-object v0, v4

    .line 1162299
    if-eqz v0, :cond_0

    .line 1162300
    invoke-interface {v0, p1}, LX/70M;->a(LX/0lF;)Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;

    move-result-object v0

    .line 1162301
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1162302
    :cond_2
    const-string v0, "available_altpay_options"

    invoke-static {p1, v0}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    .line 1162303
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1162304
    sget-object v3, LX/6zQ;->ALTPAY_ADYEN:LX/6zQ;

    invoke-virtual {v3}, LX/6zQ;->getValue()Ljava/lang/String;

    move-result-object v3

    const-string v4, "payment_method_type"

    invoke-static {v0, v4}, LX/16N;->g(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1162305
    new-instance v3, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;

    const-string v4, "credential_id"

    invoke-static {v0, v4}, LX/70P;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "logo_uri"

    invoke-static {v0, v5}, LX/70P;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const-string v6, "title"

    invoke-static {v0, v6}, LX/70P;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v5, v0}, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1162306
    :cond_4
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_5
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static c(LX/70P;LX/0lF;)LX/0Px;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0lF;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethod;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1162307
    const-string v0, "existing_payment_methods"

    invoke-static {p1, v0}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    .line 1162308
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1162309
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1162310
    const-string v3, "type"

    invoke-static {v0, v3}, LX/16N;->g(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/6zU;->forValue(Ljava/lang/String;)LX/6zU;

    move-result-object v3

    .line 1162311
    iget-object v4, p0, LX/70P;->a:LX/70Z;

    invoke-virtual {v4, v3}, LX/70Z;->a(LX/6zU;)LX/70S;

    move-result-object v3

    .line 1162312
    if-eqz v3, :cond_0

    .line 1162313
    invoke-interface {v3, v0}, LX/70S;->a(LX/0lF;)Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    move-result-object v0

    .line 1162314
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1162315
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
