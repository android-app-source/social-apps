.class public LX/8BQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field private b:LX/8BO;

.field private c:LX/0lB;

.field private d:LX/0cX;


# direct methods
.method public constructor <init>(LX/8BO;LX/0lB;LX/0cX;)V
    .locals 0
    .param p1    # LX/8BO;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1309185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1309186
    iput-object p1, p0, LX/8BQ;->b:LX/8BO;

    .line 1309187
    iput-object p2, p0, LX/8BQ;->c:LX/0lB;

    .line 1309188
    iput-object p3, p0, LX/8BQ;->d:LX/0cX;

    .line 1309189
    invoke-virtual {p0}, LX/8BQ;->b()V

    .line 1309190
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1309191
    iget v0, p0, LX/8BQ;->a:I

    return v0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 12

    .prologue
    const-wide/16 v6, 0x0

    .line 1309192
    iget-object v0, p0, LX/8BQ;->b:LX/8BO;

    const-string v1, "Start of exception handler"

    invoke-virtual {v0, v1}, LX/8BO;->a(Ljava/lang/String;)V

    .line 1309193
    invoke-static {p1}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v2

    .line 1309194
    invoke-virtual {v2}, LX/73z;->e()I

    move-result v0

    .line 1309195
    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/16 v1, 0x1770

    if-eq v0, v1, :cond_0

    const/16 v1, 0x64

    if-eq v0, v1, :cond_0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_1

    .line 1309196
    :cond_0
    iget-object v0, v2, LX/73z;->g:Ljava/lang/Exception;

    move-object v0, v0

    .line 1309197
    throw v0

    .line 1309198
    :cond_1
    instance-of v0, p1, LX/2Oo;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 1309199
    check-cast v0, LX/2Oo;

    .line 1309200
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    .line 1309201
    if-eqz v0, :cond_2

    .line 1309202
    iget v1, v0, Lcom/facebook/http/protocol/ApiErrorResult;->mErrorSubCode:I

    move v1, v1

    .line 1309203
    const v3, 0x14cc5d

    if-ne v1, v3, :cond_2

    .line 1309204
    const/4 v1, 0x0

    .line 1309205
    :try_start_0
    iget-object v3, p0, LX/8BQ;->c:LX/0lB;

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->d()Ljava/lang/String;

    move-result-object v0

    const-class v4, Lcom/facebook/media/upload/video/VideoUploadErrorHandler$InvalidOffsetErrorData;

    invoke-virtual {v3, v0, v4}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/media/upload/video/VideoUploadErrorHandler$InvalidOffsetErrorData;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 1309206
    :goto_0
    if-eqz v1, :cond_2

    iget-wide v4, v1, Lcom/facebook/media/upload/video/VideoUploadErrorHandler$InvalidOffsetErrorData;->startOffset:J

    cmp-long v0, v4, v6

    if-ltz v0, :cond_2

    iget-wide v4, v1, Lcom/facebook/media/upload/video/VideoUploadErrorHandler$InvalidOffsetErrorData;->endOffset:J

    cmp-long v0, v4, v6

    if-ltz v0, :cond_2

    .line 1309207
    new-instance v0, LX/8BP;

    iget-wide v2, v1, Lcom/facebook/media/upload/video/VideoUploadErrorHandler$InvalidOffsetErrorData;->startOffset:J

    iget-wide v4, v1, Lcom/facebook/media/upload/video/VideoUploadErrorHandler$InvalidOffsetErrorData;->endOffset:J

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/8BP;-><init>(Ljava/lang/Throwable;JJ)V

    throw v0

    .line 1309208
    :cond_2
    iget-boolean v0, v2, LX/73z;->m:Z

    move v0, v0

    .line 1309209
    if-nez v0, :cond_3

    .line 1309210
    iget-object v0, v2, LX/73z;->g:Ljava/lang/Exception;

    move-object v0, v0

    .line 1309211
    throw v0

    .line 1309212
    :cond_3
    iget v0, p0, LX/8BQ;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/8BQ;->a:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_4

    .line 1309213
    const-wide v8, 0x40b3880000000000L    # 5000.0

    iget v10, p0, LX/8BQ;->a:I

    int-to-double v10, v10

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    double-to-long v8, v8

    move-wide v0, v8

    .line 1309214
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 1309215
    return-void

    .line 1309216
    :cond_4
    iget-object v0, v2, LX/73z;->g:Ljava/lang/Exception;

    move-object v0, v0

    .line 1309217
    throw v0

    :catch_0
    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1309218
    const/4 v0, 0x0

    iput v0, p0, LX/8BQ;->a:I

    .line 1309219
    return-void
.end method
