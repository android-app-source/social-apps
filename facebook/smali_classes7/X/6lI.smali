.class public LX/6lI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1142551
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142552
    iput-object p1, p0, LX/6lI;->a:Ljava/lang/String;

    .line 1142553
    iput p2, p0, LX/6lI;->b:I

    .line 1142554
    iput-object p3, p0, LX/6lI;->c:Ljava/lang/String;

    .line 1142555
    iput-object p4, p0, LX/6lI;->d:Ljava/lang/String;

    .line 1142556
    iput-object p5, p0, LX/6lI;->e:Ljava/lang/String;

    .line 1142557
    iput p6, p0, LX/6lI;->f:I

    .line 1142558
    iput-object p7, p0, LX/6lI;->g:Ljava/lang/String;

    .line 1142559
    iput-object p8, p0, LX/6lI;->h:Ljava/lang/String;

    .line 1142560
    iput p9, p0, LX/6lI;->i:I

    .line 1142561
    return-void
.end method

.method public static a(LX/6lI;LX/0XI;)LX/0XI;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1142562
    iget v0, p0, LX/6lI;->b:I

    if-eq v0, v3, :cond_0

    .line 1142563
    :goto_0
    return-object p1

    .line 1142564
    :cond_0
    iget-object v0, p1, LX/0XI;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1142565
    invoke-static {v0}, LX/0XI;->o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1142566
    const/4 v0, 0x0

    .line 1142567
    iput-object v0, p1, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 1142568
    move-object v0, p1

    .line 1142569
    iget-object v1, p0, LX/6lI;->d:Ljava/lang/String;

    .line 1142570
    iput-object v1, v0, LX/0XI;->h:Ljava/lang/String;

    .line 1142571
    move-object v0, v0

    .line 1142572
    iget-object v1, p0, LX/6lI;->g:Ljava/lang/String;

    .line 1142573
    iput-object v1, v0, LX/0XI;->y:Ljava/lang/String;

    .line 1142574
    iget v0, p0, LX/6lI;->f:I

    if-ne v0, v2, :cond_2

    .line 1142575
    iput-boolean v2, p1, LX/0XI;->F:Z

    .line 1142576
    :cond_1
    :goto_1
    invoke-static {p0}, LX/6lI;->a(LX/6lI;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 1142577
    iput-object v0, p1, LX/0XI;->al:Lcom/facebook/user/model/User;

    .line 1142578
    move-object p1, p1

    .line 1142579
    goto :goto_0

    .line 1142580
    :cond_2
    iget v0, p0, LX/6lI;->f:I

    if-ne v0, v3, :cond_1

    .line 1142581
    const/4 v0, 0x0

    .line 1142582
    iput-boolean v0, p1, LX/0XI;->F:Z

    .line 1142583
    move-object v0, p1

    .line 1142584
    iput-boolean v2, v0, LX/0XI;->G:Z

    .line 1142585
    goto :goto_1
.end method

.method public static a(LX/6lI;)Lcom/facebook/user/model/User;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1142586
    iget v0, p0, LX/6lI;->b:I

    if-eq v0, v5, :cond_0

    iget v0, p0, LX/6lI;->b:I

    if-eq v0, v1, :cond_0

    .line 1142587
    const/4 v0, 0x0

    .line 1142588
    :goto_0
    return-object v0

    .line 1142589
    :cond_0
    new-instance v0, LX/0XI;

    invoke-direct {v0}, LX/0XI;-><init>()V

    sget-object v3, LX/0XG;->FACEBOOK:LX/0XG;

    iget-object v4, p0, LX/6lI;->c:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v0

    iget-object v3, p0, LX/6lI;->e:Ljava/lang/String;

    .line 1142590
    iput-object v3, v0, LX/0XI;->i:Ljava/lang/String;

    .line 1142591
    move-object v0, v0

    .line 1142592
    iget-object v3, p0, LX/6lI;->d:Ljava/lang/String;

    .line 1142593
    iput-object v3, v0, LX/0XI;->h:Ljava/lang/String;

    .line 1142594
    move-object v3, v0

    .line 1142595
    iget v0, p0, LX/6lI;->f:I

    if-ne v0, v1, :cond_1

    move v0, v1

    .line 1142596
    :goto_1
    iput-boolean v0, v3, LX/0XI;->F:Z

    .line 1142597
    move-object v0, v3

    .line 1142598
    iget v3, p0, LX/6lI;->f:I

    if-ne v3, v5, :cond_2

    .line 1142599
    :goto_2
    iput-boolean v1, v0, LX/0XI;->G:Z

    .line 1142600
    move-object v0, v0

    .line 1142601
    iget-object v1, p0, LX/6lI;->g:Ljava/lang/String;

    .line 1142602
    iput-object v1, v0, LX/0XI;->y:Ljava/lang/String;

    .line 1142603
    move-object v0, v0

    .line 1142604
    iget-object v1, p0, LX/6lI;->h:Ljava/lang/String;

    .line 1142605
    iput-object v1, v0, LX/0XI;->l:Ljava/lang/String;

    .line 1142606
    move-object v0, v0

    .line 1142607
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1142608
    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1142609
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "address"

    iget-object v2, p0, LX/6lI;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "status"

    iget v2, p0, LX/6lI;->b:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "fbId"

    iget-object v2, p0, LX/6lI;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "displayName"

    iget-object v2, p0, LX/6lI;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "firstName"

    iget-object v2, p0, LX/6lI;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "relationship"

    iget v2, p0, LX/6lI;->f:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "profileType"

    iget-object v2, p0, LX/6lI;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "username"

    iget-object v2, p0, LX/6lI;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "timesShown"

    iget v2, p0, LX/6lI;->i:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
