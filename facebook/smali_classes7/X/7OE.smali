.class public final enum LX/7OE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7OE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7OE;

.field public static final enum CENTER:LX/7OE;

.field public static final enum TOP:LX/7OE;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1201236
    new-instance v0, LX/7OE;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v2}, LX/7OE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7OE;->CENTER:LX/7OE;

    .line 1201237
    new-instance v0, LX/7OE;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v3}, LX/7OE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7OE;->TOP:LX/7OE;

    .line 1201238
    const/4 v0, 0x2

    new-array v0, v0, [LX/7OE;

    sget-object v1, LX/7OE;->CENTER:LX/7OE;

    aput-object v1, v0, v2

    sget-object v1, LX/7OE;->TOP:LX/7OE;

    aput-object v1, v0, v3

    sput-object v0, LX/7OE;->$VALUES:[LX/7OE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1201239
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7OE;
    .locals 1

    .prologue
    .line 1201240
    const-class v0, LX/7OE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7OE;

    return-object v0
.end method

.method public static values()[LX/7OE;
    .locals 1

    .prologue
    .line 1201241
    sget-object v0, LX/7OE;->$VALUES:[LX/7OE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7OE;

    return-object v0
.end method
