.class public final LX/7u4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1269859
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1269860
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1269861
    :goto_0
    return v1

    .line 1269862
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1269863
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1269864
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1269865
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1269866
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1269867
    const-string v5, "object"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1269868
    invoke-static {p0, p1}, LX/7u0;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1269869
    :cond_2
    const-string v5, "taggable_activity"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1269870
    invoke-static {p0, p1}, LX/7u3;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1269871
    :cond_3
    const-string v5, "taggable_activity_icon"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1269872
    invoke-static {p0, p1}, LX/7u2;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1269873
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1269874
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1269875
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1269876
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1269877
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1269878
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1269879
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1269880
    if-eqz v0, :cond_0

    .line 1269881
    const-string v1, "object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1269882
    invoke-static {p0, v0, p2}, LX/7u0;->a(LX/15i;ILX/0nX;)V

    .line 1269883
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1269884
    if-eqz v0, :cond_1

    .line 1269885
    const-string v1, "taggable_activity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1269886
    invoke-static {p0, v0, p2, p3}, LX/7u3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1269887
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1269888
    if-eqz v0, :cond_2

    .line 1269889
    const-string v1, "taggable_activity_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1269890
    invoke-static {p0, v0, p2, p3}, LX/7u2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1269891
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1269892
    return-void
.end method
