.class public LX/6mK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;


# instance fields
.field public final href:Ljava/lang/String;

.field public final is_logged_out_push:Ljava/lang/Boolean;

.field public final message:Ljava/lang/String;

.field public final params:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final target_uid:Ljava/lang/Long;

.field public final time:Ljava/lang/Long;

.field public final type:Ljava/lang/String;

.field public final unread_count:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0xa

    const/16 v6, 0x8

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0xb

    .line 1143724
    new-instance v0, LX/1sv;

    const-string v1, "AndroidNotificationPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mK;->b:LX/1sv;

    .line 1143725
    new-instance v0, LX/1sw;

    const-string v1, "type"

    invoke-direct {v0, v1, v3, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mK;->c:LX/1sw;

    .line 1143726
    new-instance v0, LX/1sw;

    const-string v1, "time"

    invoke-direct {v0, v1, v7, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mK;->d:LX/1sw;

    .line 1143727
    new-instance v0, LX/1sw;

    const-string v1, "message"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mK;->e:LX/1sw;

    .line 1143728
    new-instance v0, LX/1sw;

    const-string v1, "unread_count"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mK;->f:LX/1sw;

    .line 1143729
    new-instance v0, LX/1sw;

    const-string v1, "target_uid"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v7, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mK;->g:LX/1sw;

    .line 1143730
    new-instance v0, LX/1sw;

    const-string v1, "href"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mK;->h:LX/1sw;

    .line 1143731
    new-instance v0, LX/1sw;

    const-string v1, "params"

    const/16 v2, 0xd

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mK;->i:LX/1sw;

    .line 1143732
    new-instance v0, LX/1sw;

    const-string v1, "is_logged_out_push"

    invoke-direct {v0, v1, v5, v6}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mK;->j:LX/1sw;

    .line 1143733
    sput-boolean v4, LX/6mK;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/util/Map;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1143734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1143735
    iput-object p1, p0, LX/6mK;->type:Ljava/lang/String;

    .line 1143736
    iput-object p2, p0, LX/6mK;->time:Ljava/lang/Long;

    .line 1143737
    iput-object p3, p0, LX/6mK;->message:Ljava/lang/String;

    .line 1143738
    iput-object p4, p0, LX/6mK;->unread_count:Ljava/lang/Integer;

    .line 1143739
    iput-object p5, p0, LX/6mK;->target_uid:Ljava/lang/Long;

    .line 1143740
    iput-object p6, p0, LX/6mK;->href:Ljava/lang/String;

    .line 1143741
    iput-object p7, p0, LX/6mK;->params:Ljava/util/Map;

    .line 1143742
    iput-object p8, p0, LX/6mK;->is_logged_out_push:Ljava/lang/Boolean;

    .line 1143743
    return-void
.end method

.method public static b(LX/1su;)LX/6mK;
    .locals 15

    .prologue
    const/16 v14, 0xa

    const/4 v9, 0x0

    const/16 v13, 0xb

    const/4 v8, 0x0

    .line 1143744
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v7, v8

    move-object v6, v8

    move-object v5, v8

    move-object v4, v8

    move-object v3, v8

    move-object v2, v8

    move-object v1, v8

    .line 1143745
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 1143746
    iget-byte v10, v0, LX/1sw;->b:B

    if-eqz v10, :cond_a

    .line 1143747
    iget-short v10, v0, LX/1sw;->c:S

    packed-switch v10, :pswitch_data_0

    .line 1143748
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1143749
    :pswitch_0
    iget-byte v10, v0, LX/1sw;->b:B

    if-ne v10, v13, :cond_1

    .line 1143750
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1143751
    :cond_1
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1143752
    :pswitch_1
    iget-byte v10, v0, LX/1sw;->b:B

    if-ne v10, v14, :cond_2

    .line 1143753
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_0

    .line 1143754
    :cond_2
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1143755
    :pswitch_2
    iget-byte v10, v0, LX/1sw;->b:B

    if-ne v10, v13, :cond_3

    .line 1143756
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 1143757
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1143758
    :pswitch_3
    iget-byte v10, v0, LX/1sw;->b:B

    const/16 v11, 0x8

    if-ne v10, v11, :cond_4

    .line 1143759
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_0

    .line 1143760
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1143761
    :pswitch_4
    iget-byte v10, v0, LX/1sw;->b:B

    if-ne v10, v14, :cond_5

    .line 1143762
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_0

    .line 1143763
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1143764
    :pswitch_5
    iget-byte v10, v0, LX/1sw;->b:B

    if-ne v10, v13, :cond_6

    .line 1143765
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 1143766
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1143767
    :pswitch_6
    iget-byte v10, v0, LX/1sw;->b:B

    const/16 v11, 0xd

    if-ne v10, v11, :cond_8

    .line 1143768
    invoke-virtual {p0}, LX/1su;->g()LX/7H3;

    move-result-object v10

    .line 1143769
    new-instance v7, Ljava/util/HashMap;

    iget v0, v10, LX/7H3;->c:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v9, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v7, v0}, Ljava/util/HashMap;-><init>(I)V

    move v0, v9

    .line 1143770
    :goto_1
    iget v11, v10, LX/7H3;->c:I

    if-gez v11, :cond_7

    invoke-static {}, LX/1su;->s()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1143771
    :goto_2
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v11

    .line 1143772
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v12

    .line 1143773
    invoke-interface {v7, v11, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1143774
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1143775
    :cond_7
    iget v11, v10, LX/7H3;->c:I

    if-ge v0, v11, :cond_0

    goto :goto_2

    .line 1143776
    :cond_8
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1143777
    :pswitch_7
    iget-byte v10, v0, LX/1sw;->b:B

    const/4 v11, 0x2

    if-ne v10, v11, :cond_9

    .line 1143778
    invoke-virtual {p0}, LX/1su;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    goto/16 :goto_0

    .line 1143779
    :cond_9
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1143780
    :cond_a
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1143781
    new-instance v0, LX/6mK;

    invoke-direct/range {v0 .. v8}, LX/6mK;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/util/Map;Ljava/lang/Boolean;)V

    .line 1143782
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1143611
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1143612
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v1, v0

    .line 1143613
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1143614
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "AndroidNotificationPayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1143615
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143616
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143617
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143618
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143619
    const-string v4, "type"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143620
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143621
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143622
    iget-object v4, p0, LX/6mK;->type:Ljava/lang/String;

    if-nez v4, :cond_6

    .line 1143623
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143624
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143625
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143626
    const-string v4, "time"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143627
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143628
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143629
    iget-object v4, p0, LX/6mK;->time:Ljava/lang/Long;

    if-nez v4, :cond_7

    .line 1143630
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143631
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143632
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143633
    const-string v4, "message"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143634
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143635
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143636
    iget-object v4, p0, LX/6mK;->message:Ljava/lang/String;

    if-nez v4, :cond_8

    .line 1143637
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143638
    :goto_5
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143639
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143640
    const-string v4, "unread_count"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143641
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143642
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143643
    iget-object v4, p0, LX/6mK;->unread_count:Ljava/lang/Integer;

    if-nez v4, :cond_9

    .line 1143644
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143645
    :goto_6
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143646
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143647
    const-string v4, "target_uid"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143648
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143649
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143650
    iget-object v4, p0, LX/6mK;->target_uid:Ljava/lang/Long;

    if-nez v4, :cond_a

    .line 1143651
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143652
    :goto_7
    iget-object v4, p0, LX/6mK;->href:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 1143653
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143654
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143655
    const-string v4, "href"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143656
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143657
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143658
    iget-object v4, p0, LX/6mK;->href:Ljava/lang/String;

    if-nez v4, :cond_b

    .line 1143659
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143660
    :cond_0
    :goto_8
    iget-object v4, p0, LX/6mK;->params:Ljava/util/Map;

    if-eqz v4, :cond_1

    .line 1143661
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143662
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143663
    const-string v4, "params"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143664
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143665
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143666
    iget-object v4, p0, LX/6mK;->params:Ljava/util/Map;

    if-nez v4, :cond_c

    .line 1143667
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143668
    :cond_1
    :goto_9
    iget-object v4, p0, LX/6mK;->is_logged_out_push:Ljava/lang/Boolean;

    if-eqz v4, :cond_2

    .line 1143669
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143670
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143671
    const-string v4, "is_logged_out_push"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143672
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143673
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143674
    iget-object v0, p0, LX/6mK;->is_logged_out_push:Ljava/lang/Boolean;

    if-nez v0, :cond_d

    .line 1143675
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143676
    :cond_2
    :goto_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143677
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1143678
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1143679
    :cond_3
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1143680
    :cond_4
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1143681
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1143682
    :cond_6
    iget-object v4, p0, LX/6mK;->type:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1143683
    :cond_7
    iget-object v4, p0, LX/6mK;->time:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1143684
    :cond_8
    iget-object v4, p0, LX/6mK;->message:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1143685
    :cond_9
    iget-object v4, p0, LX/6mK;->unread_count:Ljava/lang/Integer;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1143686
    :cond_a
    iget-object v4, p0, LX/6mK;->target_uid:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1143687
    :cond_b
    iget-object v4, p0, LX/6mK;->href:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1143688
    :cond_c
    iget-object v4, p0, LX/6mK;->params:Ljava/util/Map;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 1143689
    :cond_d
    iget-object v0, p0, LX/6mK;->is_logged_out_push:Ljava/lang/Boolean;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    const/16 v2, 0xb

    .line 1143690
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1143691
    iget-object v0, p0, LX/6mK;->type:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1143692
    sget-object v0, LX/6mK;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143693
    iget-object v0, p0, LX/6mK;->type:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1143694
    :cond_0
    iget-object v0, p0, LX/6mK;->time:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1143695
    sget-object v0, LX/6mK;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143696
    iget-object v0, p0, LX/6mK;->time:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1143697
    :cond_1
    iget-object v0, p0, LX/6mK;->message:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1143698
    sget-object v0, LX/6mK;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143699
    iget-object v0, p0, LX/6mK;->message:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1143700
    :cond_2
    iget-object v0, p0, LX/6mK;->unread_count:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1143701
    sget-object v0, LX/6mK;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143702
    iget-object v0, p0, LX/6mK;->unread_count:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1143703
    :cond_3
    iget-object v0, p0, LX/6mK;->target_uid:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1143704
    sget-object v0, LX/6mK;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143705
    iget-object v0, p0, LX/6mK;->target_uid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1143706
    :cond_4
    iget-object v0, p0, LX/6mK;->href:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1143707
    iget-object v0, p0, LX/6mK;->href:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1143708
    sget-object v0, LX/6mK;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143709
    iget-object v0, p0, LX/6mK;->href:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1143710
    :cond_5
    iget-object v0, p0, LX/6mK;->params:Ljava/util/Map;

    if-eqz v0, :cond_6

    .line 1143711
    iget-object v0, p0, LX/6mK;->params:Ljava/util/Map;

    if-eqz v0, :cond_6

    .line 1143712
    sget-object v0, LX/6mK;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143713
    new-instance v0, LX/7H3;

    iget-object v1, p0, LX/6mK;->params:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v2, v2, v1}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 1143714
    iget-object v0, p0, LX/6mK;->params:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1143715
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, LX/1su;->a(Ljava/lang/String;)V

    .line 1143716
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1143717
    :cond_6
    iget-object v0, p0, LX/6mK;->is_logged_out_push:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 1143718
    iget-object v0, p0, LX/6mK;->is_logged_out_push:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 1143719
    sget-object v0, LX/6mK;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1143720
    iget-object v0, p0, LX/6mK;->is_logged_out_push:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1143721
    :cond_7
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1143722
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1143723
    return-void
.end method

.method public final a(LX/6mK;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1143544
    if-nez p1, :cond_1

    .line 1143545
    :cond_0
    :goto_0
    return v2

    .line 1143546
    :cond_1
    iget-object v0, p0, LX/6mK;->type:Ljava/lang/String;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1143547
    :goto_1
    iget-object v3, p1, LX/6mK;->type:Ljava/lang/String;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1143548
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 1143549
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1143550
    iget-object v0, p0, LX/6mK;->type:Ljava/lang/String;

    iget-object v3, p1, LX/6mK;->type:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1143551
    :cond_3
    iget-object v0, p0, LX/6mK;->time:Ljava/lang/Long;

    if-eqz v0, :cond_14

    move v0, v1

    .line 1143552
    :goto_3
    iget-object v3, p1, LX/6mK;->time:Ljava/lang/Long;

    if-eqz v3, :cond_15

    move v3, v1

    .line 1143553
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1143554
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1143555
    iget-object v0, p0, LX/6mK;->time:Ljava/lang/Long;

    iget-object v3, p1, LX/6mK;->time:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1143556
    :cond_5
    iget-object v0, p0, LX/6mK;->message:Ljava/lang/String;

    if-eqz v0, :cond_16

    move v0, v1

    .line 1143557
    :goto_5
    iget-object v3, p1, LX/6mK;->message:Ljava/lang/String;

    if-eqz v3, :cond_17

    move v3, v1

    .line 1143558
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1143559
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1143560
    iget-object v0, p0, LX/6mK;->message:Ljava/lang/String;

    iget-object v3, p1, LX/6mK;->message:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1143561
    :cond_7
    iget-object v0, p0, LX/6mK;->unread_count:Ljava/lang/Integer;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1143562
    :goto_7
    iget-object v3, p1, LX/6mK;->unread_count:Ljava/lang/Integer;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1143563
    :goto_8
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1143564
    :cond_8
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1143565
    iget-object v0, p0, LX/6mK;->unread_count:Ljava/lang/Integer;

    iget-object v3, p1, LX/6mK;->unread_count:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1143566
    :cond_9
    iget-object v0, p0, LX/6mK;->target_uid:Ljava/lang/Long;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1143567
    :goto_9
    iget-object v3, p1, LX/6mK;->target_uid:Ljava/lang/Long;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 1143568
    :goto_a
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1143569
    :cond_a
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1143570
    iget-object v0, p0, LX/6mK;->target_uid:Ljava/lang/Long;

    iget-object v3, p1, LX/6mK;->target_uid:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1143571
    :cond_b
    iget-object v0, p0, LX/6mK;->href:Ljava/lang/String;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1143572
    :goto_b
    iget-object v3, p1, LX/6mK;->href:Ljava/lang/String;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1143573
    :goto_c
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1143574
    :cond_c
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1143575
    iget-object v0, p0, LX/6mK;->href:Ljava/lang/String;

    iget-object v3, p1, LX/6mK;->href:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1143576
    :cond_d
    iget-object v0, p0, LX/6mK;->params:Ljava/util/Map;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 1143577
    :goto_d
    iget-object v3, p1, LX/6mK;->params:Ljava/util/Map;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 1143578
    :goto_e
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1143579
    :cond_e
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1143580
    iget-object v0, p0, LX/6mK;->params:Ljava/util/Map;

    iget-object v3, p1, LX/6mK;->params:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1143581
    :cond_f
    iget-object v0, p0, LX/6mK;->is_logged_out_push:Ljava/lang/Boolean;

    if-eqz v0, :cond_20

    move v0, v1

    .line 1143582
    :goto_f
    iget-object v3, p1, LX/6mK;->is_logged_out_push:Ljava/lang/Boolean;

    if-eqz v3, :cond_21

    move v3, v1

    .line 1143583
    :goto_10
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1143584
    :cond_10
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1143585
    iget-object v0, p0, LX/6mK;->is_logged_out_push:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6mK;->is_logged_out_push:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_11
    move v2, v1

    .line 1143586
    goto/16 :goto_0

    :cond_12
    move v0, v2

    .line 1143587
    goto/16 :goto_1

    :cond_13
    move v3, v2

    .line 1143588
    goto/16 :goto_2

    :cond_14
    move v0, v2

    .line 1143589
    goto/16 :goto_3

    :cond_15
    move v3, v2

    .line 1143590
    goto/16 :goto_4

    :cond_16
    move v0, v2

    .line 1143591
    goto/16 :goto_5

    :cond_17
    move v3, v2

    .line 1143592
    goto/16 :goto_6

    :cond_18
    move v0, v2

    .line 1143593
    goto/16 :goto_7

    :cond_19
    move v3, v2

    .line 1143594
    goto/16 :goto_8

    :cond_1a
    move v0, v2

    .line 1143595
    goto/16 :goto_9

    :cond_1b
    move v3, v2

    .line 1143596
    goto/16 :goto_a

    :cond_1c
    move v0, v2

    .line 1143597
    goto :goto_b

    :cond_1d
    move v3, v2

    .line 1143598
    goto :goto_c

    :cond_1e
    move v0, v2

    .line 1143599
    goto :goto_d

    :cond_1f
    move v3, v2

    .line 1143600
    goto :goto_e

    :cond_20
    move v0, v2

    .line 1143601
    goto :goto_f

    :cond_21
    move v3, v2

    .line 1143602
    goto :goto_10
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1143603
    if-nez p1, :cond_1

    .line 1143604
    :cond_0
    :goto_0
    return v0

    .line 1143605
    :cond_1
    instance-of v1, p1, LX/6mK;

    if-eqz v1, :cond_0

    .line 1143606
    check-cast p1, LX/6mK;

    invoke-virtual {p0, p1}, LX/6mK;->a(LX/6mK;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1143607
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1143608
    sget-boolean v0, LX/6mK;->a:Z

    .line 1143609
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mK;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1143610
    return-object v0
.end method
