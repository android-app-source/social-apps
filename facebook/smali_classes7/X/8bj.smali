.class public LX/8bj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3hQ;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/8bk;

.field public final c:LX/7CU;

.field public final d:LX/2Rd;


# direct methods
.method public constructor <init>(LX/0Or;LX/8bk;LX/2Rd;LX/7CU;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/3hQ;",
            ">;",
            "LX/8bk;",
            "Lcom/facebook/user/names/Normalizer;",
            "LX/7CU;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1372247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1372248
    iput-object p1, p0, LX/8bj;->a:LX/0Or;

    .line 1372249
    iput-object p2, p0, LX/8bj;->b:LX/8bk;

    .line 1372250
    iput-object p4, p0, LX/8bj;->c:LX/7CU;

    .line 1372251
    iput-object p3, p0, LX/8bj;->d:LX/2Rd;

    .line 1372252
    return-void
.end method

.method public static b(LX/0QB;)LX/8bj;
    .locals 5

    .prologue
    .line 1372253
    new-instance v3, LX/8bj;

    const/16 v0, 0x12d3

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    .line 1372254
    new-instance v0, LX/8bk;

    invoke-direct {v0}, LX/8bk;-><init>()V

    .line 1372255
    move-object v0, v0

    .line 1372256
    move-object v0, v0

    .line 1372257
    check-cast v0, LX/8bk;

    invoke-static {p0}, LX/2Rd;->b(LX/0QB;)LX/2Rd;

    move-result-object v1

    check-cast v1, LX/2Rd;

    invoke-static {p0}, LX/7CU;->b(LX/0QB;)LX/7CU;

    move-result-object v2

    check-cast v2, LX/7CU;

    invoke-direct {v3, v4, v0, v1, v2}, LX/8bj;-><init>(LX/0Or;LX/8bk;LX/2Rd;LX/7CU;)V

    .line 1372258
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/SearchTypeaheadResult;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1372259
    iget-object v1, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->s:LX/0Px;

    invoke-virtual {p1}, Lcom/facebook/search/api/SearchTypeaheadResult;->a()I

    move-result v5

    move-object v0, p0

    .line 1372260
    iget-object v6, v0, LX/8bj;->c:LX/7CU;

    invoke-virtual {v6, v1}, LX/7CU;->a(Ljava/lang/String;)LX/0Px;

    move-result-object p0

    .line 1372261
    const v6, 0x285feb

    if-eq v6, v5, :cond_0

    move-object v6, p0

    .line 1372262
    :goto_0
    move-object v0, v6

    .line 1372263
    return-object v0

    .line 1372264
    :cond_0
    iget-object v6, v0, LX/8bj;->a:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/3hQ;

    .line 1372265
    const/4 p1, 0x0

    .line 1372266
    iput-boolean p1, v6, LX/3hQ;->i:Z

    .line 1372267
    if-eqz v1, :cond_1

    .line 1372268
    new-instance p1, Lcom/facebook/user/model/Name;

    invoke-direct {p1, v1}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, LX/3hQ;->a(Lcom/facebook/user/model/Name;)V

    .line 1372269
    :cond_1
    if-eqz v3, :cond_2

    .line 1372270
    invoke-static {v2, v3}, LX/8bk;->a(LX/15i;I)Lcom/facebook/user/model/Name;

    move-result-object p1

    invoke-virtual {v6, p1}, LX/3hQ;->a(Lcom/facebook/user/model/Name;)V

    .line 1372271
    :cond_2
    if-eqz v4, :cond_3

    .line 1372272
    invoke-virtual {v6, v4}, LX/3hQ;->a(LX/0Px;)V

    .line 1372273
    :cond_3
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object p1

    .line 1372274
    invoke-virtual {p1, p0}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 1372275
    iget-object p0, v6, LX/3hQ;->f:Ljava/util/Set;

    move-object v6, p0

    .line 1372276
    invoke-virtual {p1, v6}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 1372277
    invoke-virtual {p1}, LX/0cA;->b()LX/0Rf;

    move-result-object v6

    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    goto :goto_0
.end method

.method public final a(Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;)LX/0Px;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNormalizedTokens"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLInterfaces$AddEntityFragment$$Searchable$;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1372278
    invoke-virtual {p1}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->n()LX/0Px;

    move-result-object v0

    .line 1372279
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1372280
    if-eqz v0, :cond_0

    .line 1372281
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1372282
    iget-object p1, p0, LX/8bj;->d:LX/2Rd;

    invoke-virtual {p1, v1}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1372283
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1372284
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 1372285
    return-object v0
.end method
