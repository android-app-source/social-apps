.class public final LX/7QW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/7Qb;


# direct methods
.method public constructor <init>(LX/7Qb;)V
    .locals 0

    .prologue
    .line 1204480
    iput-object p1, p0, LX/7QW;->a:LX/7Qb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, -0x48efd820

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1204481
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 1204482
    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1204483
    iget-object v1, p0, LX/7QW;->a:LX/7Qb;

    .line 1204484
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/7Qb;->k:Z

    .line 1204485
    iget-object v2, v1, LX/7Qb;->l:LX/0Yb;

    if-eqz v2, :cond_0

    .line 1204486
    iget-object v2, v1, LX/7Qb;->l:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->c()V

    .line 1204487
    const/4 v2, 0x0

    iput-object v2, v1, LX/7Qb;->l:LX/0Yb;

    .line 1204488
    :cond_0
    iget-object v2, v1, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1204489
    new-instance p0, Ljava/util/HashMap;

    iget-object v2, v1, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {p0, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 1204490
    iget-object v2, v1, LX/7Qb;->g:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7Qa;

    .line 1204491
    iget-object p2, v2, LX/7Qa;->e:LX/0gM;

    if-eqz p2, :cond_1

    .line 1204492
    iget-object p2, v2, LX/7Qa;->b:Ljava/lang/String;

    iget-object p3, v2, LX/7Qa;->e:LX/0gM;

    invoke-interface {p0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1204493
    const/4 p2, 0x0

    .line 1204494
    iput-object p2, v2, LX/7Qa;->e:LX/0gM;

    .line 1204495
    goto :goto_0

    .line 1204496
    :cond_2
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    .line 1204497
    iget-object v2, v1, LX/7Qb;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2nv;

    .line 1204498
    iget-object p1, v2, LX/2nv;->b:LX/0gX;

    new-instance p2, Ljava/util/HashSet;

    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p3

    invoke-direct {p2, p3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, p2}, LX/0gX;->a(Ljava/util/Set;)V

    .line 1204499
    :cond_3
    :goto_1
    const v1, -0x4f682b34

    invoke-static {v1, v0}, LX/02F;->e(II)V

    return-void

    .line 1204500
    :cond_4
    const-string v2, "com.facebook.common.appstate.AppStateManager.USER_ENTERED_APP"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1204501
    iget-object v1, p0, LX/7QW;->a:LX/7Qb;

    .line 1204502
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/7Qb;->k:Z

    .line 1204503
    invoke-static {v1}, LX/7Qb;->f(LX/7Qb;)V

    .line 1204504
    iget-object v2, v1, LX/7Qb;->j:Landroid/os/Handler;

    new-instance p0, Lcom/facebook/video/videohome/liveupdates/BroadcastStatusUpdateManager$2;

    invoke-direct {p0, v1}, Lcom/facebook/video/videohome/liveupdates/BroadcastStatusUpdateManager$2;-><init>(LX/7Qb;)V

    const p1, -0x7a37924f

    invoke-static {v2, p0, p1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1204505
    goto :goto_1
.end method
