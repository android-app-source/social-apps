.class public final enum LX/6g5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6g5;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6g5;

.field public static final enum NONE:LX/6g5;

.field public static final enum VIDEO_CALL:LX/6g5;

.field public static final enum VOICE_CALL:LX/6g5;


# instance fields
.field private mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1120952
    new-instance v0, LX/6g5;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2, v2}, LX/6g5;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6g5;->NONE:LX/6g5;

    .line 1120953
    new-instance v0, LX/6g5;

    const-string v1, "VOICE_CALL"

    invoke-direct {v0, v1, v3, v3}, LX/6g5;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6g5;->VOICE_CALL:LX/6g5;

    .line 1120954
    new-instance v0, LX/6g5;

    const-string v1, "VIDEO_CALL"

    invoke-direct {v0, v1, v4, v4}, LX/6g5;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6g5;->VIDEO_CALL:LX/6g5;

    .line 1120955
    const/4 v0, 0x3

    new-array v0, v0, [LX/6g5;

    sget-object v1, LX/6g5;->NONE:LX/6g5;

    aput-object v1, v0, v2

    sget-object v1, LX/6g5;->VOICE_CALL:LX/6g5;

    aput-object v1, v0, v3

    sget-object v1, LX/6g5;->VIDEO_CALL:LX/6g5;

    aput-object v1, v0, v4

    sput-object v0, LX/6g5;->$VALUES:[LX/6g5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1120941
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1120942
    iput p3, p0, LX/6g5;->mValue:I

    .line 1120943
    return-void
.end method

.method public static fromValue(I)LX/6g5;
    .locals 5

    .prologue
    .line 1120947
    invoke-static {}, LX/6g5;->values()[LX/6g5;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1120948
    invoke-virtual {v0}, LX/6g5;->getValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 1120949
    :goto_1
    return-object v0

    .line 1120950
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1120951
    :cond_1
    sget-object v0, LX/6g5;->NONE:LX/6g5;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/6g5;
    .locals 1

    .prologue
    .line 1120946
    const-class v0, LX/6g5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6g5;

    return-object v0
.end method

.method public static values()[LX/6g5;
    .locals 1

    .prologue
    .line 1120945
    sget-object v0, LX/6g5;->$VALUES:[LX/6g5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6g5;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 1120944
    iget v0, p0, LX/6g5;->mValue:I

    return v0
.end method
