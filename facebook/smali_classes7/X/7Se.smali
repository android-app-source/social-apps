.class public LX/7Se;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/61C;


# instance fields
.field public a:LX/5Pc;

.field private b:LX/5Pb;

.field private c:LX/5Pb;

.field private d:LX/5PR;

.field public e:LX/5Pf;

.field private f:Z

.field public g:LX/61D;

.field private final h:[F

.field public final i:LX/5Pg;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v4, 0x2

    .line 1209067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1209068
    const/4 v0, 0x0

    iput-object v0, p0, LX/7Se;->e:LX/5Pf;

    .line 1209069
    new-instance v0, LX/5Pg;

    new-array v1, v3, [F

    fill-array-data v1, :array_0

    invoke-direct {v0, v1, v4}, LX/5Pg;-><init>([FI)V

    iput-object v0, p0, LX/7Se;->i:LX/5Pg;

    .line 1209070
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7Se;->f:Z

    .line 1209071
    sget-object v0, LX/61D;->DEFAULT:LX/61D;

    iput-object v0, p0, LX/7Se;->g:LX/61D;

    .line 1209072
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LX/7Se;->h:[F

    .line 1209073
    iget-object v0, p0, LX/7Se;->h:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1209074
    new-instance v0, LX/5PQ;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/5PQ;-><init>(I)V

    const/4 v1, 0x5

    .line 1209075
    iput v1, v0, LX/5PQ;->a:I

    .line 1209076
    move-object v0, v0

    .line 1209077
    const-string v1, "aPosition"

    iget-object v2, p0, LX/7Se;->i:LX/5Pg;

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    const-string v1, "aTextureCoord"

    new-instance v2, LX/5Pg;

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    invoke-direct {v2, v3, v4}, LX/5Pg;-><init>([FI)V

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    invoke-virtual {v0}, LX/5PQ;->a()LX/5PR;

    move-result-object v0

    iput-object v0, p0, LX/7Se;->d:LX/5PR;

    .line 1209078
    return-void

    nop

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 1209079
    :array_1
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private b([F)[F
    .locals 0

    .prologue
    .line 1209066
    if-eqz p1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    iget-object p1, p0, LX/7Se;->h:[F

    goto :goto_0
.end method

.method private d()LX/5Pa;
    .locals 4

    .prologue
    .line 1209055
    iget-object v0, p0, LX/7Se;->a:LX/5Pc;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Called without a program factory"

    invoke-static {v0, v1}, LX/64O;->b(ZLjava/lang/Object;)V

    .line 1209056
    sget-object v0, LX/7Sd;->a:[I

    iget-object v1, p0, LX/7Se;->g:LX/61D;

    invoke-virtual {v1}, LX/61D;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1209057
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown format override "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/7Se;->g:LX/61D;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1209058
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1209059
    :pswitch_0
    iget-object v0, p0, LX/7Se;->b:LX/5Pb;

    if-nez v0, :cond_1

    .line 1209060
    iget-object v0, p0, LX/7Se;->a:LX/5Pc;

    const v1, 0x7f070017

    const v2, 0x7f070016

    iget-boolean v3, p0, LX/7Se;->f:Z

    invoke-interface {v0, v1, v2, v3}, LX/5Pc;->a(IIZ)LX/5Pb;

    move-result-object v0

    iput-object v0, p0, LX/7Se;->b:LX/5Pb;

    .line 1209061
    :cond_1
    iget-object v0, p0, LX/7Se;->b:LX/5Pb;

    .line 1209062
    :goto_1
    invoke-virtual {v0}, LX/5Pb;->a()LX/5Pa;

    move-result-object v0

    return-object v0

    .line 1209063
    :pswitch_1
    iget-object v0, p0, LX/7Se;->c:LX/5Pb;

    if-nez v0, :cond_2

    .line 1209064
    iget-object v0, p0, LX/7Se;->a:LX/5Pc;

    const v1, 0x7f070017

    const v2, 0x7f070015

    iget-boolean v3, p0, LX/7Se;->f:Z

    invoke-interface {v0, v1, v2, v3}, LX/5Pc;->a(IIZ)LX/5Pb;

    move-result-object v0

    iput-object v0, p0, LX/7Se;->c:LX/5Pb;

    .line 1209065
    :cond_2
    iget-object v0, p0, LX/7Se;->c:LX/5Pb;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .prologue
    .line 1209054
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1209080
    iput-object p1, p0, LX/7Se;->a:LX/5Pc;

    .line 1209081
    iput-object v0, p0, LX/7Se;->c:LX/5Pb;

    .line 1209082
    iput-object v0, p0, LX/7Se;->b:LX/5Pb;

    .line 1209083
    return-void
.end method

.method public final a(ZLX/5Pc;)V
    .locals 0

    .prologue
    .line 1209050
    iput-boolean p1, p0, LX/7Se;->f:Z

    .line 1209051
    if-eqz p2, :cond_0

    .line 1209052
    invoke-virtual {p0, p2}, LX/7Se;->a(LX/5Pc;)V

    .line 1209053
    :cond_0
    return-void
.end method

.method public final a([F[F[FJ)V
    .locals 4

    .prologue
    .line 1209042
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 1209043
    invoke-direct {p0}, LX/7Se;->d()LX/5Pa;

    move-result-object v0

    .line 1209044
    const-string v1, "uSurfaceTransformMatrix"

    invoke-direct {p0, p1}, LX/7Se;->b([F)[F

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v1

    const-string v2, "uVideoTransformMatrix"

    invoke-direct {p0, p2}, LX/7Se;->b([F)[F

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v1

    const-string v2, "uSceneTransformMatrix"

    invoke-direct {p0, p3}, LX/7Se;->b([F)[F

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    .line 1209045
    iget-object v1, p0, LX/7Se;->e:LX/5Pf;

    if-eqz v1, :cond_0

    .line 1209046
    const-string v1, "sTexture"

    iget-object v2, p0, LX/7Se;->e:LX/5Pf;

    invoke-virtual {v0, v1, v2}, LX/5Pa;->a(Ljava/lang/String;LX/5Pf;)LX/5Pa;

    .line 1209047
    :cond_0
    iget-object v1, p0, LX/7Se;->d:LX/5PR;

    invoke-virtual {v0, v1}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    .line 1209048
    const-string v0, "copyRenderer::onDrawFrame"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1209049
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1209041
    iget-boolean v0, p0, LX/7Se;->f:Z

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1209036
    iput-object v0, p0, LX/7Se;->e:LX/5Pf;

    .line 1209037
    iput-object v0, p0, LX/7Se;->a:LX/5Pc;

    .line 1209038
    iput-object v0, p0, LX/7Se;->c:LX/5Pb;

    .line 1209039
    iput-object v0, p0, LX/7Se;->b:LX/5Pb;

    .line 1209040
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1209035
    const/4 v0, 0x1

    return v0
.end method
