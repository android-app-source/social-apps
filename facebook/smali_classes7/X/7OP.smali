.class public final LX/7OP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z

.field public d:Z

.field public e:Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1201404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 1201405
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1201406
    iget-object v1, p0, LX/7OP;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1201407
    iget-object v3, p0, LX/7OP;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1201408
    iget-object v5, p0, LX/7OP;->e:Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel$VideoChannelCuratorProfileModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1201409
    iget-object v6, p0, LX/7OP;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1201410
    iget-object v7, p0, LX/7OP;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1201411
    const/16 v8, 0x9

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1201412
    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 1201413
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1201414
    const/4 v1, 0x2

    iget-boolean v3, p0, LX/7OP;->c:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1201415
    const/4 v1, 0x3

    iget-boolean v3, p0, LX/7OP;->d:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1201416
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1201417
    const/4 v1, 0x5

    iget-boolean v3, p0, LX/7OP;->f:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1201418
    const/4 v1, 0x6

    iget-boolean v3, p0, LX/7OP;->g:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1201419
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1201420
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1201421
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1201422
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1201423
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1201424
    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1201425
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1201426
    new-instance v1, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;-><init>(LX/15i;)V

    .line 1201427
    return-object v1
.end method
