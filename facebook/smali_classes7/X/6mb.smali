.class public LX/6mb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;

.field private static final k:LX/1sw;

.field private static final l:LX/1sw;

.field private static final m:LX/1sw;

.field private static final n:LX/1sw;

.field private static final o:LX/1sw;

.field private static final p:LX/1sw;

.field private static final q:LX/1sw;

.field private static final r:LX/1sw;

.field private static final s:LX/1sw;

.field private static final t:LX/1sw;

.field private static final u:LX/1sw;

.field private static final v:LX/1sw;


# instance fields
.field public final androidKeyHash:Ljava/lang/String;

.field public final attributionAppId:Ljava/lang/Long;

.field public final body:Ljava/lang/String;

.field public final broadcastRecipients:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final clientTags:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final coordinates:LX/6mM;

.field public final copyAttachmentId:Ljava/lang/String;

.field public final copyMessageId:Ljava/lang/String;

.field public final fbTraceMeta:Ljava/lang/String;

.field public final genericMetadata:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final imageType:Ljava/lang/Integer;

.field public final iosBundleId:Ljava/lang/String;

.field public final locationAttachment:LX/6mT;

.field public final mediaAttachmentIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final objectAttachment:Ljava/lang/String;

.field public final offlineThreadingId:Ljava/lang/Long;

.field public final refCode:Ljava/lang/Integer;

.field public final senderFbid:Ljava/lang/Long;

.field public final to:Ljava/lang/String;

.field public final ttl:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0xc

    const/16 v7, 0xd

    const/16 v6, 0xa

    const/16 v5, 0x8

    const/16 v4, 0xb

    .line 1145717
    new-instance v0, LX/1sv;

    const-string v1, "SendMessageRequest"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6mb;->b:LX/1sv;

    .line 1145718
    new-instance v0, LX/1sw;

    const-string v1, "to"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->c:LX/1sw;

    .line 1145719
    new-instance v0, LX/1sw;

    const-string v1, "body"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->d:LX/1sw;

    .line 1145720
    new-instance v0, LX/1sw;

    const-string v1, "offlineThreadingId"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->e:LX/1sw;

    .line 1145721
    new-instance v0, LX/1sw;

    const-string v1, "coordinates"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v8, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->f:LX/1sw;

    .line 1145722
    new-instance v0, LX/1sw;

    const-string v1, "clientTags"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v7, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->g:LX/1sw;

    .line 1145723
    new-instance v0, LX/1sw;

    const-string v1, "objectAttachment"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->h:LX/1sw;

    .line 1145724
    new-instance v0, LX/1sw;

    const-string v1, "copyMessageId"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->i:LX/1sw;

    .line 1145725
    new-instance v0, LX/1sw;

    const-string v1, "copyAttachmentId"

    invoke-direct {v0, v1, v4, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->j:LX/1sw;

    .line 1145726
    new-instance v0, LX/1sw;

    const-string v1, "mediaAttachmentIds"

    const/16 v2, 0xf

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->k:LX/1sw;

    .line 1145727
    new-instance v0, LX/1sw;

    const-string v1, "fbTraceMeta"

    invoke-direct {v0, v1, v4, v6}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->l:LX/1sw;

    .line 1145728
    new-instance v0, LX/1sw;

    const-string v1, "imageType"

    invoke-direct {v0, v1, v5, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->m:LX/1sw;

    .line 1145729
    new-instance v0, LX/1sw;

    const-string v1, "senderFbid"

    invoke-direct {v0, v1, v6, v8}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->n:LX/1sw;

    .line 1145730
    new-instance v0, LX/1sw;

    const-string v1, "broadcastRecipients"

    invoke-direct {v0, v1, v7, v7}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->o:LX/1sw;

    .line 1145731
    new-instance v0, LX/1sw;

    const-string v1, "attributionAppId"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v6, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->p:LX/1sw;

    .line 1145732
    new-instance v0, LX/1sw;

    const-string v1, "iosBundleId"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->q:LX/1sw;

    .line 1145733
    new-instance v0, LX/1sw;

    const-string v1, "androidKeyHash"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->r:LX/1sw;

    .line 1145734
    new-instance v0, LX/1sw;

    const-string v1, "locationAttachment"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v8, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->s:LX/1sw;

    .line 1145735
    new-instance v0, LX/1sw;

    const-string v1, "ttl"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->t:LX/1sw;

    .line 1145736
    new-instance v0, LX/1sw;

    const-string v1, "refCode"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->u:LX/1sw;

    .line 1145737
    new-instance v0, LX/1sw;

    const-string v1, "genericMetadata"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v7, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6mb;->v:LX/1sw;

    .line 1145738
    const/4 v0, 0x1

    sput-boolean v0, LX/6mb;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;LX/6mM;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Ljava/util/Map;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;LX/6mT;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "LX/6mM;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/6mT;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1145695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1145696
    iput-object p1, p0, LX/6mb;->to:Ljava/lang/String;

    .line 1145697
    iput-object p2, p0, LX/6mb;->body:Ljava/lang/String;

    .line 1145698
    iput-object p3, p0, LX/6mb;->offlineThreadingId:Ljava/lang/Long;

    .line 1145699
    iput-object p4, p0, LX/6mb;->coordinates:LX/6mM;

    .line 1145700
    iput-object p5, p0, LX/6mb;->clientTags:Ljava/util/Map;

    .line 1145701
    iput-object p6, p0, LX/6mb;->objectAttachment:Ljava/lang/String;

    .line 1145702
    iput-object p7, p0, LX/6mb;->copyMessageId:Ljava/lang/String;

    .line 1145703
    iput-object p8, p0, LX/6mb;->copyAttachmentId:Ljava/lang/String;

    .line 1145704
    iput-object p9, p0, LX/6mb;->mediaAttachmentIds:Ljava/util/List;

    .line 1145705
    iput-object p10, p0, LX/6mb;->fbTraceMeta:Ljava/lang/String;

    .line 1145706
    iput-object p11, p0, LX/6mb;->imageType:Ljava/lang/Integer;

    .line 1145707
    iput-object p12, p0, LX/6mb;->senderFbid:Ljava/lang/Long;

    .line 1145708
    iput-object p13, p0, LX/6mb;->broadcastRecipients:Ljava/util/Map;

    .line 1145709
    iput-object p14, p0, LX/6mb;->attributionAppId:Ljava/lang/Long;

    .line 1145710
    move-object/from16 v0, p15

    iput-object v0, p0, LX/6mb;->iosBundleId:Ljava/lang/String;

    .line 1145711
    move-object/from16 v0, p16

    iput-object v0, p0, LX/6mb;->androidKeyHash:Ljava/lang/String;

    .line 1145712
    move-object/from16 v0, p17

    iput-object v0, p0, LX/6mb;->locationAttachment:LX/6mT;

    .line 1145713
    move-object/from16 v0, p18

    iput-object v0, p0, LX/6mb;->ttl:Ljava/lang/Integer;

    .line 1145714
    move-object/from16 v0, p19

    iput-object v0, p0, LX/6mb;->refCode:Ljava/lang/Integer;

    .line 1145715
    move-object/from16 v0, p20

    iput-object v0, p0, LX/6mb;->genericMetadata:Ljava/util/Map;

    .line 1145716
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1145692
    iget-object v0, p0, LX/6mb;->imageType:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, LX/6mR;->a:LX/1sn;

    iget-object v1, p0, LX/6mb;->imageType:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1145693
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'imageType\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6mb;->imageType:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1145694
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1145493
    if-eqz p2, :cond_27

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1145494
    :goto_0
    if-eqz p2, :cond_28

    const-string v0, "\n"

    move-object v3, v0

    .line 1145495
    :goto_1
    if-eqz p2, :cond_29

    const-string v0, " "

    move-object v1, v0

    .line 1145496
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "SendMessageRequest"

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1145497
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145498
    const-string v0, "("

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145499
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145500
    const/4 v0, 0x1

    .line 1145501
    iget-object v6, p0, LX/6mb;->to:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 1145502
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145503
    const-string v0, "to"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145504
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145505
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145506
    iget-object v0, p0, LX/6mb;->to:Ljava/lang/String;

    if-nez v0, :cond_2a

    .line 1145507
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v0, v2

    .line 1145508
    :cond_0
    iget-object v6, p0, LX/6mb;->body:Ljava/lang/String;

    if-eqz v6, :cond_2

    .line 1145509
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145510
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145511
    const-string v0, "body"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145512
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145513
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145514
    iget-object v0, p0, LX/6mb;->body:Ljava/lang/String;

    if-nez v0, :cond_2b

    .line 1145515
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v0, v2

    .line 1145516
    :cond_2
    iget-object v6, p0, LX/6mb;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v6, :cond_4

    .line 1145517
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145518
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145519
    const-string v0, "offlineThreadingId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145520
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145521
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145522
    iget-object v0, p0, LX/6mb;->offlineThreadingId:Ljava/lang/Long;

    if-nez v0, :cond_2c

    .line 1145523
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v0, v2

    .line 1145524
    :cond_4
    iget-object v6, p0, LX/6mb;->coordinates:LX/6mM;

    if-eqz v6, :cond_6

    .line 1145525
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145526
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145527
    const-string v0, "coordinates"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145528
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145529
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145530
    iget-object v0, p0, LX/6mb;->coordinates:LX/6mM;

    if-nez v0, :cond_2d

    .line 1145531
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    move v0, v2

    .line 1145532
    :cond_6
    iget-object v6, p0, LX/6mb;->clientTags:Ljava/util/Map;

    if-eqz v6, :cond_8

    .line 1145533
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145534
    :cond_7
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145535
    const-string v0, "clientTags"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145536
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145537
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145538
    iget-object v0, p0, LX/6mb;->clientTags:Ljava/util/Map;

    if-nez v0, :cond_2e

    .line 1145539
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_7
    move v0, v2

    .line 1145540
    :cond_8
    iget-object v6, p0, LX/6mb;->objectAttachment:Ljava/lang/String;

    if-eqz v6, :cond_a

    .line 1145541
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145542
    :cond_9
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145543
    const-string v0, "objectAttachment"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145544
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145545
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145546
    iget-object v0, p0, LX/6mb;->objectAttachment:Ljava/lang/String;

    if-nez v0, :cond_2f

    .line 1145547
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_8
    move v0, v2

    .line 1145548
    :cond_a
    iget-object v6, p0, LX/6mb;->copyMessageId:Ljava/lang/String;

    if-eqz v6, :cond_c

    .line 1145549
    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145550
    :cond_b
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145551
    const-string v0, "copyMessageId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145552
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145553
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145554
    iget-object v0, p0, LX/6mb;->copyMessageId:Ljava/lang/String;

    if-nez v0, :cond_30

    .line 1145555
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_9
    move v0, v2

    .line 1145556
    :cond_c
    iget-object v6, p0, LX/6mb;->copyAttachmentId:Ljava/lang/String;

    if-eqz v6, :cond_e

    .line 1145557
    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145558
    :cond_d
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145559
    const-string v0, "copyAttachmentId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145560
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145561
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145562
    iget-object v0, p0, LX/6mb;->copyAttachmentId:Ljava/lang/String;

    if-nez v0, :cond_31

    .line 1145563
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_a
    move v0, v2

    .line 1145564
    :cond_e
    iget-object v6, p0, LX/6mb;->mediaAttachmentIds:Ljava/util/List;

    if-eqz v6, :cond_10

    .line 1145565
    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145566
    :cond_f
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145567
    const-string v0, "mediaAttachmentIds"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145568
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145569
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145570
    iget-object v0, p0, LX/6mb;->mediaAttachmentIds:Ljava/util/List;

    if-nez v0, :cond_32

    .line 1145571
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_b
    move v0, v2

    .line 1145572
    :cond_10
    iget-object v6, p0, LX/6mb;->fbTraceMeta:Ljava/lang/String;

    if-eqz v6, :cond_12

    .line 1145573
    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145574
    :cond_11
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145575
    const-string v0, "fbTraceMeta"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145576
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145577
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145578
    iget-object v0, p0, LX/6mb;->fbTraceMeta:Ljava/lang/String;

    if-nez v0, :cond_33

    .line 1145579
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_c
    move v0, v2

    .line 1145580
    :cond_12
    iget-object v6, p0, LX/6mb;->imageType:Ljava/lang/Integer;

    if-eqz v6, :cond_15

    .line 1145581
    if-nez v0, :cond_13

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145582
    :cond_13
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145583
    const-string v0, "imageType"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145584
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145585
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145586
    iget-object v0, p0, LX/6mb;->imageType:Ljava/lang/Integer;

    if-nez v0, :cond_34

    .line 1145587
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_14
    :goto_d
    move v0, v2

    .line 1145588
    :cond_15
    iget-object v6, p0, LX/6mb;->senderFbid:Ljava/lang/Long;

    if-eqz v6, :cond_17

    .line 1145589
    if-nez v0, :cond_16

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145590
    :cond_16
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145591
    const-string v0, "senderFbid"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145592
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145593
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145594
    iget-object v0, p0, LX/6mb;->senderFbid:Ljava/lang/Long;

    if-nez v0, :cond_36

    .line 1145595
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_e
    move v0, v2

    .line 1145596
    :cond_17
    iget-object v6, p0, LX/6mb;->broadcastRecipients:Ljava/util/Map;

    if-eqz v6, :cond_19

    .line 1145597
    if-nez v0, :cond_18

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145598
    :cond_18
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145599
    const-string v0, "broadcastRecipients"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145600
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145601
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145602
    iget-object v0, p0, LX/6mb;->broadcastRecipients:Ljava/util/Map;

    if-nez v0, :cond_37

    .line 1145603
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_f
    move v0, v2

    .line 1145604
    :cond_19
    iget-object v6, p0, LX/6mb;->attributionAppId:Ljava/lang/Long;

    if-eqz v6, :cond_1b

    .line 1145605
    if-nez v0, :cond_1a

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145606
    :cond_1a
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145607
    const-string v0, "attributionAppId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145608
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145609
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145610
    iget-object v0, p0, LX/6mb;->attributionAppId:Ljava/lang/Long;

    if-nez v0, :cond_38

    .line 1145611
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_10
    move v0, v2

    .line 1145612
    :cond_1b
    iget-object v6, p0, LX/6mb;->iosBundleId:Ljava/lang/String;

    if-eqz v6, :cond_1d

    .line 1145613
    if-nez v0, :cond_1c

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145614
    :cond_1c
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145615
    const-string v0, "iosBundleId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145616
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145617
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145618
    iget-object v0, p0, LX/6mb;->iosBundleId:Ljava/lang/String;

    if-nez v0, :cond_39

    .line 1145619
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_11
    move v0, v2

    .line 1145620
    :cond_1d
    iget-object v6, p0, LX/6mb;->androidKeyHash:Ljava/lang/String;

    if-eqz v6, :cond_1f

    .line 1145621
    if-nez v0, :cond_1e

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145622
    :cond_1e
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145623
    const-string v0, "androidKeyHash"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145624
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145625
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145626
    iget-object v0, p0, LX/6mb;->androidKeyHash:Ljava/lang/String;

    if-nez v0, :cond_3a

    .line 1145627
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_12
    move v0, v2

    .line 1145628
    :cond_1f
    iget-object v6, p0, LX/6mb;->locationAttachment:LX/6mT;

    if-eqz v6, :cond_21

    .line 1145629
    if-nez v0, :cond_20

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145630
    :cond_20
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145631
    const-string v0, "locationAttachment"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145632
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145633
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145634
    iget-object v0, p0, LX/6mb;->locationAttachment:LX/6mT;

    if-nez v0, :cond_3b

    .line 1145635
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_13
    move v0, v2

    .line 1145636
    :cond_21
    iget-object v6, p0, LX/6mb;->ttl:Ljava/lang/Integer;

    if-eqz v6, :cond_23

    .line 1145637
    if-nez v0, :cond_22

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145638
    :cond_22
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145639
    const-string v0, "ttl"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145640
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145641
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145642
    iget-object v0, p0, LX/6mb;->ttl:Ljava/lang/Integer;

    if-nez v0, :cond_3c

    .line 1145643
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_14
    move v0, v2

    .line 1145644
    :cond_23
    iget-object v6, p0, LX/6mb;->refCode:Ljava/lang/Integer;

    if-eqz v6, :cond_3f

    .line 1145645
    if-nez v0, :cond_24

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145646
    :cond_24
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145647
    const-string v0, "refCode"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145648
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145649
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145650
    iget-object v0, p0, LX/6mb;->refCode:Ljava/lang/Integer;

    if-nez v0, :cond_3d

    .line 1145651
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145652
    :goto_15
    iget-object v0, p0, LX/6mb;->genericMetadata:Ljava/util/Map;

    if-eqz v0, :cond_26

    .line 1145653
    if-nez v2, :cond_25

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145654
    :cond_25
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145655
    const-string v0, "genericMetadata"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145656
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145657
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145658
    iget-object v0, p0, LX/6mb;->genericMetadata:Ljava/util/Map;

    if-nez v0, :cond_3e

    .line 1145659
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145660
    :cond_26
    :goto_16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145661
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145662
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1145663
    :cond_27
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 1145664
    :cond_28
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 1145665
    :cond_29
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 1145666
    :cond_2a
    iget-object v0, p0, LX/6mb;->to:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1145667
    :cond_2b
    iget-object v0, p0, LX/6mb;->body:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1145668
    :cond_2c
    iget-object v0, p0, LX/6mb;->offlineThreadingId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1145669
    :cond_2d
    iget-object v0, p0, LX/6mb;->coordinates:LX/6mM;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1145670
    :cond_2e
    iget-object v0, p0, LX/6mb;->clientTags:Ljava/util/Map;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1145671
    :cond_2f
    iget-object v0, p0, LX/6mb;->objectAttachment:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1145672
    :cond_30
    iget-object v0, p0, LX/6mb;->copyMessageId:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 1145673
    :cond_31
    iget-object v0, p0, LX/6mb;->copyAttachmentId:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 1145674
    :cond_32
    iget-object v0, p0, LX/6mb;->mediaAttachmentIds:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    .line 1145675
    :cond_33
    iget-object v0, p0, LX/6mb;->fbTraceMeta:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    .line 1145676
    :cond_34
    sget-object v0, LX/6mR;->b:Ljava/util/Map;

    iget-object v6, p0, LX/6mb;->imageType:Ljava/lang/Integer;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1145677
    if-eqz v0, :cond_35

    .line 1145678
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145679
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1145680
    :cond_35
    iget-object v6, p0, LX/6mb;->imageType:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1145681
    if-eqz v0, :cond_14

    .line 1145682
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_d

    .line 1145683
    :cond_36
    iget-object v0, p0, LX/6mb;->senderFbid:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_e

    .line 1145684
    :cond_37
    iget-object v0, p0, LX/6mb;->broadcastRecipients:Ljava/util/Map;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_f

    .line 1145685
    :cond_38
    iget-object v0, p0, LX/6mb;->attributionAppId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_10

    .line 1145686
    :cond_39
    iget-object v0, p0, LX/6mb;->iosBundleId:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_11

    .line 1145687
    :cond_3a
    iget-object v0, p0, LX/6mb;->androidKeyHash:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_12

    .line 1145688
    :cond_3b
    iget-object v0, p0, LX/6mb;->locationAttachment:LX/6mT;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_13

    .line 1145689
    :cond_3c
    iget-object v0, p0, LX/6mb;->ttl:Ljava/lang/Integer;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_14

    .line 1145690
    :cond_3d
    iget-object v0, p0, LX/6mb;->refCode:Ljava/lang/Integer;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_15

    .line 1145691
    :cond_3e
    iget-object v0, p0, LX/6mb;->genericMetadata:Ljava/util/Map;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_16

    :cond_3f
    move v2, v0

    goto/16 :goto_15
.end method

.method public final a(LX/1su;)V
    .locals 4

    .prologue
    const/16 v3, 0xb

    .line 1145245
    invoke-direct {p0}, LX/6mb;->a()V

    .line 1145246
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1145247
    iget-object v0, p0, LX/6mb;->to:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1145248
    iget-object v0, p0, LX/6mb;->to:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1145249
    sget-object v0, LX/6mb;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145250
    iget-object v0, p0, LX/6mb;->to:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1145251
    :cond_0
    iget-object v0, p0, LX/6mb;->body:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1145252
    iget-object v0, p0, LX/6mb;->body:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1145253
    sget-object v0, LX/6mb;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145254
    iget-object v0, p0, LX/6mb;->body:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1145255
    :cond_1
    iget-object v0, p0, LX/6mb;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1145256
    iget-object v0, p0, LX/6mb;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1145257
    sget-object v0, LX/6mb;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145258
    iget-object v0, p0, LX/6mb;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1145259
    :cond_2
    iget-object v0, p0, LX/6mb;->coordinates:LX/6mM;

    if-eqz v0, :cond_3

    .line 1145260
    iget-object v0, p0, LX/6mb;->coordinates:LX/6mM;

    if-eqz v0, :cond_3

    .line 1145261
    sget-object v0, LX/6mb;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145262
    iget-object v0, p0, LX/6mb;->coordinates:LX/6mM;

    invoke-virtual {v0, p1}, LX/6mM;->a(LX/1su;)V

    .line 1145263
    :cond_3
    iget-object v0, p0, LX/6mb;->clientTags:Ljava/util/Map;

    if-eqz v0, :cond_4

    .line 1145264
    iget-object v0, p0, LX/6mb;->clientTags:Ljava/util/Map;

    if-eqz v0, :cond_4

    .line 1145265
    sget-object v0, LX/6mb;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145266
    new-instance v0, LX/7H3;

    iget-object v1, p0, LX/6mb;->clientTags:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v3, v3, v1}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 1145267
    iget-object v0, p0, LX/6mb;->clientTags:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1145268
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, LX/1su;->a(Ljava/lang/String;)V

    .line 1145269
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1145270
    :cond_4
    iget-object v0, p0, LX/6mb;->objectAttachment:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1145271
    iget-object v0, p0, LX/6mb;->objectAttachment:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1145272
    sget-object v0, LX/6mb;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145273
    iget-object v0, p0, LX/6mb;->objectAttachment:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1145274
    :cond_5
    iget-object v0, p0, LX/6mb;->copyMessageId:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1145275
    iget-object v0, p0, LX/6mb;->copyMessageId:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1145276
    sget-object v0, LX/6mb;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145277
    iget-object v0, p0, LX/6mb;->copyMessageId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1145278
    :cond_6
    iget-object v0, p0, LX/6mb;->copyAttachmentId:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1145279
    iget-object v0, p0, LX/6mb;->copyAttachmentId:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1145280
    sget-object v0, LX/6mb;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145281
    iget-object v0, p0, LX/6mb;->copyAttachmentId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1145282
    :cond_7
    iget-object v0, p0, LX/6mb;->mediaAttachmentIds:Ljava/util/List;

    if-eqz v0, :cond_8

    .line 1145283
    iget-object v0, p0, LX/6mb;->mediaAttachmentIds:Ljava/util/List;

    if-eqz v0, :cond_8

    .line 1145284
    sget-object v0, LX/6mb;->k:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145285
    new-instance v0, LX/1u3;

    iget-object v1, p0, LX/6mb;->mediaAttachmentIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v3, v1}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1145286
    iget-object v0, p0, LX/6mb;->mediaAttachmentIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1145287
    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 1145288
    :cond_8
    iget-object v0, p0, LX/6mb;->fbTraceMeta:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1145289
    iget-object v0, p0, LX/6mb;->fbTraceMeta:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1145290
    sget-object v0, LX/6mb;->l:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145291
    iget-object v0, p0, LX/6mb;->fbTraceMeta:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1145292
    :cond_9
    iget-object v0, p0, LX/6mb;->imageType:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 1145293
    iget-object v0, p0, LX/6mb;->imageType:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 1145294
    sget-object v0, LX/6mb;->m:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145295
    iget-object v0, p0, LX/6mb;->imageType:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1145296
    :cond_a
    iget-object v0, p0, LX/6mb;->senderFbid:Ljava/lang/Long;

    if-eqz v0, :cond_b

    .line 1145297
    iget-object v0, p0, LX/6mb;->senderFbid:Ljava/lang/Long;

    if-eqz v0, :cond_b

    .line 1145298
    sget-object v0, LX/6mb;->n:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145299
    iget-object v0, p0, LX/6mb;->senderFbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1145300
    :cond_b
    iget-object v0, p0, LX/6mb;->broadcastRecipients:Ljava/util/Map;

    if-eqz v0, :cond_c

    .line 1145301
    iget-object v0, p0, LX/6mb;->broadcastRecipients:Ljava/util/Map;

    if-eqz v0, :cond_c

    .line 1145302
    sget-object v0, LX/6mb;->o:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145303
    new-instance v0, LX/7H3;

    iget-object v1, p0, LX/6mb;->broadcastRecipients:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v3, v3, v1}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 1145304
    iget-object v0, p0, LX/6mb;->broadcastRecipients:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1145305
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, LX/1su;->a(Ljava/lang/String;)V

    .line 1145306
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 1145307
    :cond_c
    iget-object v0, p0, LX/6mb;->attributionAppId:Ljava/lang/Long;

    if-eqz v0, :cond_d

    .line 1145308
    iget-object v0, p0, LX/6mb;->attributionAppId:Ljava/lang/Long;

    if-eqz v0, :cond_d

    .line 1145309
    sget-object v0, LX/6mb;->p:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145310
    iget-object v0, p0, LX/6mb;->attributionAppId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1145311
    :cond_d
    iget-object v0, p0, LX/6mb;->iosBundleId:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 1145312
    iget-object v0, p0, LX/6mb;->iosBundleId:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 1145313
    sget-object v0, LX/6mb;->q:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145314
    iget-object v0, p0, LX/6mb;->iosBundleId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1145315
    :cond_e
    iget-object v0, p0, LX/6mb;->androidKeyHash:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 1145316
    iget-object v0, p0, LX/6mb;->androidKeyHash:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 1145317
    sget-object v0, LX/6mb;->r:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145318
    iget-object v0, p0, LX/6mb;->androidKeyHash:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1145319
    :cond_f
    iget-object v0, p0, LX/6mb;->locationAttachment:LX/6mT;

    if-eqz v0, :cond_10

    .line 1145320
    iget-object v0, p0, LX/6mb;->locationAttachment:LX/6mT;

    if-eqz v0, :cond_10

    .line 1145321
    sget-object v0, LX/6mb;->s:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145322
    iget-object v0, p0, LX/6mb;->locationAttachment:LX/6mT;

    invoke-virtual {v0, p1}, LX/6mT;->a(LX/1su;)V

    .line 1145323
    :cond_10
    iget-object v0, p0, LX/6mb;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_11

    .line 1145324
    iget-object v0, p0, LX/6mb;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_11

    .line 1145325
    sget-object v0, LX/6mb;->t:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145326
    iget-object v0, p0, LX/6mb;->ttl:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1145327
    :cond_11
    iget-object v0, p0, LX/6mb;->refCode:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 1145328
    iget-object v0, p0, LX/6mb;->refCode:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 1145329
    sget-object v0, LX/6mb;->u:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145330
    iget-object v0, p0, LX/6mb;->refCode:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1145331
    :cond_12
    iget-object v0, p0, LX/6mb;->genericMetadata:Ljava/util/Map;

    if-eqz v0, :cond_13

    .line 1145332
    iget-object v0, p0, LX/6mb;->genericMetadata:Ljava/util/Map;

    if-eqz v0, :cond_13

    .line 1145333
    sget-object v0, LX/6mb;->v:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1145334
    new-instance v0, LX/7H3;

    iget-object v1, p0, LX/6mb;->genericMetadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v3, v3, v1}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 1145335
    iget-object v0, p0, LX/6mb;->genericMetadata:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1145336
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, LX/1su;->a(Ljava/lang/String;)V

    .line 1145337
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    goto :goto_3

    .line 1145338
    :cond_13
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1145339
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1145340
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1145345
    if-nez p1, :cond_1

    .line 1145346
    :cond_0
    :goto_0
    return v0

    .line 1145347
    :cond_1
    instance-of v1, p1, LX/6mb;

    if-eqz v1, :cond_0

    .line 1145348
    check-cast p1, LX/6mb;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1145349
    if-nez p1, :cond_3

    .line 1145350
    :cond_2
    :goto_1
    move v0, v2

    .line 1145351
    goto :goto_0

    .line 1145352
    :cond_3
    iget-object v0, p0, LX/6mb;->to:Ljava/lang/String;

    if-eqz v0, :cond_2c

    move v0, v1

    .line 1145353
    :goto_2
    iget-object v3, p1, LX/6mb;->to:Ljava/lang/String;

    if-eqz v3, :cond_2d

    move v3, v1

    .line 1145354
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1145355
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145356
    iget-object v0, p0, LX/6mb;->to:Ljava/lang/String;

    iget-object v3, p1, LX/6mb;->to:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145357
    :cond_5
    iget-object v0, p0, LX/6mb;->body:Ljava/lang/String;

    if-eqz v0, :cond_2e

    move v0, v1

    .line 1145358
    :goto_4
    iget-object v3, p1, LX/6mb;->body:Ljava/lang/String;

    if-eqz v3, :cond_2f

    move v3, v1

    .line 1145359
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1145360
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145361
    iget-object v0, p0, LX/6mb;->body:Ljava/lang/String;

    iget-object v3, p1, LX/6mb;->body:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145362
    :cond_7
    iget-object v0, p0, LX/6mb;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_30

    move v0, v1

    .line 1145363
    :goto_6
    iget-object v3, p1, LX/6mb;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v3, :cond_31

    move v3, v1

    .line 1145364
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1145365
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145366
    iget-object v0, p0, LX/6mb;->offlineThreadingId:Ljava/lang/Long;

    iget-object v3, p1, LX/6mb;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145367
    :cond_9
    iget-object v0, p0, LX/6mb;->coordinates:LX/6mM;

    if-eqz v0, :cond_32

    move v0, v1

    .line 1145368
    :goto_8
    iget-object v3, p1, LX/6mb;->coordinates:LX/6mM;

    if-eqz v3, :cond_33

    move v3, v1

    .line 1145369
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1145370
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145371
    iget-object v0, p0, LX/6mb;->coordinates:LX/6mM;

    iget-object v3, p1, LX/6mb;->coordinates:LX/6mM;

    invoke-virtual {v0, v3}, LX/6mM;->a(LX/6mM;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145372
    :cond_b
    iget-object v0, p0, LX/6mb;->clientTags:Ljava/util/Map;

    if-eqz v0, :cond_34

    move v0, v1

    .line 1145373
    :goto_a
    iget-object v3, p1, LX/6mb;->clientTags:Ljava/util/Map;

    if-eqz v3, :cond_35

    move v3, v1

    .line 1145374
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1145375
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145376
    iget-object v0, p0, LX/6mb;->clientTags:Ljava/util/Map;

    iget-object v3, p1, LX/6mb;->clientTags:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145377
    :cond_d
    iget-object v0, p0, LX/6mb;->objectAttachment:Ljava/lang/String;

    if-eqz v0, :cond_36

    move v0, v1

    .line 1145378
    :goto_c
    iget-object v3, p1, LX/6mb;->objectAttachment:Ljava/lang/String;

    if-eqz v3, :cond_37

    move v3, v1

    .line 1145379
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1145380
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145381
    iget-object v0, p0, LX/6mb;->objectAttachment:Ljava/lang/String;

    iget-object v3, p1, LX/6mb;->objectAttachment:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145382
    :cond_f
    iget-object v0, p0, LX/6mb;->copyMessageId:Ljava/lang/String;

    if-eqz v0, :cond_38

    move v0, v1

    .line 1145383
    :goto_e
    iget-object v3, p1, LX/6mb;->copyMessageId:Ljava/lang/String;

    if-eqz v3, :cond_39

    move v3, v1

    .line 1145384
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1145385
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145386
    iget-object v0, p0, LX/6mb;->copyMessageId:Ljava/lang/String;

    iget-object v3, p1, LX/6mb;->copyMessageId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145387
    :cond_11
    iget-object v0, p0, LX/6mb;->copyAttachmentId:Ljava/lang/String;

    if-eqz v0, :cond_3a

    move v0, v1

    .line 1145388
    :goto_10
    iget-object v3, p1, LX/6mb;->copyAttachmentId:Ljava/lang/String;

    if-eqz v3, :cond_3b

    move v3, v1

    .line 1145389
    :goto_11
    if-nez v0, :cond_12

    if-eqz v3, :cond_13

    .line 1145390
    :cond_12
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145391
    iget-object v0, p0, LX/6mb;->copyAttachmentId:Ljava/lang/String;

    iget-object v3, p1, LX/6mb;->copyAttachmentId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145392
    :cond_13
    iget-object v0, p0, LX/6mb;->mediaAttachmentIds:Ljava/util/List;

    if-eqz v0, :cond_3c

    move v0, v1

    .line 1145393
    :goto_12
    iget-object v3, p1, LX/6mb;->mediaAttachmentIds:Ljava/util/List;

    if-eqz v3, :cond_3d

    move v3, v1

    .line 1145394
    :goto_13
    if-nez v0, :cond_14

    if-eqz v3, :cond_15

    .line 1145395
    :cond_14
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145396
    iget-object v0, p0, LX/6mb;->mediaAttachmentIds:Ljava/util/List;

    iget-object v3, p1, LX/6mb;->mediaAttachmentIds:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145397
    :cond_15
    iget-object v0, p0, LX/6mb;->fbTraceMeta:Ljava/lang/String;

    if-eqz v0, :cond_3e

    move v0, v1

    .line 1145398
    :goto_14
    iget-object v3, p1, LX/6mb;->fbTraceMeta:Ljava/lang/String;

    if-eqz v3, :cond_3f

    move v3, v1

    .line 1145399
    :goto_15
    if-nez v0, :cond_16

    if-eqz v3, :cond_17

    .line 1145400
    :cond_16
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145401
    iget-object v0, p0, LX/6mb;->fbTraceMeta:Ljava/lang/String;

    iget-object v3, p1, LX/6mb;->fbTraceMeta:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145402
    :cond_17
    iget-object v0, p0, LX/6mb;->imageType:Ljava/lang/Integer;

    if-eqz v0, :cond_40

    move v0, v1

    .line 1145403
    :goto_16
    iget-object v3, p1, LX/6mb;->imageType:Ljava/lang/Integer;

    if-eqz v3, :cond_41

    move v3, v1

    .line 1145404
    :goto_17
    if-nez v0, :cond_18

    if-eqz v3, :cond_19

    .line 1145405
    :cond_18
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145406
    iget-object v0, p0, LX/6mb;->imageType:Ljava/lang/Integer;

    iget-object v3, p1, LX/6mb;->imageType:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145407
    :cond_19
    iget-object v0, p0, LX/6mb;->senderFbid:Ljava/lang/Long;

    if-eqz v0, :cond_42

    move v0, v1

    .line 1145408
    :goto_18
    iget-object v3, p1, LX/6mb;->senderFbid:Ljava/lang/Long;

    if-eqz v3, :cond_43

    move v3, v1

    .line 1145409
    :goto_19
    if-nez v0, :cond_1a

    if-eqz v3, :cond_1b

    .line 1145410
    :cond_1a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145411
    iget-object v0, p0, LX/6mb;->senderFbid:Ljava/lang/Long;

    iget-object v3, p1, LX/6mb;->senderFbid:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145412
    :cond_1b
    iget-object v0, p0, LX/6mb;->broadcastRecipients:Ljava/util/Map;

    if-eqz v0, :cond_44

    move v0, v1

    .line 1145413
    :goto_1a
    iget-object v3, p1, LX/6mb;->broadcastRecipients:Ljava/util/Map;

    if-eqz v3, :cond_45

    move v3, v1

    .line 1145414
    :goto_1b
    if-nez v0, :cond_1c

    if-eqz v3, :cond_1d

    .line 1145415
    :cond_1c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145416
    iget-object v0, p0, LX/6mb;->broadcastRecipients:Ljava/util/Map;

    iget-object v3, p1, LX/6mb;->broadcastRecipients:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145417
    :cond_1d
    iget-object v0, p0, LX/6mb;->attributionAppId:Ljava/lang/Long;

    if-eqz v0, :cond_46

    move v0, v1

    .line 1145418
    :goto_1c
    iget-object v3, p1, LX/6mb;->attributionAppId:Ljava/lang/Long;

    if-eqz v3, :cond_47

    move v3, v1

    .line 1145419
    :goto_1d
    if-nez v0, :cond_1e

    if-eqz v3, :cond_1f

    .line 1145420
    :cond_1e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145421
    iget-object v0, p0, LX/6mb;->attributionAppId:Ljava/lang/Long;

    iget-object v3, p1, LX/6mb;->attributionAppId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145422
    :cond_1f
    iget-object v0, p0, LX/6mb;->iosBundleId:Ljava/lang/String;

    if-eqz v0, :cond_48

    move v0, v1

    .line 1145423
    :goto_1e
    iget-object v3, p1, LX/6mb;->iosBundleId:Ljava/lang/String;

    if-eqz v3, :cond_49

    move v3, v1

    .line 1145424
    :goto_1f
    if-nez v0, :cond_20

    if-eqz v3, :cond_21

    .line 1145425
    :cond_20
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145426
    iget-object v0, p0, LX/6mb;->iosBundleId:Ljava/lang/String;

    iget-object v3, p1, LX/6mb;->iosBundleId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145427
    :cond_21
    iget-object v0, p0, LX/6mb;->androidKeyHash:Ljava/lang/String;

    if-eqz v0, :cond_4a

    move v0, v1

    .line 1145428
    :goto_20
    iget-object v3, p1, LX/6mb;->androidKeyHash:Ljava/lang/String;

    if-eqz v3, :cond_4b

    move v3, v1

    .line 1145429
    :goto_21
    if-nez v0, :cond_22

    if-eqz v3, :cond_23

    .line 1145430
    :cond_22
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145431
    iget-object v0, p0, LX/6mb;->androidKeyHash:Ljava/lang/String;

    iget-object v3, p1, LX/6mb;->androidKeyHash:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145432
    :cond_23
    iget-object v0, p0, LX/6mb;->locationAttachment:LX/6mT;

    if-eqz v0, :cond_4c

    move v0, v1

    .line 1145433
    :goto_22
    iget-object v3, p1, LX/6mb;->locationAttachment:LX/6mT;

    if-eqz v3, :cond_4d

    move v3, v1

    .line 1145434
    :goto_23
    if-nez v0, :cond_24

    if-eqz v3, :cond_25

    .line 1145435
    :cond_24
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145436
    iget-object v0, p0, LX/6mb;->locationAttachment:LX/6mT;

    iget-object v3, p1, LX/6mb;->locationAttachment:LX/6mT;

    invoke-virtual {v0, v3}, LX/6mT;->a(LX/6mT;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145437
    :cond_25
    iget-object v0, p0, LX/6mb;->ttl:Ljava/lang/Integer;

    if-eqz v0, :cond_4e

    move v0, v1

    .line 1145438
    :goto_24
    iget-object v3, p1, LX/6mb;->ttl:Ljava/lang/Integer;

    if-eqz v3, :cond_4f

    move v3, v1

    .line 1145439
    :goto_25
    if-nez v0, :cond_26

    if-eqz v3, :cond_27

    .line 1145440
    :cond_26
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145441
    iget-object v0, p0, LX/6mb;->ttl:Ljava/lang/Integer;

    iget-object v3, p1, LX/6mb;->ttl:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145442
    :cond_27
    iget-object v0, p0, LX/6mb;->refCode:Ljava/lang/Integer;

    if-eqz v0, :cond_50

    move v0, v1

    .line 1145443
    :goto_26
    iget-object v3, p1, LX/6mb;->refCode:Ljava/lang/Integer;

    if-eqz v3, :cond_51

    move v3, v1

    .line 1145444
    :goto_27
    if-nez v0, :cond_28

    if-eqz v3, :cond_29

    .line 1145445
    :cond_28
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145446
    iget-object v0, p0, LX/6mb;->refCode:Ljava/lang/Integer;

    iget-object v3, p1, LX/6mb;->refCode:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145447
    :cond_29
    iget-object v0, p0, LX/6mb;->genericMetadata:Ljava/util/Map;

    if-eqz v0, :cond_52

    move v0, v1

    .line 1145448
    :goto_28
    iget-object v3, p1, LX/6mb;->genericMetadata:Ljava/util/Map;

    if-eqz v3, :cond_53

    move v3, v1

    .line 1145449
    :goto_29
    if-nez v0, :cond_2a

    if-eqz v3, :cond_2b

    .line 1145450
    :cond_2a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1145451
    iget-object v0, p0, LX/6mb;->genericMetadata:Ljava/util/Map;

    iget-object v3, p1, LX/6mb;->genericMetadata:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_2b
    move v2, v1

    .line 1145452
    goto/16 :goto_1

    :cond_2c
    move v0, v2

    .line 1145453
    goto/16 :goto_2

    :cond_2d
    move v3, v2

    .line 1145454
    goto/16 :goto_3

    :cond_2e
    move v0, v2

    .line 1145455
    goto/16 :goto_4

    :cond_2f
    move v3, v2

    .line 1145456
    goto/16 :goto_5

    :cond_30
    move v0, v2

    .line 1145457
    goto/16 :goto_6

    :cond_31
    move v3, v2

    .line 1145458
    goto/16 :goto_7

    :cond_32
    move v0, v2

    .line 1145459
    goto/16 :goto_8

    :cond_33
    move v3, v2

    .line 1145460
    goto/16 :goto_9

    :cond_34
    move v0, v2

    .line 1145461
    goto/16 :goto_a

    :cond_35
    move v3, v2

    .line 1145462
    goto/16 :goto_b

    :cond_36
    move v0, v2

    .line 1145463
    goto/16 :goto_c

    :cond_37
    move v3, v2

    .line 1145464
    goto/16 :goto_d

    :cond_38
    move v0, v2

    .line 1145465
    goto/16 :goto_e

    :cond_39
    move v3, v2

    .line 1145466
    goto/16 :goto_f

    :cond_3a
    move v0, v2

    .line 1145467
    goto/16 :goto_10

    :cond_3b
    move v3, v2

    .line 1145468
    goto/16 :goto_11

    :cond_3c
    move v0, v2

    .line 1145469
    goto/16 :goto_12

    :cond_3d
    move v3, v2

    .line 1145470
    goto/16 :goto_13

    :cond_3e
    move v0, v2

    .line 1145471
    goto/16 :goto_14

    :cond_3f
    move v3, v2

    .line 1145472
    goto/16 :goto_15

    :cond_40
    move v0, v2

    .line 1145473
    goto/16 :goto_16

    :cond_41
    move v3, v2

    .line 1145474
    goto/16 :goto_17

    :cond_42
    move v0, v2

    .line 1145475
    goto/16 :goto_18

    :cond_43
    move v3, v2

    .line 1145476
    goto/16 :goto_19

    :cond_44
    move v0, v2

    .line 1145477
    goto/16 :goto_1a

    :cond_45
    move v3, v2

    .line 1145478
    goto/16 :goto_1b

    :cond_46
    move v0, v2

    .line 1145479
    goto/16 :goto_1c

    :cond_47
    move v3, v2

    .line 1145480
    goto/16 :goto_1d

    :cond_48
    move v0, v2

    .line 1145481
    goto/16 :goto_1e

    :cond_49
    move v3, v2

    .line 1145482
    goto/16 :goto_1f

    :cond_4a
    move v0, v2

    .line 1145483
    goto/16 :goto_20

    :cond_4b
    move v3, v2

    .line 1145484
    goto/16 :goto_21

    :cond_4c
    move v0, v2

    .line 1145485
    goto/16 :goto_22

    :cond_4d
    move v3, v2

    .line 1145486
    goto/16 :goto_23

    :cond_4e
    move v0, v2

    .line 1145487
    goto/16 :goto_24

    :cond_4f
    move v3, v2

    .line 1145488
    goto/16 :goto_25

    :cond_50
    move v0, v2

    .line 1145489
    goto/16 :goto_26

    :cond_51
    move v3, v2

    .line 1145490
    goto/16 :goto_27

    :cond_52
    move v0, v2

    .line 1145491
    goto/16 :goto_28

    :cond_53
    move v3, v2

    .line 1145492
    goto/16 :goto_29
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1145344
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1145341
    sget-boolean v0, LX/6mb;->a:Z

    .line 1145342
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6mb;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1145343
    return-object v0
.end method
