.class public LX/6jj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final action:Ljava/lang/Integer;

.field public final messageMetadata:LX/6kn;

.field public final recipientFbId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1132381
    new-instance v0, LX/1sv;

    const-string v1, "DeltaApprovalQueue"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jj;->b:LX/1sv;

    .line 1132382
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jj;->c:LX/1sw;

    .line 1132383
    new-instance v0, LX/1sw;

    const-string v1, "action"

    const/16 v2, 0x8

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jj;->d:LX/1sw;

    .line 1132384
    new-instance v0, LX/1sw;

    const-string v1, "recipientFbId"

    const/16 v2, 0xa

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jj;->e:LX/1sw;

    .line 1132385
    sput-boolean v4, LX/6jj;->a:Z

    return-void
.end method

.method private constructor <init>(LX/6kn;Ljava/lang/Integer;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1132376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1132377
    iput-object p1, p0, LX/6jj;->messageMetadata:LX/6kn;

    .line 1132378
    iput-object p2, p0, LX/6jj;->action:Ljava/lang/Integer;

    .line 1132379
    iput-object p3, p0, LX/6jj;->recipientFbId:Ljava/lang/Long;

    .line 1132380
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 1132367
    iget-object v0, p0, LX/6jj;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1132368
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jj;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1132369
    :cond_0
    iget-object v0, p0, LX/6jj;->action:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1132370
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'action\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jj;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1132371
    :cond_1
    iget-object v0, p0, LX/6jj;->recipientFbId:Ljava/lang/Long;

    if-nez v0, :cond_2

    .line 1132372
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'recipientFbId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jj;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1132373
    :cond_2
    iget-object v0, p0, LX/6jj;->action:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    sget-object v0, LX/6jY;->a:LX/1sn;

    iget-object v1, p0, LX/6jj;->action:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1132374
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'action\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6jj;->action:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1132375
    :cond_3
    return-void
.end method

.method public static b(LX/1su;)LX/6jj;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1132349
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v1, v0

    move-object v2, v0

    .line 1132350
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v3

    .line 1132351
    iget-byte v4, v3, LX/1sw;->b:B

    if-eqz v4, :cond_3

    .line 1132352
    iget-short v4, v3, LX/1sw;->c:S

    packed-switch v4, :pswitch_data_0

    .line 1132353
    iget-byte v3, v3, LX/1sw;->b:B

    invoke-static {p0, v3}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1132354
    :pswitch_0
    iget-byte v4, v3, LX/1sw;->b:B

    const/16 v5, 0xc

    if-ne v4, v5, :cond_0

    .line 1132355
    invoke-static {p0}, LX/6kn;->b(LX/1su;)LX/6kn;

    move-result-object v2

    goto :goto_0

    .line 1132356
    :cond_0
    iget-byte v3, v3, LX/1sw;->b:B

    invoke-static {p0, v3}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1132357
    :pswitch_1
    iget-byte v4, v3, LX/1sw;->b:B

    const/16 v5, 0x8

    if-ne v4, v5, :cond_1

    .line 1132358
    invoke-virtual {p0}, LX/1su;->m()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 1132359
    :cond_1
    iget-byte v3, v3, LX/1sw;->b:B

    invoke-static {p0, v3}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1132360
    :pswitch_2
    iget-byte v4, v3, LX/1sw;->b:B

    const/16 v5, 0xa

    if-ne v4, v5, :cond_2

    .line 1132361
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 1132362
    :cond_2
    iget-byte v3, v3, LX/1sw;->b:B

    invoke-static {p0, v3}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1132363
    :cond_3
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1132364
    new-instance v3, LX/6jj;

    invoke-direct {v3, v2, v1, v0}, LX/6jj;-><init>(LX/6kn;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 1132365
    invoke-direct {v3}, LX/6jj;->a()V

    .line 1132366
    return-object v3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1132307
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1132308
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v2, v0

    .line 1132309
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    move-object v1, v0

    .line 1132310
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "DeltaApprovalQueue"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1132311
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132312
    const-string v0, "("

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132313
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132314
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132315
    const-string v0, "messageMetadata"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132316
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132317
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132318
    iget-object v0, p0, LX/6jj;->messageMetadata:LX/6kn;

    if-nez v0, :cond_4

    .line 1132319
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132320
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132321
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132322
    const-string v0, "action"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132323
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132324
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132325
    iget-object v0, p0, LX/6jj;->action:Ljava/lang/Integer;

    if-nez v0, :cond_5

    .line 1132326
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132327
    :cond_0
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132328
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132329
    const-string v0, "recipientFbId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132330
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132331
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132332
    iget-object v0, p0, LX/6jj;->recipientFbId:Ljava/lang/Long;

    if-nez v0, :cond_7

    .line 1132333
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132334
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132335
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132336
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1132337
    :cond_1
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1132338
    :cond_2
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1132339
    :cond_3
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 1132340
    :cond_4
    iget-object v0, p0, LX/6jj;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1132341
    :cond_5
    sget-object v0, LX/6jY;->b:Ljava/util/Map;

    iget-object v5, p0, LX/6jj;->action:Ljava/lang/Integer;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1132342
    if-eqz v0, :cond_6

    .line 1132343
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132344
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132345
    :cond_6
    iget-object v5, p0, LX/6jj;->action:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1132346
    if-eqz v0, :cond_0

    .line 1132347
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1132348
    :cond_7
    iget-object v0, p0, LX/6jj;->recipientFbId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1132293
    invoke-direct {p0}, LX/6jj;->a()V

    .line 1132294
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1132295
    iget-object v0, p0, LX/6jj;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1132296
    sget-object v0, LX/6jj;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132297
    iget-object v0, p0, LX/6jj;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1132298
    :cond_0
    iget-object v0, p0, LX/6jj;->action:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1132299
    sget-object v0, LX/6jj;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132300
    iget-object v0, p0, LX/6jj;->action:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1132301
    :cond_1
    iget-object v0, p0, LX/6jj;->recipientFbId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1132302
    sget-object v0, LX/6jj;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132303
    iget-object v0, p0, LX/6jj;->recipientFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1132304
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1132305
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1132306
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1132264
    if-nez p1, :cond_1

    .line 1132265
    :cond_0
    :goto_0
    return v0

    .line 1132266
    :cond_1
    instance-of v1, p1, LX/6jj;

    if-eqz v1, :cond_0

    .line 1132267
    check-cast p1, LX/6jj;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1132268
    if-nez p1, :cond_3

    .line 1132269
    :cond_2
    :goto_1
    move v0, v2

    .line 1132270
    goto :goto_0

    .line 1132271
    :cond_3
    iget-object v0, p0, LX/6jj;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1132272
    :goto_2
    iget-object v3, p1, LX/6jj;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1132273
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1132274
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132275
    iget-object v0, p0, LX/6jj;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6jj;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132276
    :cond_5
    iget-object v0, p0, LX/6jj;->action:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1132277
    :goto_4
    iget-object v3, p1, LX/6jj;->action:Ljava/lang/Integer;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1132278
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1132279
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132280
    iget-object v0, p0, LX/6jj;->action:Ljava/lang/Integer;

    iget-object v3, p1, LX/6jj;->action:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1132281
    :cond_7
    iget-object v0, p0, LX/6jj;->recipientFbId:Ljava/lang/Long;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1132282
    :goto_6
    iget-object v3, p1, LX/6jj;->recipientFbId:Ljava/lang/Long;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1132283
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1132284
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132285
    iget-object v0, p0, LX/6jj;->recipientFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/6jj;->recipientFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 1132286
    goto :goto_1

    :cond_a
    move v0, v2

    .line 1132287
    goto :goto_2

    :cond_b
    move v3, v2

    .line 1132288
    goto :goto_3

    :cond_c
    move v0, v2

    .line 1132289
    goto :goto_4

    :cond_d
    move v3, v2

    .line 1132290
    goto :goto_5

    :cond_e
    move v0, v2

    .line 1132291
    goto :goto_6

    :cond_f
    move v3, v2

    .line 1132292
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1132263
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1132260
    sget-boolean v0, LX/6jj;->a:Z

    .line 1132261
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jj;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1132262
    return-object v0
.end method
