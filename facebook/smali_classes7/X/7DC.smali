.class public final enum LX/7DC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7DC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7DC;

.field public static final enum CUBESTRIP:LX/7DC;

.field public static final enum TILED:LX/7DC;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1182473
    new-instance v0, LX/7DC;

    const-string v1, "TILED"

    const-string v2, "tiled"

    invoke-direct {v0, v1, v3, v2}, LX/7DC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7DC;->TILED:LX/7DC;

    .line 1182474
    new-instance v0, LX/7DC;

    const-string v1, "CUBESTRIP"

    const-string v2, "cubestrip"

    invoke-direct {v0, v1, v4, v2}, LX/7DC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7DC;->CUBESTRIP:LX/7DC;

    .line 1182475
    const/4 v0, 0x2

    new-array v0, v0, [LX/7DC;

    sget-object v1, LX/7DC;->TILED:LX/7DC;

    aput-object v1, v0, v3

    sget-object v1, LX/7DC;->CUBESTRIP:LX/7DC;

    aput-object v1, v0, v4

    sput-object v0, LX/7DC;->$VALUES:[LX/7DC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1182476
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1182477
    iput-object p3, p0, LX/7DC;->value:Ljava/lang/String;

    .line 1182478
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7DC;
    .locals 1

    .prologue
    .line 1182479
    const-class v0, LX/7DC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7DC;

    return-object v0
.end method

.method public static values()[LX/7DC;
    .locals 1

    .prologue
    .line 1182480
    sget-object v0, LX/7DC;->$VALUES:[LX/7DC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7DC;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1182481
    iget-object v0, p0, LX/7DC;->value:Ljava/lang/String;

    return-object v0
.end method
