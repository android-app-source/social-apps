.class public final LX/7JM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field public b:Z

.field private c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:LX/162;

.field public f:I

.field public g:I

.field public h:Z

.field public i:Z

.field private j:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public k:LX/1bf;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/1bf;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/04D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/video/engine/VideoDataSource;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Z


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1193480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1193481
    const-string v0, ""

    iput-object v0, p0, LX/7JM;->a:Ljava/lang/String;

    .line 1193482
    const-string v0, ""

    iput-object v0, p0, LX/7JM;->c:Ljava/lang/String;

    .line 1193483
    const-string v0, ""

    iput-object v0, p0, LX/7JM;->d:Ljava/lang/String;

    .line 1193484
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    iput-object v0, p0, LX/7JM;->e:LX/162;

    .line 1193485
    iput-boolean v2, p0, LX/7JM;->b:Z

    .line 1193486
    iput-boolean v2, p0, LX/7JM;->h:Z

    .line 1193487
    iput-boolean v2, p0, LX/7JM;->i:Z

    .line 1193488
    iput-object v3, p0, LX/7JM;->n:LX/0Px;

    .line 1193489
    iput-object v3, p0, LX/7JM;->k:LX/1bf;

    .line 1193490
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    iput-object v0, p0, LX/7JM;->m:LX/04D;

    .line 1193491
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object v0, p0, LX/7JM;->j:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1193492
    const/4 v0, -0x1

    iput v0, p0, LX/7JM;->g:I

    .line 1193493
    return-void
.end method

.method public static newBuilder()LX/7JM;
    .locals 1

    .prologue
    .line 1193479
    new-instance v0, LX/7JM;

    invoke-direct {v0}, LX/7JM;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/7JM;
    .locals 0

    .prologue
    .line 1193476
    if-eqz p1, :cond_0

    .line 1193477
    iput-object p1, p0, LX/7JM;->c:Ljava/lang/String;

    .line 1193478
    :cond_0
    return-object p0
.end method

.method public final a()LX/7JN;
    .locals 18

    .prologue
    .line 1193475
    new-instance v1, LX/7JN;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7JM;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7JM;->n:LX/0Px;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/7JM;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7JM;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/7JM;->k:LX/1bf;

    move-object/from16 v0, p0

    iget-object v7, v0, LX/7JM;->l:LX/1bf;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7JM;->e:LX/162;

    move-object/from16 v0, p0

    iget-boolean v9, v0, LX/7JM;->b:Z

    move-object/from16 v0, p0

    iget-boolean v10, v0, LX/7JM;->i:Z

    move-object/from16 v0, p0

    iget-boolean v11, v0, LX/7JM;->h:Z

    move-object/from16 v0, p0

    iget-object v12, v0, LX/7JM;->j:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/7JM;->m:LX/04D;

    move-object/from16 v0, p0

    iget v14, v0, LX/7JM;->f:I

    move-object/from16 v0, p0

    iget v15, v0, LX/7JM;->g:I

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/7JM;->o:Z

    move/from16 v16, v0

    const/16 v17, 0x0

    invoke-direct/range {v1 .. v17}, LX/7JN;-><init>(Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;LX/1bf;LX/1bf;LX/162;ZZZLcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;LX/04D;IIZB)V

    return-object v1
.end method

.method public final b(Ljava/lang/String;)LX/7JM;
    .locals 0

    .prologue
    .line 1193472
    if-eqz p1, :cond_0

    .line 1193473
    iput-object p1, p0, LX/7JM;->a:Ljava/lang/String;

    .line 1193474
    :cond_0
    return-object p0
.end method
