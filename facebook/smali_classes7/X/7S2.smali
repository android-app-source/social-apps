.class public final LX/7S2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field public final synthetic a:Landroid/view/TextureView$SurfaceTextureListener;

.field public final synthetic b:LX/7S3;


# direct methods
.method public constructor <init>(LX/7S3;Landroid/view/TextureView$SurfaceTextureListener;)V
    .locals 0

    .prologue
    .line 1208046
    iput-object p1, p0, LX/7S2;->b:LX/7S3;

    iput-object p2, p0, LX/7S2;->a:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 3

    .prologue
    .line 1208047
    iget-object v0, p0, LX/7S2;->b:LX/7S3;

    iget-boolean v0, v0, LX/7S3;->h:Z

    if-eqz v0, :cond_1

    .line 1208048
    :cond_0
    :goto_0
    return-void

    .line 1208049
    :cond_1
    iget-object v1, p0, LX/7S2;->b:LX/7S3;

    monitor-enter v1

    .line 1208050
    :try_start_0
    iget-object v0, p0, LX/7S2;->b:LX/7S3;

    const/4 v2, 0x1

    .line 1208051
    iput-boolean v2, v0, LX/7S3;->h:Z

    .line 1208052
    iget-object v0, p0, LX/7S2;->b:LX/7S3;

    new-instance v2, Landroid/view/Surface;

    invoke-direct {v2, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    .line 1208053
    iput-object v2, v0, LX/7S3;->b:Landroid/view/Surface;

    .line 1208054
    iget-object v0, p0, LX/7S2;->b:LX/7S3;

    .line 1208055
    iput p2, v0, LX/7S3;->c:I

    .line 1208056
    iget-object v0, p0, LX/7S2;->b:LX/7S3;

    .line 1208057
    iput p3, v0, LX/7S3;->d:I

    .line 1208058
    iget-object v0, p0, LX/7S2;->b:LX/7S3;

    iget-object v2, p0, LX/7S2;->a:Landroid/view/TextureView$SurfaceTextureListener;

    .line 1208059
    iput-object v2, v0, LX/7S3;->f:Landroid/view/TextureView$SurfaceTextureListener;

    .line 1208060
    iget-object v0, p0, LX/7S2;->b:LX/7S3;

    const v2, -0x7e996964

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1208061
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1208062
    iget-object v0, p0, LX/7S2;->b:LX/7S3;

    iget-object v0, v0, LX/7S3;->e:LX/6Kl;

    if-eqz v0, :cond_0

    .line 1208063
    iget-object v0, p0, LX/7S2;->b:LX/7S3;

    iget-object v0, v0, LX/7S3;->e:LX/6Kl;

    iget-object v1, p0, LX/7S2;->b:LX/7S3;

    iget-object v2, p0, LX/7S2;->b:LX/7S3;

    iget-object v2, v2, LX/7S3;->b:Landroid/view/Surface;

    invoke-virtual {v0, v1, v2}, LX/6Kl;->a(LX/6Kd;Landroid/view/Surface;)V

    goto :goto_0

    .line 1208064
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 1208065
    iget-object v0, p0, LX/7S2;->b:LX/7S3;

    invoke-virtual {v0}, LX/7S3;->c()V

    .line 1208066
    const/4 v0, 0x1

    return v0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    .prologue
    .line 1208038
    iget-object v0, p0, LX/7S2;->b:LX/7S3;

    .line 1208039
    iput p2, v0, LX/7S3;->c:I

    .line 1208040
    iget-object v0, p0, LX/7S2;->b:LX/7S3;

    .line 1208041
    iput p3, v0, LX/7S3;->d:I

    .line 1208042
    iget-object v0, p0, LX/7S2;->b:LX/7S3;

    iget-object v0, v0, LX/7S3;->e:LX/6Kl;

    if-eqz v0, :cond_0

    .line 1208043
    iget-object v0, p0, LX/7S2;->b:LX/7S3;

    iget-object v0, v0, LX/7S3;->e:LX/6Kl;

    iget-object v1, p0, LX/7S2;->b:LX/7S3;

    invoke-virtual {v0, v1}, LX/6Kl;->a(LX/6Kd;)V

    .line 1208044
    :cond_0
    return-void
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 1208045
    return-void
.end method
