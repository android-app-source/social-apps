.class public LX/6wo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/6wp;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6wp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1158026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158027
    iput-object p1, p0, LX/6wo;->a:Landroid/content/Context;

    .line 1158028
    iput-object p2, p0, LX/6wo;->b:LX/6wp;

    .line 1158029
    return-void
.end method

.method public static b(LX/0QB;)LX/6wo;
    .locals 3

    .prologue
    .line 1158032
    new-instance v2, LX/6wo;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/6wp;->a(LX/0QB;)LX/6wp;

    move-result-object v1

    check-cast v1, LX/6wp;

    invoke-direct {v2, v0, v1}, LX/6wo;-><init>(Landroid/content/Context;LX/6wp;)V

    .line 1158033
    return-object v2
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1158034
    iget-object v0, p0, LX/6wo;->b:LX/6wp;

    invoke-virtual {v0}, LX/6wp;->a()I

    move-result v0

    return v0
.end method

.method public final a(LX/6z8;)Z
    .locals 1

    .prologue
    .line 1158031
    iget-object v0, p0, LX/6wo;->b:LX/6wp;

    invoke-virtual {v0, p1}, LX/6wp;->a(LX/6z8;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/6z8;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1158030
    iget-object v0, p0, LX/6wo;->a:Landroid/content/Context;

    const v1, 0x7f081e2c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
