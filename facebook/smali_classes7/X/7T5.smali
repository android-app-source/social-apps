.class public LX/7T5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final i:[I


# instance fields
.field private final b:Ljava/io/File;

.field private final c:J

.field private final d:J

.field private final e:Ljava/io/File;

.field private final f:I

.field private g:LX/60y;

.field private volatile h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1209648
    const-class v0, LX/7T5;

    sput-object v0, LX/7T5;->a:Ljava/lang/Class;

    .line 1209649
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/7T5;->i:[I

    return-void

    :array_0
    .array-data 4
        0x17700
        0x15888
        0xfa00
        0xbb80
        0xac44
        0x7d00
        0x5dc0
        0x5622
        0x3e80
        0x2ee0
        0x2b11
        0x1f40
    .end array-data
.end method

.method public constructor <init>(LX/7T4;)V
    .locals 2

    .prologue
    .line 1209650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1209651
    iget-object v0, p1, LX/7T4;->a:Ljava/io/File;

    iput-object v0, p0, LX/7T5;->b:Ljava/io/File;

    .line 1209652
    iget-wide v0, p1, LX/7T4;->b:J

    iput-wide v0, p0, LX/7T5;->c:J

    .line 1209653
    iget-wide v0, p1, LX/7T4;->c:J

    iput-wide v0, p0, LX/7T5;->d:J

    .line 1209654
    iget-object v0, p1, LX/7T4;->d:Ljava/io/File;

    iput-object v0, p0, LX/7T5;->e:Ljava/io/File;

    .line 1209655
    iget v0, p1, LX/7T4;->e:I

    iput v0, p0, LX/7T5;->f:I

    .line 1209656
    return-void
.end method

.method private static a(III)Landroid/media/MediaFormat;
    .locals 3

    .prologue
    .line 1209657
    const-string v0, "audio/mp4a-latm"

    invoke-static {v0, p0, p1}, Landroid/media/MediaFormat;->createAudioFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v0

    .line 1209658
    const-string v1, "bitrate"

    invoke-virtual {v0, v1, p2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1209659
    const-string v1, "sample-rate"

    invoke-virtual {v0, v1, p0}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1209660
    const-string v1, "channel-count"

    invoke-virtual {v0, v1, p1}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1209661
    const-string v1, "aac-profile"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1209662
    const-string v1, "max-input-size"

    const v2, 0xfa00

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1209663
    return-object v0
.end method

.method private static a(II[B)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 1209664
    const/4 v0, -0x1

    aput-byte v0, p2, v3

    .line 1209665
    const/4 v0, 0x1

    const/16 v1, -0xf

    aput-byte v1, p2, v0

    .line 1209666
    const/4 v1, 0x0

    .line 1209667
    move v0, v1

    :goto_0
    sget-object v2, LX/7T5;->i:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1209668
    sget-object v2, LX/7T5;->i:[I

    aget v2, v2, v0

    if-ne p0, v2, :cond_0

    .line 1209669
    :goto_1
    move v0, v0

    .line 1209670
    int-to-byte v0, v0

    .line 1209671
    int-to-byte v1, p1

    .line 1209672
    const/16 v2, 0x40

    aput-byte v2, p2, v4

    .line 1209673
    aget-byte v2, p2, v4

    shl-int/lit8 v0, v0, 0x2

    or-int/2addr v0, v2

    int-to-byte v0, v0

    aput-byte v0, p2, v4

    .line 1209674
    aget-byte v0, p2, v4

    shr-int/lit8 v2, v1, 0x2

    or-int/2addr v0, v2

    int-to-byte v0, v0

    aput-byte v0, p2, v4

    .line 1209675
    const/4 v0, 0x3

    and-int/lit8 v1, v1, 0x3

    shl-int/lit8 v1, v1, 0x6

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    .line 1209676
    const/4 v0, 0x4

    aput-byte v3, p2, v0

    .line 1209677
    const/4 v0, 0x5

    aput-byte v3, p2, v0

    .line 1209678
    const/4 v0, 0x6

    const/4 v1, -0x4

    aput-byte v1, p2, v0

    .line 1209679
    return-void

    .line 1209680
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1209681
    :cond_1
    sget-object v0, LX/7T5;->a:Ljava/lang/Class;

    const-string v2, "Sampling rate %s is not supported."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v0, v2, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 1209682
    goto :goto_1
.end method

.method public static a(Ljava/io/File;)Z
    .locals 5

    .prologue
    const/16 v2, 0x10

    const/4 v0, 0x0

    .line 1209683
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v2, :cond_0

    .line 1209684
    sget-object v1, LX/7T5;->a:Ljava/lang/Class;

    const-string v2, "Can\'t transcode. SDK version is below API level JELLY_BEAN (16)."

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1209685
    :goto_0
    return v0

    .line 1209686
    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1209687
    :cond_1
    sget-object v1, LX/7T5;->a:Ljava/lang/Class;

    const-string v2, "Can\'t transcode. Invalid input file or file does not exist."

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 1209688
    :cond_2
    new-instance v2, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 1209689
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 1209690
    const/16 v1, 0x10

    invoke-virtual {v2, v1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1209691
    if-eqz v1, :cond_3

    const/4 v0, 0x1

    .line 1209692
    :cond_3
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .line 1209693
    :catch_0
    move-exception v1

    .line 1209694
    :try_start_1
    sget-object v3, LX/7T5;->a:Ljava/lang/Class;

    const-string v4, "Failed: "

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1209695
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v0
.end method


# virtual methods
.method public final a(LX/60y;)V
    .locals 0

    .prologue
    .line 1209696
    iput-object p1, p0, LX/7T5;->g:LX/60y;

    .line 1209697
    return-void
.end method

.method public final a(Landroid/media/MediaExtractor;I)Z
    .locals 33

    .prologue
    .line 1209698
    move-object/from16 v0, p0

    iget-object v2, v0, LX/7T5;->b:Ljava/io/File;

    invoke-static {v2}, LX/7T5;->a(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1209699
    const/4 v2, 0x0

    .line 1209700
    :goto_0
    return v2

    .line 1209701
    :cond_0
    new-instance v17, Ljava/io/FileOutputStream;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7T5;->e:Ljava/io/File;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1209702
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v20

    .line 1209703
    :try_start_0
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v7

    .line 1209704
    const/4 v3, 0x0

    .line 1209705
    const/4 v2, 0x0

    move v6, v2

    :goto_1
    if-ge v6, v7, :cond_3

    if-nez v3, :cond_3

    .line 1209706
    invoke-static {v6}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v2

    .line 1209707
    invoke-virtual {v2}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 1209708
    invoke-virtual {v2}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object v8

    .line 1209709
    const/4 v5, 0x0

    .line 1209710
    const/4 v4, 0x0

    move/from16 v32, v4

    move v4, v5

    move/from16 v5, v32

    :goto_2
    array-length v9, v8

    if-ge v5, v9, :cond_2

    if-nez v4, :cond_2

    .line 1209711
    aget-object v9, v8, v5

    const-string v10, "audio/mp4a-latm"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1209712
    const/4 v4, 0x1

    .line 1209713
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1209714
    :cond_2
    if-eqz v4, :cond_1d

    .line 1209715
    :goto_3
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move-object v3, v2

    goto :goto_1

    .line 1209716
    :cond_3
    if-nez v3, :cond_4

    .line 1209717
    sget-object v2, LX/7T5;->a:Ljava/lang/Class;

    const-string v3, "No codec supporting mime type %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "audio/mp4a-latm"

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1209718
    invoke-virtual/range {p1 .. p1}, Landroid/media/MediaExtractor;->release()V

    .line 1209719
    new-instance v2, Ljava/io/FileNotFoundException;

    const-string v3, "No codec supporting audio/mp4a-latm"

    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1209720
    :catchall_0
    move-exception v2

    .line 1209721
    :try_start_1
    invoke-interface/range {v20 .. v20}, Ljava/nio/channels/WritableByteChannel;->close()V

    .line 1209722
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1209723
    :goto_4
    throw v2

    .line 1209724
    :cond_4
    :try_start_2
    invoke-virtual {v3}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    .line 1209725
    invoke-virtual/range {p1 .. p2}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v8

    .line 1209726
    const-string v2, "mime"

    invoke-virtual {v8, v2}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1209727
    invoke-virtual/range {p1 .. p2}, Landroid/media/MediaExtractor;->selectTrack(I)V

    .line 1209728
    const-string v4, "durationUs"

    invoke-virtual {v8, v4}, Landroid/media/MediaFormat;->getLong(Ljava/lang/String;)J

    move-result-wide v18

    .line 1209729
    invoke-static {v2}, Landroid/media/MediaCodec;->createDecoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v21

    .line 1209730
    if-nez v21, :cond_5

    .line 1209731
    sget-object v3, LX/7T5;->a:Ljava/lang/Class;

    const-string v4, "Can\'t create decoder for %s."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v3, v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1209732
    invoke-virtual/range {p1 .. p1}, Landroid/media/MediaExtractor;->release()V

    .line 1209733
    new-instance v3, Ljava/io/FileNotFoundException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Can\'t create decoder for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1209734
    :cond_5
    invoke-virtual {v3}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v2

    .line 1209735
    const/4 v7, 0x0

    .line 1209736
    const/4 v6, 0x0

    .line 1209737
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v8, v3, v4, v5}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 1209738
    invoke-virtual/range {v21 .. v21}, Landroid/media/MediaCodec;->start()V

    .line 1209739
    invoke-virtual/range {v21 .. v21}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v22

    .line 1209740
    invoke-virtual/range {v21 .. v21}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1209741
    new-instance v23, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct/range {v23 .. v23}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 1209742
    new-instance v24, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct/range {v24 .. v24}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 1209743
    const/4 v4, 0x0

    .line 1209744
    move-object/from16 v0, p0

    iget-wide v10, v0, LX/7T5;->c:J

    const-wide/16 v12, 0x0

    cmp-long v3, v10, v12

    if-lez v3, :cond_6

    .line 1209745
    move-object/from16 v0, p0

    iget-wide v10, v0, LX/7T5;->c:J

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11, v3}, Landroid/media/MediaExtractor;->seekTo(JI)V

    .line 1209746
    invoke-virtual/range {p1 .. p1}, Landroid/media/MediaExtractor;->getSampleTime()J

    .line 1209747
    :cond_6
    const/4 v3, 0x7

    new-array v0, v3, [B

    move-object/from16 v25, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1209748
    const/4 v11, 0x0

    .line 1209749
    const/4 v3, 0x0

    move v12, v4

    move-object v13, v5

    move-object v14, v6

    move-object v15, v7

    move-object/from16 v16, v8

    .line 1209750
    :goto_5
    if-nez v3, :cond_17

    :try_start_3
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7T5;->h:Z

    if-nez v4, :cond_17

    .line 1209751
    const-wide/16 v4, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v5

    .line 1209752
    if-ltz v5, :cond_c

    .line 1209753
    aget-object v4, v22, v5

    .line 1209754
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Landroid/media/MediaExtractor;->readSampleData(Ljava/nio/ByteBuffer;I)I

    move-result v7

    .line 1209755
    if-gez v7, :cond_b

    .line 1209756
    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x4

    move-object/from16 v4, v21

    invoke-virtual/range {v4 .. v10}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 1209757
    const/4 v3, 0x1

    move v10, v3

    .line 1209758
    :goto_6
    const-wide/32 v4, 0xf4240

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v4, v5}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v26

    .line 1209759
    if-ltz v26, :cond_14

    .line 1209760
    move-object/from16 v0, v23

    iget-wide v4, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/7T5;->c:J

    cmp-long v3, v4, v6

    if-ltz v3, :cond_13

    .line 1209761
    if-nez v11, :cond_1b

    .line 1209762
    const-string v3, "sample-rate"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v3

    const-string v4, "channel-count"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, LX/7T5;->f:I

    invoke-static {v3, v4, v5}, LX/7T5;->a(III)Landroid/media/MediaFormat;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 1209763
    const-string v3, "sample-rate"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v3

    const-string v4, "channel-count"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v25

    invoke-static {v3, v4, v0}, LX/7T5;->a(II[B)V

    .line 1209764
    invoke-virtual {v2}, Landroid/media/MediaCodec;->start()V

    .line 1209765
    invoke-virtual {v2}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v15

    .line 1209766
    invoke-virtual {v2}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result-object v14

    .line 1209767
    const/4 v9, 0x1

    .line 1209768
    :goto_7
    const-wide/32 v4, 0xf4240

    :try_start_4
    invoke-virtual {v2, v4, v5}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v3

    .line 1209769
    if-ltz v3, :cond_8

    .line 1209770
    aget-object v4, v15, v3

    .line 1209771
    aget-object v11, v13, v26

    .line 1209772
    move-object/from16 v0, v23

    iget v5, v0, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v11, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1209773
    move-object/from16 v0, v23

    iget v5, v0, Landroid/media/MediaCodec$BufferInfo;->offset:I

    move-object/from16 v0, v23

    iget v6, v0, Landroid/media/MediaCodec$BufferInfo;->size:I

    add-int/2addr v5, v6

    invoke-virtual {v11, v5}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 1209774
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1209775
    invoke-virtual {v4, v11}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 1209776
    const/4 v4, 0x0

    move-object/from16 v0, v23

    iget v5, v0, Landroid/media/MediaCodec$BufferInfo;->size:I

    move-object/from16 v0, v23

    iget-wide v6, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    move-object/from16 v0, v23

    iget v8, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    invoke-virtual/range {v2 .. v8}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 1209777
    move-object/from16 v0, v23

    iget v3, v0, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v11, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1209778
    move-object/from16 v0, v23

    iget v3, v0, Landroid/media/MediaCodec$BufferInfo;->offset:I

    move-object/from16 v0, v23

    iget v4, v0, Landroid/media/MediaCodec$BufferInfo;->size:I

    add-int/2addr v3, v4

    invoke-virtual {v11, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 1209779
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7T5;->d:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_7

    move-object/from16 v0, v23

    iget-wide v4, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/7T5;->d:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_7

    .line 1209780
    const/4 v10, 0x1

    .line 1209781
    :cond_7
    const-wide/16 v4, 0x0

    cmp-long v3, v18, v4

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, LX/7T5;->g:LX/60y;

    if-eqz v3, :cond_8

    .line 1209782
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7T5;->c:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gtz v3, :cond_d

    const-wide/16 v4, 0x0

    move-wide v6, v4

    .line 1209783
    :goto_8
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7T5;->d:J

    const-wide/16 v28, 0x0

    cmp-long v3, v4, v28

    if-gtz v3, :cond_e

    move-wide/from16 v4, v18

    .line 1209784
    :goto_9
    sub-long/2addr v4, v6

    .line 1209785
    const-wide/16 v28, 0x64

    move-object/from16 v0, v23

    iget-wide v0, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    move-wide/from16 v30, v0

    sub-long v6, v30, v6

    mul-long v6, v6, v28

    div-long v4, v6, v4

    long-to-int v3, v4

    .line 1209786
    if-eq v3, v12, :cond_8

    .line 1209787
    const/16 v4, 0x64

    if-ne v3, v4, :cond_f

    const/4 v4, 0x1

    :goto_a
    move-object/from16 v0, p0

    iput-boolean v4, v0, LX/7T5;->h:Z

    .line 1209788
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7T5;->g:LX/60y;

    int-to-double v6, v3

    invoke-interface {v4, v6, v7}, LX/60y;->a(D)V

    move v12, v3

    .line 1209789
    :cond_8
    const-wide/16 v4, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v2, v0, v4, v5}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v3

    .line 1209790
    :goto_b
    const/4 v4, -0x1

    if-eq v3, v4, :cond_1c

    .line 1209791
    if-ltz v3, :cond_10

    .line 1209792
    aget-object v4, v14, v3

    .line 1209793
    move-object/from16 v0, v24

    iget v5, v0, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1209794
    move-object/from16 v0, v24

    iget v5, v0, Landroid/media/MediaCodec$BufferInfo;->offset:I

    move-object/from16 v0, v24

    iget v6, v0, Landroid/media/MediaCodec$BufferInfo;->size:I

    add-int/2addr v5, v6

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 1209795
    move-object/from16 v0, v24

    iget v5, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v5, v5, 0x2

    if-nez v5, :cond_9

    .line 1209796
    move-object/from16 v0, v24

    iget v5, v0, Landroid/media/MediaCodec$BufferInfo;->size:I

    move-object/from16 v0, v24

    iget v6, v0, Landroid/media/MediaCodec$BufferInfo;->offset:I

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x7

    .line 1209797
    and-int/lit8 v6, v5, 0x7

    .line 1209798
    shr-int/lit8 v7, v5, 0x3

    and-int/lit16 v7, v7, 0xff

    .line 1209799
    shr-int/lit8 v5, v5, 0xb

    and-int/lit8 v5, v5, 0x3

    .line 1209800
    const/4 v8, 0x3

    const/4 v11, 0x3

    aget-byte v11, v25, v11

    and-int/lit16 v11, v11, 0xfc

    or-int/2addr v5, v11

    int-to-byte v5, v5

    aput-byte v5, v25, v8

    .line 1209801
    const/4 v5, 0x4

    int-to-byte v7, v7

    aput-byte v7, v25, v5

    .line 1209802
    const/4 v5, 0x5

    shl-int/lit8 v6, v6, 0x5

    or-int/lit8 v6, v6, 0x1f

    int-to-byte v6, v6

    aput-byte v6, v25, v5

    .line 1209803
    invoke-static/range {v25 .. v25}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 1209804
    move-object/from16 v0, v20

    invoke-interface {v0, v4}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 1209805
    :cond_9
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1209806
    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 1209807
    :cond_a
    :goto_c
    const-wide/16 v4, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v2, v0, v4, v5}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v3

    goto :goto_b

    .line 1209808
    :cond_b
    const/4 v6, 0x0

    :try_start_5
    invoke-virtual/range {p1 .. p1}, Landroid/media/MediaExtractor;->getSampleTime()J

    move-result-wide v8

    const/4 v10, 0x0

    move-object/from16 v4, v21

    invoke-virtual/range {v4 .. v10}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 1209809
    invoke-virtual/range {p1 .. p1}, Landroid/media/MediaExtractor;->advance()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :cond_c
    move v10, v3

    goto/16 :goto_6

    .line 1209810
    :cond_d
    :try_start_6
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7T5;->c:J

    move-wide v6, v4

    goto/16 :goto_8

    .line 1209811
    :cond_e
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7T5;->d:J

    goto/16 :goto_9

    .line 1209812
    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_a

    .line 1209813
    :cond_10
    const/4 v4, -0x3

    if-ne v3, v4, :cond_11

    .line 1209814
    invoke-virtual {v2}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v14

    goto :goto_c

    .line 1209815
    :cond_11
    const/4 v4, -0x2

    if-ne v3, v4, :cond_a

    .line 1209816
    invoke-virtual {v2}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_c

    .line 1209817
    :catchall_1
    move-exception v3

    move v4, v9

    :goto_d
    :try_start_7
    invoke-virtual/range {v21 .. v21}, Landroid/media/MediaCodec;->stop()V

    .line 1209818
    invoke-virtual/range {v21 .. v21}, Landroid/media/MediaCodec;->release()V

    .line 1209819
    if-eqz v4, :cond_12

    .line 1209820
    invoke-virtual {v2}, Landroid/media/MediaCodec;->stop()V

    .line 1209821
    :cond_12
    invoke-virtual {v2}, Landroid/media/MediaCodec;->release()V

    .line 1209822
    invoke-virtual/range {p1 .. p1}, Landroid/media/MediaExtractor;->release()V

    throw v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_13
    move v4, v11

    .line 1209823
    :goto_e
    const/4 v3, 0x0

    :try_start_8
    move-object/from16 v0, v21

    move/from16 v1, v26

    invoke-virtual {v0, v1, v3}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    move v3, v10

    move v5, v12

    move-object v6, v13

    move-object v7, v14

    move-object v8, v15

    move-object/from16 v9, v16

    .line 1209824
    :goto_f
    move-object/from16 v0, v23

    iget v10, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    and-int/lit8 v10, v10, 0x4

    if-nez v10, :cond_16

    move v11, v4

    move v12, v5

    move-object v13, v6

    move-object v14, v7

    move-object v15, v8

    move-object/from16 v16, v9

    .line 1209825
    goto/16 :goto_5

    .line 1209826
    :cond_14
    const/4 v3, -0x3

    move/from16 v0, v26

    if-ne v0, v3, :cond_15

    .line 1209827
    :try_start_9
    invoke-virtual/range {v21 .. v21}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v13

    move v3, v10

    move v4, v11

    move v5, v12

    move-object v6, v13

    move-object v7, v14

    move-object v8, v15

    move-object/from16 v9, v16

    .line 1209828
    goto :goto_f

    .line 1209829
    :cond_15
    const/4 v3, -0x2

    move/from16 v0, v26

    if-ne v0, v3, :cond_1a

    .line 1209830
    invoke-virtual/range {v21 .. v21}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    move-result-object v16

    move v3, v10

    move v4, v11

    move v5, v12

    move-object v6, v13

    move-object v7, v14

    move-object v8, v15

    move-object/from16 v9, v16

    goto :goto_f

    :cond_16
    move v11, v4

    .line 1209831
    :cond_17
    :try_start_a
    invoke-virtual/range {v21 .. v21}, Landroid/media/MediaCodec;->stop()V

    .line 1209832
    invoke-virtual/range {v21 .. v21}, Landroid/media/MediaCodec;->release()V

    .line 1209833
    if-eqz v11, :cond_18

    .line 1209834
    invoke-virtual {v2}, Landroid/media/MediaCodec;->stop()V

    .line 1209835
    :cond_18
    invoke-virtual {v2}, Landroid/media/MediaCodec;->release()V

    .line 1209836
    invoke-virtual/range {p1 .. p1}, Landroid/media/MediaExtractor;->release()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1209837
    :try_start_b
    invoke-interface/range {v20 .. v20}, Ljava/nio/channels/WritableByteChannel;->close()V

    .line 1209838
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0

    .line 1209839
    :goto_10
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/7T5;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7T5;->e:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7T5;->e:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    .line 1209840
    move-object/from16 v0, p0

    iget-object v2, v0, LX/7T5;->e:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_19

    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1209841
    :catch_0
    move-exception v2

    .line 1209842
    sget-object v3, LX/7T5;->a:Ljava/lang/Class;

    const-string v4, "Failed to close streams."

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_10

    .line 1209843
    :catch_1
    move-exception v3

    .line 1209844
    sget-object v4, LX/7T5;->a:Ljava/lang/Class;

    const-string v5, "Failed to close streams."

    invoke-static {v4, v5, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4

    .line 1209845
    :cond_19
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1209846
    :catchall_2
    move-exception v3

    goto/16 :goto_d

    :catchall_3
    move-exception v3

    move v4, v11

    goto/16 :goto_d

    :cond_1a
    move v3, v10

    move v4, v11

    move v5, v12

    move-object v6, v13

    move-object v7, v14

    move-object v8, v15

    move-object/from16 v9, v16

    goto/16 :goto_f

    :cond_1b
    move v9, v11

    goto/16 :goto_7

    :cond_1c
    move v4, v9

    goto/16 :goto_e

    :cond_1d
    move-object v2, v3

    goto/16 :goto_3
.end method
