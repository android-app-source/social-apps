.class public final LX/7tj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 1268659
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_a

    .line 1268660
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1268661
    :goto_0
    return v1

    .line 1268662
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1268663
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 1268664
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1268665
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1268666
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 1268667
    const-string v9, "__type__"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    const-string v9, "__typename"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1268668
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    goto :goto_1

    .line 1268669
    :cond_3
    const-string v9, "action_links"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1268670
    invoke-static {p0, p1}, LX/7ti;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1268671
    :cond_4
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1268672
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1268673
    :cond_5
    const-string v9, "image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1268674
    const/4 v8, 0x0

    .line 1268675
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v9, :cond_e

    .line 1268676
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1268677
    :goto_2
    move v4, v8

    .line 1268678
    goto :goto_1

    .line 1268679
    :cond_6
    const-string v9, "style"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1268680
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLEventPromotionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventPromotionStyle;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_1

    .line 1268681
    :cond_7
    const-string v9, "subtitle"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 1268682
    const/4 v8, 0x0

    .line 1268683
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v9, :cond_12

    .line 1268684
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1268685
    :goto_3
    move v2, v8

    .line 1268686
    goto/16 :goto_1

    .line 1268687
    :cond_8
    const-string v9, "title"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1268688
    const/4 v8, 0x0

    .line 1268689
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v9, :cond_16

    .line 1268690
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1268691
    :goto_4
    move v0, v8

    .line 1268692
    goto/16 :goto_1

    .line 1268693
    :cond_9
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1268694
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1268695
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1268696
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1268697
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1268698
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1268699
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1268700
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1268701
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1

    .line 1268702
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1268703
    :cond_c
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_d

    .line 1268704
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1268705
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1268706
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_c

    if-eqz v9, :cond_c

    .line 1268707
    const-string v10, "uri"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 1268708
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_5

    .line 1268709
    :cond_d
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1268710
    invoke-virtual {p1, v8, v4}, LX/186;->b(II)V

    .line 1268711
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_2

    :cond_e
    move v4, v8

    goto :goto_5

    .line 1268712
    :cond_f
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1268713
    :cond_10
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_11

    .line 1268714
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1268715
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1268716
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_10

    if-eqz v9, :cond_10

    .line 1268717
    const-string v10, "text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 1268718
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_6

    .line 1268719
    :cond_11
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1268720
    invoke-virtual {p1, v8, v2}, LX/186;->b(II)V

    .line 1268721
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_3

    :cond_12
    move v2, v8

    goto :goto_6

    .line 1268722
    :cond_13
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1268723
    :cond_14
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_15

    .line 1268724
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1268725
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1268726
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_14

    if-eqz v9, :cond_14

    .line 1268727
    const-string v10, "text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_13

    .line 1268728
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_7

    .line 1268729
    :cond_15
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1268730
    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 1268731
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_4

    :cond_16
    move v0, v8

    goto :goto_7
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 11

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 1268732
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1268733
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1268734
    if-eqz v0, :cond_0

    .line 1268735
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268736
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1268737
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1268738
    if-eqz v0, :cond_8

    .line 1268739
    const-string v1, "action_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268740
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1268741
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_7

    .line 1268742
    invoke-virtual {p0, v0, v3}, LX/15i;->q(II)I

    move-result v4

    const/4 v6, 0x0

    .line 1268743
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1268744
    invoke-virtual {p0, v4, v6}, LX/15i;->g(II)I

    move-result v5

    .line 1268745
    if-eqz v5, :cond_1

    .line 1268746
    const-string v5, "__type__"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268747
    invoke-static {p0, v4, v6, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1268748
    :cond_1
    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5}, LX/15i;->g(II)I

    move-result v5

    .line 1268749
    if-eqz v5, :cond_5

    .line 1268750
    const-string v6, "temporal_event_info"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268751
    const-wide/16 v9, 0x0

    .line 1268752
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1268753
    const/4 v7, 0x0

    invoke-virtual {p0, v5, v7, v9, v10}, LX/15i;->a(IIJ)J

    move-result-wide v7

    .line 1268754
    cmp-long v9, v7, v9

    if-eqz v9, :cond_2

    .line 1268755
    const-string v9, "start_time"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268756
    invoke-virtual {p2, v7, v8}, LX/0nX;->a(J)V

    .line 1268757
    :cond_2
    const/4 v7, 0x1

    invoke-virtual {p0, v5, v7}, LX/15i;->g(II)I

    move-result v7

    .line 1268758
    if-eqz v7, :cond_3

    .line 1268759
    const-string v8, "theme"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268760
    invoke-static {p0, v7, p2, p3}, LX/7tg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1268761
    :cond_3
    const/4 v7, 0x2

    invoke-virtual {p0, v5, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v7

    .line 1268762
    if-eqz v7, :cond_4

    .line 1268763
    const-string v8, "title"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268764
    invoke-virtual {p2, v7}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1268765
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1268766
    :cond_5
    const/4 v5, 0x2

    invoke-virtual {p0, v4, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1268767
    if-eqz v5, :cond_6

    .line 1268768
    const-string v6, "title"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268769
    invoke-virtual {p2, v5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1268770
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1268771
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1268772
    :cond_7
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1268773
    :cond_8
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1268774
    if-eqz v0, :cond_9

    .line 1268775
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268776
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1268777
    :cond_9
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1268778
    if-eqz v0, :cond_b

    .line 1268779
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268780
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1268781
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1268782
    if-eqz v1, :cond_a

    .line 1268783
    const-string v3, "uri"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268784
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1268785
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1268786
    :cond_b
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1268787
    if-eqz v0, :cond_c

    .line 1268788
    const-string v0, "style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268789
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1268790
    :cond_c
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1268791
    if-eqz v0, :cond_e

    .line 1268792
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268793
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1268794
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1268795
    if-eqz v1, :cond_d

    .line 1268796
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268797
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1268798
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1268799
    :cond_e
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1268800
    if-eqz v0, :cond_10

    .line 1268801
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268802
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1268803
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1268804
    if-eqz v1, :cond_f

    .line 1268805
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1268806
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1268807
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1268808
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1268809
    return-void
.end method
