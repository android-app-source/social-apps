.class public final LX/8RG;
.super LX/8RF;
.source ""


# instance fields
.field public final a:LX/8RD;


# direct methods
.method public constructor <init>(LX/8RD;)V
    .locals 1

    .prologue
    .line 1344418
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/8RF;-><init>(Z)V

    .line 1344419
    iput-object p1, p0, LX/8RG;->a:LX/8RD;

    .line 1344420
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1344425
    new-instance v0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->a()Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;

    move-result-object v0

    .line 1344426
    const v1, 0x7f0d0c4d

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1344427
    iget-object v1, p0, LX/8RG;->a:LX/8RD;

    sget-object v2, LX/8RD;->FRIENDS_EXCEPT:LX/8RD;

    if-ne v1, v2, :cond_0

    .line 1344428
    const v1, 0x7f020685

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->setCheckboxBackground(I)V

    .line 1344429
    :cond_0
    return-object v0
.end method

.method public final d(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1344421
    invoke-super {p0, p1}, LX/8RF;->d(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1344422
    const v1, 0x7f0d129e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1344423
    const v1, 0x7f0d0c4d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1344424
    return-object v0
.end method
