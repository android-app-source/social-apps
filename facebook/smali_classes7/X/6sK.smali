.class public LX/6sK;
.super LX/48b;
.source ""


# instance fields
.field private final a:LX/6sL;


# direct methods
.method public constructor <init>(LX/6sL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1153080
    invoke-direct {p0}, LX/48b;-><init>()V

    .line 1153081
    iput-object p1, p0, LX/6sK;->a:LX/6sL;

    .line 1153082
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153069
    const-string v0, "product_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1153070
    if-nez v0, :cond_0

    .line 1153071
    const/4 v0, 0x0

    .line 1153072
    :goto_0
    return-object v0

    .line 1153073
    :cond_0
    invoke-static {v0}, LX/6xh;->forValue(Ljava/lang/String;)LX/6xh;

    move-result-object v0

    .line 1153074
    iget-object v1, p0, LX/6sK;->a:LX/6sL;

    .line 1153075
    iget-object v2, v1, LX/6sL;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6sM;

    .line 1153076
    sget-object p1, LX/6xh;->MOCK:LX/6xh;

    move-object p1, p1

    .line 1153077
    if-ne p1, v0, :cond_1

    .line 1153078
    :goto_1
    const/4 v1, 0x0

    move-object v0, v1

    .line 1153079
    goto :goto_0

    :cond_2
    goto :goto_1
.end method
