.class public LX/78y;
.super Landroid/widget/EditText;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/text/Spannable;

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1173719
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1173720
    invoke-direct {p0}, LX/78y;->a()V

    .line 1173721
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1173716
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1173717
    invoke-direct {p0}, LX/78y;->a()V

    .line 1173718
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1173713
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1173714
    invoke-direct {p0}, LX/78y;->a()V

    .line 1173715
    return-void
.end method

.method public static synthetic a(LX/78y;Ljava/lang/String;)Landroid/text/Spannable;
    .locals 1

    .prologue
    .line 1173679
    invoke-direct {p0, p1}, LX/78y;->a(Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Landroid/text/Spannable;
    .locals 6

    .prologue
    const/16 v5, 0x21

    .line 1173707
    iget-object v0, p0, LX/78y;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1173708
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1173709
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1173710
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, LX/78y;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a05b5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1173711
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, LX/78y;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a05b1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget-object v3, p0, LX/78y;->a:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v0

    invoke-interface {v1, v2, v0, v3, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 1173712
    :cond_0
    return-object v1
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1173704
    new-instance v0, LX/78x;

    invoke-direct {v0, p0}, LX/78x;-><init>(LX/78y;)V

    invoke-virtual {p0, v0}, LX/78y;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1173705
    invoke-direct {p0}, LX/78y;->b()V

    .line 1173706
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 1173700
    iget-boolean v0, p0, LX/78y;->c:Z

    if-eqz v0, :cond_0

    .line 1173701
    invoke-virtual {p0}, LX/78y;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081943

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/78y;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1173702
    invoke-direct {p0, v0}, LX/78y;->a(Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/78y;->setHint(Ljava/lang/CharSequence;)V

    .line 1173703
    :cond_0
    return-void
.end method

.method private getEndIndex()I
    .locals 2

    .prologue
    .line 1173696
    invoke-virtual {p0}, LX/78y;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 1173697
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/78y;->b:Landroid/text/Spannable;

    if-nez v1, :cond_1

    .line 1173698
    :cond_0
    const/4 v0, -0x1

    .line 1173699
    :goto_0
    return v0

    :cond_1
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v1, p0, LX/78y;->b:Landroid/text/Spannable;

    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public getWrittenDescription()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1173691
    invoke-virtual {p0}, LX/78y;->getText()Landroid/text/Editable;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    .line 1173692
    :goto_0
    iget-object v1, p0, LX/78y;->b:Landroid/text/Spannable;

    if-eqz v1, :cond_0

    .line 1173693
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, LX/78y;->b:Landroid/text/Spannable;

    invoke-interface {v3}, Landroid/text/Spannable;->length()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1173694
    :cond_0
    return-object v0

    .line 1173695
    :cond_1
    invoke-virtual {p0}, LX/78y;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onSelectionChanged(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1173684
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onSelectionChanged(II)V

    .line 1173685
    invoke-direct {p0}, LX/78y;->getEndIndex()I

    move-result v0

    .line 1173686
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1173687
    :goto_0
    return-void

    .line 1173688
    :cond_0
    invoke-static {p1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0}, LX/78y;->getEndIndex()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1173689
    invoke-direct {p0}, LX/78y;->getEndIndex()I

    move-result v1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1173690
    invoke-virtual {p0, v0, v1}, LX/78y;->setSelection(II)V

    goto :goto_0
.end method

.method public setCategoryDescription(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1173680
    iput-object p1, p0, LX/78y;->a:Ljava/lang/String;

    .line 1173681
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/78y;->c:Z

    .line 1173682
    invoke-direct {p0}, LX/78y;->b()V

    .line 1173683
    return-void
.end method
