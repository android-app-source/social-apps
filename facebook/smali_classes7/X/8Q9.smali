.class public final LX/8Q9;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0jT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)V
    .locals 0

    .prologue
    .line 1342572
    iput-object p1, p0, LX/8Q9;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1342586
    iget-object v0, p0, LX/8Q9;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->s:Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    .line 1342587
    iget-object v0, p0, LX/8Q9;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, LX/8Q9;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v1, v1, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->r:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 1342588
    :cond_0
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 1342589
    invoke-direct {p0}, LX/8Q9;->a()V

    .line 1342590
    iget-object v0, p0, LX/8Q9;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->e:LX/2c9;

    sget-object v1, LX/2Zv;->EDIT_STORY_PRIVACY_SAVE_FAILED:LX/2Zv;

    invoke-virtual {v0, v1}, LX/2c9;->a(LX/2Zv;)V

    .line 1342591
    iget-object v0, p0, LX/8Q9;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->f:LX/8Pp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/8Pp;->a(Z)V

    .line 1342592
    iget-object v0, p0, LX/8Q9;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v1, p0, LX/8Q9;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v1, v1, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1342593
    iget-object v2, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->c:LX/03V;

    const-string v3, "edit_privacy_fragment_set_api_error"

    const-string v4, "Error setting story privacy %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object v1, v5, p0

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1342594
    iget-object v2, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->j:LX/0kL;

    new-instance v3, LX/27k;

    const v4, 0x7f08130e

    invoke-direct {v3, v4}, LX/27k;-><init>(I)V

    invoke-virtual {v2, v3}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1342595
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1342573
    check-cast p1, LX/0jT;

    .line 1342574
    invoke-direct {p0}, LX/8Q9;->a()V

    .line 1342575
    iget-object v0, p0, LX/8Q9;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->e:LX/2c9;

    sget-object v1, LX/2Zv;->EDIT_STORY_PRIVACY_SAVED:LX/2Zv;

    invoke-virtual {v0, v1}, LX/2c9;->a(LX/2Zv;)V

    .line 1342576
    iget-object v0, p0, LX/8Q9;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->f:LX/8Pp;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/8Pp;->a(Z)V

    .line 1342577
    iget-object v0, p0, LX/8Q9;->a:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    .line 1342578
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1342579
    check-cast p1, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel;

    invoke-virtual {p1}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyCoreMutationFieldsModel;->a()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel;->j()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;

    move-result-object v2

    .line 1342580
    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    .line 1342581
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;->a()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;->a()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1342582
    const-string v1, "privacy_option_to_upsell"

    invoke-virtual {v2}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel;->a()Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/privacy/protocol/PrivacyMutationsModels$SetStoryPrivacyStoryFieldsModel$PrivacyScopeModel$EducationInfoModel;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    invoke-static {p0, v1, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1342583
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    const/4 v1, -0x1

    invoke-virtual {v2, v1, p0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1342584
    invoke-static {v0}, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->s(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)V

    .line 1342585
    :cond_1
    return-void
.end method
