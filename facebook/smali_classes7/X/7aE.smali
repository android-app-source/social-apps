.class public final LX/7aE;
.super LX/7a7;
.source ""


# instance fields
.field private a:LX/2wh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2wh",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2wh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wh",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, LX/7a7;-><init>()V

    iput-object p1, p0, LX/7aE;->a:LX/2wh;

    return-void
.end method


# virtual methods
.method public final a(ILandroid/app/PendingIntent;)V
    .locals 2

    const-string v0, "LocationClientImpl"

    const-string v1, "Unexpected call to onRemoveGeofencesByPendingIntentResult"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public final a(I[Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, LX/7aE;->a:LX/2wh;

    if-nez v0, :cond_0

    const-string v0, "LocationClientImpl"

    const-string v1, "onAddGeofenceResult called multiple times"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    if-ltz p1, :cond_1

    if-le p1, v0, :cond_2

    :cond_1
    const/16 v1, 0x3e8

    if-gt v1, p1, :cond_3

    const/16 v1, 0x3ea

    if-gt p1, v1, :cond_3

    :cond_2
    :goto_1
    move v0, p1

    packed-switch v0, :pswitch_data_0

    :goto_2
    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    move-object v0, v1

    iget-object v1, p0, LX/7aE;->a:LX/2wh;

    invoke-interface {v1, v0}, LX/2wh;->a(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, LX/7aE;->a:LX/2wh;

    goto :goto_0

    :cond_3
    move p1, v0

    goto :goto_1

    :pswitch_0
    const/16 v0, 0xd

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(I[Ljava/lang/String;)V
    .locals 2

    const-string v0, "LocationClientImpl"

    const-string v1, "Unexpected call to onRemoveGeofencesByRequestIdsResult"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
