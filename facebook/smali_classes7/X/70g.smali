.class public LX/70g;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1162525
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/70g;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1162526
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1162527
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1162528
    invoke-direct {p0}, LX/70g;->a()V

    .line 1162529
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1162530
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1162531
    invoke-direct {p0}, LX/70g;->a()V

    .line 1162532
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1162533
    const v0, 0x7f03023d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1162534
    const v0, 0x7f0d08a4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/70g;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1162535
    return-void
.end method


# virtual methods
.method public setText(I)V
    .locals 1

    .prologue
    .line 1162536
    iget-object v0, p0, LX/70g;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1162537
    return-void
.end method
