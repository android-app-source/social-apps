.class public final LX/8I6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1322792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/186;LX/1Fb;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1322783
    if-nez p1, :cond_0

    .line 1322784
    :goto_0
    return v0

    .line 1322785
    :cond_0
    invoke-interface {p1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1322786
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1322787
    invoke-interface {p1}, LX/1Fb;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2, v0}, LX/186;->a(III)V

    .line 1322788
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, LX/186;->b(II)V

    .line 1322789
    const/4 v1, 0x2

    invoke-interface {p1}, LX/1Fb;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 1322790
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1322791
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;LX/1VU;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1322751
    if-nez p1, :cond_0

    .line 1322752
    :goto_0
    return v0

    .line 1322753
    :cond_0
    invoke-interface {p1}, LX/1VU;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1322754
    invoke-interface {p1}, LX/1VU;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1322755
    invoke-interface {p1}, LX/1VU;->l()LX/17A;

    move-result-object v3

    const/4 v4, 0x0

    .line 1322756
    if-nez v3, :cond_1

    .line 1322757
    :goto_1
    move v3, v4

    .line 1322758
    invoke-interface {p1}, LX/1VU;->m()LX/172;

    move-result-object v4

    const/4 v5, 0x0

    .line 1322759
    if-nez v4, :cond_2

    .line 1322760
    :goto_2
    move v4, v5

    .line 1322761
    const/16 v5, 0xa

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1322762
    invoke-interface {p1}, LX/1VU;->b()Z

    move-result v5

    invoke-virtual {p0, v0, v5}, LX/186;->a(IZ)V

    .line 1322763
    const/4 v0, 0x1

    invoke-interface {p1}, LX/1VU;->c()Z

    move-result v5

    invoke-virtual {p0, v0, v5}, LX/186;->a(IZ)V

    .line 1322764
    const/4 v0, 0x2

    invoke-interface {p1}, LX/1VU;->d()Z

    move-result v5

    invoke-virtual {p0, v0, v5}, LX/186;->a(IZ)V

    .line 1322765
    const/4 v0, 0x3

    invoke-interface {p1}, LX/1VU;->e()Z

    move-result v5

    invoke-virtual {p0, v0, v5}, LX/186;->a(IZ)V

    .line 1322766
    const/4 v0, 0x4

    invoke-interface {p1}, LX/1VU;->r_()Z

    move-result v5

    invoke-virtual {p0, v0, v5}, LX/186;->a(IZ)V

    .line 1322767
    const/4 v0, 0x5

    invoke-interface {p1}, LX/1VU;->s_()Z

    move-result v5

    invoke-virtual {p0, v0, v5}, LX/186;->a(IZ)V

    .line 1322768
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1322769
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1322770
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1322771
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1322772
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1322773
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1322774
    :cond_1
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1322775
    invoke-interface {v3}, LX/17A;->a()I

    move-result v5

    invoke-virtual {p0, v4, v5, v4}, LX/186;->a(III)V

    .line 1322776
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 1322777
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto :goto_1

    .line 1322778
    :cond_2
    const/4 v6, 0x2

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 1322779
    invoke-interface {v4}, LX/172;->a()I

    move-result v6

    invoke-virtual {p0, v5, v6, v5}, LX/186;->a(III)V

    .line 1322780
    const/4 v6, 0x1

    invoke-interface {v4}, LX/172;->b()I

    move-result v7

    invoke-virtual {p0, v6, v7, v5}, LX/186;->a(III)V

    .line 1322781
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 1322782
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;LX/1f8;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 1322744
    if-nez p1, :cond_0

    .line 1322745
    :goto_0
    return v1

    .line 1322746
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 1322747
    invoke-interface {p1}, LX/1f8;->a()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1322748
    const/4 v1, 0x1

    invoke-interface {p1}, LX/1f8;->b()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1322749
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1322750
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;LX/5kD;)I
    .locals 18

    .prologue
    .line 1322701
    if-nez p1, :cond_0

    .line 1322702
    const/4 v2, 0x0

    .line 1322703
    :goto_0
    return v2

    .line 1322704
    :cond_0
    invoke-interface/range {p1 .. p1}, LX/5kD;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    .line 1322705
    invoke-interface/range {p1 .. p1}, LX/5kD;->k()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1322706
    invoke-interface/range {p1 .. p1}, LX/5kD;->C()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/8I6;->a(LX/186;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;)I

    move-result v6

    .line 1322707
    invoke-interface/range {p1 .. p1}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/8I6;->a(LX/186;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)I

    move-result v7

    .line 1322708
    invoke-interface/range {p1 .. p1}, LX/5kD;->c()LX/1f8;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/8I6;->a(LX/186;LX/1f8;)I

    move-result v8

    .line 1322709
    invoke-interface/range {p1 .. p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1322710
    invoke-interface/range {p1 .. p1}, LX/5kD;->e()LX/1Fb;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v10

    .line 1322711
    invoke-interface/range {p1 .. p1}, LX/5kD;->aj_()LX/1Fb;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v11

    .line 1322712
    invoke-interface/range {p1 .. p1}, LX/5kD;->ai_()LX/1Fb;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v12

    .line 1322713
    invoke-interface/range {p1 .. p1}, LX/5kD;->j()LX/1Fb;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v13

    .line 1322714
    invoke-interface/range {p1 .. p1}, LX/5kD;->M()LX/1Fb;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v14

    .line 1322715
    invoke-interface/range {p1 .. p1}, LX/5kD;->U()LX/1Fb;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v15

    .line 1322716
    const/4 v2, 0x0

    .line 1322717
    invoke-interface/range {p1 .. p1}, LX/5kD;->ab()LX/0Px;

    move-result-object v16

    .line 1322718
    if-eqz v16, :cond_2

    .line 1322719
    invoke-virtual/range {v16 .. v16}, LX/0Px;->size()I

    move-result v2

    new-array v0, v2, [I

    move-object/from16 v17, v0

    .line 1322720
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual/range {v16 .. v16}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    .line 1322721
    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/8I6;->a(LX/186;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel;)I

    move-result v2

    aput v2, v17, v3

    .line 1322722
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1322723
    :cond_1
    const/4 v2, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, LX/186;->a([IZ)I

    move-result v2

    .line 1322724
    :cond_2
    invoke-interface/range {p1 .. p1}, LX/5kD;->ad()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1322725
    const/16 v16, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1322726
    const/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1322727
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 1322728
    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 1322729
    const/4 v4, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 1322730
    const/4 v4, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 1322731
    const/4 v4, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 1322732
    const/4 v4, 0x7

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 1322733
    const/16 v4, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 1322734
    const/16 v4, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 1322735
    const/16 v4, 0xa

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 1322736
    const/16 v4, 0xb

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 1322737
    const/16 v4, 0xd

    invoke-interface/range {p1 .. p1}, LX/5kD;->S()Z

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1322738
    const/16 v4, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 1322739
    const/16 v4, 0x14

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v2}, LX/186;->b(II)V

    .line 1322740
    const/16 v2, 0x15

    invoke-interface/range {p1 .. p1}, LX/5kD;->ac()I

    move-result v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4, v5}, LX/186;->a(III)V

    .line 1322741
    const/16 v2, 0x16

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1322742
    invoke-virtual/range {p0 .. p0}, LX/186;->d()I

    move-result v2

    .line 1322743
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->d(I)V

    goto/16 :goto_0
.end method

.method private static a(LX/186;LX/8IG;)I
    .locals 19

    .prologue
    .line 1322092
    if-nez p1, :cond_0

    .line 1322093
    const/4 v2, 0x0

    .line 1322094
    :goto_0
    return v2

    .line 1322095
    :cond_0
    invoke-interface/range {p1 .. p1}, LX/8IG;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    .line 1322096
    invoke-interface/range {p1 .. p1}, LX/8IG;->m()LX/1VU;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/8I6;->a(LX/186;LX/1VU;)I

    move-result v3

    .line 1322097
    invoke-interface/range {p1 .. p1}, LX/8IG;->c()LX/1f8;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/8I6;->a(LX/186;LX/1f8;)I

    move-result v4

    .line 1322098
    invoke-interface/range {p1 .. p1}, LX/8IG;->d()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1322099
    invoke-interface/range {p1 .. p1}, LX/8IG;->e()LX/1Fb;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v6

    .line 1322100
    invoke-interface/range {p1 .. p1}, LX/8IG;->aj_()LX/1Fb;

    move-result-object v7

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v7

    .line 1322101
    invoke-interface/range {p1 .. p1}, LX/8IG;->ai_()LX/1Fb;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v8

    .line 1322102
    invoke-interface/range {p1 .. p1}, LX/8IG;->j()LX/1Fb;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v9

    .line 1322103
    invoke-interface/range {p1 .. p1}, LX/8IG;->o()LX/1Fb;

    move-result-object v10

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v10

    .line 1322104
    invoke-interface/range {p1 .. p1}, LX/8IG;->r()LX/1Fb;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v11

    .line 1322105
    invoke-interface/range {p1 .. p1}, LX/8IG;->s()LX/1Fb;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v12

    .line 1322106
    invoke-interface/range {p1 .. p1}, LX/8IG;->t()LX/1Fb;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v13

    .line 1322107
    invoke-interface/range {p1 .. p1}, LX/8IG;->v()LX/1Fb;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v14

    .line 1322108
    invoke-interface/range {p1 .. p1}, LX/8IG;->w()LX/1Fb;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v15

    .line 1322109
    invoke-interface/range {p1 .. p1}, LX/8IG;->A()LX/1Fb;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v16

    .line 1322110
    invoke-interface/range {p1 .. p1}, LX/8IG;->B()LX/1Fb;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/8I6;->a(LX/186;LX/1Fb;)I

    move-result v17

    .line 1322111
    const/16 v18, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1322112
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1322113
    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1322114
    const/4 v2, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1322115
    const/4 v2, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1322116
    const/4 v2, 0x7

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1322117
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1322118
    const/16 v2, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1322119
    const/16 v2, 0xa

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1322120
    const/16 v2, 0xb

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1322121
    const/16 v2, 0xe

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1322122
    const/16 v2, 0xf

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1322123
    const/16 v2, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1322124
    const/16 v2, 0x12

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1322125
    const/16 v2, 0x13

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1322126
    const/16 v2, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1322127
    const/16 v2, 0x18

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1322128
    invoke-virtual/range {p0 .. p0}, LX/186;->d()I

    move-result v2

    .line 1322129
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->d(I)V

    goto/16 :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1322677
    if-nez p1, :cond_0

    .line 1322678
    :goto_0
    return v0

    .line 1322679
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;->c()LX/0Px;

    move-result-object v2

    .line 1322680
    if-eqz v2, :cond_2

    .line 1322681
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v1

    new-array v3, v1, [I

    move v1, v0

    .line 1322682
    :goto_1
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1322683
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel$AttachmentsModel;

    .line 1322684
    if-nez v0, :cond_3

    .line 1322685
    const/4 v5, 0x0

    .line 1322686
    :goto_2
    move v0, v5

    .line 1322687
    aput v0, v3, v1

    .line 1322688
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1322689
    :cond_1
    invoke-virtual {p0, v3, v4}, LX/186;->a([IZ)I

    move-result v0

    .line 1322690
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1322691
    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1322692
    const/4 v3, 0x7

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1322693
    invoke-virtual {p0, v4, v0}, LX/186;->b(II)V

    .line 1322694
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1322695
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1322696
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1322697
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1322698
    :cond_3
    const/4 v5, 0x6

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1322699
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 1322700
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1322793
    if-nez p1, :cond_0

    .line 1322794
    :goto_0
    return v0

    .line 1322795
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$Photo360EncodingFieldsModel$PhotoEncodingsModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1322796
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1322797
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1322798
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1322799
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1322645
    if-nez p1, :cond_0

    .line 1322646
    :goto_0
    return v0

    .line 1322647
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1322648
    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1322649
    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->p()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel$LikersModel;

    move-result-object v3

    const/4 v4, 0x0

    .line 1322650
    if-nez v3, :cond_1

    .line 1322651
    :goto_1
    move v3, v4

    .line 1322652
    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->t()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel$TopLevelCommentsModel;

    move-result-object v4

    const/4 v5, 0x0

    .line 1322653
    if-nez v4, :cond_2

    .line 1322654
    :goto_2
    move v4, v5

    .line 1322655
    const/16 v5, 0xa

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1322656
    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->b()Z

    move-result v5

    invoke-virtual {p0, v0, v5}, LX/186;->a(IZ)V

    .line 1322657
    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->c()Z

    move-result v5

    invoke-virtual {p0, v0, v5}, LX/186;->a(IZ)V

    .line 1322658
    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->d()Z

    move-result v5

    invoke-virtual {p0, v0, v5}, LX/186;->a(IZ)V

    .line 1322659
    const/4 v0, 0x3

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->e()Z

    move-result v5

    invoke-virtual {p0, v0, v5}, LX/186;->a(IZ)V

    .line 1322660
    const/4 v0, 0x4

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->aC_()Z

    move-result v5

    invoke-virtual {p0, v0, v5}, LX/186;->a(IZ)V

    .line 1322661
    const/4 v0, 0x5

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->k()Z

    move-result v5

    invoke-virtual {p0, v0, v5}, LX/186;->a(IZ)V

    .line 1322662
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1322663
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1322664
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1322665
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1322666
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1322667
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1322668
    :cond_1
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1322669
    invoke-virtual {v3}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel$LikersModel;->a()I

    move-result v5

    invoke-virtual {p0, v4, v5, v4}, LX/186;->a(III)V

    .line 1322670
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 1322671
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto :goto_1

    .line 1322672
    :cond_2
    const/4 v6, 0x2

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 1322673
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v6

    invoke-virtual {p0, v5, v6, v5}, LX/186;->a(III)V

    .line 1322674
    const/4 v6, 0x1

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel$TopLevelCommentsModel;->b()I

    move-result v7

    invoke-virtual {p0, v6, v7, v5}, LX/186;->a(III)V

    .line 1322675
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 1322676
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto :goto_2
.end method

.method public static a(LX/5kD;)LX/8IH;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1322633
    if-nez p0, :cond_1

    .line 1322634
    :cond_0
    :goto_0
    return-object v2

    .line 1322635
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1322636
    invoke-static {v0, p0}, LX/8I6;->a(LX/186;LX/5kD;)I

    move-result v1

    .line 1322637
    if-eqz v1, :cond_0

    .line 1322638
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1322639
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1322640
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1322641
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1322642
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1322643
    const-string v1, "PandoraModelConversionHelper.getPandoraMedia"

    check-cast p0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1322644
    :cond_2
    new-instance v2, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-direct {v2, v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;-><init>(LX/15i;)V

    goto :goto_0
.end method

.method public static a(LX/8IG;)LX/8IH;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1322621
    if-nez p0, :cond_1

    .line 1322622
    :cond_0
    :goto_0
    return-object v2

    .line 1322623
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1322624
    invoke-static {v0, p0}, LX/8I6;->a(LX/186;LX/8IG;)I

    move-result v1

    .line 1322625
    if-eqz v1, :cond_0

    .line 1322626
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1322627
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1322628
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1322629
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1322630
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1322631
    const-string v1, "PandoraModelConversionHelper.getPandoraMedia"

    check-cast p0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1322632
    :cond_2
    new-instance v2, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-direct {v2, v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;-><init>(LX/15i;)V

    goto :goto_0
.end method

.method public static a(LX/1VU;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 4

    .prologue
    .line 1322583
    if-nez p0, :cond_0

    .line 1322584
    const/4 v0, 0x0

    .line 1322585
    :goto_0
    return-object v0

    .line 1322586
    :cond_0
    new-instance v0, LX/3dM;

    invoke-direct {v0}, LX/3dM;-><init>()V

    .line 1322587
    invoke-interface {p0}, LX/1VU;->b()Z

    move-result v1

    .line 1322588
    iput-boolean v1, v0, LX/3dM;->e:Z

    .line 1322589
    invoke-interface {p0}, LX/1VU;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/3dM;->c(Z)LX/3dM;

    .line 1322590
    invoke-interface {p0}, LX/1VU;->d()Z

    move-result v1

    .line 1322591
    iput-boolean v1, v0, LX/3dM;->g:Z

    .line 1322592
    invoke-interface {p0}, LX/1VU;->e()Z

    move-result v1

    .line 1322593
    iput-boolean v1, v0, LX/3dM;->i:Z

    .line 1322594
    invoke-interface {p0}, LX/1VU;->r_()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/3dM;->g(Z)LX/3dM;

    .line 1322595
    invoke-interface {p0}, LX/1VU;->s_()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/3dM;->j(Z)LX/3dM;

    .line 1322596
    invoke-interface {p0}, LX/1VU;->j()Ljava/lang/String;

    move-result-object v1

    .line 1322597
    iput-object v1, v0, LX/3dM;->y:Ljava/lang/String;

    .line 1322598
    invoke-interface {p0}, LX/1VU;->k()Ljava/lang/String;

    move-result-object v1

    .line 1322599
    iput-object v1, v0, LX/3dM;->D:Ljava/lang/String;

    .line 1322600
    invoke-interface {p0}, LX/1VU;->l()LX/17A;

    move-result-object v1

    .line 1322601
    if-nez v1, :cond_1

    .line 1322602
    const/4 v2, 0x0

    .line 1322603
    :goto_1
    move-object v1, v2

    .line 1322604
    invoke-virtual {v0, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 1322605
    invoke-interface {p0}, LX/1VU;->m()LX/172;

    move-result-object v1

    .line 1322606
    if-nez v1, :cond_2

    .line 1322607
    const/4 v2, 0x0

    .line 1322608
    :goto_2
    move-object v1, v2

    .line 1322609
    invoke-virtual {v0, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    .line 1322610
    invoke-virtual {v0}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto :goto_0

    .line 1322611
    :cond_1
    new-instance v2, LX/3dN;

    invoke-direct {v2}, LX/3dN;-><init>()V

    .line 1322612
    invoke-interface {v1}, LX/17A;->a()I

    move-result v3

    .line 1322613
    iput v3, v2, LX/3dN;->b:I

    .line 1322614
    invoke-virtual {v2}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v2

    goto :goto_1

    .line 1322615
    :cond_2
    new-instance v2, LX/4ZH;

    invoke-direct {v2}, LX/4ZH;-><init>()V

    .line 1322616
    invoke-interface {v1}, LX/172;->a()I

    move-result v3

    .line 1322617
    iput v3, v2, LX/4ZH;->b:I

    .line 1322618
    invoke-interface {v1}, LX/172;->b()I

    move-result v3

    .line 1322619
    iput v3, v2, LX/4ZH;->e:I

    .line 1322620
    invoke-virtual {v2}, LX/4ZH;->a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v2

    goto :goto_2
.end method

.method public static a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 1322572
    if-nez p0, :cond_0

    .line 1322573
    const/4 v0, 0x0

    .line 1322574
    :goto_0
    return-object v0

    .line 1322575
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 1322576
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v1

    .line 1322577
    iput v1, v0, LX/2dc;->c:I

    .line 1322578
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 1322579
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 1322580
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v1

    .line 1322581
    iput v1, v0, LX/2dc;->i:I

    .line 1322582
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/8IH;)Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1322502
    if-nez p0, :cond_1

    .line 1322503
    :cond_0
    :goto_0
    return-object v0

    .line 1322504
    :cond_1
    invoke-interface {p0}, LX/8IH;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 1322505
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x4984e12

    if-ne v1, v2, :cond_0

    .line 1322506
    new-instance v2, LX/4Xy;

    invoke-direct {v2}, LX/4Xy;-><init>()V

    .line 1322507
    invoke-interface {p0}, LX/8IH;->k()Ljava/lang/String;

    move-result-object v0

    .line 1322508
    iput-object v0, v2, LX/4Xy;->b:Ljava/lang/String;

    .line 1322509
    invoke-interface {p0}, LX/8IH;->l()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;

    move-result-object v0

    invoke-static {v0}, LX/8I6;->a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1322510
    iput-object v0, v2, LX/4Xy;->z:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1322511
    invoke-interface {p0}, LX/8IH;->m()LX/1VU;

    move-result-object v0

    invoke-static {v0}, LX/8I6;->a(LX/1VU;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1322512
    iput-object v0, v2, LX/4Xy;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1322513
    invoke-interface {p0}, LX/8IH;->c()LX/1f8;

    move-result-object v0

    .line 1322514
    if-nez v0, :cond_4

    .line 1322515
    const/4 v4, 0x0

    .line 1322516
    :goto_1
    move-object v0, v4

    .line 1322517
    iput-object v0, v2, LX/4Xy;->E:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 1322518
    invoke-interface {p0}, LX/8IH;->d()Ljava/lang/String;

    move-result-object v0

    .line 1322519
    iput-object v0, v2, LX/4Xy;->I:Ljava/lang/String;

    .line 1322520
    invoke-interface {p0}, LX/8IH;->e()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1322521
    iput-object v0, v2, LX/4Xy;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1322522
    invoke-interface {p0}, LX/8IH;->aj_()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1322523
    iput-object v0, v2, LX/4Xy;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1322524
    invoke-interface {p0}, LX/8IH;->ai_()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1322525
    iput-object v0, v2, LX/4Xy;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1322526
    invoke-interface {p0}, LX/8IH;->j()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1322527
    iput-object v0, v2, LX/4Xy;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1322528
    invoke-interface {p0}, LX/8IH;->o()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1322529
    iput-object v0, v2, LX/4Xy;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1322530
    invoke-interface {p0}, LX/8IH;->q()Z

    move-result v0

    .line 1322531
    iput-boolean v0, v2, LX/4Xy;->ai:Z

    .line 1322532
    invoke-interface {p0}, LX/8IH;->r()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1322533
    iput-object v0, v2, LX/4Xy;->ak:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1322534
    invoke-interface {p0}, LX/8IH;->s()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1322535
    iput-object v0, v2, LX/4Xy;->am:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1322536
    invoke-interface {p0}, LX/8IH;->t()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1322537
    iput-object v0, v2, LX/4Xy;->an:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1322538
    invoke-interface {p0}, LX/8IH;->v()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1322539
    iput-object v0, v2, LX/4Xy;->ax:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1322540
    invoke-interface {p0}, LX/8IH;->w()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1322541
    iput-object v0, v2, LX/4Xy;->ay:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1322542
    invoke-interface {p0}, LX/8IH;->x()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1322543
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1322544
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-interface {p0}, LX/8IH;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1322545
    invoke-interface {p0}, LX/8IH;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$PhotoEncodingsModel;

    .line 1322546
    if-nez v0, :cond_5

    .line 1322547
    const/4 v4, 0x0

    .line 1322548
    :goto_3
    move-object v0, v4

    .line 1322549
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1322550
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1322551
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1322552
    iput-object v0, v2, LX/4Xy;->aE:LX/0Px;

    .line 1322553
    :cond_3
    invoke-interface {p0}, LX/8IH;->y()I

    move-result v0

    .line 1322554
    iput v0, v2, LX/4Xy;->aI:I

    .line 1322555
    invoke-interface {p0}, LX/8IH;->z()Ljava/lang/String;

    move-result-object v0

    .line 1322556
    iput-object v0, v2, LX/4Xy;->aJ:Ljava/lang/String;

    .line 1322557
    invoke-interface {p0}, LX/8IH;->A()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1322558
    iput-object v0, v2, LX/4Xy;->aK:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1322559
    invoke-interface {p0}, LX/8IH;->B()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1322560
    iput-object v0, v2, LX/4Xy;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1322561
    invoke-virtual {v2}, LX/4Xy;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    goto/16 :goto_0

    .line 1322562
    :cond_4
    new-instance v4, LX/4ZN;

    invoke-direct {v4}, LX/4ZN;-><init>()V

    .line 1322563
    invoke-interface {v0}, LX/1f8;->a()D

    move-result-wide v6

    .line 1322564
    iput-wide v6, v4, LX/4ZN;->b:D

    .line 1322565
    invoke-interface {v0}, LX/1f8;->b()D

    move-result-wide v6

    .line 1322566
    iput-wide v6, v4, LX/4ZN;->c:D

    .line 1322567
    invoke-virtual {v4}, LX/4ZN;->a()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v4

    goto/16 :goto_1

    .line 1322568
    :cond_5
    new-instance v4, LX/4Xz;

    invoke-direct {v4}, LX/4Xz;-><init>()V

    .line 1322569
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$PhotoEncodingsModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 1322570
    iput-object v5, v4, LX/4Xz;->c:Ljava/lang/String;

    .line 1322571
    invoke-virtual {v4}, LX/4Xz;->a()Lcom/facebook/graphql/model/GraphQLPhotoEncoding;

    move-result-object v4

    goto :goto_3
.end method

.method public static a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 14

    .prologue
    const/4 v2, 0x0

    .line 1322130
    if-nez p0, :cond_0

    .line 1322131
    const/4 v0, 0x0

    .line 1322132
    :goto_0
    return-object v0

    .line 1322133
    :cond_0
    new-instance v3, LX/23u;

    invoke-direct {v3}, LX/23u;-><init>()V

    .line 1322134
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1322135
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 1322136
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1322137
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$ActorsModel;

    .line 1322138
    if-nez v0, :cond_5

    .line 1322139
    const/4 v5, 0x0

    .line 1322140
    :goto_2
    move-object v0, v5

    .line 1322141
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1322142
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1322143
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1322144
    iput-object v0, v3, LX/23u;->d:LX/0Px;

    .line 1322145
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1322146
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1322147
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1322148
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel;

    .line 1322149
    if-nez v0, :cond_6

    .line 1322150
    const/4 v4, 0x0

    .line 1322151
    :goto_4
    move-object v0, v4

    .line 1322152
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1322153
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1322154
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1322155
    iput-object v0, v3, LX/23u;->k:LX/0Px;

    .line 1322156
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 1322157
    iput-object v0, v3, LX/23u;->m:Ljava/lang/String;

    .line 1322158
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;->d()J

    move-result-wide v0

    .line 1322159
    iput-wide v0, v3, LX/23u;->v:J

    .line 1322160
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;->e()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;

    move-result-object v0

    .line 1322161
    if-nez v0, :cond_b

    .line 1322162
    const/4 v1, 0x0

    .line 1322163
    :goto_5
    move-object v0, v1

    .line 1322164
    iput-object v0, v3, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1322165
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;->eF_()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$ShareableModel;

    move-result-object v0

    .line 1322166
    if-nez v0, :cond_2a

    .line 1322167
    const/4 v1, 0x0

    .line 1322168
    :goto_6
    move-object v0, v1

    .line 1322169
    iput-object v0, v3, LX/23u;->at:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1322170
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;->eE_()Ljava/lang/String;

    move-result-object v0

    .line 1322171
    iput-object v0, v3, LX/23u;->aM:Ljava/lang/String;

    .line 1322172
    invoke-virtual {v3}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto/16 :goto_0

    .line 1322173
    :cond_5
    new-instance v5, LX/3dL;

    invoke-direct {v5}, LX/3dL;-><init>()V

    .line 1322174
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$ActorsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    .line 1322175
    iput-object v6, v5, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1322176
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$ActorsModel;->b()Z

    move-result v6

    .line 1322177
    iput-boolean v6, v5, LX/3dL;->R:Z

    .line 1322178
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$ActorsModel;->c()Ljava/lang/String;

    move-result-object v6

    .line 1322179
    iput-object v6, v5, LX/3dL;->ag:Ljava/lang/String;

    .line 1322180
    invoke-virtual {v5}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    goto/16 :goto_2

    .line 1322181
    :cond_6
    new-instance v4, LX/39x;

    invoke-direct {v4}, LX/39x;-><init>()V

    .line 1322182
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$MediaModel;

    move-result-object v5

    .line 1322183
    if-nez v5, :cond_7

    .line 1322184
    const/4 v6, 0x0

    .line 1322185
    :goto_7
    move-object v5, v6

    .line 1322186
    iput-object v5, v4, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1322187
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel;->b()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$SourceModel;

    move-result-object v5

    .line 1322188
    if-nez v5, :cond_8

    .line 1322189
    const/4 v6, 0x0

    .line 1322190
    :goto_8
    move-object v5, v6

    .line 1322191
    iput-object v5, v4, LX/39x;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1322192
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel;->c()LX/0Px;

    move-result-object v5

    .line 1322193
    iput-object v5, v4, LX/39x;->p:LX/0Px;

    .line 1322194
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel;->d()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;

    move-result-object v5

    .line 1322195
    if-nez v5, :cond_9

    .line 1322196
    const/4 v6, 0x0

    .line 1322197
    :goto_9
    move-object v5, v6

    .line 1322198
    iput-object v5, v4, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1322199
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel;->e()Ljava/lang/String;

    move-result-object v5

    .line 1322200
    iput-object v5, v4, LX/39x;->t:Ljava/lang/String;

    .line 1322201
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel;->eG_()Ljava/lang/String;

    move-result-object v5

    .line 1322202
    iput-object v5, v4, LX/39x;->w:Ljava/lang/String;

    .line 1322203
    invoke-virtual {v4}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    goto/16 :goto_4

    .line 1322204
    :cond_7
    new-instance v6, LX/4XB;

    invoke-direct {v6}, LX/4XB;-><init>()V

    .line 1322205
    invoke-virtual {v5}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$MediaModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 1322206
    iput-object v7, v6, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1322207
    invoke-virtual {v5}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$MediaModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 1322208
    iput-object v7, v6, LX/4XB;->T:Ljava/lang/String;

    .line 1322209
    invoke-virtual {v5}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$MediaModel;->d()LX/1Fb;

    move-result-object v7

    invoke-static {v7}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    .line 1322210
    iput-object v7, v6, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1322211
    invoke-virtual {v6}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    goto :goto_7

    .line 1322212
    :cond_8
    new-instance v6, LX/173;

    invoke-direct {v6}, LX/173;-><init>()V

    .line 1322213
    invoke-virtual {v5}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$SourceModel;->a()Ljava/lang/String;

    move-result-object v7

    .line 1322214
    iput-object v7, v6, LX/173;->f:Ljava/lang/String;

    .line 1322215
    invoke-virtual {v6}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    goto :goto_8

    .line 1322216
    :cond_9
    new-instance v6, LX/4XR;

    invoke-direct {v6}, LX/4XR;-><init>()V

    .line 1322217
    invoke-virtual {v5}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 1322218
    iput-object v7, v6, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1322219
    invoke-virtual {v5}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel;->b()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;

    move-result-object v7

    .line 1322220
    if-nez v7, :cond_a

    .line 1322221
    const/4 v8, 0x0

    .line 1322222
    :goto_a
    move-object v7, v8

    .line 1322223
    iput-object v7, v6, LX/4XR;->Y:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 1322224
    invoke-virtual {v6}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    goto :goto_9

    .line 1322225
    :cond_a
    new-instance v8, LX/4Vr;

    invoke-direct {v8}, LX/4Vr;-><init>()V

    .line 1322226
    invoke-virtual {v7}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 1322227
    iput-object v5, v8, LX/4Vr;->m:Ljava/lang/String;

    .line 1322228
    invoke-virtual {v7}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$AttachmentsModel$TargetModel$ApplicationModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 1322229
    iput-object v5, v8, LX/4Vr;->r:Ljava/lang/String;

    .line 1322230
    invoke-virtual {v8}, LX/4Vr;->a()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v8

    goto :goto_a

    .line 1322231
    :cond_b
    new-instance v4, LX/3dM;

    invoke-direct {v4}, LX/3dM;-><init>()V

    .line 1322232
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->b()Z

    move-result v1

    .line 1322233
    iput-boolean v1, v4, LX/3dM;->d:Z

    .line 1322234
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->c()Z

    move-result v1

    .line 1322235
    iput-boolean v1, v4, LX/3dM;->e:Z

    .line 1322236
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->d()Z

    move-result v1

    invoke-virtual {v4, v1}, LX/3dM;->c(Z)LX/3dM;

    .line 1322237
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->e()Z

    move-result v1

    .line 1322238
    iput-boolean v1, v4, LX/3dM;->g:Z

    .line 1322239
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->ac_()Z

    move-result v1

    .line 1322240
    iput-boolean v1, v4, LX/3dM;->h:Z

    .line 1322241
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->ad_()Z

    move-result v1

    .line 1322242
    iput-boolean v1, v4, LX/3dM;->i:Z

    .line 1322243
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->j()Z

    move-result v1

    invoke-virtual {v4, v1}, LX/3dM;->g(Z)LX/3dM;

    .line 1322244
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->s()Z

    move-result v1

    .line 1322245
    iput-boolean v1, v4, LX/3dM;->k:Z

    .line 1322246
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->k()Z

    move-result v1

    .line 1322247
    iput-boolean v1, v4, LX/3dM;->l:Z

    .line 1322248
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 1322249
    iput-object v1, v4, LX/3dM;->p:Ljava/lang/String;

    .line 1322250
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->t()Ljava/lang/String;

    move-result-object v1

    .line 1322251
    iput-object v1, v4, LX/3dM;->r:Ljava/lang/String;

    .line 1322252
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->m()Z

    move-result v1

    invoke-virtual {v4, v1}, LX/3dM;->j(Z)LX/3dM;

    .line 1322253
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->n()Ljava/lang/String;

    move-result-object v1

    .line 1322254
    iput-object v1, v4, LX/3dM;->y:Ljava/lang/String;

    .line 1322255
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->u()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v1

    .line 1322256
    if-nez v1, :cond_e

    .line 1322257
    const/4 v2, 0x0

    .line 1322258
    :goto_b
    move-object v1, v2

    .line 1322259
    iput-object v1, v4, LX/3dM;->z:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 1322260
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->o()Z

    move-result v1

    invoke-virtual {v4, v1}, LX/3dM;->l(Z)LX/3dM;

    .line 1322261
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->p()Ljava/lang/String;

    move-result-object v1

    .line 1322262
    iput-object v1, v4, LX/3dM;->D:Ljava/lang/String;

    .line 1322263
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->v()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;

    move-result-object v1

    .line 1322264
    if-nez v1, :cond_12

    .line 1322265
    const/4 v2, 0x0

    .line 1322266
    :goto_c
    move-object v1, v2

    .line 1322267
    invoke-virtual {v4, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 1322268
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->w()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v1

    .line 1322269
    if-nez v1, :cond_13

    .line 1322270
    const/4 v2, 0x0

    .line 1322271
    :goto_d
    move-object v1, v2

    .line 1322272
    invoke-virtual {v4, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)LX/3dM;

    .line 1322273
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->x()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;

    move-result-object v1

    .line 1322274
    if-nez v1, :cond_14

    .line 1322275
    const/4 v2, 0x0

    .line 1322276
    :goto_e
    move-object v1, v2

    .line 1322277
    iput-object v1, v4, LX/3dM;->I:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    .line 1322278
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->q()Ljava/lang/String;

    move-result-object v1

    .line 1322279
    iput-object v1, v4, LX/3dM;->J:Ljava/lang/String;

    .line 1322280
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->y()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;

    move-result-object v1

    .line 1322281
    if-nez v1, :cond_1e

    .line 1322282
    const/4 v2, 0x0

    .line 1322283
    :goto_f
    move-object v1, v2

    .line 1322284
    iput-object v1, v4, LX/3dM;->K:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    .line 1322285
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->z()Z

    move-result v1

    .line 1322286
    iput-boolean v1, v4, LX/3dM;->M:Z

    .line 1322287
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->A()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 1322288
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1322289
    const/4 v1, 0x0

    move v2, v1

    :goto_10
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->A()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_c

    .line 1322290
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->A()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    .line 1322291
    if-nez v1, :cond_1f

    .line 1322292
    const/4 v6, 0x0

    .line 1322293
    :goto_11
    move-object v1, v6

    .line 1322294
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1322295
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_10

    .line 1322296
    :cond_c
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1322297
    iput-object v1, v4, LX/3dM;->N:LX/0Px;

    .line 1322298
    :cond_d
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->B()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;

    move-result-object v1

    .line 1322299
    if-nez v1, :cond_20

    .line 1322300
    const/4 v2, 0x0

    .line 1322301
    :goto_12
    move-object v1, v2

    .line 1322302
    invoke-virtual {v4, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    .line 1322303
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->C()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v1

    .line 1322304
    if-nez v1, :cond_21

    .line 1322305
    const/4 v2, 0x0

    .line 1322306
    :goto_13
    move-object v1, v2

    .line 1322307
    invoke-virtual {v4, v1}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)LX/3dM;

    .line 1322308
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->r()LX/59N;

    move-result-object v1

    .line 1322309
    if-nez v1, :cond_26

    .line 1322310
    const/4 v2, 0x0

    .line 1322311
    :goto_14
    move-object v1, v2

    .line 1322312
    iput-object v1, v4, LX/3dM;->S:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1322313
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->D()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v1

    .line 1322314
    if-nez v1, :cond_27

    .line 1322315
    const/4 v2, 0x0

    .line 1322316
    :goto_15
    move-object v1, v2

    .line 1322317
    iput-object v1, v4, LX/3dM;->T:Lcom/facebook/graphql/model/GraphQLUser;

    .line 1322318
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->E()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;

    move-result-object v1

    .line 1322319
    if-nez v1, :cond_28

    .line 1322320
    const/4 v2, 0x0

    .line 1322321
    :goto_16
    move-object v1, v2

    .line 1322322
    iput-object v1, v4, LX/3dM;->W:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1322323
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->F()I

    move-result v1

    invoke-virtual {v4, v1}, LX/3dM;->b(I)LX/3dM;

    .line 1322324
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel;->G()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;

    move-result-object v1

    .line 1322325
    if-nez v1, :cond_29

    .line 1322326
    const/4 v2, 0x0

    .line 1322327
    :goto_17
    move-object v1, v2

    .line 1322328
    iput-object v1, v4, LX/3dM;->aa:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1322329
    invoke-virtual {v4}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    goto/16 :goto_5

    .line 1322330
    :cond_e
    new-instance v6, LX/4Wx;

    invoke-direct {v6}, LX/4Wx;-><init>()V

    .line 1322331
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_10

    .line 1322332
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1322333
    const/4 v2, 0x0

    move v5, v2

    :goto_18
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v5, v2, :cond_f

    .line 1322334
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;

    .line 1322335
    if-nez v2, :cond_11

    .line 1322336
    const/4 v8, 0x0

    .line 1322337
    :goto_19
    move-object v2, v8

    .line 1322338
    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1322339
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_18

    .line 1322340
    :cond_f
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1322341
    iput-object v2, v6, LX/4Wx;->b:LX/0Px;

    .line 1322342
    :cond_10
    invoke-virtual {v6}, LX/4Wx;->a()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v2

    goto/16 :goto_b

    .line 1322343
    :cond_11
    new-instance v8, LX/3dL;

    invoke-direct {v8}, LX/3dL;-><init>()V

    .line 1322344
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    .line 1322345
    iput-object v9, v8, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1322346
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v9

    .line 1322347
    iput-object v9, v8, LX/3dL;->ag:Ljava/lang/String;

    .line 1322348
    invoke-virtual {v8}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v8

    goto :goto_19

    .line 1322349
    :cond_12
    new-instance v2, LX/3dN;

    invoke-direct {v2}, LX/3dN;-><init>()V

    .line 1322350
    invoke-virtual {v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$LikersModel;->a()I

    move-result v5

    .line 1322351
    iput v5, v2, LX/3dN;->b:I

    .line 1322352
    invoke-virtual {v2}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v2

    goto/16 :goto_c

    .line 1322353
    :cond_13
    new-instance v2, LX/3dO;

    invoke-direct {v2}, LX/3dO;-><init>()V

    .line 1322354
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v5

    .line 1322355
    iput v5, v2, LX/3dO;->b:I

    .line 1322356
    invoke-virtual {v2}, LX/3dO;->a()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v2

    goto/16 :goto_d

    .line 1322357
    :cond_14
    new-instance v2, LX/4WO;

    invoke-direct {v2}, LX/4WO;-><init>()V

    .line 1322358
    invoke-virtual {v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;->a()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel;

    move-result-object v5

    .line 1322359
    if-nez v5, :cond_15

    .line 1322360
    const/4 v6, 0x0

    .line 1322361
    :goto_1a
    move-object v5, v6

    .line 1322362
    iput-object v5, v2, LX/4WO;->b:Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    .line 1322363
    invoke-virtual {v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;->b()LX/175;

    move-result-object v5

    .line 1322364
    if-nez v5, :cond_19

    .line 1322365
    const/4 v6, 0x0

    .line 1322366
    :goto_1b
    move-object v5, v6

    .line 1322367
    iput-object v5, v2, LX/4WO;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1322368
    invoke-virtual {v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel;->c()Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    move-result-object v5

    .line 1322369
    iput-object v5, v2, LX/4WO;->d:Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    .line 1322370
    invoke-virtual {v2}, LX/4WO;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityInfo;

    move-result-object v2

    goto/16 :goto_e

    .line 1322371
    :cond_15
    new-instance v8, LX/4WN;

    invoke-direct {v8}, LX/4WN;-><init>()V

    .line 1322372
    invoke-virtual {v5}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel;->a()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_17

    .line 1322373
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 1322374
    const/4 v6, 0x0

    move v7, v6

    :goto_1c
    invoke-virtual {v5}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_16

    .line 1322375
    invoke-virtual {v5}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;

    .line 1322376
    if-nez v6, :cond_18

    .line 1322377
    const/4 v10, 0x0

    .line 1322378
    :goto_1d
    move-object v6, v10

    .line 1322379
    invoke-virtual {v9, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1322380
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1c

    .line 1322381
    :cond_16
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 1322382
    iput-object v6, v8, LX/4WN;->b:LX/0Px;

    .line 1322383
    :cond_17
    invoke-virtual {v8}, LX/4WN;->a()Lcom/facebook/graphql/model/GraphQLFeedbackRealTimeActivityActorsConnection;

    move-result-object v6

    goto :goto_1a

    .line 1322384
    :cond_18
    new-instance v10, LX/3dL;

    invoke-direct {v10}, LX/3dL;-><init>()V

    .line 1322385
    invoke-virtual {v6}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    .line 1322386
    iput-object v11, v10, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1322387
    invoke-virtual {v6}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v11

    .line 1322388
    iput-object v11, v10, LX/3dL;->E:Ljava/lang/String;

    .line 1322389
    invoke-virtual {v6}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->d()Ljava/lang/String;

    move-result-object v11

    .line 1322390
    iput-object v11, v10, LX/3dL;->ag:Ljava/lang/String;

    .line 1322391
    invoke-virtual {v6}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$FeedbackRealTimeActivityInfoFieldsModel$RealTimeActivityInfoModel$RealTimeActivityActorsModel$NodesModel;->e()LX/1Fb;

    move-result-object v11

    invoke-static {v11}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    .line 1322392
    iput-object v11, v10, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1322393
    invoke-virtual {v10}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v10

    goto :goto_1d

    .line 1322394
    :cond_19
    new-instance v8, LX/173;

    invoke-direct {v8}, LX/173;-><init>()V

    .line 1322395
    invoke-interface {v5}, LX/175;->b()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_1b

    .line 1322396
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 1322397
    const/4 v6, 0x0

    move v7, v6

    :goto_1e
    invoke-interface {v5}, LX/175;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    if-ge v7, v6, :cond_1a

    .line 1322398
    invoke-interface {v5}, LX/175;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1W5;

    .line 1322399
    if-nez v6, :cond_1c

    .line 1322400
    const/4 v10, 0x0

    .line 1322401
    :goto_1f
    move-object v6, v10

    .line 1322402
    invoke-virtual {v9, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1322403
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1e

    .line 1322404
    :cond_1a
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 1322405
    iput-object v6, v8, LX/173;->e:LX/0Px;

    .line 1322406
    :cond_1b
    invoke-interface {v5}, LX/175;->a()Ljava/lang/String;

    move-result-object v6

    .line 1322407
    iput-object v6, v8, LX/173;->f:Ljava/lang/String;

    .line 1322408
    invoke-virtual {v8}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    goto/16 :goto_1b

    .line 1322409
    :cond_1c
    new-instance v10, LX/4W6;

    invoke-direct {v10}, LX/4W6;-><init>()V

    .line 1322410
    invoke-interface {v6}, LX/1W5;->a()LX/171;

    move-result-object v11

    .line 1322411
    if-nez v11, :cond_1d

    .line 1322412
    const/4 v12, 0x0

    .line 1322413
    :goto_20
    move-object v11, v12

    .line 1322414
    iput-object v11, v10, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1322415
    invoke-interface {v6}, LX/1W5;->b()I

    move-result v11

    .line 1322416
    iput v11, v10, LX/4W6;->c:I

    .line 1322417
    invoke-interface {v6}, LX/1W5;->c()I

    move-result v11

    .line 1322418
    iput v11, v10, LX/4W6;->d:I

    .line 1322419
    invoke-virtual {v10}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v10

    goto :goto_1f

    .line 1322420
    :cond_1d
    new-instance v12, LX/170;

    invoke-direct {v12}, LX/170;-><init>()V

    .line 1322421
    invoke-interface {v11}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v13

    .line 1322422
    iput-object v13, v12, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1322423
    invoke-interface {v11}, LX/171;->c()LX/0Px;

    move-result-object v13

    .line 1322424
    iput-object v13, v12, LX/170;->b:LX/0Px;

    .line 1322425
    invoke-interface {v11}, LX/171;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v13

    .line 1322426
    iput-object v13, v12, LX/170;->l:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 1322427
    invoke-interface {v11}, LX/171;->e()Ljava/lang/String;

    move-result-object v13

    .line 1322428
    iput-object v13, v12, LX/170;->o:Ljava/lang/String;

    .line 1322429
    invoke-interface {v11}, LX/171;->v_()Ljava/lang/String;

    move-result-object v13

    .line 1322430
    iput-object v13, v12, LX/170;->A:Ljava/lang/String;

    .line 1322431
    invoke-interface {v11}, LX/171;->w_()Ljava/lang/String;

    move-result-object v13

    .line 1322432
    iput-object v13, v12, LX/170;->X:Ljava/lang/String;

    .line 1322433
    invoke-interface {v11}, LX/171;->j()Ljava/lang/String;

    move-result-object v13

    .line 1322434
    iput-object v13, v12, LX/170;->Y:Ljava/lang/String;

    .line 1322435
    invoke-virtual {v12}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v12

    goto :goto_20

    .line 1322436
    :cond_1e
    new-instance v2, LX/4Ya;

    invoke-direct {v2}, LX/4Ya;-><init>()V

    .line 1322437
    invoke-virtual {v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$ResharesModel;->a()I

    move-result v5

    .line 1322438
    iput v5, v2, LX/4Ya;->b:I

    .line 1322439
    invoke-virtual {v2}, LX/4Ya;->a()Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v2

    goto/16 :goto_f

    .line 1322440
    :cond_1f
    new-instance v6, LX/4WL;

    invoke-direct {v6}, LX/4WL;-><init>()V

    .line 1322441
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;->a()I

    move-result v7

    .line 1322442
    iput v7, v6, LX/4WL;->b:I

    .line 1322443
    invoke-virtual {v6}, LX/4WL;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v6

    goto/16 :goto_11

    .line 1322444
    :cond_20
    new-instance v2, LX/4ZH;

    invoke-direct {v2}, LX/4ZH;-><init>()V

    .line 1322445
    invoke-virtual {v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v5

    .line 1322446
    iput v5, v2, LX/4ZH;->b:I

    .line 1322447
    invoke-virtual {v1}, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$SimpleFeedFeedbackModel$TopLevelCommentsModel;->b()I

    move-result v5

    .line 1322448
    iput v5, v2, LX/4ZH;->e:I

    .line 1322449
    invoke-virtual {v2}, LX/4ZH;->a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v2

    goto/16 :goto_12

    .line 1322450
    :cond_21
    new-instance v6, LX/3dQ;

    invoke-direct {v6}, LX/3dQ;-><init>()V

    .line 1322451
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_23

    .line 1322452
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1322453
    const/4 v2, 0x0

    move v5, v2

    :goto_21
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v5, v2, :cond_22

    .line 1322454
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;

    .line 1322455
    if-nez v2, :cond_24

    .line 1322456
    const/4 v8, 0x0

    .line 1322457
    :goto_22
    move-object v2, v8

    .line 1322458
    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1322459
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_21

    .line 1322460
    :cond_22
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1322461
    iput-object v2, v6, LX/3dQ;->b:LX/0Px;

    .line 1322462
    :cond_23
    invoke-virtual {v6}, LX/3dQ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v2

    goto/16 :goto_13

    .line 1322463
    :cond_24
    new-instance v8, LX/4ZJ;

    invoke-direct {v8}, LX/4ZJ;-><init>()V

    .line 1322464
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;

    move-result-object v9

    .line 1322465
    if-nez v9, :cond_25

    .line 1322466
    const/4 v10, 0x0

    .line 1322467
    :goto_23
    move-object v9, v10

    .line 1322468
    iput-object v9, v8, LX/4ZJ;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 1322469
    invoke-virtual {v2}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->b()I

    move-result v9

    .line 1322470
    iput v9, v8, LX/4ZJ;->c:I

    .line 1322471
    invoke-virtual {v8}, LX/4ZJ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    move-result-object v8

    goto :goto_22

    .line 1322472
    :cond_25
    new-instance v10, LX/4WM;

    invoke-direct {v10}, LX/4WM;-><init>()V

    .line 1322473
    invoke-virtual {v9}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;->a()I

    move-result v11

    .line 1322474
    iput v11, v10, LX/4WM;->f:I

    .line 1322475
    invoke-virtual {v10}, LX/4WM;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v10

    goto :goto_23

    .line 1322476
    :cond_26
    new-instance v2, LX/4XY;

    invoke-direct {v2}, LX/4XY;-><init>()V

    .line 1322477
    invoke-interface {v1}, LX/59N;->b()Ljava/lang/String;

    move-result-object v5

    .line 1322478
    iput-object v5, v2, LX/4XY;->ag:Ljava/lang/String;

    .line 1322479
    invoke-interface {v1}, LX/59N;->c()Ljava/lang/String;

    move-result-object v5

    .line 1322480
    iput-object v5, v2, LX/4XY;->aT:Ljava/lang/String;

    .line 1322481
    invoke-interface {v1}, LX/59N;->d()LX/1Fb;

    move-result-object v5

    invoke-static {v5}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 1322482
    iput-object v5, v2, LX/4XY;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1322483
    invoke-virtual {v2}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    goto/16 :goto_14

    .line 1322484
    :cond_27
    new-instance v2, LX/33O;

    invoke-direct {v2}, LX/33O;-><init>()V

    .line 1322485
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 1322486
    iput-object v5, v2, LX/33O;->aI:Ljava/lang/String;

    .line 1322487
    invoke-virtual {v2}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    goto/16 :goto_15

    .line 1322488
    :cond_28
    new-instance v2, LX/173;

    invoke-direct {v2}, LX/173;-><init>()V

    .line 1322489
    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerDoesNotLikeSentenceModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 1322490
    iput-object v5, v2, LX/173;->f:Ljava/lang/String;

    .line 1322491
    invoke-virtual {v2}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    goto/16 :goto_16

    .line 1322492
    :cond_29
    new-instance v2, LX/173;

    invoke-direct {v2}, LX/173;-><init>()V

    .line 1322493
    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraQueryFeedbackModel$ViewerLikesSentenceModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 1322494
    iput-object v5, v2, LX/173;->f:Ljava/lang/String;

    .line 1322495
    invoke-virtual {v2}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    goto/16 :goto_17

    .line 1322496
    :cond_2a
    new-instance v1, LX/170;

    invoke-direct {v1}, LX/170;-><init>()V

    .line 1322497
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$ShareableModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 1322498
    iput-object v2, v1, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1322499
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel$ShareableModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 1322500
    iput-object v2, v1, LX/170;->o:Ljava/lang/String;

    .line 1322501
    invoke-virtual {v1}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    goto/16 :goto_6
.end method
