.class public abstract LX/6vH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MUTATOR::",
        "Lcom/facebook/payments/contactinfo/form/ContactInfoFormMutator;",
        "CONTACT_INPUT_VA",
        "LIDATOR::Lcom/facebook/payments/contactinfo/validation/ContactInputValidator;",
        "CONTENT_PROVIDER::",
        "LX/6v3;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/6vY;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TMUTATOR;>;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TCONTACT_INPUT_VA",
            "LIDATOR;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TCONTENT_PROVIDER;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/6vY;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6vY;",
            "LX/0Ot",
            "<TMUTATOR;>;",
            "LX/0Ot",
            "<TCONTACT_INPUT_VA",
            "LIDATOR;",
            ">;",
            "LX/0Ot",
            "<TCONTENT_PROVIDER;>;)V"
        }
    .end annotation

    .prologue
    .line 1156824
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156825
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6vY;

    iput-object v0, p0, LX/6vH;->a:LX/6vY;

    .line 1156826
    iput-object p2, p0, LX/6vH;->b:LX/0Ot;

    .line 1156827
    iput-object p3, p0, LX/6vH;->c:LX/0Ot;

    .line 1156828
    iput-object p4, p0, LX/6vH;->d:LX/0Ot;

    .line 1156829
    return-void
.end method
