.class public LX/6pr;
.super LX/6pY;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/6pr;


# instance fields
.field public final a:LX/6p6;

.field private final b:LX/6pe;


# direct methods
.method public constructor <init>(LX/6p6;LX/6pe;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1149686
    invoke-direct {p0}, LX/6pY;-><init>()V

    .line 1149687
    iput-object p1, p0, LX/6pr;->a:LX/6p6;

    .line 1149688
    iput-object p2, p0, LX/6pr;->b:LX/6pe;

    .line 1149689
    return-void
.end method

.method public static a(LX/0QB;)LX/6pr;
    .locals 5

    .prologue
    .line 1149690
    sget-object v0, LX/6pr;->c:LX/6pr;

    if-nez v0, :cond_1

    .line 1149691
    const-class v1, LX/6pr;

    monitor-enter v1

    .line 1149692
    :try_start_0
    sget-object v0, LX/6pr;->c:LX/6pr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1149693
    if-eqz v2, :cond_0

    .line 1149694
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1149695
    new-instance p0, LX/6pr;

    invoke-static {v0}, LX/6p6;->a(LX/0QB;)LX/6p6;

    move-result-object v3

    check-cast v3, LX/6p6;

    invoke-static {v0}, LX/6pe;->a(LX/0QB;)LX/6pe;

    move-result-object v4

    check-cast v4, LX/6pe;

    invoke-direct {p0, v3, v4}, LX/6pr;-><init>(LX/6p6;LX/6pe;)V

    .line 1149696
    move-object v0, p0

    .line 1149697
    sput-object v0, LX/6pr;->c:LX/6pr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1149698
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1149699
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1149700
    :cond_1
    sget-object v0, LX/6pr;->c:LX/6pr;

    return-object v0

    .line 1149701
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1149702
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/6pM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1149703
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    sget-object v1, LX/6pM;->RESET:LX/6pM;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    iget-object v1, p0, LX/6pr;->b:LX/6pe;

    invoke-virtual {v1}, LX/6pe;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/ResetPinFragment;)LX/6oW;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1149704
    new-instance v0, LX/6pp;

    invoke-direct {v0, p0, p1, p2}, LX/6pp;-><init>(LX/6pr;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/ResetPinFragment;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;LX/6pM;)LX/6od;
    .locals 1

    .prologue
    .line 1149705
    iget-object v0, p0, LX/6pr;->b:LX/6pe;

    invoke-virtual {v0, p1, p2, p3}, LX/6pe;->a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;LX/6pM;)LX/6od;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1149706
    iget-object v0, p0, LX/6pr;->a:LX/6p6;

    invoke-virtual {v0}, LX/6p6;->a()V

    .line 1149707
    return-void
.end method
