.class public final LX/77s;
.super LX/13D;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final d:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final e:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final f:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final g:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final h:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final i:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final j:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static volatile k:LX/77s;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1172235
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->THREAD_LIST_OPEN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/77s;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1172236
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/77s;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1172237
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TAB_NAVIGATION_FRIEND_REQUESTS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/77s;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1172238
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/77s;->d:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1172239
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_ADMIN_TIMELINE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/77s;->e:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1172240
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_NONADMIN_TIMELINE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/77s;->f:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1172241
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_ADMIN_TIMELINE_VIEW_VERIFY_ELIGIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/77s;->g:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1172242
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PERSONAL_PROFILE_OWNER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/77s;->h:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1172243
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PERSONAL_PROFILE_FRIEND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/77s;->i:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1172244
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PERSONAL_PROFILE_NONFRIEND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/77s;->j:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/13J;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1172245
    invoke-direct {p0, p1}, LX/13D;-><init>(LX/13J;)V

    .line 1172246
    return-void
.end method

.method public static a(LX/0QB;)LX/77s;
    .locals 4

    .prologue
    .line 1172247
    sget-object v0, LX/77s;->k:LX/77s;

    if-nez v0, :cond_1

    .line 1172248
    const-class v1, LX/77s;

    monitor-enter v1

    .line 1172249
    :try_start_0
    sget-object v0, LX/77s;->k:LX/77s;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1172250
    if-eqz v2, :cond_0

    .line 1172251
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1172252
    new-instance p0, LX/77s;

    const-class v3, LX/13J;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/13J;

    invoke-direct {p0, v3}, LX/77s;-><init>(LX/13J;)V

    .line 1172253
    move-object v0, p0

    .line 1172254
    sput-object v0, LX/77s;->k:LX/77s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1172255
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1172256
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1172257
    :cond_1
    sget-object v0, LX/77s;->k:LX/77s;

    return-object v0

    .line 1172258
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1172259
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1172260
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1172261
    const-string v0, "1820"

    return-object v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 1172262
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1172263
    const-string v0, "Footer"

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1172264
    const/4 v0, 0x1

    return v0
.end method
