.class public final LX/75l;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Landroid/os/Message;

.field public final synthetic b:LX/75n;


# direct methods
.method public constructor <init>(LX/75n;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 1169920
    iput-object p1, p0, LX/75l;->b:LX/75n;

    iput-object p2, p0, LX/75l;->a:Landroid/os/Message;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1169933
    if-eqz p1, :cond_0

    .line 1169934
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1169935
    if-eqz v0, :cond_0

    .line 1169936
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1169937
    const-string v1, "error"

    .line 1169938
    iget-object v2, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v2, v2

    .line 1169939
    iget-object p1, v2, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    move-object v2, p1

    .line 1169940
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169941
    iget-object v2, p0, LX/75l;->a:Landroid/os/Message;

    .line 1169942
    invoke-static {v2, v0}, LX/75n;->a(Landroid/os/Message;Landroid/os/Bundle;)V

    .line 1169943
    :goto_0
    return-void

    .line 1169944
    :cond_0
    iget-object v1, p0, LX/75l;->a:Landroid/os/Message;

    const-string v2, "unexpected response"

    .line 1169945
    invoke-static {v1, v2}, LX/75n;->a(Landroid/os/Message;Ljava/lang/String;)V

    .line 1169946
    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1169921
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1169922
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Result;

    .line 1169923
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1169924
    const-string v2, "access_token"

    .line 1169925
    iget-object v3, v0, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Result;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1169926
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169927
    const-string v2, "expires_in"

    .line 1169928
    iget-wide v6, v0, Lcom/facebook/platform/auth/server/ExtendAccessTokenMethod$Result;->b:J

    move-wide v4, v6

    .line 1169929
    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1169930
    iget-object v2, p0, LX/75l;->a:Landroid/os/Message;

    .line 1169931
    invoke-static {v2, v1}, LX/75n;->a(Landroid/os/Message;Landroid/os/Bundle;)V

    .line 1169932
    return-void
.end method
