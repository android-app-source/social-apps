.class public LX/7S3;
.super Landroid/view/TextureView;
.source ""

# interfaces
.implements LX/6Jh;
.implements LX/6Kd;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private a:Landroid/graphics/SurfaceTexture;

.field public b:Landroid/view/Surface;

.field public c:I

.field public d:I

.field public e:LX/6Kl;

.field public f:Landroid/view/TextureView$SurfaceTextureListener;

.field private g:LX/6Jg;

.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1208090
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/7S3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1208091
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1208105
    invoke-direct {p0, p1, p2, p3}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1208106
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7S3;->h:Z

    .line 1208107
    return-void
.end method


# virtual methods
.method public final a(LX/6Kl;)V
    .locals 1

    .prologue
    .line 1208101
    iget-object v0, p0, LX/7S3;->b:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 1208102
    iget-object v0, p0, LX/7S3;->b:Landroid/view/Surface;

    invoke-virtual {p1, p0, v0}, LX/6Kl;->a(LX/6Kd;Landroid/view/Surface;)V

    .line 1208103
    :cond_0
    iput-object p1, p0, LX/7S3;->e:LX/6Kl;

    .line 1208104
    return-void
.end method

.method public final a(Landroid/graphics/SurfaceTexture;LX/6Km;)V
    .locals 4

    .prologue
    .line 1208092
    iput-object p1, p0, LX/7S3;->a:Landroid/graphics/SurfaceTexture;

    .line 1208093
    monitor-enter p0

    .line 1208094
    :try_start_0
    iget-object v0, p0, LX/7S3;->f:Landroid/view/TextureView$SurfaceTextureListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1208095
    const-wide/16 v0, 0x1388

    const v2, -0x3d453e24

    :try_start_1
    invoke-static {p0, v0, v1, v2}, LX/02L;->a(Ljava/lang/Object;JI)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1208096
    :cond_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1208097
    iget-object v0, p0, LX/7S3;->f:Landroid/view/TextureView$SurfaceTextureListener;

    iget-object v1, p0, LX/7S3;->a:Landroid/graphics/SurfaceTexture;

    iget v2, p0, LX/7S3;->c:I

    iget v3, p0, LX/7S3;->d:I

    invoke-interface {v0, v1, v2, v3}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V

    .line 1208098
    return-void

    .line 1208099
    :catch_0
    :try_start_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "SurfaceTexture was never registered"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1208100
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1208086
    iget-object v0, p0, LX/7S3;->b:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 1208087
    iget-object v0, p0, LX/7S3;->b:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 1208088
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7S3;->h:Z

    .line 1208089
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1208085
    const/4 v0, 0x1

    return v0
.end method

.method public final dE_()V
    .locals 1

    .prologue
    .line 1208083
    const/4 v0, 0x0

    iput-object v0, p0, LX/7S3;->e:LX/6Kl;

    .line 1208084
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1208108
    const/4 v0, 0x1

    return v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 1208082
    return-void
.end method

.method public getClock()LX/6Jg;
    .locals 1

    .prologue
    .line 1208077
    iget-object v0, p0, LX/7S3;->g:LX/6Jg;

    if-eqz v0, :cond_0

    .line 1208078
    iget-object v0, p0, LX/7S3;->g:LX/6Jg;

    .line 1208079
    :goto_0
    return-object v0

    .line 1208080
    :cond_0
    sget-object v0, LX/6KW;->a:LX/6KW;

    move-object v0, v0

    .line 1208081
    goto :goto_0
.end method

.method public getInputHeight()I
    .locals 1

    .prologue
    .line 1208076
    iget v0, p0, LX/7S3;->d:I

    return v0
.end method

.method public getInputWidth()I
    .locals 1

    .prologue
    .line 1208075
    iget v0, p0, LX/7S3;->c:I

    return v0
.end method

.method public final getRotationDegrees$134621()I
    .locals 1

    .prologue
    .line 1208067
    const/4 v0, 0x0

    return v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 1208074
    const/4 v0, 0x1

    return v0
.end method

.method public setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V
    .locals 1

    .prologue
    .line 1208070
    if-eqz p1, :cond_0

    .line 1208071
    new-instance v0, LX/7S2;

    invoke-direct {v0, p0, p1}, LX/7S2;-><init>(LX/7S3;Landroid/view/TextureView$SurfaceTextureListener;)V

    invoke-super {p0, v0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 1208072
    :goto_0
    return-void

    .line 1208073
    :cond_0
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    goto :goto_0
.end method

.method public setVideoClock(LX/6Jg;)V
    .locals 0

    .prologue
    .line 1208068
    iput-object p1, p0, LX/7S3;->g:LX/6Jg;

    .line 1208069
    return-void
.end method
