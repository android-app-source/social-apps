.class public final LX/7qg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1260320
    const/4 v12, 0x0

    .line 1260321
    const/4 v11, 0x0

    .line 1260322
    const/4 v10, 0x0

    .line 1260323
    const/4 v9, 0x0

    .line 1260324
    const/4 v8, 0x0

    .line 1260325
    const/4 v7, 0x0

    .line 1260326
    const/4 v6, 0x0

    .line 1260327
    const/4 v5, 0x0

    .line 1260328
    const/4 v4, 0x0

    .line 1260329
    const/4 v3, 0x0

    .line 1260330
    const/4 v2, 0x0

    .line 1260331
    const/4 v1, 0x0

    .line 1260332
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1260333
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1260334
    const/4 v1, 0x0

    .line 1260335
    :goto_0
    return v1

    .line 1260336
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1260337
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_c

    .line 1260338
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1260339
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1260340
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1260341
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1260342
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 1260343
    :cond_2
    const-string v14, "legacy_api_id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1260344
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 1260345
    :cond_3
    const-string v14, "prefetch_priority"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1260346
    const/4 v1, 0x1

    .line 1260347
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    goto :goto_1

    .line 1260348
    :cond_4
    const-string v14, "present_participle"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1260349
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1260350
    :cond_5
    const-string v14, "previewTemplateAtPlace"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1260351
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1260352
    :cond_6
    const-string v14, "previewTemplateNoTags"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1260353
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1260354
    :cond_7
    const-string v14, "previewTemplateWithPeople"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1260355
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1260356
    :cond_8
    const-string v14, "previewTemplateWithPeopleAtPlace"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1260357
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1260358
    :cond_9
    const-string v14, "previewTemplateWithPerson"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 1260359
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1260360
    :cond_a
    const-string v14, "previewTemplateWithPersonAtPlace"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 1260361
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1260362
    :cond_b
    const-string v14, "prompt"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1260363
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 1260364
    :cond_c
    const/16 v13, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1260365
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1260366
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1260367
    if-eqz v1, :cond_d

    .line 1260368
    const/4 v1, 0x2

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v10, v11}, LX/186;->a(III)V

    .line 1260369
    :cond_d
    const/4 v1, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1260370
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1260371
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1260372
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1260373
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1260374
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1260375
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1260376
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1260377
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1260378
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1260379
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1260380
    if-eqz v0, :cond_0

    .line 1260381
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260382
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1260383
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1260384
    if-eqz v0, :cond_1

    .line 1260385
    const-string v1, "legacy_api_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260386
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1260387
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1260388
    if-eqz v0, :cond_2

    .line 1260389
    const-string v1, "prefetch_priority"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260390
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1260391
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1260392
    if-eqz v0, :cond_3

    .line 1260393
    const-string v1, "present_participle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260394
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1260395
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1260396
    if-eqz v0, :cond_4

    .line 1260397
    const-string v1, "previewTemplateAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260398
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1260399
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1260400
    if-eqz v0, :cond_5

    .line 1260401
    const-string v1, "previewTemplateNoTags"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260402
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1260403
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1260404
    if-eqz v0, :cond_6

    .line 1260405
    const-string v1, "previewTemplateWithPeople"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260406
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1260407
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1260408
    if-eqz v0, :cond_7

    .line 1260409
    const-string v1, "previewTemplateWithPeopleAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260410
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1260411
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1260412
    if-eqz v0, :cond_8

    .line 1260413
    const-string v1, "previewTemplateWithPerson"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260414
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1260415
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1260416
    if-eqz v0, :cond_9

    .line 1260417
    const-string v1, "previewTemplateWithPersonAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260418
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1260419
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1260420
    if-eqz v0, :cond_a

    .line 1260421
    const-string v1, "prompt"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1260422
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1260423
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1260424
    return-void
.end method
