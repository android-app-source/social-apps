.class public final enum LX/77R;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/77R;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/77R;

.field public static final enum DISMISS_ACTION:LX/77R;

.field public static final enum IMPRESSION:LX/77R;

.field public static final enum PRIMARY_ACTION:LX/77R;

.field public static final enum SECONDARY_ACTION:LX/77R;

.field public static final enum UNKNOWN:LX/77R;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1171817
    new-instance v0, LX/77R;

    const-string v1, "IMPRESSION"

    invoke-direct {v0, v1, v2}, LX/77R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77R;->IMPRESSION:LX/77R;

    .line 1171818
    new-instance v0, LX/77R;

    const-string v1, "PRIMARY_ACTION"

    invoke-direct {v0, v1, v3}, LX/77R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77R;->PRIMARY_ACTION:LX/77R;

    .line 1171819
    new-instance v0, LX/77R;

    const-string v1, "SECONDARY_ACTION"

    invoke-direct {v0, v1, v4}, LX/77R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77R;->SECONDARY_ACTION:LX/77R;

    .line 1171820
    new-instance v0, LX/77R;

    const-string v1, "DISMISS_ACTION"

    invoke-direct {v0, v1, v5}, LX/77R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77R;->DISMISS_ACTION:LX/77R;

    .line 1171821
    new-instance v0, LX/77R;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, LX/77R;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/77R;->UNKNOWN:LX/77R;

    .line 1171822
    const/4 v0, 0x5

    new-array v0, v0, [LX/77R;

    sget-object v1, LX/77R;->IMPRESSION:LX/77R;

    aput-object v1, v0, v2

    sget-object v1, LX/77R;->PRIMARY_ACTION:LX/77R;

    aput-object v1, v0, v3

    sget-object v1, LX/77R;->SECONDARY_ACTION:LX/77R;

    aput-object v1, v0, v4

    sget-object v1, LX/77R;->DISMISS_ACTION:LX/77R;

    aput-object v1, v0, v5

    sget-object v1, LX/77R;->UNKNOWN:LX/77R;

    aput-object v1, v0, v6

    sput-object v0, LX/77R;->$VALUES:[LX/77R;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1171823
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/77R;
    .locals 1

    .prologue
    .line 1171824
    if-nez p0, :cond_0

    .line 1171825
    :try_start_0
    sget-object v0, LX/77R;->UNKNOWN:LX/77R;

    .line 1171826
    :goto_0
    return-object v0

    .line 1171827
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/77R;->valueOf(Ljava/lang/String;)LX/77R;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1171828
    :catch_0
    sget-object v0, LX/77R;->UNKNOWN:LX/77R;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/77R;
    .locals 1

    .prologue
    .line 1171829
    const-class v0, LX/77R;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/77R;

    return-object v0
.end method

.method public static values()[LX/77R;
    .locals 1

    .prologue
    .line 1171830
    sget-object v0, LX/77R;->$VALUES:[LX/77R;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/77R;

    return-object v0
.end method
