.class public LX/7yM;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0ad;

.field private final c:LX/1tw;

.field private d:LX/7yP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:[B
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:[B
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1279164
    const-class v0, LX/7yM;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7yM;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/1tw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1279165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1279166
    iput-object p1, p0, LX/7yM;->b:LX/0ad;

    .line 1279167
    iput-object p2, p0, LX/7yM;->c:LX/1tw;

    .line 1279168
    return-void
.end method

.method private static a([B)V
    .locals 8

    .prologue
    .line 1279169
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/7yP;->a(Ljava/nio/ByteBuffer;)LX/7yP;

    move-result-object v0

    .line 1279170
    invoke-virtual {v0}, LX/7yP;->c()LX/7yQ;

    move-result-object v1

    .line 1279171
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MacerFaceDetector configuration: \n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1279172
    const-string v3, "\t maxDetectionDim: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1279173
    const/4 v4, 0x4

    invoke-virtual {v0, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_1

    iget-object v5, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v0, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v4

    :goto_0
    move v4, v4

    .line 1279174
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1279175
    const-string v3, "\t neonEnabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x0

    .line 1279176
    const/16 v5, 0x8

    invoke-virtual {v0, v5}, LX/0eW;->a(I)I

    move-result v5

    if-eqz v5, :cond_0

    iget-object v6, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v7, v0, LX/0eW;->a:I

    add-int/2addr v5, v7

    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x1

    :cond_0
    move v0, v4

    .line 1279177
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1279178
    const-string v0, "\t crop quality: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, LX/7yQ;->a()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1279179
    const-string v0, "\t crop max size: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, LX/7yQ;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1279180
    const-string v0, "\t total byte size: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    array-length v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1279181
    return-void

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()[B
    .locals 6

    .prologue
    .line 1279182
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7yM;->e:[B

    if-eqz v0, :cond_0

    .line 1279183
    iget-object v0, p0, LX/7yM;->e:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1279184
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1279185
    :cond_0
    :try_start_1
    new-instance v0, LX/0eX;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/0eX;-><init>(I)V

    .line 1279186
    const/16 v1, 0x258

    const v2, 0x7fffffff

    iget-object v3, p0, LX/7yM;->c:LX/1tw;

    invoke-virtual {v3}, LX/1tw;->a()Z

    move-result v3

    const/16 v4, 0x55

    const/16 v5, 0x98

    invoke-static {v0, v4, v5}, LX/7yQ;->a(LX/0eX;II)I

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, LX/7yP;->a(LX/0eX;IIZI)I

    move-result v1

    .line 1279187
    invoke-virtual {v0, v1}, LX/0eX;->c(I)V

    .line 1279188
    invoke-virtual {v0}, LX/0eX;->e()[B

    move-result-object v0

    iput-object v0, p0, LX/7yM;->e:[B

    .line 1279189
    const/4 v0, 0x3

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1279190
    iget-object v0, p0, LX/7yM;->e:[B

    invoke-static {v0}, LX/7yM;->a([B)V

    .line 1279191
    :cond_1
    iget-object v0, p0, LX/7yM;->e:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1279192
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()[B
    .locals 6

    .prologue
    .line 1279193
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7yM;->f:[B

    if-eqz v0, :cond_0

    .line 1279194
    iget-object v0, p0, LX/7yM;->f:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1279195
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1279196
    :cond_0
    :try_start_1
    new-instance v0, LX/0eX;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/0eX;-><init>(I)V

    .line 1279197
    const/16 v1, 0x258

    const/4 v2, 0x7

    iget-object v3, p0, LX/7yM;->c:LX/1tw;

    invoke-virtual {v3}, LX/1tw;->a()Z

    move-result v3

    const/16 v4, 0x55

    const/16 v5, 0x98

    invoke-static {v0, v4, v5}, LX/7yQ;->a(LX/0eX;II)I

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, LX/7yP;->a(LX/0eX;IIZI)I

    move-result v1

    .line 1279198
    invoke-virtual {v0, v1}, LX/0eX;->c(I)V

    .line 1279199
    invoke-virtual {v0}, LX/0eX;->e()[B

    move-result-object v0

    iput-object v0, p0, LX/7yM;->f:[B

    .line 1279200
    const/4 v0, 0x3

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1279201
    iget-object v0, p0, LX/7yM;->f:[B

    invoke-static {v0}, LX/7yM;->a([B)V

    .line 1279202
    :cond_1
    iget-object v0, p0, LX/7yM;->f:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1279203
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()LX/7yP;
    .locals 1

    .prologue
    .line 1279204
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7yM;->d:LX/7yP;

    if-nez v0, :cond_0

    .line 1279205
    invoke-virtual {p0}, LX/7yM;->a()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/7yP;->a(Ljava/nio/ByteBuffer;)LX/7yP;

    move-result-object v0

    iput-object v0, p0, LX/7yM;->d:LX/7yP;

    .line 1279206
    :cond_0
    iget-object v0, p0, LX/7yM;->d:LX/7yP;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1279207
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
