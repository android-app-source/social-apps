.class public final LX/6r5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6qa;


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6wP;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/737;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/70D;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/6qb;

.field private h:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;",
            "LX/0Or",
            "<",
            "LX/6wP;",
            ">;",
            "LX/0Or",
            "<",
            "LX/737;",
            ">;",
            "LX/0Or",
            "<",
            "LX/70D;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1151414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1151415
    iput-object p1, p0, LX/6r5;->a:Ljava/util/concurrent/Executor;

    .line 1151416
    iput-object p2, p0, LX/6r5;->b:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    .line 1151417
    iput-object p3, p0, LX/6r5;->c:LX/0Or;

    .line 1151418
    iput-object p4, p0, LX/6r5;->d:LX/0Or;

    .line 1151419
    iput-object p5, p0, LX/6r5;->e:LX/0Or;

    .line 1151420
    iput-object p6, p0, LX/6r5;->f:LX/0Or;

    .line 1151421
    return-void
.end method

.method public static b(LX/0QB;)LX/6r5;
    .locals 7

    .prologue
    .line 1151422
    new-instance v0, LX/6r5;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {p0}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    const/16 v3, 0x2d02

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x2de3

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x2d96

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x12cb

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/6r5;-><init>(Ljava/util/concurrent/Executor;Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V

    .line 1151423
    return-object v0
.end method

.method private c(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/CheckoutData;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "LX/0Px",
            "<+",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 1151424
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1151425
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    .line 1151426
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1151427
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6vb;

    .line 1151428
    iget-object v4, p0, LX/6r5;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/6wP;

    invoke-virtual {v4, v0}, LX/6wP;->a(LX/6vb;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1151429
    new-instance v5, LX/6r3;

    invoke-direct {v5, p0, v2}, LX/6r3;-><init>(LX/6r5;LX/0Pz;)V

    iget-object p1, p0, LX/6r5;->a:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1151430
    move-object v0, v4

    .line 1151431
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1151432
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1151433
    new-instance v1, LX/6r2;

    invoke-direct {v1, p0, v2}, LX/6r2;-><init>(LX/6r5;LX/0Pz;)V

    iget-object v2, p0, LX/6r5;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1151434
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5

    .prologue
    .line 1151435
    iget-object v0, p0, LX/6r5;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151436
    iget-object v0, p0, LX/6r5;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1151437
    :goto_0
    return-object v0

    .line 1151438
    :cond_0
    iget-object v0, p0, LX/6r5;->g:LX/6qb;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151439
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 1151440
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    .line 1151441
    sget-object v2, LX/6rp;->PAYMENT_METHOD:LX/6rp;

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1151442
    iget-object v2, p0, LX/6r5;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/70D;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    invoke-static {v3}, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a(LX/6xg;)LX/70C;

    move-result-object v3

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/checkout/CheckoutCommonParams;->q:Ljava/lang/String;

    .line 1151443
    iput-object v4, v3, LX/70C;->c:Ljava/lang/String;

    .line 1151444
    move-object v3, v3

    .line 1151445
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/checkout/CheckoutCommonParams;->i:Lorg/json/JSONObject;

    .line 1151446
    iput-object v4, v3, LX/70C;->d:Lorg/json/JSONObject;

    .line 1151447
    move-object v3, v3

    .line 1151448
    invoke-virtual {v3}, LX/70C;->a()Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6u3;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1151449
    new-instance v3, LX/6qz;

    invoke-direct {v3, p0}, LX/6qz;-><init>(LX/6r5;)V

    iget-object v4, p0, LX/6r5;->a:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1151450
    move-object v2, v2

    .line 1151451
    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1151452
    :cond_1
    sget-object v2, LX/6rp;->MAILING_ADDRESS:LX/6rp;

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1151453
    iget-object v2, p0, LX/6r5;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/737;

    invoke-virtual {v2}, LX/737;->g()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1151454
    new-instance v3, LX/6r0;

    invoke-direct {v3, p0}, LX/6r0;-><init>(LX/6r5;)V

    iget-object v4, p0, LX/6r5;->a:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1151455
    move-object v2, v2

    .line 1151456
    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1151457
    :cond_2
    sget-object v2, LX/6rp;->AUTHENTICATION:LX/6rp;

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1151458
    iget-object v2, p0, LX/6r5;->b:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    invoke-virtual {v2}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1151459
    new-instance v3, LX/6r1;

    invoke-direct {v3, p0}, LX/6r1;-><init>(LX/6r5;)V

    iget-object v4, p0, LX/6r5;->a:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1151460
    move-object v2, v2

    .line 1151461
    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1151462
    :cond_3
    sget-object v2, LX/6rp;->CONTACT_INFO:LX/6rp;

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1151463
    invoke-direct {p0, p1}, LX/6r5;->c(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1151464
    :cond_4
    sget-object v2, LX/6rp;->CONTACT_NAME:LX/6rp;

    invoke-virtual {v1, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1151465
    new-instance v2, Lcom/facebook/payments/contactinfo/model/NameContactInfo;

    iget-object v1, p0, LX/6r5;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/facebook/payments/contactinfo/model/NameContactInfo;-><init>(Ljava/lang/String;)V

    .line 1151466
    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1151467
    new-instance v2, LX/6r4;

    invoke-direct {v2, p0, p1}, LX/6r4;-><init>(LX/6r5;Lcom/facebook/payments/checkout/model/CheckoutData;)V

    iget-object v3, p0, LX/6r5;->a:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1151468
    move-object v1, v1

    .line 1151469
    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1151470
    :cond_5
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6r5;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1151471
    iget-object v0, p0, LX/6r5;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_0
.end method

.method public final a(LX/6qb;)V
    .locals 0

    .prologue
    .line 1151472
    iput-object p1, p0, LX/6r5;->g:LX/6qb;

    .line 1151473
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1151474
    iget-object v0, p0, LX/6r5;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    return v0
.end method
