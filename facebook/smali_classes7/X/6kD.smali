.class public LX/6kD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final addedParticipants:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6kt;",
            ">;"
        }
    .end annotation
.end field

.field public final messageMetadata:LX/6kn;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1135353
    new-instance v0, LX/1sv;

    const-string v1, "DeltaParticipantsAddedToGroupThread"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kD;->b:LX/1sv;

    .line 1135354
    new-instance v0, LX/1sw;

    const-string v1, "messageMetadata"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kD;->c:LX/1sw;

    .line 1135355
    new-instance v0, LX/1sw;

    const-string v1, "addedParticipants"

    const/16 v2, 0xf

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kD;->d:LX/1sw;

    .line 1135356
    sput-boolean v4, LX/6kD;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6kn;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6kn;",
            "Ljava/util/List",
            "<",
            "LX/6kt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1135357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1135358
    iput-object p1, p0, LX/6kD;->messageMetadata:LX/6kn;

    .line 1135359
    iput-object p2, p0, LX/6kD;->addedParticipants:Ljava/util/List;

    .line 1135360
    return-void
.end method

.method public static a(LX/6kD;)V
    .locals 4

    .prologue
    .line 1135361
    iget-object v0, p0, LX/6kD;->messageMetadata:LX/6kn;

    if-nez v0, :cond_0

    .line 1135362
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'messageMetadata\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kD;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1135363
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1135364
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1135365
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 1135366
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1135367
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaParticipantsAddedToGroupThread"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1135368
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135369
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135370
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135371
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135372
    const-string v4, "messageMetadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135373
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135374
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135375
    iget-object v4, p0, LX/6kD;->messageMetadata:LX/6kn;

    if-nez v4, :cond_4

    .line 1135376
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135377
    :goto_3
    iget-object v4, p0, LX/6kD;->addedParticipants:Ljava/util/List;

    if-eqz v4, :cond_0

    .line 1135378
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135379
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135380
    const-string v4, "addedParticipants"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135381
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135382
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135383
    iget-object v0, p0, LX/6kD;->addedParticipants:Ljava/util/List;

    if-nez v0, :cond_5

    .line 1135384
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135385
    :cond_0
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135386
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135387
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1135388
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1135389
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1135390
    :cond_3
    const-string v0, ""

    goto/16 :goto_2

    .line 1135391
    :cond_4
    iget-object v4, p0, LX/6kD;->messageMetadata:LX/6kn;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1135392
    :cond_5
    iget-object v0, p0, LX/6kD;->addedParticipants:Ljava/util/List;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1135393
    invoke-static {p0}, LX/6kD;->a(LX/6kD;)V

    .line 1135394
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1135395
    iget-object v0, p0, LX/6kD;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_0

    .line 1135396
    sget-object v0, LX/6kD;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135397
    iget-object v0, p0, LX/6kD;->messageMetadata:LX/6kn;

    invoke-virtual {v0, p1}, LX/6kn;->a(LX/1su;)V

    .line 1135398
    :cond_0
    iget-object v0, p0, LX/6kD;->addedParticipants:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1135399
    iget-object v0, p0, LX/6kD;->addedParticipants:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1135400
    sget-object v0, LX/6kD;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135401
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/6kD;->addedParticipants:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1135402
    iget-object v0, p0, LX/6kD;->addedParticipants:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6kt;

    .line 1135403
    invoke-virtual {v0, p1}, LX/6kt;->a(LX/1su;)V

    goto :goto_0

    .line 1135404
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1135405
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1135406
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1135407
    if-nez p1, :cond_1

    .line 1135408
    :cond_0
    :goto_0
    return v0

    .line 1135409
    :cond_1
    instance-of v1, p1, LX/6kD;

    if-eqz v1, :cond_0

    .line 1135410
    check-cast p1, LX/6kD;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1135411
    if-nez p1, :cond_3

    .line 1135412
    :cond_2
    :goto_1
    move v0, v2

    .line 1135413
    goto :goto_0

    .line 1135414
    :cond_3
    iget-object v0, p0, LX/6kD;->messageMetadata:LX/6kn;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1135415
    :goto_2
    iget-object v3, p1, LX/6kD;->messageMetadata:LX/6kn;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1135416
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1135417
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135418
    iget-object v0, p0, LX/6kD;->messageMetadata:LX/6kn;

    iget-object v3, p1, LX/6kD;->messageMetadata:LX/6kn;

    invoke-virtual {v0, v3}, LX/6kn;->a(LX/6kn;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1135419
    :cond_5
    iget-object v0, p0, LX/6kD;->addedParticipants:Ljava/util/List;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1135420
    :goto_4
    iget-object v3, p1, LX/6kD;->addedParticipants:Ljava/util/List;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1135421
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1135422
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135423
    iget-object v0, p0, LX/6kD;->addedParticipants:Ljava/util/List;

    iget-object v3, p1, LX/6kD;->addedParticipants:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1135424
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1135425
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1135426
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1135427
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1135428
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1135429
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1135430
    sget-boolean v0, LX/6kD;->a:Z

    .line 1135431
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kD;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1135432
    return-object v0
.end method
