.class public LX/8Sf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6LO;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/common/banner/BannerNotification;",
            ">;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile b:LX/8Sf;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1347235
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-class v1, LX/8Br;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-class v1, LX/8Se;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/8Sf;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1347236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1347237
    return-void
.end method

.method public static a(LX/0QB;)LX/8Sf;
    .locals 3

    .prologue
    .line 1347238
    sget-object v0, LX/8Sf;->b:LX/8Sf;

    if-nez v0, :cond_1

    .line 1347239
    const-class v1, LX/8Sf;

    monitor-enter v1

    .line 1347240
    :try_start_0
    sget-object v0, LX/8Sf;->b:LX/8Sf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1347241
    if-eqz v2, :cond_0

    .line 1347242
    :try_start_1
    new-instance v0, LX/8Sf;

    invoke-direct {v0}, LX/8Sf;-><init>()V

    .line 1347243
    move-object v0, v0

    .line 1347244
    sput-object v0, LX/8Sf;->b:LX/8Sf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1347245
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1347246
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1347247
    :cond_1
    sget-object v0, LX/8Sf;->b:LX/8Sf;

    return-object v0

    .line 1347248
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1347249
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/common/banner/BannerNotification;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1347250
    sget-object v0, LX/8Sf;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
