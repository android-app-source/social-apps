.class public final LX/8Iy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1327033
    const/4 v8, 0x0

    .line 1327034
    const/4 v7, 0x0

    .line 1327035
    const/4 v6, 0x0

    .line 1327036
    const-wide/16 v4, 0x0

    .line 1327037
    const/4 v3, 0x0

    .line 1327038
    const/4 v2, 0x0

    .line 1327039
    const/4 v1, 0x0

    .line 1327040
    const/4 v0, 0x0

    .line 1327041
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_c

    .line 1327042
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1327043
    const/4 v0, 0x0

    .line 1327044
    :goto_0
    return v0

    .line 1327045
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v10, :cond_a

    .line 1327046
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 1327047
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1327048
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v1, :cond_0

    .line 1327049
    const-string v10, "actors"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1327050
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1327051
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_1

    .line 1327052
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_1

    .line 1327053
    invoke-static {p0, p1}, LX/8Ir;->b(LX/15w;LX/186;)I

    move-result v9

    .line 1327054
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1327055
    :cond_1
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1327056
    move v9, v1

    goto :goto_1

    .line 1327057
    :cond_2
    const-string v10, "attachments"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1327058
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1327059
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v10, :cond_3

    .line 1327060
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v10, :cond_3

    .line 1327061
    invoke-static {p0, p1}, LX/8Iw;->b(LX/15w;LX/186;)I

    move-result v5

    .line 1327062
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1327063
    :cond_3
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1327064
    move v5, v1

    goto :goto_1

    .line 1327065
    :cond_4
    const-string v10, "cache_id"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1327066
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v4, v1

    goto/16 :goto_1

    .line 1327067
    :cond_5
    const-string v10, "creation_time"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1327068
    const/4 v0, 0x1

    .line 1327069
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto/16 :goto_1

    .line 1327070
    :cond_6
    const-string v10, "feedback"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1327071
    invoke-static {p0, p1}, LX/8J6;->a(LX/15w;LX/186;)I

    move-result v1

    move v8, v1

    goto/16 :goto_1

    .line 1327072
    :cond_7
    const-string v10, "shareable"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1327073
    invoke-static {p0, p1}, LX/8Ix;->a(LX/15w;LX/186;)I

    move-result v1

    move v7, v1

    goto/16 :goto_1

    .line 1327074
    :cond_8
    const-string v10, "tracking"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1327075
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 1327076
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1327077
    :cond_a
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1327078
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1327079
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1327080
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1327081
    if-eqz v0, :cond_b

    .line 1327082
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1327083
    :cond_b
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1327084
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1327085
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1327086
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_c
    move v9, v8

    move v8, v3

    move v12, v2

    move-wide v2, v4

    move v4, v6

    move v5, v7

    move v7, v12

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 1327087
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1327088
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327089
    if-eqz v0, :cond_1

    .line 1327090
    const-string v1, "actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327091
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1327092
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 1327093
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2}, LX/8Ir;->a(LX/15i;ILX/0nX;)V

    .line 1327094
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1327095
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1327096
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327097
    if-eqz v0, :cond_3

    .line 1327098
    const-string v1, "attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327099
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1327100
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 1327101
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/8Iw;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1327102
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1327103
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1327104
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1327105
    if-eqz v0, :cond_4

    .line 1327106
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327107
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1327108
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1327109
    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 1327110
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327111
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1327112
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327113
    if-eqz v0, :cond_6

    .line 1327114
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327115
    invoke-static {p0, v0, p2, p3}, LX/8J6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1327116
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1327117
    if-eqz v0, :cond_7

    .line 1327118
    const-string v1, "shareable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327119
    invoke-static {p0, v0, p2}, LX/8Ix;->a(LX/15i;ILX/0nX;)V

    .line 1327120
    :cond_7
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1327121
    if-eqz v0, :cond_8

    .line 1327122
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1327123
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1327124
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1327125
    return-void
.end method
