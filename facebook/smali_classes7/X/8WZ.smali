.class public LX/8WZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8TS;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/quicksilver/dataloader/QuicksilverComponentDataProvider;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/8TY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8T9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1354219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1354220
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1354221
    iput-object v0, p0, LX/8WZ;->a:LX/0Ot;

    .line 1354222
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1354223
    iput-object v0, p0, LX/8WZ;->b:LX/0Ot;

    .line 1354224
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1354225
    iput-object v0, p0, LX/8WZ;->c:LX/0Ot;

    .line 1354226
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1354227
    iput-object v0, p0, LX/8WZ;->e:LX/0Ot;

    .line 1354228
    return-void
.end method

.method public static a(LX/0QB;)LX/8WZ;
    .locals 8

    .prologue
    .line 1354229
    const-class v1, LX/8WZ;

    monitor-enter v1

    .line 1354230
    :try_start_0
    sget-object v0, LX/8WZ;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1354231
    sput-object v2, LX/8WZ;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1354232
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1354233
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1354234
    new-instance v3, LX/8WZ;

    invoke-direct {v3}, LX/8WZ;-><init>()V

    .line 1354235
    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3074

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x307b

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/8TY;->a(LX/0QB;)LX/8TY;

    move-result-object v7

    check-cast v7, LX/8TY;

    const/16 p0, 0x306f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1354236
    iput-object v4, v3, LX/8WZ;->a:LX/0Ot;

    iput-object v5, v3, LX/8WZ;->b:LX/0Ot;

    iput-object v6, v3, LX/8WZ;->c:LX/0Ot;

    iput-object v7, v3, LX/8WZ;->d:LX/8TY;

    iput-object p0, v3, LX/8WZ;->e:LX/0Ot;

    .line 1354237
    move-object v0, v3

    .line 1354238
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1354239
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8WZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1354240
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1354241
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1354242
    iget-object v0, p0, LX/8WZ;->d:LX/8TY;

    .line 1354243
    iget-object v1, v0, LX/8TY;->a:LX/0Uh;

    const/16 v2, 0x8e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1354244
    if-nez v0, :cond_0

    .line 1354245
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1354246
    :goto_0
    return-void

    .line 1354247
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1354248
    new-instance v0, LX/8WY;

    invoke-direct {v0, p0}, LX/8WY;-><init>(LX/8WZ;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
