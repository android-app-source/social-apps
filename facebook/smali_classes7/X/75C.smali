.class public LX/75C;
.super LX/759;
.source ""


# instance fields
.field public final a:J


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 1169052
    invoke-direct {p0}, LX/759;-><init>()V

    .line 1169053
    iput-wide p1, p0, LX/75C;->a:J

    .line 1169054
    return-void
.end method


# virtual methods
.method public final m()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 1169047
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "tag_uid"

    iget-wide v2, p0, LX/75C;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1169048
    :goto_0
    return-object v0

    .line 1169049
    :catch_0
    move-exception v0

    .line 1169050
    const-string v1, ""

    const-string v2, "inconceivable JSON exception"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1169051
    const/4 v0, 0x0

    goto :goto_0
.end method
