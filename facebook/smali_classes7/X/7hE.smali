.class public LX/7hE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:I

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Z

.field public final n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/7hG;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0Px;ZLX/0Px;LX/0Px;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "LX/0Px",
            "<",
            "LX/7hG;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1225803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225804
    iput-object p1, p0, LX/7hE;->a:Ljava/lang/String;

    .line 1225805
    iput-object p2, p0, LX/7hE;->b:Ljava/lang/String;

    .line 1225806
    iput-object p3, p0, LX/7hE;->d:Ljava/lang/String;

    .line 1225807
    iput-object p4, p0, LX/7hE;->e:Ljava/lang/String;

    .line 1225808
    iput-object p5, p0, LX/7hE;->f:Ljava/lang/String;

    .line 1225809
    iput-object p6, p0, LX/7hE;->g:Ljava/lang/String;

    .line 1225810
    iput-object p7, p0, LX/7hE;->h:Ljava/lang/String;

    .line 1225811
    iput p8, p0, LX/7hE;->i:I

    .line 1225812
    iput-object p9, p0, LX/7hE;->j:Ljava/lang/String;

    .line 1225813
    iput-object p10, p0, LX/7hE;->k:Ljava/lang/String;

    .line 1225814
    iput-object p11, p0, LX/7hE;->l:LX/0Px;

    .line 1225815
    iput-boolean p12, p0, LX/7hE;->m:Z

    .line 1225816
    iput-object p13, p0, LX/7hE;->n:LX/0Px;

    .line 1225817
    iput-object p14, p0, LX/7hE;->o:LX/0Px;

    .line 1225818
    iput-object p15, p0, LX/7hE;->c:Ljava/lang/String;

    .line 1225819
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0Px;ZLX/0Px;LX/0Px;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 1225820
    invoke-direct/range {p0 .. p15}, LX/7hE;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;LX/0Px;ZLX/0Px;LX/0Px;Ljava/lang/String;)V

    return-void
.end method
