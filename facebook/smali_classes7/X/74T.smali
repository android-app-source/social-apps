.class public final enum LX/74T;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/74T;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/74T;

.field public static final enum COMPOSER_TAGGING_WITH_TAG:LX/74T;

.field public static final enum COMPOSER_TAGGING_XY_TAG:LX/74T;

.field public static final enum CONSUMPTION:LX/74T;

.field public static final enum POST_POST:LX/74T;

.field public static final enum PRODUCTION:LX/74T;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1168082
    new-instance v0, LX/74T;

    const-string v1, "CONSUMPTION"

    invoke-direct {v0, v1, v2}, LX/74T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74T;->CONSUMPTION:LX/74T;

    .line 1168083
    new-instance v0, LX/74T;

    const-string v1, "PRODUCTION"

    invoke-direct {v0, v1, v3}, LX/74T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74T;->PRODUCTION:LX/74T;

    .line 1168084
    new-instance v0, LX/74T;

    const-string v1, "POST_POST"

    invoke-direct {v0, v1, v4}, LX/74T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74T;->POST_POST:LX/74T;

    .line 1168085
    new-instance v0, LX/74T;

    const-string v1, "COMPOSER_TAGGING_WITH_TAG"

    invoke-direct {v0, v1, v5}, LX/74T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74T;->COMPOSER_TAGGING_WITH_TAG:LX/74T;

    .line 1168086
    new-instance v0, LX/74T;

    const-string v1, "COMPOSER_TAGGING_XY_TAG"

    invoke-direct {v0, v1, v6}, LX/74T;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/74T;->COMPOSER_TAGGING_XY_TAG:LX/74T;

    .line 1168087
    const/4 v0, 0x5

    new-array v0, v0, [LX/74T;

    sget-object v1, LX/74T;->CONSUMPTION:LX/74T;

    aput-object v1, v0, v2

    sget-object v1, LX/74T;->PRODUCTION:LX/74T;

    aput-object v1, v0, v3

    sget-object v1, LX/74T;->POST_POST:LX/74T;

    aput-object v1, v0, v4

    sget-object v1, LX/74T;->COMPOSER_TAGGING_WITH_TAG:LX/74T;

    aput-object v1, v0, v5

    sget-object v1, LX/74T;->COMPOSER_TAGGING_XY_TAG:LX/74T;

    aput-object v1, v0, v6

    sput-object v0, LX/74T;->$VALUES:[LX/74T;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1168088
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/74T;
    .locals 1

    .prologue
    .line 1168089
    const-class v0, LX/74T;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/74T;

    return-object v0
.end method

.method public static values()[LX/74T;
    .locals 1

    .prologue
    .line 1168090
    sget-object v0, LX/74T;->$VALUES:[LX/74T;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/74T;

    return-object v0
.end method
