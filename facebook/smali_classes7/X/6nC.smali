.class public LX/6nC;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public static $ul_$xXXandroid_content_Context$xXXFACTORY_METHOD(LX/0QB;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 1147176
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_analytics_CounterLogger$xXXFACTORY_METHOD(LX/0QB;)LX/13Q;
    .locals 1

    .prologue
    .line 1147175
    invoke-static {p0}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v0

    check-cast v0, LX/13Q;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_auth_datastore_LoggedInUserAuthDataStore$xXXFACTORY_METHOD(LX/0QB;)LX/0WJ;
    .locals 1

    .prologue
    .line 1147174
    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v0

    check-cast v0, LX/0WJ;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_common_errorreporting_FbErrorReporter$xXXFACTORY_METHOD(LX/0QB;)LX/03V;
    .locals 1

    .prologue
    .line 1147173
    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_common_time_SystemClock$xXXFACTORY_METHOD(LX/0QB;)LX/0SF;
    .locals 1

    .prologue
    .line 1147172
    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SF;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_config_application_FbAppType$xXXFACTORY_METHOD(LX/0QB;)LX/00H;
    .locals 1

    .prologue
    .line 1147171
    const-class v0, LX/00H;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00H;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_gk_store_GatekeeperStore$xXXFACTORY_METHOD(LX/0QB;)LX/0Uh;
    .locals 1

    .prologue
    .line 1147170
    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_inject_Lazy$x3Ccom_facebook_omnistore_module_OmnistoreComponentManager$x3E$xXXFACTORY_METHOD(LX/0QB;)LX/0Ot;
    .locals 1

    .prologue
    .line 1147169
    const/16 v0, 0xea6

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_mobileconfig_factory_MobileConfigFactory$xXXFACTORY_METHOD(LX/0QB;)LX/0W3;
    .locals 1

    .prologue
    .line 1147168
    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v0

    check-cast v0, LX/0W3;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_omnistore_MqttProtocolProvider$xXXcom_facebook_omnistore_module_OverrideMqttProtocolProvider$xXXFACTORY_METHOD(LX/0QB;)Lcom/facebook/omnistore/MqttProtocolProvider;
    .locals 1

    .prologue
    .line 1147155
    invoke-static {p0}, LX/2Pm;->getInstance__com_facebook_omnistore_MqttProtocolProvider__INJECTED_BY_TemplateInjector(LX/0QB;)Lcom/facebook/omnistore/MqttProtocolProvider;

    move-result-object v0

    check-cast v0, Lcom/facebook/omnistore/MqttProtocolProvider;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_omnistore_OmnistoreErrorReporter$xXXFACTORY_METHOD(LX/0QB;)Lcom/facebook/omnistore/OmnistoreErrorReporter;
    .locals 1

    .prologue
    .line 1147167
    invoke-static {p0}, LX/2Pq;->getInstance__com_facebook_omnistore_logger_FbOmnistoreErrorReporter__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pq;

    move-result-object v0

    check-cast v0, Lcom/facebook/omnistore/OmnistoreErrorReporter;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_omnistore_module_DefaultOmnistoreOpener$xXXFACTORY_METHOD(LX/0QB;)LX/2Pl;
    .locals 1

    .prologue
    .line 1147166
    invoke-static {p0}, LX/2Pl;->createInstance__com_facebook_omnistore_module_DefaultOmnistoreOpener__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pl;

    move-result-object v0

    check-cast v0, LX/2Pl;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_omnistore_module_OmnistoreInitTimeBugReportInfo$xXXFACTORY_METHOD(LX/0QB;)LX/2Pt;
    .locals 1

    .prologue
    .line 1147165
    invoke-static {p0}, LX/2Pt;->getInstance__com_facebook_omnistore_module_OmnistoreInitTimeBugReportInfo__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pt;

    move-result-object v0

    check-cast v0, LX/2Pt;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_omnistore_module_OmnistoreOpener$xXXFACTORY_METHOD(LX/0QB;)LX/2Pl;
    .locals 1

    .prologue
    .line 1147164
    invoke-static {p0}, LX/2Pl;->createInstance__com_facebook_omnistore_module_DefaultOmnistoreOpener__INJECTED_BY_TemplateInjector(LX/0QB;)LX/2Pl;

    move-result-object v0

    check-cast v0, LX/2Pl;

    return-object v0
.end method

.method public static $ul_$xXXcom_facebook_xanalytics_XAnalyticsProvider$xXXFACTORY_METHOD(LX/0QB;)LX/29a;
    .locals 1

    .prologue
    .line 1147163
    invoke-static {p0}, LX/29a;->a(LX/0QB;)LX/29a;

    move-result-object v0

    check-cast v0, LX/29a;

    return-object v0
.end method

.method public static $ul_$xXXjava_util_concurrent_ExecutorService$xXXcom_facebook_common_idleexecutor_DefaultIdleExecutor$xXXFACTORY_METHOD(LX/0QB;)Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 1147162
    invoke-static {p0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public static $ul_$xXXjava_util_concurrent_ScheduledExecutorService$xXXcom_facebook_common_executors_BackgroundExecutorService$xXXFACTORY_METHOD(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    .prologue
    .line 1147161
    invoke-static {p0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method public static $ul_$xXXjavax_inject_Provider$x3Ccom_facebook_omnistore_module_OmnistoreComponentManager$x3E$xXXFACTORY_METHOD(LX/0QB;)LX/0Or;
    .locals 1

    .prologue
    .line 1147160
    const/16 v0, 0xea6

    invoke-static {p0, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public static $ul_$xXXjavax_inject_Provider$x3Cjava_lang_String$x3E$xXXcom_facebook_auth_annotations_ViewerContextUserId$xXXFACTORY_METHOD(LX/0QB;)LX/0Or;
    .locals 1

    .prologue
    .line 1147159
    const/16 v0, 0x15e8

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    return-object v0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1147157
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 1147158
    return-void
.end method


# virtual methods
.method public configure()V
    .locals 1

    .prologue
    .line 1147156
    return-void
.end method
