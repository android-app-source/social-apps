.class public final LX/7XZ;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/11w;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/7XZ;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/11w;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1218515
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 1218516
    return-void
.end method

.method public static a(LX/0QB;)LX/7XZ;
    .locals 4

    .prologue
    .line 1218517
    sget-object v0, LX/7XZ;->a:LX/7XZ;

    if-nez v0, :cond_1

    .line 1218518
    const-class v1, LX/7XZ;

    monitor-enter v1

    .line 1218519
    :try_start_0
    sget-object v0, LX/7XZ;->a:LX/7XZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1218520
    if-eqz v2, :cond_0

    .line 1218521
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1218522
    new-instance v3, LX/7XZ;

    const/16 p0, 0x13f0

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/7XZ;-><init>(LX/0Ot;)V

    .line 1218523
    move-object v0, v3

    .line 1218524
    sput-object v0, LX/7XZ;->a:LX/7XZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1218525
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1218526
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1218527
    :cond_1
    sget-object v0, LX/7XZ;->a:LX/7XZ;

    return-object v0

    .line 1218528
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1218529
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1218530
    check-cast p3, LX/11w;

    .line 1218531
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1218532
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1218533
    invoke-virtual {p3}, LX/11w;->a()V

    .line 1218534
    :cond_0
    return-void
.end method
