.class public LX/6rL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ev;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ev",
        "<",
        "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6sY;

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:LX/6r9;

.field private d:LX/6qh;

.field public e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

.field public f:LX/6qd;

.field private g:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/6sY;Ljava/util/concurrent/Executor;LX/6r9;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1152093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152094
    iput-object p1, p0, LX/6rL;->a:LX/6sY;

    .line 1152095
    iput-object p2, p0, LX/6rL;->b:Ljava/util/concurrent/Executor;

    .line 1152096
    iput-object p3, p0, LX/6rL;->c:LX/6r9;

    .line 1152097
    return-void
.end method

.method public static b(LX/0QB;)LX/6rL;
    .locals 4

    .prologue
    .line 1152098
    new-instance v3, LX/6rL;

    invoke-static {p0}, LX/6sY;->b(LX/0QB;)LX/6sY;

    move-result-object v0

    check-cast v0, LX/6sY;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/6r9;->a(LX/0QB;)LX/6r9;

    move-result-object v2

    check-cast v2, LX/6r9;

    invoke-direct {v3, v0, v1, v2}, LX/6rL;-><init>(LX/6sY;Ljava/util/concurrent/Executor;LX/6r9;)V

    .line 1152099
    return-object v3
.end method


# virtual methods
.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1152088
    iput-object p1, p0, LX/6rL;->d:LX/6qh;

    .line 1152089
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1152090
    const-string v1, "selected_mailing_address"

    iget-object v0, p0, LX/6rL;->h:LX/0am;

    invoke-static {v0}, LX/47j;->a(LX/0am;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1152091
    return-void

    .line 1152092
    :cond_0
    iget-object v0, p0, LX/6rL;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    goto :goto_0
.end method

.method public final bridge synthetic a(Landroid/os/Bundle;Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 0

    .prologue
    .line 1152087
    invoke-virtual {p0, p1}, LX/6rL;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 4

    .prologue
    .line 1152057
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    const/4 v1, 0x1

    .line 1152058
    iput-object p1, p0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 1152059
    iget-object v0, p0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {v0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->d:LX/6re;

    sget-object v2, LX/6re;->UPDATE_CHECKOUT_API:LX/6re;

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152060
    iget-object v0, p0, LX/6rL;->h:LX/0am;

    iget-object v2, p0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {v2}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->h()LX/0am;

    move-result-object v2

    if-eq v0, v2, :cond_6

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1152061
    if-nez v0, :cond_1

    .line 1152062
    :goto_2
    return-void

    .line 1152063
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1152064
    :cond_1
    iget-object v0, p0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {v0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->h()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/6rL;->h:LX/0am;

    .line 1152065
    iget-object v0, p0, LX/6rL;->c:LX/6r9;

    iget-object v2, p0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {v2}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v2}, LX/6r9;->b(LX/6qw;)LX/6qd;

    move-result-object v0

    iput-object v0, p0, LX/6rL;->f:LX/6qd;

    .line 1152066
    iget-object v0, p0, LX/6rL;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1152067
    iget-object v0, p0, LX/6rL;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1152068
    :cond_2
    iget-object v0, p0, LX/6rL;->f:LX/6qd;

    iget-object v2, p0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    sget-object v3, LX/6rp;->SHIPPING_OPTION:LX/6rp;

    invoke-static {v3}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v3

    invoke-interface {v0, v2, v3}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Rf;)V

    .line 1152069
    iget-object v0, p0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {v0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    .line 1152070
    iget-object v0, p0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {v0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v3, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    invoke-static {v0, v3}, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xg;)LX/6sd;

    move-result-object v0

    iget-object v3, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->p:Ljava/lang/String;

    .line 1152071
    iput-object v3, v0, LX/6sd;->c:Ljava/lang/String;

    .line 1152072
    move-object v0, v0

    .line 1152073
    iget-object v3, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->A:LX/0m9;

    .line 1152074
    iput-object v3, v0, LX/6sd;->h:LX/0m9;

    .line 1152075
    move-object v3, v0

    .line 1152076
    iget-object v0, p0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {v0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->h()LX/0am;

    move-result-object v0

    invoke-static {v0}, LX/47j;->a(LX/0am;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1152077
    iget-object v0, p0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {v0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->h()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/MailingAddress;->a()Ljava/lang/String;

    move-result-object v0

    .line 1152078
    iput-object v0, v3, LX/6sd;->n:Ljava/lang/String;

    .line 1152079
    :cond_3
    iget-object v0, p0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {v0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->j()LX/0am;

    move-result-object v0

    invoke-static {v0}, LX/47j;->a(LX/0am;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1152080
    iget-object v0, p0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {v0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->j()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/ShippingOption;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingOption;->a()Ljava/lang/String;

    move-result-object v0

    .line 1152081
    iput-object v0, v3, LX/6sd;->o:Ljava/lang/String;

    .line 1152082
    :cond_4
    iget-object v0, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    sget-object v2, LX/6rp;->CHECKOUT_OPTIONS:LX/6rp;

    invoke-virtual {v0, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1152083
    iget-object v0, p0, LX/6rL;->e:Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-static {v0, v3}, LX/6rI;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6sd;)V

    .line 1152084
    :cond_5
    iget-object v0, p0, LX/6rL;->a:LX/6sY;

    invoke-virtual {v3}, LX/6sd;->a()Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6rL;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1152085
    iget-object v0, p0, LX/6rL;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/6rK;

    invoke-direct {v2, p0}, LX/6rK;-><init>(LX/6rL;)V

    iget-object v3, p0, LX/6rL;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1152086
    iget-object v0, p0, LX/6rL;->d:LX/6qh;

    iget-object v2, p0, LX/6rL;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-virtual {v0, v2, v1}, LX/6qh;->a(Lcom/google/common/util/concurrent/ListenableFuture;Z)V

    goto/16 :goto_2

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1152054
    if-nez p1, :cond_0

    .line 1152055
    :goto_0
    return-void

    .line 1152056
    :cond_0
    const-string v0, "selected_mailing_address"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/6rL;->h:LX/0am;

    goto :goto_0
.end method

.method public final bridge synthetic b(Landroid/os/Bundle;Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 0

    .prologue
    .line 1152053
    invoke-virtual {p0, p1}, LX/6rL;->b(Landroid/os/Bundle;)V

    return-void
.end method
