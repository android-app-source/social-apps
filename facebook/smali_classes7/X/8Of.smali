.class public LX/8Of;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/7TG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/74n;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/7St;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Fp;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1339828
    const-class v0, LX/8Of;

    sput-object v0, LX/8Of;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1339829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1339830
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1339831
    iput-object v0, p0, LX/8Of;->f:LX/0Ot;

    .line 1339832
    return-void
.end method

.method public static a(LX/0QB;)LX/8Of;
    .locals 1

    .prologue
    .line 1339833
    invoke-static {p0}, LX/8Of;->b(LX/0QB;)LX/8Of;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8Of;
    .locals 6

    .prologue
    .line 1339834
    new-instance v0, LX/8Of;

    invoke-direct {v0}, LX/8Of;-><init>()V

    .line 1339835
    invoke-static {p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, LX/0TD;

    invoke-static {p0}, LX/7TG;->a(LX/0QB;)LX/7TG;

    move-result-object v2

    check-cast v2, LX/7TG;

    invoke-static {p0}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v3

    check-cast v3, LX/74n;

    invoke-static {p0}, LX/7St;->a(LX/0QB;)LX/7St;

    move-result-object v4

    check-cast v4, LX/7St;

    const/16 v5, 0x2e24

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    .line 1339836
    iput-object v1, v0, LX/8Of;->b:LX/0TD;

    iput-object v2, v0, LX/8Of;->c:LX/7TG;

    iput-object v3, v0, LX/8Of;->d:LX/74n;

    iput-object v4, v0, LX/8Of;->e:LX/7St;

    iput-object v5, v0, LX/8Of;->f:LX/0Ot;

    .line 1339837
    return-object v0
.end method


# virtual methods
.method public final a(LX/0Px;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/videocodec/effects/common/GLRendererConfig;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/61B;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1339838
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1339839
    new-instance v0, LX/7SP;

    invoke-direct {v0}, LX/7SP;-><init>()V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1339840
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/videocodec/effects/common/GLRendererConfig;

    .line 1339841
    iget-object v1, p0, LX/8Of;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Fp;

    .line 1339842
    instance-of v5, v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 1339843
    check-cast v0, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    .line 1339844
    iget-object v5, v1, LX/8Fp;->b:LX/8Fr;

    invoke-virtual {v5, v0}, LX/8Fr;->a(Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;)LX/8Fq;

    move-result-object v5

    move-object v0, v5

    .line 1339845
    check-cast v0, LX/8Fq;

    .line 1339846
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1339847
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1339848
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
