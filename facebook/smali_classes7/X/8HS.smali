.class public LX/8HS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1321029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321030
    return-void
.end method

.method public static a(LX/0QB;)LX/8HS;
    .locals 1

    .prologue
    .line 1320994
    new-instance v0, LX/8HS;

    invoke-direct {v0}, LX/8HS;-><init>()V

    .line 1320995
    move-object v0, v0

    .line 1320996
    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1321028
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    const-string p0, " "

    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1320999
    check-cast p1, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;

    .line 1321000
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1321001
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1321002
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "aid"

    iget-object v3, p1, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1321003
    iget-object v1, p1, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1321004
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "name"

    iget-object v3, p1, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1321005
    :cond_0
    iget-object v1, p1, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1321006
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "place"

    iget-object v3, p1, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->c:Ljava/lang/String;

    invoke-static {v3}, LX/8HS;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1321007
    :cond_1
    iget-object v1, p1, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1321008
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "description"

    iget-object v3, p1, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->d:Ljava/lang/String;

    invoke-static {v3}, LX/8HS;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1321009
    :cond_2
    iget-object v1, p1, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1321010
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "audience"

    iget-object v3, p1, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->e:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1321011
    :cond_3
    iget-object v1, p1, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    iget-object v1, p1, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->f:LX/8HU;

    sget-object v2, LX/8HU;->NORMAL_TO_SHARED:LX/8HU;

    if-eq v1, v2, :cond_4

    .line 1321012
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "allow_contributors"

    iget-object v3, p1, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->g:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1321013
    :cond_4
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "editPhotoAlbum"

    .line 1321014
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1321015
    move-object v1, v1

    .line 1321016
    const-string v2, "POST"

    .line 1321017
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1321018
    move-object v1, v1

    .line 1321019
    iget-object v2, p1, Lcom/facebook/photos/data/method/UpdatePhotoAlbumParams;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1321020
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1321021
    move-object v1, v1

    .line 1321022
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1321023
    move-object v0, v1

    .line 1321024
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1321025
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1321026
    move-object v0, v0

    .line 1321027
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1320997
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1320998
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
