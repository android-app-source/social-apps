.class public LX/6w2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6w1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6w1",
        "<",
        "Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/1Ck;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6wP;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/70k;


# direct methods
.method public constructor <init>(LX/1Ck;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ck;",
            "LX/0Or",
            "<",
            "LX/6wP;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1157395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157396
    iput-object p1, p0, LX/6w2;->a:LX/1Ck;

    .line 1157397
    iput-object p2, p0, LX/6w2;->b:LX/0Or;

    .line 1157398
    return-void
.end method

.method private a(LX/6zj;LX/0Pz;LX/6vb;Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6zj;",
            "LX/0Pz",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;",
            "LX/6vb;",
            "Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1157388
    new-instance v1, LX/6w0;

    invoke-direct {v1, p0, p2, p1, p4}, LX/6w0;-><init>(LX/6w2;LX/0Pz;LX/6zj;Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;)V

    .line 1157389
    iget-object v2, p0, LX/6w2;->a:LX/1Ck;

    const-string v3, "contact_info_task_key"

    .line 1157390
    iget-object v0, p4, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    move-object v0, v0

    .line 1157391
    check-cast v0, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;

    .line 1157392
    iget-boolean p1, v0, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;->a:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, LX/6w2;->b:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/6wP;

    invoke-virtual {p1, p3}, LX/6wP;->a(LX/6vb;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    :goto_0
    move-object v0, p1

    .line 1157393
    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1157394
    return-void

    :cond_0
    iget-object p1, p0, LX/6w2;->b:LX/0Or;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/6wP;

    invoke-virtual {p1, p3}, LX/6wP;->b(LX/6vb;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1157386
    iget-object v0, p0, LX/6w2;->a:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1157387
    return-void
.end method

.method public final a(LX/6zj;Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;)V
    .locals 3

    .prologue
    .line 1157399
    iget-object v0, p0, LX/6w2;->c:LX/70k;

    invoke-virtual {v0}, LX/70k;->a()V

    .line 1157400
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1157401
    invoke-virtual {p2}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    .line 1157402
    iget-object v0, v0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6vb;

    .line 1157403
    invoke-direct {p0, p1, v1, v0, p2}, LX/6w2;->a(LX/6zj;LX/0Pz;LX/6vb;Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;)V

    goto :goto_0

    .line 1157404
    :cond_0
    return-void
.end method

.method public final bridge synthetic a(LX/6zj;Lcom/facebook/payments/picker/model/PickerRunTimeData;)V
    .locals 0

    .prologue
    .line 1157385
    check-cast p2, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;

    invoke-virtual {p0, p1, p2}, LX/6w2;->a(LX/6zj;Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerRunTimeData;)V

    return-void
.end method

.method public final a(LX/70k;)V
    .locals 0

    .prologue
    .line 1157383
    iput-object p1, p0, LX/6w2;->c:LX/70k;

    .line 1157384
    return-void
.end method

.method public final bridge synthetic b(LX/6zj;Lcom/facebook/payments/picker/model/PickerRunTimeData;)V
    .locals 0

    .prologue
    .line 1157382
    return-void
.end method
