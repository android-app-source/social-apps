.class public LX/8Mh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/7ma;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7mW;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/8Mb;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/7ma;LX/0Ot;LX/8Mb;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7ma;",
            "LX/0Ot",
            "<",
            "LX/7mW;",
            ">;",
            "LX/8Mb;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1334991
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1334992
    iput-object p1, p0, LX/8Mh;->a:LX/7ma;

    .line 1334993
    iput-object p2, p0, LX/8Mh;->b:LX/0Ot;

    .line 1334994
    iput-object p3, p0, LX/8Mh;->c:LX/8Mb;

    .line 1334995
    iput-object p4, p0, LX/8Mh;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1334996
    iput-object p5, p0, LX/8Mh;->e:Landroid/content/Context;

    .line 1334997
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3

    .prologue
    .line 1334998
    iget-object v0, p0, LX/8Mh;->c:LX/8Mb;

    invoke-virtual {v0, p1}, LX/8Mb;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1334999
    iget-object v0, p0, LX/8Mh;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f082058

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1335000
    const/4 v0, 0x0

    .line 1335001
    :goto_0
    move-object v0, v0

    .line 1335002
    if-nez v0, :cond_0

    .line 1335003
    :goto_1
    return-void

    .line 1335004
    :cond_0
    iget-object v1, p0, LX/8Mh;->e:Landroid/content/Context;

    sget-object v2, LX/8Lr;->DRAFT_FEED_ENTRY_POINT:LX/8Lr;

    invoke-virtual {v0}, LX/7mi;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, v2, p1}, Lcom/facebook/photos/upload/progresspage/CompostActivity;->a(Landroid/content/Context;LX/8Lr;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1335005
    iget-object v2, p0, LX/8Mh;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object p1, p0, LX/8Mh;->e:Landroid/content/Context;

    invoke-interface {v2, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1335006
    goto :goto_1

    .line 1335007
    :cond_1
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1335008
    iget-object v1, p0, LX/8Mh;->c:LX/8Mb;

    invoke-virtual {v1, p1}, LX/8Mb;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1335009
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LX/7mj;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)LX/7mj;

    move-result-object v1

    .line 1335010
    iget-object v0, p0, LX/8Mh;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7mW;

    invoke-virtual {v0, v1}, LX/7mW;->a(LX/7mj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v1

    .line 1335011
    goto :goto_0
.end method
