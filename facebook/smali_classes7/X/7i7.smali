.class public final LX/7i7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/7i6;

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lorg/json/JSONObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Z

.field public x:Z


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/7i6;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/7i6;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1227062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1227063
    iput-object p1, p0, LX/7i7;->a:Landroid/net/Uri;

    .line 1227064
    iput-object p2, p0, LX/7i7;->b:Ljava/lang/String;

    .line 1227065
    iput-object p3, p0, LX/7i7;->c:Ljava/lang/String;

    .line 1227066
    iput-object p4, p0, LX/7i7;->d:Ljava/lang/String;

    .line 1227067
    iput-object p5, p0, LX/7i7;->g:Ljava/lang/String;

    .line 1227068
    iput-object p6, p0, LX/7i7;->i:LX/7i6;

    .line 1227069
    iput-object p7, p0, LX/7i7;->h:Ljava/util/List;

    .line 1227070
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/7i7;
    .locals 0

    .prologue
    .line 1227077
    iput-object p1, p0, LX/7i7;->e:Ljava/lang/String;

    .line 1227078
    return-object p0
.end method

.method public final a(Z)LX/7i7;
    .locals 0

    .prologue
    .line 1227075
    iput-boolean p1, p0, LX/7i7;->n:Z

    .line 1227076
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/7i7;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1227073
    iput-object p1, p0, LX/7i7;->f:Ljava/lang/String;

    .line 1227074
    return-object p0
.end method

.method public final b(Z)LX/7i7;
    .locals 0

    .prologue
    .line 1227071
    iput-boolean p1, p0, LX/7i7;->p:Z

    .line 1227072
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/7i7;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1227060
    iput-object p1, p0, LX/7i7;->j:Ljava/lang/String;

    .line 1227061
    return-object p0
.end method

.method public final c(Z)LX/7i7;
    .locals 0

    .prologue
    .line 1227058
    iput-boolean p1, p0, LX/7i7;->q:Z

    .line 1227059
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/7i7;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1227079
    iput-object p1, p0, LX/7i7;->l:Ljava/lang/String;

    .line 1227080
    return-object p0
.end method

.method public final d(Z)LX/7i7;
    .locals 0

    .prologue
    .line 1227056
    iput-boolean p1, p0, LX/7i7;->r:Z

    .line 1227057
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/7i7;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1227054
    iput-object p1, p0, LX/7i7;->m:Ljava/lang/String;

    .line 1227055
    return-object p0
.end method

.method public final e(Z)LX/7i7;
    .locals 0

    .prologue
    .line 1227052
    iput-boolean p1, p0, LX/7i7;->s:Z

    .line 1227053
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/7i7;
    .locals 0

    .prologue
    .line 1227042
    iput-object p1, p0, LX/7i7;->v:Ljava/lang/String;

    .line 1227043
    return-object p0
.end method

.method public final f(Z)LX/7i7;
    .locals 0

    .prologue
    .line 1227050
    iput-boolean p1, p0, LX/7i7;->t:Z

    .line 1227051
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/7i7;
    .locals 0

    .prologue
    .line 1227048
    iput-object p1, p0, LX/7i7;->u:Ljava/lang/String;

    .line 1227049
    return-object p0
.end method

.method public final g(Z)LX/7i7;
    .locals 0

    .prologue
    .line 1227046
    iput-boolean p1, p0, LX/7i7;->w:Z

    .line 1227047
    return-object p0
.end method

.method public final h(Z)LX/7i7;
    .locals 0

    .prologue
    .line 1227044
    iput-boolean p1, p0, LX/7i7;->x:Z

    .line 1227045
    return-object p0
.end method
