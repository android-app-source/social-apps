.class public final LX/7hb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7hX;


# instance fields
.field public final synthetic a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;


# direct methods
.method public constructor <init>(Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;)V
    .locals 0

    .prologue
    .line 1226315
    iput-object p1, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(LX/7hb;Landroid/view/View;Landroid/view/View;)Landroid/view/animation/AnimationSet;
    .locals 1

    .prologue
    .line 1226316
    invoke-static {p1, p2}, LX/7hb;->a(Landroid/view/View;Landroid/view/View;)Landroid/view/animation/AnimationSet;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)Landroid/view/animation/AnimationSet;
    .locals 10

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x3

    const/4 v5, 0x2

    .line 1226255
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 1226256
    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1226257
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    aput v1, v0, v5

    .line 1226258
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    aput v1, v0, v6

    .line 1226259
    const/4 v1, 0x4

    new-array v1, v1, [I

    fill-array-data v1, :array_1

    .line 1226260
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1226261
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    aput v2, v1, v5

    .line 1226262
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    aput v2, v1, v6

    .line 1226263
    new-instance v2, Landroid/view/animation/AnimationSet;

    invoke-direct {v2, v7}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1226264
    new-instance v3, Landroid/view/animation/ScaleAnimation;

    aget v4, v1, v5

    int-to-float v4, v4

    aget v5, v0, v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    aget v5, v1, v6

    int-to-float v5, v5

    aget v6, v0, v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-direct {v3, v9, v4, v9, v5}, Landroid/view/animation/ScaleAnimation;-><init>(FFFF)V

    invoke-virtual {v2, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1226265
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    const/4 v4, 0x0

    aget v4, v1, v4

    const/4 v5, 0x0

    aget v5, v0, v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    aget v1, v1, v7

    aget v0, v0, v7

    sub-int v0, v1, v0

    int-to-float v0, v0

    invoke-direct {v3, v8, v4, v8, v0}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    invoke-virtual {v2, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1226266
    return-object v2

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 1226267
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public static a(LX/7hb;II)[Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1226307
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/Bitmap;

    aput-object v1, v0, v2

    aput-object v1, v0, v3

    .line 1226308
    if-eqz p1, :cond_0

    .line 1226309
    iget-object v1, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    invoke-virtual {v1}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1}, LX/436;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v2

    .line 1226310
    :cond_0
    if-ne p2, p1, :cond_2

    .line 1226311
    aget-object v1, v0, v2

    aput-object v1, v0, v3

    .line 1226312
    :cond_1
    :goto_0
    return-object v0

    .line 1226313
    :cond_2
    if-eqz p2, :cond_1

    .line 1226314
    iget-object v1, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    invoke-virtual {v1}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p2}, LX/436;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v3

    goto :goto_0
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1226298
    iget-object v0, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v0, v0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogoGroup:Landroid/view/View;

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1226299
    iget-object v1, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v1, v1, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashGroup:Landroid/view/View;

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1226300
    iget-object v2, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    invoke-virtual {v2}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 1226301
    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v3, v2, :cond_0

    .line 1226302
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1226303
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1226304
    :goto_0
    return-void

    .line 1226305
    :cond_0
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1226306
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 1226270
    invoke-direct {p0}, LX/7hb;->c()V

    .line 1226271
    iget-object v0, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    invoke-virtual {v0}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "orca:authparam:help_url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1226272
    if-eqz v0, :cond_0

    .line 1226273
    iget-object v0, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v0, v0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mHelpButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1226274
    :cond_0
    iget-object v0, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v0, v0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mHelpButton:Landroid/widget/ImageButton;

    new-instance v1, LX/7hZ;

    invoke-direct {v1, p0}, LX/7hZ;-><init>(LX/7hb;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1226275
    iget-object v0, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    invoke-virtual {v0}, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "orca:authparam:splash_transition"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/7hc;

    .line 1226276
    iget-object v1, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v1, v1, Lcom/facebook/auth/login/ui/AuthFragmentViewGroup;->control:LX/7hV;

    invoke-interface {v1}, LX/7hV;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1226277
    sget-object v0, LX/7hc;->NONE:LX/7hc;

    .line 1226278
    :cond_1
    sget-object v1, LX/7hW;->a:[I

    invoke-virtual {v0}, LX/7hc;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1226279
    :goto_0
    return-void

    .line 1226280
    :pswitch_0
    const/4 v2, 0x0

    .line 1226281
    iget-object v0, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v0, v0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo1View:Landroid/widget/ImageView;

    iget-object v1, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget v1, v1, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo1ResId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1226282
    iget-object v0, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v0, v0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo2View:Landroid/widget/ImageView;

    iget-object v1, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget v1, v1, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo2ResId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1226283
    iget-object v0, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v0, v0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashGroup:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1226284
    iget-object v0, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v0, v0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mRootGroup:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1226285
    iget-object v0, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v0, v0, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogoGroup:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1226286
    goto :goto_0

    .line 1226287
    :pswitch_1
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1226288
    new-instance v2, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$2;

    invoke-direct {v2, p0}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$2;-><init>(LX/7hb;)V

    .line 1226289
    iget-object v3, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget v3, v3, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo1ResId:I

    iget-object v4, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget v4, v4, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo1ResId:I

    invoke-static {p0, v3, v4}, LX/7hb;->a(LX/7hb;II)[Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1226290
    iget-object v4, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget v4, v4, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo2ResId:I

    iget-object v5, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget v5, v5, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo2ResId:I

    invoke-static {p0, v4, v5}, LX/7hb;->a(LX/7hb;II)[Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1226291
    new-instance v5, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;

    invoke-direct {v5, p0, v2}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup$ModernLayoutDelegate$3;-><init>(LX/7hb;Ljava/lang/Runnable;)V

    .line 1226292
    iget-object v2, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v2, v2, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo1View:Landroid/widget/ImageView;

    aget-object v6, v3, v7

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1226293
    iget-object v2, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v2, v2, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mSplashLogo2View:Landroid/widget/ImageView;

    aget-object v6, v4, v7

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1226294
    iget-object v2, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v2, v2, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo1View:Landroid/widget/ImageView;

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1226295
    iget-object v2, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    iget-object v2, v2, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->mLogo2View:Landroid/widget/ImageView;

    aget-object v3, v4, v8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1226296
    iget-object v2, p0, LX/7hb;->a:Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;

    const-wide/16 v6, 0xc8

    invoke-virtual {v2, v5, v6, v7}, Lcom/facebook/auth/login/ui/AuthFragmentLogoViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1226297
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1226268
    invoke-direct {p0}, LX/7hb;->c()V

    .line 1226269
    return-void
.end method
