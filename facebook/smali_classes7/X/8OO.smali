.class public final LX/8OO;
.super LX/8O7;
.source ""


# instance fields
.field public final synthetic a:LX/8OR;

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:Ljava/util/Map;

.field public final synthetic d:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;LX/8OR;Ljava/util/List;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1339065
    iput-object p1, p0, LX/8OO;->d:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    iput-object p2, p0, LX/8OO;->a:LX/8OR;

    iput-object p3, p0, LX/8OO;->b:Ljava/util/List;

    iput-object p4, p0, LX/8OO;->c:Ljava/util/Map;

    invoke-direct {p0}, LX/8O7;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(III)V
    .locals 12

    .prologue
    .line 1339047
    iget-object v0, p0, LX/8OO;->a:LX/8OR;

    iget-object v1, p0, LX/8OO;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v1, p1

    iget-object v2, p0, LX/8OO;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v2, p2

    .line 1339048
    iget-object v3, v0, LX/8OR;->a:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    iget-object v3, v3, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->f:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v9

    .line 1339049
    iget-wide v3, v0, LX/8OR;->c:J

    sub-long v3, v9, v3

    const-wide/16 v5, 0xc8

    cmp-long v3, v3, v5

    if-gez v3, :cond_0

    int-to-long v3, v1

    iget-wide v5, v0, LX/8OR;->e:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_3

    .line 1339050
    :cond_0
    int-to-long v3, p3

    iget-wide v5, v0, LX/8OR;->d:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_1

    int-to-long v3, v1

    iget-wide v5, v0, LX/8OR;->e:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_3

    .line 1339051
    :cond_1
    :try_start_0
    iget-object v3, v0, LX/8OR;->a:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    iget-object v11, v3, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->b:LX/0b3;

    new-instance v3, LX/0bP;

    iget-object v4, v0, LX/8OR;->b:Lcom/facebook/photos/upload/operation/UploadOperation;

    sget-object v7, LX/8Ki;->UPLOADING:LX/8Ki;

    move v5, v1

    move v6, v2

    move v8, p3

    invoke-direct/range {v3 .. v8}, LX/0bP;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;IILX/8Ki;I)V

    invoke-virtual {v11, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1339052
    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    int-to-long v3, v1

    iget-wide v5, v0, LX/8OR;->e:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_2

    .line 1339053
    new-instance v3, LX/8Ka;

    iget-object v4, v0, LX/8OR;->b:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1339054
    iget-object v5, v4, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v4, v5

    .line 1339055
    invoke-direct {v3, v1, v4}, LX/8Ka;-><init>(ILjava/lang/String;)V

    .line 1339056
    iget-object v4, v0, LX/8OR;->a:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    iget-object v4, v4, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->b:LX/0b3;

    invoke-virtual {v4, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1339057
    iget-object v4, v0, LX/8OR;->a:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    iget-object v4, v4, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->l:LX/1EZ;

    .line 1339058
    iput-object v3, v4, LX/1EZ;->C:LX/8Ka;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1339059
    :cond_2
    :goto_0
    iput-wide v9, v0, LX/8OR;->c:J

    .line 1339060
    int-to-long v3, p3

    iput-wide v3, v0, LX/8OR;->d:J

    .line 1339061
    int-to-long v3, v1

    iput-wide v3, v0, LX/8OR;->e:J

    .line 1339062
    :cond_3
    return-void

    .line 1339063
    :catch_0
    move-exception v3

    .line 1339064
    iget-object v4, v0, LX/8OR;->a:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    iget-object v4, v4, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->d:LX/03V;

    const-string v5, "Upload progress notification"

    invoke-virtual {v4, v5, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;Lcom/facebook/photos/upload/operation/UploadRecord;)V
    .locals 2

    .prologue
    .line 1339044
    iget-object v0, p0, LX/8OO;->d:Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;

    iget-object v0, v0, Lcom/facebook/photos/upload/uploaders/MultiPhotoUploader;->h:LX/0cW;

    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/0cW;->a(Ljava/lang/String;Lcom/facebook/photos/upload/operation/UploadRecord;)Z

    .line 1339045
    iget-object v0, p0, LX/8OO;->c:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->O()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339046
    return-void
.end method
