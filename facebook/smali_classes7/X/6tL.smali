.class public final LX/6tL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
        "Lcom/facebook/payments/picker/option/PaymentsPickerOption;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6tM;


# direct methods
.method public constructor <init>(LX/6tM;)V
    .locals 0

    .prologue
    .line 1154639
    iput-object p1, p0, LX/6tL;->a:LX/6tM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1154640
    check-cast p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    .line 1154641
    new-instance v0, Lcom/facebook/payments/picker/option/PaymentsPickerOption;

    iget-object v1, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    iget-object v2, p0, LX/6tL;->a:LX/6tM;

    iget-object v2, v2, LX/6tM;->a:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    .line 1154642
    iget-object v3, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->d:LX/0Px;

    invoke-static {v3}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(LX/0Px;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v3

    .line 1154643
    if-eqz v3, :cond_0

    iget-object p0, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->b:Ljava/lang/String;

    invoke-virtual {v3, v2, p0}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    move-object v2, v3

    .line 1154644
    invoke-direct {v0, v1, v2}, Lcom/facebook/payments/picker/option/PaymentsPickerOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    iget-object v3, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->b:Ljava/lang/String;

    goto :goto_0
.end method
