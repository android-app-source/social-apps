.class public LX/87w;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/23P;


# direct methods
.method public constructor <init>(LX/23P;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1300759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300760
    iput-object p1, p0, LX/87w;->a:LX/23P;

    .line 1300761
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1300762
    iget-object v0, p0, LX/87w;->a:LX/23P;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/87w;
    .locals 2

    .prologue
    .line 1300763
    new-instance v1, LX/87w;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    invoke-direct {v1, v0}, LX/87w;-><init>(LX/23P;)V

    .line 1300764
    return-object v1
.end method


# virtual methods
.method public final a(LX/0Px;Ljava/lang/String;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1300765
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1300766
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1300767
    :goto_0
    return-object p1

    .line 1300768
    :cond_0
    invoke-direct {p0, p2}, LX/87w;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1300769
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/data/FriendData;

    .line 1300770
    iget-object v5, v0, Lcom/facebook/goodfriends/data/FriendData;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1300771
    invoke-direct {p0, v5}, LX/87w;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1300772
    iget-object v5, v0, Lcom/facebook/goodfriends/data/FriendData;->e:Ljava/lang/String;

    move-object v5, v5

    .line 1300773
    invoke-direct {p0, v5}, LX/87w;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1300774
    :cond_1
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1300775
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1300776
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    goto :goto_0
.end method
