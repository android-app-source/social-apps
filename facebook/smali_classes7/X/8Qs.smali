.class public final enum LX/8Qs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8Qs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8Qs;

.field public static final enum MORE_OPTIONS:LX/8Qs;

.field public static final enum SEE_ALL_LISTS:LX/8Qs;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1343729
    new-instance v0, LX/8Qs;

    const-string v1, "MORE_OPTIONS"

    invoke-direct {v0, v1, v2}, LX/8Qs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Qs;->MORE_OPTIONS:LX/8Qs;

    .line 1343730
    new-instance v0, LX/8Qs;

    const-string v1, "SEE_ALL_LISTS"

    invoke-direct {v0, v1, v3}, LX/8Qs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8Qs;->SEE_ALL_LISTS:LX/8Qs;

    .line 1343731
    const/4 v0, 0x2

    new-array v0, v0, [LX/8Qs;

    sget-object v1, LX/8Qs;->MORE_OPTIONS:LX/8Qs;

    aput-object v1, v0, v2

    sget-object v1, LX/8Qs;->SEE_ALL_LISTS:LX/8Qs;

    aput-object v1, v0, v3

    sput-object v0, LX/8Qs;->$VALUES:[LX/8Qs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1343732
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8Qs;
    .locals 1

    .prologue
    .line 1343733
    const-class v0, LX/8Qs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8Qs;

    return-object v0
.end method

.method public static values()[LX/8Qs;
    .locals 1

    .prologue
    .line 1343734
    sget-object v0, LX/8Qs;->$VALUES:[LX/8Qs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8Qs;

    return-object v0
.end method
