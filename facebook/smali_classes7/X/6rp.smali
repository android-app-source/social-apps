.class public final enum LX/6rp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6rp;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6rp;

.field public static final enum AUTHENTICATION:LX/6rp;

.field public static final enum CHECKOUT_OPTIONS:LX/6rp;

.field public static final enum CONTACT_INFO:LX/6rp;

.field public static final enum CONTACT_NAME:LX/6rp;

.field public static final enum MAILING_ADDRESS:LX/6rp;

.field public static final enum NOTE:LX/6rp;

.field public static final enum PAYMENT_METHOD:LX/6rp;

.field public static final enum PRICE_SELECTOR:LX/6rp;

.field public static final enum SHIPPING_OPTION:LX/6rp;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1152695
    new-instance v0, LX/6rp;

    const-string v1, "CHECKOUT_OPTIONS"

    invoke-direct {v0, v1, v3}, LX/6rp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6rp;->CHECKOUT_OPTIONS:LX/6rp;

    .line 1152696
    new-instance v0, LX/6rp;

    const-string v1, "CONTACT_INFO"

    invoke-direct {v0, v1, v4}, LX/6rp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6rp;->CONTACT_INFO:LX/6rp;

    .line 1152697
    new-instance v0, LX/6rp;

    const-string v1, "CONTACT_NAME"

    invoke-direct {v0, v1, v5}, LX/6rp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6rp;->CONTACT_NAME:LX/6rp;

    .line 1152698
    new-instance v0, LX/6rp;

    const-string v1, "MAILING_ADDRESS"

    invoke-direct {v0, v1, v6}, LX/6rp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6rp;->MAILING_ADDRESS:LX/6rp;

    .line 1152699
    new-instance v0, LX/6rp;

    const-string v1, "NOTE"

    invoke-direct {v0, v1, v7}, LX/6rp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6rp;->NOTE:LX/6rp;

    .line 1152700
    new-instance v0, LX/6rp;

    const-string v1, "PAYMENT_METHOD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6rp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6rp;->PAYMENT_METHOD:LX/6rp;

    .line 1152701
    new-instance v0, LX/6rp;

    const-string v1, "AUTHENTICATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/6rp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6rp;->AUTHENTICATION:LX/6rp;

    .line 1152702
    new-instance v0, LX/6rp;

    const-string v1, "SHIPPING_OPTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/6rp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6rp;->SHIPPING_OPTION:LX/6rp;

    .line 1152703
    new-instance v0, LX/6rp;

    const-string v1, "PRICE_SELECTOR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/6rp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6rp;->PRICE_SELECTOR:LX/6rp;

    .line 1152704
    const/16 v0, 0x9

    new-array v0, v0, [LX/6rp;

    sget-object v1, LX/6rp;->CHECKOUT_OPTIONS:LX/6rp;

    aput-object v1, v0, v3

    sget-object v1, LX/6rp;->CONTACT_INFO:LX/6rp;

    aput-object v1, v0, v4

    sget-object v1, LX/6rp;->CONTACT_NAME:LX/6rp;

    aput-object v1, v0, v5

    sget-object v1, LX/6rp;->MAILING_ADDRESS:LX/6rp;

    aput-object v1, v0, v6

    sget-object v1, LX/6rp;->NOTE:LX/6rp;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6rp;->PAYMENT_METHOD:LX/6rp;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6rp;->AUTHENTICATION:LX/6rp;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6rp;->SHIPPING_OPTION:LX/6rp;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6rp;->PRICE_SELECTOR:LX/6rp;

    aput-object v2, v0, v1

    sput-object v0, LX/6rp;->$VALUES:[LX/6rp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1152694
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6rp;
    .locals 1

    .prologue
    .line 1152692
    const-class v0, LX/6rp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6rp;

    return-object v0
.end method

.method public static values()[LX/6rp;
    .locals 1

    .prologue
    .line 1152693
    sget-object v0, LX/6rp;->$VALUES:[LX/6rp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6rp;

    return-object v0
.end method
