.class public final enum LX/6iW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6iW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6iW;

.field public static final enum ARCHIVED:LX/6iW;

.field public static final enum READ:LX/6iW;

.field public static final enum SPAM:LX/6iW;


# instance fields
.field private apiName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1129542
    new-instance v0, LX/6iW;

    const-string v1, "READ"

    const-string v2, "read"

    invoke-direct {v0, v1, v3, v2}, LX/6iW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6iW;->READ:LX/6iW;

    .line 1129543
    new-instance v0, LX/6iW;

    const-string v1, "ARCHIVED"

    const-string v2, "archived"

    invoke-direct {v0, v1, v4, v2}, LX/6iW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6iW;->ARCHIVED:LX/6iW;

    .line 1129544
    new-instance v0, LX/6iW;

    const-string v1, "SPAM"

    const-string v2, "spam"

    invoke-direct {v0, v1, v5, v2}, LX/6iW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6iW;->SPAM:LX/6iW;

    .line 1129545
    const/4 v0, 0x3

    new-array v0, v0, [LX/6iW;

    sget-object v1, LX/6iW;->READ:LX/6iW;

    aput-object v1, v0, v3

    sget-object v1, LX/6iW;->ARCHIVED:LX/6iW;

    aput-object v1, v0, v4

    sget-object v1, LX/6iW;->SPAM:LX/6iW;

    aput-object v1, v0, v5

    sput-object v0, LX/6iW;->$VALUES:[LX/6iW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1129546
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1129547
    iput-object p3, p0, LX/6iW;->apiName:Ljava/lang/String;

    .line 1129548
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6iW;
    .locals 1

    .prologue
    .line 1129549
    const-class v0, LX/6iW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6iW;

    return-object v0
.end method

.method public static values()[LX/6iW;
    .locals 1

    .prologue
    .line 1129550
    sget-object v0, LX/6iW;->$VALUES:[LX/6iW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6iW;

    return-object v0
.end method


# virtual methods
.method public final getApiName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1129551
    iget-object v0, p0, LX/6iW;->apiName:Ljava/lang/String;

    return-object v0
.end method
