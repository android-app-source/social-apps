.class public LX/80u;
.super LX/0rn;
.source ""


# instance fields
.field private final d:LX/0sa;

.field private final e:LX/0rq;

.field private final f:LX/0w9;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/325;

.field private final i:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final j:Lcom/facebook/debug/feed/DebugFeedConfig;

.field private final k:LX/0u7;

.field private final l:LX/0oz;

.field private final m:LX/0Yi;

.field private final n:LX/0pX;

.field private final o:LX/0dC;

.field private final p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;"
        }
    .end annotation
.end field

.field public final q:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final r:Landroid/content/res/AssetManager;

.field private final s:LX/0ad;

.field private final t:LX/0sX;

.field private final u:LX/0rW;

.field private final v:LX/0tK;


# direct methods
.method public constructor <init>(LX/0sa;LX/0rq;LX/0w9;LX/0u7;LX/0oz;LX/0dC;LX/0sO;LX/03V;LX/0SG;LX/0Or;Lcom/facebook/performancelogger/PerformanceLogger;LX/325;LX/0So;LX/0Yl;Lcom/facebook/debug/feed/DebugFeedConfig;LX/0pX;LX/0Yi;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;Landroid/content/Context;LX/0sX;LX/0sZ;LX/0rW;LX/0tK;)V
    .locals 9
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p21    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0sa;",
            "LX/0rq;",
            "LX/0w9;",
            "Lcom/facebook/common/hardware/BatteryStateManager;",
            "LX/0oz;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0sO;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "LX/325;",
            "LX/0So;",
            "LX/0Yl;",
            "Lcom/facebook/debug/feed/DebugFeedConfig;",
            "LX/0pX;",
            "LX/0Yi;",
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0ad;",
            "Landroid/content/Context;",
            "LX/0sX;",
            "LX/0sZ;",
            "LX/0rW;",
            "LX/0tK;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1284801
    move-object v1, p0

    move-object/from16 v2, p7

    move-object/from16 v3, p8

    move-object/from16 v4, p14

    move-object/from16 v5, p9

    move-object/from16 v6, p13

    move-object/from16 v7, p20

    move-object/from16 v8, p23

    invoke-direct/range {v1 .. v8}, LX/0rn;-><init>(LX/0sO;LX/03V;LX/0Yl;LX/0SG;LX/0So;LX/0ad;LX/0sZ;)V

    .line 1284802
    iput-object p1, p0, LX/80u;->d:LX/0sa;

    .line 1284803
    iput-object p2, p0, LX/80u;->e:LX/0rq;

    .line 1284804
    iput-object p3, p0, LX/80u;->f:LX/0w9;

    .line 1284805
    iput-object p4, p0, LX/80u;->k:LX/0u7;

    .line 1284806
    iput-object p6, p0, LX/80u;->o:LX/0dC;

    .line 1284807
    iput-object p5, p0, LX/80u;->l:LX/0oz;

    .line 1284808
    move-object/from16 v0, p10

    iput-object v0, p0, LX/80u;->g:LX/0Or;

    .line 1284809
    move-object/from16 v0, p12

    iput-object v0, p0, LX/80u;->h:LX/325;

    .line 1284810
    move-object/from16 v0, p11

    iput-object v0, p0, LX/80u;->i:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 1284811
    move-object/from16 v0, p15

    iput-object v0, p0, LX/80u;->j:Lcom/facebook/debug/feed/DebugFeedConfig;

    .line 1284812
    move-object/from16 v0, p16

    iput-object v0, p0, LX/80u;->n:LX/0pX;

    .line 1284813
    move-object/from16 v0, p18

    iput-object v0, p0, LX/80u;->p:LX/0Or;

    .line 1284814
    move-object/from16 v0, p17

    iput-object v0, p0, LX/80u;->m:LX/0Yi;

    .line 1284815
    move-object/from16 v0, p19

    iput-object v0, p0, LX/80u;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1284816
    invoke-virtual/range {p21 .. p21}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    iput-object v1, p0, LX/80u;->r:Landroid/content/res/AssetManager;

    .line 1284817
    move-object/from16 v0, p20

    iput-object v0, p0, LX/80u;->s:LX/0ad;

    .line 1284818
    move-object/from16 v0, p22

    iput-object v0, p0, LX/80u;->t:LX/0sX;

    .line 1284819
    move-object/from16 v0, p24

    iput-object v0, p0, LX/80u;->u:LX/0rW;

    .line 1284820
    move-object/from16 v0, p25

    iput-object v0, p0, LX/80u;->v:LX/0tK;

    .line 1284821
    return-void
.end method

.method private a(Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 1

    .prologue
    .line 1284797
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1284798
    iget-object v0, p0, LX/80u;->h:LX/325;

    invoke-virtual {v0, p1}, LX/325;->b(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 1284799
    :goto_0
    return-void

    .line 1284800
    :cond_0
    iget-object v0, p0, LX/80u;->h:LX/325;

    invoke-virtual {v0, p1}, LX/325;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/feed/FetchFeedParams;LX/1pN;LX/15w;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 3

    .prologue
    .line 1284788
    invoke-super {p0, p1, p2, p3}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;LX/1pN;LX/15w;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    .line 1284789
    sget-object v1, LX/80t;->a:[I

    invoke-static {p1}, LX/2XD;->getQueryType(Lcom/facebook/api/feed/FetchFeedParams;)LX/2XD;

    move-result-object v2

    invoke-virtual {v2}, LX/2XD;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1284790
    :goto_0
    return-object v0

    .line 1284791
    :pswitch_0
    invoke-direct {p0, v0}, LX/80u;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 1284792
    :pswitch_1
    invoke-virtual {v0}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1284793
    iget-object v1, p0, LX/80u;->h:LX/325;

    invoke-virtual {v1, v0}, LX/325;->d(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 1284794
    :goto_1
    goto :goto_0

    .line 1284795
    :pswitch_2
    invoke-direct {p0, v0}, LX/80u;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    goto :goto_0

    .line 1284796
    :cond_0
    iget-object v1, p0, LX/80u;->h:LX/325;

    invoke-virtual {v1, v0}, LX/325;->c(Lcom/facebook/api/feed/FetchFeedResult;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1284776
    invoke-super {p0, p1, p2}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    .line 1284777
    invoke-static {p2}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v1

    .line 1284778
    sget-object v2, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v1, v2, :cond_0

    sget-object v2, LX/1nY;->HTTP_500_CLASS:LX/1nY;

    if-ne v1, v2, :cond_3

    .line 1284779
    :cond_0
    instance-of v1, p2, LX/4cm;

    if-eqz v1, :cond_2

    .line 1284780
    iget-object v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v1, v1

    .line 1284781
    sget-object v2, LX/0rU;->CHUNKED_REMAINDER:LX/0rU;

    if-ne v1, v2, :cond_1

    .line 1284782
    iget-object v1, p0, LX/80u;->n:LX/0pX;

    invoke-virtual {v1, p1}, LX/0pX;->a(Lcom/facebook/api/feed/FetchFeedParams;)Z

    .line 1284783
    :cond_1
    :goto_0
    return-object v0

    .line 1284784
    :cond_2
    iget-object v1, p0, LX/80u;->n:LX/0pX;

    invoke-virtual {v1, p1, p2}, LX/0pX;->b(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Z

    goto :goto_0

    .line 1284785
    :cond_3
    sget-object v2, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v1, v2, :cond_4

    .line 1284786
    iget-object v1, p0, LX/80u;->n:LX/0pX;

    invoke-virtual {v1, p1, p2}, LX/0pX;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Z

    goto :goto_0

    .line 1284787
    :cond_4
    iget-object v1, p0, LX/80u;->n:LX/0pX;

    invoke-virtual {v1, p1, p2}, LX/0pX;->c(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Z

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1284775
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {p0, p1, p2}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1284768
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    .line 1284769
    :try_start_0
    invoke-super {p0, p1, p2}, LX/0rn;->a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/FetchFeedResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 1284770
    :catch_0
    move-exception v0

    .line 1284771
    iget-object v1, p0, LX/80u;->n:LX/0pX;

    .line 1284772
    iget-boolean v2, p2, LX/1pN;->f:Z

    move v2, v2

    .line 1284773
    invoke-virtual {v1, p1, v2, v0}, LX/0pX;->a(Lcom/facebook/api/feed/FetchFeedParams;ZLjava/lang/Exception;)Z

    .line 1284774
    throw v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1284767
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {p0, p1, p2, p3}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;LX/1pN;LX/15w;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedParams;)V
    .locals 1

    .prologue
    .line 1284764
    invoke-super {p0, p1}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;)V

    .line 1284765
    iget-object v0, p0, LX/80u;->m:LX/0Yi;

    invoke-virtual {v0, p1}, LX/0Yi;->a(Lcom/facebook/api/feed/FetchFeedParams;)V

    .line 1284766
    return-void
.end method

.method public final synthetic a_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1284700
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {p0, p1}, LX/0rn;->a(Lcom/facebook/api/feed/FetchFeedParams;)V

    return-void
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 1284763
    const/4 v0, 0x2

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1284762
    const-string v0, "fetch_news_feed_streaming"

    return-object v0
.end method

.method public final c(Lcom/facebook/api/feed/FetchFeedParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1284759
    iget-object v0, p0, LX/80u;->i:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0, p1}, LX/0rm;->a(Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/api/feed/FetchFeedParams;)LX/0Yj;

    move-result-object v0

    .line 1284760
    iget-object p0, v0, LX/0Yj;->d:Ljava/lang/String;

    move-object v0, p0

    .line 1284761
    return-object v0
.end method

.method public final d(Lcom/facebook/api/feed/FetchFeedParams;)I
    .locals 1

    .prologue
    .line 1284756
    iget-object v0, p0, LX/80u;->i:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {v0, p1}, LX/0rm;->a(Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/api/feed/FetchFeedParams;)LX/0Yj;

    move-result-object v0

    .line 1284757
    iget p0, v0, LX/0Yj;->a:I

    move v0, p0

    .line 1284758
    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 9

    .prologue
    .line 1284701
    check-cast p1, Lcom/facebook/api/feed/FetchFeedParams;

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1284702
    invoke-static {}, LX/0vY;->a()LX/0w4;

    move-result-object v1

    .line 1284703
    iput-boolean v5, v1, LX/0gW;->l:Z

    .line 1284704
    const-string v2, "PRODUCTION"

    .line 1284705
    iget-object v0, p0, LX/80u;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1284706
    if-eqz v0, :cond_6

    .line 1284707
    iget-boolean v3, v0, Lcom/facebook/user/model/User;->o:Z

    move v0, v3

    .line 1284708
    if-eqz v0, :cond_6

    const/4 v0, 0x0

    .line 1284709
    iget-object v3, p0, LX/80u;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/80u;->q:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0eJ;->g:LX/0Tn;

    invoke-interface {v3, v4, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 1284710
    if-nez v0, :cond_6

    .line 1284711
    const-string v0, "DEBUG"

    .line 1284712
    :goto_0
    invoke-static {v1}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 1284713
    iget-object v2, p0, LX/80u;->k:LX/0u7;

    invoke-static {v1, v2}, LX/0w9;->a(LX/0gW;LX/0u7;)LX/0gW;

    .line 1284714
    iget-object v2, p0, LX/80u;->f:LX/0w9;

    invoke-virtual {v2, v1}, LX/0w9;->e(LX/0gW;)LX/0gW;

    .line 1284715
    iget-object v2, p0, LX/80u;->l:LX/0oz;

    invoke-static {v1, v2}, LX/0w9;->a(LX/0gW;LX/0oz;)LX/0gW;

    .line 1284716
    iget-object v2, p0, LX/80u;->o:LX/0dC;

    invoke-static {v1, v2}, LX/0w9;->a(LX/0gW;LX/0dC;)LX/0gW;

    .line 1284717
    iget-object v2, p0, LX/80u;->f:LX/0w9;

    invoke-virtual {v2, v1}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 1284718
    const-string v2, "before_home_story_param"

    const-string v3, "after_home_story_param"

    invoke-static {v1, p1, v2, v3}, LX/0w9;->a(LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1284719
    iget-object v2, p0, LX/80u;->f:LX/0w9;

    const-string v3, "cached_stories_range"

    invoke-virtual {v2, v1, p1, v3}, LX/0w9;->a(LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;)LX/0gW;

    .line 1284720
    iget-object v2, p0, LX/80u;->f:LX/0w9;

    invoke-virtual {v2, v1}, LX/0w9;->c(LX/0gW;)LX/0gW;

    .line 1284721
    invoke-static {v1}, LX/0w9;->d(LX/0gW;)LX/0gW;

    .line 1284722
    const-string v2, "creative_low_img_size"

    iget-object v3, p0, LX/80u;->d:LX/0sa;

    invoke-virtual {v3}, LX/0sa;->B()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "creative_med_img_size"

    iget-object v4, p0, LX/80u;->d:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->C()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "creative_high_img_size"

    iget-object v4, p0, LX/80u;->d:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->D()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "pymk_size_param"

    iget-object v4, p0, LX/80u;->d:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->f()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "saved_item_pic_width"

    iget-object v4, p0, LX/80u;->d:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->i()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "saved_item_pic_height"

    iget-object v4, p0, LX/80u;->d:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->j()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "pyml_size_param"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "profile_pic_swipe_size_param"

    iget-object v4, p0, LX/80u;->d:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->m()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "gysj_size_param"

    iget-object v4, p0, LX/80u;->d:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->n()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "gysj_facepile_size_param"

    iget-object v4, p0, LX/80u;->d:LX/0sa;

    .line 1284723
    iget-object v7, v4, LX/0sa;->e:Landroid/content/res/Resources;

    const v8, 0x7f0b093b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 1284724
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object v4, v7

    .line 1284725
    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "gysj_facepile_count_param"

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "celebrations_profile_pic_size_param"

    invoke-static {}, LX/0sa;->r()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "multi_share_item_image_size_param"

    iget-object v4, p0, LX/80u;->d:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->v()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "friends_locations_profile_pic_size_param"

    iget-object v4, p0, LX/80u;->d:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->I()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "quick_promotion_image_size_param"

    iget-object v4, p0, LX/80u;->d:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->J()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "quick_promotion_large_image_size_param"

    iget-object v4, p0, LX/80u;->d:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->K()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "first_home_story_param"

    .line 1284726
    iget v4, p1, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v4, v4

    .line 1284727
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "discovery_image_size"

    iget-object v4, p0, LX/80u;->d:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->n()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "debug_mode"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "ad_media_type"

    iget-object v3, p0, LX/80u;->e:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->a()LX/0wF;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    const-string v2, "num_faceboxes_and_tags"

    iget-object v3, p0, LX/80u;->d:LX/0sa;

    .line 1284728
    iget-object v4, v3, LX/0sa;->b:Ljava/lang/Integer;

    move-object v3, v4

    .line 1284729
    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v2, "include_replies_in_total_count"

    iget-object v3, p0, LX/80u;->s:LX/0ad;

    sget-short v4, LX/0wg;->i:S

    invoke-interface {v3, v4, v6}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "client_query_id"

    .line 1284730
    iget-object v3, p1, Lcom/facebook/api/feed/FetchFeedParams;->g:Ljava/lang/String;

    move-object v3, v3

    .line 1284731
    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "greeting_card_image_size_large"

    iget-object v3, p0, LX/80u;->e:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->f()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v2, "greeting_card_image_size_medium"

    iget-object v3, p0, LX/80u;->e:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->g()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v2, "num_media_question_options"

    const/16 v3, 0xf

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v2, "media_question_photo_size"

    iget-object v3, p0, LX/80u;->e:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->f()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v2, "enable_hd"

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "automatic_photo_captioning_enabled"

    iget-object v3, p0, LX/80u;->t:LX/0sX;

    invoke-virtual {v3}, LX/0sX;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "allow_pinned_dummy_stories"

    .line 1284732
    iget-boolean v3, p1, Lcom/facebook/api/feed/FetchFeedParams;->p:Z

    move v3, v3

    .line 1284733
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1284734
    iget-object v0, p0, LX/80u;->u:LX/0rW;

    const-string v2, "recent_vpvs"

    const-string v3, "recent_vpvs_v2"

    const-string v4, "recent_vpvs_v2_enabled"

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, LX/0rW;->a(LX/0gW;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/api/feed/FetchFeedParams;)V

    .line 1284735
    iget-object v0, p0, LX/80u;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    .line 1284736
    const/4 v0, 0x1

    move v0, v0

    .line 1284737
    if-eqz v0, :cond_4

    .line 1284738
    const-string v0, "scrubbing"

    const-string v2, "MPEG_DASH"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1284739
    :cond_1
    :goto_1
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v0, v0

    .line 1284740
    iget-object v2, v0, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v0, v2

    .line 1284741
    sget-object v2, Lcom/facebook/api/feedtype/FeedType$Name;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-virtual {v0, v2}, Lcom/facebook/api/feedtype/FeedType$Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1284742
    const-string v0, "orderby_home_story_param"

    sget-object v2, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;

    invoke-virtual {v2}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1284743
    :goto_2
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1284744
    if-nez v0, :cond_2

    .line 1284745
    const-string v0, "refresh_mode_param"

    .line 1284746
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v2, v2

    .line 1284747
    invoke-virtual {v2}, LX/0gf;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1284748
    :cond_2
    iget-object v0, p0, LX/80u;->v:LX/0tK;

    invoke-virtual {v0, v6}, LX/0tK;->b(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1284749
    const-string v0, "data_savings_mode"

    const-string v2, "active"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1284750
    :cond_3
    return-object v1

    .line 1284751
    :cond_4
    iget-object v0, p0, LX/80u;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wp;

    invoke-virtual {v0}, LX/0wp;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1284752
    const-string v0, "scrubbing"

    const-string v2, "WEBM_DASH"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto :goto_1

    .line 1284753
    :cond_5
    const-string v0, "orderby_home_story_param"

    .line 1284754
    iget-object v2, p1, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v2, v2

    .line 1284755
    invoke-virtual {v2}, Lcom/facebook/api/feedtype/FeedType;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto :goto_2

    :cond_6
    move-object v0, v2

    goto/16 :goto_0
.end method
