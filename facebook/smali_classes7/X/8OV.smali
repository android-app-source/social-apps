.class public LX/8OV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8Oa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8Oa;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1339609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1339610
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1339611
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1339612
    iput-object p1, p0, LX/8OV;->b:Ljava/util/List;

    .line 1339613
    iput p2, p0, LX/8OV;->a:I

    .line 1339614
    return-void

    :cond_0
    move v0, v2

    .line 1339615
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1339616
    goto :goto_1
.end method

.method private static a(LX/8OZ;III)LX/7T7;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1339575
    const/4 v0, -0x1

    .line 1339576
    const/4 v5, -0x2

    .line 1339577
    sget-object v3, LX/8OZ;->Video:LX/8OZ;

    if-ne p0, v3, :cond_3

    .line 1339578
    add-int/lit8 v3, p1, -0x1

    if-ne p2, v3, :cond_1

    move v3, v2

    .line 1339579
    :goto_0
    if-nez p2, :cond_2

    move v4, v2

    .line 1339580
    :goto_1
    if-nez v3, :cond_4

    .line 1339581
    add-int/lit8 v3, p2, 0x1

    mul-int v5, v3, p3

    move v3, v2

    .line 1339582
    :goto_2
    if-nez v4, :cond_0

    .line 1339583
    mul-int v0, p2, p3

    :cond_0
    move v4, v0

    move v6, v1

    move v1, v2

    move v2, v6

    .line 1339584
    :goto_3
    new-instance v0, LX/7T7;

    invoke-direct/range {v0 .. v5}, LX/7T7;-><init>(ZZZII)V

    .line 1339585
    return-object v0

    :cond_1
    move v3, v1

    .line 1339586
    goto :goto_0

    :cond_2
    move v4, v1

    .line 1339587
    goto :goto_1

    :cond_3
    move v4, v0

    move v3, v1

    .line 1339588
    goto :goto_3

    :cond_4
    move v3, v1

    goto :goto_2
.end method

.method public static a(JI)LX/8OV;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1339589
    const-wide/16 v2, 0x4e20

    cmp-long v0, p0, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1339590
    const/4 v0, 0x2

    .line 1339591
    const-wide/32 v2, 0x9c40

    cmp-long v2, p0, v2

    if-ltz v2, :cond_0

    .line 1339592
    const/4 v0, 0x4

    .line 1339593
    :cond_0
    int-to-long v2, v0

    div-long v2, p0, v2

    long-to-int v4, v2

    .line 1339594
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1339595
    if-lez p2, :cond_2

    .line 1339596
    add-int/lit8 v2, v0, 0x1

    :goto_1
    move v3, v1

    .line 1339597
    :goto_2
    if-ge v3, v2, :cond_4

    .line 1339598
    if-ge v3, v0, :cond_3

    .line 1339599
    sget-object v1, LX/8OZ;->Video:LX/8OZ;

    .line 1339600
    :goto_3
    new-instance v6, LX/8Oa;

    invoke-direct {v6, v1, v3}, LX/8Oa;-><init>(LX/8OZ;I)V

    .line 1339601
    iget-object v1, v6, LX/8Oa;->c:LX/8OZ;

    invoke-static {v1, v0, v3, v4}, LX/8OV;->a(LX/8OZ;III)LX/7T7;

    move-result-object v1

    iput-object v1, v6, LX/8Oa;->g:LX/7T7;

    .line 1339602
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1339603
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_1
    move v0, v1

    .line 1339604
    goto :goto_0

    :cond_2
    move v2, v0

    .line 1339605
    goto :goto_1

    .line 1339606
    :cond_3
    sget-object v1, LX/8OZ;->Audio:LX/8OZ;

    goto :goto_3

    .line 1339607
    :cond_4
    new-instance v1, LX/8OV;

    invoke-direct {v1, v5, v0}, LX/8OV;-><init>(Ljava/util/List;I)V

    .line 1339608
    return-object v1
.end method


# virtual methods
.method public final a()LX/8Oa;
    .locals 2

    .prologue
    .line 1339574
    iget-object v0, p0, LX/8OV;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Oa;

    return-object v0
.end method

.method public final a(I)LX/8Oa;
    .locals 1

    .prologue
    .line 1339571
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/8OV;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1339572
    iget-object v0, p0, LX/8OV;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Oa;

    return-object v0

    .line 1339573
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(J)LX/8Oa;
    .locals 5

    .prologue
    .line 1339565
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/8OV;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1339566
    iget-object v0, p0, LX/8OV;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Oa;

    .line 1339567
    iget-wide v2, v0, LX/8Oa;->f:J

    cmp-long v2, p1, v2

    if-gez v2, :cond_0

    .line 1339568
    :goto_1
    return-object v0

    .line 1339569
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1339570
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1339562
    iget-object v0, p0, LX/8OV;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1339563
    iget-object v0, p0, LX/8OV;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    .line 1339564
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
