.class public LX/7B0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1177662
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1177663
    return-void
.end method

.method private static a(LX/7B0;LX/7Az;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1177664
    sget-object v1, LX/7Ay;->a:[I

    invoke-virtual {p1}, LX/7Az;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1177665
    :goto_0
    return v0

    .line 1177666
    :pswitch_0
    iget-object v1, p0, LX/7B0;->a:LX/0ad;

    sget-short v2, LX/100;->ao:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0

    .line 1177667
    :pswitch_1
    iget-object v1, p0, LX/7B0;->a:LX/0ad;

    sget-short v2, LX/100;->an:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0

    .line 1177668
    :pswitch_2
    iget-object v1, p0, LX/7B0;->a:LX/0ad;

    sget-short v2, LX/100;->am:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0

    .line 1177669
    :pswitch_3
    iget-object v1, p0, LX/7B0;->a:LX/0ad;

    sget-short v2, LX/100;->aj:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0

    .line 1177670
    :pswitch_4
    iget-object v1, p0, LX/7B0;->a:LX/0ad;

    sget-short v2, LX/100;->ak:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/7B0;
    .locals 2

    .prologue
    .line 1177671
    new-instance v1, LX/7B0;

    invoke-direct {v1}, LX/7B0;-><init>()V

    .line 1177672
    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    .line 1177673
    iput-object v0, v1, LX/7B0;->a:LX/0ad;

    .line 1177674
    return-object v1
.end method


# virtual methods
.method public final a(LX/7Az;LX/0v6;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/7Az;",
            "LX/0v6;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1177675
    invoke-static {p0, p1}, LX/7B0;->a(LX/7B0;LX/7Az;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1177676
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string v1, "X-FB-Search-Edge-Routing"

    const-string v2, "LATENCY"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1177677
    iput-object v0, p2, LX/0v6;->k:LX/0Px;

    .line 1177678
    :cond_0
    return-void
.end method

.method public final a(LX/7Az;LX/0zO;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/7Az;",
            "LX/0zO",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1177679
    invoke-static {p0, p1}, LX/7B0;->a(LX/7B0;LX/7Az;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1177680
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string v1, "X-FB-Search-Edge-Routing"

    const-string v2, "LATENCY"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1177681
    iput-object v0, p2, LX/0zO;->f:LX/0Px;

    .line 1177682
    :cond_0
    return-void
.end method

.method public final a(LX/7Az;LX/14O;)V
    .locals 3

    .prologue
    .line 1177683
    invoke-static {p0, p1}, LX/7B0;->a(LX/7B0;LX/7Az;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1177684
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string v1, "X-FB-Search-Edge-Routing"

    const-string v2, "LATENCY"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/14O;->a(LX/0Px;)LX/14O;

    .line 1177685
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 1177686
    iget-object v0, p0, LX/7B0;->a:LX/0ad;

    sget-short v1, LX/100;->al:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
