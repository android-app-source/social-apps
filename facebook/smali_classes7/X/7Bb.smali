.class public LX/7Bb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/7Bb;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0y2;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1179211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1179212
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1179213
    iput-object v0, p0, LX/7Bb;->a:LX/0Ot;

    .line 1179214
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1179215
    iput-object v0, p0, LX/7Bb;->b:LX/0Ot;

    .line 1179216
    return-void
.end method

.method public static a(LX/0QB;)LX/7Bb;
    .locals 5

    .prologue
    .line 1179196
    sget-object v0, LX/7Bb;->c:LX/7Bb;

    if-nez v0, :cond_1

    .line 1179197
    const-class v1, LX/7Bb;

    monitor-enter v1

    .line 1179198
    :try_start_0
    sget-object v0, LX/7Bb;->c:LX/7Bb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1179199
    if-eqz v2, :cond_0

    .line 1179200
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1179201
    new-instance v3, LX/7Bb;

    invoke-direct {v3}, LX/7Bb;-><init>()V

    .line 1179202
    const/16 v4, 0xc7e

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x1032

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1179203
    iput-object v4, v3, LX/7Bb;->a:LX/0Ot;

    iput-object p0, v3, LX/7Bb;->b:LX/0Ot;

    .line 1179204
    move-object v0, v3

    .line 1179205
    sput-object v0, LX/7Bb;->c:LX/7Bb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1179206
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1179207
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1179208
    :cond_1
    sget-object v0, LX/7Bb;->c:LX/7Bb;

    return-object v0

    .line 1179209
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1179210
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/search/api/GraphSearchQuery;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1179178
    new-instance v0, LX/0lp;

    invoke-direct {v0}, LX/0lp;-><init>()V

    .line 1179179
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 1179180
    :try_start_0
    invoke-virtual {v0, v1}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v0

    .line 1179181
    invoke-virtual {v0}, LX/0nX;->f()V

    .line 1179182
    const-string v2, "uid"

    .line 1179183
    iget-object v3, p0, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    move-object v3, v3

    .line 1179184
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, LX/0nX;->a(Ljava/lang/String;J)V

    .line 1179185
    const-string v2, "type"

    .line 1179186
    iget-object v3, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v3, v3

    .line 1179187
    invoke-virtual {v3}, LX/103;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1179188
    const-string v2, "text"

    .line 1179189
    iget-object v3, p0, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v3, v3

    .line 1179190
    invoke-virtual {v0, v2, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1179191
    invoke-virtual {v0}, LX/0nX;->g()V

    .line 1179192
    invoke-virtual {v0}, LX/0nX;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1179193
    invoke-virtual {v1}, Ljava/io/StringWriter;->getBuffer()Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1179194
    :catch_0
    move-exception v0

    .line 1179195
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to generate single state pivot query"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1179160
    iget-object v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    .line 1179161
    iget-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    sget-object v2, LX/7BZ;->SINGLE_STATE_MODE:LX/7BZ;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 1179162
    :goto_0
    iget-object v2, v1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v2, v2

    .line 1179163
    sget-object v3, LX/103;->URL:LX/103;

    if-ne v2, v3, :cond_1

    .line 1179164
    iget-object v0, v1, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1179165
    :goto_1
    return-object v0

    .line 1179166
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1179167
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1179168
    invoke-virtual {v1}, Lcom/facebook/search/api/GraphSearchQuery;->k()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1179169
    invoke-static {v1}, LX/7Bb;->a(Lcom/facebook/search/api/GraphSearchQuery;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1179170
    :cond_2
    iget-object v3, v1, LX/7B6;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1179171
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    if-nez v0, :cond_3

    .line 1179172
    invoke-virtual {v1}, Lcom/facebook/search/api/GraphSearchQuery;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1179173
    const-string v0, ", \""

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1179174
    :goto_2
    iget-object v0, v1, LX/7B6;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1179175
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1179176
    :cond_3
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1179177
    :cond_4
    const-string v0, "\""

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method public static a(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1179159
    iget-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->m:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    iget-object p1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->m:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1179125
    iget-object v0, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1179126
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "uuid"

    iget-object v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->g:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179127
    :cond_0
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "filter"

    iget-object v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->i:Ljava/util/List;

    .line 1179128
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1179129
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 1179130
    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1179131
    const-string v3, "\'"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7BK;

    invoke-virtual {v3}, LX/7BK;->name()Ljava/lang/String;

    move-result-object v3

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "\'"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1179132
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1179133
    const-string v3, ", "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1179134
    :cond_2
    const-string v3, "]"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 1179135
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179136
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "photo_size"

    iget v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179137
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "context"

    iget-object v0, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->p:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->p:Ljava/lang/String;

    :goto_1
    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179138
    iget v0, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->j:I

    if-lez v0, :cond_3

    .line 1179139
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "limit"

    iget v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179140
    :cond_3
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "include_native_android_url"

    const-string v2, "true"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179141
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179142
    const-string v1, ""

    .line 1179143
    iget-object v0, p0, LX/7Bb;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0y2;

    invoke-virtual {v0}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v2

    .line 1179144
    if-eqz v2, :cond_5

    .line 1179145
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->c()LX/0m9;

    move-result-object v1

    .line 1179146
    const-string v0, "latitude"

    invoke-virtual {v2}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v4

    invoke-virtual {v1, v0, v4, v5}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 1179147
    const-string v0, "longitude"

    invoke-virtual {v2}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v4

    invoke-virtual {v1, v0, v4, v5}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 1179148
    const-string v3, "accuracy"

    invoke-virtual {v2}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v1, v3, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Float;)LX/0m9;

    .line 1179149
    const-string v3, "timestamp"

    invoke-virtual {v2}, Lcom/facebook/location/ImmutableLocation;->g()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-float v0, v4

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v0, v2

    float-to-int v0, v0

    invoke-virtual {v1, v3, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1179150
    invoke-virtual {v1}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1179151
    :goto_2
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "viewer_coordinates"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179152
    return-void

    .line 1179153
    :cond_4
    const-string v0, "mobile_search_android"

    goto/16 :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method public final b(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1179154
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "keyword_mode"

    iget-object v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    invoke-virtual {v2}, LX/7BZ;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179155
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "ranking_model"

    iget-object v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->o:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179156
    iget-object v0, p0, LX/7Bb;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/100;->bH:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1179157
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "include_entity_data_in_keywords"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179158
    :cond_0
    return-void
.end method
