.class public final LX/7dz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/nearby/messages/internal/MessageType;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/nearby/messages/devices/NearbyDeviceFilter;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7dz;->a:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7dz;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/UUID;Ljava/lang/Short;Ljava/lang/Short;)LX/7dz;
    .locals 4
    .param p2    # Ljava/lang/Short;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Short;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    const-string v0, "__reserved_namespace"

    const-string v1, "__i_beacon_id"

    iget-object v2, p0, LX/7dz;->a:Ljava/util/List;

    new-instance v3, Lcom/google/android/gms/nearby/messages/internal/MessageType;

    invoke-direct {v3, v0, v1}, Lcom/google/android/gms/nearby/messages/internal/MessageType;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, LX/7dz;->b:Ljava/util/List;

    new-instance v1, LX/7eI;

    invoke-direct {v1, p1, p2, p3}, LX/7eI;-><init>(Ljava/util/UUID;Ljava/lang/Short;Ljava/lang/Short;)V

    new-instance v2, Lcom/google/android/gms/nearby/messages/devices/NearbyDeviceFilter;

    const/4 v3, 0x3

    iget-object p1, v1, LX/7eC;->b:[B

    move-object v1, p1

    invoke-direct {v2, v3, v1}, Lcom/google/android/gms/nearby/messages/devices/NearbyDeviceFilter;-><init>(I[B)V

    move-object v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final b()Lcom/google/android/gms/nearby/messages/MessageFilter;
    .locals 5

    const/4 v1, 0x0

    iget-boolean v0, p0, LX/7dz;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/7dz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "At least one of the include methods must be called."

    invoke-static {v0, v2}, LX/1ol;->a(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/gms/nearby/messages/MessageFilter;

    iget-object v2, p0, LX/7dz;->a:Ljava/util/List;

    iget-object v3, p0, LX/7dz;->b:Ljava/util/List;

    iget-boolean v4, p0, LX/7dz;->c:Z

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/gms/nearby/messages/MessageFilter;-><init>(Ljava/util/List;Ljava/util/List;Z)V

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
