.class public LX/7vW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0ie;

.field public final b:LX/0tX;


# direct methods
.method public constructor <init>(LX/0ie;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1274675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274676
    iput-object p1, p0, LX/7vW;->a:LX/0ie;

    .line 1274677
    iput-object p2, p0, LX/7vW;->b:LX/0tX;

    .line 1274678
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;)LX/4EL;
    .locals 3

    .prologue
    .line 1274604
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    .line 1274605
    iget-object v1, p1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1274606
    iget-object v1, p1, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1274607
    iget-object v1, p1, Lcom/facebook/events/common/EventAnalyticsParams;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->c(Ljava/lang/String;)LX/4EG;

    .line 1274608
    :cond_0
    new-instance v1, LX/4EG;

    invoke-direct {v1}, LX/4EG;-><init>()V

    .line 1274609
    iget-object v2, p1, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1274610
    invoke-virtual {v1, p0}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1274611
    if-eqz p2, :cond_1

    .line 1274612
    invoke-virtual {v1, p2}, LX/4EG;->c(Ljava/lang/String;)LX/4EG;

    .line 1274613
    :cond_1
    new-instance v2, LX/4EL;

    invoke-direct {v2}, LX/4EL;-><init>()V

    .line 1274614
    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 1274615
    return-object v2
.end method

.method public static a(LX/0QB;)LX/7vW;
    .locals 1

    .prologue
    .line 1274674
    invoke-static {p0}, LX/7vW;->b(LX/0QB;)LX/7vW;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1274626
    new-instance v0, LX/7ug;

    invoke-direct {v0}, LX/7ug;-><init>()V

    invoke-interface {p0}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v2

    .line 1274627
    iput-object v2, v0, LX/7ug;->e:Ljava/lang/String;

    .line 1274628
    move-object v0, v0

    .line 1274629
    iput-boolean v1, v0, LX/7ug;->g:Z

    .line 1274630
    move-object v0, v0

    .line 1274631
    iput-object p1, v0, LX/7ug;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1274632
    move-object v3, v0

    .line 1274633
    instance-of v0, p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-eqz v0, :cond_2

    .line 1274634
    check-cast p0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 1274635
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v1

    .line 1274636
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    move-result-object v2

    if-nez v2, :cond_4

    move v2, v1

    .line 1274637
    :goto_1
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v4, :cond_5

    .line 1274638
    new-instance v4, LX/7uj;

    invoke-direct {v4}, LX/7uj;-><init>()V

    add-int/lit8 v5, v0, 0x1

    .line 1274639
    iput v5, v4, LX/7uj;->a:I

    .line 1274640
    move-object v4, v4

    .line 1274641
    invoke-virtual {v4}, LX/7uj;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    move-result-object v4

    .line 1274642
    iput-object v4, v3, LX/7ug;->c:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    .line 1274643
    :cond_0
    :goto_2
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p2, v4, :cond_6

    .line 1274644
    new-instance v2, LX/7uj;

    invoke-direct {v2}, LX/7uj;-><init>()V

    add-int/lit8 v0, v0, -0x1

    .line 1274645
    iput v0, v2, LX/7uj;->a:I

    .line 1274646
    move-object v0, v2

    .line 1274647
    invoke-virtual {v0}, LX/7uj;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    move-result-object v0

    .line 1274648
    iput-object v0, v3, LX/7ug;->c:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMembersModel;

    .line 1274649
    :cond_1
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1274650
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v0

    if-nez v0, :cond_7

    .line 1274651
    :goto_4
    new-instance v0, LX/7uh;

    invoke-direct {v0}, LX/7uh;-><init>()V

    add-int/lit8 v1, v1, -0x1

    .line 1274652
    iput v1, v0, LX/7uh;->a:I

    .line 1274653
    move-object v0, v0

    .line 1274654
    invoke-virtual {v0}, LX/7uh;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    move-result-object v0

    .line 1274655
    iput-object v0, v3, LX/7ug;->a:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventInviteesModel;

    .line 1274656
    :cond_2
    invoke-virtual {v3}, LX/7ug;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    move-result-object v0

    return-object v0

    .line 1274657
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aF()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMembersModel;->a()I

    move-result v0

    goto :goto_0

    .line 1274658
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aE()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventMaybesModel;->a()I

    move-result v2

    goto :goto_1

    .line 1274659
    :cond_5
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p1, v4, :cond_0

    .line 1274660
    new-instance v4, LX/7ui;

    invoke-direct {v4}, LX/7ui;-><init>()V

    add-int/lit8 v5, v2, 0x1

    .line 1274661
    iput v5, v4, LX/7ui;->a:I

    .line 1274662
    move-object v4, v4

    .line 1274663
    invoke-virtual {v4}, LX/7ui;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    move-result-object v4

    .line 1274664
    iput-object v4, v3, LX/7ug;->b:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    .line 1274665
    goto :goto_2

    .line 1274666
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p2, v0, :cond_1

    .line 1274667
    new-instance v0, LX/7ui;

    invoke-direct {v0}, LX/7ui;-><init>()V

    add-int/lit8 v2, v2, -0x1

    .line 1274668
    iput v2, v0, LX/7ui;->a:I

    .line 1274669
    move-object v0, v0

    .line 1274670
    invoke-virtual {v0}, LX/7ui;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    move-result-object v0

    .line 1274671
    iput-object v0, v3, LX/7ug;->b:Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel$EventMaybesModel;

    .line 1274672
    goto :goto_3

    .line 1274673
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->aD()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel$EventInviteesModel;->a()I

    move-result v1

    goto :goto_4
.end method

.method public static a(LX/7vW;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsMutationsModels$EventRsvpMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1274616
    new-instance v0, LX/7ug;

    invoke-direct {v0}, LX/7ug;-><init>()V

    .line 1274617
    iput-object p1, v0, LX/7ug;->e:Ljava/lang/String;

    .line 1274618
    move-object v0, v0

    .line 1274619
    const/4 v1, 0x0

    .line 1274620
    iput-boolean v1, v0, LX/7ug;->g:Z

    .line 1274621
    move-object v0, v0

    .line 1274622
    iput-object p2, v0, LX/7ug;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1274623
    move-object v0, v0

    .line 1274624
    invoke-virtual {v0}, LX/7ug;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    move-result-object v3

    .line 1274625
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v6}, LX/7vW;->a(LX/7vW;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/7vW;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;",
            "Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsMutationsModels$EventRsvpMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1274679
    new-instance v0, LX/4ES;

    invoke-direct {v0}, LX/4ES;-><init>()V

    invoke-static {p5, p4, p6}, LX/7vW;->a(Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;)LX/4EL;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ES;->a(LX/4EL;)LX/4ES;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/4ES;->b(Ljava/lang/String;)LX/4ES;

    move-result-object v0

    invoke-static {p2}, LX/7vW;->b(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ES;->c(Ljava/lang/String;)LX/4ES;

    move-result-object v0

    .line 1274680
    invoke-static {}, LX/7uR;->e()LX/7uI;

    move-result-object v1

    .line 1274681
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1274682
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    .line 1274683
    iget-object v1, p0, LX/7vW;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1274684
    new-instance v1, LX/7vV;

    invoke-direct {v1, p0, p2}, LX/7vV;-><init>(LX/7vW;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    .line 1274685
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 1274686
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1274687
    return-object v0
.end method

.method public static b(LX/0QB;)LX/7vW;
    .locals 3

    .prologue
    .line 1274588
    new-instance v2, LX/7vW;

    invoke-static {p0}, LX/0ie;->a(LX/0QB;)LX/0ie;

    move-result-object v0

    check-cast v0, LX/0ie;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-direct {v2, v0, v1}, LX/7vW;-><init>(LX/0ie;LX/0tX;)V

    .line 1274589
    return-object v2
.end method

.method public static b(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Ljava/lang/String;
    .locals 3
    .annotation build Lcom/facebook/graphql/calls/EventGuestStatusMutationValue;
    .end annotation

    .prologue
    .line 1274578
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p0, v0, :cond_0

    .line 1274579
    const-string v0, "going"

    .line 1274580
    :goto_0
    return-object v0

    .line 1274581
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p0, v0, :cond_1

    .line 1274582
    const-string v0, "maybe"

    goto :goto_0

    .line 1274583
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p0, v0, :cond_2

    .line 1274584
    const-string v0, "not_going"

    goto :goto_0

    .line 1274585
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p0, v0, :cond_3

    .line 1274586
    const/4 v0, 0x0

    goto :goto_0

    .line 1274587
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported guest status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/events/common/ActionMechanism;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsMutationsModels$EventRsvpMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1274590
    const-string v0, "unknown"

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    if-eqz p5, :cond_0

    invoke-virtual {p5}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v6

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v6}, LX/7vW;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v6, "unknown"

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/events/common/ActionMechanism;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsMutationsModels$EventRsvpMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1274591
    new-instance v0, LX/7ug;

    invoke-direct {v0}, LX/7ug;-><init>()V

    .line 1274592
    iput-object p1, v0, LX/7ug;->e:Ljava/lang/String;

    .line 1274593
    move-object v0, v0

    .line 1274594
    const/4 v1, 0x0

    .line 1274595
    iput-boolean v1, v0, LX/7ug;->g:Z

    .line 1274596
    move-object v0, v0

    .line 1274597
    iput-object p2, v0, LX/7ug;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1274598
    move-object v0, v0

    .line 1274599
    invoke-virtual {v0}, LX/7ug;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    move-result-object v3

    .line 1274600
    new-instance v4, Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v0, Lcom/facebook/events/common/EventActionContext;->a:Lcom/facebook/events/common/EventActionContext;

    invoke-virtual {p3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    invoke-direct {v4, v0, v1, v2, v5}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1274601
    invoke-virtual {p5}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p6

    invoke-static/range {v0 .. v6}, LX/7vW;->a(LX/7vW;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsMutationsModels$EventRsvpMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1274602
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v1, Lcom/facebook/events/common/EventActionContext;->a:Lcom/facebook/events/common/EventActionContext;

    const/4 v5, 0x0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1274603
    invoke-static {p0, p1, p2, v0, p6}, LX/7vW;->a(LX/7vW;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
