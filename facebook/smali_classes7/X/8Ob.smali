.class public LX/8Ob;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:LX/74b;

.field public B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/upload/operation/UploadPartitionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public C:Lcom/facebook/photos/upload/operation/UploadRecord;

.field public D:I

.field public E:LX/60x;

.field public F:Z

.field public G:Z

.field public H:LX/7Su;

.field public I:Z

.field public J:I

.field public K:Z

.field public L:Z

.field public M:I

.field public N:I

.field public O:Landroid/graphics/RectF;

.field public P:LX/0Px;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/videocodec/effects/common/GLRendererConfig;",
            ">;"
        }
    .end annotation
.end field

.field public Q:Z

.field public R:J

.field public S:J

.field public T:J

.field public U:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/fbuploader/FbUploader$FbUploadJobHandle;",
            ">;"
        }
    .end annotation
.end field

.field public V:LX/8OY;

.field private W:Ljava/util/concurrent/atomic/AtomicInteger;

.field public volatile a:Lcom/facebook/photos/upload/operation/UploadOperation;

.field public b:Ljava/lang/Long;

.field public c:LX/14U;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/operation/UploadRecord;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/8Oo;

.field public i:Z

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:J

.field public m:J

.field public n:Ljava/lang/String;

.field public o:Z

.field public p:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public q:Ljava/lang/String;

.field public r:J

.field public s:I

.field public t:I

.field public u:J

.field public v:J

.field public w:Ljava/lang/Exception;

.field public x:LX/8Oi;

.field public y:LX/73w;

.field public z:LX/8OV;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v6, -0x1

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1339659
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1339660
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/8Ob;->b:Ljava/lang/Long;

    .line 1339661
    iput-object v2, p0, LX/8Ob;->c:LX/14U;

    .line 1339662
    iget-object v0, p0, LX/8Ob;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8Ob;->d:Ljava/lang/String;

    .line 1339663
    const-string v0, "-1"

    iput-object v0, p0, LX/8Ob;->e:Ljava/lang/String;

    .line 1339664
    const-string v0, ""

    iput-object v0, p0, LX/8Ob;->f:Ljava/lang/String;

    .line 1339665
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/8Ob;->g:Ljava/util/Map;

    .line 1339666
    const-string v0, ""

    iput-object v0, p0, LX/8Ob;->j:Ljava/lang/String;

    .line 1339667
    const-string v0, ""

    iput-object v0, p0, LX/8Ob;->k:Ljava/lang/String;

    .line 1339668
    iput-wide v4, p0, LX/8Ob;->l:J

    .line 1339669
    iput-wide v8, p0, LX/8Ob;->m:J

    .line 1339670
    const-string v0, "standard"

    iput-object v0, p0, LX/8Ob;->n:Ljava/lang/String;

    .line 1339671
    iput-boolean v3, p0, LX/8Ob;->o:Z

    .line 1339672
    iput-object p1, p0, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1339673
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/8Ob;->p:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1339674
    const-string v0, ""

    iput-object v0, p0, LX/8Ob;->q:Ljava/lang/String;

    .line 1339675
    iput-wide v4, p0, LX/8Ob;->r:J

    .line 1339676
    iput v6, p0, LX/8Ob;->s:I

    .line 1339677
    iput v6, p0, LX/8Ob;->t:I

    .line 1339678
    iput-wide v4, p0, LX/8Ob;->u:J

    .line 1339679
    iput-wide v4, p0, LX/8Ob;->v:J

    .line 1339680
    iput-object v2, p0, LX/8Ob;->w:Ljava/lang/Exception;

    .line 1339681
    sget-object v0, LX/8Oi;->START:LX/8Oi;

    iput-object v0, p0, LX/8Ob;->x:LX/8Oi;

    .line 1339682
    iput-object v2, p0, LX/8Ob;->y:LX/73w;

    .line 1339683
    iput-object v2, p0, LX/8Ob;->z:LX/8OV;

    .line 1339684
    iput-object v2, p0, LX/8Ob;->A:LX/74b;

    .line 1339685
    iput-object v2, p0, LX/8Ob;->C:Lcom/facebook/photos/upload/operation/UploadRecord;

    .line 1339686
    iput-object v2, p0, LX/8Ob;->E:LX/60x;

    .line 1339687
    iput-object v2, p0, LX/8Ob;->H:LX/7Su;

    .line 1339688
    iput-boolean v3, p0, LX/8Ob;->I:Z

    .line 1339689
    iput v3, p0, LX/8Ob;->J:I

    .line 1339690
    iput-boolean v3, p0, LX/8Ob;->K:Z

    .line 1339691
    iput-boolean v3, p0, LX/8Ob;->L:Z

    .line 1339692
    iput v6, p0, LX/8Ob;->M:I

    .line 1339693
    const/4 v0, -0x2

    iput v0, p0, LX/8Ob;->N:I

    .line 1339694
    iput-object v2, p0, LX/8Ob;->O:Landroid/graphics/RectF;

    .line 1339695
    iget-boolean v0, p1, Lcom/facebook/photos/upload/operation/UploadOperation;->ad:Z

    move v0, v0

    .line 1339696
    if-nez v0, :cond_0

    .line 1339697
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/8Ob;->a(Z)V

    .line 1339698
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8Ob;->B:Ljava/util/List;

    .line 1339699
    iput v3, p0, LX/8Ob;->D:I

    .line 1339700
    iput-boolean v3, p0, LX/8Ob;->Q:Z

    .line 1339701
    const-wide/16 v0, 0x5

    iput-wide v0, p0, LX/8Ob;->R:J

    .line 1339702
    const-wide/16 v0, 0x5

    iput-wide v0, p0, LX/8Ob;->S:J

    .line 1339703
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/8Ob;->U:Ljava/util/ArrayList;

    .line 1339704
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LX/8Ob;->W:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1339705
    iput-object v2, p0, LX/8Ob;->P:LX/0Px;

    .line 1339706
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1339707
    iget-object v0, p0, LX/8Ob;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1339708
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/8Ob;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_transcode"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1339709
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1339710
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1339711
    iget-object v1, p0, LX/8Ob;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1339712
    iget-object v2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->p:Ljava/lang/String;

    move-object v1, v2

    .line 1339713
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1339714
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1339715
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1339716
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1339717
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1339718
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1339719
    iget v1, p0, LX/8Ob;->D:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/8Ob;->D:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1339720
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1339721
    iget-object v0, p0, LX/8Ob;->p:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const v2, 0x3253ced3

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1339722
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1339723
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1339724
    iget-object v1, p0, LX/8Ob;->E:LX/60x;

    if-eqz v1, :cond_3

    .line 1339725
    iget-object v1, p0, LX/8Ob;->E:LX/60x;

    iget-object v1, v1, LX/60x;->i:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1339726
    const-string v1, "location"

    iget-object v2, p0, LX/8Ob;->E:LX/60x;

    iget-object v2, v2, LX/60x;->i:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339727
    :cond_0
    iget-object v1, p0, LX/8Ob;->E:LX/60x;

    iget-object v1, v1, LX/60x;->j:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1339728
    const-string v1, "date"

    iget-object v2, p0, LX/8Ob;->E:LX/60x;

    iget-object v2, v2, LX/60x;->j:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339729
    :cond_1
    iget-object v1, p0, LX/8Ob;->E:LX/60x;

    iget-wide v2, v1, LX/60x;->a:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 1339730
    const-string v1, "durationMs"

    iget-object v2, p0, LX/8Ob;->E:LX/60x;

    iget-wide v2, v2, LX/60x;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339731
    :cond_2
    const-string v1, "source_width"

    iget-object v2, p0, LX/8Ob;->E:LX/60x;

    iget v2, v2, LX/60x;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339732
    const-string v1, "source_height"

    iget-object v2, p0, LX/8Ob;->E:LX/60x;

    iget v2, v2, LX/60x;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339733
    const-string v1, "source_bit_rate"

    iget-object v2, p0, LX/8Ob;->E:LX/60x;

    iget v2, v2, LX/60x;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339734
    const-string v1, "source_audio_bit_rate"

    iget-object v2, p0, LX/8Ob;->E:LX/60x;

    iget v2, v2, LX/60x;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339735
    const-string v1, "source_rotation_angle"

    iget-object v2, p0, LX/8Ob;->E:LX/60x;

    iget v2, v2, LX/60x;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339736
    :cond_3
    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1339737
    iget-object v0, p0, LX/8Ob;->W:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 1339738
    return-void
.end method

.method public final f()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 1339739
    iget-object v0, p0, LX/8Ob;->W:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method
