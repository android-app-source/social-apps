.class public LX/6nr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1La;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:LX/6ni;

.field private final c:LX/1La;

.field public final d:LX/1La;

.field public final e:LX/6o9;

.field public final f:LX/6o6;

.field public final g:LX/6oC;

.field private final h:LX/6pC;

.field private final i:Lcom/facebook/content/SecureContextHelper;

.field public final j:LX/6p6;

.field public final k:LX/6nh;

.field public final l:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(LX/6o9;LX/6o6;LX/6oC;LX/6pC;Lcom/facebook/content/SecureContextHelper;LX/6p6;LX/6nh;)V
    .locals 1
    .param p7    # LX/6nh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1147668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1147669
    new-instance v0, LX/6nj;

    invoke-direct {v0, p0}, LX/6nj;-><init>(LX/6nr;)V

    iput-object v0, p0, LX/6nr;->b:LX/6ni;

    .line 1147670
    new-instance v0, LX/6nk;

    invoke-direct {v0, p0}, LX/6nk;-><init>(LX/6nr;)V

    iput-object v0, p0, LX/6nr;->a:LX/1La;

    .line 1147671
    new-instance v0, LX/6nl;

    invoke-direct {v0, p0}, LX/6nl;-><init>(LX/6nr;)V

    iput-object v0, p0, LX/6nr;->c:LX/1La;

    .line 1147672
    new-instance v0, LX/6nm;

    invoke-direct {v0, p0}, LX/6nm;-><init>(LX/6nr;)V

    iput-object v0, p0, LX/6nr;->d:LX/1La;

    .line 1147673
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/6nr;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1147674
    iput-object p1, p0, LX/6nr;->e:LX/6o9;

    .line 1147675
    iput-object p2, p0, LX/6nr;->f:LX/6o6;

    .line 1147676
    iput-object p3, p0, LX/6nr;->g:LX/6oC;

    .line 1147677
    iput-object p4, p0, LX/6nr;->h:LX/6pC;

    .line 1147678
    iput-object p5, p0, LX/6nr;->i:Lcom/facebook/content/SecureContextHelper;

    .line 1147679
    iput-object p6, p0, LX/6nr;->j:LX/6p6;

    .line 1147680
    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6nh;

    iput-object v0, p0, LX/6nr;->k:LX/6nh;

    .line 1147681
    invoke-static {p0}, LX/6nr;->g(LX/6nr;)V

    .line 1147682
    return-void
.end method

.method public static a(LX/6nr;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 1147647
    iget-object v0, p0, LX/6nr;->k:LX/6nh;

    .line 1147648
    iget-object v1, v0, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    move-object v0, v1

    .line 1147649
    iget-object v1, p0, LX/6nr;->a:LX/1La;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 1147650
    iget-object v0, p0, LX/6nr;->k:LX/6nh;

    .line 1147651
    iget-object v1, v0, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    move-object v0, v1

    .line 1147652
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0052

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 1147653
    iget-object v1, p0, LX/6nr;->k:LX/6nh;

    .line 1147654
    iget-object v2, v1, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    move-object v1, v2

    .line 1147655
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/6pF;->VERIFY:LX/6pF;

    invoke-static {v2}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b(LX/6pF;)LX/6pE;

    move-result-object v2

    .line 1147656
    iput-object p2, v2, LX/6pE;->f:Ljava/lang/String;

    .line 1147657
    move-object v2, v2

    .line 1147658
    iput v0, v2, LX/6pE;->g:F

    .line 1147659
    move-object v0, v2

    .line 1147660
    invoke-static {}, LX/6nr;->l()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v2

    .line 1147661
    iput-object v2, v0, LX/6pE;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1147662
    move-object v0, v0

    .line 1147663
    invoke-virtual {v0}, LX/6pE;->a()Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->a(Landroid/content/Context;Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;)Landroid/content/Intent;

    move-result-object v0

    .line 1147664
    iget-object v1, p0, LX/6nr;->i:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/6nr;->k:LX/6nh;

    .line 1147665
    iget-object p0, v2, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    move-object v2, p0

    .line 1147666
    invoke-interface {v1, v0, p1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1147667
    return-void
.end method

.method public static a$redex0(LX/6nr;Lcom/facebook/payments/auth/pin/model/PaymentPin;)V
    .locals 2

    .prologue
    .line 1147638
    invoke-virtual {p1}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1147639
    iget-object v0, p0, LX/6nr;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1147640
    :goto_0
    return-void

    .line 1147641
    :cond_0
    invoke-static {p0}, LX/6nr;->o(LX/6nr;)LX/6nc;

    move-result-object v0

    new-instance v1, LX/6qI;

    invoke-direct {v1}, LX/6qI;-><init>()V

    invoke-virtual {v0, v1}, LX/6nc;->a(LX/6qH;)V

    goto :goto_0

    .line 1147642
    :cond_1
    invoke-static {p0}, LX/6nr;->o(LX/6nr;)LX/6nc;

    move-result-object v0

    invoke-virtual {v0}, LX/6nc;->b()V

    .line 1147643
    iget-object v0, p0, LX/6nr;->f:LX/6o6;

    invoke-virtual {v0}, LX/6o6;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/6nr;->e:LX/6o9;

    invoke-virtual {v0}, LX/6o9;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1147644
    if-eqz v0, :cond_2

    .line 1147645
    invoke-static {p0}, LX/6nr;->e$redex0(LX/6nr;)V

    goto :goto_0

    .line 1147646
    :cond_2
    invoke-static {p0}, LX/6nr;->h(LX/6nr;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static e$redex0(LX/6nr;)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1147610
    iget-object v0, p0, LX/6nr;->f:LX/6o6;

    iget-object v1, p0, LX/6nr;->g:LX/6oC;

    invoke-virtual {v0, v1}, LX/6o6;->a(LX/6oB;)LX/6o5;

    move-result-object v0

    .line 1147611
    sget-object v1, LX/6nq;->a:[I

    invoke-virtual {v0}, LX/6o5;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1147612
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected Availability "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 1147613
    :pswitch_0
    const/16 v0, 0x1389

    iget-object v1, p0, LX/6nr;->k:LX/6nh;

    .line 1147614
    iget-object v2, v1, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    move-object v1, v2

    .line 1147615
    const v2, 0x7f081dda    # 1.8093E38f

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/6nr;->a(LX/6nr;ILjava/lang/String;)V

    .line 1147616
    :goto_0
    return-void

    .line 1147617
    :pswitch_1
    iget-object v0, p0, LX/6nr;->e:LX/6o9;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/6o9;->a(Z)V

    .line 1147618
    invoke-static {p0}, LX/6nr;->h(LX/6nr;)V

    goto :goto_0

    .line 1147619
    :pswitch_2
    invoke-direct {p0}, LX/6nr;->j()V

    goto :goto_0

    .line 1147620
    :pswitch_3
    iget-object v0, p0, LX/6nr;->g:LX/6oC;

    .line 1147621
    iget-object v1, v0, LX/6oC;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6oH;

    const-string v2, "nonce_key/"

    .line 1147622
    iget-object v0, v1, LX/6oH;->h:LX/6ny;

    invoke-virtual {v0, v2}, LX/6ny;->a(Ljava/lang/String;)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    move v1, v0

    .line 1147623
    move v0, v1

    .line 1147624
    if-eqz v0, :cond_0

    .line 1147625
    iget-object v0, p0, LX/6nr;->k:LX/6nh;

    .line 1147626
    iget-object v1, v0, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    move-object v0, v1

    .line 1147627
    iget-object v1, p0, LX/6nr;->d:LX/1La;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 1147628
    new-instance v0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;

    invoke-direct {v0}, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;-><init>()V

    .line 1147629
    iget-object v1, p0, LX/6nr;->b:LX/6ni;

    .line 1147630
    iput-object v1, v0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->s:LX/6ni;

    .line 1147631
    iget-object v1, p0, LX/6nr;->k:LX/6nh;

    .line 1147632
    iget-object v2, v1, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    move-object v1, v2

    .line 1147633
    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    iget-object v2, p0, LX/6nr;->k:LX/6nh;

    .line 1147634
    iget-object p0, v2, LX/6nh;->c:Ljava/lang/String;

    move-object v2, p0

    .line 1147635
    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1147636
    goto :goto_0

    .line 1147637
    :cond_0
    invoke-direct {p0}, LX/6nr;->j()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static g(LX/6nr;)V
    .locals 3

    .prologue
    .line 1147600
    iget-object v0, p0, LX/6nr;->k:LX/6nh;

    .line 1147601
    iget-object v1, v0, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    move-object v0, v1

    .line 1147602
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    iget-object v1, p0, LX/6nr;->k:LX/6nh;

    .line 1147603
    iget-object v2, v1, LX/6nh;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1147604
    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;

    .line 1147605
    if-eqz v0, :cond_0

    .line 1147606
    iget-object v1, p0, LX/6nr;->b:LX/6ni;

    .line 1147607
    iput-object v1, v0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->s:LX/6ni;

    .line 1147608
    iget-object v0, p0, LX/6nr;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1147609
    :cond_0
    return-void
.end method

.method public static h(LX/6nr;)V
    .locals 4

    .prologue
    .line 1147683
    iget-object v0, p0, LX/6nr;->k:LX/6nh;

    .line 1147684
    iget-object v1, v0, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    move-object v0, v1

    .line 1147685
    iget-object v1, p0, LX/6nr;->a:LX/1La;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 1147686
    iget-object v0, p0, LX/6nr;->k:LX/6nh;

    .line 1147687
    iget-object v1, v0, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    move-object v0, v1

    .line 1147688
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/6pF;->VERIFY:LX/6pF;

    invoke-static {v1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b(LX/6pF;)LX/6pE;

    move-result-object v1

    invoke-static {}, LX/6nr;->l()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v2

    .line 1147689
    iput-object v2, v1, LX/6pE;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1147690
    move-object v1, v1

    .line 1147691
    invoke-virtual {v1}, LX/6pE;->a()Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->a(Landroid/content/Context;Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;)Landroid/content/Intent;

    move-result-object v0

    .line 1147692
    iget-object v1, p0, LX/6nr;->i:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x1389

    iget-object v3, p0, LX/6nr;->k:LX/6nh;

    .line 1147693
    iget-object p0, v3, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    move-object v3, p0

    .line 1147694
    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1147695
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    .line 1147596
    const/16 v0, 0x138a

    iget-object v1, p0, LX/6nr;->k:LX/6nh;

    .line 1147597
    iget-object v2, v1, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    move-object v1, v2

    .line 1147598
    const v2, 0x7f081dd9

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/6nr;->a(LX/6nr;ILjava/lang/String;)V

    .line 1147599
    return-void
.end method

.method private static l()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;
    .locals 2

    .prologue
    .line 1147589
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v0

    sget-object v1, LX/6ws;->MODAL_BOTTOM:LX/6ws;

    .line 1147590
    iput-object v1, v0, LX/6wu;->a:LX/6ws;

    .line 1147591
    move-object v0, v0

    .line 1147592
    sget-object v1, LX/73i;->PAYMENTS_WHITE:LX/73i;

    .line 1147593
    iput-object v1, v0, LX/6wu;->b:LX/73i;

    .line 1147594
    move-object v0, v0

    .line 1147595
    invoke-virtual {v0}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v0

    return-object v0
.end method

.method public static n(LX/6nr;)V
    .locals 2

    .prologue
    .line 1147587
    iget-object v0, p0, LX/6nr;->l:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    const-string v1, "authentication not in progress"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1147588
    return-void
.end method

.method public static o(LX/6nr;)LX/6nc;
    .locals 1

    .prologue
    .line 1147584
    iget-object v0, p0, LX/6nr;->k:LX/6nh;

    .line 1147585
    iget-object p0, v0, LX/6nh;->b:LX/6nc;

    move-object v0, p0

    .line 1147586
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1147566
    iget-object v0, p0, LX/6nr;->k:LX/6nh;

    .line 1147567
    iget-object v1, v0, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    move-object v0, v1

    .line 1147568
    iget-object v1, p0, LX/6nr;->a:LX/1La;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(LX/1Lb;)V

    .line 1147569
    iget-object v0, p0, LX/6nr;->k:LX/6nh;

    .line 1147570
    iget-object v1, v0, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    move-object v0, v1

    .line 1147571
    iget-object v1, p0, LX/6nr;->c:LX/1La;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(LX/1Lb;)V

    .line 1147572
    iget-object v0, p0, LX/6nr;->k:LX/6nh;

    .line 1147573
    iget-object v1, v0, LX/6nh;->a:Lcom/facebook/base/fragment/FbFragment;

    move-object v0, v1

    .line 1147574
    iget-object v1, p0, LX/6nr;->c:LX/1La;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 1147575
    iget-object v0, p0, LX/6nr;->k:LX/6nh;

    .line 1147576
    iget-object v1, v0, LX/6nh;->d:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    move-object v0, v1

    .line 1147577
    if-eqz v0, :cond_0

    .line 1147578
    iget-object v0, p0, LX/6nr;->k:LX/6nh;

    .line 1147579
    iget-object v1, v0, LX/6nh;->d:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    move-object v0, v1

    .line 1147580
    invoke-static {p0, v0}, LX/6nr;->a$redex0(LX/6nr;Lcom/facebook/payments/auth/pin/model/PaymentPin;)V

    .line 1147581
    :goto_0
    return-void

    .line 1147582
    :cond_0
    iget-object v0, p0, LX/6nr;->j:LX/6p6;

    new-instance v1, LX/6no;

    invoke-direct {v1, p0}, LX/6no;-><init>(LX/6nr;)V

    invoke-virtual {v0, v1}, LX/6p6;->a(LX/6nn;)V

    .line 1147583
    goto :goto_0
.end method
