.class public final LX/7Pd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Di;


# instance fields
.field public final synthetic a:LX/3Di;

.field public final synthetic b:J

.field public final synthetic c:J

.field public final synthetic d:LX/7Pe;


# direct methods
.method public constructor <init>(LX/7Pe;LX/3Di;JJ)V
    .locals 1

    .prologue
    .line 1203018
    iput-object p1, p0, LX/7Pd;->d:LX/7Pe;

    iput-object p2, p0, LX/7Pd;->a:LX/3Di;

    iput-wide p3, p0, LX/7Pd;->b:J

    iput-wide p5, p0, LX/7Pd;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/3Dd;)Ljava/io/OutputStream;
    .locals 7

    .prologue
    .line 1203019
    iget-object v0, p0, LX/7Pd;->a:LX/3Di;

    invoke-interface {v0, p1}, LX/3Di;->a(LX/3Dd;)Ljava/io/OutputStream;

    move-result-object v0

    .line 1203020
    iget-object v1, p0, LX/7Pd;->d:LX/7Pe;

    iget-object v1, v1, LX/7Pe;->a:LX/7Po;

    iget-wide v2, p0, LX/7Pd;->b:J

    iget-wide v4, p0, LX/7Pd;->c:J

    iget-object v6, p0, LX/7Pd;->d:LX/7Pe;

    iget-object v6, v6, LX/7Pe;->c:Ljava/lang/String;

    .line 1203021
    if-eqz v6, :cond_0

    .line 1203022
    new-instance p0, LX/2qR;

    invoke-direct {p0, v2, v3, v4, v5}, LX/2qR;-><init>(JJ)V

    invoke-static {v1, v6, p0}, LX/7Po;->a$redex0(LX/7Po;Ljava/lang/String;LX/1AD;)V

    .line 1203023
    :cond_0
    return-object v0
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 1

    .prologue
    .line 1203024
    iget-object v0, p0, LX/7Pd;->d:LX/7Pe;

    iget-object v0, v0, LX/7Pe;->d:LX/04n;

    if-eqz v0, :cond_0

    .line 1203025
    iget-object v0, p0, LX/7Pd;->d:LX/7Pe;

    iget-object v0, v0, LX/7Pe;->d:LX/04n;

    invoke-interface {v0}, LX/04n;->c()V

    .line 1203026
    :cond_0
    iget-object v0, p0, LX/7Pd;->a:LX/3Di;

    invoke-interface {v0, p1}, LX/3Di;->a(Ljava/io/IOException;)V

    .line 1203027
    return-void
.end method

.method public final a(Ljava/io/OutputStream;Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 1203028
    iget-object v0, p0, LX/7Pd;->a:LX/3Di;

    invoke-interface {v0, p1, p2}, LX/3Di;->a(Ljava/io/OutputStream;Ljava/io/IOException;)V

    .line 1203029
    iget-object v0, p0, LX/7Pd;->d:LX/7Pe;

    iget-object v0, v0, LX/7Pe;->d:LX/04n;

    if-eqz v0, :cond_0

    .line 1203030
    iget-object v0, p0, LX/7Pd;->d:LX/7Pe;

    iget-object v0, v0, LX/7Pe;->d:LX/04n;

    invoke-interface {v0}, LX/04n;->c()V

    .line 1203031
    :cond_0
    iget-object v0, p0, LX/7Pd;->d:LX/7Pe;

    iget-object v0, v0, LX/7Pe;->a:LX/7Po;

    iget-object v1, p0, LX/7Pd;->d:LX/7Pe;

    iget-object v1, v1, LX/7Pe;->c:Ljava/lang/String;

    invoke-static {v0, v1}, LX/7Po;->a$redex0(LX/7Po;Ljava/lang/String;)V

    .line 1203032
    return-void
.end method
