.class public final LX/6yh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6yS;


# instance fields
.field public a:LX/6qh;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1160314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160315
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)LX/6E6;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1160316
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1160317
    iput-object p1, p0, LX/6yh;->a:LX/6qh;

    .line 1160318
    return-void
.end method

.method public final b(Landroid/view/ViewGroup;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)LX/6E6;
    .locals 3

    .prologue
    .line 1160319
    new-instance v0, LX/73Y;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/73Y;-><init>(Landroid/content/Context;)V

    .line 1160320
    const v1, 0x7f080c75

    invoke-virtual {v0, v1}, LX/73Y;->setSecurityInfo(I)V

    .line 1160321
    const-string v1, "https://m.facebook.com/payer_protection"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "https://m.facebook.com/payments_terms"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1160322
    iget-object p1, v0, LX/73Y;->a:Lcom/facebook/payments/ui/PaymentsSecurityInfoView;

    invoke-virtual {p1, v1, v2}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->a(Landroid/net/Uri;Landroid/net/Uri;)V

    .line 1160323
    iget-object v1, p0, LX/6yh;->a:LX/6qh;

    invoke-virtual {v0, v1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1160324
    invoke-interface {p2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    if-eqz v1, :cond_0

    invoke-interface {p2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    iget-boolean v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->d:Z

    if-eqz v1, :cond_0

    .line 1160325
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/73Y;->setVisibilityOfDeleteButton(I)V

    .line 1160326
    const v1, 0x7f081e7c

    invoke-virtual {v0, v1}, LX/73Y;->setDeleteButtonText(I)V

    .line 1160327
    new-instance v1, LX/6yg;

    invoke-direct {v1, p0, p2}, LX/6yg;-><init>(LX/6yh;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)V

    invoke-virtual {v0, v1}, LX/73Y;->setOnClickListenerForDeleteButton(Landroid/view/View$OnClickListener;)V

    .line 1160328
    :cond_0
    return-object v0
.end method
