.class public LX/7Q0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0po;


# static fields
.field private static final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/io/File;

.field private final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1203517
    new-instance v0, LX/7Pz;

    invoke-direct {v0}, LX/7Pz;-><init>()V

    sput-object v0, LX/7Q0;->c:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;I)V
    .locals 0

    .prologue
    .line 1203518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1203519
    iput-object p1, p0, LX/7Q0;->a:Ljava/io/File;

    .line 1203520
    iput p2, p0, LX/7Q0;->b:I

    .line 1203521
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1203522
    iget-object v0, p0, LX/7Q0;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 1203523
    if-nez v0, :cond_1

    .line 1203524
    :cond_0
    return-void

    .line 1203525
    :cond_1
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1203526
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, p1, :cond_0

    .line 1203527
    sget-object v1, LX/7Q0;->c:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1203528
    const/4 v1, 0x0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, p1

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 1203529
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 1203530
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method


# virtual methods
.method public final U_()V
    .locals 1

    .prologue
    .line 1203531
    iget v0, p0, LX/7Q0;->b:I

    invoke-direct {p0, v0}, LX/7Q0;->a(I)V

    .line 1203532
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1203533
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/7Q0;->a(I)V

    .line 1203534
    return-void
.end method
