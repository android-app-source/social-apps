.class public final LX/6fc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:J

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1120101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)LX/6fc;
    .locals 1

    .prologue
    .line 1120078
    iput-wide p1, p0, LX/6fc;->e:J

    .line 1120079
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;)LX/6fc;
    .locals 0

    .prologue
    .line 1120099
    iput-object p1, p0, LX/6fc;->c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 1120100
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/6fc;
    .locals 0

    .prologue
    .line 1120097
    iput-object p1, p0, LX/6fc;->a:Ljava/lang/String;

    .line 1120098
    return-object p0
.end method

.method public final a()Lcom/facebook/messaging/model/threads/BookingRequestDetail;
    .locals 1

    .prologue
    .line 1120096
    new-instance v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/model/threads/BookingRequestDetail;-><init>(LX/6fc;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/6fc;
    .locals 0

    .prologue
    .line 1120094
    iput-object p1, p0, LX/6fc;->b:Ljava/lang/String;

    .line 1120095
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/6fc;
    .locals 0

    .prologue
    .line 1120092
    iput-object p1, p0, LX/6fc;->d:Ljava/lang/String;

    .line 1120093
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/6fc;
    .locals 0

    .prologue
    .line 1120090
    iput-object p1, p0, LX/6fc;->f:Ljava/lang/String;

    .line 1120091
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/6fc;
    .locals 0

    .prologue
    .line 1120088
    iput-object p1, p0, LX/6fc;->g:Ljava/lang/String;

    .line 1120089
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/6fc;
    .locals 0

    .prologue
    .line 1120086
    iput-object p1, p0, LX/6fc;->h:Ljava/lang/String;

    .line 1120087
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/6fc;
    .locals 0

    .prologue
    .line 1120084
    iput-object p1, p0, LX/6fc;->i:Ljava/lang/String;

    .line 1120085
    return-object p0
.end method

.method public final h(Ljava/lang/String;)LX/6fc;
    .locals 0

    .prologue
    .line 1120082
    iput-object p1, p0, LX/6fc;->j:Ljava/lang/String;

    .line 1120083
    return-object p0
.end method

.method public final i(Ljava/lang/String;)LX/6fc;
    .locals 0

    .prologue
    .line 1120080
    iput-object p1, p0, LX/6fc;->k:Ljava/lang/String;

    .line 1120081
    return-object p0
.end method
