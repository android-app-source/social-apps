.class public final LX/7Cv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private a:LX/3II;

.field private b:F

.field private c:F


# direct methods
.method public constructor <init>(LX/3II;FF)V
    .locals 0

    .prologue
    .line 1182018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1182019
    iput-object p1, p0, LX/7Cv;->a:LX/3II;

    .line 1182020
    iput p2, p0, LX/7Cv;->b:F

    .line 1182021
    iput p3, p0, LX/7Cv;->c:F

    .line 1182022
    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 1182023
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v0

    .line 1182024
    iget v1, p0, LX/7Cv;->b:F

    iget v2, p0, LX/7Cv;->c:F

    iget v3, p0, LX/7Cv;->b:F

    sub-float/2addr v2, v3

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    .line 1182025
    iget-object v1, p0, LX/7Cv;->a:LX/3II;

    invoke-interface {v1}, LX/3II;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1182026
    iget-object v1, p0, LX/7Cv;->a:LX/3II;

    invoke-interface {v1}, LX/3II;->get360TextureView()LX/2qW;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/2qW;->setPreferredVerticalFOVOnZoom(F)V

    .line 1182027
    :cond_0
    return-void
.end method
