.class public final enum LX/7zK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7zK;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7zK;

.field public static final enum FOUND:LX/7zK;

.field public static final enum NOT_ATTEMPTED:LX/7zK;

.field public static final enum NOT_FOUND:LX/7zK;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1280904
    new-instance v0, LX/7zK;

    const-string v1, "NOT_ATTEMPTED"

    invoke-direct {v0, v1, v2}, LX/7zK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7zK;->NOT_ATTEMPTED:LX/7zK;

    .line 1280905
    new-instance v0, LX/7zK;

    const-string v1, "FOUND"

    invoke-direct {v0, v1, v3}, LX/7zK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7zK;->FOUND:LX/7zK;

    .line 1280906
    new-instance v0, LX/7zK;

    const-string v1, "NOT_FOUND"

    invoke-direct {v0, v1, v4}, LX/7zK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7zK;->NOT_FOUND:LX/7zK;

    .line 1280907
    const/4 v0, 0x3

    new-array v0, v0, [LX/7zK;

    sget-object v1, LX/7zK;->NOT_ATTEMPTED:LX/7zK;

    aput-object v1, v0, v2

    sget-object v1, LX/7zK;->FOUND:LX/7zK;

    aput-object v1, v0, v3

    sget-object v1, LX/7zK;->NOT_FOUND:LX/7zK;

    aput-object v1, v0, v4

    sput-object v0, LX/7zK;->$VALUES:[LX/7zK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1280908
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7zK;
    .locals 1

    .prologue
    .line 1280909
    const-class v0, LX/7zK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7zK;

    return-object v0
.end method

.method public static values()[LX/7zK;
    .locals 1

    .prologue
    .line 1280910
    sget-object v0, LX/7zK;->$VALUES:[LX/7zK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7zK;

    return-object v0
.end method
