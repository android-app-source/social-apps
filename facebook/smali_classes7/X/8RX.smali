.class public final LX/8RX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 1344707
    iput-object p1, p0, LX/8RX;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 1344708
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 1344709
    packed-switch p2, :pswitch_data_0

    .line 1344710
    :goto_0
    return-void

    .line 1344711
    :pswitch_0
    iget-object v0, p0, LX/8RX;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->j:LX/0Sy;

    iget-object v1, p0, LX/8RX;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    .line 1344712
    iget-object p1, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, p1

    .line 1344713
    invoke-virtual {v0, v1}, LX/0Sy;->a(Landroid/view/View;)V

    .line 1344714
    iget-object v0, p0, LX/8RX;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->D(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    goto :goto_0

    .line 1344715
    :pswitch_1
    iget-object v0, p0, LX/8RX;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->j:LX/0Sy;

    iget-object v1, p0, LX/8RX;->a:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    .line 1344716
    iget-object p0, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, p0

    .line 1344717
    invoke-virtual {v0, v1}, LX/0Sy;->b(Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
