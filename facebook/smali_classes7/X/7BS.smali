.class public LX/7BS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:Lcom/facebook/search/api/GraphSearchQuery;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7BK;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:Ljava/lang/String;

.field public i:LX/7BZ;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1178891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178892
    sget-object v0, LX/7BZ;->DEFAULT_KEYWORD_MODE:LX/7BZ;

    iput-object v0, p0, LX/7BS;->i:LX/7BZ;

    return-void
.end method


# virtual methods
.method public f()Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;
    .locals 1

    .prologue
    .line 1178893
    iget-object v0, p0, LX/7BS;->a:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1178894
    iget v0, p0, LX/7BS;->c:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1178895
    new-instance v0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;

    invoke-direct {v0, p0}, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;-><init>(LX/7BS;)V

    return-object v0

    .line 1178896
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
