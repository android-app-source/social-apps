.class public LX/7St;
.super LX/2Md;
.source ""


# instance fields
.field public a:LX/2Mg;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1209361
    invoke-direct {p0}, LX/2Md;-><init>()V

    .line 1209362
    sget-object v0, LX/2Mg;->h:LX/2Mg;

    iput-object v0, p0, LX/7St;->a:LX/2Mg;

    .line 1209363
    const/4 v0, 0x0

    .line 1209364
    iput-boolean v0, p0, LX/2Md;->b:Z

    .line 1209365
    return-void
.end method

.method public static a(LX/0QB;)LX/7St;
    .locals 1

    .prologue
    .line 1209366
    new-instance v0, LX/7St;

    invoke-direct {v0}, LX/7St;-><init>()V

    .line 1209367
    move-object v0, v0

    .line 1209368
    return-object v0
.end method

.method public static b(II)I
    .locals 4

    .prologue
    .line 1209369
    mul-int v0, p0, p1

    mul-int/lit8 v0, v0, 0x1e

    mul-int/lit8 v0, v0, 0x4

    int-to-double v0, v0

    const-wide v2, 0x3fb1eb851eb851ecL    # 0.07

    mul-double/2addr v0, v2

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a()LX/2Mg;
    .locals 1

    .prologue
    .line 1209370
    iget-object v0, p0, LX/7St;->a:LX/2Mg;

    return-object v0
.end method

.method public final a(II)V
    .locals 9

    .prologue
    .line 1209371
    invoke-static {p1, p2}, LX/7St;->b(II)I

    move-result v0

    const/4 v6, -0x1

    .line 1209372
    new-instance v1, LX/2Mg;

    const/16 v2, 0x500

    mul-int/lit16 v3, v0, 0x400

    const/high16 v4, 0x41f00000    # 30.0f

    const/16 v5, 0xa

    const/4 v8, 0x0

    move v7, v6

    invoke-direct/range {v1 .. v8}, LX/2Mg;-><init>(IIFIIIZ)V

    iput-object v1, p0, LX/7St;->a:LX/2Mg;

    .line 1209373
    return-void
.end method
