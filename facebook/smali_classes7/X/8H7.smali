.class public LX/8H7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/photos/data/method/DeleteVideoParams;",
        "Lcom/facebook/photos/data/method/DeleteVideoResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1320539
    const-class v0, LX/8H7;

    sput-object v0, LX/8H7;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1320537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1320538
    return-void
.end method

.method public static a(LX/0QB;)LX/8H7;
    .locals 1

    .prologue
    .line 1320540
    new-instance v0, LX/8H7;

    invoke-direct {v0}, LX/8H7;-><init>()V

    .line 1320541
    move-object v0, v0

    .line 1320542
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1320517
    check-cast p1, Lcom/facebook/photos/data/method/DeleteVideoParams;

    .line 1320518
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1320519
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1320520
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    sget-object v2, LX/8H7;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1320521
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1320522
    move-object v1, v1

    .line 1320523
    const-string v2, "DELETE"

    .line 1320524
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1320525
    move-object v1, v1

    .line 1320526
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "v2.3/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1320527
    iget-object v3, p1, Lcom/facebook/photos/data/method/DeleteVideoParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1320528
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1320529
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1320530
    move-object v1, v1

    .line 1320531
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1320532
    move-object v0, v1

    .line 1320533
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1320534
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1320535
    move-object v0, v0

    .line 1320536
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1320514
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1320515
    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1320516
    new-instance v1, Lcom/facebook/photos/data/method/DeleteVideoResponse;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v1, v0}, Lcom/facebook/photos/data/method/DeleteVideoResponse;-><init>(Z)V

    return-object v1
.end method
