.class public final LX/7K2;
.super LX/0aq;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aq",
        "<",
        "Landroid/net/Uri;",
        "LX/7K5;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7K3;


# direct methods
.method public constructor <init>(LX/7K3;I)V
    .locals 0

    .prologue
    .line 1194404
    iput-object p1, p0, LX/7K2;->a:LX/7K3;

    .line 1194405
    invoke-direct {p0, p2}, LX/0aq;-><init>(I)V

    .line 1194406
    return-void
.end method


# virtual methods
.method public final a(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1194393
    check-cast p2, Landroid/net/Uri;

    check-cast p3, LX/7K5;

    check-cast p4, LX/7K5;

    const/4 v1, 0x0

    .line 1194394
    if-eqz p1, :cond_1

    .line 1194395
    invoke-virtual {p3}, LX/7K5;->b()Landroid/media/MediaPlayer;

    move-result-object v0

    .line 1194396
    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 1194397
    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 1194398
    iget-object v1, p0, LX/7K2;->a:LX/7K3;

    iget-object v1, v1, LX/7K3;->l:LX/0wq;

    iget-boolean v1, v1, LX/0wq;->v:Z

    if-eqz v1, :cond_0

    .line 1194399
    iget-object v1, p0, LX/7K2;->a:LX/7K3;

    iget-object v1, v1, LX/7K3;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/video/engine/MediaPlayerPool$MediaPlayerCache$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/video/engine/MediaPlayerPool$MediaPlayerCache$1;-><init>(LX/7K2;Landroid/media/MediaPlayer;)V

    const v0, 0x4d667dca    # 2.41687712E8f

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1194400
    :goto_0
    return-void

    .line 1194401
    :cond_0
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    goto :goto_0

    .line 1194402
    :cond_1
    invoke-virtual {p3}, LX/7K5;->c()LX/2qD;

    iget-object v0, p0, LX/7K2;->a:LX/7K3;

    iget-object v0, v0, LX/7K3;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-virtual {p3}, LX/7K5;->d()J

    .line 1194403
    if-nez p4, :cond_2

    const/4 v0, 0x1

    :goto_1
    const-string v1, "Entry shall not be removed by overwriting using put(), it should be remove()ed"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
