.class public final LX/7YH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(Lcom/facebook/bugreporter/BugReport;LX/186;)I
    .locals 26

    .prologue
    .line 1219518
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/bugreporter/BugReport;->a:Landroid/net/Uri;

    sget-object v4, LX/4Bw;->a:LX/4Bw;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(Ljava/lang/Object;LX/4Bv;)I

    move-result v3

    .line 1219519
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/bugreporter/BugReport;->b:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1219520
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/bugreporter/BugReport;->c:Landroid/net/Uri;

    sget-object v6, LX/4Bw;->a:LX/4Bw;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, LX/186;->a(Ljava/lang/Object;LX/4Bv;)I

    move-result v5

    .line 1219521
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/bugreporter/BugReport;->d:LX/0Px;

    sget-object v7, LX/4Bw;->a:LX/4Bw;

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7, v8}, LX/186;->a(Ljava/util/List;LX/4Bv;Z)I

    move-result v6

    .line 1219522
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/bugreporter/BugReport;->e:LX/0P1;

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, LX/186;->a(Ljava/util/Map;Z)I

    move-result v7

    .line 1219523
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/bugreporter/BugReport;->f:LX/0P1;

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v9}, LX/186;->a(Ljava/util/Map;Z)I

    move-result v8

    .line 1219524
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/bugreporter/BugReport;->g:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1219525
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/bugreporter/BugReport;->h:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1219526
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/bugreporter/BugReport;->i:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1219527
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/bugreporter/BugReport;->j:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1219528
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/bugreporter/BugReport;->k:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1219529
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/bugreporter/BugReport;->l:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1219530
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/facebook/bugreporter/BugReport;->m:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 1219531
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/bugreporter/BugReport;->n:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 1219532
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/bugreporter/BugReport;->o:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 1219533
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/bugreporter/BugReport;->p:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 1219534
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/bugreporter/BugReport;->q:LX/6Fb;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 1219535
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/bugreporter/BugReport;->r:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 1219536
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/bugreporter/BugReport;->t:LX/0P1;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->a(Ljava/util/Map;Z)I

    move-result v21

    .line 1219537
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/bugreporter/BugReport;->u:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 1219538
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/bugreporter/BugReport;->v:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 1219539
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/bugreporter/BugReport;->w:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 1219540
    const/16 v25, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1219541
    const/16 v25, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1219542
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1219543
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1219544
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1219545
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1219546
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1219547
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1219548
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1219549
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1219550
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1219551
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1219552
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1219553
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1219554
    const/16 v3, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1219555
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1219556
    const/16 v3, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1219557
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1219558
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1219559
    const/16 v3, 0x13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/bugreporter/BugReport;->s:I

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 1219560
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1219561
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1219562
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1219563
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1219564
    const/16 v3, 0x18

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/bugreporter/BugReport;->x:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1219565
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public static a(Ljava/lang/String;)LX/6zP;
    .locals 2

    .prologue
    .line 1219566
    invoke-static {}, LX/6zU;->values()[LX/6zU;

    move-result-object v0

    invoke-static {v0, p0}, LX/6LV;->a([LX/6LU;Ljava/lang/Object;)LX/6LU;

    move-result-object v0

    invoke-static {}, LX/6zQ;->values()[LX/6zQ;

    move-result-object v1

    invoke-static {v1, p0}, LX/6LV;->a([LX/6LU;Ljava/lang/Object;)LX/6LU;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/6zU;->UNKNOWN:LX/6zU;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6zP;

    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1219567
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1219568
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1219569
    if-eqz v0, :cond_0

    .line 1219570
    const-string v0, "broadcast_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219571
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219572
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1219573
    if-eqz v0, :cond_1

    .line 1219574
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219575
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219576
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1219577
    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1219578
    check-cast p0, Landroid/media/MediaRouter$UserRouteInfo;

    check-cast p1, Landroid/media/MediaRouter$VolumeCallback;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolumeCallback(Landroid/media/MediaRouter$VolumeCallback;)V

    .line 1219579
    return-void
.end method

.method public static a$redex0(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1219580
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1219581
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1219582
    :goto_0
    return v1

    .line 1219583
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1219584
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1219585
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1219586
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1219587
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1219588
    const-string v4, "broadcast_status"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1219589
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto :goto_1

    .line 1219590
    :cond_2
    const-string v4, "id"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1219591
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1219592
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1219593
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1219594
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1219595
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a$redex0(LX/15w;)LX/15i;
    .locals 15

    .prologue
    .line 1219596
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1219597
    const-wide/16 v6, 0x0

    const/4 v3, 0x1

    const/4 v9, 0x0

    .line 1219598
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_7

    .line 1219599
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1219600
    :goto_0
    move v1, v9

    .line 1219601
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1219602
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    return-object v0

    .line 1219603
    :cond_0
    const-string v13, "total_tip_amount"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 1219604
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v2, v3

    .line 1219605
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, v13, :cond_4

    .line 1219606
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1219607
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1219608
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v13, v14, :cond_1

    if-eqz v12, :cond_1

    .line 1219609
    const-string v13, "id"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1219610
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 1219611
    :cond_2
    const-string v13, "unique_tip_giver_count"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1219612
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v8

    move v10, v8

    move v8, v3

    goto :goto_1

    .line 1219613
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1219614
    :cond_4
    const/4 v12, 0x3

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1219615
    invoke-virtual {v0, v9, v11}, LX/186;->b(II)V

    .line 1219616
    if-eqz v2, :cond_5

    move-object v2, v0

    .line 1219617
    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1219618
    :cond_5
    if-eqz v8, :cond_6

    .line 1219619
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v10, v9}, LX/186;->a(III)V

    .line 1219620
    :cond_6
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    goto :goto_0

    :cond_7
    move v8, v9

    move v2, v9

    move v10, v9

    move-wide v4, v6

    move v11, v9

    goto :goto_1
.end method

.method public static a$redex0(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1219621
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1219622
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1219623
    if-eqz v0, :cond_0

    .line 1219624
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219625
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219626
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1219627
    if-eqz v0, :cond_1

    .line 1219628
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219629
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219630
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1219631
    if-eqz v0, :cond_2

    .line 1219632
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219633
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1219634
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1219635
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1219636
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1219637
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1219638
    :goto_0
    return v1

    .line 1219639
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1219640
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 1219641
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1219642
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1219643
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1219644
    const-string v6, "__type__"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "__typename"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1219645
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    goto :goto_1

    .line 1219646
    :cond_3
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1219647
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1219648
    :cond_4
    const-string v6, "name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1219649
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1219650
    :cond_5
    const-string v6, "profile_picture"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1219651
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1219652
    :cond_6
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1219653
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1219654
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1219655
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1219656
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1219657
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1219658
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1219659
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1219660
    if-eqz v0, :cond_0

    .line 1219661
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219662
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1219663
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1219664
    if-eqz v0, :cond_1

    .line 1219665
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219666
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219667
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1219668
    if-eqz v0, :cond_2

    .line 1219669
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219670
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1219671
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1219672
    if-eqz v0, :cond_3

    .line 1219673
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1219674
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1219675
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1219676
    return-void
.end method
