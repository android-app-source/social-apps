.class public final LX/7DX;
.super Landroid/os/CountDownTimer;
.source ""


# instance fields
.field public final synthetic a:LX/7Da;

.field public b:Z


# direct methods
.method public constructor <init>(LX/7Da;JJ)V
    .locals 2

    .prologue
    .line 1183047
    iput-object p1, p0, LX/7DX;->a:LX/7Da;

    .line 1183048
    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    .line 1183049
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7DX;->b:Z

    .line 1183050
    return-void
.end method


# virtual methods
.method public final onFinish()V
    .locals 8

    .prologue
    .line 1183053
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7DX;->b:Z

    .line 1183054
    new-instance v0, LX/6B6;

    invoke-direct {v0}, LX/6B6;-><init>()V

    move-object v2, v0

    .line 1183055
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1183056
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1183057
    iget-object v0, p0, LX/7DX;->a:LX/7Da;

    iget-object v0, v0, LX/7Da;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1183058
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7DZ;

    .line 1183059
    iget-object v6, v1, LX/7DZ;->a:LX/7DY;

    sget-object v7, LX/7DY;->URI_IN_QUEUE:LX/7DY;

    if-ne v6, v7, :cond_0

    .line 1183060
    sget-object v6, LX/7DY;->FETCHING_URI:LX/7DY;

    .line 1183061
    iput-object v6, v1, LX/7DZ;->a:LX/7DY;

    .line 1183062
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7D7;

    .line 1183063
    new-instance v1, LX/4Jo;

    invoke-direct {v1}, LX/4Jo;-><init>()V

    .line 1183064
    iget v6, v0, LX/7D7;->a:I

    move v6, v6

    .line 1183065
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 1183066
    const-string v7, "level"

    invoke-virtual {v1, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1183067
    iget v6, v0, LX/7D7;->b:I

    move v6, v6

    .line 1183068
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 1183069
    const-string v7, "face"

    invoke-virtual {v1, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1183070
    iget v6, v0, LX/7D7;->c:I

    move v6, v6

    .line 1183071
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 1183072
    const-string v7, "col"

    invoke-virtual {v1, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1183073
    iget v6, v0, LX/7D7;->d:I

    move v6, v6

    .line 1183074
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 1183075
    const-string v7, "row"

    invoke-virtual {v1, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1183076
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1183077
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1183078
    :cond_1
    const-string v0, "id"

    iget-object v1, p0, LX/7DX;->a:LX/7Da;

    iget-object v1, v1, LX/7Da;->f:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1183079
    const-string v0, "tile_info_list"

    invoke-virtual {v2, v0, v3}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1183080
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1183081
    iget-object v1, p0, LX/7DX;->a:LX/7Da;

    iget-object v1, v1, LX/7Da;->d:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1183082
    new-instance v1, LX/7DW;

    invoke-direct {v1, p0, v4}, LX/7DW;-><init>(LX/7DX;Ljava/util/List;)V

    invoke-static {}, LX/44p;->b()LX/44p;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1183083
    return-void
.end method

.method public final onTick(J)V
    .locals 1

    .prologue
    .line 1183051
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7DX;->b:Z

    .line 1183052
    return-void
.end method
