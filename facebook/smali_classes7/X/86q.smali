.class public final enum LX/86q;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/86q;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/86q;

.field public static final enum CAPTURE_PHOTO_REQUESTED:LX/86q;

.field public static final enum ERROR:LX/86q;

.field public static final enum FLIPPING:LX/86q;

.field public static final enum READY:LX/86q;

.field public static final enum RECORDING_VIDEO:LX/86q;

.field public static final enum START_RECORD_VIDEO_REQUESTED:LX/86q;

.field public static final enum STOP_RECORD_VIDEO_REQUESTED:LX/86q;

.field public static final enum UNINITIALIZED:LX/86q;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1297949
    new-instance v0, LX/86q;

    const-string v1, "UNINITIALIZED"

    invoke-direct {v0, v1, v3}, LX/86q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86q;->UNINITIALIZED:LX/86q;

    .line 1297950
    new-instance v0, LX/86q;

    const-string v1, "FLIPPING"

    invoke-direct {v0, v1, v4}, LX/86q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86q;->FLIPPING:LX/86q;

    .line 1297951
    new-instance v0, LX/86q;

    const-string v1, "READY"

    invoke-direct {v0, v1, v5}, LX/86q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86q;->READY:LX/86q;

    .line 1297952
    new-instance v0, LX/86q;

    const-string v1, "CAPTURE_PHOTO_REQUESTED"

    invoke-direct {v0, v1, v6}, LX/86q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86q;->CAPTURE_PHOTO_REQUESTED:LX/86q;

    .line 1297953
    new-instance v0, LX/86q;

    const-string v1, "START_RECORD_VIDEO_REQUESTED"

    invoke-direct {v0, v1, v7}, LX/86q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86q;->START_RECORD_VIDEO_REQUESTED:LX/86q;

    .line 1297954
    new-instance v0, LX/86q;

    const-string v1, "RECORDING_VIDEO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/86q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86q;->RECORDING_VIDEO:LX/86q;

    .line 1297955
    new-instance v0, LX/86q;

    const-string v1, "STOP_RECORD_VIDEO_REQUESTED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/86q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86q;->STOP_RECORD_VIDEO_REQUESTED:LX/86q;

    .line 1297956
    new-instance v0, LX/86q;

    const-string v1, "ERROR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/86q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86q;->ERROR:LX/86q;

    .line 1297957
    const/16 v0, 0x8

    new-array v0, v0, [LX/86q;

    sget-object v1, LX/86q;->UNINITIALIZED:LX/86q;

    aput-object v1, v0, v3

    sget-object v1, LX/86q;->FLIPPING:LX/86q;

    aput-object v1, v0, v4

    sget-object v1, LX/86q;->READY:LX/86q;

    aput-object v1, v0, v5

    sget-object v1, LX/86q;->CAPTURE_PHOTO_REQUESTED:LX/86q;

    aput-object v1, v0, v6

    sget-object v1, LX/86q;->START_RECORD_VIDEO_REQUESTED:LX/86q;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/86q;->RECORDING_VIDEO:LX/86q;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/86q;->STOP_RECORD_VIDEO_REQUESTED:LX/86q;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/86q;->ERROR:LX/86q;

    aput-object v2, v0, v1

    sput-object v0, LX/86q;->$VALUES:[LX/86q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1297958
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/86q;
    .locals 1

    .prologue
    .line 1297959
    const-class v0, LX/86q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/86q;

    return-object v0
.end method

.method public static values()[LX/86q;
    .locals 1

    .prologue
    .line 1297960
    sget-object v0, LX/86q;->$VALUES:[LX/86q;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/86q;

    return-object v0
.end method
