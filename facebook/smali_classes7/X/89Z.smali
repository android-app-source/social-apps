.class public final enum LX/89Z;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/89Z;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/89Z;

.field public static final enum FAVORITE_MEDIA_PICKER:LX/89Z;

.field public static final enum PRODUCTION_PROMPTS:LX/89Z;

.field public static final enum SIMPLEPICKER_CAMERA_BUTTON:LX/89Z;

.field public static final enum SIMPLEPICKER_LIVECAM_CELL:LX/89Z;

.field public static final enum UNKNOWN:LX/89Z;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1305071
    new-instance v0, LX/89Z;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v3, v2}, LX/89Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89Z;->UNKNOWN:LX/89Z;

    .line 1305072
    new-instance v0, LX/89Z;

    const-string v1, "SIMPLEPICKER_LIVECAM_CELL"

    const-string v2, "simplepicker_livecam_cell"

    invoke-direct {v0, v1, v4, v2}, LX/89Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89Z;->SIMPLEPICKER_LIVECAM_CELL:LX/89Z;

    .line 1305073
    new-instance v0, LX/89Z;

    const-string v1, "SIMPLEPICKER_CAMERA_BUTTON"

    const-string v2, "simplepicker_camera_button"

    invoke-direct {v0, v1, v5, v2}, LX/89Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89Z;->SIMPLEPICKER_CAMERA_BUTTON:LX/89Z;

    .line 1305074
    new-instance v0, LX/89Z;

    const-string v1, "PRODUCTION_PROMPTS"

    const-string v2, "production_prompts"

    invoke-direct {v0, v1, v6, v2}, LX/89Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89Z;->PRODUCTION_PROMPTS:LX/89Z;

    .line 1305075
    new-instance v0, LX/89Z;

    const-string v1, "FAVORITE_MEDIA_PICKER"

    const-string v2, "favorite_media_picker"

    invoke-direct {v0, v1, v7, v2}, LX/89Z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/89Z;->FAVORITE_MEDIA_PICKER:LX/89Z;

    .line 1305076
    const/4 v0, 0x5

    new-array v0, v0, [LX/89Z;

    sget-object v1, LX/89Z;->UNKNOWN:LX/89Z;

    aput-object v1, v0, v3

    sget-object v1, LX/89Z;->SIMPLEPICKER_LIVECAM_CELL:LX/89Z;

    aput-object v1, v0, v4

    sget-object v1, LX/89Z;->SIMPLEPICKER_CAMERA_BUTTON:LX/89Z;

    aput-object v1, v0, v5

    sget-object v1, LX/89Z;->PRODUCTION_PROMPTS:LX/89Z;

    aput-object v1, v0, v6

    sget-object v1, LX/89Z;->FAVORITE_MEDIA_PICKER:LX/89Z;

    aput-object v1, v0, v7

    sput-object v0, LX/89Z;->$VALUES:[LX/89Z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1305077
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1305078
    iput-object p3, p0, LX/89Z;->name:Ljava/lang/String;

    .line 1305079
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/89Z;
    .locals 1

    .prologue
    .line 1305080
    const-class v0, LX/89Z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/89Z;

    return-object v0
.end method

.method public static values()[LX/89Z;
    .locals 1

    .prologue
    .line 1305081
    sget-object v0, LX/89Z;->$VALUES:[LX/89Z;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/89Z;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1305082
    iget-object v0, p0, LX/89Z;->name:Ljava/lang/String;

    return-object v0
.end method
