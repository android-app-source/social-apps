.class public LX/7Ur;
.super Landroid/view/View;
.source ""

# interfaces
.implements LX/7Um;


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Path;

.field private final c:F

.field private d:Landroid/support/v4/view/ViewPager;

.field private e:LX/0hc;

.field private f:I

.field private g:I

.field private h:F


# virtual methods
.method public final B_(I)V
    .locals 1

    .prologue
    .line 1213944
    iget v0, p0, LX/7Ur;->f:I

    if-nez v0, :cond_0

    .line 1213945
    iput p1, p0, LX/7Ur;->g:I

    .line 1213946
    const/4 v0, 0x0

    iput v0, p0, LX/7Ur;->h:F

    .line 1213947
    invoke-virtual {p0}, LX/7Ur;->invalidate()V

    .line 1213948
    :cond_0
    iget-object v0, p0, LX/7Ur;->e:LX/0hc;

    if-eqz v0, :cond_1

    .line 1213949
    iget-object v0, p0, LX/7Ur;->e:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->B_(I)V

    .line 1213950
    :cond_1
    return-void
.end method

.method public final a(IFI)V
    .locals 1

    .prologue
    .line 1213938
    iput p1, p0, LX/7Ur;->g:I

    .line 1213939
    iput p2, p0, LX/7Ur;->h:F

    .line 1213940
    invoke-virtual {p0}, LX/7Ur;->invalidate()V

    .line 1213941
    iget-object v0, p0, LX/7Ur;->e:LX/0hc;

    if-eqz v0, :cond_0

    .line 1213942
    iget-object v0, p0, LX/7Ur;->e:LX/0hc;

    invoke-interface {v0, p1, p2, p3}, LX/0hc;->a(IFI)V

    .line 1213943
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1213905
    iput p1, p0, LX/7Ur;->f:I

    .line 1213906
    iget-object v0, p0, LX/7Ur;->e:LX/0hc;

    if-eqz v0, :cond_0

    .line 1213907
    iget-object v0, p0, LX/7Ur;->e:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->b(I)V

    .line 1213908
    :cond_0
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1213922
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1213923
    iget-object v0, p0, LX/7Ur;->d:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_1

    .line 1213924
    :cond_0
    :goto_0
    return-void

    .line 1213925
    :cond_1
    iget-object v0, p0, LX/7Ur;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    .line 1213926
    if-eqz v0, :cond_0

    .line 1213927
    iget v1, p0, LX/7Ur;->g:I

    if-lt v1, v0, :cond_2

    .line 1213928
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, LX/7Ur;->setCurrentItem(I)V

    goto :goto_0

    .line 1213929
    :cond_2
    invoke-virtual {p0}, LX/7Ur;->getPaddingLeft()I

    move-result v1

    .line 1213930
    invoke-virtual {p0}, LX/7Ur;->getWidth()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-virtual {p0}, LX/7Ur;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    const/high16 v3, 0x3f800000    # 1.0f

    int-to-float v0, v0

    mul-float/2addr v0, v3

    div-float v0, v2, v0

    .line 1213931
    int-to-float v1, v1

    iget v2, p0, LX/7Ur;->g:I

    int-to-float v2, v2

    iget v3, p0, LX/7Ur;->h:F

    add-float/2addr v2, v3

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    .line 1213932
    add-float/2addr v0, v1

    .line 1213933
    invoke-virtual {p0}, LX/7Ur;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    .line 1213934
    invoke-virtual {p0}, LX/7Ur;->getHeight()I

    move-result v3

    invoke-virtual {p0}, LX/7Ur;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    .line 1213935
    iget-object v4, p0, LX/7Ur;->b:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/Path;->reset()V

    .line 1213936
    iget-object v4, p0, LX/7Ur;->b:Landroid/graphics/Path;

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v1, v2, v0, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget v0, p0, LX/7Ur;->c:F

    iget v1, p0, LX/7Ur;->c:F

    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v4, v5, v0, v1, v2}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 1213937
    iget-object v0, p0, LX/7Ur;->b:Landroid/graphics/Path;

    iget-object v1, p0, LX/7Ur;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 1213917
    check-cast p1, Lcom/facebook/widget/viewpageindicator/LinePageIndicator$SavedState;

    .line 1213918
    invoke-virtual {p1}, Lcom/facebook/widget/viewpageindicator/LinePageIndicator$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1213919
    iget v0, p1, Lcom/facebook/widget/viewpageindicator/LinePageIndicator$SavedState;->a:I

    iput v0, p0, LX/7Ur;->g:I

    .line 1213920
    invoke-virtual {p0}, LX/7Ur;->requestLayout()V

    .line 1213921
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1213951
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1213952
    new-instance v1, Lcom/facebook/widget/viewpageindicator/LinePageIndicator$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/widget/viewpageindicator/LinePageIndicator$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1213953
    iget v0, p0, LX/7Ur;->g:I

    iput v0, v1, Lcom/facebook/widget/viewpageindicator/LinePageIndicator$SavedState;->a:I

    .line 1213954
    return-object v1
.end method

.method public setCurrentItem(I)V
    .locals 2

    .prologue
    .line 1213911
    iget-object v0, p0, LX/7Ur;->d:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    .line 1213912
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewPager has not been bound."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1213913
    :cond_0
    iget-object v0, p0, LX/7Ur;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1213914
    iput p1, p0, LX/7Ur;->g:I

    .line 1213915
    invoke-virtual {p0}, LX/7Ur;->invalidate()V

    .line 1213916
    return-void
.end method

.method public setOnPageChangeListener(LX/0hc;)V
    .locals 0

    .prologue
    .line 1213909
    iput-object p1, p0, LX/7Ur;->e:LX/0hc;

    .line 1213910
    return-void
.end method

.method public setSelectedColor(I)V
    .locals 1

    .prologue
    .line 1213902
    iget-object v0, p0, LX/7Ur;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213903
    invoke-virtual {p0}, LX/7Ur;->invalidate()V

    .line 1213904
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 2

    .prologue
    .line 1213893
    iget-object v0, p0, LX/7Ur;->d:Landroid/support/v4/view/ViewPager;

    if-ne v0, p1, :cond_0

    .line 1213894
    :goto_0
    return-void

    .line 1213895
    :cond_0
    iget-object v0, p0, LX/7Ur;->d:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    .line 1213896
    iget-object v0, p0, LX/7Ur;->d:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1213897
    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1213898
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewPager does not have adapter instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1213899
    :cond_2
    iput-object p1, p0, LX/7Ur;->d:Landroid/support/v4/view/ViewPager;

    .line 1213900
    iget-object v0, p0, LX/7Ur;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1213901
    invoke-virtual {p0}, LX/7Ur;->invalidate()V

    goto :goto_0
.end method
