.class public LX/6s3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6rs;


# direct methods
.method public constructor <init>(LX/6rs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1152879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152880
    iput-object p1, p0, LX/6s3;->a:LX/6rs;

    .line 1152881
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1152882
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 1152883
    const-string v0, "identifier"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6rc;->forValue(Ljava/lang/String;)LX/6rc;

    move-result-object v0

    .line 1152884
    sget-object v1, LX/6rc;->OPTIONS:LX/6rc;

    if-ne v0, v1, :cond_0

    move v0, v6

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152885
    const-string v0, "collected_data_key"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152886
    const-string v0, "title"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152887
    const-string v0, "actionable_title"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152888
    iget-object v0, p0, LX/6s3;->a:LX/6rs;

    invoke-virtual {v0, p1}, LX/6rs;->m(Ljava/lang/String;)LX/6rr;

    move-result-object v0

    const-string v1, "options"

    invoke-static {p2, v1}, LX/16N;->d(LX/0lF;Ljava/lang/String;)LX/162;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Px;

    .line 1152889
    const-string v0, "should_pre_select"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v7, v6

    .line 1152890
    :goto_1
    const-string v0, "collected_data_key"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "actionable_title"

    invoke-virtual {p2, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "option_list_title"

    invoke-virtual {p2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    if-eqz v7, :cond_2

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    iget-object v4, v4, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    :goto_2
    invoke-static/range {v0 .. v5}, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;)LX/6rY;

    move-result-object v0

    .line 1152891
    iput-boolean v6, v0, LX/6rY;->h:Z

    .line 1152892
    move-object v0, v0

    .line 1152893
    invoke-virtual {v0}, LX/6rY;->a()Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v4

    .line 1152894
    goto/16 :goto_0

    :cond_1
    move v7, v4

    .line 1152895
    goto :goto_1

    .line 1152896
    :cond_2
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 1152897
    goto :goto_2
.end method
