.class public final LX/6pW;
.super LX/6nn;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6nn",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

.field public final synthetic b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/6pZ;


# direct methods
.method public constructor <init>(LX/6pZ;Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1149466
    iput-object p1, p0, LX/6pW;->d:LX/6pZ;

    iput-object p2, p0, LX/6pW;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iput-object p3, p0, LX/6pW;->b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    iput-object p4, p0, LX/6pW;->c:Ljava/lang/String;

    invoke-direct {p0}, LX/6nn;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1149467
    sget-object v0, LX/6pZ;->a:Ljava/lang/String;

    const-string v1, "Failed to create nonce"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1149468
    iget-object v0, p0, LX/6pW;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iget-object v1, p0, LX/6pW;->b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    iget-object v2, p0, LX/6pW;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/6pZ;->f(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V

    .line 1149469
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1149470
    check-cast p1, Ljava/lang/String;

    .line 1149471
    iget-object v0, p0, LX/6pW;->d:LX/6pZ;

    iget-object v0, v0, LX/6pZ;->d:LX/6oC;

    invoke-virtual {v0, p1}, LX/6oC;->a(Ljava/lang/String;)V

    .line 1149472
    iget-object v0, p0, LX/6pW;->a:Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    iget-object v1, p0, LX/6pW;->b:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    iget-object v2, p0, LX/6pW;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/6pZ;->f(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V

    .line 1149473
    return-void
.end method
