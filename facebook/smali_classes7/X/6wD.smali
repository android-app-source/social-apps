.class public final LX/6wD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wC;


# instance fields
.field private final a:LX/712;


# direct methods
.method public constructor <init>(LX/712;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1157480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157481
    iput-object p1, p0, LX/6wD;->a:LX/712;

    .line 1157482
    return-void
.end method


# virtual methods
.method public final a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1157483
    sget-object v0, LX/6wB;->a:[I

    invoke-interface {p2}, LX/6vm;->a()LX/71I;

    move-result-object v1

    invoke-virtual {v1}, LX/71I;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1157484
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal row type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, LX/6vm;->a()LX/71I;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1157485
    :pswitch_0
    check-cast p2, LX/6w8;

    .line 1157486
    if-nez p3, :cond_0

    new-instance p3, LX/6wA;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/6wA;-><init>(Landroid/content/Context;)V

    .line 1157487
    :goto_0
    invoke-virtual {p3, p1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1157488
    invoke-virtual {p3, p2}, LX/6wA;->a(LX/6w8;)V

    .line 1157489
    move-object v0, p3

    .line 1157490
    :goto_1
    return-object v0

    .line 1157491
    :pswitch_1
    check-cast p2, LX/6vn;

    .line 1157492
    if-nez p3, :cond_1

    new-instance p3, LX/6vr;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/6vr;-><init>(Landroid/content/Context;)V

    .line 1157493
    :goto_2
    invoke-virtual {p3, p1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1157494
    invoke-virtual {p3, p2}, LX/6vr;->a(LX/6vn;)V

    .line 1157495
    move-object v0, p3

    .line 1157496
    goto :goto_1

    .line 1157497
    :pswitch_2
    iget-object v0, p0, LX/6wD;->a:LX/712;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/712;->a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 1157498
    :cond_0
    check-cast p3, LX/6wA;

    goto :goto_0

    .line 1157499
    :cond_1
    check-cast p3, LX/6vr;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
