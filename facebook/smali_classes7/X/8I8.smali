.class public final LX/8I8;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1322842
    const-class v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraCampaignMediasetQueryModel;

    const v0, 0x6f86e8fe

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "PandoraCampaignMediasetQuery"

    const-string v6, "5b88ea337f63f45d78f7aeca764908ce"

    const-string v7, "node"

    const-string v8, "10155207368766729"

    const-string v9, "10155259089746729"

    .line 1322843
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1322844
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1322845
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1322852
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1322853
    sparse-switch v0, :sswitch_data_0

    .line 1322854
    :goto_0
    return-object p1

    .line 1322855
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1322856
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1322857
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1322858
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1322859
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1322860
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1322861
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1322862
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1322863
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1322864
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1322865
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1322866
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1322867
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1322868
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1322869
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1322870
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 1322871
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 1322872
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 1322873
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 1322874
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 1322875
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 1322876
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 1322877
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 1322878
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 1322879
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 1322880
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    .line 1322881
    :sswitch_1a
    const-string p1, "26"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6e3ba572 -> :sswitch_1
        -0x6a24640d -> :sswitch_18
        -0x680de62a -> :sswitch_a
        -0x6326fdb3 -> :sswitch_6
        -0x4496acc9 -> :sswitch_b
        -0x421ba035 -> :sswitch_9
        -0x41a91745 -> :sswitch_17
        -0x3c54de38 -> :sswitch_11
        -0x3be4a271 -> :sswitch_7
        -0x3b85b241 -> :sswitch_1a
        -0x36dc9999 -> :sswitch_15
        -0x30b65c8f -> :sswitch_4
        -0x2c889631 -> :sswitch_10
        -0x2a63cba2 -> :sswitch_8
        -0x1b87b280 -> :sswitch_5
        -0x12efdeb3 -> :sswitch_c
        0x5a7510f -> :sswitch_3
        0xa1fa812 -> :sswitch_0
        0x214100e0 -> :sswitch_d
        0x2d446f86 -> :sswitch_f
        0x3052e0ff -> :sswitch_2
        0x518e5e8f -> :sswitch_14
        0x53ffd947 -> :sswitch_12
        0x617a8767 -> :sswitch_e
        0x73a026b5 -> :sswitch_13
        0x7506f93c -> :sswitch_19
        0x7e07ec78 -> :sswitch_16
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1322846
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1322847
    :goto_1
    return v0

    .line 1322848
    :sswitch_0
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "4"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "26"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 1322849
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1322850
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1322851
    :pswitch_2
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x32 -> :sswitch_0
        0x34 -> :sswitch_1
        0x644 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
