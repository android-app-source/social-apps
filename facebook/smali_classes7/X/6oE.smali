.class public final LX/6oE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;)V
    .locals 0

    .prologue
    .line 1147949
    iput-object p1, p0, LX/6oE;->a:Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 1147950
    iget-object v0, p0, LX/6oE;->a:Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;

    iget-boolean v0, v0, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->t:Z

    if-eqz v0, :cond_0

    .line 1147951
    iget-object v0, p0, LX/6oE;->a:Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;

    .line 1147952
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object p1, LX/6pF;->VERIFY:LX/6pF;

    invoke-static {p1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->a(LX/6pF;)Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->a(Landroid/content/Context;Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;)Landroid/content/Intent;

    move-result-object p0

    .line 1147953
    invoke-static {v0, p0}, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->a(Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;Landroid/content/Intent;)V

    .line 1147954
    :goto_0
    return-void

    .line 1147955
    :cond_0
    iget-object v0, p0, LX/6oE;->a:Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;

    .line 1147956
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object p1, LX/6pF;->CREATE:LX/6pF;

    invoke-static {p1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->a(LX/6pF;)Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->a(Landroid/content/Context;Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;)Landroid/content/Intent;

    move-result-object p0

    .line 1147957
    invoke-static {v0, p0}, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->a(Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;Landroid/content/Intent;)V

    .line 1147958
    goto :goto_0
.end method
