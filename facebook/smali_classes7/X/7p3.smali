.class public final LX/7p3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Lcom/facebook/graphql/enums/GraphQLEventVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Z

.field public G:Z

.field public H:Z

.field public I:Z

.field public J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Z

.field public M:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:J

.field public O:J

.field public P:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public R:I

.field public S:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public T:Z

.field public U:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public V:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:J

.field public k:J

.field public l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/enums/GraphQLEventType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1243246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;
    .locals 38

    .prologue
    .line 1243247
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1243248
    move-object/from16 v0, p0

    iget-object v3, v0, LX/7p3;->a:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    invoke-virtual {v2, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1243249
    move-object/from16 v0, p0

    iget-object v4, v0, LX/7p3;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$AdminSettingModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1243250
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7p3;->g:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-virtual {v2, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1243251
    move-object/from16 v0, p0

    iget-object v6, v0, LX/7p3;->h:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    invoke-static {v2, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1243252
    move-object/from16 v0, p0

    iget-object v7, v0, LX/7p3;->i:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$CreatedForGroupModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1243253
    move-object/from16 v0, p0

    iget-object v8, v0, LX/7p3;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventCategoryLabelModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1243254
    move-object/from16 v0, p0

    iget-object v9, v0, LX/7p3;->m:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1243255
    move-object/from16 v0, p0

    iget-object v10, v0, LX/7p3;->n:Ljava/lang/String;

    invoke-virtual {v2, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1243256
    move-object/from16 v0, p0

    iget-object v11, v0, LX/7p3;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventActorModel;

    invoke-static {v2, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1243257
    move-object/from16 v0, p0

    iget-object v12, v0, LX/7p3;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    invoke-static {v2, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1243258
    move-object/from16 v0, p0

    iget-object v13, v0, LX/7p3;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    invoke-static {v2, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1243259
    move-object/from16 v0, p0

    iget-object v14, v0, LX/7p3;->r:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$EventHostsModel;

    invoke-static {v2, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1243260
    move-object/from16 v0, p0

    iget-object v15, v0, LX/7p3;->s:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-virtual {v2, v15}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v15

    .line 1243261
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->t:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1243262
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1243263
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->v:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1243264
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->w:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 1243265
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->x:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v20

    .line 1243266
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->y:Lcom/facebook/graphql/enums/GraphQLEventType;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v21

    .line 1243267
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->z:Lcom/facebook/events/graphql/EventsGraphQLModels$EventViewerCapabilityModel;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 1243268
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->A:Lcom/facebook/graphql/enums/GraphQLEventVisibility;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v23

    .line 1243269
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->B:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 1243270
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->C:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 1243271
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->D:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1243272
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->E:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    .line 1243273
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->J:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 1243274
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->K:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel$ParentGroupModel;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 1243275
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->M:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionModel;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 1243276
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->P:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 1243277
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->Q:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    .line 1243278
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->S:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v33

    .line 1243279
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->U:LX/0Px;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v34

    .line 1243280
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->V:Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v35

    .line 1243281
    move-object/from16 v0, p0

    iget-object v0, v0, LX/7p3;->W:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v36

    .line 1243282
    const/16 v37, 0x31

    move/from16 v0, v37

    invoke-virtual {v2, v0}, LX/186;->c(I)V

    .line 1243283
    const/16 v37, 0x0

    move/from16 v0, v37

    invoke-virtual {v2, v0, v3}, LX/186;->b(II)V

    .line 1243284
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v4}, LX/186;->b(II)V

    .line 1243285
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7p3;->c:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1243286
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7p3;->d:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1243287
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7p3;->e:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1243288
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7p3;->f:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1243289
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1243290
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v6}, LX/186;->b(II)V

    .line 1243291
    const/16 v3, 0x8

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1243292
    const/16 v3, 0x9

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7p3;->j:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1243293
    const/16 v3, 0xa

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7p3;->k:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1243294
    const/16 v3, 0xb

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1243295
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1243296
    const/16 v3, 0xd

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1243297
    const/16 v3, 0xe

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 1243298
    const/16 v3, 0xf

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 1243299
    const/16 v3, 0x10

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 1243300
    const/16 v3, 0x11

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 1243301
    const/16 v3, 0x12

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 1243302
    const/16 v3, 0x13

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243303
    const/16 v3, 0x14

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243304
    const/16 v3, 0x15

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243305
    const/16 v3, 0x16

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243306
    const/16 v3, 0x17

    move/from16 v0, v20

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243307
    const/16 v3, 0x18

    move/from16 v0, v21

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243308
    const/16 v3, 0x19

    move/from16 v0, v22

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243309
    const/16 v3, 0x1a

    move/from16 v0, v23

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243310
    const/16 v3, 0x1b

    move/from16 v0, v24

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243311
    const/16 v3, 0x1c

    move/from16 v0, v25

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243312
    const/16 v3, 0x1d

    move/from16 v0, v26

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243313
    const/16 v3, 0x1e

    move/from16 v0, v27

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243314
    const/16 v3, 0x1f

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7p3;->F:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1243315
    const/16 v3, 0x20

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7p3;->G:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1243316
    const/16 v3, 0x21

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7p3;->H:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1243317
    const/16 v3, 0x22

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7p3;->I:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1243318
    const/16 v3, 0x23

    move/from16 v0, v28

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243319
    const/16 v3, 0x24

    move/from16 v0, v29

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243320
    const/16 v3, 0x25

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7p3;->L:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1243321
    const/16 v3, 0x26

    move/from16 v0, v30

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243322
    const/16 v3, 0x27

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7p3;->N:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1243323
    const/16 v3, 0x28

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/7p3;->O:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1243324
    const/16 v3, 0x29

    move/from16 v0, v31

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243325
    const/16 v3, 0x2a

    move/from16 v0, v32

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243326
    const/16 v3, 0x2b

    move-object/from16 v0, p0

    iget v4, v0, LX/7p3;->R:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1243327
    const/16 v3, 0x2c

    move/from16 v0, v33

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243328
    const/16 v3, 0x2d

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/7p3;->T:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1243329
    const/16 v3, 0x2e

    move/from16 v0, v34

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243330
    const/16 v3, 0x2f

    move/from16 v0, v35

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243331
    const/16 v3, 0x30

    move/from16 v0, v36

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1243332
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1243333
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1243334
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1243335
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1243336
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1243337
    new-instance v3, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    invoke-direct {v3, v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;-><init>(LX/15i;)V

    .line 1243338
    return-object v3
.end method
