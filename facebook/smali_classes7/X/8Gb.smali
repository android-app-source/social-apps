.class public final LX/8Gb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0xi;


# instance fields
.field public final synthetic a:LX/8Gc;


# direct methods
.method public constructor <init>(LX/8Gc;)V
    .locals 0

    .prologue
    .line 1319805
    iput-object p1, p0, LX/8Gb;->a:LX/8Gc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 4

    .prologue
    .line 1319806
    iget-object v0, p0, LX/8Gb;->a:LX/8Gc;

    iget-object v0, v0, LX/8Gc;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1319807
    if-nez v0, :cond_0

    .line 1319808
    :goto_0
    return-void

    .line 1319809
    :cond_0
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    .line 1319810
    iget-object v2, p0, LX/8Gb;->a:LX/8Gc;

    iget v2, v2, LX/8Gc;->e:F

    iget-object v3, p0, LX/8Gb;->a:LX/8Gc;

    iget v3, v3, LX/8Gc;->d:F

    invoke-static {v2, v3, v1}, LX/0yq;->a(FFF)F

    move-result v1

    .line 1319811
    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 1319812
    iget-object v0, p0, LX/8Gb;->a:LX/8Gc;

    iget-object v0, v0, LX/8Gc;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1319813
    if-nez v0, :cond_0

    .line 1319814
    :goto_0
    return-void

    .line 1319815
    :cond_0
    iget-object v1, p0, LX/8Gb;->a:LX/8Gc;

    iget v1, v1, LX/8Gc;->d:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1319816
    iget-object v0, p0, LX/8Gb;->a:LX/8Gc;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 1319817
    iput-object v1, v0, LX/8Gc;->c:LX/0am;

    .line 1319818
    invoke-virtual {p1}, LX/0wd;->a()V

    goto :goto_0
.end method

.method public final c(LX/0wd;)V
    .locals 0

    .prologue
    .line 1319819
    return-void
.end method

.method public final d(LX/0wd;)V
    .locals 0

    .prologue
    .line 1319820
    return-void
.end method
