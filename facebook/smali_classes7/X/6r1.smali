.class public final LX/6r1;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6r5;


# direct methods
.method public constructor <init>(LX/6r5;)V
    .locals 0

    .prologue
    .line 1151390
    iput-object p1, p0, LX/6r1;->a:LX/6r5;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 1

    .prologue
    .line 1151391
    iget-object v0, p0, LX/6r1;->a:LX/6r5;

    iget-object v0, v0, LX/6r5;->g:LX/6qb;

    invoke-interface {v0}, LX/6qb;->a()V

    .line 1151392
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1151393
    check-cast p1, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1151394
    iget-object v0, p0, LX/6r1;->a:LX/6r5;

    iget-object v0, v0, LX/6r5;->g:LX/6qb;

    invoke-virtual {p1}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->b()Lcom/facebook/payments/model/PaymentsPin;

    move-result-object v1

    invoke-interface {v0, v1}, LX/6qb;->a(Lcom/facebook/payments/model/PaymentsPin;)V

    .line 1151395
    return-void
.end method
