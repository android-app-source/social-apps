.class public LX/8G2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8G1;


# instance fields
.field private final a:LX/0tX;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/0ad;

.field public final d:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0ad;Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1318438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318439
    iput-object p1, p0, LX/8G2;->a:LX/0tX;

    .line 1318440
    iput-object p2, p0, LX/8G2;->b:Ljava/util/concurrent/ExecutorService;

    .line 1318441
    iput-object p3, p0, LX/8G2;->c:LX/0ad;

    .line 1318442
    iput-object p4, p0, LX/8G2;->d:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    .line 1318443
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1318444
    iget-object v0, p0, LX/8G2;->c:LX/0ad;

    sget-short v1, LX/8Fn;->d:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1318445
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1318446
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1318447
    :goto_0
    return-object v0

    .line 1318448
    :cond_0
    new-instance v0, LX/5iH;

    invoke-direct {v0}, LX/5iH;-><init>()V

    move-object v0, v0

    .line 1318449
    const-string v1, "frame_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1318450
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0xe10

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 1318451
    iget-object v1, p0, LX/8G2;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/8G0;

    invoke-direct {v1, p0}, LX/8G0;-><init>(LX/8G2;)V

    iget-object v2, p0, LX/8G2;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
