.class public final LX/8Xt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/views/loading/ProgressTextView;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/loading/ProgressTextView;)V
    .locals 0

    .prologue
    .line 1355637
    iput-object p1, p0, LX/8Xt;->a:Lcom/facebook/quicksilver/views/loading/ProgressTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6

    .prologue
    .line 1355638
    iget-object v1, p0, LX/8Xt;->a:Lcom/facebook/quicksilver/views/loading/ProgressTextView;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v1, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->b:I

    .line 1355639
    iget-object v0, p0, LX/8Xt;->a:Lcom/facebook/quicksilver/views/loading/ProgressTextView;

    iget-object v1, p0, LX/8Xt;->a:Lcom/facebook/quicksilver/views/loading/ProgressTextView;

    invoke-virtual {v1}, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082366

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/8Xt;->a:Lcom/facebook/quicksilver/views/loading/ProgressTextView;

    iget v5, v5, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/quicksilver/views/loading/ProgressTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1355640
    return-void
.end method
