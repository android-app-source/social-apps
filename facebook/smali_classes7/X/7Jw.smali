.class public LX/7Jw;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;

.field private c:LX/1m0;

.field private d:LX/0Sh;

.field private e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0wq;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0ad;

.field private final g:LX/0wp;

.field private final h:LX/0ka;

.field private final i:LX/04m;

.field private final j:LX/0oz;

.field private final k:LX/0TD;

.field private final l:LX/0YR;

.field private final m:LX/1AA;

.field private final n:LX/19j;

.field private final o:LX/0wq;

.field private final p:LX/19m;

.field private final q:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1194242
    const-class v0, LX/7Jw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7Jw;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1m0;LX/0Sh;LX/0Or;LX/0wp;LX/0TD;LX/0ka;LX/0oz;LX/0ad;LX/0YR;LX/1AA;LX/19j;LX/0wq;LX/19m;LX/03V;)V
    .locals 2
    .param p6    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/VideoPerformanceExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1m0;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Or",
            "<",
            "LX/0wq;",
            ">;",
            "LX/0wp;",
            "LX/0TD;",
            "LX/0ka;",
            "LX/0oz;",
            "LX/0ad;",
            "LX/0YR;",
            "LX/1AA;",
            "LX/19j;",
            "LX/0wq;",
            "LX/19m;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1194243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194244
    iput-object p1, p0, LX/7Jw;->b:Landroid/content/Context;

    .line 1194245
    iput-object p2, p0, LX/7Jw;->c:LX/1m0;

    .line 1194246
    iput-object p3, p0, LX/7Jw;->d:LX/0Sh;

    .line 1194247
    iput-object p4, p0, LX/7Jw;->e:LX/0Or;

    .line 1194248
    iput-object p5, p0, LX/7Jw;->g:LX/0wp;

    .line 1194249
    iput-object p6, p0, LX/7Jw;->k:LX/0TD;

    .line 1194250
    iput-object p7, p0, LX/7Jw;->h:LX/0ka;

    .line 1194251
    invoke-virtual {p2}, LX/1m0;->c()LX/04m;

    move-result-object v1

    iput-object v1, p0, LX/7Jw;->i:LX/04m;

    .line 1194252
    iput-object p8, p0, LX/7Jw;->j:LX/0oz;

    .line 1194253
    iput-object p9, p0, LX/7Jw;->f:LX/0ad;

    .line 1194254
    iput-object p10, p0, LX/7Jw;->l:LX/0YR;

    .line 1194255
    iput-object p11, p0, LX/7Jw;->m:LX/1AA;

    .line 1194256
    iput-object p12, p0, LX/7Jw;->n:LX/19j;

    .line 1194257
    iput-object p13, p0, LX/7Jw;->o:LX/0wq;

    .line 1194258
    move-object/from16 v0, p14

    iput-object v0, p0, LX/7Jw;->p:LX/19m;

    .line 1194259
    move-object/from16 v0, p15

    iput-object v0, p0, LX/7Jw;->q:LX/03V;

    .line 1194260
    return-void
.end method

.method public static b(LX/0QB;)LX/7Jw;
    .locals 17

    .prologue
    .line 1194261
    new-instance v1, LX/7Jw;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/1m0;->a(LX/0QB;)LX/1m0;

    move-result-object v3

    check-cast v3, LX/1m0;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    const/16 v5, 0x12e5

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/0wp;->a(LX/0QB;)LX/0wp;

    move-result-object v6

    check-cast v6, LX/0wp;

    invoke-static/range {p0 .. p0}, LX/19U;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v8

    check-cast v8, LX/0ka;

    invoke-static/range {p0 .. p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v9

    check-cast v9, LX/0oz;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0YQ;->a(LX/0QB;)LX/0YQ;

    move-result-object v11

    check-cast v11, LX/0YR;

    invoke-static/range {p0 .. p0}, LX/1A9;->a(LX/0QB;)LX/1AA;

    move-result-object v12

    check-cast v12, LX/1AA;

    invoke-static/range {p0 .. p0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v13

    check-cast v13, LX/19j;

    invoke-static/range {p0 .. p0}, LX/0wq;->a(LX/0QB;)LX/0wq;

    move-result-object v14

    check-cast v14, LX/0wq;

    invoke-static/range {p0 .. p0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object v15

    check-cast v15, LX/19m;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v16

    check-cast v16, LX/03V;

    invoke-direct/range {v1 .. v16}, LX/7Jw;-><init>(Landroid/content/Context;LX/1m0;LX/0Sh;LX/0Or;LX/0wp;LX/0TD;LX/0ka;LX/0oz;LX/0ad;LX/0YR;LX/1AA;LX/19j;LX/0wq;LX/19m;LX/03V;)V

    .line 1194262
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;LX/1m6;LX/0Jy;LX/0KZ;Z)LX/0Ji;
    .locals 29

    .prologue
    .line 1194263
    invoke-static/range {p1 .. p1}, LX/1m0;->d(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 1194264
    if-nez v1, :cond_0

    move-object/from16 v1, p1

    .line 1194265
    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 1194266
    new-instance v6, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v6, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1194267
    if-eqz p2, :cond_1

    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, ".mpd"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    invoke-static/range {p3 .. p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_2
    const/4 v1, 0x1

    .line 1194268
    :goto_0
    const-string v3, ".m3u8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, ".m3u"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_3
    const/4 v2, 0x1

    .line 1194269
    :goto_1
    if-eqz v2, :cond_a

    .line 1194270
    if-eqz p2, :cond_7

    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".mpd"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1194271
    new-instance v1, LX/0K0;

    invoke-virtual/range {p4 .. p4}, LX/1m6;->b()LX/04G;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Jw;->n:LX/19j;

    iget-boolean v2, v2, LX/19j;->az:Z

    if-eqz v2, :cond_6

    invoke-virtual/range {p4 .. p4}, LX/1m6;->e()Ljava/lang/String;

    move-result-object v4

    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, LX/7Jw;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Jw;->n:LX/19j;

    iget-object v2, v2, LX/19j;->R:Ljava/util/Map;

    sget-object v7, LX/0p3;->UNKNOWN:LX/0p3;

    invoke-interface {v2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Jw;->n:LX/19j;

    iget-object v2, v2, LX/19j;->c:LX/19k;

    invoke-virtual {v2}, LX/19k;->a()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v12, v0, LX/7Jw;->n:LX/19j;

    move-object/from16 v2, p2

    move-object/from16 v7, p5

    move-object/from16 v8, p5

    move-object/from16 v11, p3

    move-object/from16 v13, p6

    invoke-direct/range {v1 .. v13}, LX/0K0;-><init>(Landroid/net/Uri;LX/04G;Ljava/lang/String;Landroid/content/Context;Landroid/os/Handler;LX/0Jv;LX/0Js;IILjava/lang/String;LX/19j;LX/0KZ;)V

    move-object v3, v1

    .line 1194272
    :goto_3
    return-object v3

    .line 1194273
    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    .line 1194274
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    .line 1194275
    :cond_6
    const-string v4, ""

    goto :goto_2

    .line 1194276
    :cond_7
    if-nez p2, :cond_9

    .line 1194277
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Jw;->q:LX/03V;

    sget-object v2, LX/7Jw;->a:Ljava/lang/String;

    const-string v3, "preferredVideoUri is null"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1194278
    :cond_8
    :goto_4
    new-instance v3, LX/0Jl;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7Jw;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Jw;->f:LX/0ad;

    sget-short v2, LX/0ws;->dt:S

    const/4 v4, 0x1

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, LX/7Jw;->c:LX/1m0;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Jw;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wq;

    iget v11, v1, LX/0wq;->i:I

    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Jw;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wq;

    iget v12, v1, LX/0wq;->j:I

    move-object/from16 v4, p1

    move-object/from16 v9, p5

    move-object/from16 v10, p5

    invoke-direct/range {v3 .. v12}, LX/0Jl;-><init>(Landroid/net/Uri;Landroid/content/Context;Landroid/os/Handler;ZLX/1m0;LX/0Jv;LX/0Js;II)V

    goto :goto_3

    .line 1194279
    :cond_9
    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".mpd"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1194280
    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Jw;->q:LX/03V;

    sget-object v2, LX/7Jw;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "last segment is"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 1194281
    :cond_a
    if-eqz v1, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Jw;->g:LX/0wp;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Jw;->h:LX/0ka;

    invoke-virtual {v1, v2}, LX/0wp;->a(LX/0ka;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1194282
    if-eqz p7, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Jw;->g:LX/0wp;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/7Jw;->h:LX/0ka;

    invoke-virtual/range {p4 .. p4}, LX/1m6;->f()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/0wp;->a(LX/0ka;Z)I

    move-result v1

    if-ltz v1, :cond_b

    const/16 v21, 0x1

    .line 1194283
    :goto_5
    new-instance v7, LX/0Jj;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/7Jw;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Jw;->d:LX/0Sh;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Jw;->c:LX/1m0;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Jw;->g:LX/0wp;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Jw;->i:LX/04m;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Jw;->h:LX/0ka;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Jw;->j:LX/0oz;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Jw;->k:LX/0TD;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Jw;->l:LX/0YR;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Jw;->m:LX/1AA;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Jw;->o:LX/0wq;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/7Jw;->p:LX/19m;

    move-object/from16 v28, v0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move-object v13, v6

    move-object/from16 v14, p5

    move-object/from16 v15, p5

    move-object/from16 v19, p6

    invoke-direct/range {v7 .. v28}, LX/0Jj;-><init>(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;LX/1m6;Landroid/content/Context;Landroid/os/Handler;LX/0Jv;LX/0Js;LX/0Sh;LX/1m0;LX/0wp;LX/0KZ;LX/04m;ZLX/0ka;LX/0oz;LX/0TD;LX/0YR;LX/1AA;LX/0wq;LX/19m;)V

    move-object v3, v7

    goto/16 :goto_3

    .line 1194284
    :cond_b
    const/16 v21, 0x0

    goto :goto_5

    .line 1194285
    :cond_c
    new-instance v3, LX/0Jo;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/7Jw;->b:Landroid/content/Context;

    new-instance v9, LX/16V;

    invoke-direct {v9}, LX/16V;-><init>()V

    move-object/from16 v0, p0

    iget-object v10, v0, LX/7Jw;->d:LX/0Sh;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/7Jw;->c:LX/1m0;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Jw;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wq;

    iget-object v12, v1, LX/0wq;->b:LX/0wr;

    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Jw;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wq;

    iget v13, v1, LX/0wq;->k:I

    move-object/from16 v0, p0

    iget-object v1, v0, LX/7Jw;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0wq;

    iget v14, v1, LX/0wq;->l:I

    move-object/from16 v4, p1

    move-object/from16 v7, p5

    move-object/from16 v8, p5

    invoke-direct/range {v3 .. v14}, LX/0Jo;-><init>(Landroid/net/Uri;Landroid/content/Context;Landroid/os/Handler;LX/0Jv;LX/0Js;LX/16V;LX/0Sh;LX/1m0;LX/0wr;II)V

    goto/16 :goto_3
.end method
