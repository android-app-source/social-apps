.class public interface abstract LX/6nA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/inject/Bindings;
.end annotation


# virtual methods
.method public abstract addBugReportExtraFileMapProviders(LX/6n7;)LX/1MS;
    .annotation build Lcom/facebook/inject/MultiBind;
    .end annotation
.end method

.method public abstract addBugReportFileProviders(LX/6n7;)LX/1MT;
    .annotation build Lcom/facebook/inject/MultiBind;
    .end annotation
.end method

.method public abstract addIHaveUserData(LX/2Pk;)LX/0c5;
    .annotation build Lcom/facebook/inject/MultiBind;
    .end annotation
.end method

.method public abstract defaultMqttProtocolProvider(Lcom/facebook/omnistore/MqttProtocolProvider;)Lcom/facebook/omnistore/MqttProtocolProvider;
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/omnistore/module/OverrideMqttProtocolProvider;
    .end annotation
.end method

.method public abstract provideBugReportBackgroundDataProvider(LX/2Pt;)LX/1MT;
    .annotation build Lcom/facebook/inject/MultiBind;
    .end annotation
.end method

.method public abstract provideBugReportExtraDataMapProvider(LX/2Pt;)LX/1MS;
    .annotation build Lcom/facebook/inject/MultiBind;
    .end annotation
.end method

.method public abstract provideOmnistoreErrorReporter(LX/2Pq;)Lcom/facebook/omnistore/OmnistoreErrorReporter;
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation
.end method

.method public abstract provideOmnistoreOpener(LX/2Pl;)LX/2Pl;
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation
.end method
