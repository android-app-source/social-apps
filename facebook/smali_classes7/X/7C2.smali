.class public LX/7C2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:D

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7C1;)V
    .locals 4

    .prologue
    .line 1179812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1179813
    iget-object v0, p1, LX/7C1;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1179814
    iput-object v0, p0, LX/7C2;->a:Ljava/lang/String;

    .line 1179815
    iget-object v0, p1, LX/7C1;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1179816
    iput-object v0, p0, LX/7C2;->b:Ljava/lang/String;

    .line 1179817
    iget-object v0, p1, LX/7C1;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1179818
    iput-object v0, p0, LX/7C2;->c:Ljava/lang/String;

    .line 1179819
    iget-object v0, p1, LX/7C1;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1179820
    iput-object v0, p0, LX/7C2;->d:Ljava/lang/String;

    .line 1179821
    iget-object v0, p1, LX/7C1;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1179822
    iput-object v0, p0, LX/7C2;->e:Ljava/lang/String;

    .line 1179823
    iget-object v0, p1, LX/7C1;->f:LX/0Px;

    move-object v0, v0

    .line 1179824
    iput-object v0, p0, LX/7C2;->f:LX/0Px;

    .line 1179825
    iget-wide v2, p1, LX/7C1;->g:D

    move-wide v0, v2

    .line 1179826
    iput-wide v0, p0, LX/7C2;->g:D

    .line 1179827
    iget-object v0, p1, LX/7C1;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1179828
    iput-object v0, p0, LX/7C2;->h:Ljava/lang/String;

    .line 1179829
    return-void
.end method

.method public static i(LX/7C2;)V
    .locals 6

    .prologue
    .line 1179793
    iget-object v0, p0, LX/7C2;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1179794
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1179795
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_BOOTSTRAP_SUGGESTION:LX/3Ql;

    const-string v2, "Missing name for keyword suggestion"

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1179796
    :cond_0
    iget-object v0, p0, LX/7C2;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1179797
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1179798
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_BOOTSTRAP_SUGGESTION:LX/3Ql;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing type for keyword suggestion "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1179799
    iget-object v3, p0, LX/7C2;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1179800
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1179801
    :cond_1
    iget-wide v4, p0, LX/7C2;->g:D

    move-wide v0, v4

    .line 1179802
    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2

    .line 1179803
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_BOOTSTRAP_SUGGESTION:LX/3Ql;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing costs for keyword suggestion "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1179804
    iget-object v3, p0, LX/7C2;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1179805
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1179806
    :cond_2
    iget-object v0, p0, LX/7C2;->f:LX/0Px;

    move-object v0, v0

    .line 1179807
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1179808
    new-instance v0, LX/7C4;

    sget-object v1, LX/3Ql;->BAD_BOOTSTRAP_SUGGESTION:LX/3Ql;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing or empty name search tokens for keyword suggestion "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1179809
    iget-object v3, p0, LX/7C2;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1179810
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/String;)V

    throw v0

    .line 1179811
    :cond_3
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1179790
    instance-of v0, p1, LX/7C2;

    if-nez v0, :cond_0

    .line 1179791
    const/4 v0, 0x0

    .line 1179792
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/7C2;->a:Ljava/lang/String;

    check-cast p1, LX/7C2;

    iget-object v1, p1, LX/7C2;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1179786
    iget-object v0, p0, LX/7C2;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1179787
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BootstrapKeyword["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1179788
    iget-object v1, p0, LX/7C2;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1179789
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
