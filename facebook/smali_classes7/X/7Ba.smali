.class public LX/7Ba;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7BW;


# instance fields
.field private final a:LX/7BO;

.field private final b:LX/7Bb;

.field private final c:LX/0Uh;


# direct methods
.method public constructor <init>(LX/7BO;LX/7Bb;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1179079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1179080
    iput-object p1, p0, LX/7Ba;->a:LX/7BO;

    .line 1179081
    iput-object p2, p0, LX/7Ba;->b:LX/7Bb;

    .line 1179082
    iput-object p3, p0, LX/7Ba;->c:LX/0Uh;

    .line 1179083
    return-void
.end method

.method public static b(LX/0QB;)LX/7Ba;
    .locals 4

    .prologue
    .line 1179084
    new-instance v3, LX/7Ba;

    invoke-static {p0}, LX/7BO;->a(LX/0QB;)LX/7BO;

    move-result-object v0

    check-cast v0, LX/7BO;

    invoke-static {p0}, LX/7Bb;->a(LX/0QB;)LX/7Bb;

    move-result-object v1

    check-cast v1, LX/7Bb;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-direct {v3, v0, v1, v2}, LX/7Ba;-><init>(LX/7BO;LX/7Bb;LX/0Uh;)V

    .line 1179085
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 1179086
    check-cast p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1179087
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1179088
    iget-object v3, p0, LX/7Ba;->b:LX/7Bb;

    invoke-virtual {v3, p1, v2}, LX/7Bb;->a(Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;Ljava/util/List;)V

    .line 1179089
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "query"

    iget-object v5, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    .line 1179090
    iget-object v6, v5, LX/7B6;->b:Ljava/lang/String;

    move-object v5, v6

    .line 1179091
    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179092
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "cached_ids"

    iget-object v5, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->k:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179093
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "support_groups_icons"

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179094
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "group_icon_scale"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179095
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "include_is_verified"

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179096
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "no_profile_image_urls"

    iget-boolean v5, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->l:Z

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179097
    iget-object v3, p0, LX/7Ba;->c:LX/0Uh;

    sget v4, LX/2SU;->G:I

    invoke-virtual {v3, v4, v0}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, LX/7Ba;->c:LX/0Uh;

    sget v4, LX/2SU;->H:I

    invoke-virtual {v3, v4, v0}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v0, v1

    .line 1179098
    :cond_1
    iget-object v1, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->m:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "simplesearch_typeahead"

    .line 1179099
    :goto_0
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v3

    .line 1179100
    iput-object v1, v3, LX/14O;->b:Ljava/lang/String;

    .line 1179101
    move-object v1, v3

    .line 1179102
    const-string v3, "GET"

    .line 1179103
    iput-object v3, v1, LX/14O;->c:Ljava/lang/String;

    .line 1179104
    move-object v1, v1

    .line 1179105
    const-string v3, "method/ubersearch.get"

    .line 1179106
    iput-object v3, v1, LX/14O;->d:Ljava/lang/String;

    .line 1179107
    move-object v1, v1

    .line 1179108
    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v1, v3}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v1

    .line 1179109
    iput-object v2, v1, LX/14O;->g:Ljava/util/List;

    .line 1179110
    move-object v1, v1

    .line 1179111
    sget-object v2, LX/14S;->JSONPARSER:LX/14S;

    .line 1179112
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1179113
    move-object v2, v1

    .line 1179114
    if-eqz v0, :cond_3

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v3, "X-FB-ForkingType"

    const-string v4, "edge-sgp-search"

    invoke-direct {v1, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    :goto_1
    invoke-virtual {v2, v1}, LX/14O;->a(LX/0Px;)LX/14O;

    move-result-object v1

    .line 1179115
    iput-boolean v0, v1, LX/14O;->B:Z

    .line 1179116
    move-object v0, v1

    .line 1179117
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 1179118
    :cond_2
    iget-object v1, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->m:Ljava/lang/String;

    goto :goto_0

    .line 1179119
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1179120
    const-string v0, "FetchUberbarResultMethod.getResponse"

    const v1, 0x23996b2f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1179121
    :try_start_0
    new-instance v0, LX/7Hc;

    iget-object v1, p0, LX/7Ba;->a:LX/7BO;

    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/7BO;->a(LX/15w;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7Hc;-><init>(LX/0Px;)V

    .line 1179122
    invoke-static {p2}, LX/7Hc;->a(LX/1pN;)I

    move-result v1

    .line 1179123
    iput v1, v0, LX/7Hc;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1179124
    const v1, 0x2cea3df5

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x747b3124

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
