.class public LX/6jl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final payload:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1132544
    new-instance v0, LX/1sv;

    const-string v1, "DeltaClientPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6jl;->b:LX/1sv;

    .line 1132545
    new-instance v0, LX/1sw;

    const-string v1, "payload"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6jl;->c:LX/1sw;

    .line 1132546
    sput-boolean v3, LX/6jl;->a:Z

    return-void
.end method

.method public constructor <init>([B)V
    .locals 0

    .prologue
    .line 1132541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1132542
    iput-object p1, p0, LX/6jl;->payload:[B

    .line 1132543
    return-void
.end method

.method public static a(LX/6jl;)V
    .locals 4

    .prologue
    .line 1132538
    iget-object v0, p0, LX/6jl;->payload:[B

    if-nez v0, :cond_0

    .line 1132539
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'payload\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6jl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1132540
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0x80

    .line 1132547
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1132548
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v2, v0

    .line 1132549
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1132550
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "DeltaClientPayload"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1132551
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132552
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132553
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132554
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132555
    const-string v1, "payload"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132556
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132557
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132558
    iget-object v0, p0, LX/6jl;->payload:[B

    if-nez v0, :cond_4

    .line 1132559
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132560
    :cond_0
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132561
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132562
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1132563
    :cond_1
    const-string v0, ""

    move-object v3, v0

    goto :goto_0

    .line 1132564
    :cond_2
    const-string v0, ""

    move-object v2, v0

    goto :goto_1

    .line 1132565
    :cond_3
    const-string v0, ""

    goto :goto_2

    .line 1132566
    :cond_4
    iget-object v0, p0, LX/6jl;->payload:[B

    array-length v0, v0

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 1132567
    const/4 v0, 0x0

    move v1, v0

    :goto_4
    if-ge v1, v5, :cond_7

    .line 1132568
    if-eqz v1, :cond_5

    const-string v0, " "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132569
    :cond_5
    iget-object v0, p0, LX/6jl;->payload:[B

    aget-byte v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v6, 0x1

    if-le v0, v6, :cond_6

    iget-object v0, p0, LX/6jl;->payload:[B

    aget-byte v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, LX/6jl;->payload:[B

    aget-byte v6, v6, v1

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132570
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1132571
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "0"

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, LX/6jl;->payload:[B

    aget-byte v6, v6, v1

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1132572
    :cond_7
    iget-object v0, p0, LX/6jl;->payload:[B

    array-length v0, v0

    if-le v0, v7, :cond_0

    const-string v0, " ..."

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1132530
    invoke-static {p0}, LX/6jl;->a(LX/6jl;)V

    .line 1132531
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1132532
    iget-object v0, p0, LX/6jl;->payload:[B

    if-eqz v0, :cond_0

    .line 1132533
    sget-object v0, LX/6jl;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1132534
    iget-object v0, p0, LX/6jl;->payload:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 1132535
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1132536
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1132537
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1132515
    if-nez p1, :cond_1

    .line 1132516
    :cond_0
    :goto_0
    return v0

    .line 1132517
    :cond_1
    instance-of v1, p1, LX/6jl;

    if-eqz v1, :cond_0

    .line 1132518
    check-cast p1, LX/6jl;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1132519
    if-nez p1, :cond_3

    .line 1132520
    :cond_2
    :goto_1
    move v0, v2

    .line 1132521
    goto :goto_0

    .line 1132522
    :cond_3
    iget-object v0, p0, LX/6jl;->payload:[B

    if-eqz v0, :cond_6

    move v0, v1

    .line 1132523
    :goto_2
    iget-object v3, p1, LX/6jl;->payload:[B

    if-eqz v3, :cond_7

    move v3, v1

    .line 1132524
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1132525
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1132526
    iget-object v0, p0, LX/6jl;->payload:[B

    iget-object v3, p1, LX/6jl;->payload:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 1132527
    goto :goto_1

    :cond_6
    move v0, v2

    .line 1132528
    goto :goto_2

    :cond_7
    move v3, v2

    .line 1132529
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1132511
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1132512
    sget-boolean v0, LX/6jl;->a:Z

    .line 1132513
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6jl;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1132514
    return-object v0
.end method
