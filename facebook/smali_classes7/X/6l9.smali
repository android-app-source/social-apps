.class public LX/6l9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final otherUserFbId:Ljava/lang/Long;

.field public final threadFbId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v3, 0x1

    .line 1142274
    new-instance v0, LX/1sv;

    const-string v1, "ThreadKey"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6l9;->b:LX/1sv;

    .line 1142275
    new-instance v0, LX/1sw;

    const-string v1, "otherUserFbId"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6l9;->c:LX/1sw;

    .line 1142276
    new-instance v0, LX/1sw;

    const-string v1, "threadFbId"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6l9;->d:LX/1sw;

    .line 1142277
    sput-boolean v3, LX/6l9;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1142270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1142271
    iput-object p1, p0, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    .line 1142272
    iput-object p2, p0, LX/6l9;->threadFbId:Ljava/lang/Long;

    .line 1142273
    return-void
.end method

.method public static b(LX/1su;)LX/6l9;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/16 v4, 0xa

    .line 1142256
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v1, v0

    .line 1142257
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 1142258
    iget-byte v3, v2, LX/1sw;->b:B

    if-eqz v3, :cond_2

    .line 1142259
    iget-short v3, v2, LX/1sw;->c:S

    packed-switch v3, :pswitch_data_0

    .line 1142260
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1142261
    :pswitch_0
    iget-byte v3, v2, LX/1sw;->b:B

    if-ne v3, v4, :cond_0

    .line 1142262
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 1142263
    :cond_0
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1142264
    :pswitch_1
    iget-byte v3, v2, LX/1sw;->b:B

    if-ne v3, v4, :cond_1

    .line 1142265
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 1142266
    :cond_1
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1142267
    :cond_2
    invoke-virtual {p0}, LX/1su;->e()V

    .line 1142268
    new-instance v2, LX/6l9;

    invoke-direct {v2, v1, v0}, LX/6l9;-><init>(Ljava/lang/Long;Ljava/lang/Long;)V

    .line 1142269
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1142224
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1142225
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1142226
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1142227
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "ThreadKey"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1142228
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142229
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142230
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142231
    const/4 v1, 0x1

    .line 1142232
    iget-object v5, p0, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    if-eqz v5, :cond_0

    .line 1142233
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142234
    const-string v1, "otherUserFbId"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142235
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142236
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142237
    iget-object v1, p0, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    if-nez v1, :cond_6

    .line 1142238
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142239
    :goto_3
    const/4 v1, 0x0

    .line 1142240
    :cond_0
    iget-object v5, p0, LX/6l9;->threadFbId:Ljava/lang/Long;

    if-eqz v5, :cond_2

    .line 1142241
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142242
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142243
    const-string v1, "threadFbId"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142244
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142245
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142246
    iget-object v0, p0, LX/6l9;->threadFbId:Ljava/lang/Long;

    if-nez v0, :cond_7

    .line 1142247
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142248
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142249
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142250
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1142251
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1142252
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1142253
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1142254
    :cond_6
    iget-object v1, p0, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1142255
    :cond_7
    iget-object v0, p0, LX/6l9;->threadFbId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 1142278
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1142279
    iget-object v0, p0, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1142280
    iget-object v0, p0, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1142281
    sget-object v0, LX/6l9;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142282
    iget-object v0, p0, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1142283
    :cond_0
    iget-object v0, p0, LX/6l9;->threadFbId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1142284
    iget-object v0, p0, LX/6l9;->threadFbId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1142285
    sget-object v0, LX/6l9;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1142286
    iget-object v0, p0, LX/6l9;->threadFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1142287
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1142288
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1142289
    return-void
.end method

.method public final a(LX/6l9;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1142207
    if-nez p1, :cond_1

    .line 1142208
    :cond_0
    :goto_0
    return v2

    .line 1142209
    :cond_1
    iget-object v0, p0, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    if-eqz v0, :cond_6

    move v0, v1

    .line 1142210
    :goto_1
    iget-object v3, p1, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    if-eqz v3, :cond_7

    move v3, v1

    .line 1142211
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 1142212
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1142213
    iget-object v0, p0, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/6l9;->otherUserFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1142214
    :cond_3
    iget-object v0, p0, LX/6l9;->threadFbId:Ljava/lang/Long;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1142215
    :goto_3
    iget-object v3, p1, LX/6l9;->threadFbId:Ljava/lang/Long;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1142216
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1142217
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1142218
    iget-object v0, p0, LX/6l9;->threadFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/6l9;->threadFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_5
    move v2, v1

    .line 1142219
    goto :goto_0

    :cond_6
    move v0, v2

    .line 1142220
    goto :goto_1

    :cond_7
    move v3, v2

    .line 1142221
    goto :goto_2

    :cond_8
    move v0, v2

    .line 1142222
    goto :goto_3

    :cond_9
    move v3, v2

    .line 1142223
    goto :goto_4
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1142203
    if-nez p1, :cond_1

    .line 1142204
    :cond_0
    :goto_0
    return v0

    .line 1142205
    :cond_1
    instance-of v1, p1, LX/6l9;

    if-eqz v1, :cond_0

    .line 1142206
    check-cast p1, LX/6l9;

    invoke-virtual {p0, p1}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1142202
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1142199
    sget-boolean v0, LX/6l9;->a:Z

    .line 1142200
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6l9;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1142201
    return-object v0
.end method
