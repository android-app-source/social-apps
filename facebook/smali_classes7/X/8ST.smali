.class public LX/8ST;
.super LX/622;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/622",
        "<",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1346693
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/622;-><init>(Ljava/lang/String;)V

    .line 1346694
    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/8ST;->a:Ljava/util/List;

    .line 1346695
    invoke-static {p2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/8ST;->b:Ljava/util/List;

    .line 1346696
    return-void
.end method


# virtual methods
.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1346697
    iget-object v0, p0, LX/8ST;->b:Ljava/util/List;

    return-object v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1346698
    iget-object v0, p0, LX/8ST;->a:Ljava/util/List;

    return-object v0
.end method
