.class public final LX/7G9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/7GT;


# direct methods
.method private constructor <init>(Ljava/lang/String;LX/7GT;)V
    .locals 1

    .prologue
    .line 1188839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1188840
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/7G9;->a:Ljava/lang/String;

    .line 1188841
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7GT;

    iput-object v0, p0, LX/7G9;->b:LX/7GT;

    .line 1188842
    return-void
.end method

.method public static a(Ljava/lang/String;LX/7GT;)LX/7G9;
    .locals 2

    .prologue
    .line 1188843
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1188844
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "queueEntityId cannot be null nor empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1188845
    :cond_0
    if-nez p1, :cond_1

    .line 1188846
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "queueType cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1188847
    :cond_1
    new-instance v0, LX/7G9;

    invoke-direct {v0, p0, p1}, LX/7G9;-><init>(Ljava/lang/String;LX/7GT;)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1188848
    if-ne p0, p1, :cond_1

    .line 1188849
    :cond_0
    :goto_0
    return v0

    .line 1188850
    :cond_1
    instance-of v2, p1, LX/7G9;

    if-nez v2, :cond_2

    move v0, v1

    .line 1188851
    goto :goto_0

    .line 1188852
    :cond_2
    check-cast p1, LX/7G9;

    .line 1188853
    iget-object v2, p0, LX/7G9;->a:Ljava/lang/String;

    iget-object v3, p1, LX/7G9;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/7G9;->b:LX/7GT;

    iget-object v3, p1, LX/7G9;->b:LX/7GT;

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1188854
    iget-object v0, p0, LX/7G9;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1188855
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/7G9;->b:LX/7GT;

    invoke-virtual {v1}, LX/7GT;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1188856
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1188857
    const-string v0, "id:%s, type:%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/7G9;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LX/7G9;->b:LX/7GT;

    invoke-virtual {v3}, LX/7GT;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
