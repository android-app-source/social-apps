.class public LX/6mF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/view/GestureDetector;

.field public b:LX/6m1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1143436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1143437
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, LX/6mE;

    invoke-direct {v1, p0}, LX/6mE;-><init>(LX/6mF;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/6mF;->a:Landroid/view/GestureDetector;

    .line 1143438
    return-void
.end method

.method public static b(LX/0QB;)LX/6mF;
    .locals 2

    .prologue
    .line 1143446
    new-instance v1, LX/6mF;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/6mF;-><init>(Landroid/content/Context;)V

    .line 1143447
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1143442
    iget-object v1, p0, LX/6mF;->a:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1143443
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 1143444
    iput-boolean v0, p0, LX/6mF;->d:Z

    .line 1143445
    :cond_0
    iget-object v1, p0, LX/6mF;->b:LX/6m1;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, LX/6mF;->c:Z

    if-eqz v1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 1143439
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 1143440
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6mF;->d:Z

    .line 1143441
    :cond_0
    return-void
.end method
