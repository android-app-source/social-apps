.class public LX/8Jf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TAGVIEW:",
        "Landroid/view/View;",
        ":",
        "LX/8Jj;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:LX/8JG;


# instance fields
.field public b:LX/8JG;

.field public c:Landroid/graphics/PointF;

.field public final d:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTAGVIEW;"
        }
    .end annotation
.end field

.field private final e:LX/8Jk;

.field public f:LX/8Jh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1329013
    sget-object v0, LX/8JG;->UP:LX/8JG;

    sput-object v0, LX/8Jf;->a:LX/8JG;

    return-void
.end method

.method public constructor <init>(LX/8Jj;Ljava/lang/Integer;LX/8Jk;)V
    .locals 2
    .param p1    # LX/8Jj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/8Jk;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1329005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1329006
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, LX/8Jf;->c:Landroid/graphics/PointF;

    .line 1329007
    new-instance v0, LX/8Jh;

    invoke-direct {v0}, LX/8Jh;-><init>()V

    iput-object v0, p0, LX/8Jf;->f:LX/8Jh;

    .line 1329008
    check-cast p1, Landroid/view/View;

    iput-object p1, p0, LX/8Jf;->d:Landroid/view/View;

    .line 1329009
    iput-object p3, p0, LX/8Jf;->e:LX/8Jk;

    .line 1329010
    iget-object v0, p0, LX/8Jf;->c:Landroid/graphics/PointF;

    iget-object v1, p0, LX/8Jf;->e:LX/8Jk;

    iget-object v1, v1, LX/8Jk;->a:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 1329011
    sget-object v0, LX/8Jf;->a:LX/8JG;

    invoke-virtual {p0, v0}, LX/8Jf;->a(LX/8JG;)V

    .line 1329012
    return-void
.end method


# virtual methods
.method public final a()LX/8JG;
    .locals 1

    .prologue
    .line 1329004
    iget-object v0, p0, LX/8Jf;->b:LX/8JG;

    return-object v0
.end method

.method public final a(LX/8JG;)V
    .locals 3

    .prologue
    .line 1328989
    iput-object p1, p0, LX/8Jf;->b:LX/8JG;

    .line 1328990
    iget-object v0, p0, LX/8Jf;->e:LX/8Jk;

    iget-object v0, v0, LX/8Jk;->b:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    .line 1328991
    iget-object v0, p0, LX/8Jf;->e:LX/8Jk;

    iget-object v0, v0, LX/8Jk;->b:Landroid/graphics/RectF;

    iget-object v1, p0, LX/8Jf;->c:Landroid/graphics/PointF;

    .line 1328992
    sget-object v2, LX/8Je;->a:[I

    invoke-virtual {p1}, LX/8JG;->ordinal()I

    move-result p0

    aget v2, v2, p0

    packed-switch v2, :pswitch_data_0

    .line 1328993
    :cond_0
    :goto_0
    return-void

    .line 1328994
    :pswitch_0
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    iget p0, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2, p0}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 1328995
    :pswitch_1
    iget v2, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result p0

    invoke-virtual {v1, v2, p0}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 1328996
    :pswitch_2
    iget v2, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result p0

    invoke-virtual {v1, v2, p0}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 1328997
    :pswitch_3
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    iget p0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2, p0}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 1328998
    :pswitch_4
    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget p0, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2, p0}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 1328999
    :pswitch_5
    iget v2, v0, Landroid/graphics/RectF;->right:F

    iget p0, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2, p0}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 1329000
    :pswitch_6
    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget p0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2, p0}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    .line 1329001
    :pswitch_7
    iget v2, v0, Landroid/graphics/RectF;->right:F

    iget p0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2, p0}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final b()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 1329003
    iget-object v0, p0, LX/8Jf;->c:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final c()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1329002
    iget-object v0, p0, LX/8Jf;->f:LX/8Jh;

    iget-object v0, v0, LX/8Jh;->a:Landroid/graphics/Rect;

    return-object v0
.end method
