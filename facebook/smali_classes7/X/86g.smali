.class public LX/86g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/86d;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1297646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1297647
    return-void
.end method

.method public static a(LX/0QB;)LX/86g;
    .locals 3

    .prologue
    .line 1297635
    const-class v1, LX/86g;

    monitor-enter v1

    .line 1297636
    :try_start_0
    sget-object v0, LX/86g;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1297637
    sput-object v2, LX/86g;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1297638
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1297639
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1297640
    new-instance v0, LX/86g;

    invoke-direct {v0}, LX/86g;-><init>()V

    .line 1297641
    move-object v0, v0

    .line 1297642
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1297643
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/86g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1297644
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1297645
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;ZZ)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1297626
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getFrame()Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 1297627
    :goto_0
    return v0

    .line 1297628
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getFrame()Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->getFrame()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v0

    .line 1297629
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aW_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aW_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1297630
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aW_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;

    .line 1297631
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1297632
    goto :goto_0

    .line 1297633
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1297634
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
