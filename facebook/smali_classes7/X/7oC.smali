.class public final LX/7oC;
.super LX/0gV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gV",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderStatusSubscriptionModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1240822
    const-class v1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventTicketOrderStatusSubscriptionModel;

    const v0, 0x751a355e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "EventTicketOrderStatusSubscription"

    const-string v6, "eff43f2d4e9bf30e60c709815c0a9de3"

    const-string v7, "event_ticket_order_purchase_status_change"

    const-string v8, "0"

    const-string v9, "10155069963931729"

    const/4 v10, 0x0

    .line 1240823
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1240824
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0gV;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1240825
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1240817
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1240818
    sparse-switch v0, :sswitch_data_0

    .line 1240819
    :goto_0
    return-object p1

    .line 1240820
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1240821
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7d1a28b1 -> :sswitch_1
        0x5fb57ca -> :sswitch_0
    .end sparse-switch
.end method
