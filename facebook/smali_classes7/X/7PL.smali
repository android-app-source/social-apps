.class public final enum LX/7PL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7PL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7PL;

.field public static final enum BANDWIDTH:LX/7PL;

.field public static final enum VIDEO_BUFFER_MANAGER:LX/7PL;

.field public static final enum VIDEO_PLAYER_SESSION:LX/7PL;

.field public static final enum ZERO:LX/7PL;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1202772
    new-instance v0, LX/7PL;

    const-string v1, "ZERO"

    const-string v2, "zero"

    invoke-direct {v0, v1, v3, v2}, LX/7PL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7PL;->ZERO:LX/7PL;

    .line 1202773
    new-instance v0, LX/7PL;

    const-string v1, "BANDWIDTH"

    const-string v2, "bandwidth"

    invoke-direct {v0, v1, v4, v2}, LX/7PL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7PL;->BANDWIDTH:LX/7PL;

    .line 1202774
    new-instance v0, LX/7PL;

    const-string v1, "VIDEO_BUFFER_MANAGER"

    const-string v2, "vbm"

    invoke-direct {v0, v1, v5, v2}, LX/7PL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7PL;->VIDEO_BUFFER_MANAGER:LX/7PL;

    .line 1202775
    new-instance v0, LX/7PL;

    const-string v1, "VIDEO_PLAYER_SESSION"

    const-string v2, "vps"

    invoke-direct {v0, v1, v6, v2}, LX/7PL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7PL;->VIDEO_PLAYER_SESSION:LX/7PL;

    .line 1202776
    const/4 v0, 0x4

    new-array v0, v0, [LX/7PL;

    sget-object v1, LX/7PL;->ZERO:LX/7PL;

    aput-object v1, v0, v3

    sget-object v1, LX/7PL;->BANDWIDTH:LX/7PL;

    aput-object v1, v0, v4

    sget-object v1, LX/7PL;->VIDEO_BUFFER_MANAGER:LX/7PL;

    aput-object v1, v0, v5

    sget-object v1, LX/7PL;->VIDEO_PLAYER_SESSION:LX/7PL;

    aput-object v1, v0, v6

    sput-object v0, LX/7PL;->$VALUES:[LX/7PL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1202769
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1202770
    iput-object p3, p0, LX/7PL;->value:Ljava/lang/String;

    .line 1202771
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7PL;
    .locals 1

    .prologue
    .line 1202768
    const-class v0, LX/7PL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7PL;

    return-object v0
.end method

.method public static values()[LX/7PL;
    .locals 1

    .prologue
    .line 1202766
    sget-object v0, LX/7PL;->$VALUES:[LX/7PL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7PL;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1202767
    iget-object v0, p0, LX/7PL;->value:Ljava/lang/String;

    return-object v0
.end method
