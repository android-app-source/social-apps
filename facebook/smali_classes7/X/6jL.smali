.class public LX/6jL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static volatile d:LX/6jL;


# instance fields
.field public b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/3MK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1130581
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, LX/3MO;->a:LX/0U1;

    .line 1130582
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1130583
    aput-object v2, v0, v1

    sput-object v0, LX/6jL;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1130584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/6jL;
    .locals 4

    .prologue
    .line 1130585
    sget-object v0, LX/6jL;->d:LX/6jL;

    if-nez v0, :cond_1

    .line 1130586
    const-class v1, LX/6jL;

    monitor-enter v1

    .line 1130587
    :try_start_0
    sget-object v0, LX/6jL;->d:LX/6jL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1130588
    if-eqz v2, :cond_0

    .line 1130589
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1130590
    new-instance p0, LX/6jL;

    invoke-direct {p0}, LX/6jL;-><init>()V

    .line 1130591
    invoke-static {v0}, LX/3MK;->a(LX/0QB;)LX/3MK;

    move-result-object v3

    check-cast v3, LX/3MK;

    .line 1130592
    iput-object v3, p0, LX/6jL;->c:LX/3MK;

    .line 1130593
    move-object v0, p0

    .line 1130594
    sput-object v0, LX/6jL;->d:LX/6jL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1130595
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1130596
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1130597
    :cond_1
    sget-object v0, LX/6jL;->d:LX/6jL;

    return-object v0

    .line 1130598
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1130599
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 1130600
    iget-object v0, p0, LX/6jL;->b:Ljava/util/Set;

    if-nez v0, :cond_1

    .line 1130601
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 1130602
    :try_start_0
    iget-object v0, p0, LX/6jL;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "server_spam_list"

    sget-object v2, LX/6jL;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1130603
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1130604
    sget-object v0, LX/3MO;->a:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 1130605
    invoke-interface {v9, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 1130606
    :catch_0
    move-exception v0

    .line 1130607
    :goto_1
    :try_start_2
    const-string v2, "SmsTakeoverSpamDbHandler"

    const-string v3, "Error getting spam info"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1130608
    if-eqz v1, :cond_0

    .line 1130609
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1130610
    :cond_0
    :goto_2
    iput-object v9, p0, LX/6jL;->b:Ljava/util/Set;

    .line 1130611
    :cond_1
    iget-object v0, p0, LX/6jL;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0

    .line 1130612
    :cond_2
    if-eqz v1, :cond_0

    .line 1130613
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 1130614
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_3
    if-eqz v1, :cond_3

    .line 1130615
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 1130616
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 1130617
    :catch_1
    move-exception v0

    move-object v1, v8

    goto :goto_1
.end method

.method public final b(Ljava/util/Collection;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1130618
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1130619
    iget-object v0, p0, LX/6jL;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v2, -0x6cdd4244

    invoke-static {v0, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1130620
    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1130621
    sget-object v3, LX/3MO;->a:LX/0U1;

    .line 1130622
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1130623
    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1130624
    iget-object v0, p0, LX/6jL;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v3, "server_spam_list"

    const/4 v4, 0x0

    const/4 v5, 0x4

    const v6, 0x5cbcf802

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {v0, v3, v4, v1, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const v0, 0x5b772a19

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1130625
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/6jL;->c:LX/3MK;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const v2, -0x166f1406    # -2.189996E25f

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 1130626
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/6jL;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1130627
    iget-object v0, p0, LX/6jL;->b:Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 1130628
    iget-object v0, p0, LX/6jL;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1130629
    :cond_1
    iget-object v0, p0, LX/6jL;->c:LX/3MK;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const v1, -0x73fdd4a7

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1130630
    return-void
.end method
