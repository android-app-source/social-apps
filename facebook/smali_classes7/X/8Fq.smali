.class public LX/8Fq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/61B;


# instance fields
.field private a:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

.field private b:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;LX/7SH;)V
    .locals 1
    .param p1    # Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1318211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318212
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p2, v0}, LX/7SH;->a(Landroid/net/Uri;)Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    move-result-object v0

    iput-object v0, p0, LX/8Fq;->a:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    .line 1318213
    invoke-virtual {p0, p1}, LX/8Fq;->a(Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;)V

    .line 1318214
    return-void

    .line 1318215
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 1318218
    iget-object v0, p0, LX/8Fq;->a:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->a(II)V

    .line 1318219
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 1

    .prologue
    .line 1318216
    iget-object v0, p0, LX/8Fq;->a:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    invoke-virtual {v0, p1}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->a(LX/5Pc;)V

    .line 1318217
    return-void
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 1318220
    if-nez p1, :cond_0

    .line 1318221
    iget-object v1, p0, LX/8Fq;->a:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    invoke-virtual {v1, v0}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->a(Landroid/net/Uri;)V

    .line 1318222
    :goto_0
    return-void

    .line 1318223
    :cond_0
    iput-object p1, p0, LX/8Fq;->b:Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;

    .line 1318224
    invoke-static {v6}, LX/8Fj;->a(F)F

    move-result v1

    .line 1318225
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getWidthPercentage()F

    move-result v3

    add-float/2addr v2, v3

    .line 1318226
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v3

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getHeightPercentage()F

    move-result v4

    add-float/2addr v3, v4

    .line 1318227
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getLeftPercentage()F

    move-result v4

    invoke-static {v4}, LX/8Fj;->a(F)F

    move-result v4

    add-float/2addr v4, v6

    .line 1318228
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getTopPercentage()F

    move-result v5

    sub-float v5, v7, v5

    invoke-static {v5, v1}, LX/8Fj;->a(FF)F

    move-result v5

    .line 1318229
    invoke-static {v2}, LX/8Fj;->a(F)F

    move-result v2

    add-float/2addr v2, v6

    .line 1318230
    sub-float v3, v7, v3

    invoke-static {v3, v1}, LX/8Fj;->a(FF)F

    move-result v1

    .line 1318231
    iget-object v3, p0, LX/8Fq;->a:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->a(Landroid/net/Uri;)V

    .line 1318232
    iget-object v0, p0, LX/8Fq;->a:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    .line 1318233
    sget-object v3, LX/8Fj;->a:[F

    const/4 v6, 0x0

    aput v4, v3, v6

    .line 1318234
    sget-object v3, LX/8Fj;->a:[F

    const/4 v6, 0x1

    aput v1, v3, v6

    .line 1318235
    sget-object v3, LX/8Fj;->a:[F

    const/4 v6, 0x2

    aput v2, v3, v6

    .line 1318236
    sget-object v3, LX/8Fj;->a:[F

    const/4 v6, 0x3

    aput v1, v3, v6

    .line 1318237
    sget-object v3, LX/8Fj;->a:[F

    const/4 v6, 0x4

    aput v4, v3, v6

    .line 1318238
    sget-object v3, LX/8Fj;->a:[F

    const/4 v6, 0x5

    aput v5, v3, v6

    .line 1318239
    sget-object v3, LX/8Fj;->a:[F

    const/4 v6, 0x6

    aput v2, v3, v6

    .line 1318240
    sget-object v3, LX/8Fj;->a:[F

    const/4 v6, 0x7

    aput v5, v3, v6

    .line 1318241
    sget-object v3, LX/8Fj;->a:[F

    move-object v1, v3

    .line 1318242
    invoke-virtual {v0, v1}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->a([F)V

    goto :goto_0

    .line 1318243
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/RelativeImageOverlayParams;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1
.end method

.method public final a([F[F[FJ)V
    .locals 6

    .prologue
    .line 1318206
    iget-object v0, p0, LX/8Fq;->a:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    move-object v1, p1

    move-object v2, p1

    move-object v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->a([F[F[FJ)V

    .line 1318207
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1318208
    iget-object v0, p0, LX/8Fq;->a:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->b()V

    .line 1318209
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1318210
    iget-object v0, p0, LX/8Fq;->a:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->c()Z

    move-result v0

    return v0
.end method
