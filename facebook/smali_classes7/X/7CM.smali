.class public final enum LX/7CM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7CM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7CM;

.field public static final enum INLINE_EVENTS_LINK:LX/7CM;

.field public static final enum INLINE_FRIENDS_LINK:LX/7CM;

.field public static final enum INLINE_PHOTOS_LINK:LX/7CM;

.field public static final enum MESSAGE_SELLER:LX/7CM;

.field public static final enum OPEN_CHANNEL_FEED:LX/7CM;

.field public static final enum OPEN_PAGE_CTA:LX/7CM;

.field public static final enum OPEN_PRODUCT_DETAIL_PAGE:LX/7CM;

.field public static final enum OPEN_PROFILE_SNAPSHOT:LX/7CM;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1180470
    new-instance v0, LX/7CM;

    const-string v1, "OPEN_CHANNEL_FEED"

    const-string v2, "open_channel_feed"

    invoke-direct {v0, v1, v4, v2}, LX/7CM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7CM;->OPEN_CHANNEL_FEED:LX/7CM;

    .line 1180471
    new-instance v0, LX/7CM;

    const-string v1, "MESSAGE_SELLER"

    const-string v2, "message_seller"

    invoke-direct {v0, v1, v5, v2}, LX/7CM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7CM;->MESSAGE_SELLER:LX/7CM;

    .line 1180472
    new-instance v0, LX/7CM;

    const-string v1, "OPEN_PAGE_CTA"

    const-string v2, "open_page_cta"

    invoke-direct {v0, v1, v6, v2}, LX/7CM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7CM;->OPEN_PAGE_CTA:LX/7CM;

    .line 1180473
    new-instance v0, LX/7CM;

    const-string v1, "OPEN_PRODUCT_DETAIL_PAGE"

    const-string v2, "open_product_detail_page"

    invoke-direct {v0, v1, v7, v2}, LX/7CM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7CM;->OPEN_PRODUCT_DETAIL_PAGE:LX/7CM;

    .line 1180474
    new-instance v0, LX/7CM;

    const-string v1, "OPEN_PROFILE_SNAPSHOT"

    const-string v2, "open_profile_snapshot"

    invoke-direct {v0, v1, v8, v2}, LX/7CM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7CM;->OPEN_PROFILE_SNAPSHOT:LX/7CM;

    .line 1180475
    new-instance v0, LX/7CM;

    const-string v1, "INLINE_PHOTOS_LINK"

    const/4 v2, 0x5

    const-string v3, "inline_photos_link"

    invoke-direct {v0, v1, v2, v3}, LX/7CM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7CM;->INLINE_PHOTOS_LINK:LX/7CM;

    .line 1180476
    new-instance v0, LX/7CM;

    const-string v1, "INLINE_FRIENDS_LINK"

    const/4 v2, 0x6

    const-string v3, "inline_friends_link"

    invoke-direct {v0, v1, v2, v3}, LX/7CM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7CM;->INLINE_FRIENDS_LINK:LX/7CM;

    .line 1180477
    new-instance v0, LX/7CM;

    const-string v1, "INLINE_EVENTS_LINK"

    const/4 v2, 0x7

    const-string v3, "inline_events_link"

    invoke-direct {v0, v1, v2, v3}, LX/7CM;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/7CM;->INLINE_EVENTS_LINK:LX/7CM;

    .line 1180478
    const/16 v0, 0x8

    new-array v0, v0, [LX/7CM;

    sget-object v1, LX/7CM;->OPEN_CHANNEL_FEED:LX/7CM;

    aput-object v1, v0, v4

    sget-object v1, LX/7CM;->MESSAGE_SELLER:LX/7CM;

    aput-object v1, v0, v5

    sget-object v1, LX/7CM;->OPEN_PAGE_CTA:LX/7CM;

    aput-object v1, v0, v6

    sget-object v1, LX/7CM;->OPEN_PRODUCT_DETAIL_PAGE:LX/7CM;

    aput-object v1, v0, v7

    sget-object v1, LX/7CM;->OPEN_PROFILE_SNAPSHOT:LX/7CM;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/7CM;->INLINE_PHOTOS_LINK:LX/7CM;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/7CM;->INLINE_FRIENDS_LINK:LX/7CM;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/7CM;->INLINE_EVENTS_LINK:LX/7CM;

    aput-object v2, v0, v1

    sput-object v0, LX/7CM;->$VALUES:[LX/7CM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1180467
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1180468
    iput-object p3, p0, LX/7CM;->value:Ljava/lang/String;

    .line 1180469
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7CM;
    .locals 1

    .prologue
    .line 1180466
    const-class v0, LX/7CM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7CM;

    return-object v0
.end method

.method public static values()[LX/7CM;
    .locals 1

    .prologue
    .line 1180465
    sget-object v0, LX/7CM;->$VALUES:[LX/7CM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7CM;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1180464
    iget-object v0, p0, LX/7CM;->value:Ljava/lang/String;

    return-object v0
.end method
