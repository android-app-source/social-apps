.class public final LX/84G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/3UJ;


# direct methods
.method public constructor <init>(LX/3UJ;J)V
    .locals 0

    .prologue
    .line 1290874
    iput-object p1, p0, LX/84G;->b:LX/3UJ;

    iput-wide p2, p0, LX/84G;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1290875
    iget-object v0, p0, LX/84G;->b:LX/3UJ;

    iget-object v1, v0, LX/3UJ;->a:LX/2dj;

    iget-wide v2, p0, LX/84G;->a:J

    sget-object v4, LX/2h8;->SUGGESTION:LX/2h8;

    move-object v6, v5

    invoke-virtual/range {v1 .. v6}, LX/2dj;->a(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1290876
    iget-object v1, p0, LX/84G;->b:LX/3UJ;

    iget-object v1, v1, LX/3UJ;->b:LX/2do;

    new-instance v2, LX/2f2;

    iget-wide v4, p0, LX/84G;->a:J

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v6, 0x1

    invoke-direct {v2, v4, v5, v3, v6}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1290877
    iget-object v1, p0, LX/84G;->b:LX/3UJ;

    iget-object v1, v1, LX/3UJ;->d:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ADD_FRIEND_IGNORE_WARN"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, LX/84G;->a:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/84E;

    invoke-direct {v3, p0, v0}, LX/84E;-><init>(LX/84G;Lcom/google/common/util/concurrent/ListenableFuture;)V

    new-instance v0, LX/84F;

    invoke-direct {v0, p0}, LX/84F;-><init>(LX/84G;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1290878
    return-void
.end method
