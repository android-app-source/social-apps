.class public LX/7SP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/61B;


# instance fields
.field private a:LX/5Pb;

.field private b:LX/5PR;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x2

    .line 1208778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1208779
    new-instance v0, LX/5PQ;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/5PQ;-><init>(I)V

    const/4 v1, 0x5

    .line 1208780
    iput v1, v0, LX/5PQ;->a:I

    .line 1208781
    move-object v0, v0

    .line 1208782
    const-string v1, "aPosition"

    new-instance v2, LX/5Pg;

    new-array v3, v5, [F

    fill-array-data v3, :array_0

    invoke-direct {v2, v3, v4}, LX/5Pg;-><init>([FI)V

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    const-string v1, "aTextureCoord"

    new-instance v2, LX/5Pg;

    new-array v3, v5, [F

    fill-array-data v3, :array_1

    invoke-direct {v2, v3, v4}, LX/5Pg;-><init>([FI)V

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    invoke-virtual {v0}, LX/5PQ;->a()LX/5PR;

    move-result-object v0

    iput-object v0, p0, LX/7SP;->b:LX/5PR;

    .line 1208783
    return-void

    nop

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .prologue
    .line 1208777
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 2

    .prologue
    .line 1208784
    const v0, 0x7f070097

    const v1, 0x7f070091

    invoke-interface {p1, v0, v1}, LX/5Pc;->a(II)LX/5Pb;

    move-result-object v0

    iput-object v0, p0, LX/7SP;->a:LX/5Pb;

    .line 1208785
    return-void
.end method

.method public final a([F[F[FJ)V
    .locals 2

    .prologue
    .line 1208774
    const-string v0, "onDrawFrame"

    invoke-static {v0}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1208775
    iget-object v0, p0, LX/7SP;->a:LX/5Pb;

    invoke-virtual {v0}, LX/5Pb;->a()LX/5Pa;

    move-result-object v0

    const-string v1, "uSTMatrix"

    invoke-virtual {v0, v1, p1}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    const-string v1, "uConstMatrix"

    invoke-virtual {v0, v1, p2}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    const-string v1, "uSceneMatrix"

    invoke-virtual {v0, v1, p3}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    iget-object v1, p0, LX/7SP;->b:LX/5PR;

    invoke-virtual {v0, v1}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    .line 1208776
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1208773
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1208772
    const/4 v0, 0x1

    return v0
.end method
