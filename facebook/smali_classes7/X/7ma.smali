.class public LX/7ma;
.super LX/7mV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7mV",
        "<",
        "LX/7ml;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final e:Ljava/lang/String;

.field private static volatile h:LX/7ma;


# instance fields
.field private final f:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field private final g:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1237263
    const-class v0, LX/7ma;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7ma;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/0SG;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1237259
    invoke-direct {p0, p2}, LX/7mV;-><init>(LX/0SG;)V

    .line 1237260
    iput-object p1, p0, LX/7ma;->f:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1237261
    iput-object p3, p0, LX/7ma;->g:LX/0ad;

    .line 1237262
    return-void
.end method

.method public static a(LX/0QB;)LX/7ma;
    .locals 6

    .prologue
    .line 1237246
    sget-object v0, LX/7ma;->h:LX/7ma;

    if-nez v0, :cond_1

    .line 1237247
    const-class v1, LX/7ma;

    monitor-enter v1

    .line 1237248
    :try_start_0
    sget-object v0, LX/7ma;->h:LX/7ma;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1237249
    if-eqz v2, :cond_0

    .line 1237250
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1237251
    new-instance p0, LX/7ma;

    invoke-static {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/7ma;-><init>(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/0SG;LX/0ad;)V

    .line 1237252
    move-object v0, p0

    .line 1237253
    sput-object v0, LX/7ma;->h:LX/7ma;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1237254
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1237255
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1237256
    :cond_1
    sget-object v0, LX/7ma;->h:LX/7ma;

    return-object v0

    .line 1237257
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1237258
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1237236
    invoke-virtual {p0, p1}, LX/7mV;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1237237
    invoke-super {p0, p1}, LX/7mV;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1237238
    :goto_0
    return-object v0

    .line 1237239
    :cond_0
    iget-object v0, p0, LX/7mV;->d:LX/7mc;

    if-eqz v0, :cond_1

    .line 1237240
    new-instance v0, LX/7ml;

    new-instance v1, LX/5M1;

    new-instance v2, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    new-instance v3, LX/23u;

    invoke-direct {v3}, LX/23u;-><init>()V

    .line 1237241
    iput-object p1, v3, LX/23u;->t:Ljava/lang/String;

    .line 1237242
    move-object v3, v3

    .line 1237243
    invoke-virtual {v3}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-direct {v2, v3, v4, v4}, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    invoke-direct {v1, v2}, LX/5M1;-><init>(Lcom/facebook/composer/publish/common/PendingStoryPersistentData;)V

    invoke-virtual {v1}, LX/5M1;->a()Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v1

    sget-object v2, LX/7mm;->POST:LX/7mm;

    invoke-direct {v0, v1, v2}, LX/7ml;-><init>(Lcom/facebook/composer/publish/common/PendingStory;LX/7mm;)V

    .line 1237244
    iget-object v1, p0, LX/7mV;->d:LX/7mc;

    invoke-interface {v1, v0}, LX/7mc;->b(Ljava/lang/Object;)V

    .line 1237245
    :cond_1
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1237210
    const-wide/32 v0, 0x127500

    return-wide v0
.end method

.method public final b(Ljava/lang/String;)LX/7ml;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237229
    invoke-super {p0, p1}, LX/7mV;->c(Ljava/lang/String;)LX/7mi;

    move-result-object v0

    check-cast v0, LX/7ml;

    .line 1237230
    if-eqz v0, :cond_0

    .line 1237231
    :goto_0
    return-object v0

    .line 1237232
    :cond_0
    iget-object v0, p0, LX/7ma;->f:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v1

    .line 1237233
    if-eqz v1, :cond_1

    .line 1237234
    new-instance v0, LX/7ml;

    sget-object v2, LX/7mm;->POST:LX/7mm;

    invoke-direct {v0, v1, v2}, LX/7ml;-><init>(Lcom/facebook/composer/publish/common/PendingStory;LX/7mm;)V

    goto :goto_0

    .line 1237235
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/7ml;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1237212
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1237213
    iget-object v0, p0, LX/7ma;->g:LX/0ad;

    sget-short v2, LX/1aO;->as:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1237214
    iget-object v0, p0, LX/7ma;->f:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PendingStory;

    .line 1237215
    invoke-static {v0}, LX/7md;->a(Lcom/facebook/composer/publish/common/PendingStory;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1237216
    new-instance v6, LX/7ml;

    sget-object v7, LX/7mm;->POST:LX/7mm;

    invoke-direct {v6, v0, v7}, LX/7ml;-><init>(Lcom/facebook/composer/publish/common/PendingStory;LX/7mm;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1237217
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1237218
    :cond_1
    iget-object v0, p0, LX/7ma;->f:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PendingStory;

    .line 1237219
    new-instance v6, LX/7ml;

    sget-object v7, LX/7mm;->POST:LX/7mm;

    invoke-direct {v6, v0, v7}, LX/7ml;-><init>(Lcom/facebook/composer/publish/common/PendingStory;LX/7mm;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1237220
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1237221
    :cond_2
    :try_start_0
    invoke-virtual {p0}, LX/7mV;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const v2, -0x4e435522

    invoke-static {v0, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7ml;

    .line 1237222
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1237223
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 1237224
    :catch_0
    move-exception v0

    .line 1237225
    :goto_3
    sget-object v1, LX/7ma;->e:Ljava/lang/String;

    const-string v2, "Failed to add extra stories"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1237226
    :cond_3
    new-instance v0, LX/7mZ;

    invoke-direct {v0, p0}, LX/7mZ;-><init>(LX/7ma;)V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1237227
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 1237228
    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method public final synthetic c(Ljava/lang/String;)LX/7mi;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1237211
    invoke-virtual {p0, p1}, LX/7ma;->b(Ljava/lang/String;)LX/7ml;

    move-result-object v0

    return-object v0
.end method
