.class public LX/739;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0TD;

.field public final b:LX/730;

.field public final c:LX/732;

.field public final d:LX/733;


# direct methods
.method public constructor <init>(LX/0TD;LX/730;LX/732;LX/733;)V
    .locals 0
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1165318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1165319
    iput-object p1, p0, LX/739;->a:LX/0TD;

    .line 1165320
    iput-object p2, p0, LX/739;->b:LX/730;

    .line 1165321
    iput-object p3, p0, LX/739;->c:LX/732;

    .line 1165322
    iput-object p4, p0, LX/739;->d:LX/733;

    .line 1165323
    return-void
.end method

.method public static b(LX/0QB;)LX/739;
    .locals 5

    .prologue
    .line 1165324
    new-instance v4, LX/739;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, LX/0TD;

    invoke-static {p0}, LX/730;->b(LX/0QB;)LX/730;

    move-result-object v1

    check-cast v1, LX/730;

    invoke-static {p0}, LX/732;->b(LX/0QB;)LX/732;

    move-result-object v2

    check-cast v2, LX/732;

    invoke-static {p0}, LX/733;->b(LX/0QB;)LX/733;

    move-result-object v3

    check-cast v3, LX/733;

    invoke-direct {v4, v0, v1, v2, v3}, LX/739;-><init>(LX/0TD;LX/730;LX/732;LX/733;)V

    .line 1165325
    return-object v4
.end method
