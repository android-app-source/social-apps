.class public final enum LX/8TK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8TK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8TK;

.field public static final enum CHALLENGE_CARD:LX/8TK;

.field public static final enum CHALLENGE_CARD_POPOVER:LX/8TK;

.field public static final enum CHALLENGE_CREATION:LX/8TK;

.field public static final enum CHALLENGE_LIST:LX/8TK;

.field public static final enum LEADERBOARD_ROW:LX/8TK;

.field public static final enum PLAY_ALL_FACEBOOK_FRIENDS:LX/8TK;


# instance fields
.field public final effect:LX/8TJ;

.field public final loggingTag:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1348465
    new-instance v0, LX/8TK;

    const-string v1, "CHALLENGE_CARD_POPOVER"

    const-string v2, "challenge_card_popover"

    sget-object v3, LX/8TJ;->UPDATE:LX/8TJ;

    invoke-direct {v0, v1, v5, v2, v3}, LX/8TK;-><init>(Ljava/lang/String;ILjava/lang/String;LX/8TJ;)V

    sput-object v0, LX/8TK;->CHALLENGE_CARD_POPOVER:LX/8TK;

    .line 1348466
    new-instance v0, LX/8TK;

    const-string v1, "PLAY_ALL_FACEBOOK_FRIENDS"

    const-string v2, "play_facebook_friends"

    sget-object v3, LX/8TJ;->REMOVE:LX/8TJ;

    invoke-direct {v0, v1, v6, v2, v3}, LX/8TK;-><init>(Ljava/lang/String;ILjava/lang/String;LX/8TJ;)V

    sput-object v0, LX/8TK;->PLAY_ALL_FACEBOOK_FRIENDS:LX/8TK;

    .line 1348467
    new-instance v0, LX/8TK;

    const-string v1, "CHALLENGE_LIST"

    const-string v2, "challenge_list"

    sget-object v3, LX/8TJ;->UPDATE:LX/8TJ;

    invoke-direct {v0, v1, v7, v2, v3}, LX/8TK;-><init>(Ljava/lang/String;ILjava/lang/String;LX/8TJ;)V

    sput-object v0, LX/8TK;->CHALLENGE_LIST:LX/8TK;

    .line 1348468
    new-instance v0, LX/8TK;

    const-string v1, "CHALLENGE_CREATION"

    const-string v2, "challenge_creation"

    sget-object v3, LX/8TJ;->UPDATE:LX/8TJ;

    invoke-direct {v0, v1, v8, v2, v3}, LX/8TK;-><init>(Ljava/lang/String;ILjava/lang/String;LX/8TJ;)V

    sput-object v0, LX/8TK;->CHALLENGE_CREATION:LX/8TK;

    .line 1348469
    new-instance v0, LX/8TK;

    const-string v1, "CHALLENGE_CARD"

    const-string v2, "challenge_card"

    sget-object v3, LX/8TJ;->UPDATE:LX/8TJ;

    invoke-direct {v0, v1, v9, v2, v3}, LX/8TK;-><init>(Ljava/lang/String;ILjava/lang/String;LX/8TJ;)V

    sput-object v0, LX/8TK;->CHALLENGE_CARD:LX/8TK;

    .line 1348470
    new-instance v0, LX/8TK;

    const-string v1, "LEADERBOARD_ROW"

    const/4 v2, 0x5

    const-string v3, "leaderboard_row"

    sget-object v4, LX/8TJ;->UPDATE:LX/8TJ;

    invoke-direct {v0, v1, v2, v3, v4}, LX/8TK;-><init>(Ljava/lang/String;ILjava/lang/String;LX/8TJ;)V

    sput-object v0, LX/8TK;->LEADERBOARD_ROW:LX/8TK;

    .line 1348471
    const/4 v0, 0x6

    new-array v0, v0, [LX/8TK;

    sget-object v1, LX/8TK;->CHALLENGE_CARD_POPOVER:LX/8TK;

    aput-object v1, v0, v5

    sget-object v1, LX/8TK;->PLAY_ALL_FACEBOOK_FRIENDS:LX/8TK;

    aput-object v1, v0, v6

    sget-object v1, LX/8TK;->CHALLENGE_LIST:LX/8TK;

    aput-object v1, v0, v7

    sget-object v1, LX/8TK;->CHALLENGE_CREATION:LX/8TK;

    aput-object v1, v0, v8

    sget-object v1, LX/8TK;->CHALLENGE_CARD:LX/8TK;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/8TK;->LEADERBOARD_ROW:LX/8TK;

    aput-object v2, v0, v1

    sput-object v0, LX/8TK;->$VALUES:[LX/8TK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;LX/8TJ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/8TJ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1348472
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1348473
    iput-object p3, p0, LX/8TK;->loggingTag:Ljava/lang/String;

    .line 1348474
    iput-object p4, p0, LX/8TK;->effect:LX/8TJ;

    .line 1348475
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8TK;
    .locals 1

    .prologue
    .line 1348476
    const-class v0, LX/8TK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8TK;

    return-object v0
.end method

.method public static values()[LX/8TK;
    .locals 1

    .prologue
    .line 1348477
    sget-object v0, LX/8TK;->$VALUES:[LX/8TK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8TK;

    return-object v0
.end method
