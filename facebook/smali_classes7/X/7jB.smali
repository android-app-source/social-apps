.class public LX/7jB;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/7jC;",
        "LX/7jA;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/7jB;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1229239
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 1229240
    return-void
.end method

.method public static a(LX/0QB;)LX/7jB;
    .locals 3

    .prologue
    .line 1229241
    sget-object v0, LX/7jB;->a:LX/7jB;

    if-nez v0, :cond_1

    .line 1229242
    const-class v1, LX/7jB;

    monitor-enter v1

    .line 1229243
    :try_start_0
    sget-object v0, LX/7jB;->a:LX/7jB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1229244
    if-eqz v2, :cond_0

    .line 1229245
    :try_start_1
    new-instance v0, LX/7jB;

    invoke-direct {v0}, LX/7jB;-><init>()V

    .line 1229246
    move-object v0, v0

    .line 1229247
    sput-object v0, LX/7jB;->a:LX/7jB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1229248
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1229249
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1229250
    :cond_1
    sget-object v0, LX/7jB;->a:LX/7jB;

    return-object v0

    .line 1229251
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1229252
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
