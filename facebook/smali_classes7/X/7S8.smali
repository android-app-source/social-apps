.class public final enum LX/7S8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7S8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7S8;

.field public static final enum CLEAR:LX/7S8;

.field public static final enum END:LX/7S8;

.field public static final enum MOVE:LX/7S8;

.field public static final enum START:LX/7S8;

.field public static final enum UNDO:LX/7S8;

.field public static final enum VIEW_INIT:LX/7S8;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1208187
    new-instance v0, LX/7S8;

    const-string v1, "START"

    invoke-direct {v0, v1, v3}, LX/7S8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7S8;->START:LX/7S8;

    .line 1208188
    new-instance v0, LX/7S8;

    const-string v1, "MOVE"

    invoke-direct {v0, v1, v4}, LX/7S8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7S8;->MOVE:LX/7S8;

    .line 1208189
    new-instance v0, LX/7S8;

    const-string v1, "END"

    invoke-direct {v0, v1, v5}, LX/7S8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7S8;->END:LX/7S8;

    .line 1208190
    new-instance v0, LX/7S8;

    const-string v1, "UNDO"

    invoke-direct {v0, v1, v6}, LX/7S8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7S8;->UNDO:LX/7S8;

    .line 1208191
    new-instance v0, LX/7S8;

    const-string v1, "CLEAR"

    invoke-direct {v0, v1, v7}, LX/7S8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7S8;->CLEAR:LX/7S8;

    .line 1208192
    new-instance v0, LX/7S8;

    const-string v1, "VIEW_INIT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/7S8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7S8;->VIEW_INIT:LX/7S8;

    .line 1208193
    const/4 v0, 0x6

    new-array v0, v0, [LX/7S8;

    sget-object v1, LX/7S8;->START:LX/7S8;

    aput-object v1, v0, v3

    sget-object v1, LX/7S8;->MOVE:LX/7S8;

    aput-object v1, v0, v4

    sget-object v1, LX/7S8;->END:LX/7S8;

    aput-object v1, v0, v5

    sget-object v1, LX/7S8;->UNDO:LX/7S8;

    aput-object v1, v0, v6

    sget-object v1, LX/7S8;->CLEAR:LX/7S8;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/7S8;->VIEW_INIT:LX/7S8;

    aput-object v2, v0, v1

    sput-object v0, LX/7S8;->$VALUES:[LX/7S8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1208194
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7S8;
    .locals 1

    .prologue
    .line 1208195
    const-class v0, LX/7S8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7S8;

    return-object v0
.end method

.method public static values()[LX/7S8;
    .locals 1

    .prologue
    .line 1208196
    sget-object v0, LX/7S8;->$VALUES:[LX/7S8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7S8;

    return-object v0
.end method
