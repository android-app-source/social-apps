.class public final LX/8O4;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/1FJ",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/43G;

.field public final synthetic b:Ljava/io/File;

.field public final synthetic c:Landroid/net/Uri;

.field public final synthetic d:Ljava/util/concurrent/CountDownLatch;

.field public final synthetic e:LX/8OJ;


# direct methods
.method public constructor <init>(LX/8OJ;LX/43G;Ljava/io/File;Landroid/net/Uri;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 1338468
    iput-object p1, p0, LX/8O4;->e:LX/8OJ;

    iput-object p2, p0, LX/8O4;->a:LX/43G;

    iput-object p3, p0, LX/8O4;->b:Ljava/io/File;

    iput-object p4, p0, LX/8O4;->c:Landroid/net/Uri;

    iput-object p5, p0, LX/8O4;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1338469
    sget-object v0, LX/8OJ;->a:Ljava/lang/Class;

    const-string v1, "Error creating overlay bitmap."

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1338470
    iget-object v0, p0, LX/8O4;->e:LX/8OJ;

    const/4 v1, 0x0

    .line 1338471
    iput-boolean v1, v0, LX/8OJ;->t:Z

    .line 1338472
    iget-object v0, p0, LX/8O4;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 1338473
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1338450
    check-cast p1, LX/1FJ;

    .line 1338451
    :try_start_0
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    iget-object v2, p0, LX/8O4;->a:LX/43G;

    .line 1338452
    iget v3, v2, LX/43G;->c:I

    move v2, v3

    .line 1338453
    iget-object v3, p0, LX/8O4;->b:Ljava/io/File;

    invoke-static {v0, v1, v2, v3}, LX/2Qx;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;ILjava/io/File;)V

    .line 1338454
    iget-object v0, p0, LX/8O4;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/8O4;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/2Qx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1338455
    iget-object v0, p0, LX/8O4;->e:LX/8OJ;

    const/4 v1, 0x1

    .line 1338456
    iput-boolean v1, v0, LX/8OJ;->t:Z
    :try_end_0
    .catch LX/42w; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1338457
    invoke-virtual {p1}, LX/1FJ;->close()V

    .line 1338458
    iget-object v0, p0, LX/8O4;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 1338459
    :goto_0
    return-void

    .line 1338460
    :catch_0
    move-exception v0

    .line 1338461
    :try_start_1
    sget-object v1, LX/8OJ;->a:Ljava/lang/Class;

    const-string v2, "Error writing overlay photo to disk."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1338462
    iget-object v0, p0, LX/8O4;->e:LX/8OJ;

    const/4 v1, 0x0

    .line 1338463
    iput-boolean v1, v0, LX/8OJ;->t:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1338464
    invoke-virtual {p1}, LX/1FJ;->close()V

    .line 1338465
    iget-object v0, p0, LX/8O4;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 1338466
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, LX/1FJ;->close()V

    .line 1338467
    iget-object v1, p0, LX/8O4;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method
