.class public final LX/8MT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/7ml;

.field public final synthetic b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)V
    .locals 0

    .prologue
    .line 1334442
    iput-object p1, p0, LX/8MT;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iput-object p2, p0, LX/8MT;->a:LX/7ml;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const v0, 0x5844d2df

    invoke-static {v5, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1334443
    new-instance v1, LX/5OM;

    iget-object v2, p0, LX/8MT;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v2, v2, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->P:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 1334444
    invoke-virtual {v1}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 1334445
    iget-object v3, p0, LX/8MT;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v4, p0, LX/8MT;->a:LX/7ml;

    invoke-static {v3, v4, v2, v6}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a$redex0(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;LX/5OG;Z)V

    .line 1334446
    iget-object v3, p0, LX/8MT;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v4, p0, LX/8MT;->a:LX/7ml;

    invoke-static {v3, v4}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->c(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;)Z

    move-result v3

    .line 1334447
    if-eqz v3, :cond_0

    iget-object v3, p0, LX/8MT;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    invoke-static {v3}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->B(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1334448
    iget-object v3, p0, LX/8MT;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v4, p0, LX/8MT;->a:LX/7ml;

    invoke-static {v3, v4, v2}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->c$redex0(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/7ml;LX/5OG;)V

    .line 1334449
    :cond_0
    invoke-virtual {v1, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1334450
    iget-object v2, p0, LX/8MT;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    invoke-static {v2, v1}, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->a$redex0(Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;LX/5OM;)V

    .line 1334451
    invoke-virtual {v1}, LX/0ht;->d()V

    .line 1334452
    iget-object v1, p0, LX/8MT;->b:Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;

    iget-object v1, v1, Lcom/facebook/photos/upload/progresspage/CompostStoryViewHolder;->t:LX/1RW;

    sget-object v2, LX/8K6;->FATAL_SECTION:LX/8K6;

    iget-object v2, v2, LX/8K6;->analyticsName:Ljava/lang/String;

    iget-object v3, p0, LX/8MT;->a:LX/7ml;

    invoke-virtual {v3}, LX/7mi;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1RW;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1334453
    const v1, 0x377a324a

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
