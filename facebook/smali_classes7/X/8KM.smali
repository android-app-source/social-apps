.class public final LX/8KM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;)V
    .locals 0

    .prologue
    .line 1329995
    iput-object p1, p0, LX/8KM;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1329996
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1329997
    iget-object v0, p0, LX/8KM;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v1, p0, LX/8KM;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v1, v1, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    const-string v2, "onUploadOptions"

    const-string v3, "cancelUpload / fatal"

    .line 1329998
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->a$redex0(Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;Ljava/lang/String;)V

    .line 1329999
    iget-object v0, p0, LX/8KM;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v0, v0, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->q:LX/1EZ;

    iget-object v1, p0, LX/8KM;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    iget-object v1, v1, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->D:Lcom/facebook/photos/upload/operation/UploadOperation;

    const-string v2, "Upload Dialog Cancel"

    invoke-virtual {v0, v1, v2}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;Ljava/lang/String;)V

    .line 1330000
    iget-object v0, p0, LX/8KM;->a:Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;

    invoke-virtual {v0}, Lcom/facebook/photos/upload/dialog/UploadDialogsActivity;->finish()V

    .line 1330001
    return-void
.end method
