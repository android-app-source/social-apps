.class public LX/7SW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/5Pc;

.field private b:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/5Pb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5Pc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1208935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1208936
    iput-object p1, p0, LX/7SW;->a:LX/5Pc;

    .line 1208937
    return-void
.end method

.method public static a(LX/0QB;)LX/7SW;
    .locals 4

    .prologue
    .line 1208938
    const-class v1, LX/7SW;

    monitor-enter v1

    .line 1208939
    :try_start_0
    sget-object v0, LX/7SW;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1208940
    sput-object v2, LX/7SW;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1208941
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1208942
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1208943
    new-instance p0, LX/7SW;

    invoke-static {v0}, LX/6Wx;->b(LX/0QB;)LX/6Wx;

    move-result-object v3

    check-cast v3, LX/5Pc;

    invoke-direct {p0, v3}, LX/7SW;-><init>(LX/5Pc;)V

    .line 1208944
    move-object v0, p0

    .line 1208945
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1208946
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/7SW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1208947
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1208948
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/1FJ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1FJ",
            "<",
            "LX/5Pb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1208931
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7SW;->b:LX/1FJ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7SW;->b:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1208932
    :cond_0
    iget-object v0, p0, LX/7SW;->a:LX/5Pc;

    const v1, 0x7f07004b

    const v2, 0x7f07004a

    invoke-interface {v0, v1, v2}, LX/5Pc;->a(II)LX/5Pb;

    move-result-object v0

    new-instance v1, LX/7SV;

    invoke-direct {v1, p0}, LX/7SV;-><init>(LX/7SW;)V

    invoke-static {v0, v1}, LX/1FJ;->a(Ljava/lang/Object;LX/1FN;)LX/1FJ;

    move-result-object v0

    iput-object v0, p0, LX/7SW;->b:LX/1FJ;

    .line 1208933
    :cond_1
    iget-object v0, p0, LX/7SW;->b:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->b()LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 1208934
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 1208927
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7SW;->b:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 1208928
    const/4 v0, 0x0

    iput-object v0, p0, LX/7SW;->b:LX/1FJ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1208929
    monitor-exit p0

    return-void

    .line 1208930
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
