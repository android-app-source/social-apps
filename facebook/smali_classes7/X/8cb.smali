.class public final LX/8cb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/7C3;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3Qn;


# direct methods
.method public constructor <init>(LX/3Qn;)V
    .locals 0

    .prologue
    .line 1374682
    iput-object p1, p0, LX/8cb;->a:LX/3Qn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1374683
    iget-object v0, p0, LX/8cb;->a:LX/3Qn;

    sget-object v1, LX/3Ql;->FETCH_DB_BOOTSTRAP_KEYWORD_PRE_FAIL:LX/3Ql;

    .line 1374684
    iput-object v1, v0, LX/3Qn;->q:LX/3Ql;

    .line 1374685
    iget-object v0, p0, LX/8cb;->a:LX/3Qn;

    iget-object v0, v0, LX/3Qn;->d:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_DB_BOOTSTRAP_KEYWORD_FAIL:LX/3Ql;

    invoke-virtual {v0, v1, p1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 1374686
    iget-object v0, p0, LX/8cb;->a:LX/3Qn;

    invoke-static {v0}, LX/3Qn;->c(LX/3Qn;)V

    .line 1374687
    iget-object v0, p0, LX/8cb;->a:LX/3Qn;

    iget-object v0, v0, LX/3Qn;->h:LX/3Qe;

    const-string v1, "time_to_load_bootstrap_keywords"

    invoke-virtual {v0, v1}, LX/3Qe;->d(Ljava/lang/String;)V

    .line 1374688
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1374689
    check-cast p1, LX/7C3;

    .line 1374690
    iget-object v0, p0, LX/8cb;->a:LX/3Qn;

    iget-object v0, v0, LX/3Qn;->h:LX/3Qe;

    const-string v1, "time_to_write_bootstrap_keywords"

    invoke-virtual {v0, v1}, LX/3Qe;->a(Ljava/lang/String;)V

    .line 1374691
    :try_start_0
    iget-object v0, p0, LX/8cb;->a:LX/3Qn;

    iget-object v0, v0, LX/3Qn;->l:LX/0Sy;

    invoke-virtual {v0}, LX/0Sy;->c()V

    .line 1374692
    iget-object v0, p0, LX/8cb;->a:LX/3Qn;

    iget-object v0, v0, LX/3Qn;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Bf;

    invoke-virtual {v0, p1}, LX/7Bf;->a(LX/7C3;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1374693
    iget-object v0, p0, LX/8cb;->a:LX/3Qn;

    invoke-static {v0}, LX/3Qn;->c(LX/3Qn;)V

    .line 1374694
    iget-object v0, p0, LX/8cb;->a:LX/3Qn;

    iget-object v0, v0, LX/3Qn;->h:LX/3Qe;

    const-string v1, "time_to_write_bootstrap_keywords"

    invoke-virtual {v0, v1}, LX/3Qe;->b(Ljava/lang/String;)V

    .line 1374695
    iget-object v0, p0, LX/8cb;->a:LX/3Qn;

    iget-object v0, v0, LX/3Qn;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 1374696
    sget-object v1, LX/3Qm;->d:LX/0Tn;

    iget-object v2, p0, LX/8cb;->a:LX/3Qn;

    invoke-static {v2}, LX/3Qn;->e(LX/3Qn;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1374697
    sget-object v1, LX/3Qm;->c:LX/0Tn;

    iget-object v2, p0, LX/8cb;->a:LX/3Qn;

    iget-object v2, v2, LX/3Qn;->j:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 1374698
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1374699
    iget-object v0, p0, LX/8cb;->a:LX/3Qn;

    iget-object v0, v0, LX/3Qn;->h:LX/3Qe;

    const-string v1, "time_to_load_bootstrap_keywords"

    invoke-virtual {v0, v1}, LX/3Qe;->c(Ljava/lang/String;)V

    .line 1374700
    :goto_0
    return-void

    .line 1374701
    :catch_0
    move-exception v0

    .line 1374702
    :try_start_1
    iget-object v1, p0, LX/8cb;->a:LX/3Qn;

    sget-object v2, LX/3Ql;->INSERT_DB_BOOTSTRAP_KEYWORD_FAIL:LX/3Ql;

    .line 1374703
    iput-object v2, v1, LX/3Qn;->q:LX/3Ql;

    .line 1374704
    iget-object v1, p0, LX/8cb;->a:LX/3Qn;

    iget-object v1, v1, LX/3Qn;->d:LX/2Sc;

    sget-object v2, LX/3Ql;->INSERT_DB_BOOTSTRAP_KEYWORD_FAIL:LX/3Ql;

    invoke-virtual {v1, v2, v0}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 1374705
    iget-object v1, p0, LX/8cb;->a:LX/3Qn;

    iget-object v1, v1, LX/3Qn;->h:LX/3Qe;

    const/4 v2, 0x0

    const-string v3, "time_to_write_bootstrap_keywords"

    invoke-virtual {v1, v2, v0, v3}, LX/3Qe;->a(ZLjava/lang/Exception;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1374706
    iget-object v0, p0, LX/8cb;->a:LX/3Qn;

    invoke-static {v0}, LX/3Qn;->c(LX/3Qn;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/8cb;->a:LX/3Qn;

    invoke-static {v1}, LX/3Qn;->c(LX/3Qn;)V

    throw v0
.end method
