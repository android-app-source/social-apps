.class public final enum LX/6oJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6oJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6oJ;

.field public static final enum EMPTY:LX/6oJ;

.field public static final enum INVALID:LX/6oJ;

.field public static final enum VALID:LX/6oJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1148081
    new-instance v0, LX/6oJ;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v2}, LX/6oJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6oJ;->EMPTY:LX/6oJ;

    .line 1148082
    new-instance v0, LX/6oJ;

    const-string v1, "VALID"

    invoke-direct {v0, v1, v3}, LX/6oJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6oJ;->VALID:LX/6oJ;

    .line 1148083
    new-instance v0, LX/6oJ;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v4}, LX/6oJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6oJ;->INVALID:LX/6oJ;

    .line 1148084
    const/4 v0, 0x3

    new-array v0, v0, [LX/6oJ;

    sget-object v1, LX/6oJ;->EMPTY:LX/6oJ;

    aput-object v1, v0, v2

    sget-object v1, LX/6oJ;->VALID:LX/6oJ;

    aput-object v1, v0, v3

    sget-object v1, LX/6oJ;->INVALID:LX/6oJ;

    aput-object v1, v0, v4

    sput-object v0, LX/6oJ;->$VALUES:[LX/6oJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1148085
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6oJ;
    .locals 1

    .prologue
    .line 1148086
    const-class v0, LX/6oJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6oJ;

    return-object v0
.end method

.method public static values()[LX/6oJ;
    .locals 1

    .prologue
    .line 1148087
    sget-object v0, LX/6oJ;->$VALUES:[LX/6oJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6oJ;

    return-object v0
.end method
