.class public final LX/74m;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:J

.field public c:Ljava/lang/String;

.field public d:J

.field public e:Lcom/facebook/ipc/media/data/LocalMediaData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:J

.field private i:Ljava/lang/String;

.field private j:LX/4gP;

.field private k:LX/4gN;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    .line 1168408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1168409
    iput-wide v0, p0, LX/74m;->h:J

    .line 1168410
    iput-wide v0, p0, LX/74m;->a:J

    .line 1168411
    iput-wide v0, p0, LX/74m;->b:J

    .line 1168412
    iput-object v2, p0, LX/74m;->c:Ljava/lang/String;

    .line 1168413
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/74m;->d:J

    .line 1168414
    iput-object v2, p0, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1168415
    iput-object v2, p0, LX/74m;->f:Ljava/lang/String;

    .line 1168416
    iput-object v2, p0, LX/74m;->g:Ljava/lang/String;

    .line 1168417
    const-string v0, ""

    iput-object v0, p0, LX/74m;->i:Ljava/lang/String;

    .line 1168418
    new-instance v0, LX/4gP;

    invoke-direct {v0}, LX/4gP;-><init>()V

    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v0

    sget-object v1, LX/4gQ;->Video:LX/4gQ;

    invoke-virtual {v0, v1}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v0

    iput-object v0, p0, LX/74m;->j:LX/4gP;

    .line 1168419
    new-instance v0, LX/4gN;

    invoke-direct {v0}, LX/4gN;-><init>()V

    iput-object v0, p0, LX/74m;->k:LX/4gN;

    .line 1168420
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/base/media/VideoItem;
    .locals 6

    .prologue
    .line 1168403
    iget-object v0, p0, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    if-nez v0, :cond_0

    .line 1168404
    iget-object v0, p0, LX/74m;->j:LX/4gP;

    new-instance v1, Lcom/facebook/ipc/media/MediaIdKey;

    iget-object v2, p0, LX/74m;->i:Ljava/lang/String;

    iget-wide v4, p0, LX/74m;->h:J

    invoke-direct {v1, v2, v4, v5}, Lcom/facebook/ipc/media/MediaIdKey;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    .line 1168405
    iget-object v0, p0, LX/74m;->j:LX/4gP;

    invoke-virtual {v0}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1168406
    iget-object v1, p0, LX/74m;->k:LX/4gN;

    invoke-virtual {v1, v0}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v0

    invoke-virtual {v0}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v0

    iput-object v0, p0, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1168407
    :cond_0
    new-instance v0, Lcom/facebook/photos/base/media/VideoItem;

    invoke-direct {v0, p0}, Lcom/facebook/photos/base/media/VideoItem;-><init>(LX/74m;)V

    return-object v0
.end method

.method public final c(Ljava/lang/String;)LX/74m;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1168400
    iget-object v0, p0, LX/74m;->j:LX/4gP;

    invoke-static {p1}, LX/74n;->c(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    .line 1168401
    iput-object p1, p0, LX/74m;->i:Ljava/lang/String;

    .line 1168402
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/74m;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1168398
    iget-object v0, p0, LX/74m;->j:LX/4gP;

    invoke-static {p1}, Lcom/facebook/ipc/media/data/MimeType;->a(Ljava/lang/String;)Lcom/facebook/ipc/media/data/MimeType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4gP;->a(Lcom/facebook/ipc/media/data/MimeType;)LX/4gP;

    .line 1168399
    return-object p0
.end method
