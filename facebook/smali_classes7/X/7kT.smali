.class public final LX/7kT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1231963
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1231964
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1231965
    :goto_0
    return v1

    .line 1231966
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1231967
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1231968
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1231969
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1231970
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1231971
    const-string v4, "catalog_items"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1231972
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1231973
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_b

    .line 1231974
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1231975
    :goto_2
    move v2, v3

    .line 1231976
    goto :goto_1

    .line 1231977
    :cond_2
    const-string v4, "id"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1231978
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1231979
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1231980
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1231981
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1231982
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1231983
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_9

    .line 1231984
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1231985
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1231986
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_5

    if-eqz v7, :cond_5

    .line 1231987
    const-string v8, "count"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1231988
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    move v6, v2

    move v2, v4

    goto :goto_3

    .line 1231989
    :cond_6
    const-string v8, "edges"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1231990
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1231991
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_7

    .line 1231992
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_7

    .line 1231993
    const/4 v8, 0x0

    .line 1231994
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_10

    .line 1231995
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1231996
    :goto_5
    move v7, v8

    .line 1231997
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1231998
    :cond_7
    invoke-static {v5, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 1231999
    goto :goto_3

    .line 1232000
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1232001
    :cond_9
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1232002
    if-eqz v2, :cond_a

    .line 1232003
    invoke-virtual {p1, v3, v6, v3}, LX/186;->a(III)V

    .line 1232004
    :cond_a
    invoke-virtual {p1, v4, v5}, LX/186;->b(II)V

    .line 1232005
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_b
    move v2, v3

    move v5, v3

    move v6, v3

    goto :goto_3

    .line 1232006
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1232007
    :cond_d
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_f

    .line 1232008
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1232009
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1232010
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_d

    if-eqz v10, :cond_d

    .line 1232011
    const-string v11, "cursor"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 1232012
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_6

    .line 1232013
    :cond_e
    const-string v11, "node"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 1232014
    invoke-static {p0, p1}, LX/7jm;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_6

    .line 1232015
    :cond_f
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1232016
    invoke-virtual {p1, v8, v9}, LX/186;->b(II)V

    .line 1232017
    const/4 v8, 0x1

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1232018
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto :goto_5

    :cond_10
    move v7, v8

    move v9, v8

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1232019
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1232020
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1232021
    if-eqz v0, :cond_5

    .line 1232022
    const-string v1, "catalog_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1232023
    const/4 v1, 0x0

    .line 1232024
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1232025
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1232026
    if-eqz v1, :cond_0

    .line 1232027
    const-string v2, "count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1232028
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 1232029
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1232030
    if-eqz v1, :cond_4

    .line 1232031
    const-string v2, "edges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1232032
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1232033
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 1232034
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    .line 1232035
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1232036
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1232037
    if-eqz v4, :cond_1

    .line 1232038
    const-string v0, "cursor"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1232039
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1232040
    :cond_1
    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1232041
    if-eqz v4, :cond_2

    .line 1232042
    const-string v0, "node"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1232043
    invoke-static {p0, v4, p2, p3}, LX/7jm;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1232044
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1232045
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1232046
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1232047
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1232048
    :cond_5
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1232049
    if-eqz v0, :cond_6

    .line 1232050
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1232051
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1232052
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1232053
    return-void
.end method
