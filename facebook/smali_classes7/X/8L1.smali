.class public final LX/8L1;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "LX/8Kl;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8L4;


# direct methods
.method public constructor <init>(LX/8L4;)V
    .locals 0

    .prologue
    .line 1331076
    iput-object p1, p0, LX/8L1;->a:LX/8L4;

    invoke-direct {p0}, LX/8KR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/8Kl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1331077
    const-class v0, LX/8Kl;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 1331078
    check-cast p1, LX/8Kl;

    .line 1331079
    new-instance v0, LX/8KU;

    iget-object v1, p0, LX/8L1;->a:LX/8L4;

    iget-object v1, v1, LX/8L4;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/8KU;-><init>(Landroid/content/Context;)V

    const-string v1, "too_slow_request"

    .line 1331080
    iput-object v1, v0, LX/8KU;->b:Ljava/lang/String;

    .line 1331081
    move-object v0, v0

    .line 1331082
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1331083
    iput-object v1, v0, LX/8KU;->c:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 1331084
    move-object v0, v0

    .line 1331085
    iget-wide v4, p1, LX/8Kl;->a:J

    move-wide v2, v4

    .line 1331086
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 1331087
    iput-object v1, v0, LX/8KU;->e:Ljava/lang/Long;

    .line 1331088
    move-object v0, v0

    .line 1331089
    invoke-virtual {v0}, LX/8KU;->a()Landroid/content/Intent;

    move-result-object v0

    .line 1331090
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1331091
    iget-object v1, p0, LX/8L1;->a:LX/8L4;

    iget-object v1, v1, LX/8L4;->i:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/8L1;->a:LX/8L4;

    iget-object v2, v2, LX/8L4;->b:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1331092
    return-void
.end method
