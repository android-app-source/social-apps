.class public LX/7Df;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7D0;


# static fields
.field public static a:LX/0SG;


# instance fields
.field private final b:LX/5Pc;

.field public final c:LX/7Dc;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/7D7;",
            "LX/7De;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7D7;",
            ">;"
        }
    .end annotation
.end field

.field private final f:[LX/7Db;

.field private final g:[[F

.field private final h:[[F

.field private final i:[F

.field private final j:[F

.field private final k:[F

.field private final l:[F

.field private m:LX/5Pb;

.field public n:LX/7Da;

.field private o:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

.field public p:LX/7DR;

.field public q:LX/7DD;

.field private r:[F

.field public s:Z

.field public t:F

.field public u:I

.field public v:I

.field private w:J

.field private x:F

.field private y:I

.field public z:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0SG;I)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/16 v4, 0x10

    const/4 v1, 0x0

    const/4 v3, 0x4

    .line 1183483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1183484
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7Df;->d:Ljava/util/Map;

    .line 1183485
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7Df;->e:Ljava/util/List;

    .line 1183486
    const/4 v0, 0x6

    new-array v0, v0, [LX/7Db;

    iput-object v0, p0, LX/7Df;->f:[LX/7Db;

    .line 1183487
    filled-new-array {v3, v3}, [I

    move-result-object v0

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[F

    iput-object v0, p0, LX/7Df;->g:[[F

    .line 1183488
    filled-new-array {v3, v3}, [I

    move-result-object v0

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[F

    iput-object v0, p0, LX/7Df;->h:[[F

    .line 1183489
    new-array v0, v4, [F

    iput-object v0, p0, LX/7Df;->i:[F

    .line 1183490
    new-array v0, v4, [F

    iput-object v0, p0, LX/7Df;->j:[F

    .line 1183491
    new-array v0, v4, [F

    iput-object v0, p0, LX/7Df;->k:[F

    .line 1183492
    new-array v0, v3, [F

    iput-object v0, p0, LX/7Df;->l:[F

    .line 1183493
    new-instance v0, LX/7DD;

    invoke-direct {v0}, LX/7DD;-><init>()V

    iput-object v0, p0, LX/7Df;->q:LX/7DD;

    .line 1183494
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, LX/7Df;->t:F

    .line 1183495
    iput v5, p0, LX/7Df;->u:I

    .line 1183496
    iput v5, p0, LX/7Df;->v:I

    .line 1183497
    sput-object p2, LX/7Df;->a:LX/0SG;

    .line 1183498
    new-instance v0, LX/5Pd;

    invoke-direct {v0, p1}, LX/5Pd;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, LX/7Df;->b:LX/5Pc;

    move v0, v1

    .line 1183499
    :goto_0
    iget-object v2, p0, LX/7Df;->f:[LX/7Db;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1183500
    iget-object v2, p0, LX/7Df;->f:[LX/7Db;

    new-instance v3, LX/7Db;

    invoke-direct {v3}, LX/7Db;-><init>()V

    aput-object v3, v2, v0

    .line 1183501
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1183502
    :cond_0
    new-instance v0, LX/7Dc;

    invoke-direct {v0, p3}, LX/7Dc;-><init>(I)V

    iput-object v0, p0, LX/7Df;->c:LX/7Dc;

    .line 1183503
    return-void
.end method

.method private a(LX/7D7;)F
    .locals 14

    .prologue
    const/16 v7, 0xde1

    .line 1183447
    invoke-static {p0, p1}, LX/7Df;->b(LX/7Df;LX/7D7;)LX/7Dd;

    move-result-object v0

    .line 1183448
    iget-object v1, p0, LX/7Df;->c:LX/7Dc;

    .line 1183449
    iget-object v3, v1, LX/7Dc;->a:Ljava/util/Map;

    .line 1183450
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7D7;

    .line 1183451
    iget v5, p1, LX/7D7;->b:I

    move v5, v5

    .line 1183452
    iget v6, v2, LX/7D7;->b:I

    move v6, v6

    .line 1183453
    if-ne v5, v6, :cond_0

    .line 1183454
    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7Dd;

    .line 1183455
    :goto_0
    move-object v1, v2

    .line 1183456
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 1183457
    iget-object v2, v0, LX/7Dd;->a:LX/7D7;

    .line 1183458
    new-instance v3, LX/5PQ;

    const/16 v4, 0xc

    invoke-direct {v3, v4}, LX/5PQ;-><init>(I)V

    const/4 v4, 0x4

    .line 1183459
    iput v4, v3, LX/5PQ;->a:I

    .line 1183460
    move-object v3, v3

    .line 1183461
    const-string v4, "aPosition"

    invoke-static {p1}, LX/7D6;->a(LX/7D7;)LX/5Pg;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v3

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x1

    .line 1183462
    const/4 v4, 0x6

    new-array v4, v4, [S

    .line 1183463
    aput-short v8, v4, v8

    .line 1183464
    aput-short v6, v4, v6

    .line 1183465
    aput-short v5, v4, v5

    .line 1183466
    aput-short v5, v4, v9

    .line 1183467
    const/4 v5, 0x4

    aput-short v6, v4, v5

    .line 1183468
    const/4 v5, 0x5

    aput-short v9, v4, v5

    .line 1183469
    new-instance v5, LX/5PY;

    invoke-direct {v5, v4}, LX/5PY;-><init>([S)V

    move-object v4, v5

    .line 1183470
    invoke-virtual {v3, v4}, LX/5PQ;->a(LX/5PY;)LX/5PQ;

    move-result-object v3

    const-string v4, "aTextureCoord"

    invoke-static {p1, v2}, LX/7D6;->b(LX/7D7;LX/7D7;)LX/5Pg;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v3

    const-string v4, "aBGTextureCoord"

    const/4 v8, 0x0

    .line 1183471
    new-instance v5, LX/7D7;

    .line 1183472
    iget v6, p1, LX/7D7;->b:I

    move v6, v6

    .line 1183473
    invoke-direct {v5, v8, v6, v8, v8}, LX/7D7;-><init>(IIII)V

    invoke-static {p1, v5}, LX/7D6;->b(LX/7D7;LX/7D7;)LX/5Pg;

    move-result-object v5

    move-object v5, v5

    .line 1183474
    invoke-virtual {v3, v4, v5}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v3

    invoke-virtual {v3}, LX/5PQ;->a()LX/5PR;

    move-result-object v3

    move-object v2, v3

    .line 1183475
    iget-object v3, p0, LX/7Df;->m:LX/5Pb;

    invoke-virtual {v3}, LX/5Pb;->a()LX/5Pa;

    move-result-object v3

    const-string v4, "sTexture"

    const/4 v5, 0x0

    iget v6, v0, LX/7Dd;->b:I

    invoke-virtual {v3, v4, v5, v7, v6}, LX/5Pa;->a(Ljava/lang/String;III)LX/5Pa;

    move-result-object v3

    const-string v4, "sBGTexture"

    const/4 v5, 0x1

    iget v1, v1, LX/7Dd;->b:I

    invoke-virtual {v3, v4, v5, v7, v1}, LX/5Pa;->a(Ljava/lang/String;III)LX/5Pa;

    move-result-object v1

    const-string v3, "uMVPMatrix"

    iget-object v4, p0, LX/7Df;->i:[F

    invoke-virtual {v1, v3, v4}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    .line 1183476
    iget-object v0, v0, LX/7Dd;->a:LX/7D7;

    .line 1183477
    iget v1, v0, LX/7D7;->a:I

    move v0, v1

    .line 1183478
    iget v8, p0, LX/7Df;->v:I

    int-to-float v8, v8

    iget v9, p0, LX/7Df;->t:F

    div-float/2addr v8, v9

    .line 1183479
    const/high16 v9, 0x42b40000    # 90.0f

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    int-to-double v12, v0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    double-to-float v10, v10

    div-float/2addr v9, v10

    .line 1183480
    const/high16 v10, 0x43ff0000    # 510.0f

    div-float v9, v10, v9

    .line 1183481
    div-float v8, v9, v8

    move v0, v8

    .line 1183482
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/7D7;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1183428
    sget-object v0, LX/7Df;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 1183429
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7D7;

    move v7, v6

    .line 1183430
    :goto_0
    iget v1, v0, LX/7D7;->a:I

    move v1, v1

    .line 1183431
    if-ge v7, v1, :cond_0

    .line 1183432
    invoke-virtual {v0, v7}, LX/7D7;->a(I)LX/7D7;

    move-result-object v9

    .line 1183433
    iget-object v1, p0, LX/7Df;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7De;

    .line 1183434
    if-eqz v1, :cond_1

    .line 1183435
    iget-wide v10, v1, LX/7De;->a:J

    sub-long v10, v2, v10

    iput-wide v10, v1, LX/7De;->b:J

    .line 1183436
    :goto_1
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    .line 1183437
    goto :goto_0

    .line 1183438
    :cond_1
    new-instance v1, LX/7De;

    const-wide/16 v4, 0x3e8

    invoke-direct/range {v1 .. v5}, LX/7De;-><init>(JJ)V

    .line 1183439
    iget-object v4, p0, LX/7Df;->d:Ljava/util/Map;

    invoke-interface {v4, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1183440
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1183441
    iget-object v0, p0, LX/7Df;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7D7;

    .line 1183442
    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1183443
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1183444
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7D7;

    .line 1183445
    iget-object v2, p0, LX/7Df;->d:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 1183446
    :cond_5
    return-void
.end method

.method private static a([FF[FF)[F
    .locals 5

    .prologue
    .line 1183413
    neg-float v0, p1

    sub-float v1, p3, p1

    div-float/2addr v0, v1

    .line 1183414
    const/4 v1, 0x4

    new-array v1, v1, [F

    .line 1183415
    const/4 p3, 0x2

    const/4 p1, 0x1

    const/4 v4, 0x0

    .line 1183416
    aget v2, p2, v4

    aget v3, p0, v4

    sub-float/2addr v2, v3

    aput v2, v1, v4

    .line 1183417
    aget v2, p2, p1

    aget v3, p0, p1

    sub-float/2addr v2, v3

    aput v2, v1, p1

    .line 1183418
    aget v2, p2, p3

    aget v3, p0, p3

    sub-float/2addr v2, v3

    aput v2, v1, p3

    .line 1183419
    const/4 p1, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1183420
    aget v2, v1, v3

    mul-float/2addr v2, v0

    aput v2, v1, v3

    .line 1183421
    aget v2, v1, v4

    mul-float/2addr v2, v0

    aput v2, v1, v4

    .line 1183422
    aget v2, v1, p1

    mul-float/2addr v2, v0

    aput v2, v1, p1

    .line 1183423
    const/4 p1, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1183424
    aget v0, v1, v3

    aget v2, p0, v3

    add-float/2addr v0, v2

    aput v0, v1, v3

    .line 1183425
    aget v0, v1, v4

    aget v2, p0, v4

    add-float/2addr v0, v2

    aput v0, v1, v4

    .line 1183426
    aget v0, v1, p1

    aget v2, p0, p1

    add-float/2addr v0, v2

    aput v0, v1, p1

    .line 1183427
    return-object v1
.end method

.method private static a(LX/7Df;[[F[[F)[[F
    .locals 13

    .prologue
    const/4 v5, 0x1

    const/4 v12, 0x0

    const/4 v3, 0x0

    .line 1183384
    array-length v0, p2

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p2, v0

    move v2, v3

    .line 1183385
    :goto_0
    array-length v1, p2

    if-ge v2, v1, :cond_6

    .line 1183386
    aget-object v4, p2, v2

    .line 1183387
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1183388
    iget-object v1, p0, LX/7Df;->l:[F

    invoke-static {v1, v0, v4}, LX/7E4;->c([F[F[F)V

    .line 1183389
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-object v6, p1, v0

    .line 1183390
    iget-object v0, p0, LX/7Df;->l:[F

    invoke-static {v0, v6}, LX/7E4;->a([F[F)F

    move-result v1

    .line 1183391
    cmpl-float v0, v1, v12

    if-ltz v0, :cond_2

    move v0, v5

    :goto_1
    move v7, v1

    move-object v9, v6

    move v1, v0

    move v0, v3

    .line 1183392
    :goto_2
    array-length v6, p1

    if-ge v0, v6, :cond_4

    .line 1183393
    aget-object v10, p1, v0

    .line 1183394
    iget-object v6, p0, LX/7Df;->l:[F

    invoke-static {v6, v10}, LX/7E4;->a([F[F)F

    move-result v8

    .line 1183395
    cmpl-float v6, v8, v12

    if-ltz v6, :cond_3

    move v6, v5

    .line 1183396
    :goto_3
    if-eq v6, v1, :cond_0

    .line 1183397
    invoke-static {v9, v7, v10, v8}, LX/7Df;->a([FF[FF)[F

    move-result-object v1

    .line 1183398
    invoke-static {v9, v1}, LX/7E4;->b([F[F)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {v10, v1}, LX/7E4;->b([F[F)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1183399
    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1183400
    :cond_0
    if-eqz v6, :cond_1

    .line 1183401
    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1183402
    :cond_1
    add-int/lit8 v0, v0, 0x1

    move v1, v6

    move v7, v8

    move-object v9, v10

    goto :goto_2

    :cond_2
    move v0, v3

    .line 1183403
    goto :goto_1

    :cond_3
    move v6, v3

    .line 1183404
    goto :goto_3

    .line 1183405
    :cond_4
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x4

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[F

    move v6, v3

    .line 1183406
    :goto_4
    array-length v1, v0

    if-ge v6, v1, :cond_5

    .line 1183407
    invoke-interface {v11, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [F

    aput-object v1, v0, v6

    .line 1183408
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_4

    .line 1183409
    :cond_5
    array-length v1, v0

    if-nez v1, :cond_7

    .line 1183410
    const/4 p1, 0x0

    .line 1183411
    :cond_6
    return-object p1

    .line 1183412
    :cond_7
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object p1, v0

    move-object v0, v4

    goto/16 :goto_0
.end method

.method private static b(LX/7Df;LX/7D7;)LX/7Dd;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1183357
    iget-object v0, p0, LX/7Df;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7De;

    .line 1183358
    iget-wide v4, v0, LX/7De;->b:J

    iget-wide v6, v0, LX/7De;->c:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_5

    const/4 v4, 0x1

    :goto_0
    move v2, v4

    .line 1183359
    if-eqz v2, :cond_1

    iget-boolean v2, v0, LX/7De;->d:Z

    if-nez v2, :cond_1

    const/4 v2, 0x1

    .line 1183360
    :goto_1
    if-eqz v2, :cond_0

    .line 1183361
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/7De;->d:Z

    .line 1183362
    :cond_0
    move v0, v1

    .line 1183363
    :goto_2
    iget v1, p1, LX/7D7;->a:I

    move v1, v1

    .line 1183364
    if-gt v0, v1, :cond_4

    .line 1183365
    invoke-virtual {p1, v0}, LX/7D7;->a(I)LX/7D7;

    move-result-object v1

    .line 1183366
    iget-object v3, p0, LX/7Df;->c:LX/7Dc;

    invoke-static {v3, v1}, LX/7Dc;->a(LX/7Dc;LX/7D7;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1183367
    iget-object v0, p0, LX/7Df;->c:LX/7Dc;

    .line 1183368
    iget v4, v1, LX/7D7;->a:I

    move v4, v4

    .line 1183369
    invoke-static {v0, v4}, LX/7Dc;->a(LX/7Dc;I)Ljava/util/Map;

    move-result-object v5

    .line 1183370
    if-eqz v5, :cond_6

    .line 1183371
    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/7Dd;

    .line 1183372
    if-eqz v4, :cond_6

    .line 1183373
    sget-object v6, LX/7Df;->a:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    .line 1183374
    iput-wide v6, v4, LX/7Dd;->c:J

    .line 1183375
    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/7Dd;

    .line 1183376
    :goto_3
    move-object v0, v4

    .line 1183377
    :goto_4
    return-object v0

    :cond_1
    move v2, v1

    .line 1183378
    goto :goto_1

    .line 1183379
    :cond_2
    if-eqz v2, :cond_3

    .line 1183380
    iget-object v3, p0, LX/7Df;->n:LX/7Da;

    invoke-virtual {v3, v1}, LX/7Da;->a(LX/7D7;)V

    .line 1183381
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 1183382
    goto :goto_2

    .line 1183383
    :cond_4
    const/4 v0, 0x0

    goto :goto_4

    :cond_5
    const/4 v4, 0x0

    goto :goto_0

    :cond_6
    const/4 v4, 0x0

    goto :goto_3
.end method

.method private g()V
    .locals 13

    .prologue
    const/high16 v12, 0x3f800000    # 1.0f

    .line 1183327
    iget-object v0, p0, LX/7Df;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1183328
    const v6, 0x44d48000    # 1700.0f

    .line 1183329
    iget v2, p0, LX/7Df;->t:F

    .line 1183330
    iget v0, p0, LX/7Df;->u:I

    int-to-float v0, v0

    mul-float/2addr v0, v2

    iget v1, p0, LX/7Df;->v:I

    int-to-float v1, v1

    div-float v3, v0, v1

    .line 1183331
    iget v0, p0, LX/7Df;->v:I

    int-to-float v0, v0

    div-float v4, v0, v2

    .line 1183332
    const/4 v1, 0x0

    .line 1183333
    const v0, 0x40b55555

    .line 1183334
    :goto_0
    const v5, 0x3fb33333    # 1.4f

    mul-float/2addr v5, v0

    cmpg-float v5, v5, v4

    if-gez v5, :cond_0

    mul-float v5, v0, v2

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    mul-float v5, v0, v3

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    .line 1183335
    add-int/lit8 v1, v1, 0x1

    .line 1183336
    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v0, v5

    goto :goto_0

    .line 1183337
    :cond_0
    iget-object v0, p0, LX/7Df;->n:LX/7Da;

    .line 1183338
    iget v2, v0, LX/7Da;->g:I

    move v0, v2

    .line 1183339
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v4, v0

    .line 1183340
    invoke-static {p0}, LX/7Df;->h(LX/7Df;)V

    .line 1183341
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    int-to-double v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v5, v0

    .line 1183342
    const/high16 v0, 0x40000000    # 2.0f

    div-float v6, v0, v5

    .line 1183343
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, LX/7Df;->f:[LX/7Db;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 1183344
    iget-object v1, p0, LX/7Df;->f:[LX/7Db;

    aget-object v3, v1, v0

    .line 1183345
    iget-boolean v1, v3, LX/7Db;->a:Z

    if-eqz v1, :cond_2

    .line 1183346
    iget v1, v3, LX/7Db;->b:F

    div-float/2addr v1, v6

    float-to-double v8, v1

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-int v1, v8

    .line 1183347
    iget v2, v3, LX/7Db;->c:F

    div-float/2addr v2, v6

    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    sub-float v2, v5, v12

    float-to-double v10, v2

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(DD)D

    move-result-wide v8

    double-to-int v7, v8

    .line 1183348
    iget v2, v3, LX/7Db;->d:F

    div-float/2addr v2, v6

    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-int v2, v8

    .line 1183349
    iget v3, v3, LX/7Db;->e:F

    div-float/2addr v3, v6

    float-to-double v8, v3

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    sub-float v3, v5, v12

    float-to-double v10, v3

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(DD)D

    move-result-wide v8

    double-to-int v8, v8

    move v3, v1

    .line 1183350
    :goto_2
    if-gt v3, v7, :cond_2

    move v1, v2

    .line 1183351
    :goto_3
    if-gt v1, v8, :cond_1

    .line 1183352
    iget-object v9, p0, LX/7Df;->e:Ljava/util/List;

    new-instance v10, LX/7D7;

    invoke-direct {v10, v4, v0, v3, v1}, LX/7D7;-><init>(IIII)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1183353
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1183354
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 1183355
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1183356
    :cond_3
    return-void
.end method

.method private static h(LX/7Df;)V
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1183293
    invoke-static {p0}, LX/7Df;->i(LX/7Df;)V

    move v7, v1

    .line 1183294
    :goto_0
    const/4 v0, 0x6

    if-ge v7, v0, :cond_3

    .line 1183295
    iget-object v0, p0, LX/7Df;->f:[LX/7Db;

    aget-object v0, v0, v7

    iput-boolean v1, v0, LX/7Db;->a:Z

    .line 1183296
    sget-object v0, LX/7E4;->b:[[[F

    aget-object v3, v0, v7

    move v0, v1

    .line 1183297
    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_0

    .line 1183298
    iget-object v4, p0, LX/7Df;->h:[[F

    aget-object v4, v4, v0

    aget-object v5, v3, v0

    aget v5, v5, v1

    aput v5, v4, v1

    .line 1183299
    iget-object v4, p0, LX/7Df;->h:[[F

    aget-object v4, v4, v0

    aget-object v5, v3, v0

    aget v5, v5, v2

    aput v5, v4, v2

    .line 1183300
    iget-object v4, p0, LX/7Df;->h:[[F

    aget-object v4, v4, v0

    aget-object v5, v3, v0

    aget v5, v5, v12

    aput v5, v4, v12

    .line 1183301
    iget-object v4, p0, LX/7Df;->h:[[F

    aget-object v4, v4, v0

    aget-object v5, v3, v0

    aget v5, v5, v13

    aput v5, v4, v13

    .line 1183302
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1183303
    :cond_0
    iget-object v0, p0, LX/7Df;->h:[[F

    iget-object v3, p0, LX/7Df;->g:[[F

    invoke-static {p0, v0, v3}, LX/7Df;->a(LX/7Df;[[F[[F)[[F

    move-result-object v8

    .line 1183304
    if-eqz v8, :cond_2

    .line 1183305
    iget-object v0, p0, LX/7Df;->k:[F

    sget-object v3, LX/7E4;->a:[[F

    aget-object v3, v3, v7

    invoke-static {v0, v1, v3, v1}, Landroid/opengl/Matrix;->invertM([FI[FI)Z

    .line 1183306
    iget-object v0, p0, LX/7Df;->k:[F

    invoke-static {v0, v8}, LX/7E4;->a([F[[F)V

    .line 1183307
    aget-object v0, v8, v1

    .line 1183308
    aget v5, v0, v1

    .line 1183309
    aget v4, v0, v1

    .line 1183310
    aget v3, v0, v2

    .line 1183311
    aget v0, v0, v2

    move v6, v5

    move v5, v4

    move v4, v3

    move v3, v0

    move v0, v2

    .line 1183312
    :goto_2
    array-length v9, v8

    if-ge v0, v9, :cond_1

    .line 1183313
    aget-object v9, v8, v0

    .line 1183314
    aget v10, v9, v1

    invoke-static {v6, v10}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 1183315
    aget v10, v9, v1

    invoke-static {v5, v10}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 1183316
    aget v10, v9, v2

    invoke-static {v4, v10}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 1183317
    aget v9, v9, v2

    invoke-static {v3, v9}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 1183318
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1183319
    :cond_1
    iget-object v0, p0, LX/7Df;->f:[LX/7Db;

    aget-object v0, v0, v7

    add-float/2addr v6, v11

    add-float/2addr v5, v11

    sub-float v3, v11, v3

    sub-float v4, v11, v4

    .line 1183320
    const/4 v8, 0x1

    iput-boolean v8, v0, LX/7Db;->a:Z

    .line 1183321
    iput v6, v0, LX/7Db;->b:F

    .line 1183322
    iput v5, v0, LX/7Db;->c:F

    .line 1183323
    iput v3, v0, LX/7Db;->d:F

    .line 1183324
    iput v4, v0, LX/7Db;->e:F

    .line 1183325
    :cond_2
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_0

    .line 1183326
    :cond_3
    return-void
.end method

.method private static i(LX/7Df;)V
    .locals 9

    .prologue
    const/high16 v8, -0x40800000    # -1.0f

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1183270
    iget v0, p0, LX/7Df;->t:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1183271
    iget v1, p0, LX/7Df;->u:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iget v2, p0, LX/7Df;->v:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    const v2, 0x3f8ccccd    # 1.1f

    mul-float/2addr v1, v2

    .line 1183272
    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v1, v2

    .line 1183273
    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v0, v2

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v0, v2

    .line 1183274
    iget-object v2, p0, LX/7Df;->g:[[F

    aget-object v2, v2, v4

    neg-float v3, v1

    aput v3, v2, v4

    .line 1183275
    iget-object v2, p0, LX/7Df;->g:[[F

    aget-object v2, v2, v4

    aput v0, v2, v5

    .line 1183276
    iget-object v2, p0, LX/7Df;->g:[[F

    aget-object v2, v2, v4

    aput v8, v2, v6

    .line 1183277
    iget-object v2, p0, LX/7Df;->g:[[F

    aget-object v2, v2, v4

    const/4 v3, 0x0

    aput v3, v2, v7

    .line 1183278
    iget-object v2, p0, LX/7Df;->g:[[F

    aget-object v2, v2, v5

    aput v1, v2, v4

    .line 1183279
    iget-object v2, p0, LX/7Df;->g:[[F

    aget-object v2, v2, v5

    aput v0, v2, v5

    .line 1183280
    iget-object v2, p0, LX/7Df;->g:[[F

    aget-object v2, v2, v5

    aput v8, v2, v6

    .line 1183281
    iget-object v2, p0, LX/7Df;->g:[[F

    aget-object v2, v2, v5

    const/4 v3, 0x0

    aput v3, v2, v7

    .line 1183282
    iget-object v2, p0, LX/7Df;->g:[[F

    aget-object v2, v2, v6

    aput v1, v2, v4

    .line 1183283
    iget-object v2, p0, LX/7Df;->g:[[F

    aget-object v2, v2, v6

    neg-float v3, v0

    aput v3, v2, v5

    .line 1183284
    iget-object v2, p0, LX/7Df;->g:[[F

    aget-object v2, v2, v6

    aput v8, v2, v6

    .line 1183285
    iget-object v2, p0, LX/7Df;->g:[[F

    aget-object v2, v2, v6

    const/4 v3, 0x0

    aput v3, v2, v7

    .line 1183286
    iget-object v2, p0, LX/7Df;->g:[[F

    aget-object v2, v2, v7

    neg-float v1, v1

    aput v1, v2, v4

    .line 1183287
    iget-object v1, p0, LX/7Df;->g:[[F

    aget-object v1, v1, v7

    neg-float v0, v0

    aput v0, v1, v5

    .line 1183288
    iget-object v0, p0, LX/7Df;->g:[[F

    aget-object v0, v0, v7

    aput v8, v0, v6

    .line 1183289
    iget-object v0, p0, LX/7Df;->g:[[F

    aget-object v0, v0, v7

    const/4 v1, 0x0

    aput v1, v0, v7

    .line 1183290
    iget-object v0, p0, LX/7Df;->j:[F

    iget-object v1, p0, LX/7Df;->r:[F

    invoke-static {v0, v4, v1, v4}, Landroid/opengl/Matrix;->invertM([FI[FI)Z

    .line 1183291
    iget-object v0, p0, LX/7Df;->j:[F

    iget-object v1, p0, LX/7Df;->g:[[F

    invoke-static {v0, v1}, LX/7E4;->a([F[[F)V

    .line 1183292
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1183269
    const/4 v0, 0x0

    return v0
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 1183194
    iput p1, p0, LX/7Df;->t:F

    .line 1183195
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 1183268
    return-void
.end method

.method public final a(LX/19o;)V
    .locals 0

    .prologue
    .line 1183267
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 1183266
    return-void
.end method

.method public final a(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)V
    .locals 0

    .prologue
    .line 1183265
    return-void
.end method

.method public final a(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;)V
    .locals 0

    .prologue
    .line 1183263
    iput-object p1, p0, LX/7Df;->o:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    .line 1183264
    return-void
.end method

.method public final a([F[F[FII)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1183240
    iget v0, p0, LX/7Df;->t:F

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7Df;->n:LX/7Da;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7Df;->c:LX/7Dc;

    invoke-static {v0}, LX/7Dc;->a(LX/7Dc;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1183241
    :cond_0
    :goto_0
    return-void

    .line 1183242
    :cond_1
    iget v0, p0, LX/7Df;->y:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/7Df;->y:I

    .line 1183243
    sget-object v0, LX/7Df;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 1183244
    iget v0, p0, LX/7Df;->u:I

    if-ne p4, v0, :cond_2

    iget v0, p0, LX/7Df;->v:I

    if-eq p5, v0, :cond_3

    .line 1183245
    :cond_2
    invoke-static {v1, v1, p4, p5}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1183246
    iput p4, p0, LX/7Df;->u:I

    .line 1183247
    iput p5, p0, LX/7Df;->v:I

    .line 1183248
    :cond_3
    iput-object p1, p0, LX/7Df;->r:[F

    .line 1183249
    iget-object v0, p0, LX/7Df;->i:[F

    move-object v2, p2

    move v3, v1

    move-object v4, p1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1183250
    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 1183251
    invoke-direct {p0}, LX/7Df;->g()V

    .line 1183252
    iget-object v0, p0, LX/7Df;->e:Ljava/util/List;

    invoke-direct {p0, v0}, LX/7Df;->a(Ljava/util/List;)V

    .line 1183253
    const/4 v0, 0x0

    .line 1183254
    iget-object v1, p0, LX/7Df;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7D7;

    .line 1183255
    invoke-direct {p0, v0}, LX/7Df;->a(LX/7D7;)F

    move-result v0

    add-float/2addr v0, v1

    move v1, v0

    .line 1183256
    goto :goto_1

    .line 1183257
    :cond_4
    iget-object v0, p0, LX/7Df;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/7Df;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-float v0, v0

    .line 1183258
    :goto_2
    div-float v0, v1, v0

    .line 1183259
    iget v1, p0, LX/7Df;->x:F

    add-float/2addr v0, v1

    iput v0, p0, LX/7Df;->x:F

    .line 1183260
    iget-wide v0, p0, LX/7Df;->w:J

    sget-object v2, LX/7Df;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v2, v6

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/7Df;->w:J

    .line 1183261
    iget-object v0, p0, LX/7Df;->q:LX/7DD;

    iget-wide v2, p0, LX/7Df;->w:J

    long-to-float v1, v2

    iget v2, p0, LX/7Df;->y:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, v0, LX/7DD;->a:F

    goto :goto_0

    .line 1183262
    :cond_5
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_2
.end method

.method public final b()V
    .locals 8

    .prologue
    .line 1183224
    iget-object v0, p0, LX/7Df;->b:LX/5Pc;

    const v1, 0x7f070051

    const v2, 0x7f070050

    invoke-interface {v0, v1, v2}, LX/5Pc;->a(II)LX/5Pb;

    move-result-object v0

    iput-object v0, p0, LX/7Df;->m:LX/5Pb;

    .line 1183225
    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 1183226
    iget-object v0, p0, LX/7Df;->o:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    if-eqz v0, :cond_0

    .line 1183227
    iget-object v0, p0, LX/7Df;->o:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    .line 1183228
    iget v1, v0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->a:F

    move v0, v1

    .line 1183229
    neg-float v0, v0

    .line 1183230
    iget-object v1, p0, LX/7Df;->o:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    .line 1183231
    iget v2, v1, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->b:F

    move v1, v2

    .line 1183232
    iget-object v2, p0, LX/7Df;->o:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    .line 1183233
    iget v3, v2, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->c:F

    move v2, v3

    .line 1183234
    iget-object v3, p0, LX/7Df;->o:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    .line 1183235
    iget v4, v3, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->d:F

    move v3, v4

    .line 1183236
    neg-float v3, v3

    .line 1183237
    iget-object v4, p0, LX/7Df;->m:LX/5Pb;

    invoke-virtual {v4}, LX/5Pb;->a()LX/5Pa;

    move-result-object v4

    const-string v5, "uHorizontalAngleBounds"

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    double-to-float v0, v6

    float-to-double v6, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    double-to-float v1, v6

    invoke-virtual {v4, v5, v0, v1}, LX/5Pa;->a(Ljava/lang/String;FF)LX/5Pa;

    .line 1183238
    iget-object v0, p0, LX/7Df;->m:LX/5Pb;

    invoke-virtual {v0}, LX/5Pb;->a()LX/5Pa;

    move-result-object v0

    const-string v1, "uVerticleAngleBounds"

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    double-to-float v3, v4

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    double-to-float v2, v4

    invoke-virtual {v0, v1, v3, v2}, LX/5Pa;->a(Ljava/lang/String;FF)LX/5Pa;

    .line 1183239
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 1183212
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7Df;->s:Z

    .line 1183213
    iget-object v0, p0, LX/7Df;->c:LX/7Dc;

    .line 1183214
    iget-object v1, v0, LX/7Dc;->a:Ljava/util/Map;

    invoke-static {v1}, LX/7Dc;->a(Ljava/util/Map;)V

    .line 1183215
    iget-object v1, v0, LX/7Dc;->b:Ljava/util/Map;

    invoke-static {v1}, LX/7Dc;->a(Ljava/util/Map;)V

    .line 1183216
    iget-object v0, p0, LX/7Df;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1183217
    iget-object v0, p0, LX/7Df;->n:LX/7Da;

    if-eqz v0, :cond_2

    .line 1183218
    iget-object v0, p0, LX/7Df;->n:LX/7Da;

    .line 1183219
    iget-object v1, v0, LX/7Da;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7DZ;

    .line 1183220
    iget-object v3, v1, LX/7DZ;->c:LX/1ca;

    if-eqz v3, :cond_0

    .line 1183221
    iget-object v1, v1, LX/7DZ;->c:LX/1ca;

    invoke-interface {v1}, LX/1ca;->g()Z

    goto :goto_0

    .line 1183222
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/7Df;->n:LX/7Da;

    .line 1183223
    :cond_2
    return-void
.end method

.method public final d()LX/7DC;
    .locals 1

    .prologue
    .line 1183211
    sget-object v0, LX/7DC;->TILED:LX/7DC;

    return-object v0
.end method

.method public final e()LX/7DD;
    .locals 4

    .prologue
    .line 1183196
    iget-object v0, p0, LX/7Df;->n:LX/7Da;

    if-nez v0, :cond_0

    .line 1183197
    const/4 v0, 0x0

    .line 1183198
    :goto_0
    return-object v0

    .line 1183199
    :cond_0
    iget-object v0, p0, LX/7Df;->q:LX/7DD;

    iget v1, p0, LX/7Df;->x:F

    iget v2, p0, LX/7Df;->y:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, v0, LX/7DD;->b:F

    .line 1183200
    iget-object v0, p0, LX/7Df;->q:LX/7DD;

    iget-object v1, p0, LX/7Df;->c:LX/7Dc;

    .line 1183201
    iget-object v2, v1, LX/7Dc;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    iget-object v3, v1, LX/7Dc;->b:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    add-int/2addr v2, v3

    move v2, v2

    .line 1183202
    mul-int/lit16 v2, v2, 0x200

    mul-int/lit16 v2, v2, 0x200

    mul-int/lit8 v2, v2, 0x4

    move v1, v2

    .line 1183203
    iput v1, v0, LX/7DD;->d:I

    .line 1183204
    iget-object v0, p0, LX/7Df;->q:LX/7DD;

    iget-object v1, p0, LX/7Df;->n:LX/7Da;

    .line 1183205
    iget v2, v1, LX/7Da;->g:I

    move v1, v2

    .line 1183206
    iput v1, v0, LX/7DD;->e:I

    .line 1183207
    iget-object v0, p0, LX/7Df;->q:LX/7DD;

    iget v1, p0, LX/7Df;->z:I

    mul-int/lit16 v1, v1, 0x200

    mul-int/lit16 v1, v1, 0x200

    iput v1, v0, LX/7DD;->f:I

    .line 1183208
    iget-object v0, p0, LX/7Df;->q:LX/7DD;

    iget v1, p0, LX/7Df;->v:I

    iput v1, v0, LX/7DD;->g:I

    .line 1183209
    iget-object v0, p0, LX/7Df;->q:LX/7DD;

    iget v1, p0, LX/7Df;->u:I

    iput v1, v0, LX/7DD;->h:I

    .line 1183210
    iget-object v0, p0, LX/7Df;->q:LX/7DD;

    goto :goto_0
.end method
