.class public LX/7ic;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/7id;",
        "LX/7iZ;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/7ic;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1227698
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 1227699
    return-void
.end method

.method public static a(LX/0QB;)LX/7ic;
    .locals 3

    .prologue
    .line 1227700
    sget-object v0, LX/7ic;->a:LX/7ic;

    if-nez v0, :cond_1

    .line 1227701
    const-class v1, LX/7ic;

    monitor-enter v1

    .line 1227702
    :try_start_0
    sget-object v0, LX/7ic;->a:LX/7ic;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1227703
    if-eqz v2, :cond_0

    .line 1227704
    :try_start_1
    new-instance v0, LX/7ic;

    invoke-direct {v0}, LX/7ic;-><init>()V

    .line 1227705
    move-object v0, v0

    .line 1227706
    sput-object v0, LX/7ic;->a:LX/7ic;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1227707
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1227708
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1227709
    :cond_1
    sget-object v0, LX/7ic;->a:LX/7ic;

    return-object v0

    .line 1227710
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1227711
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
