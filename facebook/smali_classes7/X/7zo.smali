.class public LX/7zo;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/7zn;


# instance fields
.field private final b:LX/1jY;

.field public final c:LX/1jZ;

.field public final d:LX/0ad;

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field public g:I

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1281371
    new-instance v0, LX/7zn;

    invoke-direct {v0}, LX/7zn;-><init>()V

    sput-object v0, LX/7zo;->a:LX/7zn;

    return-void
.end method

.method public constructor <init>(LX/1jY;LX/1jZ;LX/0ad;)V
    .locals 1
    .param p1    # LX/1jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1281372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1281373
    iput-object p1, p0, LX/7zo;->b:LX/1jY;

    .line 1281374
    iput-object p2, p0, LX/7zo;->c:LX/1jZ;

    .line 1281375
    iput-object p3, p0, LX/7zo;->d:LX/0ad;

    .line 1281376
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/7zo;->e:Ljava/util/Map;

    .line 1281377
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7zo;->f:Ljava/util/List;

    .line 1281378
    return-void
.end method

.method public static a(LX/7zo;Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 1281379
    const/4 v0, -0x1

    .line 1281380
    iget-object v1, p0, LX/7zo;->d:LX/0ad;

    sget-short v2, LX/0fe;->M:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1281381
    :cond_0
    :goto_0
    move v0, v0

    .line 1281382
    if-gez v0, :cond_1

    .line 1281383
    iget-object v0, p0, LX/7zo;->b:LX/1jY;

    const/4 v2, -0x1

    .line 1281384
    iget-object v1, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual {v1, p1}, LX/0qp;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1jg;

    .line 1281385
    if-nez v1, :cond_4

    move v1, v2

    .line 1281386
    :goto_1
    move v0, v1

    .line 1281387
    :cond_1
    const/4 v3, 0x0

    .line 1281388
    iget-object v1, p0, LX/7zo;->d:LX/0ad;

    sget-short v2, LX/0fe;->M:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1281389
    :cond_2
    :goto_2
    return v0

    .line 1281390
    :cond_3
    iget-object v1, p0, LX/7zo;->h:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/7zo;->h:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1281391
    iget v0, p0, LX/7zo;->i:I

    goto :goto_0

    .line 1281392
    :cond_4
    iget-object v3, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual {v3, v1}, LX/0qp;->a(Ljava/lang/Object;)I

    move-result v1

    .line 1281393
    if-gez v1, :cond_5

    move v1, v2

    .line 1281394
    goto :goto_1

    .line 1281395
    :cond_5
    const/4 v2, 0x0

    .line 1281396
    add-int/lit8 v1, v1, -0x1

    move v3, v1

    :goto_3
    if-ltz v3, :cond_6

    iget-object v1, v0, LX/1jY;->c:LX/0qp;

    invoke-virtual {v1}, LX/0qp;->size()I

    move-result v1

    if-ge v3, v1, :cond_6

    .line 1281397
    iget-object v1, v0, LX/1jY;->c:LX/0qp;

    .line 1281398
    iget-object v4, v1, LX/0qp;->e:Ljava/util/List;

    move-object v1, v4

    .line 1281399
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1jg;

    .line 1281400
    iget-boolean v4, v1, LX/1jg;->f:Z

    if-nez v4, :cond_6

    iget-boolean v1, v1, LX/1jg;->g:Z

    if-nez v1, :cond_6

    .line 1281401
    add-int/lit8 v2, v2, 0x1

    .line 1281402
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    goto :goto_3

    :cond_6
    move v1, v2

    .line 1281403
    goto :goto_1

    .line 1281404
    :cond_7
    if-ltz v0, :cond_2

    iget-object v1, p0, LX/7zo;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LX/7zo;->f:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1281405
    iput-object p1, p0, LX/7zo;->h:Ljava/lang/String;

    .line 1281406
    iput v0, p0, LX/7zo;->i:I

    goto :goto_2
.end method
