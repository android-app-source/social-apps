.class public final LX/8X7;
.super LX/2s5;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;

.field private b:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

.field private c:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;LX/0gc;)V
    .locals 0

    .prologue
    .line 1354582
    iput-object p1, p0, LX/8X7;->a:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameCardViewFragment;

    .line 1354583
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 1354584
    return-void
.end method

.method private f()Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;
    .locals 2

    .prologue
    .line 1354585
    iget-object v0, p0, LX/8X7;->c:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;

    if-nez v0, :cond_0

    .line 1354586
    new-instance v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;

    invoke-direct {v0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;-><init>()V

    iput-object v0, p0, LX/8X7;->c:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;

    .line 1354587
    iget-object v0, p0, LX/8X7;->c:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;

    new-instance v1, LX/8X6;

    invoke-direct {v1, p0}, LX/8X6;-><init>(LX/8X7;)V

    .line 1354588
    iput-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;->e:LX/8VT;

    .line 1354589
    :cond_0
    iget-object v0, p0, LX/8X7;->c:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;

    return-object v0
.end method


# virtual methods
.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 1354590
    packed-switch p1, :pswitch_data_0

    .line 1354591
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1354592
    :pswitch_0
    invoke-virtual {p0}, LX/8X7;->d()Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    move-result-object v0

    goto :goto_0

    .line 1354593
    :pswitch_1
    invoke-direct {p0}, LX/8X7;->f()Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1354594
    invoke-super {p0, p1, p2, p3}, LX/2s5;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 1354595
    iget-object v0, p0, LX/8X7;->b:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    if-ne p3, v0, :cond_0

    .line 1354596
    const/4 v0, 0x0

    iput-object v0, p0, LX/8X7;->b:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    .line 1354597
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1354598
    const/4 v0, 0x2

    return v0
.end method

.method public final d()Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;
    .locals 2

    .prologue
    .line 1354599
    iget-object v0, p0, LX/8X7;->b:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    if-nez v0, :cond_0

    .line 1354600
    new-instance v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    invoke-direct {v0}, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;-><init>()V

    iput-object v0, p0, LX/8X7;->b:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    .line 1354601
    iget-object v0, p0, LX/8X7;->b:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    new-instance v1, LX/8X5;

    invoke-direct {v1, p0}, LX/8X5;-><init>(LX/8X7;)V

    .line 1354602
    iput-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->n:LX/8VT;

    .line 1354603
    :cond_0
    iget-object v0, p0, LX/8X7;->b:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    return-object v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1354604
    iget-object v0, p0, LX/8X7;->b:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    if-eqz v0, :cond_0

    .line 1354605
    iget-object v0, p0, LX/8X7;->b:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;

    .line 1354606
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameSummaryFragment;->m:LX/8WI;

    invoke-virtual {v1}, LX/8WI;->b()V

    .line 1354607
    :cond_0
    iget-object v0, p0, LX/8X7;->c:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;

    if-eqz v0, :cond_2

    .line 1354608
    iget-object v0, p0, LX/8X7;->c:Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;

    .line 1354609
    iget-object v1, v0, Lcom/facebook/quicksilver/views/endgame/QuicksilverEndgameMatchesFragment;->d:LX/8XB;

    .line 1354610
    iget-object v0, v1, LX/8XB;->b:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    if-eqz v0, :cond_1

    .line 1354611
    iget-object v0, v1, LX/8XB;->b:Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/GamesAllMatchesFragment;->a()V

    .line 1354612
    :cond_1
    iget-object v0, v1, LX/8XB;->c:Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    if-eqz v0, :cond_2

    .line 1354613
    iget-object v0, v1, LX/8XB;->c:Lcom/facebook/quicksilver/views/GamesTopScoresFragment;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/views/GamesTopScoresFragment;->a()V

    .line 1354614
    :cond_2
    return-void
.end method
