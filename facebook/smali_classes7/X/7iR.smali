.class public LX/7iR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1227490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1227491
    return-void
.end method

.method public static a(LX/7iP;)I
    .locals 2

    .prologue
    .line 1227492
    sget-object v0, LX/7iK;->a:[I

    invoke-virtual {p0}, LX/7iP;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1227493
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1227494
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1227495
    :pswitch_1
    const/4 v0, 0x6

    goto :goto_0

    .line 1227496
    :pswitch_2
    const/16 v0, 0xc

    goto :goto_0

    .line 1227497
    :pswitch_3
    const/16 v0, 0x20

    goto :goto_0

    .line 1227498
    :pswitch_4
    const/16 v0, 0x22

    goto :goto_0

    .line 1227499
    :pswitch_5
    const/16 v0, 0x23

    goto :goto_0

    .line 1227500
    :pswitch_6
    const/16 v0, 0x25

    goto :goto_0

    .line 1227501
    :pswitch_7
    const/16 v0, 0x26

    goto :goto_0

    .line 1227502
    :pswitch_8
    const/16 v0, 0x27

    goto :goto_0

    .line 1227503
    :pswitch_9
    const/16 v0, 0x2a

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static a(I)LX/7iP;
    .locals 5

    .prologue
    .line 1227504
    invoke-static {}, LX/7iP;->values()[LX/7iP;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1227505
    invoke-static {v0}, LX/7iR;->a(LX/7iP;)I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 1227506
    :goto_1
    return-object v0

    .line 1227507
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1227508
    :cond_1
    sget-object v0, LX/7iP;->UNKNOWN:LX/7iP;

    goto :goto_1
.end method
