.class public final LX/6uC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1155638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;
    .locals 2

    .prologue
    .line 1155639
    new-instance v0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;

    invoke-direct {v0, p0}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1155640
    invoke-static {p1}, LX/6uC;->a(Landroid/os/Parcel;)Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1155641
    new-array v0, p1, [Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;

    move-object v0, v0

    .line 1155642
    return-object v0
.end method
