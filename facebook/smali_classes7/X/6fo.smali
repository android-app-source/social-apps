.class public final LX/6fo;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

.field public b:I

.field public c:I

.field public d:I

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1120460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1120461
    sget-object v0, Lcom/facebook/messaging/model/threads/BookingRequestDetail;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    iput-object v0, p0, LX/6fo;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    return-void
.end method


# virtual methods
.method public final a(I)LX/6fo;
    .locals 0

    .prologue
    .line 1120462
    iput p1, p0, LX/6fo;->b:I

    .line 1120463
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/BookingRequestDetail;)LX/6fo;
    .locals 2

    .prologue
    .line 1120456
    if-nez p1, :cond_0

    .line 1120457
    const-string v0, "Booking"

    const-string v1, "null booking request detail"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1120458
    :goto_0
    return-object p0

    .line 1120459
    :cond_0
    iput-object p1, p0, LX/6fo;->a:Lcom/facebook/messaging/model/threads/BookingRequestDetail;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)LX/6fo;
    .locals 0

    .prologue
    .line 1120464
    iput-object p1, p0, LX/6fo;->e:Ljava/lang/String;

    .line 1120465
    return-object p0
.end method

.method public final a()Lcom/facebook/messaging/model/threads/ThreadBookingRequests;
    .locals 1

    .prologue
    .line 1120455
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/model/threads/ThreadBookingRequests;-><init>(LX/6fo;)V

    return-object v0
.end method

.method public final b(I)LX/6fo;
    .locals 0

    .prologue
    .line 1120453
    iput p1, p0, LX/6fo;->c:I

    .line 1120454
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/6fo;
    .locals 0

    .prologue
    .line 1120449
    iput-object p1, p0, LX/6fo;->f:Ljava/lang/String;

    .line 1120450
    return-object p0
.end method

.method public final c(I)LX/6fo;
    .locals 0

    .prologue
    .line 1120451
    iput p1, p0, LX/6fo;->d:I

    .line 1120452
    return-object p0
.end method
