.class public LX/84c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1290977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1290978
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/84c;->a:Ljava/lang/String;

    .line 1290979
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1290980
    check-cast p1, Ljava/lang/String;

    .line 1290981
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1290982
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "contact_id"

    invoke-direct {v1, v2, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1290983
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "id"

    iget-object v3, p0, LX/84c;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1290984
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "blacklistPeopleYouMayInvite"

    .line 1290985
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1290986
    move-object v1, v1

    .line 1290987
    const-string v2, "POST"

    .line 1290988
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1290989
    move-object v1, v1

    .line 1290990
    const-string v2, "me/blacklisted_pymi_contacts"

    .line 1290991
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1290992
    move-object v1, v1

    .line 1290993
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1290994
    move-object v0, v1

    .line 1290995
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1290996
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1290997
    move-object v0, v0

    .line 1290998
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1290999
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1291000
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1291001
    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
