.class public LX/7GJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DE:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDE;"
        }
    .end annotation
.end field

.field public final b:J

.field public final c:Lcom/facebook/fbtrace/FbTraceNode;


# direct methods
.method public constructor <init>(Ljava/lang/Object;JLcom/facebook/fbtrace/FbTraceNode;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDE;J",
            "Lcom/facebook/fbtrace/FbTraceNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1189056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189057
    iput-object p1, p0, LX/7GJ;->a:Ljava/lang/Object;

    .line 1189058
    iput-wide p2, p0, LX/7GJ;->b:J

    .line 1189059
    iput-object p4, p0, LX/7GJ;->c:Lcom/facebook/fbtrace/FbTraceNode;

    .line 1189060
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1189061
    if-eqz p1, :cond_0

    instance-of v1, p1, LX/7GJ;

    if-nez v1, :cond_1

    .line 1189062
    :cond_0
    :goto_0
    return v0

    .line 1189063
    :cond_1
    check-cast p1, LX/7GJ;

    .line 1189064
    iget-object v1, p0, LX/7GJ;->a:Ljava/lang/Object;

    iget-object v2, p1, LX/7GJ;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, LX/7GJ;->b:J

    iget-wide v4, p1, LX/7GJ;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p0, LX/7GJ;->c:Lcom/facebook/fbtrace/FbTraceNode;

    iget-object v2, p1, LX/7GJ;->c:Lcom/facebook/fbtrace/FbTraceNode;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1189065
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/7GJ;->a:Ljava/lang/Object;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, LX/7GJ;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/7GJ;->c:Lcom/facebook/fbtrace/FbTraceNode;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
