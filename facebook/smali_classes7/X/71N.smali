.class public LX/71N;
.super LX/6vv;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6vv",
        "<",
        "Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerRunTimeData;",
        "Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;",
        "Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;",
        "Lcom/facebook/payments/picker/model/CoreClientData;",
        "LX/71X;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1163296
    invoke-direct {p0}, LX/6vv;-><init>()V

    .line 1163297
    return-void
.end method

.method public static a(LX/0QB;)LX/71N;
    .locals 3

    .prologue
    .line 1163300
    const-class v1, LX/71N;

    monitor-enter v1

    .line 1163301
    :try_start_0
    sget-object v0, LX/71N;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1163302
    sput-object v2, LX/71N;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1163303
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1163304
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1163305
    new-instance v0, LX/71N;

    invoke-direct {v0}, LX/71N;-><init>()V

    .line 1163306
    move-object v0, v0

    .line 1163307
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1163308
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/71N;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1163309
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1163310
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 1163311
    check-cast p1, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;

    .line 1163312
    new-instance v0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerRunTimeData;

    invoke-direct {v0, p1}, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerRunTimeData;-><init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 1163298
    check-cast p1, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;

    .line 1163299
    new-instance v0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerRunTimeData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerRunTimeData;-><init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)V

    return-object v0
.end method
