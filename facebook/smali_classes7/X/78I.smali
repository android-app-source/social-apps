.class public final LX/78I;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/78H;

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1172537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1172538
    sget-object v0, LX/78H;->Insignificant:LX/78H;

    iput-object v0, p0, LX/78I;->a:LX/78H;

    .line 1172539
    const/4 v0, 0x0

    iput v0, p0, LX/78I;->c:I

    iput v0, p0, LX/78I;->b:I

    .line 1172540
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1172541
    const/4 v0, 0x0

    iput v0, p0, LX/78I;->c:I

    iput v0, p0, LX/78I;->b:I

    .line 1172542
    return-void
.end method

.method public final a(ZZ)V
    .locals 2

    .prologue
    .line 1172543
    sget-object v0, LX/78F;->a:[I

    iget-object v1, p0, LX/78I;->a:LX/78H;

    invoke-virtual {v1}, LX/78H;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1172544
    :cond_0
    :goto_0
    return-void

    .line 1172545
    :pswitch_0
    if-eqz p1, :cond_1

    .line 1172546
    sget-object v0, LX/78H;->AboveThreshold:LX/78H;

    iput-object v0, p0, LX/78I;->a:LX/78H;

    .line 1172547
    iget v0, p0, LX/78I;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/78I;->b:I

    goto :goto_0

    .line 1172548
    :cond_1
    if-eqz p2, :cond_0

    .line 1172549
    sget-object v0, LX/78H;->BelowThreshold:LX/78H;

    iput-object v0, p0, LX/78I;->a:LX/78H;

    .line 1172550
    iget v0, p0, LX/78I;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/78I;->c:I

    goto :goto_0

    .line 1172551
    :pswitch_1
    if-eqz p2, :cond_2

    .line 1172552
    sget-object v0, LX/78H;->BelowThreshold:LX/78H;

    iput-object v0, p0, LX/78I;->a:LX/78H;

    .line 1172553
    iget v0, p0, LX/78I;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/78I;->c:I

    goto :goto_0

    .line 1172554
    :cond_2
    if-nez p1, :cond_0

    .line 1172555
    sget-object v0, LX/78H;->Insignificant:LX/78H;

    iput-object v0, p0, LX/78I;->a:LX/78H;

    goto :goto_0

    .line 1172556
    :pswitch_2
    if-eqz p1, :cond_3

    .line 1172557
    sget-object v0, LX/78H;->AboveThreshold:LX/78H;

    iput-object v0, p0, LX/78I;->a:LX/78H;

    .line 1172558
    iget v0, p0, LX/78I;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/78I;->b:I

    goto :goto_0

    .line 1172559
    :cond_3
    if-nez p2, :cond_0

    .line 1172560
    sget-object v0, LX/78H;->Insignificant:LX/78H;

    iput-object v0, p0, LX/78I;->a:LX/78H;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1172561
    iget v0, p0, LX/78I;->b:I

    if-lt v0, v1, :cond_0

    iget v0, p0, LX/78I;->c:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
