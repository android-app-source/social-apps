.class public final LX/84S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/2h7;

.field public final synthetic c:LX/2iT;


# direct methods
.method public constructor <init>(LX/2iT;JLX/2h7;)V
    .locals 0

    .prologue
    .line 1290939
    iput-object p1, p0, LX/84S;->c:LX/2iT;

    iput-wide p2, p0, LX/84S;->a:J

    iput-object p4, p0, LX/84S;->b:LX/2h7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    .line 1290940
    iget-object v0, p0, LX/84S;->c:LX/2iT;

    iget-object v1, v0, LX/2hY;->a:LX/2dj;

    iget-wide v2, p0, LX/84S;->a:J

    iget-object v0, p0, LX/84S;->b:LX/2h7;

    iget-object v4, v0, LX/2h7;->friendRequestHowFound:LX/2h8;

    iget-object v0, p0, LX/84S;->b:LX/2h7;

    iget-object v5, v0, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, LX/2dj;->a(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1290941
    iget-object v1, p0, LX/84S;->c:LX/2iT;

    iget-wide v2, p0, LX/84S;->a:J

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v5, 0x1

    invoke-virtual {v1, v2, v3, v4, v5}, LX/2hY;->a(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 1290942
    iget-object v1, p0, LX/84S;->c:LX/2iT;

    iget-object v1, v1, LX/2iT;->c:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ADD_FRIEND_IGNORE_WARN"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, LX/84S;->a:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/84Q;

    invoke-direct {v3, p0, v0}, LX/84Q;-><init>(LX/84S;Lcom/google/common/util/concurrent/ListenableFuture;)V

    new-instance v0, LX/84R;

    invoke-direct {v0, p0}, LX/84R;-><init>(LX/84S;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1290943
    return-void
.end method
