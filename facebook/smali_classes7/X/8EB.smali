.class public LX/8EB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/8EB;


# instance fields
.field public final a:LX/11i;

.field public final b:LX/01T;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/0Pq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/11i;LX/01T;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1313472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1313473
    iput-object p1, p0, LX/8EB;->a:LX/11i;

    .line 1313474
    iput-object p2, p0, LX/8EB;->b:LX/01T;

    .line 1313475
    iget-object v0, p0, LX/8EB;->b:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_0

    .line 1313476
    sget-object v0, LX/8Dx;->d:LX/8Dv;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/8EB;->c:LX/0Px;

    .line 1313477
    :goto_0
    return-void

    .line 1313478
    :cond_0
    sget-object v0, LX/8Dx;->f:LX/2rx;

    sget-object v1, LX/8Dx;->e:LX/8Dt;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/8EB;->c:LX/0Px;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/8EB;
    .locals 5

    .prologue
    .line 1313479
    sget-object v0, LX/8EB;->d:LX/8EB;

    if-nez v0, :cond_1

    .line 1313480
    const-class v1, LX/8EB;

    monitor-enter v1

    .line 1313481
    :try_start_0
    sget-object v0, LX/8EB;->d:LX/8EB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1313482
    if-eqz v2, :cond_0

    .line 1313483
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1313484
    new-instance p0, LX/8EB;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v4

    check-cast v4, LX/01T;

    invoke-direct {p0, v3, v4}, LX/8EB;-><init>(LX/11i;LX/01T;)V

    .line 1313485
    move-object v0, p0

    .line 1313486
    sput-object v0, LX/8EB;->d:LX/8EB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1313487
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1313488
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1313489
    :cond_1
    sget-object v0, LX/8EB;->d:LX/8EB;

    return-object v0

    .line 1313490
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1313491
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()LX/8EB;
    .locals 4

    .prologue
    .line 1313492
    iget-object v0, p0, LX/8EB;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/8EB;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pq;

    .line 1313493
    iget-object v3, p0, LX/8EB;->a:LX/11i;

    invoke-interface {v3, v0}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v3

    .line 1313494
    if-eqz v3, :cond_0

    .line 1313495
    iget-object v3, p0, LX/8EB;->a:LX/11i;

    invoke-interface {v3, v0}, LX/11i;->d(LX/0Pq;)V

    .line 1313496
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1313497
    :cond_1
    return-object p0
.end method
