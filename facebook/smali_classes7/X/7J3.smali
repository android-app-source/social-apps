.class public LX/7J3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/7J3;


# instance fields
.field private final a:LX/0AU;

.field public final b:LX/0So;

.field public final c:LX/37Y;

.field private final d:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/7Iy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0AU;LX/0So;LX/37Y;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1193138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1193139
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/7J3;->d:Ljava/util/LinkedList;

    .line 1193140
    iput-object p1, p0, LX/7J3;->a:LX/0AU;

    .line 1193141
    iput-object p2, p0, LX/7J3;->b:LX/0So;

    .line 1193142
    iput-object p3, p0, LX/7J3;->c:LX/37Y;

    .line 1193143
    iget-object v0, p0, LX/7J3;->c:LX/37Y;

    new-instance p1, LX/7Ix;

    invoke-direct {p1, p0}, LX/7Ix;-><init>(LX/7J3;)V

    invoke-virtual {v0, p1}, LX/37Y;->a(LX/7Iw;)V

    .line 1193144
    return-void
.end method

.method public static a(LX/0QB;)LX/7J3;
    .locals 6

    .prologue
    .line 1193125
    sget-object v0, LX/7J3;->e:LX/7J3;

    if-nez v0, :cond_1

    .line 1193126
    const-class v1, LX/7J3;

    monitor-enter v1

    .line 1193127
    :try_start_0
    sget-object v0, LX/7J3;->e:LX/7J3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1193128
    if-eqz v2, :cond_0

    .line 1193129
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1193130
    new-instance p0, LX/7J3;

    invoke-static {v0}, LX/0AU;->a(LX/0QB;)LX/0AU;

    move-result-object v3

    check-cast v3, LX/0AU;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v0}, LX/37Y;->a(LX/0QB;)LX/37Y;

    move-result-object v5

    check-cast v5, LX/37Y;

    invoke-direct {p0, v3, v4, v5}, LX/7J3;-><init>(LX/0AU;LX/0So;LX/37Y;)V

    .line 1193131
    move-object v0, p0

    .line 1193132
    sput-object v0, LX/7J3;->e:LX/7J3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1193133
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1193134
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1193135
    :cond_1
    sget-object v0, LX/7J3;->e:LX/7J3;

    return-object v0

    .line 1193136
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1193137
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/7Iy;)V
    .locals 4

    .prologue
    .line 1193171
    iget-object v0, p0, LX/7J3;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1193172
    iget-object v0, p0, LX/7J3;->a:LX/0AU;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1193173
    iget-object v2, p1, LX/7Iy;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1193174
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".start"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1193175
    iget-object v2, p1, LX/7Iy;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1193176
    iget-object v3, p1, LX/7Iy;->e:Ljava/lang/String;

    move-object v3, v3

    .line 1193177
    invoke-virtual {v0, v1, v2, v3}, LX/0AU;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1193178
    return-void
.end method

.method public static b(LX/7J3;)V
    .locals 12

    .prologue
    .line 1193151
    iget-object v0, p0, LX/7J3;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 1193152
    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1193153
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Iy;

    .line 1193154
    invoke-virtual {v0}, LX/7Iy;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1193155
    invoke-virtual {v0}, LX/7Iy;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1193156
    const-string v1, ".succeed"

    move-object v2, v1

    .line 1193157
    :goto_0
    iget-object v1, p0, LX/7J3;->a:LX/0AU;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1193158
    iget-object v4, v0, LX/7Iy;->c:Ljava/lang/String;

    move-object v4, v4

    .line 1193159
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1193160
    iget-object v3, v0, LX/7Iy;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1193161
    invoke-virtual {v0}, LX/7Iy;->f()J

    move-result-wide v4

    .line 1193162
    iget-object v6, v0, LX/7Iy;->e:Ljava/lang/String;

    move-object v6, v6

    .line 1193163
    invoke-virtual/range {v1 .. v6}, LX/0AU;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 1193164
    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    .line 1193165
    invoke-virtual {v0}, LX/7Iy;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1193166
    :cond_1
    return-void

    .line 1193167
    :cond_2
    const-string v1, ".fail"

    move-object v2, v1

    goto :goto_0

    .line 1193168
    :cond_3
    invoke-virtual {v0}, LX/7Iy;->f()J

    move-result-wide v8

    const-wide/32 v10, 0xea60

    cmp-long v8, v8, v10

    if-ltz v8, :cond_4

    const/4 v8, 0x1

    :goto_1
    move v1, v8

    .line 1193169
    if-eqz v1, :cond_0

    .line 1193170
    const-string v1, ".fail.expire"

    move-object v2, v1

    goto :goto_0

    :cond_4
    const/4 v8, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/7J2;
    .locals 1

    .prologue
    .line 1193179
    new-instance v0, LX/7J2;

    invoke-direct {v0, p0, p1}, LX/7J2;-><init>(LX/7J3;Ljava/lang/String;)V

    .line 1193180
    invoke-direct {p0, v0}, LX/7J3;->a(LX/7Iy;)V

    .line 1193181
    return-object v0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1193149
    new-instance v0, LX/7J1;

    invoke-direct {v0, p0, p1, p2}, LX/7J1;-><init>(LX/7J3;Ljava/lang/String;I)V

    invoke-direct {p0, v0}, LX/7J3;->a(LX/7Iy;)V

    .line 1193150
    return-void
.end method

.method public final a(Ljava/lang/String;LX/38a;)V
    .locals 1

    .prologue
    .line 1193147
    new-instance v0, LX/7Iz;

    invoke-direct {v0, p0, p1, p2}, LX/7Iz;-><init>(LX/7J3;Ljava/lang/String;LX/38a;)V

    invoke-direct {p0, v0}, LX/7J3;->a(LX/7Iy;)V

    .line 1193148
    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1193145
    new-instance v0, LX/7J0;

    invoke-direct {v0, p0, p1, p2}, LX/7J0;-><init>(LX/7J3;Ljava/lang/String;I)V

    invoke-direct {p0, v0}, LX/7J3;->a(LX/7Iy;)V

    .line 1193146
    return-void
.end method
