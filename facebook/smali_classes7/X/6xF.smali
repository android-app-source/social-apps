.class public LX/6xF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wy;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6wy",
        "<",
        "Lcom/facebook/payments/form/model/ShippingMethodFormData;",
        ">;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:I

.field public final c:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public final d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public final e:LX/6x7;

.field public f:LX/6x9;

.field private g:LX/6qh;

.field private h:Lcom/facebook/payments/form/model/ShippingMethodFormData;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6x7;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1158589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158590
    iput-object p1, p0, LX/6xF;->a:Landroid/content/Context;

    .line 1158591
    iput-object p2, p0, LX/6xF;->e:LX/6x7;

    .line 1158592
    iget-object v0, p0, LX/6xF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0546

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/6xF;->b:I

    .line 1158593
    new-instance v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, LX/6xF;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/6xF;->c:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1158594
    iget-object v0, p0, LX/6xF;->c:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, LX/6xF;->a:Landroid/content/Context;

    const v2, 0x7f081e40

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1158595
    iget-object v0, p0, LX/6xF;->c:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, LX/6xF;->e:LX/6x7;

    invoke-virtual {v1}, LX/6x7;->b()I

    move-result v1

    iget-object v2, p0, LX/6xF;->e:LX/6x7;

    invoke-virtual {v2}, LX/6x7;->b()I

    move-result v2

    iget p1, p0, LX/6xF;->b:I

    iget p2, p0, LX/6xF;->b:I

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setPadding(IIII)V

    .line 1158596
    new-instance v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, LX/6xF;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/6xF;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1158597
    iget-object v0, p0, LX/6xF;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, LX/6xF;->a:Landroid/content/Context;

    const v2, 0x7f081e41

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1158598
    iget-object v0, p0, LX/6xF;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const/16 v1, 0x2002

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputType(I)V

    .line 1158599
    iget-object v0, p0, LX/6xF;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget v1, p0, LX/6xF;->b:I

    iget-object v2, p0, LX/6xF;->e:LX/6x7;

    invoke-virtual {v2}, LX/6x7;->b()I

    move-result v2

    iget-object p1, p0, LX/6xF;->e:LX/6x7;

    invoke-virtual {p1}, LX/6x7;->b()I

    move-result p1

    iget p2, p0, LX/6xF;->b:I

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setPadding(IIII)V

    .line 1158600
    return-void
.end method

.method public static a(LX/0QB;)LX/6xF;
    .locals 5

    .prologue
    .line 1158578
    const-class v1, LX/6xF;

    monitor-enter v1

    .line 1158579
    :try_start_0
    sget-object v0, LX/6xF;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1158580
    sput-object v2, LX/6xF;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1158581
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1158582
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1158583
    new-instance p0, LX/6xF;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/6x7;->b(LX/0QB;)LX/6x7;

    move-result-object v4

    check-cast v4, LX/6x7;

    invoke-direct {p0, v3, v4}, LX/6xF;-><init>(Landroid/content/Context;LX/6x7;)V

    .line 1158584
    move-object v0, p0

    .line 1158585
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1158586
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6xF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1158587
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1158588
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/payments/ui/PaymentFormEditTextView;)V
    .locals 1

    .prologue
    .line 1158576
    new-instance v0, LX/6xE;

    invoke-direct {v0, p0}, LX/6xE;-><init>(LX/6xF;)V

    invoke-virtual {p1, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a(Landroid/text/TextWatcher;)V

    .line 1158577
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1158601
    invoke-virtual {p0}, LX/6xF;->b()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1158602
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1158603
    const-string v1, "extra_text"

    iget-object v2, p0, LX/6xF;->c:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1158604
    new-instance v1, Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v2, p0, LX/6xF;->h:Lcom/facebook/payments/form/model/ShippingMethodFormData;

    iget-object v2, v2, Lcom/facebook/payments/form/model/ShippingMethodFormData;->a:Ljava/util/Currency;

    new-instance v3, Ljava/math/BigDecimal;

    iget-object v4, p0, LX/6xF;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v4}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2, v3}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/util/Currency;Ljava/math/BigDecimal;)V

    .line 1158605
    const-string v2, "extra_currency_amount"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1158606
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1158607
    const-string v2, "extra_activity_result_data"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1158608
    iget-object v0, p0, LX/6xF;->g:LX/6qh;

    new-instance v2, LX/73T;

    sget-object v3, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v2, v3, v1}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    invoke-virtual {v0, v2}, LX/6qh;->a(LX/73T;)V

    .line 1158609
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1158574
    iput-object p1, p0, LX/6xF;->g:LX/6qh;

    .line 1158575
    return-void
.end method

.method public final a(LX/6x9;)V
    .locals 0

    .prologue
    .line 1158563
    iput-object p1, p0, LX/6xF;->f:LX/6x9;

    .line 1158564
    return-void
.end method

.method public final a(LX/6xD;Lcom/facebook/payments/form/model/PaymentsFormData;)V
    .locals 5

    .prologue
    .line 1158566
    check-cast p2, Lcom/facebook/payments/form/model/ShippingMethodFormData;

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1158567
    const-string v0, "Shipping Form Data has not been set."

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/ShippingMethodFormData;

    iput-object v0, p0, LX/6xF;->h:Lcom/facebook/payments/form/model/ShippingMethodFormData;

    .line 1158568
    iget-object v0, p0, LX/6xF;->c:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-direct {p0, v0}, LX/6xF;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;)V

    .line 1158569
    iget-object v0, p0, LX/6xF;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-direct {p0, v0}, LX/6xF;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;)V

    .line 1158570
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, LX/6xF;->c:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    aput-object v1, v0, v3

    iget-object v1, p0, LX/6xF;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    aput-object v1, v0, v4

    invoke-virtual {p1, v0}, LX/6xD;->a([Landroid/view/View;)V

    .line 1158571
    new-array v0, v4, [Landroid/view/View;

    new-instance v1, LX/73W;

    iget-object v2, p0, LX/6xF;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/73W;-><init>(Landroid/content/Context;)V

    aput-object v1, v0, v3

    invoke-virtual {p1, v0}, LX/6xD;->a([Landroid/view/View;)V

    .line 1158572
    new-array v0, v4, [Landroid/view/View;

    iget-object v1, p0, LX/6xF;->e:LX/6x7;

    invoke-virtual {v1}, LX/6x7;->a()LX/73Y;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p1, v0}, LX/6xD;->a([Landroid/view/View;)V

    .line 1158573
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1158565
    iget-object v0, p0, LX/6xF;->c:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6xF;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
