.class public LX/7mf;
.super LX/7mV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7mV",
        "<",
        "LX/7mo;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/7mf;


# instance fields
.field private final e:LX/0bH;

.field private final f:LX/7me;


# direct methods
.method public constructor <init>(LX/0SG;LX/0bH;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1237313
    invoke-direct {p0, p1}, LX/7mV;-><init>(LX/0SG;)V

    .line 1237314
    iput-object p2, p0, LX/7mf;->e:LX/0bH;

    .line 1237315
    new-instance v0, LX/7me;

    invoke-direct {v0, p0}, LX/7me;-><init>(LX/7mf;)V

    iput-object v0, p0, LX/7mf;->f:LX/7me;

    .line 1237316
    iget-object v0, p0, LX/7mf;->e:LX/0bH;

    iget-object v1, p0, LX/7mf;->f:LX/7me;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1237317
    return-void
.end method

.method public static a(LX/0QB;)LX/7mf;
    .locals 5

    .prologue
    .line 1237318
    sget-object v0, LX/7mf;->g:LX/7mf;

    if-nez v0, :cond_1

    .line 1237319
    const-class v1, LX/7mf;

    monitor-enter v1

    .line 1237320
    :try_start_0
    sget-object v0, LX/7mf;->g:LX/7mf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1237321
    if-eqz v2, :cond_0

    .line 1237322
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1237323
    new-instance p0, LX/7mf;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v4

    check-cast v4, LX/0bH;

    invoke-direct {p0, v3, v4}, LX/7mf;-><init>(LX/0SG;LX/0bH;)V

    .line 1237324
    move-object v0, p0

    .line 1237325
    sput-object v0, LX/7mf;->g:LX/7mf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1237326
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1237327
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1237328
    :cond_1
    sget-object v0, LX/7mf;->g:LX/7mf;

    return-object v0

    .line 1237329
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1237330
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;LX/7mm;)V
    .locals 9
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1237331
    if-nez p2, :cond_2

    .line 1237332
    if-eqz p1, :cond_1

    const/4 v2, 0x1

    :goto_0
    const-string v3, "passed in null objects for both pending and uploaded story objects"

    invoke-static {v2, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1237333
    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v2

    .line 1237334
    :cond_0
    :goto_1
    iget-object v3, p0, LX/7mV;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 1237335
    iput-wide v4, v2, LX/23u;->v:J

    .line 1237336
    invoke-virtual {v2}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object v0, v2

    .line 1237337
    new-instance v1, LX/7mn;

    invoke-direct {v1, v0}, LX/7mn;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1237338
    iput-object p4, v1, LX/7mn;->b:LX/7mm;

    .line 1237339
    move-object v0, v1

    .line 1237340
    iput-object p3, v0, LX/7mn;->c:Ljava/lang/String;

    .line 1237341
    move-object v0, v0

    .line 1237342
    new-instance v1, LX/7mo;

    invoke-direct {v1, v0}, LX/7mo;-><init>(LX/7mn;)V

    move-object v0, v1

    .line 1237343
    invoke-virtual {p0, v0}, LX/7mV;->a(LX/7mi;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1237344
    return-void

    .line 1237345
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1237346
    :cond_2
    invoke-static {p2}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v2

    .line 1237347
    if-eqz p1, :cond_0

    .line 1237348
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v3

    .line 1237349
    iput-object v3, v2, LX/23u;->k:LX/0Px;

    .line 1237350
    goto :goto_1
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1237351
    const-wide/32 v0, 0x127500

    return-wide v0
.end method

.method public final finalize()V
    .locals 2

    .prologue
    .line 1237352
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 1237353
    iget-object v0, p0, LX/7mf;->e:LX/0bH;

    iget-object v1, p0, LX/7mf;->f:LX/7me;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1237354
    return-void
.end method
