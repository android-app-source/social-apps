.class public LX/7Ok;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Oi;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1Ln;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ln",
            "<",
            "LX/37C;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/37C;

.field private final d:LX/7Oi;

.field private final e:J

.field private f:LX/3Da;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Da",
            "<",
            "LX/37C;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/3Dd;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1202098
    const-class v0, LX/7Ok;

    sput-object v0, LX/7Ok;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1Ln;LX/37C;LX/7Oi;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ln",
            "<",
            "LX/37C;",
            ">;",
            "LX/37C;",
            "LX/7Oi;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 1202092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202093
    iput-object p1, p0, LX/7Ok;->b:LX/1Ln;

    .line 1202094
    iput-object p2, p0, LX/7Ok;->c:LX/37C;

    .line 1202095
    iput-object p3, p0, LX/7Ok;->d:LX/7Oi;

    .line 1202096
    iput-wide p4, p0, LX/7Ok;->e:J

    .line 1202097
    return-void
.end method

.method private a(Ljava/io/OutputStream;LX/2WF;)J
    .locals 5

    .prologue
    .line 1202089
    invoke-static {p0}, LX/7Ok;->b(LX/7Ok;)LX/3Da;

    move-result-object v0

    iget-wide v2, p2, LX/2WF;->a:J

    invoke-interface {v0, v2, v3}, LX/3Da;->b(J)Ljava/io/InputStream;

    move-result-object v1

    .line 1202090
    :try_start_0
    new-instance v0, LX/45Z;

    invoke-virtual {p2}, LX/2WF;->a()J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, LX/45Z;-><init>(Ljava/io/InputStream;JZ)V

    invoke-static {v0, p1}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 1202091
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return-wide v2

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method private b(Ljava/io/OutputStream;LX/2WF;)J
    .locals 8

    .prologue
    .line 1202028
    invoke-static {p0}, LX/7Ok;->b(LX/7Ok;)LX/3Da;

    move-result-object v0

    iget-wide v2, p2, LX/2WF;->a:J

    invoke-interface {v0, v2, v3}, LX/3Da;->a(J)Ljava/io/OutputStream;

    move-result-object v0

    .line 1202029
    new-instance v2, LX/7PG;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/io/OutputStream;

    const/4 v3, 0x0

    invoke-static {p1}, LX/45j;->a(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x1

    aput-object v0, v1, v3

    invoke-direct {v2, v1}, LX/7PG;-><init>([Ljava/io/OutputStream;)V

    .line 1202030
    iget-wide v0, p2, LX/2WF;->b:J

    .line 1202031
    iget-wide v4, p0, LX/7Ok;->e:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    invoke-virtual {p2}, LX/2WF;->a()J

    move-result-wide v4

    iget-wide v6, p0, LX/7Ok;->e:J

    cmp-long v3, v4, v6

    if-gtz v3, :cond_0

    .line 1202032
    iget-wide v0, p2, LX/2WF;->a:J

    iget-wide v4, p0, LX/7Ok;->e:J

    add-long/2addr v0, v4

    .line 1202033
    invoke-virtual {p0}, LX/7Ok;->a()LX/3Dd;

    move-result-object v3

    iget-wide v4, v3, LX/3Dd;->a:J

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 1202034
    :cond_0
    new-instance v3, LX/2WF;

    iget-wide v4, p2, LX/2WF;->a:J

    invoke-direct {v3, v4, v5, v0, v1}, LX/2WF;-><init>(JJ)V

    .line 1202035
    :try_start_0
    iget-object v0, p0, LX/7Ok;->d:LX/7Oi;

    invoke-interface {v0, v3, v2}, LX/7Oi;->a(LX/2WF;Ljava/io/OutputStream;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 1202036
    invoke-virtual {v2}, LX/7PG;->close()V

    return-wide v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/7PG;->close()V

    throw v0
.end method

.method private static b(LX/7Ok;)LX/3Da;
    .locals 11

    .prologue
    .line 1202063
    iget-object v0, p0, LX/7Ok;->f:LX/3Da;

    if-nez v0, :cond_1

    .line 1202064
    iget-object v0, p0, LX/7Ok;->b:LX/1Ln;

    iget-object v1, p0, LX/7Ok;->c:LX/37C;

    invoke-interface {v0, v1}, LX/1Ln;->b(Ljava/lang/Object;)LX/3Da;

    move-result-object v0

    iput-object v0, p0, LX/7Ok;->f:LX/3Da;

    .line 1202065
    iget-object v0, p0, LX/7Ok;->f:LX/3Da;

    if-nez v0, :cond_2

    .line 1202066
    iget-object v0, p0, LX/7Ok;->g:LX/3Dd;

    if-nez v0, :cond_0

    .line 1202067
    iget-object v0, p0, LX/7Ok;->d:LX/7Oi;

    invoke-interface {v0}, LX/7Oi;->a()LX/3Dd;

    move-result-object v0

    iput-object v0, p0, LX/7Ok;->g:LX/3Dd;

    .line 1202068
    :cond_0
    iget-object v0, p0, LX/7Ok;->g:LX/3Dd;

    const/4 v10, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1202069
    const-string v1, "Cache-Control"

    .line 1202070
    iget-object v4, v0, LX/3Dd;->c:Ljava/util/Map;

    if-nez v4, :cond_7

    .line 1202071
    const/4 v4, 0x0

    .line 1202072
    :goto_0
    move-object v1, v4

    .line 1202073
    check-cast v1, Ljava/lang/String;

    .line 1202074
    if-nez v1, :cond_3

    move v1, v2

    .line 1202075
    :goto_1
    move v0, v1

    .line 1202076
    if-eqz v0, :cond_1

    .line 1202077
    iget-object v0, p0, LX/7Ok;->b:LX/1Ln;

    iget-object v1, p0, LX/7Ok;->c:LX/37C;

    iget-object v2, p0, LX/7Ok;->g:LX/3Dd;

    invoke-interface {v0, v1, v2}, LX/1Ln;->a(Ljava/lang/Object;LX/3Dd;)LX/3Da;

    move-result-object v0

    iput-object v0, p0, LX/7Ok;->f:LX/3Da;

    .line 1202078
    :cond_1
    :goto_2
    iget-object v0, p0, LX/7Ok;->f:LX/3Da;

    return-object v0

    .line 1202079
    :cond_2
    iget-object v0, p0, LX/7Ok;->f:LX/3Da;

    invoke-interface {v0}, LX/3Da;->b()LX/3Dd;

    move-result-object v0

    iput-object v0, p0, LX/7Ok;->g:LX/3Dd;

    goto :goto_2

    .line 1202080
    :cond_3
    const-string v4, "\\s"

    const-string v5, ""

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1202081
    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1202082
    new-array v6, v10, [Ljava/lang/String;

    const-string v1, "no-cache"

    aput-object v1, v6, v3

    const-string v1, "no-store"

    aput-object v1, v6, v2

    const/4 v1, 0x2

    const-string v4, "max-age=0"

    aput-object v4, v6, v1

    move v4, v3

    :goto_3
    if-ge v4, v10, :cond_6

    aget-object v7, v6, v4

    .line 1202083
    array-length v8, v5

    move v1, v3

    :goto_4
    if-ge v1, v8, :cond_5

    aget-object v9, v5, v1

    .line 1202084
    invoke-virtual {v9, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    move v1, v3

    .line 1202085
    goto :goto_1

    .line 1202086
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1202087
    :cond_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    :cond_6
    move v1, v2

    .line 1202088
    goto :goto_1

    :cond_7
    iget-object v4, v0, LX/3Dd;->c:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2WF;Ljava/io/OutputStream;)J
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1202039
    invoke-static {p0}, LX/7Ok;->b(LX/7Ok;)LX/3Da;

    move-result-object v0

    .line 1202040
    if-nez v0, :cond_0

    .line 1202041
    iget-object v0, p0, LX/7Ok;->d:LX/7Oi;

    invoke-interface {v0, p1, p2}, LX/7Oi;->a(LX/2WF;Ljava/io/OutputStream;)J

    move-result-wide v0

    .line 1202042
    :goto_0
    return-wide v0

    .line 1202043
    :cond_0
    iget-wide v4, p1, LX/2WF;->a:J

    .line 1202044
    iget-wide v0, p1, LX/2WF;->b:J

    iget-object v3, p0, LX/7Ok;->g:LX/3Dd;

    iget-wide v6, v3, LX/3Dd;->a:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_1

    .line 1202045
    new-instance v0, LX/2WF;

    iget-wide v6, p1, LX/2WF;->a:J

    iget-object v1, p0, LX/7Ok;->g:LX/3Dd;

    iget-wide v8, v1, LX/3Dd;->a:J

    invoke-direct {v0, v6, v7, v8, v9}, LX/2WF;-><init>(JJ)V

    move-object p1, v0

    :cond_1
    move-object v1, v2

    .line 1202046
    :cond_2
    :goto_1
    invoke-virtual {p1, v4, v5}, LX/2WF;->a(J)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1202047
    new-instance v3, LX/2WF;

    iget-wide v6, p1, LX/2WF;->b:J

    invoke-direct {v3, v4, v5, v6, v7}, LX/2WF;-><init>(JJ)V

    .line 1202048
    if-nez v1, :cond_3

    .line 1202049
    invoke-static {p0}, LX/7Ok;->b(LX/7Ok;)LX/3Da;

    move-result-object v0

    invoke-interface {v0}, LX/3Da;->g()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/2WF;->a(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 1202050
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1202051
    iget-wide v6, v3, LX/2WF;->b:J

    .line 1202052
    invoke-direct {p0, p2, v3}, LX/7Ok;->a(Ljava/io/OutputStream;LX/2WF;)J

    move-result-wide v8

    add-long/2addr v4, v8

    .line 1202053
    :goto_3
    cmp-long v0, v6, v4

    if-eqz v0, :cond_2

    move-object v1, v2

    .line 1202054
    goto :goto_1

    .line 1202055
    :cond_3
    invoke-virtual {v3, v1}, LX/2WF;->a(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 1202056
    :cond_4
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2WF;

    .line 1202057
    invoke-virtual {v0, v4, v5}, LX/2WF;->a(J)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1202058
    iget-wide v6, v0, LX/2WF;->a:J

    .line 1202059
    new-instance v0, LX/2WF;

    invoke-direct {v0, v4, v5, v6, v7}, LX/2WF;-><init>(JJ)V

    invoke-direct {p0, p2, v0}, LX/7Ok;->a(Ljava/io/OutputStream;LX/2WF;)J

    move-result-wide v8

    add-long/2addr v4, v8

    goto :goto_3

    .line 1202060
    :cond_5
    iget-wide v6, v0, LX/2WF;->b:J

    .line 1202061
    new-instance v3, LX/2WF;

    iget-wide v8, v0, LX/2WF;->b:J

    invoke-direct {v3, v4, v5, v8, v9}, LX/2WF;-><init>(JJ)V

    invoke-direct {p0, p2, v3}, LX/7Ok;->b(Ljava/io/OutputStream;LX/2WF;)J

    move-result-wide v8

    add-long/2addr v4, v8

    goto :goto_3

    .line 1202062
    :cond_6
    iget-wide v0, p1, LX/2WF;->a:J

    sub-long v0, v4, v0

    goto :goto_0
.end method

.method public final a()LX/3Dd;
    .locals 1

    .prologue
    .line 1202037
    invoke-static {p0}, LX/7Ok;->b(LX/7Ok;)LX/3Da;

    .line 1202038
    iget-object v0, p0, LX/7Ok;->g:LX/3Dd;

    return-object v0
.end method
