.class public LX/6oH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6oB;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Ljava/security/KeyStore;

.field private final d:Ljava/security/KeyPairGenerator;

.field public final e:Ljava/security/KeyFactory;

.field private final f:LX/6o4;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljavax/crypto/Cipher;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/6ny;

.field public final i:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1148070
    const-class v0, LX/6oH;

    sput-object v0, LX/6oH;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/security/KeyStore;Ljava/security/KeyPairGenerator;Ljava/security/KeyFactory;LX/6o4;LX/0Or;LX/6ny;Ljava/lang/String;)V
    .locals 0
    .param p7    # LX/6ny;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/security/KeyStore;",
            "Ljava/security/KeyPairGenerator;",
            "Ljava/security/KeyFactory;",
            "LX/6o4;",
            "LX/0Or",
            "<",
            "Ljavax/crypto/Cipher;",
            ">;",
            "Lcom/facebook/payments/auth/fingerprint/FingerprintStorage;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1148059
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148060
    iput-object p1, p0, LX/6oH;->b:Landroid/content/Context;

    .line 1148061
    iput-object p2, p0, LX/6oH;->c:Ljava/security/KeyStore;

    .line 1148062
    iput-object p3, p0, LX/6oH;->d:Ljava/security/KeyPairGenerator;

    .line 1148063
    iput-object p4, p0, LX/6oH;->e:Ljava/security/KeyFactory;

    .line 1148064
    iput-object p5, p0, LX/6oH;->f:LX/6o4;

    .line 1148065
    iput-object p6, p0, LX/6oH;->g:LX/0Or;

    .line 1148066
    iput-object p7, p0, LX/6oH;->h:LX/6ny;

    .line 1148067
    iput-object p8, p0, LX/6oH;->i:Ljava/lang/String;

    .line 1148068
    invoke-direct {p0}, LX/6oH;->b()V

    .line 1148069
    return-void
.end method

.method public static a$redex0(LX/6oH;Ljava/lang/String;Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;I)V
    .locals 9

    .prologue
    .line 1148054
    iget-object v0, p0, LX/6oH;->f:LX/6o4;

    invoke-direct {p0}, LX/6oH;->d()Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    move-result-object v1

    new-instance v2, LX/6oG;

    invoke-direct {v2, p0, p1, p2, p3}, LX/6oG;-><init>(LX/6oH;Ljava/lang/String;Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;I)V

    const/4 v6, 0x0

    .line 1148055
    new-instance v3, Landroid/os/CancellationSignal;

    invoke-direct {v3}, Landroid/os/CancellationSignal;-><init>()V

    iput-object v3, v0, LX/6o4;->b:Landroid/os/CancellationSignal;

    .line 1148056
    iput-boolean v6, v0, LX/6o4;->c:Z

    .line 1148057
    iget-object v3, v0, LX/6o4;->a:LX/6oL;

    invoke-virtual {v3}, LX/6oL;->a()Landroid/hardware/fingerprint/FingerprintManager;

    move-result-object v3

    iget-object v5, v0, LX/6o4;->b:Landroid/os/CancellationSignal;

    new-instance v7, LX/6o3;

    invoke-direct {v7, v0, v2}, LX/6o3;-><init>(LX/6o4;LX/6oG;)V

    const/4 v8, 0x0

    move-object v4, v1

    invoke-virtual/range {v3 .. v8}, Landroid/hardware/fingerprint/FingerprintManager;->authenticate(Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;Landroid/os/CancellationSignal;ILandroid/hardware/fingerprint/FingerprintManager$AuthenticationCallback;Landroid/os/Handler;)V

    .line 1148058
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1148049
    :try_start_0
    iget-object v0, p0, LX/6oH;->c:Ljava/security/KeyStore;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/security/KeyStore;->load(Ljava/security/KeyStore$LoadStoreParameter;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1148050
    return-void

    .line 1148051
    :catch_0
    move-exception v0

    .line 1148052
    :goto_0
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1148053
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private d()Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;
    .locals 5

    .prologue
    .line 1148071
    :try_start_0
    invoke-virtual {p0}, LX/6oH;->c()LX/6oJ;

    .line 1148072
    iget-object v0, p0, LX/6oH;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/crypto/Cipher;

    .line 1148073
    const/4 v1, 0x2

    .line 1148074
    iget-object v2, p0, LX/6oH;->c:Ljava/security/KeyStore;

    iget-object v3, p0, LX/6oH;->i:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object v2

    check-cast v2, Ljava/security/PrivateKey;

    move-object v2, v2

    .line 1148075
    invoke-virtual {v0, v1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 1148076
    new-instance v1, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;

    invoke-direct {v1, v0}, Landroid/hardware/fingerprint/FingerprintManager$CryptoObject;-><init>(Ljavax/crypto/Cipher;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 1148077
    :catch_0
    move-exception v0

    .line 1148078
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to create the crypto object"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1148047
    iget-object v0, p0, LX/6oH;->f:LX/6o4;

    invoke-virtual {v0}, LX/6o4;->a()V

    .line 1148048
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1148027
    if-nez p2, :cond_0

    .line 1148028
    iget-object v0, p0, LX/6oH;->h:LX/6ny;

    invoke-virtual {v0, p1}, LX/6ny;->b(Ljava/lang/String;)V

    .line 1148029
    :goto_0
    return-void

    .line 1148030
    :cond_0
    invoke-virtual {p0}, LX/6oH;->c()LX/6oJ;

    .line 1148031
    :try_start_0
    iget-object v0, p0, LX/6oH;->h:LX/6ny;

    .line 1148032
    iget-object v1, p0, LX/6oH;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljavax/crypto/Cipher;

    .line 1148033
    const/4 v2, 0x1

    .line 1148034
    iget-object v3, p0, LX/6oH;->c:Ljava/security/KeyStore;

    iget-object v7, p0, LX/6oH;->i:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v3

    invoke-virtual {v3}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v3

    .line 1148035
    new-instance v7, Ljava/security/spec/X509EncodedKeySpec;

    invoke-interface {v3}, Ljava/security/PublicKey;->getEncoded()[B

    move-result-object v3

    invoke-direct {v7, v3}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V

    .line 1148036
    iget-object v3, p0, LX/6oH;->e:Ljava/security/KeyFactory;

    invoke-virtual {v3, v7}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v3

    move-object v3, v3

    .line 1148037
    invoke-virtual {v1, v2, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 1148038
    invoke-static {p2}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v2

    .line 1148039
    invoke-virtual {v2}, LX/673;->f()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v1

    invoke-static {v1}, LX/673;->a([B)LX/673;

    move-result-object v1

    invoke-virtual {v1}, LX/673;->b()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1148040
    if-nez v1, :cond_1

    .line 1148041
    invoke-virtual {v0, p1}, LX/6ny;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1148042
    :goto_1
    goto :goto_0

    .line 1148043
    :catch_0
    move-exception v0

    .line 1148044
    sget-object v1, LX/6oH;->a:Ljava/lang/Class;

    const-string v2, "Failed to write value for %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p1, v3, v4

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1148045
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to put. key=%s, value=%s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p1, v3, v4

    aput-object p2, v3, v5

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1148046
    :cond_1
    iget-object v2, v0, LX/6ny;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    invoke-static {v0, p1}, LX/6ny;->c(LX/6ny;Ljava/lang/String;)LX/0Tn;

    move-result-object v3

    invoke-interface {v2, v3, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_1
.end method

.method public final c()LX/6oJ;
    .locals 6

    .prologue
    .line 1148013
    :try_start_0
    iget-object v0, p0, LX/6oH;->c:Ljava/security/KeyStore;

    iget-object v1, p0, LX/6oH;->i:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object v1

    .line 1148014
    iget-object v0, p0, LX/6oH;->c:Ljava/security/KeyStore;

    iget-object v2, p0, LX/6oH;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 1148015
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1148016
    :try_start_1
    iget-object v0, p0, LX/6oH;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/crypto/Cipher;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 1148017
    sget-object v0, LX/6oJ;->VALID:LX/6oJ;
    :try_end_1
    .catch Landroid/security/keystore/KeyPermanentlyInvalidatedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1148018
    :goto_0
    return-object v0

    .line 1148019
    :catch_0
    :try_start_2
    sget-object v0, LX/6oJ;->INVALID:LX/6oJ;

    .line 1148020
    :goto_1
    iget-object v1, p0, LX/6oH;->h:LX/6ny;

    .line 1148021
    iget-object v2, v1, LX/6ny;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    iget-object v3, v1, LX/6ny;->b:LX/0Tn;

    invoke-interface {v2, v3}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1148022
    iget-object v1, p0, LX/6oH;->d:Ljava/security/KeyPairGenerator;

    new-instance v2, Landroid/security/keystore/KeyGenParameterSpec$Builder;

    iget-object v3, p0, LX/6oH;->i:Ljava/lang/String;

    const/4 v4, 0x3

    invoke-direct {v2, v3, v4}, Landroid/security/keystore/KeyGenParameterSpec$Builder;-><init>(Ljava/lang/String;I)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "ECB"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Landroid/security/keystore/KeyGenParameterSpec$Builder;->setBlockModes([Ljava/lang/String;)Landroid/security/keystore/KeyGenParameterSpec$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/security/keystore/KeyGenParameterSpec$Builder;->setUserAuthenticationRequired(Z)Landroid/security/keystore/KeyGenParameterSpec$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "PKCS1Padding"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Landroid/security/keystore/KeyGenParameterSpec$Builder;->setEncryptionPaddings([Ljava/lang/String;)Landroid/security/keystore/KeyGenParameterSpec$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/security/keystore/KeyGenParameterSpec$Builder;->build()Landroid/security/keystore/KeyGenParameterSpec;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/KeyPairGenerator;->initialize(Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 1148023
    iget-object v1, p0, LX/6oH;->d:Ljava/security/KeyPairGenerator;

    invoke-virtual {v1}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;
    :try_end_2
    .catch Ljava/security/GeneralSecurityException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 1148024
    :catch_1
    move-exception v0

    .line 1148025
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1148026
    :cond_0
    :try_start_3
    sget-object v0, LX/6oJ;->EMPTY:LX/6oJ;
    :try_end_3
    .catch Ljava/security/GeneralSecurityException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1
.end method
