.class public LX/7NY;
.super LX/2oy;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/soundwave/SoundWaveView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1200378
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7NY;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1200379
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1200380
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7NY;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200381
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1200382
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200383
    const v0, 0x7f03138b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1200384
    const v0, 0x7f0d2d2d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/soundwave/SoundWaveView;

    iput-object v0, p0, LX/7NY;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    .line 1200385
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7NX;

    invoke-direct {v1, p0}, LX/7NX;-><init>(LX/7NY;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1200386
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 1

    .prologue
    .line 1200387
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1200388
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1200389
    iget-object p1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, p1

    .line 1200390
    invoke-virtual {p0, v0}, LX/7NY;->a(LX/2qV;)V

    .line 1200391
    return-void
.end method

.method public a(LX/2qV;)V
    .locals 2

    .prologue
    .line 1200392
    sget-object v0, LX/2qV;->PLAYING:LX/2qV;

    if-ne p1, v0, :cond_0

    .line 1200393
    iget-object v0, p0, LX/7NY;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    invoke-virtual {v0}, Lcom/facebook/widget/soundwave/SoundWaveView;->a()V

    .line 1200394
    iget-object v0, p0, LX/7NY;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/soundwave/SoundWaveView;->setVisibility(I)V

    .line 1200395
    :goto_0
    return-void

    .line 1200396
    :cond_0
    iget-object v0, p0, LX/7NY;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/soundwave/SoundWaveView;->setVisibility(I)V

    .line 1200397
    iget-object v0, p0, LX/7NY;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    invoke-virtual {v0}, Lcom/facebook/widget/soundwave/SoundWaveView;->b()V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1200398
    iget-object v0, p0, LX/7NY;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/soundwave/SoundWaveView;->setVisibility(I)V

    .line 1200399
    iget-object v0, p0, LX/7NY;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    invoke-virtual {v0}, Lcom/facebook/widget/soundwave/SoundWaveView;->b()V

    .line 1200400
    return-void
.end method
