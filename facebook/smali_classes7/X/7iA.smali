.class public final LX/7iA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4BY;

.field public final synthetic b:Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;LX/4BY;)V
    .locals 0

    .prologue
    .line 1227137
    iput-object p1, p0, LX/7iA;->b:Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;

    iput-object p2, p0, LX/7iA;->a:LX/4BY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1227138
    iget-object v0, p0, LX/7iA;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1227139
    iget-object v0, p0, LX/7iA;->b:Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;

    invoke-virtual {v0}, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->finish()V

    .line 1227140
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1227141
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1227142
    invoke-direct {p0}, LX/7iA;->a()V

    .line 1227143
    :goto_0
    return-void

    .line 1227144
    :cond_0
    iget-object v0, p0, LX/7iA;->b:Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;

    iget-object v0, v0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->w:LX/17Y;

    iget-object v1, p0, LX/7iA;->b:Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;

    invoke-interface {v0, v1, p1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1227145
    iget-object v1, p0, LX/7iA;->b:Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;

    iget-object v1, v1, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->u:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/7iA;->b:Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1227146
    iget-object v0, p0, LX/7iA;->b:Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;

    iget-object v0, v0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->r:LX/03V;

    sget-object v1, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1227147
    invoke-direct {p0}, LX/7iA;->a()V

    .line 1227148
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1227149
    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, LX/7iA;->a(Ljava/lang/String;)V

    return-void
.end method
