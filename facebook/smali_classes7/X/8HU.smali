.class public final enum LX/8HU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8HU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8HU;

.field public static final enum NORMAL_TO_SHARED:LX/8HU;

.field public static final enum NO_CONVERSION:LX/8HU;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1321034
    new-instance v0, LX/8HU;

    const-string v1, "NO_CONVERSION"

    invoke-direct {v0, v1, v2}, LX/8HU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8HU;->NO_CONVERSION:LX/8HU;

    .line 1321035
    new-instance v0, LX/8HU;

    const-string v1, "NORMAL_TO_SHARED"

    invoke-direct {v0, v1, v3}, LX/8HU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8HU;->NORMAL_TO_SHARED:LX/8HU;

    .line 1321036
    const/4 v0, 0x2

    new-array v0, v0, [LX/8HU;

    sget-object v1, LX/8HU;->NO_CONVERSION:LX/8HU;

    aput-object v1, v0, v2

    sget-object v1, LX/8HU;->NORMAL_TO_SHARED:LX/8HU;

    aput-object v1, v0, v3

    sput-object v0, LX/8HU;->$VALUES:[LX/8HU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1321037
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8HU;
    .locals 1

    .prologue
    .line 1321038
    const-class v0, LX/8HU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8HU;

    return-object v0
.end method

.method public static values()[LX/8HU;
    .locals 1

    .prologue
    .line 1321039
    sget-object v0, LX/8HU;->$VALUES:[LX/8HU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8HU;

    return-object v0
.end method
