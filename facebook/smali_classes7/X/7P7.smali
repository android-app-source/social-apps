.class public abstract LX/7P7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7P5;


# instance fields
.field public final synthetic b:Lcom/facebook/video/server/NetworkRangeWriter;


# direct methods
.method public constructor <init>(Lcom/facebook/video/server/NetworkRangeWriter;)V
    .locals 0

    .prologue
    .line 1202471
    iput-object p1, p0, LX/7P7;->b:Lcom/facebook/video/server/NetworkRangeWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/15D;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/15D",
            "<",
            "LX/4nJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1202472
    invoke-virtual {p0}, LX/7P7;->b()Lorg/apache/http/client/methods/HttpRequestBase;

    move-result-object v0

    .line 1202473
    iget-object v1, p0, LX/7P7;->b:Lcom/facebook/video/server/NetworkRangeWriter;

    invoke-static {v1, v0}, Lcom/facebook/video/server/NetworkRangeWriter;->a$redex0(Lcom/facebook/video/server/NetworkRangeWriter;Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 1202474
    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpRequestBase;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 1202475
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v1

    .line 1202476
    iput-object v0, v1, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 1202477
    move-object v1, v1

    .line 1202478
    iget-object v2, p0, LX/7P7;->b:Lcom/facebook/video/server/NetworkRangeWriter;

    iget-object v2, v2, Lcom/facebook/video/server/NetworkRangeWriter;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 1202479
    iput-object v2, v1, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1202480
    move-object v1, v1

    .line 1202481
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getHeadersForVideo-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpRequestBase;->getMethod()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1202482
    iput-object v0, v1, LX/15E;->c:Ljava/lang/String;

    .line 1202483
    move-object v0, v1

    .line 1202484
    iget-object v1, p0, LX/7P7;->b:Lcom/facebook/video/server/NetworkRangeWriter;

    iget-object v1, v1, Lcom/facebook/video/server/NetworkRangeWriter;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1202485
    iput-object v1, v0, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1202486
    move-object v0, v0

    .line 1202487
    new-instance v1, LX/7P9;

    iget-object v2, p0, LX/7P7;->b:Lcom/facebook/video/server/NetworkRangeWriter;

    invoke-direct {v1, v2}, LX/7P9;-><init>(Lcom/facebook/video/server/NetworkRangeWriter;)V

    .line 1202488
    iput-object v1, v0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 1202489
    move-object v0, v0

    .line 1202490
    sget-object v1, LX/14P;->RETRY_SAFE:LX/14P;

    .line 1202491
    iput-object v1, v0, LX/15E;->j:LX/14P;

    .line 1202492
    move-object v0, v0

    .line 1202493
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    return-object v0
.end method

.method public abstract b()Lorg/apache/http/client/methods/HttpRequestBase;
.end method
