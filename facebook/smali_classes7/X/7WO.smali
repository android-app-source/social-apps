.class public abstract LX/7WO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0Xl;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Xl;LX/0Ot;LX/0Or;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Xl;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1216322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1216323
    iput-object p1, p0, LX/7WO;->a:Landroid/content/Context;

    .line 1216324
    iput-object p2, p0, LX/7WO;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1216325
    iput-object p3, p0, LX/7WO;->c:LX/0Xl;

    .line 1216326
    iput-object p4, p0, LX/7WO;->d:LX/0Ot;

    .line 1216327
    iput-object p5, p0, LX/7WO;->e:LX/0Or;

    .line 1216328
    iput-object p6, p0, LX/7WO;->f:Lcom/facebook/content/SecureContextHelper;

    .line 1216329
    return-void
.end method
