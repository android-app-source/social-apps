.class public LX/728;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6wK",
        "<",
        "LX/729;",
        "Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1163989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163990
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;)LX/0Px;
    .locals 2

    .prologue
    .line 1163991
    sget-object v0, LX/729;->SHIPPING_ADDRESSES:LX/729;

    sget-object v1, LX/729;->SHIPPING_SECURITY_MESSAGE:LX/729;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
