.class public final enum LX/74j;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/74j;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/74j;

.field public static final enum CREATIVECAM_MEDIA:LX/74j;

.field public static final enum DEFAULT:LX/74j;

.field public static final enum REMOTE_MEDIA:LX/74j;

.field public static final enum SINGLE_SHOT_CAMERA:LX/74j;


# instance fields
.field private mValue:J


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1168355
    new-instance v0, LX/74j;

    const-string v1, "CREATIVECAM_MEDIA"

    const-wide/16 v2, -0x3

    invoke-direct {v0, v1, v4, v2, v3}, LX/74j;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LX/74j;->CREATIVECAM_MEDIA:LX/74j;

    .line 1168356
    new-instance v0, LX/74j;

    const-string v1, "REMOTE_MEDIA"

    const-wide/16 v2, -0x2

    invoke-direct {v0, v1, v5, v2, v3}, LX/74j;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LX/74j;->REMOTE_MEDIA:LX/74j;

    .line 1168357
    new-instance v0, LX/74j;

    const-string v1, "SINGLE_SHOT_CAMERA"

    const-wide/16 v2, -0x1

    invoke-direct {v0, v1, v6, v2, v3}, LX/74j;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LX/74j;->SINGLE_SHOT_CAMERA:LX/74j;

    .line 1168358
    new-instance v0, LX/74j;

    const-string v1, "DEFAULT"

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v7, v2, v3}, LX/74j;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LX/74j;->DEFAULT:LX/74j;

    .line 1168359
    const/4 v0, 0x4

    new-array v0, v0, [LX/74j;

    sget-object v1, LX/74j;->CREATIVECAM_MEDIA:LX/74j;

    aput-object v1, v0, v4

    sget-object v1, LX/74j;->REMOTE_MEDIA:LX/74j;

    aput-object v1, v0, v5

    sget-object v1, LX/74j;->SINGLE_SHOT_CAMERA:LX/74j;

    aput-object v1, v0, v6

    sget-object v1, LX/74j;->DEFAULT:LX/74j;

    aput-object v1, v0, v7

    sput-object v0, LX/74j;->$VALUES:[LX/74j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    .prologue
    .line 1168360
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1168361
    iput-wide p3, p0, LX/74j;->mValue:J

    .line 1168362
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/74j;
    .locals 1

    .prologue
    .line 1168363
    const-class v0, LX/74j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/74j;

    return-object v0
.end method

.method public static values()[LX/74j;
    .locals 1

    .prologue
    .line 1168364
    sget-object v0, LX/74j;->$VALUES:[LX/74j;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/74j;

    return-object v0
.end method


# virtual methods
.method public final getValue()J
    .locals 2

    .prologue
    .line 1168365
    iget-wide v0, p0, LX/74j;->mValue:J

    return-wide v0
.end method
