.class public final LX/84R;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/84S;


# direct methods
.method public constructor <init>(LX/84S;)V
    .locals 0

    .prologue
    .line 1290933
    iput-object p1, p0, LX/84R;->a:LX/84S;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1290934
    iget-object v0, p0, LX/84R;->a:LX/84S;

    iget-object v0, v0, LX/84S;->c:LX/2iT;

    iget-object v1, p0, LX/84R;->a:LX/84S;

    iget-wide v2, v1, LX/84S;->a:J

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v1, v4}, LX/2hY;->a(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 1290935
    iget-object v0, p0, LX/84R;->a:LX/84S;

    iget-object v0, v0, LX/84S;->c:LX/2iT;

    iget-object v0, v0, LX/2hY;->b:LX/2hZ;

    invoke-virtual {v0, p1}, LX/2hZ;->a(Ljava/lang/Throwable;)V

    .line 1290936
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1290937
    iget-object v0, p0, LX/84R;->a:LX/84S;

    iget-object v0, v0, LX/84S;->c:LX/2iT;

    iget-object v1, p0, LX/84R;->a:LX/84S;

    iget-wide v2, v1, LX/84S;->a:J

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v1, v4}, LX/2hY;->a(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 1290938
    return-void
.end method
