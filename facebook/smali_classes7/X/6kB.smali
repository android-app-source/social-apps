.class public LX/6kB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final jsonBlob:Ljava/lang/String;

.field public final threadKey:LX/6l9;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1135197
    new-instance v0, LX/1sv;

    const-string v1, "DeltaPagesManagerEvent"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6kB;->b:LX/1sv;

    .line 1135198
    new-instance v0, LX/1sw;

    const-string v1, "threadKey"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kB;->c:LX/1sw;

    .line 1135199
    new-instance v0, LX/1sw;

    const-string v1, "jsonBlob"

    const/16 v2, 0xb

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6kB;->d:LX/1sw;

    .line 1135200
    sput-boolean v4, LX/6kB;->a:Z

    return-void
.end method

.method public constructor <init>(LX/6l9;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1135271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1135272
    iput-object p1, p0, LX/6kB;->threadKey:LX/6l9;

    .line 1135273
    iput-object p2, p0, LX/6kB;->jsonBlob:Ljava/lang/String;

    .line 1135274
    return-void
.end method

.method public static a(LX/6kB;)V
    .locals 4

    .prologue
    .line 1135268
    iget-object v0, p0, LX/6kB;->threadKey:LX/6l9;

    if-nez v0, :cond_0

    .line 1135269
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'threadKey\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6kB;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1135270
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1135239
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1135240
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 1135241
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1135242
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DeltaPagesManagerEvent"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1135243
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135244
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135245
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135246
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135247
    const-string v4, "threadKey"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135248
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135249
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135250
    iget-object v4, p0, LX/6kB;->threadKey:LX/6l9;

    if-nez v4, :cond_4

    .line 1135251
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135252
    :goto_3
    iget-object v4, p0, LX/6kB;->jsonBlob:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 1135253
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135254
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135255
    const-string v4, "jsonBlob"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135256
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135257
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135258
    iget-object v0, p0, LX/6kB;->jsonBlob:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 1135259
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135260
    :cond_0
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135261
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1135262
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1135263
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1135264
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1135265
    :cond_3
    const-string v0, ""

    goto/16 :goto_2

    .line 1135266
    :cond_4
    iget-object v4, p0, LX/6kB;->threadKey:LX/6l9;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1135267
    :cond_5
    iget-object v0, p0, LX/6kB;->jsonBlob:Ljava/lang/String;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1135227
    invoke-static {p0}, LX/6kB;->a(LX/6kB;)V

    .line 1135228
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1135229
    iget-object v0, p0, LX/6kB;->threadKey:LX/6l9;

    if-eqz v0, :cond_0

    .line 1135230
    sget-object v0, LX/6kB;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135231
    iget-object v0, p0, LX/6kB;->threadKey:LX/6l9;

    invoke-virtual {v0, p1}, LX/6l9;->a(LX/1su;)V

    .line 1135232
    :cond_0
    iget-object v0, p0, LX/6kB;->jsonBlob:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1135233
    iget-object v0, p0, LX/6kB;->jsonBlob:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1135234
    sget-object v0, LX/6kB;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1135235
    iget-object v0, p0, LX/6kB;->jsonBlob:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1135236
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1135237
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1135238
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1135205
    if-nez p1, :cond_1

    .line 1135206
    :cond_0
    :goto_0
    return v0

    .line 1135207
    :cond_1
    instance-of v1, p1, LX/6kB;

    if-eqz v1, :cond_0

    .line 1135208
    check-cast p1, LX/6kB;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1135209
    if-nez p1, :cond_3

    .line 1135210
    :cond_2
    :goto_1
    move v0, v2

    .line 1135211
    goto :goto_0

    .line 1135212
    :cond_3
    iget-object v0, p0, LX/6kB;->threadKey:LX/6l9;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1135213
    :goto_2
    iget-object v3, p1, LX/6kB;->threadKey:LX/6l9;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1135214
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1135215
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135216
    iget-object v0, p0, LX/6kB;->threadKey:LX/6l9;

    iget-object v3, p1, LX/6kB;->threadKey:LX/6l9;

    invoke-virtual {v0, v3}, LX/6l9;->a(LX/6l9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1135217
    :cond_5
    iget-object v0, p0, LX/6kB;->jsonBlob:Ljava/lang/String;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1135218
    :goto_4
    iget-object v3, p1, LX/6kB;->jsonBlob:Ljava/lang/String;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1135219
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1135220
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1135221
    iget-object v0, p0, LX/6kB;->jsonBlob:Ljava/lang/String;

    iget-object v3, p1, LX/6kB;->jsonBlob:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1135222
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1135223
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1135224
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1135225
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1135226
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1135204
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1135201
    sget-boolean v0, LX/6kB;->a:Z

    .line 1135202
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6kB;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1135203
    return-object v0
.end method
