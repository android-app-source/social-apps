.class public LX/8Ul;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/8TS;

.field public final c:LX/8TD;

.field public final d:Ljava/util/concurrent/Executor;

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Tn;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/8Sp;


# direct methods
.method public constructor <init>(LX/8TS;LX/0tX;LX/8TD;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1351233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1351234
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1351235
    iput-object v0, p0, LX/8Ul;->e:LX/0Ot;

    .line 1351236
    iput-object p1, p0, LX/8Ul;->b:LX/8TS;

    .line 1351237
    iput-object p2, p0, LX/8Ul;->a:LX/0tX;

    .line 1351238
    iput-object p3, p0, LX/8Ul;->c:LX/8TD;

    .line 1351239
    iput-object p4, p0, LX/8Ul;->d:Ljava/util/concurrent/Executor;

    .line 1351240
    return-void
.end method

.method public static a(LX/0QB;)LX/8Ul;
    .locals 7

    .prologue
    .line 1351241
    const-class v1, LX/8Ul;

    monitor-enter v1

    .line 1351242
    :try_start_0
    sget-object v0, LX/8Ul;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1351243
    sput-object v2, LX/8Ul;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1351244
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1351245
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1351246
    new-instance p0, LX/8Ul;

    invoke-static {v0}, LX/8TS;->a(LX/0QB;)LX/8TS;

    move-result-object v3

    check-cast v3, LX/8TS;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/8TD;->a(LX/0QB;)LX/8TD;

    move-result-object v5

    check-cast v5, LX/8TD;

    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v3, v4, v5, v6}, LX/8Ul;-><init>(LX/8TS;LX/0tX;LX/8TD;Ljava/util/concurrent/Executor;)V

    .line 1351247
    const/16 v3, 0x307a

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 1351248
    iput-object v3, p0, LX/8Ul;->e:LX/0Ot;

    .line 1351249
    move-object v0, p0

    .line 1351250
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1351251
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8Ul;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1351252
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1351253
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/8Ul;LX/8TP;IZ)V
    .locals 7

    .prologue
    .line 1351254
    new-instance v0, LX/4GT;

    invoke-direct {v0}, LX/4GT;-><init>()V

    .line 1351255
    sget-object v1, LX/8Uk;->a:[I

    .line 1351256
    iget-object v2, p1, LX/8TP;->a:LX/8Ta;

    move-object v2, v2

    .line 1351257
    invoke-virtual {v2}, LX/8Ta;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1351258
    const-string v1, "QuicksilverGameGQLMutations"

    const-string v2, "Unsupported context %s for leaderboard write"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, LX/8TP;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1351259
    :goto_0
    :pswitch_0
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1351260
    const-string v2, "score"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1351261
    move-object v1, v0

    .line 1351262
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1351263
    const-string v3, "send_admin"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1351264
    move-object v1, v1

    .line 1351265
    iget-object v2, p0, LX/8Ul;->b:LX/8TS;

    .line 1351266
    iget-object v3, v2, LX/8TS;->q:Ljava/lang/String;

    move-object v2, v3

    .line 1351267
    const-string v3, "session_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1351268
    move-object v1, v1

    .line 1351269
    invoke-static {p0}, LX/8Ul;->f(LX/8Ul;)Ljava/lang/String;

    move-result-object v2

    .line 1351270
    const-string v3, "game_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1351271
    new-instance v1, LX/8UY;

    invoke-direct {v1}, LX/8UY;-><init>()V

    move-object v1, v1

    .line 1351272
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1351273
    new-instance v0, LX/8Ua;

    invoke-direct {v0}, LX/8Ua;-><init>()V

    invoke-virtual {v0}, LX/8Ua;->a()Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;

    move-result-object v0

    .line 1351274
    new-instance v2, LX/3G1;

    invoke-direct {v2}, LX/3G1;-><init>()V

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/3G1;->a(LX/399;)LX/3G1;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 1351275
    iput-wide v2, v0, LX/3G2;->d:J

    .line 1351276
    move-object v0, v0

    .line 1351277
    const/16 v1, 0x64

    .line 1351278
    iput v1, v0, LX/3G2;->f:I

    .line 1351279
    move-object v0, v0

    .line 1351280
    invoke-virtual {v0}, LX/3G2;->a()LX/3G3;

    move-result-object v0

    check-cast v0, LX/3G4;

    .line 1351281
    iget-object v1, p0, LX/8Ul;->a:LX/0tX;

    sget-object v2, LX/3Fz;->c:LX/3Fz;

    invoke-virtual {v1, v0, v2}, LX/0tX;->a(LX/3G4;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1351282
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1351283
    sget-object v2, LX/8TE;->EXTRAS_THREAD_ID:LX/8TE;

    iget-object v2, v2, LX/8TE;->value:Ljava/lang/String;

    .line 1351284
    invoke-virtual {p1}, LX/8TP;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p1, LX/8TP;->b:Ljava/lang/String;

    :goto_1
    move-object v3, v3

    .line 1351285
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1351286
    sget-object v2, LX/8TE;->EXTRAS_CONTEXT:LX/8TE;

    iget-object v2, v2, LX/8TE;->value:Ljava/lang/String;

    .line 1351287
    iget-object v3, p1, LX/8TP;->a:LX/8Ta;

    move-object v3, v3

    .line 1351288
    invoke-virtual {v3}, LX/8Ta;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1351289
    sget-object v2, LX/8TE;->EXTRAS_CONTEXT_ID:LX/8TE;

    iget-object v2, v2, LX/8TE;->value:Ljava/lang/String;

    .line 1351290
    iget-object v3, p1, LX/8TP;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1351291
    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1351292
    sget-object v2, LX/8TE;->EXTRAS_SCORE:LX/8TE;

    iget-object v2, v2, LX/8TE;->value:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1351293
    sget-object v2, LX/8TE;->EXTRAS_SEND_ADMIN_MESSAGE:LX/8TE;

    iget-object v2, v2, LX/8TE;->value:Ljava/lang/String;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1351294
    new-instance v2, LX/8Uj;

    invoke-direct {v2, p0, v1}, LX/8Uj;-><init>(LX/8Ul;Ljava/util/Map;)V

    iget-object v1, p0, LX/8Ul;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1351295
    return-void

    .line 1351296
    :pswitch_1
    iget-object v1, p1, LX/8TP;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1351297
    const-string v2, "thread_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1351298
    goto/16 :goto_0

    .line 1351299
    :pswitch_2
    iget-object v1, p1, LX/8TP;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1351300
    const-string v2, "group_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1351301
    goto/16 :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static f(LX/8Ul;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1351302
    iget-object v0, p0, LX/8Ul;->b:LX/8TS;

    .line 1351303
    iget-object v1, v0, LX/8TS;->e:LX/8TO;

    move-object v0, v1

    .line 1351304
    if-eqz v0, :cond_0

    .line 1351305
    iget-object v1, v0, LX/8TO;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1351306
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1351307
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No game information or game id found"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1351308
    :cond_1
    iget-object v1, v0, LX/8TO;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1351309
    return-object v0
.end method
