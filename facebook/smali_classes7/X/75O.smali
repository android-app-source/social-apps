.class public final LX/75O;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Lcom/facebook/photos/base/tagging/Tag;

.field private final d:LX/6Z0;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/photos/base/tagging/Tag;LX/6Z0;)V
    .locals 0

    .prologue
    .line 1169332
    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 1169333
    iput-object p1, p0, LX/75O;->a:Landroid/content/Context;

    .line 1169334
    iput-object p2, p0, LX/75O;->b:Ljava/lang/String;

    .line 1169335
    iput-object p3, p0, LX/75O;->c:Lcom/facebook/photos/base/tagging/Tag;

    .line 1169336
    iput-object p4, p0, LX/75O;->d:LX/6Z0;

    .line 1169337
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1169338
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1169339
    const-string v0, "%s = ? AND %s = ? AND %s = ? AND %s = ? AND %s = ?"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, LX/6Yy;->k:LX/0U1;

    .line 1169340
    iget-object p1, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, p1

    .line 1169341
    aput-object v2, v1, v3

    sget-object v2, LX/6Yy;->b:LX/0U1;

    .line 1169342
    iget-object p1, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, p1

    .line 1169343
    aput-object v2, v1, v4

    sget-object v2, LX/6Yy;->c:LX/0U1;

    .line 1169344
    iget-object p1, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, p1

    .line 1169345
    aput-object v2, v1, v5

    sget-object v2, LX/6Yy;->d:LX/0U1;

    .line 1169346
    iget-object p1, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, p1

    .line 1169347
    aput-object v2, v1, v6

    sget-object v2, LX/6Yy;->e:LX/0U1;

    .line 1169348
    iget-object p1, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, p1

    .line 1169349
    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1169350
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, LX/75O;->b:Ljava/lang/String;

    aput-object v2, v1, v3

    iget-object v2, p0, LX/75O;->c:Lcom/facebook/photos/base/tagging/Tag;

    .line 1169351
    iget-object v3, v2, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v2, v3

    .line 1169352
    invoke-interface {v2}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->left:F

    invoke-static {v2}, LX/75Q;->b(F)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v2, p0, LX/75O;->c:Lcom/facebook/photos/base/tagging/Tag;

    .line 1169353
    iget-object v3, v2, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v2, v3

    .line 1169354
    invoke-interface {v2}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-static {v2}, LX/75Q;->b(F)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    iget-object v2, p0, LX/75O;->c:Lcom/facebook/photos/base/tagging/Tag;

    .line 1169355
    iget-object v3, v2, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v2, v3

    .line 1169356
    invoke-interface {v2}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->right:F

    invoke-static {v2}, LX/75Q;->b(F)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, LX/75O;->c:Lcom/facebook/photos/base/tagging/Tag;

    .line 1169357
    iget-object v3, v2, Lcom/facebook/photos/base/tagging/Tag;->a:Lcom/facebook/photos/base/tagging/TagTarget;

    move-object v2, v3

    .line 1169358
    invoke-interface {v2}, Lcom/facebook/photos/base/tagging/TagTarget;->d()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    invoke-static {v2}, LX/75Q;->b(F)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    .line 1169359
    iget-object v2, p0, LX/75O;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, LX/75O;->d:LX/6Z0;

    iget-object v3, v3, LX/6Z0;->d:Landroid/net/Uri;

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1169360
    const/4 v0, 0x0

    return-object v0
.end method
