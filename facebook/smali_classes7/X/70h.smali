.class public final enum LX/70h;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/70h;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/70h;

.field public static final enum ACTIVE:LX/70h;

.field public static final enum INACTIVE:LX/70h;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1162538
    new-instance v0, LX/70h;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v2}, LX/70h;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/70h;->ACTIVE:LX/70h;

    .line 1162539
    new-instance v0, LX/70h;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v3}, LX/70h;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/70h;->INACTIVE:LX/70h;

    .line 1162540
    const/4 v0, 0x2

    new-array v0, v0, [LX/70h;

    sget-object v1, LX/70h;->ACTIVE:LX/70h;

    aput-object v1, v0, v2

    sget-object v1, LX/70h;->INACTIVE:LX/70h;

    aput-object v1, v0, v3

    sput-object v0, LX/70h;->$VALUES:[LX/70h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1162543
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/70h;
    .locals 1

    .prologue
    .line 1162542
    const-class v0, LX/70h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/70h;

    return-object v0
.end method

.method public static values()[LX/70h;
    .locals 1

    .prologue
    .line 1162541
    sget-object v0, LX/70h;->$VALUES:[LX/70h;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/70h;

    return-object v0
.end method
