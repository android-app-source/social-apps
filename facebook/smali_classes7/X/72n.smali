.class public LX/72n;
.super LX/6vv;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6vv",
        "<",
        "Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerRunTimeData;",
        "Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;",
        "Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;",
        "Lcom/facebook/payments/picker/model/CoreClientData;",
        "LX/72x;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1164939
    invoke-direct {p0}, LX/6vv;-><init>()V

    .line 1164940
    return-void
.end method

.method public static a(LX/0QB;)LX/72n;
    .locals 3

    .prologue
    .line 1164941
    const-class v1, LX/72n;

    monitor-enter v1

    .line 1164942
    :try_start_0
    sget-object v0, LX/72n;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1164943
    sput-object v2, LX/72n;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1164944
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1164945
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1164946
    new-instance v0, LX/72n;

    invoke-direct {v0}, LX/72n;-><init>()V

    .line 1164947
    move-object v0, v0

    .line 1164948
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1164949
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/72n;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1164950
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1164951
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 1164952
    check-cast p1, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;

    .line 1164953
    new-instance v0, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerRunTimeData;

    invoke-direct {v0, p1}, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerRunTimeData;-><init>(Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 1164954
    check-cast p1, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;

    .line 1164955
    new-instance v0, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerRunTimeData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerRunTimeData;-><init>(Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)V

    return-object v0
.end method
