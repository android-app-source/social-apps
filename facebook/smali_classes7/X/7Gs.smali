.class public LX/7Gs;
.super Landroid/text/style/TypefaceSpan;
.source ""


# instance fields
.field private final a:Landroid/graphics/Typeface;

.field private final b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 1189954
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7Gs;-><init>(Ljava/lang/String;Landroid/graphics/Typeface;I)V

    .line 1189955
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/graphics/Typeface;I)V
    .locals 0

    .prologue
    .line 1189956
    invoke-direct {p0, p1}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    .line 1189957
    iput-object p2, p0, LX/7Gs;->a:Landroid/graphics/Typeface;

    .line 1189958
    iput p3, p0, LX/7Gs;->b:I

    .line 1189959
    return-void
.end method


# virtual methods
.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1189960
    iget-object v0, p0, LX/7Gs;->a:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1189961
    invoke-virtual {p1}, Landroid/text/TextPaint;->getFlags()I

    move-result v0

    iget v1, p0, LX/7Gs;->b:I

    or-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setFlags(I)V

    .line 1189962
    return-void
.end method

.method public final updateMeasureState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1189963
    iget-object v0, p0, LX/7Gs;->a:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1189964
    invoke-virtual {p1}, Landroid/text/TextPaint;->getFlags()I

    move-result v0

    iget v1, p0, LX/7Gs;->b:I

    or-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setFlags(I)V

    .line 1189965
    return-void
.end method
