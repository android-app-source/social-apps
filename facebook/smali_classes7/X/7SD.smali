.class public final LX/7SD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/61B;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/7SH;


# direct methods
.method public constructor <init>(LX/61B;LX/7SH;)V
    .locals 1

    .prologue
    .line 1208389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1208390
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7SD;->a:Ljava/util/List;

    .line 1208391
    iget-object v0, p0, LX/7SD;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1208392
    iput-object p2, p0, LX/7SD;->b:LX/7SH;

    .line 1208393
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/61B;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1208394
    iget-object v0, p0, LX/7SD;->a:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)LX/7SD;
    .locals 2

    .prologue
    .line 1208395
    iget-object v0, p0, LX/7SD;->a:Ljava/util/List;

    iget-object v1, p0, LX/7SD;->b:LX/7SH;

    invoke-virtual {v1, p1}, LX/7SH;->a(Landroid/net/Uri;)Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1208396
    return-object p0
.end method
