.class public final LX/7zE;
.super LX/7z4;
.source ""


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/7z8;


# direct methods
.method public constructor <init>(LX/7z8;Z)V
    .locals 0

    .prologue
    .line 1280856
    iput-object p1, p0, LX/7zE;->b:LX/7z8;

    iput-boolean p2, p0, LX/7zE;->a:Z

    invoke-direct {p0}, LX/7z4;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1280857
    iget-object v0, p0, LX/7zE;->b:LX/7z8;

    invoke-static {v0}, LX/7z8;->g(LX/7z8;)V

    .line 1280858
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1280859
    iget-object v0, p0, LX/7zE;->b:LX/7z8;

    invoke-static {v0, p1}, LX/7z8;->a$redex0(LX/7z8;I)V

    .line 1280860
    return-void
.end method

.method public final a(Ljava/lang/Exception;Z)V
    .locals 2

    .prologue
    .line 1280861
    iget-object v0, p0, LX/7zE;->b:LX/7z8;

    iget-object v0, v0, LX/7z8;->a:LX/7yw;

    .line 1280862
    iget-object v1, v0, LX/7yw;->f:LX/7yz;

    move-object v0, v1

    .line 1280863
    invoke-virtual {v0, p1}, LX/7yz;->a(Ljava/lang/Exception;)V

    .line 1280864
    iget-object v0, p0, LX/7zE;->b:LX/7z8;

    const-string v1, "Failed to complete POST request"

    invoke-static {v0, v1, p1, p2}, LX/7z8;->a$redex0(LX/7z8;Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 1280865
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1280866
    iget-object v0, p0, LX/7zE;->b:LX/7z8;

    iget-boolean v1, p0, LX/7zE;->a:Z

    .line 1280867
    :try_start_0
    invoke-static {v0, p1, v1}, LX/7z8;->b(LX/7z8;Ljava/lang/String;Z)LX/7zL;

    move-result-object v2

    .line 1280868
    if-eqz v1, :cond_0

    .line 1280869
    const/4 v3, 0x0

    invoke-static {v0, v3}, LX/7z8;->a$redex0(LX/7z8;I)V

    .line 1280870
    :cond_0
    iget-object v3, v0, LX/7z8;->b:LX/7yp;

    invoke-interface {v3, v2}, LX/7yp;->a(LX/7zL;)V
    :try_end_0
    .catch LX/7z7; {:try_start_0 .. :try_end_0} :catch_0

    .line 1280871
    :goto_0
    return-void

    .line 1280872
    :catch_0
    move-exception v2

    .line 1280873
    const-string v3, "Failed to parse result from POST request response"

    const/4 p0, 0x1

    invoke-static {v0, v3, v2, p0}, LX/7z8;->a$redex0(LX/7z8;Ljava/lang/String;Ljava/lang/Exception;Z)V

    goto :goto_0
.end method
