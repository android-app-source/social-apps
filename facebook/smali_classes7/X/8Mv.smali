.class public LX/8Mv;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1335502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1335503
    return-void
.end method

.method public static a(LX/0QB;)LX/8Mv;
    .locals 1

    .prologue
    .line 1335499
    new-instance v0, LX/8Mv;

    invoke-direct {v0}, LX/8Mv;-><init>()V

    .line 1335500
    move-object v0, v0

    .line 1335501
    return-object v0
.end method

.method public static final a(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Ljava/lang/String;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1335449
    invoke-static {p0}, LX/8Mv;->h(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    .line 1335450
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1335451
    :cond_0
    const/4 v0, 0x0

    .line 1335452
    :goto_0
    return-object v0

    .line 1335453
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->y:Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    move-object v0, v0

    .line 1335454
    iget-object v1, v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v1

    .line 1335455
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v2

    .line 1335456
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v3

    .line 1335457
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 1335458
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->g()Ljava/lang/String;

    move-result-object v5

    .line 1335459
    const/4 v0, 0x1

    .line 1335460
    invoke-virtual {v3, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1335461
    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1335462
    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1335463
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1335464
    :cond_3
    new-instance v2, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 1335465
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1335466
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v2, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/Integer;)LX/0m9;

    goto :goto_2

    .line 1335467
    :cond_4
    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final b(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1335488
    invoke-static {p0}, LX/8Mv;->h(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    .line 1335489
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1335490
    :cond_0
    const/4 v0, 0x0

    .line 1335491
    :goto_0
    return-object v0

    .line 1335492
    :cond_1
    new-instance v2, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v1}, LX/162;-><init>(LX/0mC;)V

    .line 1335493
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/TextParams;

    .line 1335494
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/TextParams;->l()Ljava/lang/String;

    move-result-object v0

    .line 1335495
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1335496
    invoke-virtual {v2, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 1335497
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1335498
    :cond_3
    invoke-virtual {v2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final c(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1335484
    invoke-static {p0}, LX/8Mv;->h(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    .line 1335485
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-static {v1}, LX/63w;->b(Lcom/facebook/photos/creativeediting/model/PersistableRect;)F

    move-result v1

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    invoke-static {v0}, LX/63w;->a(Lcom/facebook/photos/creativeediting/model/PersistableRect;)F

    move-result v0

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_1

    .line 1335486
    :cond_0
    const/4 v0, 0x1

    .line 1335487
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final d(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z
    .locals 1

    .prologue
    .line 1335480
    invoke-static {p0}, LX/8Mv;->h(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    .line 1335481
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->isRotated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1335482
    const/4 v0, 0x1

    .line 1335483
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final e(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Z
    .locals 2

    .prologue
    .line 1335476
    invoke-static {p0}, LX/8Mv;->h(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    .line 1335477
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/5iB;->e(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LX/5iL;->AE08bit:LX/5iL;

    invoke-virtual {v1}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1335478
    const/4 v0, 0x1

    .line 1335479
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/photos/upload/protocol/UploadPhotoParams;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1335468
    if-eqz p0, :cond_0

    .line 1335469
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->y:Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    move-object v0, v0

    .line 1335470
    if-nez v0, :cond_1

    .line 1335471
    :cond_0
    const/4 v0, 0x0

    .line 1335472
    :goto_0
    return-object v0

    .line 1335473
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->y:Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    move-object v0, v0

    .line 1335474
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;->a:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, p0

    .line 1335475
    goto :goto_0
.end method
