.class public LX/8KY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/8KY;


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public final b:Landroid/content/Context;

.field public final c:LX/0V8;

.field private final d:LX/0en;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0V8;LX/0en;LX/0Or;)V
    .locals 1
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/upload/gatekeeper/PersistProcessedFilesEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0V8;",
            "LX/0en;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1330410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1330411
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, LX/8KY;->a:Ljava/lang/Class;

    .line 1330412
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8KY;->f:Z

    .line 1330413
    iput-object p1, p0, LX/8KY;->b:Landroid/content/Context;

    .line 1330414
    iput-object p2, p0, LX/8KY;->c:LX/0V8;

    .line 1330415
    iput-object p3, p0, LX/8KY;->d:LX/0en;

    .line 1330416
    iput-object p4, p0, LX/8KY;->e:LX/0Or;

    .line 1330417
    return-void
.end method

.method private static a(LX/8KY;Ljava/io/File;ZILjava/lang/String;)LX/8KX;
    .locals 11
    .param p0    # LX/8KY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 1330487
    const/4 v1, 0x0

    .line 1330488
    if-eqz p1, :cond_3

    .line 1330489
    iget-object v7, p0, LX/8KY;->c:LX/0V8;

    invoke-virtual {v7}, LX/0V8;->b()V

    .line 1330490
    iget-object v7, p0, LX/8KY;->c:LX/0V8;

    sget-object v8, LX/0VA;->INTERNAL:LX/0VA;

    int-to-long v9, p3

    invoke-virtual {v7, v8, v9, v10}, LX/0V8;->a(LX/0VA;J)Z

    move-result v7

    if-nez v7, :cond_4

    const/4 v7, 0x1

    :goto_0
    move v0, v7

    .line 1330491
    if-eqz v0, :cond_3

    .line 1330492
    new-instance v2, Ljava/io/File;

    const-string v0, "%s.%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    aput-object p4, v3, v6

    invoke-static {v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1330493
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1330494
    if-eqz p2, :cond_1

    new-instance v0, LX/8KX;

    const/4 v3, 0x1

    invoke-direct {v0, p1, v2, v3}, LX/8KX;-><init>(Ljava/io/File;Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1330495
    :goto_1
    if-nez v0, :cond_0

    .line 1330496
    invoke-direct {p0}, LX/8KY;->d()V

    .line 1330497
    :cond_0
    :goto_2
    return-object v0

    .line 1330498
    :cond_1
    :try_start_1
    new-instance v0, LX/8KX;

    const/4 v3, 0x1

    invoke-direct {v0, v2, p1, v3}, LX/8KX;-><init>(Ljava/io/File;Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1330499
    :catch_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1330500
    invoke-direct {p0}, LX/8KY;->d()V

    move-object v0, v1

    goto :goto_2

    .line 1330501
    :catchall_0
    move-exception v0

    .line 1330502
    invoke-direct {p0}, LX/8KY;->d()V

    throw v0

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    const/4 v7, 0x0

    goto :goto_0
.end method

.method private static a(LX/8KY;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)LX/8KX;
    .locals 7

    .prologue
    .line 1330479
    invoke-static {p2}, LX/8KY;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1330480
    invoke-static {p0, p3, p4}, LX/8KY;->a(LX/8KY;II)Ljava/io/File;

    move-result-object v1

    .line 1330481
    if-eqz p7, :cond_0

    invoke-static {p0, v1, p1}, LX/8KY;->b(LX/8KY;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 1330482
    :goto_0
    move-object v1, v1

    .line 1330483
    new-instance v2, Ljava/io/File;

    const-string v3, "%s.%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x1

    aput-object p5, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1330484
    new-instance v3, Ljava/io/File;

    const-string v4, "%s.%s.%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    aput-object p5, v5, v0

    const/4 v0, 0x2

    aput-object p6, v5, v0

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1330485
    new-instance v0, LX/8KX;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, LX/8KX;-><init>(Ljava/io/File;Ljava/io/File;Z)V

    return-object v0

    .line 1330486
    :cond_0
    invoke-static {p0, v1, p1}, LX/8KY;->a(LX/8KY;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/8KY;
    .locals 7

    .prologue
    .line 1330466
    sget-object v0, LX/8KY;->g:LX/8KY;

    if-nez v0, :cond_1

    .line 1330467
    const-class v1, LX/8KY;

    monitor-enter v1

    .line 1330468
    :try_start_0
    sget-object v0, LX/8KY;->g:LX/8KY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1330469
    if-eqz v2, :cond_0

    .line 1330470
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1330471
    new-instance v6, LX/8KY;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v4

    check-cast v4, LX/0V8;

    invoke-static {v0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v5

    check-cast v5, LX/0en;

    const/16 p0, 0x346

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/8KY;-><init>(Landroid/content/Context;LX/0V8;LX/0en;LX/0Or;)V

    .line 1330472
    move-object v0, v6

    .line 1330473
    sput-object v0, LX/8KY;->g:LX/8KY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1330474
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1330475
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1330476
    :cond_1
    sget-object v0, LX/8KY;->g:LX/8KY;

    return-object v0

    .line 1330477
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1330478
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/8KY;II)Ljava/io/File;
    .locals 6

    .prologue
    .line 1330459
    iget-object v0, p0, LX/8KY;->c:LX/0V8;

    sget-object v1, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v0, v1}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v2

    .line 1330460
    int-to-long v0, p1

    cmp-long v0, v2, v0

    if-ltz v0, :cond_1

    iget-object v0, p0, LX/8KY;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v1, "uploads"

    invoke-static {p0, v0, v1}, LX/8KY;->a(LX/8KY;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1330461
    :cond_0
    return-object v0

    .line 1330462
    :cond_1
    iget-object v0, p0, LX/8KY;->c:LX/0V8;

    sget-object v1, LX/0VA;->EXTERNAL:LX/0VA;

    invoke-virtual {v0, v1}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v4

    .line 1330463
    int-to-long v0, p1

    cmp-long v0, v4, v0

    if-gez v0, :cond_2

    cmp-long v0, v4, v2

    if-lez v0, :cond_3

    int-to-long v0, p2

    cmp-long v0, v4, v0

    if-ltz v0, :cond_3

    :cond_2
    iget-object v0, p0, LX/8KY;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v1, "uploads"

    invoke-static {p0, v0, v1}, LX/8KY;->a(LX/8KY;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1330464
    :cond_3
    cmp-long v0, v2, v4

    if-ltz v0, :cond_4

    int-to-long v0, p2

    cmp-long v0, v2, v0

    if-ltz v0, :cond_4

    iget-object v0, p0, LX/8KY;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v1, "uploads"

    invoke-static {p0, v0, v1}, LX/8KY;->a(LX/8KY;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1330465
    :cond_4
    new-instance v0, LX/8KV;

    invoke-direct {v0}, LX/8KV;-><init>()V

    throw v0
.end method

.method public static declared-synchronized a(LX/8KY;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 1330456
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1330457
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1330458
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;Z)Ljava/io/File;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1330449
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LX/8KY;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 1330450
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1330451
    :cond_1
    :try_start_1
    new-instance v1, Ljava/io/File;

    invoke-static {p0}, LX/8KY;->c(LX/8KY;)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1330452
    if-nez p2, :cond_2

    move-object v0, v1

    .line 1330453
    goto :goto_0

    .line 1330454
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move-object v0, v1

    goto :goto_0

    .line 1330455
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(LX/8KY;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 1330442
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1330443
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1330444
    invoke-static {v0}, LX/2W9;->a(Ljava/io/File;)Z

    .line 1330445
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1330446
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1330447
    monitor-exit p0

    return-object v0

    .line 1330448
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1330441
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, LX/0YN;->b(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/8KY;)Ljava/io/File;
    .locals 3

    .prologue
    .line 1330440
    iget-object v0, p0, LX/8KY;->b:Landroid/content/Context;

    const-string v1, "uploads"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 1330438
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/8KY;->f:Z

    .line 1330439
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/File;I)LX/8KX;
    .locals 1
    .param p1    # Ljava/io/File;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1330437
    const-string v0, "scratch"

    invoke-virtual {p0, p1, p2, v0}, LX/8KY;->a(Ljava/io/File;ILjava/lang/String;)LX/8KX;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/io/File;ILjava/lang/String;)LX/8KX;
    .locals 1
    .param p1    # Ljava/io/File;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1330436
    const/4 v0, 0x1

    invoke-static {p0, p1, v0, p2, p3}, LX/8KY;->a(LX/8KY;Ljava/io/File;ZILjava/lang/String;)LX/8KX;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)LX/8KX;
    .locals 8

    .prologue
    .line 1330409
    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v7}, LX/8KY;->a(LX/8KY;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)LX/8KX;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;IIZ)LX/8KX;
    .locals 8

    .prologue
    .line 1330435
    const-string v5, "tmp"

    const-string v6, "scratch"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v7, p5

    invoke-static/range {v0 .. v7}, LX/8KY;->a(LX/8KY;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)LX/8KX;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1330434
    const-string v0, "tmp"

    invoke-virtual {p0, p1, p2, v0}, LX/8KY;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 1330427
    invoke-static {p2}, LX/8KY;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1330428
    iget-object v0, p0, LX/8KY;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v6}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 1330429
    :goto_0
    return-object v0

    .line 1330430
    :cond_0
    invoke-direct {p0, p1, v6}, LX/8KY;->a(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v3

    .line 1330431
    if-nez v3, :cond_1

    move-object v0, v1

    .line 1330432
    goto :goto_0

    .line 1330433
    :cond_1
    new-instance v0, Ljava/io/File;

    const-string v1, "%s.%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    aput-object p3, v4, v6

    invoke-static {v1, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1330419
    invoke-direct {p0, p1, v2}, LX/8KY;->a(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    .line 1330420
    if-eqz v0, :cond_1

    .line 1330421
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1330422
    invoke-static {v0}, LX/2W9;->a(Ljava/io/File;)Z

    .line 1330423
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1330424
    :cond_1
    iget-boolean v0, p0, LX/8KY;->f:Z

    if-eqz v0, :cond_2

    invoke-static {p0}, LX/8KY;->c(LX/8KY;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1330425
    iput-boolean v2, p0, LX/8KY;->f:Z

    .line 1330426
    :cond_2
    return-void
.end method

.method public final b(Ljava/io/File;I)LX/8KX;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1330418
    const/4 v0, 0x0

    const-string v1, "tmp"

    invoke-static {p0, p1, v0, p2, v1}, LX/8KY;->a(LX/8KY;Ljava/io/File;ZILjava/lang/String;)LX/8KX;

    move-result-object v0

    return-object v0
.end method
