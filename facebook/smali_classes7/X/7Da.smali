.class public LX/7Da;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/7D7;",
            "LX/7DZ;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1HI;

.field private final c:Lcom/facebook/common/callercontext/CallerContext;

.field public final d:LX/0tX;

.field public final e:LX/7DX;

.field public final f:Ljava/lang/String;

.field public final g:I

.field public h:LX/7DQ;


# direct methods
.method public constructor <init>(LX/1HI;LX/0tX;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;I)V
    .locals 6

    .prologue
    const-wide/16 v2, 0xc8

    .line 1183098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1183099
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, LX/7Da;->a:Ljava/util/Map;

    .line 1183100
    new-instance v0, LX/7DX;

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, LX/7DX;-><init>(LX/7Da;JJ)V

    iput-object v0, p0, LX/7Da;->e:LX/7DX;

    .line 1183101
    iput-object p1, p0, LX/7Da;->b:LX/1HI;

    .line 1183102
    iput-object p2, p0, LX/7Da;->d:LX/0tX;

    .line 1183103
    iput-object p3, p0, LX/7Da;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1183104
    iput-object p4, p0, LX/7Da;->f:Ljava/lang/String;

    .line 1183105
    iput p5, p0, LX/7Da;->g:I

    .line 1183106
    return-void
.end method

.method public static c(LX/7Da;LX/7D7;)V
    .locals 4

    .prologue
    .line 1183107
    iget-object v0, p0, LX/7Da;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7DZ;

    .line 1183108
    sget-object v1, LX/7DY;->FETCHING_IMAGE:LX/7DY;

    .line 1183109
    iput-object v1, v0, LX/7DZ;->a:LX/7DY;

    .line 1183110
    iget v1, p1, LX/7D7;->a:I

    move v1, v1

    .line 1183111
    const/4 v2, 0x1

    if-gt v1, v2, :cond_0

    .line 1183112
    sget-object v1, LX/1bc;->MEDIUM:LX/1bc;

    .line 1183113
    :goto_0
    iget-object v2, v0, LX/7DZ;->b:Landroid/net/Uri;

    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    .line 1183114
    iput-object v1, v2, LX/1bX;->i:LX/1bc;

    .line 1183115
    move-object v1, v2

    .line 1183116
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 1183117
    iget-object v2, p0, LX/7Da;->b:LX/1HI;

    iget-object v3, p0, LX/7Da;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, v3}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v1

    .line 1183118
    iput-object v1, v0, LX/7DZ;->c:LX/1ca;

    .line 1183119
    iget-object v0, v0, LX/7DZ;->c:LX/1ca;

    new-instance v1, LX/7DV;

    invoke-direct {v1, p0, p1}, LX/7DV;-><init>(LX/7Da;LX/7D7;)V

    invoke-static {}, LX/44p;->b()LX/44p;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1183120
    return-void

    .line 1183121
    :cond_0
    sget-object v1, LX/1bc;->LOW:LX/1bc;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/7D7;)V
    .locals 2

    .prologue
    .line 1183122
    iget-object v0, p0, LX/7Da;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7DZ;

    .line 1183123
    if-nez v0, :cond_1

    .line 1183124
    new-instance v0, LX/7DZ;

    invoke-direct {v0}, LX/7DZ;-><init>()V

    .line 1183125
    sget-object v1, LX/7DY;->URI_IN_QUEUE:LX/7DY;

    .line 1183126
    iput-object v1, v0, LX/7DZ;->a:LX/7DY;

    .line 1183127
    iget-object v1, p0, LX/7Da;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1183128
    iget-object v0, p0, LX/7Da;->e:LX/7DX;

    .line 1183129
    iget-boolean v1, v0, LX/7DX;->b:Z

    move v0, v1

    .line 1183130
    if-nez v0, :cond_0

    .line 1183131
    iget-object v0, p0, LX/7Da;->e:LX/7DX;

    invoke-virtual {v0}, LX/7DX;->start()Landroid/os/CountDownTimer;

    .line 1183132
    :cond_0
    :goto_0
    return-void

    .line 1183133
    :cond_1
    iget-object v0, v0, LX/7DZ;->a:LX/7DY;

    sget-object v1, LX/7DY;->URI_READY:LX/7DY;

    if-ne v0, v1, :cond_0

    .line 1183134
    invoke-static {p0, p1}, LX/7Da;->c(LX/7Da;LX/7D7;)V

    goto :goto_0
.end method
