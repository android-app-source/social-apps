.class public final LX/7io;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1228650
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1228651
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1228652
    :goto_0
    return v1

    .line 1228653
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1228654
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1228655
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1228656
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1228657
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1228658
    const-string v5, "collection_product_items"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1228659
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1228660
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_c

    .line 1228661
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1228662
    :goto_2
    move v3, v4

    .line 1228663
    goto :goto_1

    .line 1228664
    :cond_2
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1228665
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1228666
    :cond_3
    const-string v5, "name"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1228667
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1228668
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1228669
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1228670
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1228671
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1228672
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 1228673
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_a

    .line 1228674
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1228675
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1228676
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_6

    if-eqz v8, :cond_6

    .line 1228677
    const-string v9, "count"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1228678
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v5

    goto :goto_3

    .line 1228679
    :cond_7
    const-string v9, "edges"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1228680
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1228681
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, v9, :cond_8

    .line 1228682
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, v9, :cond_8

    .line 1228683
    const/4 v9, 0x0

    .line 1228684
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v10, :cond_10

    .line 1228685
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1228686
    :goto_5
    move v8, v9

    .line 1228687
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1228688
    :cond_8
    invoke-static {v6, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 1228689
    goto :goto_3

    .line 1228690
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1228691
    :cond_a
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1228692
    if-eqz v3, :cond_b

    .line 1228693
    invoke-virtual {p1, v4, v7, v4}, LX/186;->a(III)V

    .line 1228694
    :cond_b
    invoke-virtual {p1, v5, v6}, LX/186;->b(II)V

    .line 1228695
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_2

    :cond_c
    move v3, v4

    move v6, v4

    move v7, v4

    goto :goto_3

    .line 1228696
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1228697
    :cond_e
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_f

    .line 1228698
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1228699
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1228700
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_e

    if-eqz v10, :cond_e

    .line 1228701
    const-string v11, "node"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 1228702
    invoke-static {p0, p1}, LX/7in;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_6

    .line 1228703
    :cond_f
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1228704
    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1228705
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto :goto_5

    :cond_10
    move v8, v9

    goto :goto_6
.end method
