.class public final enum LX/7UL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7UL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7UL;

.field public static final enum PROGRESS_BAR_DETERMINATE_WITH_PLACEHOLDER:LX/7UL;

.field public static final enum PROGRESS_BAR_HIDDEN:LX/7UL;

.field public static final enum PROGRESS_BAR_INDETERMINATE:LX/7UL;

.field public static final enum PROGRESS_BAR_INDETERMINATE_WITH_PLACEHOLDER:LX/7UL;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1211722
    new-instance v0, LX/7UL;

    const-string v1, "PROGRESS_BAR_HIDDEN"

    invoke-direct {v0, v1, v2}, LX/7UL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7UL;->PROGRESS_BAR_HIDDEN:LX/7UL;

    .line 1211723
    new-instance v0, LX/7UL;

    const-string v1, "PROGRESS_BAR_INDETERMINATE"

    invoke-direct {v0, v1, v3}, LX/7UL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7UL;->PROGRESS_BAR_INDETERMINATE:LX/7UL;

    .line 1211724
    new-instance v0, LX/7UL;

    const-string v1, "PROGRESS_BAR_DETERMINATE_WITH_PLACEHOLDER"

    invoke-direct {v0, v1, v4}, LX/7UL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7UL;->PROGRESS_BAR_DETERMINATE_WITH_PLACEHOLDER:LX/7UL;

    .line 1211725
    new-instance v0, LX/7UL;

    const-string v1, "PROGRESS_BAR_INDETERMINATE_WITH_PLACEHOLDER"

    invoke-direct {v0, v1, v5}, LX/7UL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7UL;->PROGRESS_BAR_INDETERMINATE_WITH_PLACEHOLDER:LX/7UL;

    .line 1211726
    const/4 v0, 0x4

    new-array v0, v0, [LX/7UL;

    sget-object v1, LX/7UL;->PROGRESS_BAR_HIDDEN:LX/7UL;

    aput-object v1, v0, v2

    sget-object v1, LX/7UL;->PROGRESS_BAR_INDETERMINATE:LX/7UL;

    aput-object v1, v0, v3

    sget-object v1, LX/7UL;->PROGRESS_BAR_DETERMINATE_WITH_PLACEHOLDER:LX/7UL;

    aput-object v1, v0, v4

    sget-object v1, LX/7UL;->PROGRESS_BAR_INDETERMINATE_WITH_PLACEHOLDER:LX/7UL;

    aput-object v1, v0, v5

    sput-object v0, LX/7UL;->$VALUES:[LX/7UL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1211728
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7UL;
    .locals 1

    .prologue
    .line 1211729
    const-class v0, LX/7UL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7UL;

    return-object v0
.end method

.method public static values()[LX/7UL;
    .locals 1

    .prologue
    .line 1211727
    sget-object v0, LX/7UL;->$VALUES:[LX/7UL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7UL;

    return-object v0
.end method
