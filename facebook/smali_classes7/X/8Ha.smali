.class public LX/8Ha;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26t;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/8Ha;


# instance fields
.field public final a:LX/8Gn;


# direct methods
.method public constructor <init>(LX/8Gn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1321427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321428
    iput-object p1, p0, LX/8Ha;->a:LX/8Gn;

    .line 1321429
    return-void
.end method

.method public static a(LX/0QB;)LX/8Ha;
    .locals 4

    .prologue
    .line 1321430
    sget-object v0, LX/8Ha;->b:LX/8Ha;

    if-nez v0, :cond_1

    .line 1321431
    const-class v1, LX/8Ha;

    monitor-enter v1

    .line 1321432
    :try_start_0
    sget-object v0, LX/8Ha;->b:LX/8Ha;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1321433
    if-eqz v2, :cond_0

    .line 1321434
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1321435
    new-instance p0, LX/8Ha;

    invoke-static {v0}, LX/8Gn;->a(LX/0QB;)LX/8Gn;

    move-result-object v3

    check-cast v3, LX/8Gn;

    invoke-direct {p0, v3}, LX/8Ha;-><init>(LX/8Gn;)V

    .line 1321436
    move-object v0, p0

    .line 1321437
    sput-object v0, LX/8Ha;->b:LX/8Ha;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1321438
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1321439
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1321440
    :cond_1
    sget-object v0, LX/8Ha;->b:LX/8Ha;

    return-object v0

    .line 1321441
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1321442
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 1321443
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1321444
    const-string v1, "delete_photo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1321445
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 1321446
    iget-boolean v0, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 1321447
    if-nez v0, :cond_1

    move-object v0, v1

    .line 1321448
    :goto_0
    move-object v0, v0

    .line 1321449
    :goto_1
    return-object v0

    :cond_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1

    .line 1321450
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/method/DeletePhotoResponse;

    .line 1321451
    iget-boolean p1, v0, Lcom/facebook/photos/data/method/DeletePhotoResponse;->a:Z

    move v0, p1

    .line 1321452
    if-nez v0, :cond_2

    move-object v0, v1

    .line 1321453
    goto :goto_0

    .line 1321454
    :cond_2
    iget-object v0, p0, LX/8Ha;->a:LX/8Gn;

    invoke-virtual {v0}, LX/8Gn;->clearUserData()V

    move-object v0, v1

    .line 1321455
    goto :goto_0
.end method
