.class public LX/6sI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6rs;


# direct methods
.method public constructor <init>(LX/6rs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1153059
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153060
    iput-object p1, p0, LX/6sI;->a:LX/6rs;

    .line 1153061
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1153062
    iget-object v0, p0, LX/6sI;->a:LX/6rs;

    .line 1153063
    iget-object v1, v0, LX/6rs;->x:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6rr;

    move-object v0, v1

    .line 1153064
    const-string v1, "participant"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;

    .line 1153065
    new-instance v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;

    const-string v2, "description"

    invoke-virtual {p2, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;-><init>(Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;Ljava/lang/String;)V

    return-object v1
.end method
