.class public LX/8Tm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/8TD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1349075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1349076
    iput-object p1, p0, LX/8Tm;->a:LX/0tX;

    .line 1349077
    iput-object p2, p0, LX/8Tm;->b:LX/1Ck;

    .line 1349078
    return-void
.end method

.method public static a(LX/0QB;)LX/8Tm;
    .locals 5

    .prologue
    .line 1349079
    const-class v1, LX/8Tm;

    monitor-enter v1

    .line 1349080
    :try_start_0
    sget-object v0, LX/8Tm;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1349081
    sput-object v2, LX/8Tm;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1349082
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1349083
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1349084
    new-instance p0, LX/8Tm;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-direct {p0, v3, v4}, LX/8Tm;-><init>(LX/0tX;LX/1Ck;)V

    .line 1349085
    invoke-static {v0}, LX/8TD;->a(LX/0QB;)LX/8TD;

    move-result-object v3

    check-cast v3, LX/8TD;

    .line 1349086
    iput-object v3, p0, LX/8Tm;->c:LX/8TD;

    .line 1349087
    move-object v0, p0

    .line 1349088
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1349089
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8Tm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1349090
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1349091
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
