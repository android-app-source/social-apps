.class public final LX/81Z;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1286208
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1286209
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1286210
    :goto_0
    return v1

    .line 1286211
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1286212
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1286213
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1286214
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1286215
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1286216
    const-string v4, "comments"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1286217
    invoke-static {p0, p1}, LX/81X;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1286218
    :cond_2
    const-string v4, "likers"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1286219
    invoke-static {p0, p1}, LX/81Y;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1286220
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1286221
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1286222
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1286223
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1286224
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1286225
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1286226
    if-eqz v0, :cond_0

    .line 1286227
    const-string v1, "comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1286228
    invoke-static {p0, v0, p2}, LX/81X;->a(LX/15i;ILX/0nX;)V

    .line 1286229
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1286230
    if-eqz v0, :cond_1

    .line 1286231
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1286232
    invoke-static {p0, v0, p2}, LX/81Y;->a(LX/15i;ILX/0nX;)V

    .line 1286233
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1286234
    return-void
.end method
