.class public final LX/7S0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Ru;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public final synthetic a:LX/3nl;


# direct methods
.method public constructor <init>(LX/3nl;)V
    .locals 0

    .prologue
    .line 1208006
    iput-object p1, p0, LX/7S0;->a:LX/3nl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0oG;)V
    .locals 4

    .prologue
    .line 1208007
    iget-object v0, p0, LX/7S0;->a:LX/3nl;

    iget-object v0, v0, LX/3nl;->b:LX/376;

    .line 1208008
    iget-object v1, v0, LX/376;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1208009
    if-nez v0, :cond_0

    .line 1208010
    :goto_0
    return-void

    .line 1208011
    :cond_0
    invoke-virtual {p1}, LX/0oG;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7Rp;->fromString(Ljava/lang/String;)LX/7Rp;

    move-result-object v0

    sget-object v1, LX/7Rp;->SESSION_STARTED:LX/7Rp;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 1208012
    :goto_1
    if-eqz v0, :cond_2

    const-wide/16 v0, 0x0

    .line 1208013
    :goto_2
    sget-object v2, LX/7Rq;->SESSION_ID:LX/7Rq;

    iget-object v2, v2, LX/7Rq;->value:Ljava/lang/String;

    iget-object v3, p0, LX/7S0;->a:LX/3nl;

    iget-object v3, v3, LX/3nl;->b:LX/376;

    .line 1208014
    iget-object p0, v3, LX/376;->d:Ljava/lang/String;

    move-object v3, p0

    .line 1208015
    invoke-virtual {p1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1208016
    sget-object v2, LX/7Rq;->SESSION_ELAPSED_TIME:LX/7Rq;

    iget-object v2, v2, LX/7Rq;->value:Ljava/lang/String;

    invoke-virtual {p1, v2, v0, v1}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    goto :goto_0

    .line 1208017
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1208018
    :cond_2
    iget-object v0, p0, LX/7S0;->a:LX/3nl;

    iget-object v0, v0, LX/3nl;->b:LX/376;

    invoke-virtual {v0}, LX/376;->f()J

    move-result-wide v0

    goto :goto_2
.end method
