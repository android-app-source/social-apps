.class public final LX/7KB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7KA;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:Z

.field public f:LX/04A;


# direct methods
.method public constructor <init>(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 1194576
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194577
    iput-object p1, p0, LX/7KB;->a:Ljava/lang/String;

    .line 1194578
    iput-wide p2, p0, LX/7KB;->b:J

    .line 1194579
    const/16 v0, 0xa

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/7KB;->c:Ljava/util/List;

    .line 1194580
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7KB;->e:Z

    .line 1194581
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7KB;->d:Z

    .line 1194582
    const/4 v0, 0x0

    iput-object v0, p0, LX/7KB;->f:LX/04A;

    .line 1194583
    return-void
.end method


# virtual methods
.method public final c()Ljava/lang/String;
    .locals 8

    .prologue
    .line 1194584
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x1f4

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1194585
    const-string v0, "VideoSession VideoID \'%s\' Time: %d\n"

    iget-object v2, p0, LX/7KB;->a:Ljava/lang/String;

    iget-wide v4, p0, LX/7KB;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1194586
    iget-object v0, p0, LX/7KB;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7KA;

    .line 1194587
    const-string v3, "- %d %s (%s, original: %s), video time: %d, origin: %s:%s\n"

    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, v0, LX/7KA;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, v0, LX/7KA;->a:LX/04A;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, v0, LX/7KA;->d:LX/04g;

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget-object v6, v0, LX/7KA;->e:LX/04g;

    aput-object v6, v4, v5

    const/4 v5, 0x4

    iget-wide v6, v0, LX/7KA;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    iget-object v6, v0, LX/7KA;->f:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x6

    iget-object v0, v0, LX/7KA;->g:Ljava/lang/String;

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1194588
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
