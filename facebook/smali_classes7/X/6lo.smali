.class public LX/6lo;
.super Landroid/view/ViewGroup;
.source ""


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1142965
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 1142966
    const/4 v0, -0x1

    iput v0, p0, LX/6lo;->a:I

    .line 1142967
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1142975
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1142976
    const/4 v0, -0x1

    iput v0, p0, LX/6lo;->a:I

    .line 1142977
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1142972
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1142973
    const/4 v0, -0x1

    iput v0, p0, LX/6lo;->a:I

    .line 1142974
    return-void
.end method

.method private static a()LX/6ln;
    .locals 3

    .prologue
    .line 1142971
    new-instance v0, LX/6ln;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, LX/6ln;-><init>(II)V

    return-object v0
.end method

.method private a(Landroid/util/AttributeSet;)LX/6ln;
    .locals 2

    .prologue
    .line 1142970
    new-instance v0, LX/6ln;

    invoke-virtual {p0}, LX/6lo;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/6ln;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;)LX/6ln;
    .locals 1

    .prologue
    .line 1142969
    new-instance v0, LX/6ln;

    invoke-direct {v0, p0}, LX/6ln;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method


# virtual methods
.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 1142968
    instance-of v0, p1, LX/6ln;

    return v0
.end method

.method public final synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1142978
    invoke-static {}, LX/6lo;->a()LX/6ln;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1142940
    invoke-direct {p0, p1}, LX/6lo;->a(Landroid/util/AttributeSet;)LX/6ln;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1142941
    invoke-static {p1}, LX/6lo;->a(Landroid/view/ViewGroup$LayoutParams;)LX/6ln;

    move-result-object v0

    return-object v0
.end method

.method public getChildSizingPreference()LX/6lm;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1142942
    invoke-virtual {p0, v1}, LX/6lo;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1142943
    invoke-virtual {p0, v1}, LX/6lo;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/6ln;

    .line 1142944
    iget-object v1, v0, LX/6ln;->a:LX/6lm;

    move-object v0, v1

    .line 1142945
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/6lm;->MATCH_LARGEST_NONTEXT:LX/6lm;

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1142946
    invoke-virtual {p0, v3}, LX/6lo;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1142947
    if-nez v0, :cond_0

    .line 1142948
    :goto_0
    return-void

    .line 1142949
    :cond_0
    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1142950
    invoke-virtual {p0, v0}, LX/6lo;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1142951
    if-nez v1, :cond_0

    .line 1142952
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 1142953
    :goto_0
    return-void

    .line 1142954
    :cond_0
    iget v2, p0, LX/6lo;->a:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 1142955
    invoke-virtual {v1, p1, p2}, Landroid/view/View;->measure(II)V

    .line 1142956
    :goto_1
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/6lo;->setMeasuredDimension(II)V

    goto :goto_0

    .line 1142957
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1142958
    :goto_2
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1142959
    invoke-virtual {v1, v0, p2}, Landroid/view/View;->measure(II)V

    goto :goto_1

    .line 1142960
    :sswitch_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    goto :goto_2

    .line 1142961
    :sswitch_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v2, p0, LX/6lo;->a:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_2

    .line 1142962
    :sswitch_2
    iget v0, p0, LX/6lo;->a:I

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_2
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public setTargetExactWidth(I)V
    .locals 0

    .prologue
    .line 1142963
    iput p1, p0, LX/6lo;->a:I

    .line 1142964
    return-void
.end method
