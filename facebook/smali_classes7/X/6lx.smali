.class public final LX/6lx;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public final a:I

.field public b:Z

.field public c:LX/4oI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:[F

.field private final e:Landroid/graphics/Path;

.field private final f:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1143042
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1143043
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LX/6lx;->e:Landroid/graphics/Path;

    .line 1143044
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/6lx;->f:Landroid/graphics/RectF;

    .line 1143045
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b07ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/6lx;->a:I

    .line 1143046
    const/16 v0, 0x8

    new-array v0, v0, [F

    const/4 v1, 0x0

    iget v2, p0, LX/6lx;->a:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, LX/6lx;->a:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, LX/6lx;->a:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, LX/6lx;->a:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, LX/6lx;->a:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, LX/6lx;->a:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, LX/6lx;->a:I

    int-to-float v2, v2

    aput v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, LX/6lx;->a:I

    int-to-float v2, v2

    aput v2, v0, v1

    iput-object v0, p0, LX/6lx;->d:[F

    .line 1143047
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1143041
    iget-boolean v0, p0, LX/6lx;->b:Z

    return v0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1143048
    iget-object v0, p0, LX/6lx;->e:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 1143049
    iget-object v0, p0, LX/6lx;->f:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1143050
    iget-object v0, p0, LX/6lx;->e:Landroid/graphics/Path;

    iget-object v1, p0, LX/6lx;->f:Landroid/graphics/RectF;

    iget-object v2, p0, LX/6lx;->d:[F

    sget-object v3, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    .line 1143051
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 1143052
    iget-object v1, p0, LX/6lx;->e:Landroid/graphics/Path;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 1143053
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1143054
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1143055
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4310663a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1143037
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1143038
    const/4 v1, 0x1

    .line 1143039
    iput-boolean v1, p0, LX/6lx;->b:Z

    .line 1143040
    const/16 v1, 0x2d

    const v2, 0x1f2f0fc7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x288eb179

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1143033
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1143034
    const/4 v1, 0x0

    .line 1143035
    iput-boolean v1, p0, LX/6lx;->b:Z

    .line 1143036
    const/16 v1, 0x2d

    const v2, 0x44c5b1df

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
