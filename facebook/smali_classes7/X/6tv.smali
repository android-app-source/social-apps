.class public final LX/6tv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/payments/checkout/statemachine/CheckoutStateMachineHandler",
        "<",
        "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/6r9;

.field public final c:LX/6xb;

.field private final d:LX/6ns;

.field public e:LX/6qd;

.field public f:LX/6qp;

.field public g:LX/6qh;

.field private h:LX/6nr;

.field public i:Lcom/facebook/payments/checkout/model/CheckoutData;

.field private final j:LX/6nc;

.field private final k:LX/6Ex;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6r9;LX/6xb;LX/6ns;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1155279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1155280
    new-instance v0, LX/6ts;

    invoke-direct {v0, p0}, LX/6ts;-><init>(LX/6tv;)V

    iput-object v0, p0, LX/6tv;->j:LX/6nc;

    .line 1155281
    new-instance v0, LX/6tt;

    invoke-direct {v0, p0}, LX/6tt;-><init>(LX/6tv;)V

    iput-object v0, p0, LX/6tv;->k:LX/6Ex;

    .line 1155282
    iput-object p1, p0, LX/6tv;->a:Landroid/content/Context;

    .line 1155283
    iput-object p2, p0, LX/6tv;->b:LX/6r9;

    .line 1155284
    iput-object p3, p0, LX/6tv;->c:LX/6xb;

    .line 1155285
    iput-object p4, p0, LX/6tv;->d:LX/6ns;

    .line 1155286
    return-void
.end method

.method private static a(LX/0Px;LX/6tr;)LX/6tr;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/6tr;",
            ">;",
            "LX/6tr;",
            ")",
            "LX/6tr;"
        }
    .end annotation

    .prologue
    .line 1155393
    invoke-virtual {p0, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 1155394
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Next state not found for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1155395
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6tr;

    return-object v0

    .line 1155396
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(LX/6tv;)V
    .locals 3

    .prologue
    .line 1155402
    iget-object v0, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->z:Z

    if-eqz v0, :cond_0

    .line 1155403
    iget-object v0, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->x()Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1155404
    iget-object v0, p0, LX/6tv;->b:LX/6r9;

    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->e(LX/6qw;)LX/6E0;

    move-result-object v0

    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    iget-object v2, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v2}, Lcom/facebook/payments/checkout/model/CheckoutData;->x()Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/6E0;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)Lcom/facebook/payments/confirmation/ConfirmationParams;

    move-result-object v0

    .line 1155405
    iget-object v1, p0, LX/6tv;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/facebook/payments/confirmation/ConfirmationActivity;->a(Landroid/content/Context;Lcom/facebook/payments/confirmation/ConfirmationParams;)Landroid/content/Intent;

    move-result-object v0

    .line 1155406
    iget-object v1, p0, LX/6tv;->g:LX/6qh;

    invoke-virtual {v1, v0}, LX/6qh;->a(Landroid/content/Intent;)V

    .line 1155407
    :cond_0
    new-instance v0, LX/73T;

    sget-object v1, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v0, v1}, LX/73T;-><init>(LX/73S;)V

    .line 1155408
    iget-object v1, p0, LX/6tv;->g:LX/6qh;

    invoke-virtual {v1, v0}, LX/6qh;->a(LX/73T;)V

    .line 1155409
    return-void
.end method

.method public static j(LX/6tv;)V
    .locals 3

    .prologue
    .line 1155397
    iget-object v0, p0, LX/6tv;->b:LX/6r9;

    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->h(LX/6qw;)LX/6Dw;

    move-result-object v0

    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1}, LX/6Dw;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;

    move-result-object v0

    .line 1155398
    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->r()LX/6tr;

    move-result-object v1

    invoke-static {v0, v1}, LX/6tv;->a(LX/0Px;LX/6tr;)LX/6tr;

    move-result-object v1

    .line 1155399
    invoke-static {v0, v1}, LX/6tv;->a(LX/0Px;LX/6tr;)LX/6tr;

    move-result-object v0

    .line 1155400
    iget-object v1, p0, LX/6tv;->e:LX/6qd;

    iget-object v2, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1, v2, v0}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6tr;)V

    .line 1155401
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 1155313
    sget-object v0, LX/6tu;->b:[I

    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->r()LX/6tr;

    move-result-object v1

    invoke-virtual {v1}, LX/6tr;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1155314
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid state found + "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v2}, Lcom/facebook/payments/checkout/model/CheckoutData;->r()LX/6tr;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1155315
    :pswitch_0
    iget-object v0, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1155316
    invoke-virtual {p0}, LX/6tv;->c()V

    .line 1155317
    :cond_0
    :goto_0
    :pswitch_1
    return-void

    .line 1155318
    :pswitch_2
    iget-object v0, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->s()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 1155319
    sget-object v1, LX/6tu;->c:[I

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->b()LX/6zU;

    move-result-object v2

    invoke-virtual {v2}, LX/6zU;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 1155320
    :cond_1
    invoke-static {p0}, LX/6tv;->j(LX/6tv;)V

    .line 1155321
    :goto_1
    goto :goto_0

    .line 1155322
    :pswitch_3
    iget-object v0, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->s()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 1155323
    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->b()LX/6zU;

    move-result-object v1

    sget-object v2, LX/6zU;->CREDIT_CARD:LX/6zU;

    if-ne v1, v2, :cond_2

    .line 1155324
    invoke-virtual {p0}, LX/6tv;->c()V

    .line 1155325
    check-cast v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    .line 1155326
    iget-object v1, p0, LX/6tv;->a:Landroid/content/Context;

    .line 1155327
    iget-object v2, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v2}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v2

    invoke-interface {v2}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    invoke-virtual {v2}, LX/6xg;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v3}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-static {v2, v3}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a(Ljava/lang/String;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6xw;

    move-result-object v2

    sget-object v3, LX/6xZ;->CONFIRM_SECURITY_CODE:LX/6xZ;

    .line 1155328
    iput-object v3, v2, LX/6xw;->c:LX/6xZ;

    .line 1155329
    move-object v2, v2

    .line 1155330
    invoke-virtual {v2}, LX/6xw;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    move-result-object v2

    .line 1155331
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v3

    iget-object v4, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v4}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v4

    invoke-interface {v4}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v3, v4}, LX/6wu;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6wu;

    move-result-object v3

    sget-object v4, LX/6ws;->MODAL_BOTTOM:LX/6ws;

    .line 1155332
    iput-object v4, v3, LX/6wu;->a:LX/6ws;

    .line 1155333
    move-object v3, v3

    .line 1155334
    invoke-virtual {v3}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v3

    .line 1155335
    iget-object v4, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v4}, Lcom/facebook/payments/checkout/model/CheckoutData;->t()Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f()Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;

    move-result-object v4

    .line 1155336
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1155337
    invoke-static {}, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->newBuilder()LX/6yR;

    move-result-object v4

    iget-object v5, p0, LX/6tv;->a:Landroid/content/Context;

    const v6, 0x7f081d46

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1155338
    iput-object v5, v4, LX/6yR;->a:Ljava/lang/String;

    .line 1155339
    move-object v4, v4

    .line 1155340
    iget-object v5, p0, LX/6tv;->a:Landroid/content/Context;

    const v6, 0x7f081d47

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1155341
    iput-object v5, v4, LX/6yR;->b:Ljava/lang/String;

    .line 1155342
    move-object v4, v4

    .line 1155343
    iput-object v3, v4, LX/6yR;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1155344
    move-object v3, v4

    .line 1155345
    invoke-virtual {v3}, LX/6yR;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    move-result-object v3

    .line 1155346
    sget-object v4, LX/6yO;->CONFIRM_CSC:LX/6yO;

    iget-object v5, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v5}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v5

    invoke-interface {v5}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v5

    iget-object v5, v5, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    invoke-static {v4, v2, v5}, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a(LX/6yO;Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;LX/6xg;)LX/6xy;

    move-result-object v2

    .line 1155347
    iput-object v3, v2, LX/6xy;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    .line 1155348
    move-object v2, v2

    .line 1155349
    iget-object v3, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v3}, Lcom/facebook/payments/checkout/model/CheckoutData;->t()Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    move-result-object v3

    .line 1155350
    iget-object v4, v3, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->b:Lcom/facebook/common/locale/Country;

    move-object v3, v4

    .line 1155351
    iput-object v3, v2, LX/6xy;->g:Lcom/facebook/common/locale/Country;

    .line 1155352
    move-object v2, v2

    .line 1155353
    iput-object v0, v2, LX/6xy;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    .line 1155354
    move-object v2, v2

    .line 1155355
    invoke-virtual {v2}, LX/6xy;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    move-object v2, v2

    .line 1155356
    invoke-static {v1, v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Landroid/content/Intent;

    move-result-object v1

    .line 1155357
    iget-object v2, p0, LX/6tv;->g:LX/6qh;

    const/16 v3, 0x6e

    invoke-virtual {v2, v1, v3}, LX/6qh;->a(Landroid/content/Intent;I)V

    .line 1155358
    :goto_2
    goto/16 :goto_0

    .line 1155359
    :pswitch_4
    iget-object v0, p0, LX/6tv;->h:LX/6nr;

    invoke-virtual {v0}, LX/6nr;->a()V

    goto/16 :goto_0

    .line 1155360
    :pswitch_5
    invoke-virtual {p0}, LX/6tv;->c()V

    .line 1155361
    iget-object v0, p0, LX/6tv;->b:LX/6r9;

    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->g(LX/6qw;)LX/6Du;

    move-result-object v0

    .line 1155362
    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1}, LX/6Du;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1155363
    iget-object v1, p0, LX/6tv;->g:LX/6qh;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/6qh;->a(Lcom/google/common/util/concurrent/ListenableFuture;Z)V

    .line 1155364
    goto/16 :goto_0

    .line 1155365
    :pswitch_6
    iget-object v3, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v3}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    iget-boolean v3, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->n:Z

    if-eqz v3, :cond_3

    .line 1155366
    invoke-static {p0}, LX/6tv;->i(LX/6tv;)V

    .line 1155367
    :goto_3
    goto/16 :goto_0

    .line 1155368
    :pswitch_7
    check-cast v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    .line 1155369
    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->t()Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f()Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a(Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1155370
    invoke-virtual {p0}, LX/6tv;->c()V

    .line 1155371
    iget-object v1, p0, LX/6tv;->b:LX/6r9;

    iget-object v2, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v2}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v2

    invoke-interface {v2}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v1, v2}, LX/6r9;->e(LX/6qw;)LX/6E0;

    move-result-object v1

    iget-object v2, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1, v2, v0}, LX/6E0;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    move-result-object v1

    .line 1155372
    iget-object v2, p0, LX/6tv;->a:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Landroid/content/Intent;

    move-result-object v1

    .line 1155373
    iget-object v2, p0, LX/6tv;->g:LX/6qh;

    const/16 v3, 0x6a

    invoke-virtual {v2, v1, v3}, LX/6qh;->a(Landroid/content/Intent;I)V

    .line 1155374
    goto/16 :goto_1

    .line 1155375
    :pswitch_8
    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    .line 1155376
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->c:LX/6zT;

    sget-object v2, LX/6zT;->MIB:LX/6zT;

    if-ne v1, v2, :cond_1

    .line 1155377
    invoke-virtual {p0}, LX/6tv;->c()V

    .line 1155378
    new-instance v1, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;

    iget-object v2, p0, LX/6tv;->a:Landroid/content/Context;

    const v3, 0x7f081d47

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;-><init>(Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;Ljava/lang/String;)V

    iget-object v2, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v2}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-static {v1, v2}, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->a(Lcom/facebook/payments/consent/model/ConsentExtraData;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6v0;

    move-result-object v1

    invoke-virtual {v1}, LX/6v0;->a()Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    move-result-object v1

    .line 1155379
    iget-object v2, p0, LX/6tv;->g:LX/6qh;

    iget-object v3, p0, LX/6tv;->a:Landroid/content/Context;

    invoke-static {v3, v1}, Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;)Landroid/content/Intent;

    move-result-object v1

    const/16 v3, 0x73

    invoke-virtual {v2, v1, v3}, LX/6qh;->a(Landroid/content/Intent;I)V

    .line 1155380
    goto/16 :goto_1

    .line 1155381
    :cond_2
    invoke-static {p0}, LX/6tv;->j(LX/6tv;)V

    goto/16 :goto_2

    .line 1155382
    :cond_3
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/facebook/payments/checkout/statemachine/SimpleCheckoutStateMachineHandler$3;

    invoke-direct {v4, p0}, Lcom/facebook/payments/checkout/statemachine/SimpleCheckoutStateMachineHandler$3;-><init>(LX/6tv;)V

    const-wide/16 v5, 0x258

    const v7, -0x149cf3b5

    invoke-static {v3, v4, v5, v6, v7}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_1
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 1155383
    sparse-switch p1, :sswitch_data_0

    .line 1155384
    :goto_0
    return-void

    .line 1155385
    :sswitch_0
    if-ne p2, v0, :cond_0

    .line 1155386
    iget-object v0, p0, LX/6tv;->g:LX/6qh;

    new-instance v1, LX/73T;

    sget-object v2, LX/73S;->RELOAD:LX/73S;

    invoke-direct {v1, v2}, LX/73T;-><init>(LX/73S;)V

    invoke-virtual {v0, v1}, LX/6qh;->a(LX/73T;)V

    goto :goto_0

    .line 1155387
    :cond_0
    invoke-virtual {p0}, LX/6tv;->b()V

    goto :goto_0

    .line 1155388
    :sswitch_1
    if-ne p2, v0, :cond_1

    .line 1155389
    const-string v0, "cvv_code"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1155390
    iget-object v1, p0, LX/6tv;->e:LX/6qd;

    iget-object v2, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1, v2, v0}, LX/6qd;->c(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;)V

    .line 1155391
    invoke-virtual {p0}, LX/6tv;->c()V

    goto :goto_0

    .line 1155392
    :cond_1
    invoke-virtual {p0}, LX/6tv;->b()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x6a -> :sswitch_0
        0x6e -> :sswitch_1
        0x73 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(LX/6ng;)V
    .locals 2

    .prologue
    .line 1155304
    iget-object v0, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->e()Lcom/facebook/payments/model/PaymentsPin;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a(Lcom/facebook/payments/model/PaymentsPin;)Lcom/facebook/payments/auth/pin/model/PaymentPin;

    move-result-object v0

    .line 1155305
    iput-object v0, p1, LX/6ng;->d:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1155306
    move-object v0, p1

    .line 1155307
    iget-object v1, p0, LX/6tv;->j:LX/6nc;

    .line 1155308
    iput-object v1, v0, LX/6ng;->b:LX/6nc;

    .line 1155309
    move-object v0, v0

    .line 1155310
    invoke-virtual {v0}, LX/6ng;->a()LX/6nh;

    move-result-object v0

    .line 1155311
    iget-object v1, p0, LX/6tv;->d:LX/6ns;

    invoke-virtual {v1, v0}, LX/6ns;->a(LX/6nh;)LX/6nr;

    move-result-object v0

    iput-object v0, p0, LX/6tv;->h:LX/6nr;

    .line 1155312
    return-void
.end method

.method public final a(LX/6qp;)V
    .locals 2

    .prologue
    .line 1155298
    iget-object v0, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    const-string v1, "setCheckoutData(CheckoutData) should be called before a call to this function."

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1155299
    iput-object p1, p0, LX/6tv;->f:LX/6qp;

    .line 1155300
    iget-object v0, p0, LX/6tv;->b:LX/6r9;

    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->g(LX/6qw;)LX/6Du;

    move-result-object v0

    .line 1155301
    iget-object v1, p0, LX/6tv;->g:LX/6qh;

    invoke-interface {v0, v1}, LX/6Du;->a(LX/6qh;)V

    .line 1155302
    iget-object v1, p0, LX/6tv;->k:LX/6Ex;

    invoke-interface {v0, v1}, LX/6Du;->a(LX/6Ex;)V

    .line 1155303
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 2

    .prologue
    .line 1155294
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 1155295
    iput-object p1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    .line 1155296
    iget-object v0, p0, LX/6tv;->b:LX/6r9;

    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->b(LX/6qw;)LX/6qd;

    move-result-object v0

    iput-object v0, p0, LX/6tv;->e:LX/6qd;

    .line 1155297
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1155291
    iget-object v0, p0, LX/6tv;->e:LX/6qd;

    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Z)V

    .line 1155292
    iget-object v0, p0, LX/6tv;->e:LX/6qd;

    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    sget-object v2, LX/6tr;->PREPARE_CHECKOUT:LX/6tr;

    invoke-interface {v0, v1, v2}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6tr;)V

    .line 1155293
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1155287
    iget-object v0, p0, LX/6tv;->b:LX/6r9;

    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->h(LX/6qw;)LX/6Dw;

    move-result-object v0

    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1}, LX/6Dw;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;

    move-result-object v0

    .line 1155288
    iget-object v1, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->r()LX/6tr;

    move-result-object v1

    invoke-static {v0, v1}, LX/6tv;->a(LX/0Px;LX/6tr;)LX/6tr;

    move-result-object v0

    .line 1155289
    iget-object v1, p0, LX/6tv;->e:LX/6qd;

    iget-object v2, p0, LX/6tv;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1, v2, v0}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6tr;)V

    .line 1155290
    return-void
.end method
