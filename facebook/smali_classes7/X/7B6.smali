.class public LX/7B6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final a:LX/7B6;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1177775
    new-instance v0, LX/7B6;

    const-string v1, ""

    const-string v2, ""

    .line 1177776
    sget-object v3, LX/0Rg;->a:LX/0Rg;

    move-object v3, v3

    .line 1177777
    invoke-direct {v0, v1, v2, v3}, LX/7B6;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0P1;)V

    sput-object v0, LX/7B6;->a:LX/7B6;

    return-void
.end method

.method public constructor <init>(LX/7B2;)V
    .locals 1

    .prologue
    .line 1177770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1177771
    iget-object v0, p1, LX/7B2;->a:Ljava/lang/String;

    iput-object v0, p0, LX/7B6;->b:Ljava/lang/String;

    .line 1177772
    iget-object v0, p1, LX/7B2;->b:Ljava/lang/String;

    iput-object v0, p0, LX/7B6;->c:Ljava/lang/String;

    .line 1177773
    iget-object v0, p1, LX/7B2;->c:LX/0P2;

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/7B6;->d:LX/0P1;

    .line 1177774
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1177766
    const-string v0, ""

    .line 1177767
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 1177768
    invoke-direct {p0, p1, v0, v1}, LX/7B6;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0P1;)V

    .line 1177769
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/0P1;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1177754
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1177755
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1177756
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1177757
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7B6;->b:Ljava/lang/String;

    .line 1177758
    iput-object p2, p0, LX/7B6;->c:Ljava/lang/String;

    .line 1177759
    iput-object p3, p0, LX/7B6;->d:LX/0P1;

    .line 1177760
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 1177765
    const/4 v0, 0x0

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 1177764
    const/4 v0, 0x1

    return v0
.end method

.method public c()LX/7B2;
    .locals 1

    .prologue
    .line 1177763
    new-instance v0, LX/7B2;

    invoke-direct {v0, p0, p0}, LX/7B2;-><init>(LX/7B6;LX/7B6;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1177762
    instance-of v0, p1, LX/7B6;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, LX/7B6;

    iget-object v0, v0, LX/7B6;->b:Ljava/lang/String;

    iget-object v1, p0, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, LX/7B6;

    iget-object v0, p1, LX/7B6;->c:Ljava/lang/String;

    iget-object v1, p0, LX/7B6;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 1177761
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/7B6;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/7B6;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
