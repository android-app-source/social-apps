.class public LX/6ka;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final key:Ljava/lang/String;

.field public final op:LX/6kc;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1139842
    new-instance v0, LX/1sv;

    const-string v1, "GenericMapKeyMutation"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6ka;->b:LX/1sv;

    .line 1139843
    new-instance v0, LX/1sw;

    const-string v1, "key"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ka;->c:LX/1sw;

    .line 1139844
    new-instance v0, LX/1sw;

    const-string v1, "op"

    const/16 v2, 0xc

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ka;->d:LX/1sw;

    .line 1139845
    sput-boolean v4, LX/6ka;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/6kc;)V
    .locals 0

    .prologue
    .line 1139846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1139847
    iput-object p1, p0, LX/6ka;->key:Ljava/lang/String;

    .line 1139848
    iput-object p2, p0, LX/6ka;->op:LX/6kc;

    .line 1139849
    return-void
.end method

.method public static a(LX/6ka;)V
    .locals 4

    .prologue
    .line 1139827
    iget-object v0, p0, LX/6ka;->key:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1139828
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'key\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6ka;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1139829
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1139798
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1139799
    :goto_0
    if-eqz p2, :cond_2

    const-string v0, "\n"

    move-object v1, v0

    .line 1139800
    :goto_1
    if-eqz p2, :cond_3

    const-string v0, " "

    .line 1139801
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GenericMapKeyMutation"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1139802
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139803
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139804
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139805
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139806
    const-string v4, "key"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139807
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139808
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139809
    iget-object v4, p0, LX/6ka;->key:Ljava/lang/String;

    if-nez v4, :cond_4

    .line 1139810
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139811
    :goto_3
    iget-object v4, p0, LX/6ka;->op:LX/6kc;

    if-eqz v4, :cond_0

    .line 1139812
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139813
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139814
    const-string v4, "op"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139815
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139816
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139817
    iget-object v0, p0, LX/6ka;->op:LX/6kc;

    if-nez v0, :cond_5

    .line 1139818
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139819
    :cond_0
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139820
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1139821
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1139822
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1139823
    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1139824
    :cond_3
    const-string v0, ""

    goto/16 :goto_2

    .line 1139825
    :cond_4
    iget-object v4, p0, LX/6ka;->key:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1139826
    :cond_5
    iget-object v0, p0, LX/6ka;->op:LX/6kc;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 1139830
    invoke-static {p0}, LX/6ka;->a(LX/6ka;)V

    .line 1139831
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1139832
    iget-object v0, p0, LX/6ka;->key:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1139833
    sget-object v0, LX/6ka;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1139834
    iget-object v0, p0, LX/6ka;->key:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1139835
    :cond_0
    iget-object v0, p0, LX/6ka;->op:LX/6kc;

    if-eqz v0, :cond_1

    .line 1139836
    iget-object v0, p0, LX/6ka;->op:LX/6kc;

    if-eqz v0, :cond_1

    .line 1139837
    sget-object v0, LX/6ka;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1139838
    iget-object v0, p0, LX/6ka;->op:LX/6kc;

    invoke-virtual {v0, p1}, LX/6kT;->a(LX/1su;)V

    .line 1139839
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1139840
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1139841
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1139776
    if-nez p1, :cond_1

    .line 1139777
    :cond_0
    :goto_0
    return v0

    .line 1139778
    :cond_1
    instance-of v1, p1, LX/6ka;

    if-eqz v1, :cond_0

    .line 1139779
    check-cast p1, LX/6ka;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1139780
    if-nez p1, :cond_3

    .line 1139781
    :cond_2
    :goto_1
    move v0, v2

    .line 1139782
    goto :goto_0

    .line 1139783
    :cond_3
    iget-object v0, p0, LX/6ka;->key:Ljava/lang/String;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1139784
    :goto_2
    iget-object v3, p1, LX/6ka;->key:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1139785
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1139786
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1139787
    iget-object v0, p0, LX/6ka;->key:Ljava/lang/String;

    iget-object v3, p1, LX/6ka;->key:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1139788
    :cond_5
    iget-object v0, p0, LX/6ka;->op:LX/6kc;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1139789
    :goto_4
    iget-object v3, p1, LX/6ka;->op:LX/6kc;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1139790
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1139791
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1139792
    iget-object v0, p0, LX/6ka;->op:LX/6kc;

    iget-object v3, p1, LX/6ka;->op:LX/6kc;

    invoke-virtual {v0, v3}, LX/6kc;->a(LX/6kc;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1139793
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1139794
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1139795
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1139796
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1139797
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1139775
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1139772
    sget-boolean v0, LX/6ka;->a:Z

    .line 1139773
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6ka;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1139774
    return-object v0
.end method
