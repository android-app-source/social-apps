.class public LX/6st;
.super LX/6E8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E8",
        "<",
        "LX/6ss;",
        "LX/6su;",
        ">;"
    }
.end annotation


# instance fields
.field private l:LX/6qh;


# direct methods
.method public constructor <init>(LX/6ss;)V
    .locals 0

    .prologue
    .line 1153848
    invoke-direct {p0, p1}, LX/6E8;-><init>(LX/6E6;)V

    .line 1153849
    return-void
.end method


# virtual methods
.method public final a(LX/6E2;)V
    .locals 4

    .prologue
    .line 1153834
    check-cast p1, LX/6su;

    .line 1153835
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    check-cast v0, LX/6ss;

    .line 1153836
    iget-object v1, p0, LX/6st;->l:LX/6qh;

    .line 1153837
    iput-object v1, v0, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a:LX/6qh;

    .line 1153838
    iget-object v1, p1, LX/6su;->a:Ljava/lang/String;

    .line 1153839
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1153840
    iget-object v2, v0, LX/6ss;->a:Lcom/facebook/widget/text/BetterTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1153841
    iget-object v2, v0, LX/6ss;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1153842
    iget-object v2, v0, LX/6ss;->b:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setTextColor(I)V

    .line 1153843
    :goto_0
    iget-object v1, p1, LX/6su;->b:Ljava/lang/String;

    .line 1153844
    iget-object v2, v0, LX/6ss;->b:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1153845
    return-void

    .line 1153846
    :cond_0
    iget-object v2, v0, LX/6ss;->a:Lcom/facebook/widget/text/BetterTextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1153847
    iget-object v2, v0, LX/6ss;->b:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    invoke-virtual {v0}, LX/6ss;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p0, 0x7f0a00a3

    invoke-virtual {v3, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1153832
    iput-object p1, p0, LX/6st;->l:LX/6qh;

    .line 1153833
    return-void
.end method
