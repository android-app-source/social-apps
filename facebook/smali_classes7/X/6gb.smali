.class public final LX/6gb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1125770
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1125771
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125772
    :goto_0
    return v1

    .line 1125773
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1125774
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1125775
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1125776
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1125777
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1125778
    const-string v6, "best_mask_package"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1125779
    invoke-static {p0, p1}, LX/6gY;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1125780
    :cond_2
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1125781
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1125782
    :cond_3
    const-string v6, "instructions"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1125783
    invoke-static {p0, p1}, LX/6gZ;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1125784
    :cond_4
    const-string v6, "thumbnail_image"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1125785
    invoke-static {p0, p1}, LX/6ga;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1125786
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1125787
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1125788
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1125789
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1125790
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1125791
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1125792
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1125793
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125794
    if-eqz v0, :cond_0

    .line 1125795
    const-string v1, "best_mask_package"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125796
    invoke-static {p0, v0, p2, p3}, LX/6gY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1125797
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1125798
    if-eqz v0, :cond_1

    .line 1125799
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125800
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1125801
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125802
    if-eqz v0, :cond_2

    .line 1125803
    const-string v1, "instructions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125804
    invoke-static {p0, v0, p2}, LX/6gZ;->a(LX/15i;ILX/0nX;)V

    .line 1125805
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1125806
    if-eqz v0, :cond_3

    .line 1125807
    const-string v1, "thumbnail_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1125808
    invoke-static {p0, v0, p2}, LX/6ga;->a(LX/15i;ILX/0nX;)V

    .line 1125809
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1125810
    return-void
.end method
