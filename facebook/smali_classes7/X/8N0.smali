.class public LX/8N0;
.super Ljava/lang/Exception;
.source ""

# interfaces
.implements LX/73y;


# instance fields
.field public mInterpreter:LX/73z;


# direct methods
.method public constructor <init>(LX/73z;)V
    .locals 0

    .prologue
    .line 1335542
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 1335543
    iput-object p1, p0, LX/8N0;->mInterpreter:LX/73z;

    .line 1335544
    return-void
.end method

.method public static a(LX/0cX;Ljava/lang/Exception;)LX/73z;
    .locals 1

    .prologue
    .line 1335545
    instance-of v0, p1, LX/8N0;

    if-eqz v0, :cond_0

    .line 1335546
    check-cast p1, LX/8N0;

    .line 1335547
    iget-object v0, p1, LX/8N0;->mInterpreter:LX/73z;

    move-object v0, v0

    .line 1335548
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, LX/0cX;->a(Ljava/lang/Exception;)LX/73z;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1335549
    iget-object v0, p0, LX/8N0;->mInterpreter:LX/73z;

    invoke-virtual {v0}, LX/73z;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1335550
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1335551
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1335552
    iget-object v0, p0, LX/8N0;->mInterpreter:LX/73z;

    invoke-virtual {v0}, LX/73z;->e()I

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1335553
    iget-object v0, p0, LX/8N0;->mInterpreter:LX/73z;

    invoke-virtual {v0}, LX/73z;->f()I

    move-result v0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1335554
    iget-object v0, p0, LX/8N0;->mInterpreter:LX/73z;

    invoke-virtual {v0}, LX/73z;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()LX/73z;
    .locals 1

    .prologue
    .line 1335555
    iget-object v0, p0, LX/8N0;->mInterpreter:LX/73z;

    return-object v0
.end method
