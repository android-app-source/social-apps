.class public LX/6po;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/6po;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6pe;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6px;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6pZ;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6pi;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6pm;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6pr;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6pu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/6pe;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6px;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6pZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6pi;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6pm;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6pr;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6pu;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1149639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1149640
    iput-object p1, p0, LX/6po;->a:LX/0Ot;

    .line 1149641
    iput-object p2, p0, LX/6po;->b:LX/0Ot;

    .line 1149642
    iput-object p3, p0, LX/6po;->c:LX/0Ot;

    .line 1149643
    iput-object p4, p0, LX/6po;->d:LX/0Ot;

    .line 1149644
    iput-object p5, p0, LX/6po;->e:LX/0Ot;

    .line 1149645
    iput-object p6, p0, LX/6po;->f:LX/0Ot;

    .line 1149646
    iput-object p7, p0, LX/6po;->g:LX/0Ot;

    .line 1149647
    return-void
.end method

.method public static a(LX/0QB;)LX/6po;
    .locals 11

    .prologue
    .line 1149648
    sget-object v0, LX/6po;->h:LX/6po;

    if-nez v0, :cond_1

    .line 1149649
    const-class v1, LX/6po;

    monitor-enter v1

    .line 1149650
    :try_start_0
    sget-object v0, LX/6po;->h:LX/6po;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1149651
    if-eqz v2, :cond_0

    .line 1149652
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1149653
    new-instance v3, LX/6po;

    const/16 v4, 0x2c86

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2c8c

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2c85

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2c87

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2c88

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2c8a

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2c8b

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/6po;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1149654
    move-object v0, v3

    .line 1149655
    sput-object v0, LX/6po;->h:LX/6po;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1149656
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1149657
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1149658
    :cond_1
    sget-object v0, LX/6po;->h:LX/6po;

    return-object v0

    .line 1149659
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1149660
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/6pF;)LX/6pY;
    .locals 3

    .prologue
    .line 1149661
    sget-object v0, LX/6pn;->a:[I

    invoke-virtual {p1}, LX/6pF;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1149662
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No PinActionController for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1149663
    :pswitch_0
    iget-object v0, p0, LX/6po;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6pY;

    .line 1149664
    :goto_0
    return-object v0

    .line 1149665
    :pswitch_1
    iget-object v0, p0, LX/6po;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6pY;

    goto :goto_0

    .line 1149666
    :pswitch_2
    iget-object v0, p0, LX/6po;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6pY;

    goto :goto_0

    .line 1149667
    :pswitch_3
    iget-object v0, p0, LX/6po;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6pY;

    goto :goto_0

    .line 1149668
    :pswitch_4
    iget-object v0, p0, LX/6po;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6pY;

    goto :goto_0

    .line 1149669
    :pswitch_5
    iget-object v0, p0, LX/6po;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6pY;

    goto :goto_0

    .line 1149670
    :pswitch_6
    iget-object v0, p0, LX/6po;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6pY;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
