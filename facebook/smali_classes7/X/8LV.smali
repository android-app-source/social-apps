.class public LX/8LV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0SG;

.field public final c:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/8LT;


# direct methods
.method public constructor <init>(LX/0Or;LX/0SG;LX/0Ot;Lcom/facebook/ui/media/attachments/MediaResourceHelper;LX/8LT;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;",
            "Lcom/facebook/ui/media/attachments/MediaResourceHelper;",
            "LX/8LT;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1332968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1332969
    iput-object p1, p0, LX/8LV;->a:LX/0Or;

    .line 1332970
    iput-object p2, p0, LX/8LV;->b:LX/0SG;

    .line 1332971
    iput-object p4, p0, LX/8LV;->c:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    .line 1332972
    iput-object p3, p0, LX/8LV;->d:LX/0Ot;

    .line 1332973
    iput-object p5, p0, LX/8LV;->e:LX/8LT;

    .line 1332974
    return-void
.end method

.method public static a(LX/0QB;)LX/8LV;
    .locals 1

    .prologue
    .line 1332975
    invoke-static {p0}, LX/8LV;->b(LX/0QB;)LX/8LV;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8LV;
    .locals 6

    .prologue
    .line 1332976
    new-instance v0, LX/8LV;

    const/16 v1, 0x15e7

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    const/16 v3, 0x1032

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(LX/0QB;)Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    move-result-object v4

    check-cast v4, Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-static {p0}, LX/8LT;->b(LX/0QB;)LX/8LT;

    move-result-object v5

    check-cast v5, LX/8LT;

    invoke-direct/range {v0 .. v5}, LX/8LV;-><init>(LX/0Or;LX/0SG;LX/0Ot;Lcom/facebook/ui/media/attachments/MediaResourceHelper;LX/8LT;)V

    .line 1332977
    return-object v0
.end method


# virtual methods
.method public final a(LX/0Px;JLjava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;J",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .prologue
    .line 1332978
    new-instance v0, LX/8LQ;

    invoke-direct {v0}, LX/8LQ;-><init>()V

    .line 1332979
    iput-object p4, v0, LX/8LQ;->a:Ljava/lang/String;

    .line 1332980
    move-object v0, v0

    .line 1332981
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 1332982
    iput-object v1, v0, LX/8LQ;->b:LX/0Px;

    .line 1332983
    move-object v0, v0

    .line 1332984
    iput-wide p2, v0, LX/8LQ;->j:J

    .line 1332985
    move-object v0, v0

    .line 1332986
    sget-object v1, LX/8LR;->PLACE_PHOTO:LX/8LR;

    .line 1332987
    iput-object v1, v0, LX/8LQ;->p:LX/8LR;

    .line 1332988
    move-object v0, v0

    .line 1332989
    sget-object v1, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->d:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332990
    iput-object v1, v0, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332991
    move-object v0, v0

    .line 1332992
    sget-object v1, LX/8LS;->PLACE_PHOTO:LX/8LS;

    .line 1332993
    iput-object v1, v0, LX/8LQ;->q:LX/8LS;

    .line 1332994
    move-object v0, v0

    .line 1332995
    const-string v1, "place_photo"

    .line 1332996
    iput-object v1, v0, LX/8LQ;->i:Ljava/lang/String;

    .line 1332997
    move-object v0, v0

    .line 1332998
    iget-object v1, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1332999
    iput-wide v2, v0, LX/8LQ;->x:J

    .line 1333000
    move-object v0, v0

    .line 1333001
    invoke-virtual {v0}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;LX/0Px;JLcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "LX/0Px",
            "<",
            "Landroid/os/Bundle;",
            ">;J",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .prologue
    .line 1333002
    new-instance v0, LX/8LQ;

    invoke-direct {v0}, LX/8LQ;-><init>()V

    .line 1333003
    iput-object p6, v0, LX/8LQ;->a:Ljava/lang/String;

    .line 1333004
    move-object v0, v0

    .line 1333005
    iput-object p1, v0, LX/8LQ;->b:LX/0Px;

    .line 1333006
    move-object v0, v0

    .line 1333007
    iput-object p2, v0, LX/8LQ;->c:LX/0Px;

    .line 1333008
    move-object v0, v0

    .line 1333009
    iput-wide p3, v0, LX/8LQ;->j:J

    .line 1333010
    move-object v0, v0

    .line 1333011
    sget-object v1, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->d:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1333012
    iput-object v1, v0, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1333013
    move-object v0, v0

    .line 1333014
    sget-object v1, LX/8LR;->MENU_PHOTO:LX/8LR;

    .line 1333015
    iput-object v1, v0, LX/8LQ;->p:LX/8LR;

    .line 1333016
    move-object v0, v0

    .line 1333017
    sget-object v1, LX/8LS;->MENU_PHOTO:LX/8LS;

    .line 1333018
    iput-object v1, v0, LX/8LQ;->q:LX/8LS;

    .line 1333019
    move-object v0, v0

    .line 1333020
    const-string v1, "menu_photo"

    .line 1333021
    iput-object v1, v0, LX/8LQ;->i:Ljava/lang/String;

    .line 1333022
    move-object v0, v0

    .line 1333023
    iget-object v1, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1333024
    iput-wide v2, v0, LX/8LQ;->x:J

    .line 1333025
    move-object v0, v0

    .line 1333026
    iput-object p5, v0, LX/8LQ;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1333027
    move-object v0, v0

    .line 1333028
    invoke-virtual {v0}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;LX/0Px;JLjava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;JLjava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;LX/5Rn;JLcom/facebook/graphql/model/GraphQLBudgetRecommendationData;Ljava/lang/String;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;LX/5Ra;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 9
    .param p6    # Lcom/facebook/ipc/composer/model/MinutiaeTag;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "LX/0Px",
            "<",
            "Landroid/os/Bundle;",
            ">;J",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/model/MinutiaeTag;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/5Rn;",
            "J",
            "Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;",
            "LX/5Ra;",
            ")",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .prologue
    .line 1333029
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1333030
    new-instance v2, LX/8LQ;

    invoke-direct {v2}, LX/8LQ;-><init>()V

    move-object/from16 v0, p10

    invoke-virtual {v2, v0}, LX/8LQ;->a(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p1}, LX/8LQ;->a(LX/0Px;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/8LQ;->b(LX/0Px;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p5}, LX/8LQ;->c(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p6}, LX/8LQ;->a(Lcom/facebook/ipc/composer/model/MinutiaeTag;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, LX/8LQ;->d(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, LX/8LQ;->a(J)LX/8LQ;

    move-result-object v2

    const-string v3, "own_page_timeline"

    invoke-virtual {v2, v3}, LX/8LQ;->e(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move-wide/from16 v0, p8

    invoke-virtual {v2, v0, v1}, LX/8LQ;->b(J)LX/8LQ;

    move-result-object v2

    sget-object v3, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->d:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    invoke-virtual {v2, v3}, LX/8LQ;->a(Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;)LX/8LQ;

    move-result-object v2

    sget-object v3, LX/8LR;->SLIDESHOW:LX/8LR;

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LR;)LX/8LQ;

    move-result-object v2

    sget-object v3, LX/8LS;->TARGET:LX/8LS;

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LS;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p11

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/8LQ;

    move-result-object v2

    iget-object v3, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, LX/8LQ;->d(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p12

    invoke-virtual {v2, v0}, LX/8LQ;->a(LX/5Rn;)LX/8LQ;

    move-result-object v2

    move-wide/from16 v0, p13

    invoke-virtual {v2, v0, v1}, LX/8LQ;->e(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p15

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p16

    invoke-virtual {v2, v0}, LX/8LQ;->i(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move/from16 v0, p17

    invoke-virtual {v2, v0}, LX/8LQ;->e(Z)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p18

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p19

    invoke-virtual {v2, v0}, LX/8LQ;->a(LX/5Ra;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    return-object v2
.end method

.method public final a(LX/0Px;LX/0Px;JLjava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;JZZLjava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;LX/5Rn;JLcom/facebook/graphql/model/GraphQLBudgetRecommendationData;Ljava/lang/String;ZZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 9
    .param p6    # Lcom/facebook/ipc/composer/model/MinutiaeTag;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "LX/0Px",
            "<",
            "Landroid/os/Bundle;",
            ">;J",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/model/MinutiaeTag;",
            "Ljava/lang/String;",
            "JZZ",
            "Ljava/lang/String;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/5Rn;",
            "J",
            "Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;",
            "Ljava/lang/String;",
            "ZZ",
            "Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .prologue
    .line 1333031
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1333032
    sget-object v3, LX/8LR;->STATUS:LX/8LR;

    .line 1333033
    const-string v2, "own_page_timeline"

    .line 1333034
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    if-eqz p13, :cond_0

    invoke-virtual/range {p13 .. p13}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v4, v4, p3

    if-eqz v4, :cond_2

    .line 1333035
    :cond_0
    sget-object v3, LX/8LR;->TARGET:LX/8LR;

    .line 1333036
    const-string v2, "target_as_target"

    .line 1333037
    :cond_1
    :goto_0
    new-instance v4, LX/8LQ;

    invoke-direct {v4}, LX/8LQ;-><init>()V

    move-object/from16 v0, p12

    invoke-virtual {v4, v0}, LX/8LQ;->a(Ljava/lang/String;)LX/8LQ;

    move-result-object v4

    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/8LQ;->a(LX/0Px;)LX/8LQ;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/8LQ;->b(LX/0Px;)LX/8LQ;

    move-result-object v4

    invoke-virtual {v4, p5}, LX/8LQ;->c(Ljava/lang/String;)LX/8LQ;

    move-result-object v4

    invoke-virtual {v4, p6}, LX/8LQ;->a(Lcom/facebook/ipc/composer/model/MinutiaeTag;)LX/8LQ;

    move-result-object v4

    move-object/from16 v0, p7

    invoke-virtual {v4, v0}, LX/8LQ;->d(Ljava/lang/String;)LX/8LQ;

    move-result-object v4

    invoke-virtual {v4, p3, p4}, LX/8LQ;->a(J)LX/8LQ;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/8LQ;->e(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move-wide/from16 v0, p8

    invoke-virtual {v2, v0, v1}, LX/8LQ;->b(J)LX/8LQ;

    move-result-object v2

    move/from16 v0, p10

    invoke-virtual {v2, v0}, LX/8LQ;->a(Z)LX/8LQ;

    move-result-object v2

    move/from16 v0, p11

    invoke-virtual {v2, v0}, LX/8LQ;->b(Z)LX/8LQ;

    move-result-object v2

    sget-object v4, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->d:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    invoke-virtual {v2, v4}, LX/8LQ;->a(Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LR;)LX/8LQ;

    move-result-object v2

    sget-object v3, LX/8LS;->TARGET:LX/8LS;

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LS;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p13

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/8LQ;

    move-result-object v2

    iget-object v3, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, LX/8LQ;->d(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p14

    invoke-virtual {v2, v0}, LX/8LQ;->a(LX/5Rn;)LX/8LQ;

    move-result-object v2

    move-wide/from16 v0, p15

    invoke-virtual {v2, v0, v1}, LX/8LQ;->e(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p17

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p18

    invoke-virtual {v2, v0}, LX/8LQ;->i(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move/from16 v0, p19

    invoke-virtual {v2, v0}, LX/8LQ;->e(Z)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p21

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p22

    invoke-virtual {v2, v0}, LX/8LQ;->g(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    return-object v2

    .line 1333038
    :cond_2
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    if-eqz p17, :cond_3

    invoke-virtual/range {p17 .. p17}, Lcom/facebook/graphql/model/GraphQLBudgetRecommendationData;->a()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->a()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_4

    .line 1333039
    :cond_3
    sget-object v3, LX/8LR;->SINGLE_PHOTO:LX/8LR;

    goto/16 :goto_0

    .line 1333040
    :cond_4
    if-eqz p20, :cond_1

    .line 1333041
    sget-object v3, LX/8LR;->MULTIMEDIA:LX/8LR;

    goto/16 :goto_0
.end method

.method public final a(LX/0Px;LX/0Px;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "LX/0Px",
            "<",
            "Landroid/os/Bundle;",
            ">;J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "Ljava/lang/String;",
            "Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;",
            ")",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .prologue
    .line 1333042
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1333043
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1333044
    new-instance v2, LX/8LQ;

    invoke-direct {v2}, LX/8LQ;-><init>()V

    invoke-virtual {v2, p6}, LX/8LQ;->a(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/0Px;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/8LQ;->b(LX/0Px;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p5}, LX/8LQ;->c(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, LX/8LQ;->a(J)LX/8LQ;

    move-result-object v2

    const-string v3, "own_page_album"

    invoke-virtual {v2, v3}, LX/8LQ;->e(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v4, v5}, LX/8LQ;->b(J)LX/8LQ;

    move-result-object v2

    sget-object v3, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->d:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    invoke-virtual {v2, v3}, LX/8LQ;->a(Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;)LX/8LQ;

    move-result-object v2

    sget-object v3, LX/8LR;->TARGET:LX/8LR;

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LR;)LX/8LQ;

    move-result-object v2

    sget-object v3, LX/8LS;->ALBUM:LX/8LS;

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LS;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/8LQ;

    move-result-object v2

    iget-object v3, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, LX/8LQ;->d(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p8

    invoke-virtual {v2, v0}, LX/8LQ;->i(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p9

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    return-object v2

    .line 1333045
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(LX/0Px;LX/0Px;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;LX/0Px;JZLjava/lang/String;ZZLjava/lang/String;Lcom/facebook/ipc/composer/model/ProductItemAttachment;JLjava/lang/String;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Z)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 8
    .param p2    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p13    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p17    # Lcom/facebook/ipc/composer/model/ProductItemAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "LX/0Px",
            "<",
            "Landroid/os/Bundle;",
            ">;J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/model/MinutiaeTag;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;JZ",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/model/ProductItemAttachment;",
            "J",
            "Ljava/lang/String;",
            "Z",
            "Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;",
            "Z)",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .prologue
    .line 1333046
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1333047
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-lez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1333048
    if-nez p17, :cond_3

    .line 1333049
    if-eqz p21, :cond_2

    .line 1333050
    sget-object v2, LX/8LR;->MULTIMEDIA:LX/8LR;

    .line 1333051
    :goto_1
    new-instance v3, LX/8LQ;

    invoke-direct {v3}, LX/8LQ;-><init>()V

    move-object/from16 v0, p16

    invoke-virtual {v3, v0}, LX/8LQ;->a(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/8LQ;->a(LX/0Px;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/8LQ;->b(LX/0Px;)LX/8LQ;

    move-result-object v3

    invoke-static/range {p9 .. p9}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/8LQ;->c(LX/0Px;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, p6}, LX/8LQ;->c(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, p7}, LX/8LQ;->a(Lcom/facebook/ipc/composer/model/MinutiaeTag;)LX/8LQ;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-virtual {v3, v0}, LX/8LQ;->d(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, LX/8LQ;->a(J)LX/8LQ;

    move-result-object v3

    invoke-static {p5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string p5, "targeted"

    :cond_0
    invoke-virtual {v3, p5}, LX/8LQ;->e(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    move-wide/from16 v0, p10

    invoke-virtual {v3, v0, v1}, LX/8LQ;->b(J)LX/8LQ;

    move-result-object v3

    move/from16 v0, p12

    invoke-virtual {v3, v0}, LX/8LQ;->e(Z)LX/8LQ;

    move-result-object v3

    move-object/from16 v0, p13

    invoke-virtual {v3, v0}, LX/8LQ;->f(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    move/from16 v0, p14

    invoke-virtual {v3, v0}, LX/8LQ;->a(Z)LX/8LQ;

    move-result-object v3

    move/from16 v0, p15

    invoke-virtual {v3, v0}, LX/8LQ;->b(Z)LX/8LQ;

    move-result-object v3

    sget-object v4, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->a:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    invoke-virtual {v3, v4}, LX/8LQ;->a(Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/8LQ;->a(LX/8LR;)LX/8LQ;

    move-result-object v2

    sget-object v3, LX/8LS;->TARGET:LX/8LS;

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LS;)LX/8LQ;

    move-result-object v2

    iget-object v3, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, LX/8LQ;->d(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p17

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)LX/8LQ;

    move-result-object v2

    move-wide/from16 v0, p18

    invoke-virtual {v2, v0, v1}, LX/8LQ;->f(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p20

    invoke-virtual {v2, v0}, LX/8LQ;->i(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p22

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)LX/8LQ;

    move-result-object v2

    move/from16 v0, p23

    invoke-virtual {v2, v0}, LX/8LQ;->i(Z)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    return-object v2

    .line 1333052
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1333053
    :cond_2
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    sget-object v2, LX/8LR;->TARGET:LX/8LR;

    goto/16 :goto_1

    :cond_3
    sget-object v2, LX/8LR;->STATUS:LX/8LR;

    goto/16 :goto_1
.end method

.method public final a(LX/0Px;LX/0Px;Ljava/lang/String;JLX/0Px;JLjava/lang/String;ZZLjava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;ZZLjava/lang/String;ZZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 8
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p13    # Lcom/facebook/share/model/ComposerAppAttribution;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "LX/0Px",
            "<",
            "Landroid/os/Bundle;",
            ">;",
            "Ljava/lang/String;",
            "J",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;J",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/lang/String;",
            "Lcom/facebook/share/model/ComposerAppAttribution;",
            "ZZ",
            "Ljava/lang/String;",
            "ZZ",
            "Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;",
            ")",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .prologue
    .line 1333054
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1333055
    if-nez p17, :cond_0

    .line 1333056
    new-instance v2, LX/8LQ;

    invoke-direct {v2}, LX/8LQ;-><init>()V

    move-object/from16 v0, p12

    invoke-virtual {v2, v0}, LX/8LQ;->a(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/0Px;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/8LQ;->b(LX/0Px;)LX/8LQ;

    move-result-object v2

    invoke-static {p6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8LQ;->c(LX/0Px;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/8LQ;->c(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, LX/8LQ;->a(J)LX/8LQ;

    move-result-object v2

    const-string v3, "album"

    invoke-virtual {v2, v3}, LX/8LQ;->e(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move-wide v0, p7

    invoke-virtual {v2, v0, v1}, LX/8LQ;->b(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p9

    invoke-virtual {v2, v0}, LX/8LQ;->f(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move/from16 v0, p10

    invoke-virtual {v2, v0}, LX/8LQ;->a(Z)LX/8LQ;

    move-result-object v2

    move/from16 v0, p11

    invoke-virtual {v2, v0}, LX/8LQ;->b(Z)LX/8LQ;

    move-result-object v2

    sget-object v3, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->a:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    invoke-virtual {v2, v3}, LX/8LQ;->a(Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;)LX/8LQ;

    move-result-object v2

    sget-object v3, LX/8LR;->TARGET:LX/8LR;

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LR;)LX/8LQ;

    move-result-object v2

    sget-object v3, LX/8LS;->ALBUM:LX/8LS;

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LS;)LX/8LQ;

    move-result-object v2

    iget-object v3, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, LX/8LQ;->d(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p13

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/share/model/ComposerAppAttribution;)LX/8LQ;

    move-result-object v2

    move/from16 v0, p14

    invoke-virtual {v2, v0}, LX/8LQ;->d(Z)LX/8LQ;

    move-result-object v2

    move/from16 v0, p15

    invoke-virtual {v2, v0}, LX/8LQ;->e(Z)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p16

    invoke-virtual {v2, v0}, LX/8LQ;->i(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move/from16 v0, p18

    invoke-virtual {v2, v0}, LX/8LQ;->g(Z)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p19

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    .line 1333057
    :goto_0
    return-object v2

    :cond_0
    new-instance v2, LX/8LQ;

    invoke-direct {v2}, LX/8LQ;-><init>()V

    move-object/from16 v0, p12

    invoke-virtual {v2, v0}, LX/8LQ;->a(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/0Px;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/8LQ;->b(LX/0Px;)LX/8LQ;

    move-result-object v2

    invoke-static {p6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8LQ;->c(LX/0Px;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/8LQ;->c(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    iget-object v2, p0, LX/8LV;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, LX/8LQ;->a(J)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, LX/8LQ;->c(J)LX/8LQ;

    move-result-object v2

    const-string v3, "own_timeline"

    invoke-virtual {v2, v3}, LX/8LQ;->e(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move-wide v0, p7

    invoke-virtual {v2, v0, v1}, LX/8LQ;->b(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p9

    invoke-virtual {v2, v0}, LX/8LQ;->f(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move/from16 v0, p10

    invoke-virtual {v2, v0}, LX/8LQ;->a(Z)LX/8LQ;

    move-result-object v2

    move/from16 v0, p11

    invoke-virtual {v2, v0}, LX/8LQ;->b(Z)LX/8LQ;

    move-result-object v2

    sget-object v3, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->a:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    invoke-virtual {v2, v3}, LX/8LQ;->a(Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;)LX/8LQ;

    move-result-object v2

    sget-object v3, LX/8LR;->MULTIMEDIA:LX/8LR;

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LR;)LX/8LQ;

    move-result-object v2

    sget-object v3, LX/8LS;->OWN_TIMELINE:LX/8LS;

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LS;)LX/8LQ;

    move-result-object v2

    iget-object v3, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, LX/8LQ;->d(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p13

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/share/model/ComposerAppAttribution;)LX/8LQ;

    move-result-object v2

    move/from16 v0, p14

    invoke-virtual {v2, v0}, LX/8LQ;->d(Z)LX/8LQ;

    move-result-object v2

    move/from16 v0, p15

    invoke-virtual {v2, v0}, LX/8LQ;->e(Z)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p16

    invoke-virtual {v2, v0}, LX/8LQ;->i(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move/from16 v0, p18

    invoke-virtual {v2, v0}, LX/8LQ;->g(Z)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p19

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public final a(LX/0Px;LX/0Px;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;LX/0Px;JLjava/lang/String;ZZLjava/lang/String;Lcom/facebook/ipc/composer/model/ProductItemAttachment;JLcom/facebook/share/model/ComposerAppAttribution;ZZLjava/lang/String;ZZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;Lcom/facebook/audience/model/UploadShot;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 8
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p14    # Lcom/facebook/ipc/composer/model/ProductItemAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p24    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p25    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p26    # Lcom/facebook/audience/model/UploadShot;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "LX/0Px",
            "<",
            "Landroid/os/Bundle;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/model/MinutiaeTag;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;J",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/model/ProductItemAttachment;",
            "J",
            "Lcom/facebook/share/model/ComposerAppAttribution;",
            "ZZ",
            "Ljava/lang/String;",
            "ZZ",
            "Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;",
            "Lcom/facebook/productionprompts/logging/PromptAnalytics;",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;",
            "Lcom/facebook/audience/model/UploadShot;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .prologue
    .line 1332962
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1332963
    if-nez p14, :cond_1

    .line 1332964
    if-eqz p21, :cond_0

    .line 1332965
    sget-object v2, LX/8LR;->MULTIMEDIA:LX/8LR;

    move-object v3, v2

    .line 1332966
    :goto_0
    new-instance v2, LX/8LQ;

    invoke-direct {v2}, LX/8LQ;-><init>()V

    move-object/from16 v0, p13

    invoke-virtual {v2, v0}, LX/8LQ;->a(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/8LQ;->a(LX/0Px;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/8LQ;->b(LX/0Px;)LX/8LQ;

    move-result-object v2

    invoke-static {p7}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/8LQ;->c(LX/0Px;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/8LQ;->c(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p4}, LX/8LQ;->a(Lcom/facebook/ipc/composer/model/MinutiaeTag;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p5}, LX/8LQ;->d(Ljava/lang/String;)LX/8LQ;

    move-result-object v4

    iget-object v2, p0, LX/8LV;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, LX/8LQ;->a(J)LX/8LQ;

    move-result-object v2

    const-string v4, "own_timeline"

    invoke-virtual {v2, v4}, LX/8LQ;->e(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move-wide/from16 v0, p8

    invoke-virtual {v2, v0, v1}, LX/8LQ;->b(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p10

    invoke-virtual {v2, v0}, LX/8LQ;->f(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move/from16 v0, p11

    invoke-virtual {v2, v0}, LX/8LQ;->a(Z)LX/8LQ;

    move-result-object v2

    move/from16 v0, p12

    invoke-virtual {v2, v0}, LX/8LQ;->b(Z)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p6}, LX/8LQ;->a(Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LR;)LX/8LQ;

    move-result-object v2

    sget-object v3, LX/8LS;->OWN_TIMELINE:LX/8LS;

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LS;)LX/8LQ;

    move-result-object v2

    iget-object v3, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, LX/8LQ;->d(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p14

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)LX/8LQ;

    move-result-object v2

    move-wide/from16 v0, p15

    invoke-virtual {v2, v0, v1}, LX/8LQ;->f(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p17

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/share/model/ComposerAppAttribution;)LX/8LQ;

    move-result-object v2

    move/from16 v0, p18

    invoke-virtual {v2, v0}, LX/8LQ;->d(Z)LX/8LQ;

    move-result-object v2

    move/from16 v0, p19

    invoke-virtual {v2, v0}, LX/8LQ;->e(Z)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p20

    invoke-virtual {v2, v0}, LX/8LQ;->i(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move/from16 v0, p22

    invoke-virtual {v2, v0}, LX/8LQ;->g(Z)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p23

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p24

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/productionprompts/logging/PromptAnalytics;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p25

    invoke-virtual {v2, v0}, LX/8LQ;->d(LX/0Px;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p26

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/audience/model/UploadShot;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p27

    invoke-virtual {v2, v0}, LX/8LQ;->g(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    return-object v2

    .line 1332967
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    sget-object v2, LX/8LR;->SINGLE_PHOTO:LX/8LR;

    move-object v3, v2

    goto/16 :goto_0

    :cond_1
    sget-object v2, LX/8LR;->STATUS:LX/8LR;

    move-object v3, v2

    goto/16 :goto_0
.end method

.method public final a(LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/composer/publish/common/EditPostParams;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "LX/0Px",
            "<",
            "Landroid/os/Bundle;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/composer/publish/common/EditPostParams;",
            ")",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .prologue
    .line 1333058
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1333059
    new-instance v0, LX/8LQ;

    invoke-direct {v0}, LX/8LQ;-><init>()V

    .line 1333060
    iput-object p4, v0, LX/8LQ;->a:Ljava/lang/String;

    .line 1333061
    move-object v0, v0

    .line 1333062
    iput-object p1, v0, LX/8LQ;->b:LX/0Px;

    .line 1333063
    move-object v0, v0

    .line 1333064
    iput-object p2, v0, LX/8LQ;->c:LX/0Px;

    .line 1333065
    move-object v0, v0

    .line 1333066
    invoke-virtual {p6}, Lcom/facebook/composer/publish/common/EditPostParams;->getTargetId()J

    move-result-wide v2

    .line 1333067
    iput-wide v2, v0, LX/8LQ;->h:J

    .line 1333068
    move-object v0, v0

    .line 1333069
    invoke-virtual {p6}, Lcom/facebook/composer/publish/common/EditPostParams;->getLegacyStoryApiId()Ljava/lang/String;

    move-result-object v1

    .line 1333070
    iput-object v1, v0, LX/8LQ;->T:Ljava/lang/String;

    .line 1333071
    move-object v0, v0

    .line 1333072
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p3, "photo_story"

    .line 1333073
    :cond_0
    iput-object p3, v0, LX/8LQ;->i:Ljava/lang/String;

    .line 1333074
    move-object v0, v0

    .line 1333075
    const-wide/16 v2, -0x1

    .line 1333076
    iput-wide v2, v0, LX/8LQ;->j:J

    .line 1333077
    move-object v0, v0

    .line 1333078
    sget-object v1, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->a:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1333079
    iput-object v1, v0, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1333080
    move-object v0, v0

    .line 1333081
    sget-object v1, LX/8LR;->EDIT_MULTIMEDIA:LX/8LR;

    .line 1333082
    iput-object v1, v0, LX/8LQ;->p:LX/8LR;

    .line 1333083
    move-object v0, v0

    .line 1333084
    sget-object v1, LX/8LS;->TARGET:LX/8LS;

    .line 1333085
    iput-object v1, v0, LX/8LQ;->q:LX/8LS;

    .line 1333086
    move-object v0, v0

    .line 1333087
    iget-object v1, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1333088
    iput-wide v2, v0, LX/8LQ;->x:J

    .line 1333089
    move-object v0, v0

    .line 1333090
    iput-object p5, v0, LX/8LQ;->I:Ljava/lang/String;

    .line 1333091
    move-object v0, v0

    .line 1333092
    iput-object p6, v0, LX/8LQ;->U:Lcom/facebook/composer/publish/common/EditPostParams;

    .line 1333093
    move-object v0, v0

    .line 1333094
    invoke-virtual {v0}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;Lcom/facebook/composer/protocol/PostReviewParams;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "Lcom/facebook/composer/protocol/PostReviewParams;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .prologue
    .line 1332925
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1332926
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1332927
    new-instance v0, LX/8LQ;

    invoke-direct {v0}, LX/8LQ;-><init>()V

    .line 1332928
    iput-object p3, v0, LX/8LQ;->a:Ljava/lang/String;

    .line 1332929
    move-object v0, v0

    .line 1332930
    iput-object p1, v0, LX/8LQ;->b:LX/0Px;

    .line 1332931
    move-object v0, v0

    .line 1332932
    iget-object v1, p2, Lcom/facebook/composer/protocol/PostReviewParams;->c:Ljava/lang/String;

    .line 1332933
    iput-object v1, v0, LX/8LQ;->e:Ljava/lang/String;

    .line 1332934
    move-object v0, v0

    .line 1332935
    const-wide/16 v2, -0x1

    .line 1332936
    iput-wide v2, v0, LX/8LQ;->h:J

    .line 1332937
    move-object v0, v0

    .line 1332938
    const-string v1, "review"

    .line 1332939
    iput-object v1, v0, LX/8LQ;->i:Ljava/lang/String;

    .line 1332940
    move-object v0, v0

    .line 1332941
    iget-wide v2, p2, Lcom/facebook/composer/protocol/PostReviewParams;->b:J

    .line 1332942
    iput-wide v2, v0, LX/8LQ;->j:J

    .line 1332943
    move-object v0, v0

    .line 1332944
    new-instance v1, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    iget-object v2, p2, Lcom/facebook/composer/protocol/PostReviewParams;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;-><init>(Ljava/lang/String;)V

    .line 1332945
    iput-object v1, v0, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332946
    move-object v0, v0

    .line 1332947
    sget-object v1, LX/8LR;->PHOTO_REVIEW:LX/8LR;

    .line 1332948
    iput-object v1, v0, LX/8LQ;->p:LX/8LR;

    .line 1332949
    move-object v0, v0

    .line 1332950
    sget-object v1, LX/8LS;->PHOTO_REVIEW:LX/8LS;

    .line 1332951
    iput-object v1, v0, LX/8LQ;->q:LX/8LS;

    .line 1332952
    move-object v0, v0

    .line 1332953
    const/4 v1, 0x1

    .line 1332954
    iput-boolean v1, v0, LX/8LQ;->u:Z

    .line 1332955
    move-object v0, v0

    .line 1332956
    iput-object p2, v0, LX/8LQ;->v:Lcom/facebook/composer/protocol/PostReviewParams;

    .line 1332957
    move-object v0, v0

    .line 1332958
    iget-object v1, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1332959
    iput-wide v2, v0, LX/8LQ;->x:J

    .line 1332960
    move-object v0, v0

    .line 1332961
    invoke-virtual {v0}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/composer/publish/common/EditPostParams;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/composer/publish/common/EditPostParams;",
            ")",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .prologue
    .line 1332889
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1332890
    new-instance v0, LX/8LQ;

    invoke-direct {v0}, LX/8LQ;-><init>()V

    .line 1332891
    iput-object p3, v0, LX/8LQ;->a:Ljava/lang/String;

    .line 1332892
    move-object v0, v0

    .line 1332893
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 1332894
    iput-object v1, v0, LX/8LQ;->b:LX/0Px;

    .line 1332895
    move-object v0, v0

    .line 1332896
    invoke-virtual {p5}, Lcom/facebook/composer/publish/common/EditPostParams;->getTargetId()J

    move-result-wide v2

    .line 1332897
    iput-wide v2, v0, LX/8LQ;->h:J

    .line 1332898
    move-object v0, v0

    .line 1332899
    invoke-virtual {p5}, Lcom/facebook/composer/publish/common/EditPostParams;->getLegacyStoryApiId()Ljava/lang/String;

    move-result-object v1

    .line 1332900
    iput-object v1, v0, LX/8LQ;->T:Ljava/lang/String;

    .line 1332901
    move-object v0, v0

    .line 1332902
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p2, "photo_story"

    .line 1332903
    :cond_0
    iput-object p2, v0, LX/8LQ;->i:Ljava/lang/String;

    .line 1332904
    move-object v0, v0

    .line 1332905
    const-wide/16 v2, -0x1

    .line 1332906
    iput-wide v2, v0, LX/8LQ;->j:J

    .line 1332907
    move-object v0, v0

    .line 1332908
    sget-object v1, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->a:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332909
    iput-object v1, v0, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332910
    move-object v0, v0

    .line 1332911
    sget-object v1, LX/8LR;->EDIT_POST:LX/8LR;

    .line 1332912
    iput-object v1, v0, LX/8LQ;->p:LX/8LR;

    .line 1332913
    move-object v0, v0

    .line 1332914
    sget-object v1, LX/8LS;->TARGET:LX/8LS;

    .line 1332915
    iput-object v1, v0, LX/8LQ;->q:LX/8LS;

    .line 1332916
    move-object v0, v0

    .line 1332917
    iget-object v1, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1332918
    iput-wide v2, v0, LX/8LQ;->x:J

    .line 1332919
    move-object v0, v0

    .line 1332920
    iput-object p4, v0, LX/8LQ;->I:Ljava/lang/String;

    .line 1332921
    move-object v0, v0

    .line 1332922
    iput-object p5, v0, LX/8LQ;->U:Lcom/facebook/composer/publish/common/EditPostParams;

    .line 1332923
    move-object v0, v0

    .line 1332924
    invoke-virtual {v0}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/audience/model/UploadShot;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1332654
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getMediaType()LX/7gi;

    move-result-object v0

    sget-object v2, LX/7gi;->PHOTO:LX/7gi;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 1332655
    :goto_0
    new-instance v2, Ljava/io/File;

    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1332656
    new-instance v3, LX/4gN;

    invoke-direct {v3}, LX/4gN;-><init>()V

    new-instance v4, LX/4gP;

    invoke-direct {v4}, LX/4gP;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object v4

    iget-object v5, p0, LX/8LV;->c:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-virtual {v5, v2}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/ipc/media/data/MimeType;->a(Ljava/lang/String;)Lcom/facebook/ipc/media/data/MimeType;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4gP;->a(Lcom/facebook/ipc/media/data/MimeType;)LX/4gP;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v4

    if-eqz v0, :cond_1

    sget-object v2, LX/4gQ;->Photo:LX/4gQ;

    :goto_1
    invoke-virtual {v4, v2}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v2

    invoke-virtual {v2}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getCreatedAtTime()J

    move-result-wide v4

    .line 1332657
    iput-wide v4, v2, LX/4gN;->b:J

    .line 1332658
    move-object v2, v2

    .line 1332659
    const-string v3, ""

    invoke-virtual {v2, v3}, LX/4gN;->a(Ljava/lang/String;)LX/4gN;

    move-result-object v2

    invoke-virtual {v2}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v2

    .line 1332660
    if-eqz v0, :cond_2

    new-instance v0, LX/74k;

    invoke-direct {v0}, LX/74k;-><init>()V

    .line 1332661
    iput-object v2, v0, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1332662
    move-object v0, v0

    .line 1332663
    invoke-virtual {v0}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v0

    move-object v2, v0

    .line 1332664
    :goto_2
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1332665
    const-string v0, "creative_editing_metadata"

    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getCreativeEditingData()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1332666
    const-string v0, "video_creative_editing_metadata"

    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getVideoCreativeEditingData()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1332667
    const-string v0, "video_upload_quality"

    const-string v4, "raw"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1332668
    new-instance v0, LX/8LQ;

    invoke-direct {v0}, LX/8LQ;-><init>()V

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 1332669
    iput-object v4, v0, LX/8LQ;->b:LX/0Px;

    .line 1332670
    move-object v4, v0

    .line 1332671
    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getCreativeEditingData()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 1332672
    :goto_3
    iput-object v0, v4, LX/8LQ;->c:LX/0Px;

    .line 1332673
    move-object v0, v4

    .line 1332674
    const-string v4, ""

    .line 1332675
    iput-object v4, v0, LX/8LQ;->e:Ljava/lang/String;

    .line 1332676
    move-object v0, v0

    .line 1332677
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1332678
    iput-object v4, v0, LX/8LQ;->a:Ljava/lang/String;

    .line 1332679
    move-object v0, v0

    .line 1332680
    sget-object v4, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    invoke-virtual {v0, v4}, LX/8LQ;->a(Lcom/facebook/ipc/composer/model/MinutiaeTag;)LX/8LQ;

    move-result-object v4

    iget-object v0, p0, LX/8LV;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 1332681
    iput-wide v6, v4, LX/8LQ;->h:J

    .line 1332682
    move-object v0, v4

    .line 1332683
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 1332684
    iput-object v4, v0, LX/8LQ;->d:LX/0Px;

    .line 1332685
    move-object v0, v0

    .line 1332686
    const-string v4, "own_timeline"

    .line 1332687
    iput-object v4, v0, LX/8LQ;->i:Ljava/lang/String;

    .line 1332688
    move-object v0, v0

    .line 1332689
    const-wide/16 v4, -0x1

    .line 1332690
    iput-wide v4, v0, LX/8LQ;->j:J

    .line 1332691
    move-object v0, v0

    .line 1332692
    iput-boolean v1, v0, LX/8LQ;->m:Z

    .line 1332693
    move-object v0, v0

    .line 1332694
    sget-object v4, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->a:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332695
    iput-object v4, v0, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332696
    move-object v0, v0

    .line 1332697
    sget-object v4, LX/8LR;->MULTIMEDIA:LX/8LR;

    .line 1332698
    iput-object v4, v0, LX/8LQ;->p:LX/8LR;

    .line 1332699
    move-object v0, v0

    .line 1332700
    sget-object v4, LX/8LS;->MULTIMEDIA:LX/8LS;

    .line 1332701
    iput-object v4, v0, LX/8LQ;->q:LX/8LS;

    .line 1332702
    move-object v0, v0

    .line 1332703
    iget-object v4, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 1332704
    iput-wide v4, v0, LX/8LQ;->x:J

    .line 1332705
    move-object v0, v0

    .line 1332706
    iput-boolean v1, v0, LX/8LQ;->z:Z

    .line 1332707
    move-object v0, v0

    .line 1332708
    iput-boolean v1, v0, LX/8LQ;->A:Z

    .line 1332709
    move-object v0, v0

    .line 1332710
    const-string v1, "feed_inline"

    .line 1332711
    iput-object v1, v0, LX/8LQ;->I:Ljava/lang/String;

    .line 1332712
    move-object v0, v0

    .line 1332713
    iget-object v1, p0, LX/8LV;->e:LX/8LT;

    invoke-virtual {v1, v2, v3}, LX/8LT;->a(Lcom/facebook/ipc/media/MediaItem;Landroid/os/Bundle;)I

    move-result v1

    .line 1332714
    iput v1, v0, LX/8LQ;->W:I

    .line 1332715
    move-object v0, v0

    .line 1332716
    invoke-static {v3}, LX/8LT;->a(Landroid/os/Bundle;)I

    move-result v1

    .line 1332717
    iput v1, v0, LX/8LQ;->Y:I

    .line 1332718
    move-object v0, v0

    .line 1332719
    const/16 v1, 0x28

    invoke-virtual {v0, v1}, LX/8LQ;->e(I)LX/8LQ;

    move-result-object v0

    sget-object v1, LX/5Rn;->SAVE_DRAFT:LX/5Rn;

    .line 1332720
    iput-object v1, v0, LX/8LQ;->B:LX/5Rn;

    .line 1332721
    move-object v0, v0

    .line 1332722
    iput-object p1, v0, LX/8LQ;->V:Lcom/facebook/audience/model/UploadShot;

    .line 1332723
    move-object v0, v0

    .line 1332724
    invoke-virtual {v0}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 1332725
    goto/16 :goto_0

    .line 1332726
    :cond_1
    sget-object v2, LX/4gQ;->Video:LX/4gQ;

    goto/16 :goto_1

    .line 1332727
    :cond_2
    new-instance v0, LX/74m;

    invoke-direct {v0}, LX/74m;-><init>()V

    .line 1332728
    iput-object v2, v0, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1332729
    move-object v0, v0

    .line 1332730
    invoke-virtual {v0}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_2

    .line 1332731
    :cond_3
    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto/16 :goto_3
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/ipc/media/MediaItem;FFLjava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 7

    .prologue
    .line 1332846
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1332847
    iget-object v0, p1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1332848
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1332849
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1332850
    const-string v3, "focusX"

    invoke-virtual {v2, v3, p3}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 1332851
    const-string v3, "focusY"

    invoke-virtual {v2, v3, p4}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 1332852
    new-instance v3, LX/8LQ;

    invoke-direct {v3}, LX/8LQ;-><init>()V

    .line 1332853
    iput-object p5, v3, LX/8LQ;->a:Ljava/lang/String;

    .line 1332854
    move-object v3, v3

    .line 1332855
    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 1332856
    iput-object v4, v3, LX/8LQ;->b:LX/0Px;

    .line 1332857
    move-object v3, v3

    .line 1332858
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1332859
    iput-object v2, v3, LX/8LQ;->c:LX/0Px;

    .line 1332860
    move-object v2, v3

    .line 1332861
    const-string v3, ""

    .line 1332862
    iput-object v3, v2, LX/8LQ;->e:Ljava/lang/String;

    .line 1332863
    move-object v2, v2

    .line 1332864
    iput-wide v0, v2, LX/8LQ;->h:J

    .line 1332865
    move-object v0, v2

    .line 1332866
    const-string v1, "cover_photo"

    .line 1332867
    iput-object v1, v0, LX/8LQ;->i:Ljava/lang/String;

    .line 1332868
    move-object v0, v0

    .line 1332869
    const-wide/16 v2, -0x1

    .line 1332870
    iput-wide v2, v0, LX/8LQ;->j:J

    .line 1332871
    move-object v0, v0

    .line 1332872
    sget-object v1, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->a:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332873
    iput-object v1, v0, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332874
    move-object v0, v0

    .line 1332875
    sget-object v1, LX/8LR;->COVER_PHOTO:LX/8LR;

    .line 1332876
    iput-object v1, v0, LX/8LQ;->p:LX/8LR;

    .line 1332877
    move-object v0, v0

    .line 1332878
    sget-object v1, LX/8LS;->COVER_PHOTO:LX/8LS;

    .line 1332879
    iput-object v1, v0, LX/8LQ;->q:LX/8LS;

    .line 1332880
    move-object v0, v0

    .line 1332881
    iget-boolean v1, p1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, v1

    .line 1332882
    if-eqz v1, :cond_0

    .line 1332883
    :goto_0
    iput-object p1, v0, LX/8LQ;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1332884
    move-object v0, v0

    .line 1332885
    iget-object v1, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1332886
    iput-wide v2, v0, LX/8LQ;->x:J

    .line 1332887
    move-object v0, v0

    .line 1332888
    invoke-virtual {v0}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/ipc/media/MediaItem;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 9
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Lcom/facebook/share/model/ComposerAppAttribution;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1332843
    invoke-virtual {p1}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1332844
    const-string v4, "temp_file_to_clean_up"

    invoke-virtual {p2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1332845
    new-instance v4, LX/8LQ;

    invoke-direct {v4}, LX/8LQ;-><init>()V

    invoke-virtual {v4, p4}, LX/8LQ;->a(Ljava/lang/String;)LX/8LQ;

    move-result-object v4

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/8LQ;->a(LX/0Px;)LX/8LQ;

    move-result-object v4

    invoke-static {p3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/8LQ;->b(LX/0Px;)LX/8LQ;

    move-result-object v4

    invoke-virtual {v4, p6}, LX/8LQ;->c(Ljava/lang/String;)LX/8LQ;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, LX/8LQ;->a(J)LX/8LQ;

    move-result-object v2

    const-string v3, "profile_video"

    invoke-virtual {v2, v3}, LX/8LQ;->e(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v4, v5}, LX/8LQ;->b(J)LX/8LQ;

    move-result-object v2

    sget-object v3, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->a:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    invoke-virtual {v2, v3}, LX/8LQ;->a(Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p11

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/share/model/ComposerAppAttribution;)LX/8LQ;

    move-result-object v2

    sget-object v3, LX/8LR;->PROFILE_VIDEO:LX/8LR;

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LR;)LX/8LQ;

    move-result-object v2

    sget-object v3, LX/8LS;->PROFILE_VIDEO:LX/8LS;

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LS;)LX/8LQ;

    move-result-object v2

    const-string v3, "PROFILE_VIDEO"

    invoke-virtual {v2, v3}, LX/8LQ;->h(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    iget-object v3, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, LX/8LQ;->d(J)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p5}, LX/8LQ;->j(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    const/16 v3, 0x28

    invoke-virtual {v2, v3}, LX/8LQ;->e(I)LX/8LQ;

    move-result-object v2

    const/4 v3, -0x2

    invoke-virtual {v2, v3}, LX/8LQ;->c(I)LX/8LQ;

    move-result-object v2

    move-wide/from16 v0, p7

    invoke-virtual {v2, v0, v1}, LX/8LQ;->g(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p10

    invoke-virtual {v2, v0}, LX/8LQ;->l(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p9

    invoke-virtual {v2, v0}, LX/8LQ;->k(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    return-object v2
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/facebook/share/model/ComposerAppAttribution;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 8
    .param p8    # Lcom/facebook/share/model/ComposerAppAttribution;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1332835
    invoke-static/range {p11 .. p11}, LX/5iB;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v2

    .line 1332836
    invoke-virtual {p1}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1332837
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1332838
    const-string v6, "creative_editing_metadata"

    move-object/from16 v0, p11

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1332839
    const-string v6, "temp_file_to_clean_up"

    invoke-virtual {v3, v6, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1332840
    const-string v6, "caption"

    move-object/from16 v0, p12

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1332841
    new-instance v6, LX/74k;

    invoke-direct {v6}, LX/74k;-><init>()V

    invoke-virtual {v6, p2}, LX/74k;->a(Ljava/lang/String;)LX/74k;

    move-result-object v6

    invoke-virtual {v6, p5}, LX/74k;->a(I)LX/74k;

    move-result-object v6

    invoke-virtual {v6}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v6

    .line 1332842
    new-instance v7, LX/8LQ;

    invoke-direct {v7}, LX/8LQ;-><init>()V

    invoke-virtual {v7, p3}, LX/8LQ;->a(Ljava/lang/String;)LX/8LQ;

    move-result-object v7

    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/8LQ;->a(LX/0Px;)LX/8LQ;

    move-result-object v6

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/8LQ;->b(LX/0Px;)LX/8LQ;

    move-result-object v3

    invoke-static/range {p12 .. p12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    :goto_0
    move-object/from16 v0, p12

    invoke-virtual {v3, v0}, LX/8LQ;->c(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-virtual {v3, v0}, LX/8LQ;->a(Lcom/facebook/share/model/ComposerAppAttribution;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, LX/8LQ;->a(J)LX/8LQ;

    move-result-object v3

    const-string v4, "profile_pic"

    invoke-virtual {v3, v4}, LX/8LQ;->e(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    const-wide/16 v4, -0x1

    invoke-virtual {v3, v4, v5}, LX/8LQ;->b(J)LX/8LQ;

    move-result-object v3

    sget-object v4, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->a:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    invoke-virtual {v3, v4}, LX/8LQ;->a(Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;)LX/8LQ;

    move-result-object v3

    sget-object v4, LX/8LR;->PROFILE_PIC:LX/8LR;

    invoke-virtual {v3, v4}, LX/8LQ;->a(LX/8LR;)LX/8LQ;

    move-result-object v3

    sget-object v4, LX/8LS;->PROFILE_PIC:LX/8LS;

    invoke-virtual {v3, v4}, LX/8LQ;->a(LX/8LS;)LX/8LQ;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/auth/viewercontext/ViewerContext;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_1
    invoke-virtual {v3, p1}, LX/8LQ;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/8LQ;

    move-result-object v3

    iget-object v4, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, LX/8LQ;->d(J)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, p4}, LX/8LQ;->j(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, p6, p7}, LX/8LQ;->g(J)LX/8LQ;

    move-result-object v3

    move-object/from16 v0, p9

    invoke-virtual {v3, v0}, LX/8LQ;->k(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    move-object/from16 v0, p10

    invoke-virtual {v3, v0}, LX/8LQ;->l(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/8LQ;->f(Z)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    return-object v2

    :cond_0
    invoke-static/range {p12 .. p12}, LX/8oj;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p12

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;LX/0Px;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 7
    .param p4    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ")",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .prologue
    .line 1332810
    new-instance v0, LX/8LQ;

    invoke-direct {v0}, LX/8LQ;-><init>()V

    .line 1332811
    iput-object p1, v0, LX/8LQ;->P:Lcom/facebook/commerce/publishing/graphql/CommerceProductItemMutateParams;

    .line 1332812
    move-object v0, v0

    .line 1332813
    iput-object p3, v0, LX/8LQ;->a:Ljava/lang/String;

    .line 1332814
    move-object v0, v0

    .line 1332815
    iput-object p4, v0, LX/8LQ;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1332816
    move-object v0, v0

    .line 1332817
    iput-object p2, v0, LX/8LQ;->b:LX/0Px;

    .line 1332818
    move-object v0, v0

    .line 1332819
    sget-object v1, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->d:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332820
    iput-object v1, v0, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332821
    move-object v0, v0

    .line 1332822
    sget-object v1, LX/8LR;->PRODUCT_IMAGE:LX/8LR;

    .line 1332823
    iput-object v1, v0, LX/8LQ;->p:LX/8LR;

    .line 1332824
    move-object v0, v0

    .line 1332825
    sget-object v1, LX/8LS;->PRODUCT_IMAGE:LX/8LS;

    .line 1332826
    iput-object v1, v0, LX/8LQ;->q:LX/8LS;

    .line 1332827
    move-object v0, v0

    .line 1332828
    const-string v1, "product_image"

    .line 1332829
    iput-object v1, v0, LX/8LQ;->i:Ljava/lang/String;

    .line 1332830
    move-object v0, v0

    .line 1332831
    iget-object v1, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1332832
    iput-wide v2, v0, LX/8LQ;->x:J

    .line 1332833
    move-object v0, v0

    .line 1332834
    invoke-virtual {v0}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/ipc/media/MediaItem;Landroid/os/Bundle;JLjava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;JZLjava/lang/String;ZLX/5Rn;Ljava/lang/Long;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 8
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/ipc/composer/model/MinutiaeTag;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p14    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1332805
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1332806
    invoke-static {p1}, LX/74c;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, LX/8LS;->GIF:LX/8LS;

    .line 1332807
    :goto_0
    new-instance v3, LX/8LQ;

    invoke-direct {v3}, LX/8LQ;-><init>()V

    move-object/from16 v0, p15

    invoke-virtual {v3, v0}, LX/8LQ;->a(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/8LQ;->a(LX/0Px;)LX/8LQ;

    move-result-object v4

    if-nez p2, :cond_1

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v4, v3}, LX/8LQ;->b(LX/0Px;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, p5}, LX/8LQ;->c(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, p6}, LX/8LQ;->a(Lcom/facebook/ipc/composer/model/MinutiaeTag;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, p7}, LX/8LQ;->d(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, LX/8LQ;->a(J)LX/8LQ;

    move-result-object v3

    const-string v4, "own_page_timeline"

    invoke-virtual {v3, v4}, LX/8LQ;->e(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    move-wide/from16 v0, p8

    invoke-virtual {v3, v0, v1}, LX/8LQ;->b(J)LX/8LQ;

    move-result-object v3

    move/from16 v0, p10

    invoke-virtual {v3, v0}, LX/8LQ;->e(Z)LX/8LQ;

    move-result-object v3

    move-object/from16 v0, p11

    invoke-virtual {v3, v0}, LX/8LQ;->f(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    move/from16 v0, p12

    invoke-virtual {v3, v0}, LX/8LQ;->a(Z)LX/8LQ;

    move-result-object v3

    move-object/from16 v0, p13

    invoke-virtual {v3, v0}, LX/8LQ;->a(LX/5Rn;)LX/8LQ;

    move-result-object v3

    if-eqz p14, :cond_2

    invoke-virtual/range {p14 .. p14}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    :goto_2
    invoke-virtual {v3, v4, v5}, LX/8LQ;->e(J)LX/8LQ;

    move-result-object v3

    sget-object v4, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->d:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    invoke-virtual {v3, v4}, LX/8LQ;->a(Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;)LX/8LQ;

    move-result-object v3

    sget-object v4, LX/8LR;->VIDEO_STATUS:LX/8LR;

    invoke-virtual {v3, v4}, LX/8LQ;->a(LX/8LR;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/8LQ;->a(LX/8LS;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p16

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/8LQ;

    move-result-object v2

    iget-object v3, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, LX/8LQ;->d(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p17

    invoke-virtual {v2, v0}, LX/8LQ;->i(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    const/16 v3, 0x28

    invoke-virtual {v2, v3}, LX/8LQ;->e(I)LX/8LQ;

    move-result-object v2

    iget-object v3, p0, LX/8LV;->e:LX/8LT;

    invoke-virtual {v3, p1, p2}, LX/8LT;->a(Lcom/facebook/ipc/media/MediaItem;Landroid/os/Bundle;)I

    move-result v3

    invoke-virtual {v2, v3}, LX/8LQ;->c(I)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p18

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p19

    invoke-virtual {v2, v0}, LX/8LQ;->g(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    return-object v2

    .line 1332808
    :cond_0
    sget-object v2, LX/8LS;->VIDEO:LX/8LS;

    goto/16 :goto_0

    .line 1332809
    :cond_1
    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    goto/16 :goto_1

    :cond_2
    const-wide/16 v4, 0x0

    goto :goto_2
.end method

.method public final a(Lcom/facebook/ipc/media/MediaItem;Ljava/lang/String;JLcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 8

    .prologue
    .line 1332768
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1332769
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1332770
    iget-object v0, p0, LX/8LV;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1332771
    cmp-long v2, v0, p3

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-gtz v2, :cond_1

    :cond_0
    move-wide p3, v0

    .line 1332772
    :cond_1
    new-instance v0, LX/8LQ;

    invoke-direct {v0}, LX/8LQ;-><init>()V

    .line 1332773
    iput-object p2, v0, LX/8LQ;->a:Ljava/lang/String;

    .line 1332774
    move-object v1, v0

    .line 1332775
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1332776
    iput-object v2, v1, LX/8LQ;->b:LX/0Px;

    .line 1332777
    move-object v1, v1

    .line 1332778
    sget-object v2, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->b:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332779
    iput-object v2, v1, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332780
    move-object v1, v1

    .line 1332781
    iput-wide p3, v1, LX/8LQ;->h:J

    .line 1332782
    move-object v1, v1

    .line 1332783
    const-string v2, "own_timeline"

    .line 1332784
    iput-object v2, v1, LX/8LQ;->i:Ljava/lang/String;

    .line 1332785
    move-object v1, v1

    .line 1332786
    sget-object v2, LX/8LR;->VIDEO_TARGET:LX/8LR;

    .line 1332787
    iput-object v2, v1, LX/8LQ;->p:LX/8LR;

    .line 1332788
    move-object v1, v1

    .line 1332789
    sget-object v2, LX/8LS;->VIDEO:LX/8LS;

    .line 1332790
    iput-object v2, v1, LX/8LQ;->q:LX/8LS;

    .line 1332791
    move-object v1, v1

    .line 1332792
    iget-object v2, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1332793
    iput-wide v2, v1, LX/8LQ;->x:J

    .line 1332794
    move-object v1, v1

    .line 1332795
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1332796
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, v1, LX/8LQ;->H:Z

    .line 1332797
    move-object v1, v1

    .line 1332798
    const/16 v2, 0x28

    invoke-virtual {v1, v2}, LX/8LQ;->e(I)LX/8LQ;

    move-result-object v1

    iget-object v2, p0, LX/8LV;->e:LX/8LT;

    invoke-virtual {v2}, LX/8LT;->a()I

    move-result v2

    .line 1332799
    iput v2, v1, LX/8LQ;->W:I

    .line 1332800
    if-eqz p5, :cond_2

    .line 1332801
    iput-object p5, v0, LX/8LQ;->s:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1332802
    sget-object v1, LX/8LR;->VIDEO_STATUS:LX/8LR;

    .line 1332803
    iput-object v1, v0, LX/8LQ;->p:LX/8LR;

    .line 1332804
    :cond_2
    invoke-virtual {v0}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/photos/base/media/VideoItem;Landroid/os/Bundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;LX/0Px;JZLjava/lang/String;ZLjava/lang/String;Lcom/facebook/ipc/composer/model/ProductItemAttachment;JLjava/lang/String;Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Z)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 9
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p13    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p16    # Lcom/facebook/ipc/composer/model/ProductItemAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/base/media/VideoItem;",
            "Landroid/os/Bundle;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/model/MinutiaeTag;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;JZ",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/model/ProductItemAttachment;",
            "J",
            "Ljava/lang/String;",
            "Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;",
            "Z)",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .prologue
    .line 1332760
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1332761
    invoke-static/range {p9 .. p9}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1332762
    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-lez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    const-string v3, "Invalid targetId %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1332763
    invoke-static {p1}, LX/74c;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, LX/8LS;->GIF:LX/8LS;

    .line 1332764
    :goto_1
    new-instance v3, LX/8LQ;

    invoke-direct {v3}, LX/8LQ;-><init>()V

    move-object/from16 v0, p15

    invoke-virtual {v3, v0}, LX/8LQ;->a(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/8LQ;->a(LX/0Px;)LX/8LQ;

    move-result-object v4

    if-nez p2, :cond_3

    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v4, v3}, LX/8LQ;->b(LX/0Px;)LX/8LQ;

    move-result-object v3

    move-object/from16 v0, p9

    invoke-virtual {v3, v0}, LX/8LQ;->c(LX/0Px;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, p6}, LX/8LQ;->c(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-virtual {v3, v0}, LX/8LQ;->a(Lcom/facebook/ipc/composer/model/MinutiaeTag;)LX/8LQ;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-virtual {v3, v0}, LX/8LQ;->d(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, LX/8LQ;->a(J)LX/8LQ;

    move-result-object v3

    invoke-static {p5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string p5, "targeted"

    :cond_0
    invoke-virtual {v3, p5}, LX/8LQ;->e(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    move-wide/from16 v0, p10

    invoke-virtual {v3, v0, v1}, LX/8LQ;->b(J)LX/8LQ;

    move-result-object v3

    move/from16 v0, p12

    invoke-virtual {v3, v0}, LX/8LQ;->e(Z)LX/8LQ;

    move-result-object v3

    move-object/from16 v0, p13

    invoke-virtual {v3, v0}, LX/8LQ;->f(Ljava/lang/String;)LX/8LQ;

    move-result-object v3

    move/from16 v0, p14

    invoke-virtual {v3, v0}, LX/8LQ;->a(Z)LX/8LQ;

    move-result-object v3

    sget-object v4, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->a:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    invoke-virtual {v3, v4}, LX/8LQ;->a(Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;)LX/8LQ;

    move-result-object v3

    sget-object v4, LX/8LR;->VIDEO_TARGET:LX/8LR;

    invoke-virtual {v3, v4}, LX/8LQ;->a(LX/8LR;)LX/8LQ;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/8LQ;->a(LX/8LS;)LX/8LQ;

    move-result-object v2

    iget-object v3, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, LX/8LQ;->d(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p16

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)LX/8LQ;

    move-result-object v2

    move-wide/from16 v0, p17

    invoke-virtual {v2, v0, v1}, LX/8LQ;->f(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p19

    invoke-virtual {v2, v0}, LX/8LQ;->i(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    const/16 v3, 0x28

    invoke-virtual {v2, v3}, LX/8LQ;->e(I)LX/8LQ;

    move-result-object v2

    iget-object v3, p0, LX/8LV;->e:LX/8LT;

    invoke-virtual {v3}, LX/8LT;->a()I

    move-result v3

    invoke-virtual {v2, v3}, LX/8LQ;->c(I)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p20

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)LX/8LQ;

    move-result-object v2

    move/from16 v0, p21

    invoke-virtual {v2, v0}, LX/8LQ;->i(Z)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    return-object v2

    .line 1332765
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1332766
    :cond_2
    sget-object v2, LX/8LS;->VIDEO:LX/8LS;

    goto/16 :goto_1

    .line 1332767
    :cond_3
    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    goto/16 :goto_2
.end method

.method public final a(Lcom/facebook/photos/base/media/VideoItem;Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;LX/0Px;JLjava/lang/String;ZLjava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;ZZLjava/lang/String;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Lcom/facebook/productionprompts/logging/PromptAnalytics;LX/0Px;Lcom/facebook/audience/model/UploadShot;Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 8
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p20    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p21    # Lcom/facebook/audience/model/UploadShot;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/base/media/VideoItem;",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/composer/model/MinutiaeTag;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;J",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/facebook/share/model/ComposerAppAttribution;",
            "ZZ",
            "Ljava/lang/String;",
            "Z",
            "Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;",
            "Lcom/facebook/productionprompts/logging/PromptAnalytics;",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;",
            "Lcom/facebook/audience/model/UploadShot;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/photos/upload/operation/UploadOperation;"
        }
    .end annotation

    .prologue
    .line 1332750
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1332751
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1332752
    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1332753
    const/4 v2, 0x0

    .line 1332754
    if-eqz p2, :cond_0

    .line 1332755
    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1332756
    :cond_0
    invoke-static {p1}, LX/74c;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, LX/8LS;->GIF:LX/8LS;

    .line 1332757
    :goto_0
    new-instance v4, LX/8LQ;

    invoke-direct {v4}, LX/8LQ;-><init>()V

    move-object/from16 v0, p12

    invoke-virtual {v4, v0}, LX/8LQ;->a(Ljava/lang/String;)LX/8LQ;

    move-result-object v4

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/8LQ;->a(LX/0Px;)LX/8LQ;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/8LQ;->b(LX/0Px;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p7}, LX/8LQ;->c(LX/0Px;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/8LQ;->c(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p4}, LX/8LQ;->a(Lcom/facebook/ipc/composer/model/MinutiaeTag;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p5}, LX/8LQ;->d(Ljava/lang/String;)LX/8LQ;

    move-result-object v4

    iget-object v2, p0, LX/8LV;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, LX/8LQ;->a(J)LX/8LQ;

    move-result-object v2

    const-string v4, "own_timeline"

    invoke-virtual {v2, v4}, LX/8LQ;->e(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move-wide/from16 v0, p8

    invoke-virtual {v2, v0, v1}, LX/8LQ;->b(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p10

    invoke-virtual {v2, v0}, LX/8LQ;->f(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    move/from16 v0, p11

    invoke-virtual {v2, v0}, LX/8LQ;->a(Z)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, p6}, LX/8LQ;->a(Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;)LX/8LQ;

    move-result-object v4

    if-eqz p21, :cond_2

    sget-object v2, LX/8LR;->MULTIMEDIA:LX/8LR;

    :goto_1
    invoke-virtual {v4, v2}, LX/8LQ;->a(LX/8LR;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/8LQ;->a(LX/8LS;)LX/8LQ;

    move-result-object v2

    iget-object v3, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, LX/8LQ;->d(J)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p13

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/share/model/ComposerAppAttribution;)LX/8LQ;

    move-result-object v2

    move/from16 v0, p14

    invoke-virtual {v2, v0}, LX/8LQ;->d(Z)LX/8LQ;

    move-result-object v2

    move/from16 v0, p15

    invoke-virtual {v2, v0}, LX/8LQ;->e(Z)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p16

    invoke-virtual {v2, v0}, LX/8LQ;->i(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    const/16 v3, 0x28

    invoke-virtual {v2, v3}, LX/8LQ;->e(I)LX/8LQ;

    move-result-object v2

    iget-object v3, p0, LX/8LV;->e:LX/8LT;

    invoke-virtual {v3, p1, p2}, LX/8LT;->a(Lcom/facebook/ipc/media/MediaItem;Landroid/os/Bundle;)I

    move-result v3

    invoke-virtual {v2, v3}, LX/8LQ;->c(I)LX/8LQ;

    move-result-object v2

    invoke-static {p2}, LX/8LT;->a(Landroid/os/Bundle;)I

    move-result v3

    invoke-virtual {v2, v3}, LX/8LQ;->d(I)LX/8LQ;

    move-result-object v2

    move/from16 v0, p17

    invoke-virtual {v2, v0}, LX/8LQ;->g(Z)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p18

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p19

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/productionprompts/logging/PromptAnalytics;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p20

    invoke-virtual {v2, v0}, LX/8LQ;->d(LX/0Px;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p21

    invoke-virtual {v2, v0}, LX/8LQ;->a(Lcom/facebook/audience/model/UploadShot;)LX/8LQ;

    move-result-object v2

    move-object/from16 v0, p22

    invoke-virtual {v2, v0}, LX/8LQ;->g(Ljava/lang/String;)LX/8LQ;

    move-result-object v2

    invoke-virtual {v2}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    return-object v2

    .line 1332758
    :cond_1
    sget-object v3, LX/8LS;->VIDEO:LX/8LS;

    goto/16 :goto_0

    .line 1332759
    :cond_2
    sget-object v2, LX/8LR;->VIDEO_TARGET:LX/8LR;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;LX/8LS;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 7

    .prologue
    .line 1332732
    new-instance v0, LX/8LQ;

    invoke-direct {v0}, LX/8LQ;-><init>()V

    .line 1332733
    iput-object p1, v0, LX/8LQ;->a:Ljava/lang/String;

    .line 1332734
    move-object v0, v0

    .line 1332735
    const-string v1, "cancel"

    .line 1332736
    iput-object v1, v0, LX/8LQ;->i:Ljava/lang/String;

    .line 1332737
    move-object v0, v0

    .line 1332738
    sget-object v1, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->a:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332739
    iput-object v1, v0, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1332740
    move-object v0, v0

    .line 1332741
    sget-object v1, LX/8LR;->TARGET:LX/8LR;

    .line 1332742
    iput-object v1, v0, LX/8LQ;->p:LX/8LR;

    .line 1332743
    move-object v0, v0

    .line 1332744
    iput-object p2, v0, LX/8LQ;->q:LX/8LS;

    .line 1332745
    move-object v0, v0

    .line 1332746
    iget-object v1, p0, LX/8LV;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1332747
    iput-wide v2, v0, LX/8LQ;->x:J

    .line 1332748
    move-object v0, v0

    .line 1332749
    invoke-virtual {v0}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    return-object v0
.end method
