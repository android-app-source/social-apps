.class public LX/6s5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1152902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152903
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1152904
    const-string v0, "payment_item_type"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152905
    const-string v0, "receiver_id"

    invoke-virtual {p2, v0}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152906
    const-string v0, "payment_item_type"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6xg;->forValue(Ljava/lang/String;)LX/6xg;

    move-result-object v0

    const-string v1, "receiver_id"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 1152907
    new-instance p0, LX/6rb;

    invoke-direct {p0, v0, v1}, LX/6rb;-><init>(LX/6xg;Ljava/lang/String;)V

    move-object v0, p0

    .line 1152908
    const-string v1, "order_id"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 1152909
    iput-object v1, v0, LX/6rb;->c:Ljava/lang/String;

    .line 1152910
    move-object v0, v0

    .line 1152911
    const-string v1, "extra_data"

    invoke-static {p2, v1}, LX/16N;->e(LX/0lF;Ljava/lang/String;)LX/0m9;

    move-result-object v1

    .line 1152912
    iput-object v1, v0, LX/6rb;->d:LX/0m9;

    .line 1152913
    move-object v0, v0

    .line 1152914
    new-instance v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;

    invoke-direct {v1, v0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;-><init>(LX/6rb;)V

    move-object v0, v1

    .line 1152915
    return-object v0
.end method
