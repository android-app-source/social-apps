.class public final LX/8TN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Landroid/net/Uri;

.field public i:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1348514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1348515
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;->HIGHEST_BEST:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    iput-object v0, p0, LX/8TN;->i:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    .line 1348516
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;)LX/8TN;
    .locals 0

    .prologue
    .line 1348517
    if-eqz p1, :cond_0

    .line 1348518
    iput-object p1, p0, LX/8TN;->i:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlayScoreStrategy;

    .line 1348519
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/8TN;
    .locals 0

    .prologue
    .line 1348520
    iput-object p1, p0, LX/8TN;->a:Ljava/lang/String;

    .line 1348521
    return-object p0
.end method

.method public final a()LX/8TO;
    .locals 2

    .prologue
    .line 1348522
    new-instance v0, LX/8TO;

    invoke-direct {v0, p0}, LX/8TO;-><init>(LX/8TN;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/8TN;
    .locals 4

    .prologue
    .line 1348523
    const-string v0, ""

    iput-object v0, p0, LX/8TN;->b:Ljava/lang/String;

    .line 1348524
    if-eqz p1, :cond_0

    .line 1348525
    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {p1, v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8TN;->b:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1348526
    :cond_0
    :goto_0
    iput-object p1, p0, LX/8TN;->b:Ljava/lang/String;

    .line 1348527
    return-object p0

    .line 1348528
    :catch_0
    move-exception v0

    .line 1348529
    sget-object v1, LX/8TO;->a:Ljava/lang/String;

    const-string v2, "Invalid encoding given to URLDecoder."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)LX/8TN;
    .locals 0

    .prologue
    .line 1348530
    iput-object p1, p0, LX/8TN;->c:Ljava/lang/String;

    .line 1348531
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/8TN;
    .locals 1

    .prologue
    .line 1348532
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    move-result-object v0

    iput-object v0, p0, LX/8TN;->d:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    .line 1348533
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/8TN;
    .locals 4

    .prologue
    .line 1348534
    const-string v0, ""

    iput-object v0, p0, LX/8TN;->e:Ljava/lang/String;

    .line 1348535
    if-eqz p1, :cond_0

    .line 1348536
    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {p1, v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8TN;->e:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1348537
    :cond_0
    :goto_0
    return-object p0

    .line 1348538
    :catch_0
    move-exception v0

    .line 1348539
    sget-object v1, LX/8TO;->a:Ljava/lang/String;

    const-string v2, "Invalid encoding given to URLDecoder."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)LX/8TN;
    .locals 4

    .prologue
    .line 1348540
    const-string v0, ""

    iput-object v0, p0, LX/8TN;->f:Ljava/lang/String;

    .line 1348541
    if-eqz p1, :cond_0

    .line 1348542
    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {p1, v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/8TN;->f:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1348543
    :cond_0
    :goto_0
    return-object p0

    .line 1348544
    :catch_0
    move-exception v0

    .line 1348545
    sget-object v1, LX/8TO;->a:Ljava/lang/String;

    const-string v2, "Invalid encoding given to URLDecoder."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)LX/8TN;
    .locals 0

    .prologue
    .line 1348546
    iput-object p1, p0, LX/8TN;->g:Ljava/lang/String;

    .line 1348547
    return-object p0
.end method

.method public final h(Ljava/lang/String;)LX/8TN;
    .locals 4

    .prologue
    .line 1348548
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1348549
    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {p1, v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/8TN;->h:Landroid/net/Uri;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1348550
    :cond_0
    :goto_0
    return-object p0

    .line 1348551
    :catch_0
    move-exception v0

    .line 1348552
    sget-object v1, LX/8TO;->a:Ljava/lang/String;

    const-string v2, "Invalid encoding given to URLDecoder."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
