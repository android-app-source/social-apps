.class public final enum LX/7Si;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Si;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Si;

.field public static final enum BACK:LX/7Si;

.field public static final enum FRONT:LX/7Si;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1209105
    new-instance v0, LX/7Si;

    const-string v1, "FRONT"

    invoke-direct {v0, v1, v2}, LX/7Si;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Si;->FRONT:LX/7Si;

    .line 1209106
    new-instance v0, LX/7Si;

    const-string v1, "BACK"

    invoke-direct {v0, v1, v3}, LX/7Si;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Si;->BACK:LX/7Si;

    .line 1209107
    const/4 v0, 0x2

    new-array v0, v0, [LX/7Si;

    sget-object v1, LX/7Si;->FRONT:LX/7Si;

    aput-object v1, v0, v2

    sget-object v1, LX/7Si;->BACK:LX/7Si;

    aput-object v1, v0, v3

    sput-object v0, LX/7Si;->$VALUES:[LX/7Si;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1209108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Si;
    .locals 1

    .prologue
    .line 1209109
    const-class v0, LX/7Si;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Si;

    return-object v0
.end method

.method public static values()[LX/7Si;
    .locals 1

    .prologue
    .line 1209110
    sget-object v0, LX/7Si;->$VALUES:[LX/7Si;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Si;

    return-object v0
.end method
