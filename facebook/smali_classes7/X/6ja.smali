.class public LX/6ja;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;

.field private static final k:LX/1sw;

.field private static final l:LX/1sw;

.field private static final m:LX/1sw;


# instance fields
.field public final androidPackageName:Ljava/lang/String;

.field public final attributionAppIconURI:Ljava/lang/String;

.field public final attributionAppId:Ljava/lang/Long;

.field public final attributionAppName:Ljava/lang/String;

.field public final attributionMetadata:Ljava/lang/String;

.field public final attributionType:Ljava/lang/Long;

.field public final customReplyAction:Ljava/lang/String;

.field public final iOSStoreId:Ljava/lang/Long;

.field public final otherUserAppScopedFbIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final replyActionType:Ljava/lang/Integer;

.field public final visibility:LX/6jU;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x1

    const/16 v5, 0xa

    const/16 v4, 0xb

    .line 1131463
    new-instance v0, LX/1sv;

    const-string v1, "AttachmentAppAttribution"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6ja;->b:LX/1sv;

    .line 1131464
    new-instance v0, LX/1sw;

    const-string v1, "attributionAppId"

    invoke-direct {v0, v1, v5, v6}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ja;->c:LX/1sw;

    .line 1131465
    new-instance v0, LX/1sw;

    const-string v1, "attributionMetadata"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ja;->d:LX/1sw;

    .line 1131466
    new-instance v0, LX/1sw;

    const-string v1, "attributionAppName"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ja;->e:LX/1sw;

    .line 1131467
    new-instance v0, LX/1sw;

    const-string v1, "attributionAppIconURI"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ja;->f:LX/1sw;

    .line 1131468
    new-instance v0, LX/1sw;

    const-string v1, "androidPackageName"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ja;->g:LX/1sw;

    .line 1131469
    new-instance v0, LX/1sw;

    const-string v1, "iOSStoreId"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ja;->h:LX/1sw;

    .line 1131470
    new-instance v0, LX/1sw;

    const-string v1, "otherUserAppScopedFbIds"

    const/16 v2, 0xd

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ja;->i:LX/1sw;

    .line 1131471
    new-instance v0, LX/1sw;

    const-string v1, "visibility"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v7}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ja;->j:LX/1sw;

    .line 1131472
    new-instance v0, LX/1sw;

    const-string v1, "replyActionType"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v7, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ja;->k:LX/1sw;

    .line 1131473
    new-instance v0, LX/1sw;

    const-string v1, "customReplyAction"

    invoke-direct {v0, v1, v4, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ja;->l:LX/1sw;

    .line 1131474
    new-instance v0, LX/1sw;

    const-string v1, "attributionType"

    invoke-direct {v0, v1, v5, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6ja;->m:LX/1sw;

    .line 1131475
    sput-boolean v6, LX/6ja;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/util/Map;LX/6jU;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;",
            "LX/6jU;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1131450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1131451
    iput-object p1, p0, LX/6ja;->attributionAppId:Ljava/lang/Long;

    .line 1131452
    iput-object p2, p0, LX/6ja;->attributionMetadata:Ljava/lang/String;

    .line 1131453
    iput-object p3, p0, LX/6ja;->attributionAppName:Ljava/lang/String;

    .line 1131454
    iput-object p4, p0, LX/6ja;->attributionAppIconURI:Ljava/lang/String;

    .line 1131455
    iput-object p5, p0, LX/6ja;->androidPackageName:Ljava/lang/String;

    .line 1131456
    iput-object p6, p0, LX/6ja;->iOSStoreId:Ljava/lang/Long;

    .line 1131457
    iput-object p7, p0, LX/6ja;->otherUserAppScopedFbIds:Ljava/util/Map;

    .line 1131458
    iput-object p8, p0, LX/6ja;->visibility:LX/6jU;

    .line 1131459
    iput-object p9, p0, LX/6ja;->replyActionType:Ljava/lang/Integer;

    .line 1131460
    iput-object p10, p0, LX/6ja;->customReplyAction:Ljava/lang/String;

    .line 1131461
    iput-object p11, p0, LX/6ja;->attributionType:Ljava/lang/Long;

    .line 1131462
    return-void
.end method

.method private static a(LX/6ja;)V
    .locals 4

    .prologue
    .line 1131445
    iget-object v0, p0, LX/6ja;->attributionAppId:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 1131446
    new-instance v0, LX/7H4;

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required field \'attributionAppId\' was not present! Struct: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/6ja;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 1131447
    :cond_0
    iget-object v0, p0, LX/6ja;->replyActionType:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    sget-object v0, LX/6jc;->a:LX/1sn;

    iget-object v1, p0, LX/6ja;->replyActionType:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1131448
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'replyActionType\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6ja;->replyActionType:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1131449
    :cond_1
    return-void
.end method

.method public static b(LX/1su;)LX/6ja;
    .locals 18

    .prologue
    .line 1131384
    const/4 v3, 0x0

    .line 1131385
    const/4 v4, 0x0

    .line 1131386
    const/4 v5, 0x0

    .line 1131387
    const/4 v6, 0x0

    .line 1131388
    const/4 v7, 0x0

    .line 1131389
    const/4 v8, 0x0

    .line 1131390
    const/4 v9, 0x0

    .line 1131391
    const/4 v10, 0x0

    .line 1131392
    const/4 v11, 0x0

    .line 1131393
    const/4 v12, 0x0

    .line 1131394
    const/4 v13, 0x0

    .line 1131395
    invoke-virtual/range {p0 .. p0}, LX/1su;->r()LX/1sv;

    .line 1131396
    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 1131397
    iget-byte v14, v2, LX/1sw;->b:B

    if-eqz v14, :cond_d

    .line 1131398
    iget-short v14, v2, LX/1sw;->c:S

    packed-switch v14, :pswitch_data_0

    .line 1131399
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1131400
    :pswitch_0
    iget-byte v14, v2, LX/1sw;->b:B

    const/16 v15, 0xa

    if-ne v14, v15, :cond_1

    .line 1131401
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    .line 1131402
    :cond_1
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1131403
    :pswitch_1
    iget-byte v14, v2, LX/1sw;->b:B

    const/16 v15, 0xb

    if-ne v14, v15, :cond_2

    .line 1131404
    invoke-virtual/range {p0 .. p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 1131405
    :cond_2
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1131406
    :pswitch_2
    iget-byte v14, v2, LX/1sw;->b:B

    const/16 v15, 0xb

    if-ne v14, v15, :cond_3

    .line 1131407
    invoke-virtual/range {p0 .. p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 1131408
    :cond_3
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1131409
    :pswitch_3
    iget-byte v14, v2, LX/1sw;->b:B

    const/16 v15, 0xb

    if-ne v14, v15, :cond_4

    .line 1131410
    invoke-virtual/range {p0 .. p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 1131411
    :cond_4
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1131412
    :pswitch_4
    iget-byte v14, v2, LX/1sw;->b:B

    const/16 v15, 0xb

    if-ne v14, v15, :cond_5

    .line 1131413
    invoke-virtual/range {p0 .. p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 1131414
    :cond_5
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 1131415
    :pswitch_5
    iget-byte v14, v2, LX/1sw;->b:B

    const/16 v15, 0xa

    if-ne v14, v15, :cond_6

    .line 1131416
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    goto/16 :goto_0

    .line 1131417
    :cond_6
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1131418
    :pswitch_6
    iget-byte v14, v2, LX/1sw;->b:B

    const/16 v15, 0xd

    if-ne v14, v15, :cond_8

    .line 1131419
    invoke-virtual/range {p0 .. p0}, LX/1su;->g()LX/7H3;

    move-result-object v14

    .line 1131420
    new-instance v9, Ljava/util/HashMap;

    const/4 v2, 0x0

    iget v15, v14, LX/7H3;->c:I

    mul-int/lit8 v15, v15, 0x2

    invoke-static {v2, v15}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-direct {v9, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 1131421
    const/4 v2, 0x0

    .line 1131422
    :goto_1
    iget v15, v14, LX/7H3;->c:I

    if-gez v15, :cond_7

    invoke-static {}, LX/1su;->s()Z

    move-result v15

    if-eqz v15, :cond_0

    .line 1131423
    :goto_2
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 1131424
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 1131425
    move-object/from16 v0, v16

    invoke-interface {v9, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1131426
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1131427
    :cond_7
    iget v15, v14, LX/7H3;->c:I

    if-ge v2, v15, :cond_0

    goto :goto_2

    .line 1131428
    :cond_8
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1131429
    :pswitch_7
    iget-byte v14, v2, LX/1sw;->b:B

    const/16 v15, 0xc

    if-ne v14, v15, :cond_9

    .line 1131430
    invoke-static/range {p0 .. p0}, LX/6jU;->b(LX/1su;)LX/6jU;

    move-result-object v10

    goto/16 :goto_0

    .line 1131431
    :cond_9
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1131432
    :pswitch_8
    iget-byte v14, v2, LX/1sw;->b:B

    const/16 v15, 0x8

    if-ne v14, v15, :cond_a

    .line 1131433
    invoke-virtual/range {p0 .. p0}, LX/1su;->m()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    goto/16 :goto_0

    .line 1131434
    :cond_a
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1131435
    :pswitch_9
    iget-byte v14, v2, LX/1sw;->b:B

    const/16 v15, 0xb

    if-ne v14, v15, :cond_b

    .line 1131436
    invoke-virtual/range {p0 .. p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_0

    .line 1131437
    :cond_b
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1131438
    :pswitch_a
    iget-byte v14, v2, LX/1sw;->b:B

    const/16 v15, 0xa

    if-ne v14, v15, :cond_c

    .line 1131439
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    goto/16 :goto_0

    .line 1131440
    :cond_c
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 1131441
    :cond_d
    invoke-virtual/range {p0 .. p0}, LX/1su;->e()V

    .line 1131442
    new-instance v2, LX/6ja;

    invoke-direct/range {v2 .. v13}, LX/6ja;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/util/Map;LX/6jU;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1131443
    invoke-static {v2}, LX/6ja;->a(LX/6ja;)V

    .line 1131444
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1131476
    if-eqz p2, :cond_a

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1131477
    :goto_0
    if-eqz p2, :cond_b

    const-string v0, "\n"

    move-object v2, v0

    .line 1131478
    :goto_1
    if-eqz p2, :cond_c

    const-string v0, " "

    move-object v1, v0

    .line 1131479
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "AttachmentAppAttribution"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1131480
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131481
    const-string v0, "("

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131482
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131483
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131484
    const-string v0, "attributionAppId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131485
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131486
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131487
    iget-object v0, p0, LX/6ja;->attributionAppId:Ljava/lang/Long;

    if-nez v0, :cond_d

    .line 1131488
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131489
    :goto_3
    iget-object v0, p0, LX/6ja;->attributionMetadata:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1131490
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131491
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131492
    const-string v0, "attributionMetadata"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131493
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131494
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131495
    iget-object v0, p0, LX/6ja;->attributionMetadata:Ljava/lang/String;

    if-nez v0, :cond_e

    .line 1131496
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131497
    :cond_0
    :goto_4
    iget-object v0, p0, LX/6ja;->attributionAppName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1131498
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131499
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131500
    const-string v0, "attributionAppName"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131501
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131502
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131503
    iget-object v0, p0, LX/6ja;->attributionAppName:Ljava/lang/String;

    if-nez v0, :cond_f

    .line 1131504
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131505
    :cond_1
    :goto_5
    iget-object v0, p0, LX/6ja;->attributionAppIconURI:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1131506
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131507
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131508
    const-string v0, "attributionAppIconURI"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131509
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131510
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131511
    iget-object v0, p0, LX/6ja;->attributionAppIconURI:Ljava/lang/String;

    if-nez v0, :cond_10

    .line 1131512
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131513
    :cond_2
    :goto_6
    iget-object v0, p0, LX/6ja;->androidPackageName:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1131514
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131515
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131516
    const-string v0, "androidPackageName"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131517
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131518
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131519
    iget-object v0, p0, LX/6ja;->androidPackageName:Ljava/lang/String;

    if-nez v0, :cond_11

    .line 1131520
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131521
    :cond_3
    :goto_7
    iget-object v0, p0, LX/6ja;->iOSStoreId:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 1131522
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131523
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131524
    const-string v0, "iOSStoreId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131525
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131526
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131527
    iget-object v0, p0, LX/6ja;->iOSStoreId:Ljava/lang/Long;

    if-nez v0, :cond_12

    .line 1131528
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131529
    :cond_4
    :goto_8
    iget-object v0, p0, LX/6ja;->otherUserAppScopedFbIds:Ljava/util/Map;

    if-eqz v0, :cond_5

    .line 1131530
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131531
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131532
    const-string v0, "otherUserAppScopedFbIds"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131533
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131534
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131535
    iget-object v0, p0, LX/6ja;->otherUserAppScopedFbIds:Ljava/util/Map;

    if-nez v0, :cond_13

    .line 1131536
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131537
    :cond_5
    :goto_9
    iget-object v0, p0, LX/6ja;->visibility:LX/6jU;

    if-eqz v0, :cond_6

    .line 1131538
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131539
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131540
    const-string v0, "visibility"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131541
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131542
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131543
    iget-object v0, p0, LX/6ja;->visibility:LX/6jU;

    if-nez v0, :cond_14

    .line 1131544
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131545
    :cond_6
    :goto_a
    iget-object v0, p0, LX/6ja;->replyActionType:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 1131546
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131547
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131548
    const-string v0, "replyActionType"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131549
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131550
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131551
    iget-object v0, p0, LX/6ja;->replyActionType:Ljava/lang/Integer;

    if-nez v0, :cond_15

    .line 1131552
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131553
    :cond_7
    :goto_b
    iget-object v0, p0, LX/6ja;->customReplyAction:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1131554
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131555
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131556
    const-string v0, "customReplyAction"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131557
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131558
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131559
    iget-object v0, p0, LX/6ja;->customReplyAction:Ljava/lang/String;

    if-nez v0, :cond_17

    .line 1131560
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131561
    :cond_8
    :goto_c
    iget-object v0, p0, LX/6ja;->attributionType:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 1131562
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131563
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131564
    const-string v0, "attributionType"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131565
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131566
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131567
    iget-object v0, p0, LX/6ja;->attributionType:Ljava/lang/Long;

    if-nez v0, :cond_18

    .line 1131568
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131569
    :cond_9
    :goto_d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131570
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131571
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1131572
    :cond_a
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1131573
    :cond_b
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1131574
    :cond_c
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 1131575
    :cond_d
    iget-object v0, p0, LX/6ja;->attributionAppId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1131576
    :cond_e
    iget-object v0, p0, LX/6ja;->attributionMetadata:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1131577
    :cond_f
    iget-object v0, p0, LX/6ja;->attributionAppName:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1131578
    :cond_10
    iget-object v0, p0, LX/6ja;->attributionAppIconURI:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 1131579
    :cond_11
    iget-object v0, p0, LX/6ja;->androidPackageName:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 1131580
    :cond_12
    iget-object v0, p0, LX/6ja;->iOSStoreId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 1131581
    :cond_13
    iget-object v0, p0, LX/6ja;->otherUserAppScopedFbIds:Ljava/util/Map;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 1131582
    :cond_14
    iget-object v0, p0, LX/6ja;->visibility:LX/6jU;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 1131583
    :cond_15
    sget-object v0, LX/6jc;->b:Ljava/util/Map;

    iget-object v5, p0, LX/6ja;->replyActionType:Ljava/lang/Integer;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1131584
    if-eqz v0, :cond_16

    .line 1131585
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131586
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131587
    :cond_16
    iget-object v5, p0, LX/6ja;->replyActionType:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1131588
    if-eqz v0, :cond_7

    .line 1131589
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    .line 1131590
    :cond_17
    iget-object v0, p0, LX/6ja;->customReplyAction:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    .line 1131591
    :cond_18
    iget-object v0, p0, LX/6ja;->attributionType:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_d
.end method

.method public final a(LX/1su;)V
    .locals 6

    .prologue
    const/16 v2, 0xa

    .line 1131333
    invoke-static {p0}, LX/6ja;->a(LX/6ja;)V

    .line 1131334
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1131335
    iget-object v0, p0, LX/6ja;->attributionAppId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1131336
    sget-object v0, LX/6ja;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131337
    iget-object v0, p0, LX/6ja;->attributionAppId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1131338
    :cond_0
    iget-object v0, p0, LX/6ja;->attributionMetadata:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1131339
    iget-object v0, p0, LX/6ja;->attributionMetadata:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1131340
    sget-object v0, LX/6ja;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131341
    iget-object v0, p0, LX/6ja;->attributionMetadata:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1131342
    :cond_1
    iget-object v0, p0, LX/6ja;->attributionAppName:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1131343
    iget-object v0, p0, LX/6ja;->attributionAppName:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1131344
    sget-object v0, LX/6ja;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131345
    iget-object v0, p0, LX/6ja;->attributionAppName:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1131346
    :cond_2
    iget-object v0, p0, LX/6ja;->attributionAppIconURI:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1131347
    iget-object v0, p0, LX/6ja;->attributionAppIconURI:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1131348
    sget-object v0, LX/6ja;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131349
    iget-object v0, p0, LX/6ja;->attributionAppIconURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1131350
    :cond_3
    iget-object v0, p0, LX/6ja;->androidPackageName:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1131351
    iget-object v0, p0, LX/6ja;->androidPackageName:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1131352
    sget-object v0, LX/6ja;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131353
    iget-object v0, p0, LX/6ja;->androidPackageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1131354
    :cond_4
    iget-object v0, p0, LX/6ja;->iOSStoreId:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 1131355
    iget-object v0, p0, LX/6ja;->iOSStoreId:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 1131356
    sget-object v0, LX/6ja;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131357
    iget-object v0, p0, LX/6ja;->iOSStoreId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1131358
    :cond_5
    iget-object v0, p0, LX/6ja;->otherUserAppScopedFbIds:Ljava/util/Map;

    if-eqz v0, :cond_6

    .line 1131359
    iget-object v0, p0, LX/6ja;->otherUserAppScopedFbIds:Ljava/util/Map;

    if-eqz v0, :cond_6

    .line 1131360
    sget-object v0, LX/6ja;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131361
    new-instance v0, LX/7H3;

    iget-object v1, p0, LX/6ja;->otherUserAppScopedFbIds:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v2, v2, v1}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 1131362
    iget-object v0, p0, LX/6ja;->otherUserAppScopedFbIds:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1131363
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, LX/1su;->a(J)V

    .line 1131364
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    goto :goto_0

    .line 1131365
    :cond_6
    iget-object v0, p0, LX/6ja;->visibility:LX/6jU;

    if-eqz v0, :cond_7

    .line 1131366
    iget-object v0, p0, LX/6ja;->visibility:LX/6jU;

    if-eqz v0, :cond_7

    .line 1131367
    sget-object v0, LX/6ja;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131368
    iget-object v0, p0, LX/6ja;->visibility:LX/6jU;

    invoke-virtual {v0, p1}, LX/6jU;->a(LX/1su;)V

    .line 1131369
    :cond_7
    iget-object v0, p0, LX/6ja;->replyActionType:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 1131370
    iget-object v0, p0, LX/6ja;->replyActionType:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 1131371
    sget-object v0, LX/6ja;->k:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131372
    iget-object v0, p0, LX/6ja;->replyActionType:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 1131373
    :cond_8
    iget-object v0, p0, LX/6ja;->customReplyAction:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1131374
    iget-object v0, p0, LX/6ja;->customReplyAction:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1131375
    sget-object v0, LX/6ja;->l:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131376
    iget-object v0, p0, LX/6ja;->customReplyAction:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1131377
    :cond_9
    iget-object v0, p0, LX/6ja;->attributionType:Ljava/lang/Long;

    if-eqz v0, :cond_a

    .line 1131378
    iget-object v0, p0, LX/6ja;->attributionType:Ljava/lang/Long;

    if-eqz v0, :cond_a

    .line 1131379
    sget-object v0, LX/6ja;->m:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1131380
    iget-object v0, p0, LX/6ja;->attributionType:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1131381
    :cond_a
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1131382
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1131383
    return-void
.end method

.method public final a(LX/6ja;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1131253
    if-nez p1, :cond_1

    .line 1131254
    :cond_0
    :goto_0
    return v2

    .line 1131255
    :cond_1
    iget-object v0, p0, LX/6ja;->attributionAppId:Ljava/lang/Long;

    if-eqz v0, :cond_18

    move v0, v1

    .line 1131256
    :goto_1
    iget-object v3, p1, LX/6ja;->attributionAppId:Ljava/lang/Long;

    if-eqz v3, :cond_19

    move v3, v1

    .line 1131257
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 1131258
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131259
    iget-object v0, p0, LX/6ja;->attributionAppId:Ljava/lang/Long;

    iget-object v3, p1, LX/6ja;->attributionAppId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131260
    :cond_3
    iget-object v0, p0, LX/6ja;->attributionMetadata:Ljava/lang/String;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 1131261
    :goto_3
    iget-object v3, p1, LX/6ja;->attributionMetadata:Ljava/lang/String;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 1131262
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1131263
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131264
    iget-object v0, p0, LX/6ja;->attributionMetadata:Ljava/lang/String;

    iget-object v3, p1, LX/6ja;->attributionMetadata:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131265
    :cond_5
    iget-object v0, p0, LX/6ja;->attributionAppName:Ljava/lang/String;

    if-eqz v0, :cond_1c

    move v0, v1

    .line 1131266
    :goto_5
    iget-object v3, p1, LX/6ja;->attributionAppName:Ljava/lang/String;

    if-eqz v3, :cond_1d

    move v3, v1

    .line 1131267
    :goto_6
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1131268
    :cond_6
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131269
    iget-object v0, p0, LX/6ja;->attributionAppName:Ljava/lang/String;

    iget-object v3, p1, LX/6ja;->attributionAppName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131270
    :cond_7
    iget-object v0, p0, LX/6ja;->attributionAppIconURI:Ljava/lang/String;

    if-eqz v0, :cond_1e

    move v0, v1

    .line 1131271
    :goto_7
    iget-object v3, p1, LX/6ja;->attributionAppIconURI:Ljava/lang/String;

    if-eqz v3, :cond_1f

    move v3, v1

    .line 1131272
    :goto_8
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1131273
    :cond_8
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131274
    iget-object v0, p0, LX/6ja;->attributionAppIconURI:Ljava/lang/String;

    iget-object v3, p1, LX/6ja;->attributionAppIconURI:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131275
    :cond_9
    iget-object v0, p0, LX/6ja;->androidPackageName:Ljava/lang/String;

    if-eqz v0, :cond_20

    move v0, v1

    .line 1131276
    :goto_9
    iget-object v3, p1, LX/6ja;->androidPackageName:Ljava/lang/String;

    if-eqz v3, :cond_21

    move v3, v1

    .line 1131277
    :goto_a
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1131278
    :cond_a
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131279
    iget-object v0, p0, LX/6ja;->androidPackageName:Ljava/lang/String;

    iget-object v3, p1, LX/6ja;->androidPackageName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131280
    :cond_b
    iget-object v0, p0, LX/6ja;->iOSStoreId:Ljava/lang/Long;

    if-eqz v0, :cond_22

    move v0, v1

    .line 1131281
    :goto_b
    iget-object v3, p1, LX/6ja;->iOSStoreId:Ljava/lang/Long;

    if-eqz v3, :cond_23

    move v3, v1

    .line 1131282
    :goto_c
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 1131283
    :cond_c
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131284
    iget-object v0, p0, LX/6ja;->iOSStoreId:Ljava/lang/Long;

    iget-object v3, p1, LX/6ja;->iOSStoreId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131285
    :cond_d
    iget-object v0, p0, LX/6ja;->otherUserAppScopedFbIds:Ljava/util/Map;

    if-eqz v0, :cond_24

    move v0, v1

    .line 1131286
    :goto_d
    iget-object v3, p1, LX/6ja;->otherUserAppScopedFbIds:Ljava/util/Map;

    if-eqz v3, :cond_25

    move v3, v1

    .line 1131287
    :goto_e
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 1131288
    :cond_e
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131289
    iget-object v0, p0, LX/6ja;->otherUserAppScopedFbIds:Ljava/util/Map;

    iget-object v3, p1, LX/6ja;->otherUserAppScopedFbIds:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131290
    :cond_f
    iget-object v0, p0, LX/6ja;->visibility:LX/6jU;

    if-eqz v0, :cond_26

    move v0, v1

    .line 1131291
    :goto_f
    iget-object v3, p1, LX/6ja;->visibility:LX/6jU;

    if-eqz v3, :cond_27

    move v3, v1

    .line 1131292
    :goto_10
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 1131293
    :cond_10
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131294
    iget-object v0, p0, LX/6ja;->visibility:LX/6jU;

    iget-object v3, p1, LX/6ja;->visibility:LX/6jU;

    invoke-virtual {v0, v3}, LX/6jU;->a(LX/6jU;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131295
    :cond_11
    iget-object v0, p0, LX/6ja;->replyActionType:Ljava/lang/Integer;

    if-eqz v0, :cond_28

    move v0, v1

    .line 1131296
    :goto_11
    iget-object v3, p1, LX/6ja;->replyActionType:Ljava/lang/Integer;

    if-eqz v3, :cond_29

    move v3, v1

    .line 1131297
    :goto_12
    if-nez v0, :cond_12

    if-eqz v3, :cond_13

    .line 1131298
    :cond_12
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131299
    iget-object v0, p0, LX/6ja;->replyActionType:Ljava/lang/Integer;

    iget-object v3, p1, LX/6ja;->replyActionType:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131300
    :cond_13
    iget-object v0, p0, LX/6ja;->customReplyAction:Ljava/lang/String;

    if-eqz v0, :cond_2a

    move v0, v1

    .line 1131301
    :goto_13
    iget-object v3, p1, LX/6ja;->customReplyAction:Ljava/lang/String;

    if-eqz v3, :cond_2b

    move v3, v1

    .line 1131302
    :goto_14
    if-nez v0, :cond_14

    if-eqz v3, :cond_15

    .line 1131303
    :cond_14
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131304
    iget-object v0, p0, LX/6ja;->customReplyAction:Ljava/lang/String;

    iget-object v3, p1, LX/6ja;->customReplyAction:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1131305
    :cond_15
    iget-object v0, p0, LX/6ja;->attributionType:Ljava/lang/Long;

    if-eqz v0, :cond_2c

    move v0, v1

    .line 1131306
    :goto_15
    iget-object v3, p1, LX/6ja;->attributionType:Ljava/lang/Long;

    if-eqz v3, :cond_2d

    move v3, v1

    .line 1131307
    :goto_16
    if-nez v0, :cond_16

    if-eqz v3, :cond_17

    .line 1131308
    :cond_16
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 1131309
    iget-object v0, p0, LX/6ja;->attributionType:Ljava/lang/Long;

    iget-object v3, p1, LX/6ja;->attributionType:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_17
    move v2, v1

    .line 1131310
    goto/16 :goto_0

    :cond_18
    move v0, v2

    .line 1131311
    goto/16 :goto_1

    :cond_19
    move v3, v2

    .line 1131312
    goto/16 :goto_2

    :cond_1a
    move v0, v2

    .line 1131313
    goto/16 :goto_3

    :cond_1b
    move v3, v2

    .line 1131314
    goto/16 :goto_4

    :cond_1c
    move v0, v2

    .line 1131315
    goto/16 :goto_5

    :cond_1d
    move v3, v2

    .line 1131316
    goto/16 :goto_6

    :cond_1e
    move v0, v2

    .line 1131317
    goto/16 :goto_7

    :cond_1f
    move v3, v2

    .line 1131318
    goto/16 :goto_8

    :cond_20
    move v0, v2

    .line 1131319
    goto/16 :goto_9

    :cond_21
    move v3, v2

    .line 1131320
    goto/16 :goto_a

    :cond_22
    move v0, v2

    .line 1131321
    goto/16 :goto_b

    :cond_23
    move v3, v2

    .line 1131322
    goto/16 :goto_c

    :cond_24
    move v0, v2

    .line 1131323
    goto/16 :goto_d

    :cond_25
    move v3, v2

    .line 1131324
    goto/16 :goto_e

    :cond_26
    move v0, v2

    .line 1131325
    goto/16 :goto_f

    :cond_27
    move v3, v2

    .line 1131326
    goto/16 :goto_10

    :cond_28
    move v0, v2

    .line 1131327
    goto/16 :goto_11

    :cond_29
    move v3, v2

    .line 1131328
    goto/16 :goto_12

    :cond_2a
    move v0, v2

    .line 1131329
    goto :goto_13

    :cond_2b
    move v3, v2

    .line 1131330
    goto :goto_14

    :cond_2c
    move v0, v2

    .line 1131331
    goto :goto_15

    :cond_2d
    move v3, v2

    .line 1131332
    goto :goto_16
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1131249
    if-nez p1, :cond_1

    .line 1131250
    :cond_0
    :goto_0
    return v0

    .line 1131251
    :cond_1
    instance-of v1, p1, LX/6ja;

    if-eqz v1, :cond_0

    .line 1131252
    check-cast p1, LX/6ja;

    invoke-virtual {p0, p1}, LX/6ja;->a(LX/6ja;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1131248
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1131245
    sget-boolean v0, LX/6ja;->a:Z

    .line 1131246
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6ja;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1131247
    return-object v0
.end method
