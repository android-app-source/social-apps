.class public LX/8bO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0ad;

.field private final b:LX/8bZ;


# direct methods
.method public constructor <init>(LX/0ad;LX/8bZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1372010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1372011
    iput-object p1, p0, LX/8bO;->a:LX/0ad;

    .line 1372012
    iput-object p2, p0, LX/8bO;->b:LX/8bZ;

    .line 1372013
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;)LX/8bN;
    .locals 1

    .prologue
    .line 1372015
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->APP_VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->LINK_VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    if-ne p0, v0, :cond_1

    .line 1372016
    :cond_0
    sget-object v0, LX/8bN;->VIDEO:LX/8bN;

    .line 1372017
    :goto_0
    return-object v0

    .line 1372018
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;->MULTI_SHARE:Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    if-ne p0, v0, :cond_2

    .line 1372019
    sget-object v0, LX/8bN;->CAROUSEL:LX/8bN;

    goto :goto_0

    .line 1372020
    :cond_2
    sget-object v0, LX/8bN;->IMAGE:LX/8bN;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/8bO;
    .locals 1

    .prologue
    .line 1372014
    invoke-static {p0}, LX/8bO;->b(LX/0QB;)LX/8bO;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/8bO;
    .locals 3

    .prologue
    .line 1372008
    new-instance v2, LX/8bO;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/8bZ;->b(LX/0QB;)LX/8bZ;

    move-result-object v1

    check-cast v1, LX/8bZ;

    invoke-direct {v2, v0, v1}, LX/8bO;-><init>(LX/0ad;LX/8bZ;)V

    .line 1372009
    return-object v2
.end method

.method private k()Z
    .locals 3

    .prologue
    .line 1372005
    iget-object v0, p0, LX/8bO;->b:LX/8bZ;

    const/4 v1, 0x0

    .line 1372006
    invoke-virtual {v0}, LX/8bZ;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/8bZ;->d:LX/0Uh;

    const/16 p0, 0xc0

    invoke-virtual {v2, p0, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 1372007
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1372021
    invoke-direct {p0}, LX/8bO;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/8bO;->a:LX/0ad;

    sget-short v2, LX/2yD;->c:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 1372004
    iget-object v0, p0, LX/8bO;->a:LX/0ad;

    sget-short v1, LX/2yD;->g:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 1372003
    iget-object v0, p0, LX/8bO;->a:LX/0ad;

    sget-short v1, LX/2yD;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 1372002
    iget-object v0, p0, LX/8bO;->a:LX/0ad;

    sget-short v1, LX/2yD;->i:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1371999
    invoke-direct {p0}, LX/8bO;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/8bO;->a:LX/0ad;

    sget-short v2, LX/2yD;->e:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final g()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1372001
    invoke-direct {p0}, LX/8bO;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/8bO;->a:LX/0ad;

    sget-short v2, LX/2yD;->f:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final i()Z
    .locals 3

    .prologue
    .line 1372000
    iget-object v0, p0, LX/8bO;->a:LX/0ad;

    sget-short v1, LX/2yD;->d:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
