.class public LX/8EC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/8EC;


# instance fields
.field public final a:LX/11i;

.field private final b:LX/01T;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/0Pq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/11i;LX/01T;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1313498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1313499
    iput-object p1, p0, LX/8EC;->a:LX/11i;

    .line 1313500
    iput-object p2, p0, LX/8EC;->b:LX/01T;

    .line 1313501
    iget-object v0, p0, LX/8EC;->b:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_0

    .line 1313502
    sget-object v0, LX/8Dx;->a:LX/8Dw;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/8EC;->c:LX/0Px;

    .line 1313503
    :goto_0
    return-void

    .line 1313504
    :cond_0
    sget-object v0, LX/8Dx;->c:LX/8Ds;

    sget-object v1, LX/8Dx;->b:LX/8Du;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/8EC;->c:LX/0Px;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/8EC;
    .locals 5

    .prologue
    .line 1313505
    sget-object v0, LX/8EC;->d:LX/8EC;

    if-nez v0, :cond_1

    .line 1313506
    const-class v1, LX/8EC;

    monitor-enter v1

    .line 1313507
    :try_start_0
    sget-object v0, LX/8EC;->d:LX/8EC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1313508
    if-eqz v2, :cond_0

    .line 1313509
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1313510
    new-instance p0, LX/8EC;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v4

    check-cast v4, LX/01T;

    invoke-direct {p0, v3, v4}, LX/8EC;-><init>(LX/11i;LX/01T;)V

    .line 1313511
    move-object v0, p0

    .line 1313512
    sput-object v0, LX/8EC;->d:LX/8EC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1313513
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1313514
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1313515
    :cond_1
    sget-object v0, LX/8EC;->d:LX/8EC;

    return-object v0

    .line 1313516
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1313517
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
