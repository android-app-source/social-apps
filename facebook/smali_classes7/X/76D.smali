.class public abstract LX/76D;
.super Landroid/os/Binder;
.source ""

# interfaces
.implements LX/76B;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1170830
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 1170831
    const-string v0, "com.facebook.push.mqtt.ipc.MqttPublishListener"

    invoke-virtual {p0, p0, v0}, LX/76D;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 1170832
    return-void
.end method

.method public static a(Landroid/os/IBinder;)LX/76B;
    .locals 2

    .prologue
    .line 1170833
    if-nez p0, :cond_0

    .line 1170834
    const/4 v0, 0x0

    .line 1170835
    :goto_0
    return-object v0

    .line 1170836
    :cond_0
    const-string v0, "com.facebook.push.mqtt.ipc.MqttPublishListener"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 1170837
    if-eqz v0, :cond_1

    instance-of v1, v0, LX/76B;

    if-eqz v1, :cond_1

    .line 1170838
    check-cast v0, LX/76B;

    goto :goto_0

    .line 1170839
    :cond_1
    new-instance v0, LX/76C;

    invoke-direct {v0, p0}, LX/76C;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public final asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 1170840
    return-object p0
.end method

.method public final onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1170841
    sparse-switch p1, :sswitch_data_0

    .line 1170842
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 1170843
    :sswitch_0
    const-string v1, "com.facebook.push.mqtt.ipc.MqttPublishListener"

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 1170844
    :sswitch_1
    const-string v1, "com.facebook.push.mqtt.ipc.MqttPublishListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1170845
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 1170846
    invoke-virtual {p0, v2, v3}, LX/76D;->a(J)V

    goto :goto_0

    .line 1170847
    :sswitch_2
    const-string v1, "com.facebook.push.mqtt.ipc.MqttPublishListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1170848
    invoke-virtual {p0}, LX/76D;->a()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
