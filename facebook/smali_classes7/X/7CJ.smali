.class public LX/7CJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1180392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1180393
    if-nez p0, :cond_0

    .line 1180394
    const-string v0, "unknown"

    .line 1180395
    :goto_0
    return-object v0

    .line 1180396
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1180397
    const-string v0, "place"

    goto :goto_0

    .line 1180398
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->MOVIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1180399
    const-string v0, "movie"

    goto :goto_0

    .line 1180400
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->MUSIC:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1180401
    const-string v0, "music"

    goto :goto_0

    .line 1180402
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GAMES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1180403
    const-string v0, "game"

    goto :goto_0

    .line 1180404
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1180405
    const-string v0, "page"

    goto :goto_0

    .line 1180406
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->MARKETPLACE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1180407
    const-string v0, "commerce"

    goto :goto_0

    .line 1180408
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1180409
    const-string v0, "blended_videos"

    goto :goto_0

    .line 1180410
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1180411
    const-string v0, "blended_photos"

    goto :goto_0

    .line 1180412
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1180413
    const-string v0, "blended"

    goto :goto_0

    .line 1180414
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1180415
    const-string v0, "user"

    goto :goto_0

    .line 1180416
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1180417
    const-string v0, "group"

    goto :goto_0

    .line 1180418
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1180419
    const-string v0, "event"

    goto/16 :goto_0

    .line 1180420
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1180421
    const-string v0, "app"

    goto/16 :goto_0

    .line 1180422
    :cond_d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->LATEST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1180423
    const-string v0, "latest"

    goto/16 :goto_0

    .line 1180424
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->FINITE_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1180425
    const-string v0, "finite_module"

    goto/16 :goto_0

    .line 1180426
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1180427
    const-string v0, "story"

    goto/16 :goto_0

    .line 1180428
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1180429
    const-string v0, "photo"

    goto/16 :goto_0

    .line 1180430
    :cond_11
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1180431
    const-string v0, "live_video"

    goto/16 :goto_0

    .line 1180432
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1180433
    const-string v0, "video"

    goto/16 :goto_0

    .line 1180434
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1180435
    const-string v0, "video_publishers"

    goto/16 :goto_0

    .line 1180436
    :cond_14
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->QUERY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1180437
    const-string v0, "query"

    goto/16 :goto_0

    .line 1180438
    :cond_15
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_ENTITIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1180439
    const-string v0, "mixed_entities"

    goto/16 :goto_0

    .line 1180440
    :cond_16
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1180441
    const-string v0, "video_link"

    goto/16 :goto_0

    .line 1180442
    :cond_17
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 1180443
    const-string v0, "link"

    goto/16 :goto_0

    .line 1180444
    :cond_18
    const-string v0, "unknown"

    goto/16 :goto_0
.end method
