.class public final LX/7OM;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7ON;


# direct methods
.method public constructor <init>(LX/7ON;)V
    .locals 0

    .prologue
    .line 1201383
    iput-object p1, p0, LX/7OM;->a:LX/7ON;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14

    .prologue
    .line 1201384
    const/4 v6, 0x0

    .line 1201385
    iget-object v0, p0, LX/7OM;->a:LX/7ON;

    iget-object v0, v0, LX/7ON;->b:LX/3Ie;

    iget-object v0, v0, LX/3Ie;->b:LX/1C2;

    iget-object v1, p0, LX/7OM;->a:LX/7ON;

    iget-object v1, v1, LX/7ON;->b:LX/3Ie;

    iget-object v1, v1, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/7OM;->a:LX/7ON;

    iget-object v1, v1, LX/7ON;->b:LX/3Ie;

    iget-object v1, v1, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1201386
    iget-object v2, v1, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v1, v2

    .line 1201387
    :goto_0
    iget-object v2, p0, LX/7OM;->a:LX/7ON;

    iget-object v2, v2, LX/7ON;->b:LX/3Ie;

    iget-object v2, v2, LX/3Ie;->n:LX/04G;

    iget-object v3, p0, LX/7OM;->a:LX/7ON;

    iget-object v3, v3, LX/7ON;->a:LX/2pa;

    iget-object v3, v3, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v3, v3, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p0, LX/7OM;->a:LX/7ON;

    iget-object v4, v4, LX/7ON;->b:LX/3Ie;

    iget-object v4, v4, LX/2oy;->j:LX/2pb;

    if-eqz v4, :cond_2

    iget-object v4, p0, LX/7OM;->a:LX/7ON;

    iget-object v4, v4, LX/7ON;->b:LX/3Ie;

    iget-object v4, v4, LX/2oy;->j:LX/2pb;

    invoke-virtual {v4}, LX/2pb;->h()I

    move-result v4

    :goto_1
    iget-object v5, p0, LX/7OM;->a:LX/7ON;

    iget-object v5, v5, LX/7ON;->a:LX/2pa;

    iget-object v5, v5, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    .line 1201388
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v8, LX/04A;->VIDEO_VR_CAST_CLICK:LX/04A;

    iget-object v8, v8, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v8, LX/04F;->VIDEO_TIME_POSITION_PARAM:LX/04F;

    iget-object v8, v8, LX/04F;->value:Ljava/lang/String;

    int-to-float v9, v4

    const/high16 v10, 0x447a0000    # 1000.0f

    div-float/2addr v9, v10

    float-to-double v9, v9

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    .line 1201389
    const/4 v11, 0x0

    move-object v7, v0

    move-object v9, v3

    move-object v10, v5

    move-object v12, v1

    move-object v13, v2

    invoke-static/range {v7 .. v13}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    .line 1201390
    sget-object v0, LX/7D1;->GEAR_VR:LX/7D1;

    iget-object v1, p0, LX/7OM;->a:LX/7ON;

    iget-object v1, v1, LX/7ON;->a:LX/2pa;

    invoke-static {v0, v1}, LX/7Ne;->a(LX/7D1;LX/2pa;)Landroid/content/Intent;

    move-result-object v0

    .line 1201391
    if-eqz v0, :cond_0

    .line 1201392
    iget-object v1, p0, LX/7OM;->a:LX/7ON;

    iget-object v1, v1, LX/7ON;->b:LX/3Ie;

    iget-object v1, v1, LX/3Ie;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/7OM;->a:LX/7ON;

    iget-object v2, v2, LX/7ON;->b:LX/3Ie;

    invoke-virtual {v2}, LX/3Ie;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1201393
    :cond_0
    return-object v6

    :cond_1
    move-object v1, v6

    .line 1201394
    goto :goto_0

    :cond_2
    const/4 v4, -0x1

    goto :goto_1
.end method
