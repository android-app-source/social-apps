.class public LX/89P;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/89P;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1304936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304937
    iput-object p1, p0, LX/89P;->a:LX/0Ot;

    .line 1304938
    return-void
.end method

.method public static a(LX/0QB;)LX/89P;
    .locals 4

    .prologue
    .line 1304939
    sget-object v0, LX/89P;->b:LX/89P;

    if-nez v0, :cond_1

    .line 1304940
    const-class v1, LX/89P;

    monitor-enter v1

    .line 1304941
    :try_start_0
    sget-object v0, LX/89P;->b:LX/89P;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1304942
    if-eqz v2, :cond_0

    .line 1304943
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1304944
    new-instance v3, LX/89P;

    const/16 p0, 0x3be

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/89P;-><init>(LX/0Ot;)V

    .line 1304945
    move-object v0, v3

    .line 1304946
    sput-object v0, LX/89P;->b:LX/89P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1304947
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1304948
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1304949
    :cond_1
    sget-object v0, LX/89P;->b:LX/89P;

    return-object v0

    .line 1304950
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1304951
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V
    .locals 2

    .prologue
    .line 1304952
    iget-object v0, p0, LX/89P;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1, p2, p3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1304953
    return-void
.end method
