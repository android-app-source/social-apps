.class public final LX/7OD;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/7MC;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/VideoPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/VideoPlugin;)V
    .locals 0

    .prologue
    .line 1201235
    iput-object p1, p0, LX/7OD;->a:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/player/plugins/VideoPlugin;B)V
    .locals 0

    .prologue
    .line 1201234
    invoke-direct {p0, p1}, LX/7OD;-><init>(Lcom/facebook/video/player/plugins/VideoPlugin;)V

    return-void
.end method

.method private a(LX/7MC;)V
    .locals 4

    .prologue
    .line 1201227
    iget-object v0, p0, LX/7OD;->a:Lcom/facebook/video/player/plugins/VideoPlugin;

    iget-object v0, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    .line 1201228
    iget-object v0, p1, LX/7MC;->a:Landroid/graphics/Bitmap;

    move-object v0, v0

    .line 1201229
    iget-object v1, p0, LX/7OD;->a:Lcom/facebook/video/player/plugins/VideoPlugin;

    iget-object v1, v1, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v1

    .line 1201230
    iget-object v2, p0, LX/7OD;->a:Lcom/facebook/video/player/plugins/VideoPlugin;

    iget-object v2, v2, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoId()Ljava/lang/String;

    move-result-object v2

    .line 1201231
    iget-object v3, p0, LX/7OD;->a:Lcom/facebook/video/player/plugins/VideoPlugin;

    iget-object v3, v3, Lcom/facebook/video/player/plugins/VideoPlugin;->f:LX/2oz;

    invoke-virtual {v3, v2, v1, v0}, LX/2oz;->a(Ljava/lang/String;ILandroid/graphics/Bitmap;)V

    .line 1201232
    iget-object v1, p0, LX/7OD;->a:Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-static {v1, v0}, Lcom/facebook/video/player/plugins/VideoPlugin;->setPauseFrame(Lcom/facebook/video/player/plugins/VideoPlugin;Landroid/graphics/Bitmap;)V

    .line 1201233
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/7MC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1201226
    const-class v0, LX/7MC;

    return-object v0
.end method

.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 1201225
    check-cast p1, LX/7MC;

    invoke-direct {p0, p1}, LX/7OD;->a(LX/7MC;)V

    return-void
.end method
