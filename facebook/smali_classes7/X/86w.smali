.class public final enum LX/86w;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/86w;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/86w;

.field public static final enum ACTIVE_DRAWING:LX/86w;

.field public static final enum ACTIVE_EMPTY:LX/86w;

.field public static final enum ACTIVE_HAS_DRAWING:LX/86w;

.field public static final enum AVAILABLE:LX/86w;

.field public static final enum HIDDEN:LX/86w;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1298234
    new-instance v0, LX/86w;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v2}, LX/86w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86w;->HIDDEN:LX/86w;

    .line 1298235
    new-instance v0, LX/86w;

    const-string v1, "AVAILABLE"

    invoke-direct {v0, v1, v3}, LX/86w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86w;->AVAILABLE:LX/86w;

    .line 1298236
    new-instance v0, LX/86w;

    const-string v1, "ACTIVE_EMPTY"

    invoke-direct {v0, v1, v4}, LX/86w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86w;->ACTIVE_EMPTY:LX/86w;

    .line 1298237
    new-instance v0, LX/86w;

    const-string v1, "ACTIVE_HAS_DRAWING"

    invoke-direct {v0, v1, v5}, LX/86w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86w;->ACTIVE_HAS_DRAWING:LX/86w;

    .line 1298238
    new-instance v0, LX/86w;

    const-string v1, "ACTIVE_DRAWING"

    invoke-direct {v0, v1, v6}, LX/86w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/86w;->ACTIVE_DRAWING:LX/86w;

    .line 1298239
    const/4 v0, 0x5

    new-array v0, v0, [LX/86w;

    sget-object v1, LX/86w;->HIDDEN:LX/86w;

    aput-object v1, v0, v2

    sget-object v1, LX/86w;->AVAILABLE:LX/86w;

    aput-object v1, v0, v3

    sget-object v1, LX/86w;->ACTIVE_EMPTY:LX/86w;

    aput-object v1, v0, v4

    sget-object v1, LX/86w;->ACTIVE_HAS_DRAWING:LX/86w;

    aput-object v1, v0, v5

    sget-object v1, LX/86w;->ACTIVE_DRAWING:LX/86w;

    aput-object v1, v0, v6

    sput-object v0, LX/86w;->$VALUES:[LX/86w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1298241
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/86w;
    .locals 1

    .prologue
    .line 1298242
    const-class v0, LX/86w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/86w;

    return-object v0
.end method

.method public static values()[LX/86w;
    .locals 1

    .prologue
    .line 1298240
    sget-object v0, LX/86w;->$VALUES:[LX/86w;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/86w;

    return-object v0
.end method
