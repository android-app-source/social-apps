.class public final LX/8Tq;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8Xn;

.field public final synthetic b:LX/8Tr;


# direct methods
.method public constructor <init>(LX/8Tr;LX/8Xn;)V
    .locals 0

    .prologue
    .line 1349183
    iput-object p1, p0, LX/8Tq;->b:LX/8Tr;

    iput-object p2, p0, LX/8Tq;->a:LX/8Xn;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1349184
    iget-object v0, p0, LX/8Tq;->b:LX/8Tr;

    iget-object v0, v0, LX/8Tr;->b:LX/8TD;

    sget-object v1, LX/8TE;->PLAYER_SCORE:LX/8TE;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    .line 1349185
    iget-object v0, p0, LX/8Tq;->a:LX/8Xn;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/8Xn;->a(J)V

    .line 1349186
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1349187
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1349188
    iget-object v0, p0, LX/8Tq;->b:LX/8Tr;

    iget-object v0, v0, LX/8Tr;->b:LX/8TD;

    sget-object v1, LX/8TE;->PLAYER_SCORE:LX/8TE;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;)V

    .line 1349189
    iget-object v1, p0, LX/8Tq;->b:LX/8Tr;

    .line 1349190
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1349191
    check-cast v0, Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel;

    invoke-static {v1, v0}, LX/8Tr;->a(LX/8Tr;Lcom/facebook/quicksilver/graphql/UserScoreInfoQueryModels$UserHighScoreInfoQueryModel;)J

    move-result-wide v0

    .line 1349192
    iget-object v2, p0, LX/8Tq;->a:LX/8Xn;

    invoke-virtual {v2, v0, v1}, LX/8Xn;->a(J)V

    .line 1349193
    return-void
.end method
