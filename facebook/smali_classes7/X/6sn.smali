.class public LX/6sn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6sl;


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1153771
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153772
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The bundle of CheckoutRow found to be empty."

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1153773
    iput-object p1, p0, LX/6sn;->a:LX/0Px;

    .line 1153774
    return-void

    .line 1153775
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1153776
    iget-object v1, p0, LX/6sn;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1153777
    return-void

    .line 1153778
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1153779
    iget-object v0, p0, LX/6sn;->a:LX/0Px;

    return-object v0
.end method

.method public final b()LX/6so;
    .locals 2

    .prologue
    .line 1153780
    invoke-direct {p0}, LX/6sn;->e()V

    .line 1153781
    iget-object v0, p0, LX/6sn;->a:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6E3;

    invoke-interface {v0}, LX/6E3;->b()LX/6so;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1153782
    invoke-direct {p0}, LX/6sn;->e()V

    .line 1153783
    iget-object v0, p0, LX/6sn;->a:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6E3;

    invoke-interface {v0}, LX/6E3;->c()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1153784
    const/4 v0, 0x1

    return v0
.end method
