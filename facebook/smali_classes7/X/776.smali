.class public LX/776;
.super LX/2g7;
.source ""


# instance fields
.field private final a:LX/0s6;

.field private final b:LX/0SG;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/0s6;LX/0SG;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171605
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1171606
    iput-object p1, p0, LX/776;->a:LX/0s6;

    .line 1171607
    iput-object p2, p0, LX/776;->b:LX/0SG;

    .line 1171608
    iput-object p3, p0, LX/776;->c:Landroid/content/Context;

    .line 1171609
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1171610
    iget-object v1, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1171611
    iget-object v1, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1171612
    iget-object v2, p0, LX/776;->a:LX/0s6;

    iget-object v3, p0, LX/776;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LX/01H;->b(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 1171613
    if-nez v2, :cond_1

    .line 1171614
    :cond_0
    :goto_0
    return v0

    .line 1171615
    :cond_1
    iget-wide v4, v2, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    .line 1171616
    iget-wide v2, v2, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    .line 1171617
    iget-object v6, p0, LX/776;->b:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    .line 1171618
    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    sub-long v2, v6, v2

    int-to-long v4, v1

    const-wide/32 v6, 0x36ee80

    mul-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
