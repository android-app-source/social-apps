.class public interface abstract LX/7og;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7of;


# annotations
.annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
    from = "EventTicketOrder"
    processor = "com.facebook.dracula.transformer.Transformer"
.end annotation


# virtual methods
.method public abstract eX_()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCreditCardUsed"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract j()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOrderActionLink"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()LX/0Px;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTicketOrderItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/events/graphql/EventsGraphQLInterfaces$EventTicketOrder$$TicketOrderItems$;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract l()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTotalOrderPrice"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
