.class public final LX/7lN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1234791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1234792
    iput-object v0, p0, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1234793
    iput-object v0, p0, LX/7lN;->b:Ljava/lang/String;

    .line 1234794
    iput-object v0, p0, LX/7lN;->c:Ljava/lang/String;

    .line 1234795
    iput-object v0, p0, LX/7lN;->d:Ljava/lang/String;

    .line 1234796
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1234797
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1234798
    iput-object v0, p0, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1234799
    iput-object v0, p0, LX/7lN;->b:Ljava/lang/String;

    .line 1234800
    iput-object v0, p0, LX/7lN;->c:Ljava/lang/String;

    .line 1234801
    iput-object v0, p0, LX/7lN;->d:Ljava/lang/String;

    .line 1234802
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    iput-object v0, p0, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1234803
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->b:Ljava/lang/String;

    iput-object v0, p0, LX/7lN;->b:Ljava/lang/String;

    .line 1234804
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->c:Ljava/lang/String;

    iput-object v0, p0, LX/7lN;->c:Ljava/lang/String;

    .line 1234805
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->d:Ljava/lang/String;

    iput-object v0, p0, LX/7lN;->d:Ljava/lang/String;

    .line 1234806
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;
    .locals 2

    .prologue
    .line 1234807
    new-instance v0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    invoke-direct {v0, p0}, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;-><init>(LX/7lN;)V

    return-object v0
.end method
