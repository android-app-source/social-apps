.class public LX/73O;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/73N;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/73P;)V
    .locals 1

    .prologue
    .line 1165604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1165605
    iget-object v0, p1, LX/73P;->a:LX/73N;

    move-object v0, v0

    .line 1165606
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/73N;

    iput-object v0, p0, LX/73O;->a:LX/73N;

    .line 1165607
    iget-object v0, p1, LX/73P;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1165608
    iput-object v0, p0, LX/73O;->b:Ljava/lang/String;

    .line 1165609
    iget-object v0, p1, LX/73P;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1165610
    iput-object v0, p0, LX/73O;->c:Ljava/lang/String;

    .line 1165611
    iget-object v0, p1, LX/73P;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1165612
    iput-object v0, p0, LX/73O;->d:Ljava/lang/String;

    .line 1165613
    return-void
.end method

.method public static newBuilder()LX/73P;
    .locals 1

    .prologue
    .line 1165614
    new-instance v0, LX/73P;

    invoke-direct {v0}, LX/73P;-><init>()V

    return-object v0
.end method
