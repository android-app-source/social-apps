.class public LX/8GL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:LX/8GH;

.field public final c:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;)V
    .locals 1

    .prologue
    .line 1318934
    const/16 v0, 0xa

    invoke-direct {p0, p1, v0}, LX/8GL;-><init>(Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;I)V

    .line 1318935
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;I)V
    .locals 2

    .prologue
    .line 1318936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1318937
    new-instance v0, LX/8GK;

    invoke-direct {v0, p0}, LX/8GK;-><init>(LX/8GL;)V

    iput-object v0, p0, LX/8GL;->b:LX/8GH;

    .line 1318938
    iput p2, p0, LX/8GL;->a:I

    .line 1318939
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    iput-object v0, p0, LX/8GL;->c:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    .line 1318940
    iget-object v0, p0, LX/8GL;->c:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    iget v1, p0, LX/8GL;->a:I

    .line 1318941
    iput v1, v0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    .line 1318942
    return-void
.end method


# virtual methods
.method public final a(ZLX/0Px;Ljava/lang/String;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1318943
    iput-object p2, p0, LX/8GL;->d:LX/0Px;

    .line 1318944
    iget-object v0, p0, LX/8GL;->d:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/8GL;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_2

    .line 1318945
    :cond_0
    if-eqz p4, :cond_1

    .line 1318946
    invoke-virtual {p0}, LX/8GL;->c()V

    .line 1318947
    :cond_1
    :goto_0
    return-void

    .line 1318948
    :cond_2
    if-eqz p4, :cond_3

    .line 1318949
    invoke-virtual {p0}, LX/8GL;->b()V

    .line 1318950
    :cond_3
    iget-object v0, p0, LX/8GL;->c:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->setFillFirstCircle(Z)V

    .line 1318951
    iget-object v0, p0, LX/8GL;->c:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    iget-object v1, p0, LX/8GL;->d:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setCount(I)V

    move v1, v2

    .line 1318952
    :goto_1
    iget-object v0, p0, LX/8GL;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1318953
    iget-object v0, p0, LX/8GL;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1318954
    iget-object p1, v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v0, p1

    .line 1318955
    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1318956
    :goto_2
    iget-object v0, p0, LX/8GL;->c:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    invoke-virtual {v0, v1, v1, v2}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a(IIZ)V

    goto :goto_0

    .line 1318957
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1318958
    iget-object v0, p0, LX/8GL;->c:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->setVisibility(I)V

    .line 1318959
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1318960
    iget-object v0, p0, LX/8GL;->c:Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->setVisibility(I)V

    .line 1318961
    return-void
.end method
