.class public final LX/7JH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7JG;


# instance fields
.field public final synthetic a:LX/7JI;


# direct methods
.method public constructor <init>(LX/7JI;)V
    .locals 0

    .prologue
    .line 1193270
    iput-object p1, p0, LX/7JH;->a:LX/7JI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v2, 0x4

    const/4 v0, 0x0

    .line 1193271
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1193272
    const-string v3, "type"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1193273
    const-string v4, "data"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 1193274
    const-string v5, "success"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 1193275
    const-string v6, "response_num"

    const/16 v7, 0xd

    invoke-virtual {v1, v6, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    .line 1193276
    const/4 v1, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1193277
    :cond_1
    :goto_1
    return-void

    .line 1193278
    :sswitch_0
    const-string v7, "duration_change"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :sswitch_1
    const-string v0, "version_response"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v0, "launch_response"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v0, "command_result"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v0, "status_update"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_0

    .line 1193279
    :pswitch_0
    iget-object v0, p0, LX/7JH;->a:LX/7JI;

    const-string v1, "duration"

    invoke-virtual {v4, v1}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 1193280
    iput-wide v2, v0, LX/7JI;->i:D
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1193281
    goto :goto_1

    .line 1193282
    :catch_0
    move-exception v0

    .line 1193283
    iget-object v1, p0, LX/7JH;->a:LX/7JI;

    iget-object v1, v1, LX/7JI;->a:LX/38g;

    iget-object v1, v1, LX/38g;->c:LX/37e;

    sget-object v2, LX/7JJ;->FbAppPlayer_OnMessageReceived:LX/7JJ;

    invoke-virtual {v1, v2, v0}, LX/37e;->a(LX/7JJ;Ljava/lang/Exception;)V

    goto :goto_1

    .line 1193284
    :pswitch_1
    if-eqz v5, :cond_3

    .line 1193285
    :try_start_1
    iget-object v0, p0, LX/7JH;->a:LX/7JI;

    .line 1193286
    iget v1, v0, LX/7JI;->g:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    .line 1193287
    iget-object v1, v0, LX/7JI;->a:LX/38g;

    iget-object v1, v1, LX/38g;->c:LX/37e;

    sget-object v2, LX/7JJ;->FbAppPlayer_LaunchExperience:LX/7JJ;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Incorrect state: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, LX/7JI;->g:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/37e;->a(LX/7JJ;Ljava/lang/String;)V

    .line 1193288
    :cond_2
    const/4 v1, 0x2

    invoke-static {v0, v1}, LX/7JI;->a(LX/7JI;I)V

    .line 1193289
    const-string v1, "launch"

    const-string v2, "experience"

    const-string v3, "{\"name\":\"SimpleVideo\"}"

    new-instance v4, LX/7JA;

    invoke-direct {v4, v0}, LX/7JA;-><init>(LX/7JI;)V

    invoke-static {v0, v1, v2, v3, v4}, LX/7JI;->a(LX/7JI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/27U;)V

    .line 1193290
    goto :goto_1

    .line 1193291
    :cond_3
    iget-object v0, p0, LX/7JH;->a:LX/7JI;

    iget-object v0, v0, LX/7JI;->a:LX/38g;

    iget-object v0, v0, LX/38g;->d:LX/37f;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to get version: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, LX/37f;->a(ILjava/lang/String;)V

    .line 1193292
    iget-object v0, p0, LX/7JH;->a:LX/7JI;

    iget-object v0, v0, LX/7JI;->a:LX/38g;

    iget-object v0, v0, LX/38g;->h:LX/38i;

    invoke-virtual {v0}, LX/38i;->a()V

    goto/16 :goto_1

    .line 1193293
    :pswitch_2
    if-eqz v5, :cond_5

    .line 1193294
    iget-object v0, p0, LX/7JH;->a:LX/7JI;

    const-string v1, "target"

    invoke-virtual {v4, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1193295
    iput-object v1, v0, LX/7JI;->j:Ljava/lang/String;

    .line 1193296
    iget-object v0, p0, LX/7JH;->a:LX/7JI;

    .line 1193297
    iget v1, v0, LX/7JI;->g:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_4

    .line 1193298
    iget-object v1, v0, LX/7JI;->a:LX/38g;

    iget-object v1, v1, LX/38g;->c:LX/37e;

    sget-object v2, LX/7JJ;->FbAppPlayer_ConfirmLaunch:LX/7JJ;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Incorrect state: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, LX/7JI;->g:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/37e;->a(LX/7JJ;Ljava/lang/String;)V

    .line 1193299
    :cond_4
    const/4 v1, 0x3

    invoke-static {v0, v1}, LX/7JI;->a(LX/7JI;I)V

    .line 1193300
    iget-object v1, v0, LX/7JI;->a:LX/38g;

    iget-object v1, v1, LX/38g;->h:LX/38i;

    iget-object v2, v0, LX/7JI;->a:LX/38g;

    iget-object v2, v2, LX/38g;->i:Ljava/lang/Object;

    invoke-virtual {v1, v2}, LX/38i;->a(Ljava/lang/Object;)V

    .line 1193301
    goto/16 :goto_1

    .line 1193302
    :cond_5
    iget-object v0, p0, LX/7JH;->a:LX/7JI;

    iget-object v0, v0, LX/7JI;->a:LX/38g;

    iget-object v0, v0, LX/38g;->d:LX/37f;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to launch: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, LX/37f;->a(ILjava/lang/String;)V

    .line 1193303
    iget-object v0, p0, LX/7JH;->a:LX/7JI;

    iget-object v0, v0, LX/7JI;->a:LX/38g;

    iget-object v0, v0, LX/38g;->h:LX/38i;

    invoke-virtual {v0}, LX/38i;->a()V

    goto/16 :goto_1

    .line 1193304
    :pswitch_3
    if-eqz v5, :cond_7

    .line 1193305
    iget-object v0, p0, LX/7JH;->a:LX/7JI;

    iget v0, v0, LX/7JI;->g:I

    if-eq v0, v2, :cond_6

    iget-object v0, p0, LX/7JH;->a:LX/7JI;

    iget v0, v0, LX/7JI;->g:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 1193306
    :cond_6
    iget-object v0, p0, LX/7JH;->a:LX/7JI;

    invoke-virtual {v0}, LX/7JI;->g()V

    goto/16 :goto_1

    .line 1193307
    :cond_7
    iget-object v0, p0, LX/7JH;->a:LX/7JI;

    iget-object v0, v0, LX/7JI;->a:LX/38g;

    iget-object v0, v0, LX/38g;->d:LX/37f;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to process message on state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/7JH;->a:LX/7JI;

    iget v2, v2, LX/7JI;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, LX/37f;->a(ILjava/lang/String;)V

    .line 1193308
    iget-object v0, p0, LX/7JH;->a:LX/7JI;

    iget-object v0, v0, LX/7JI;->a:LX/38g;

    iget-object v0, v0, LX/38g;->h:LX/38i;

    invoke-virtual {v0}, LX/38i;->c()V

    goto/16 :goto_1

    .line 1193309
    :pswitch_4
    iget-object v0, p0, LX/7JH;->a:LX/7JI;

    invoke-static {v0, v4}, LX/7JI;->a$redex0(LX/7JI;Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x6ecb64b3 -> :sswitch_2
        -0x65d80e4a -> :sswitch_4
        -0x4d78d46f -> :sswitch_3
        -0x39e1e438 -> :sswitch_1
        0x5b3014bb -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
