.class public final LX/8at;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 23

    .prologue
    .line 1369299
    const/16 v19, 0x0

    .line 1369300
    const/16 v18, 0x0

    .line 1369301
    const/16 v17, 0x0

    .line 1369302
    const/16 v16, 0x0

    .line 1369303
    const/4 v15, 0x0

    .line 1369304
    const/4 v14, 0x0

    .line 1369305
    const/4 v13, 0x0

    .line 1369306
    const/4 v12, 0x0

    .line 1369307
    const/4 v11, 0x0

    .line 1369308
    const/4 v10, 0x0

    .line 1369309
    const/4 v9, 0x0

    .line 1369310
    const/4 v8, 0x0

    .line 1369311
    const/4 v7, 0x0

    .line 1369312
    const/4 v6, 0x0

    .line 1369313
    const/4 v5, 0x0

    .line 1369314
    const/4 v4, 0x0

    .line 1369315
    const/4 v3, 0x0

    .line 1369316
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    .line 1369317
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1369318
    const/4 v3, 0x0

    .line 1369319
    :goto_0
    return v3

    .line 1369320
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1369321
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_10

    .line 1369322
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v20

    .line 1369323
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1369324
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_1

    if-eqz v20, :cond_1

    .line 1369325
    const-string v21, "bar_configs"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 1369326
    invoke-static/range {p0 .. p1}, LX/8av;->b(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 1369327
    :cond_2
    const-string v21, "border_config"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 1369328
    invoke-static/range {p0 .. p1}, LX/8av;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 1369329
    :cond_3
    const-string v21, "byline_area_config"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 1369330
    invoke-static/range {p0 .. p1}, LX/8av;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 1369331
    :cond_4
    const-string v21, "byline_config"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 1369332
    invoke-static/range {p0 .. p1}, LX/8aw;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 1369333
    :cond_5
    const-string v21, "cover_image_config"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 1369334
    invoke-static/range {p0 .. p1}, LX/8av;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 1369335
    :cond_6
    const-string v21, "custom_fonts"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 1369336
    invoke-static/range {p0 .. p1}, LX/8a1;->b(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 1369337
    :cond_7
    const-string v21, "description_config"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 1369338
    invoke-static/range {p0 .. p1}, LX/8aw;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 1369339
    :cond_8
    const-string v21, "device_family"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 1369340
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 1369341
    :cond_9
    const-string v21, "fallback_feed_style"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 1369342
    invoke-static/range {p0 .. p1}, LX/8au;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1369343
    :cond_a
    const-string v21, "headline_config"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 1369344
    invoke-static/range {p0 .. p1}, LX/8aw;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1369345
    :cond_b
    const-string v21, "layout_spec_version"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 1369346
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 1369347
    :cond_c
    const-string v21, "page_id"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_d

    .line 1369348
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 1369349
    :cond_d
    const-string v21, "show_bottom_gradient"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_e

    .line 1369350
    const/4 v4, 0x1

    .line 1369351
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto/16 :goto_1

    .line 1369352
    :cond_e
    const-string v21, "show_top_gradient"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_f

    .line 1369353
    const/4 v3, 0x1

    .line 1369354
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    goto/16 :goto_1

    .line 1369355
    :cond_f
    const-string v21, "source_image_config"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 1369356
    invoke-static/range {p0 .. p1}, LX/8av;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1369357
    :cond_10
    const/16 v20, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1369358
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1369359
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1369360
    const/16 v18, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1369361
    const/16 v17, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1369362
    const/16 v16, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1369363
    const/4 v15, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1369364
    const/4 v14, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1369365
    const/4 v13, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1369366
    const/16 v12, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1369367
    const/16 v11, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1369368
    const/16 v10, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1369369
    const/16 v9, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1369370
    if-eqz v4, :cond_11

    .line 1369371
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, LX/186;->a(IZ)V

    .line 1369372
    :cond_11
    if-eqz v3, :cond_12

    .line 1369373
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->a(IZ)V

    .line 1369374
    :cond_12
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1369375
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
