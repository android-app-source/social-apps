.class public final LX/72A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/72Q;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/72Q;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1164008
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164009
    iput-object p1, p0, LX/72A;->a:LX/0QB;

    .line 1164010
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1164007
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/72A;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1163998
    packed-switch p2, :pswitch_data_0

    .line 1163999
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1164000
    :pswitch_0
    new-instance v0, LX/Ije;

    const/16 v1, 0x2dd9

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x2dd6

    invoke-static {p1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 p0, 0x2867

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const/16 p2, 0x2869

    invoke-static {p1, p2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-direct {v0, v1, v2, p0, p2}, LX/Ije;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1164001
    move-object v0, v0

    .line 1164002
    :goto_0
    return-object v0

    .line 1164003
    :pswitch_1
    new-instance v0, LX/72V;

    const/16 v1, 0x2dd9

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x2dd6

    invoke-static {p1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 p0, 0x2de6

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const/16 p2, 0x2de7

    invoke-static {p1, p2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    invoke-direct {v0, v1, v2, p0, p2}, LX/72V;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1164004
    move-object v0, v0

    .line 1164005
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1164006
    const/4 v0, 0x2

    return v0
.end method
