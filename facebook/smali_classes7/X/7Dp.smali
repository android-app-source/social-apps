.class public final LX/7Dp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:D

.field public g:D

.field public h:D

.field public i:D

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/19o;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;

.field public m:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/spherical/photo/model/PhotoTile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 1183663
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1183664
    iput v0, p0, LX/7Dp;->a:I

    .line 1183665
    iput v0, p0, LX/7Dp;->b:I

    .line 1183666
    iput v0, p0, LX/7Dp;->c:I

    .line 1183667
    iput v0, p0, LX/7Dp;->d:I

    .line 1183668
    iput v0, p0, LX/7Dp;->e:I

    .line 1183669
    iput-wide v2, p0, LX/7Dp;->f:D

    .line 1183670
    iput-wide v2, p0, LX/7Dp;->g:D

    .line 1183671
    iput-wide v2, p0, LX/7Dp;->h:D

    .line 1183672
    const-wide v0, 0x4051800000000000L    # 70.0

    iput-wide v0, p0, LX/7Dp;->i:D

    .line 1183673
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/7Dp;->o:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(D)LX/7Dp;
    .locals 1

    .prologue
    .line 1183661
    iput-wide p1, p0, LX/7Dp;->f:D

    .line 1183662
    return-object p0
.end method

.method public final a(I)LX/7Dp;
    .locals 0

    .prologue
    .line 1183659
    iput p1, p0, LX/7Dp;->a:I

    .line 1183660
    return-object p0
.end method

.method public final a(LX/19o;)LX/7Dp;
    .locals 0
    .param p1    # LX/19o;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1183657
    iput-object p1, p0, LX/7Dp;->k:LX/19o;

    .line 1183658
    return-object p0
.end method

.method public final a(Lcom/facebook/spherical/photo/model/PhotoVRCastParams;)LX/7Dp;
    .locals 0
    .param p1    # Lcom/facebook/spherical/photo/model/PhotoVRCastParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1183655
    iput-object p1, p0, LX/7Dp;->m:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    .line 1183656
    return-object p0
.end method

.method public final a(Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;)LX/7Dp;
    .locals 0

    .prologue
    .line 1183653
    iput-object p1, p0, LX/7Dp;->l:Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;

    .line 1183654
    return-object p0
.end method

.method public final a(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;)LX/7Dp;
    .locals 0

    .prologue
    .line 1183651
    iput-object p1, p0, LX/7Dp;->n:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    .line 1183652
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/7Dp;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1183649
    iput-object p1, p0, LX/7Dp;->j:Ljava/lang/String;

    .line 1183650
    return-object p0
.end method

.method public final a(Ljava/util/List;)LX/7Dp;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/spherical/photo/model/PhotoTile;",
            ">;)",
            "LX/7Dp;"
        }
    .end annotation

    .prologue
    .line 1183674
    iput-object p1, p0, LX/7Dp;->o:Ljava/util/List;

    .line 1183675
    return-object p0
.end method

.method public final a()Lcom/facebook/spherical/photo/model/SphericalPhotoParams;
    .locals 2

    .prologue
    .line 1183648
    new-instance v0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    invoke-direct {v0, p0}, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;-><init>(LX/7Dp;)V

    return-object v0
.end method

.method public final b(D)LX/7Dp;
    .locals 1

    .prologue
    .line 1183646
    iput-wide p1, p0, LX/7Dp;->g:D

    .line 1183647
    return-object p0
.end method

.method public final b(I)LX/7Dp;
    .locals 0

    .prologue
    .line 1183634
    iput p1, p0, LX/7Dp;->b:I

    .line 1183635
    return-object p0
.end method

.method public final c(D)LX/7Dp;
    .locals 1

    .prologue
    .line 1183644
    iput-wide p1, p0, LX/7Dp;->h:D

    .line 1183645
    return-object p0
.end method

.method public final c(I)LX/7Dp;
    .locals 0

    .prologue
    .line 1183642
    iput p1, p0, LX/7Dp;->c:I

    .line 1183643
    return-object p0
.end method

.method public final d(D)LX/7Dp;
    .locals 1

    .prologue
    .line 1183640
    iput-wide p1, p0, LX/7Dp;->i:D

    .line 1183641
    return-object p0
.end method

.method public final d(I)LX/7Dp;
    .locals 0

    .prologue
    .line 1183638
    iput p1, p0, LX/7Dp;->d:I

    .line 1183639
    return-object p0
.end method

.method public final e(I)LX/7Dp;
    .locals 0

    .prologue
    .line 1183636
    iput p1, p0, LX/7Dp;->e:I

    .line 1183637
    return-object p0
.end method
