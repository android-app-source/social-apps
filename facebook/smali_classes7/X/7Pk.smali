.class public final LX/7Pk;
.super Lorg/apache/http/entity/AbstractHttpEntity;
.source ""


# instance fields
.field private final a:LX/7Pj;


# direct methods
.method public constructor <init>(LX/7Pj;)V
    .locals 0

    .prologue
    .line 1203182
    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    .line 1203183
    iput-object p1, p0, LX/7Pk;->a:LX/7Pj;

    .line 1203184
    return-void
.end method


# virtual methods
.method public final getContent()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 1203185
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call getContent on a RangeWriterEntity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getContentLength()J
    .locals 2

    .prologue
    .line 1203186
    iget-object v0, p0, LX/7Pk;->a:LX/7Pj;

    .line 1203187
    iget-object v1, v0, LX/7Pj;->j:LX/2WF;

    move-object v0, v1

    .line 1203188
    invoke-virtual {v0}, LX/2WF;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public final isRepeatable()Z
    .locals 1

    .prologue
    .line 1203189
    const/4 v0, 0x0

    return v0
.end method

.method public final isStreaming()Z
    .locals 1

    .prologue
    .line 1203190
    const/4 v0, 0x1

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 3

    .prologue
    .line 1203191
    :try_start_0
    iget-object v0, p0, LX/7Pk;->a:LX/7Pj;

    invoke-virtual {v0, p1}, LX/7Pj;->a(Ljava/io/OutputStream;)V
    :try_end_0
    .catch LX/7P1; {:try_start_0 .. :try_end_0} :catch_3
    .catch LX/7Pc; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/7PO; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1203192
    :goto_0
    return-void

    .line 1203193
    :catch_0
    move-exception v0

    .line 1203194
    const-class v1, Ljava/io/IOException;

    invoke-static {v0, v1}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 1203195
    new-instance v1, Ljava/io/IOException;

    const-string v2, "wtf"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1203196
    :catch_1
    goto :goto_0

    :catch_2
    goto :goto_0

    :catch_3
    goto :goto_0
.end method
