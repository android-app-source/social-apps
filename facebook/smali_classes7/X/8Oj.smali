.class public LX/8Oj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:F

.field public b:F

.field public c:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1340283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1340284
    iput v0, p0, LX/8Oj;->a:F

    .line 1340285
    iput v0, p0, LX/8Oj;->b:F

    .line 1340286
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/8Oj;->c:J

    return-void
.end method


# virtual methods
.method public final a(LX/8Ob;Ljava/lang/String;)LX/8O2;
    .locals 11

    .prologue
    .line 1340297
    new-instance v1, LX/8O2;

    iget-wide v2, p0, LX/8Oj;->c:J

    iget v4, p0, LX/8Oj;->b:F

    iget v5, p0, LX/8Oj;->a:F

    iget-wide v6, p1, LX/8Ob;->u:J

    iget-wide v8, p1, LX/8Ob;->v:J

    move-object v10, p2

    invoke-direct/range {v1 .. v10}, LX/8O2;-><init>(JFFJJLjava/lang/String;)V

    .line 1340298
    return-object v1
.end method

.method public final a(Lcom/facebook/photos/upload/operation/UploadPartitionInfo;Ljava/lang/String;)LX/8O2;
    .locals 11

    .prologue
    .line 1340295
    new-instance v1, LX/8O2;

    iget-wide v2, p0, LX/8Oj;->c:J

    iget v4, p0, LX/8Oj;->b:F

    iget v5, p0, LX/8Oj;->a:F

    iget-wide v6, p1, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadOffset:J

    iget-wide v8, p1, Lcom/facebook/photos/upload/operation/UploadPartitionInfo;->chunkedUploadChunkLength:J

    move-object v10, p2

    invoke-direct/range {v1 .. v10}, LX/8O2;-><init>(JFFJJLjava/lang/String;)V

    .line 1340296
    return-object v1
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 1340291
    iput-wide p1, p0, LX/8Oj;->c:J

    .line 1340292
    iget v0, p0, LX/8Oj;->a:F

    iput v0, p0, LX/8Oj;->b:F

    .line 1340293
    const/4 v0, 0x0

    iput v0, p0, LX/8Oj;->a:F

    .line 1340294
    return-void
.end method

.method public final a(JJJJ)V
    .locals 7

    .prologue
    .line 1340287
    iput-wide p1, p0, LX/8Oj;->c:J

    .line 1340288
    iget v0, p0, LX/8Oj;->a:F

    iput v0, p0, LX/8Oj;->b:F

    .line 1340289
    long-to-float v0, p3

    sub-long v2, p7, p5

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    long-to-float v1, v2

    div-float/2addr v0, v1

    iput v0, p0, LX/8Oj;->a:F

    .line 1340290
    return-void
.end method
