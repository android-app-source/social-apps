.class public final LX/6rC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Dk;


# instance fields
.field public a:LX/6qn;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1151789
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1151790
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/util/List;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/selector/model/OptionSelectorRow;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1151876
    const-string v0, "shipping_option"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported collectedDataKey found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1151877
    invoke-static {p1}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v0

    new-instance v1, LX/6rB;

    invoke-direct {v1}, LX/6rB;-><init>()V

    invoke-virtual {v0, v1}, LX/0wv;->a(LX/0QK;)LX/0wv;

    move-result-object v0

    invoke-virtual {v0}, LX/0wv;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/6rC;
    .locals 1

    .prologue
    .line 1151873
    new-instance v0, LX/6rC;

    invoke-direct {v0}, LX/6rC;-><init>()V

    .line 1151874
    move-object v0, v0

    .line 1151875
    return-object v0
.end method

.method public static b(LX/6rC;Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;LX/0Px;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/CheckoutData;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1151878
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->u()LX/0P1;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1151879
    if-eqz v0, :cond_0

    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    if-eq v1, v2, :cond_2

    :cond_0
    move v1, v3

    .line 1151880
    :goto_0
    move v0, v1

    .line 1151881
    if-eqz v0, :cond_1

    .line 1151882
    iget-object v0, p0, LX/6rC;->a:LX/6qn;

    invoke-interface {v0, p2, p3}, LX/6qn;->a(Ljava/lang/String;LX/0Px;)V

    .line 1151883
    :cond_1
    return-void

    .line 1151884
    :cond_2
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v7

    move v6, v4

    :goto_1
    if-ge v6, v7, :cond_5

    invoke-virtual {p3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    .line 1151885
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v8

    move v5, v4

    :goto_2
    if-ge v5, v8, :cond_4

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    .line 1151886
    iget-object p1, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    iget-object v2, v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v1, v3

    .line 1151887
    goto :goto_0

    .line 1151888
    :cond_3
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    .line 1151889
    :cond_4
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    :cond_5
    move v1, v4

    .line 1151890
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1151872
    return-void
.end method

.method public final a(LX/6qn;)V
    .locals 0

    .prologue
    .line 1151870
    iput-object p1, p0, LX/6rC;->a:LX/6qn;

    .line 1151871
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;IILandroid/content/Intent;)V
    .locals 7

    .prologue
    .line 1151791
    iget-object v0, p0, LX/6rC;->a:LX/6qn;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151792
    packed-switch p2, :pswitch_data_0

    .line 1151793
    :goto_0
    :pswitch_0
    return-void

    .line 1151794
    :pswitch_1
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 1151795
    const-string v0, "contact_info"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/model/NameContactInfo;

    .line 1151796
    iget-object p1, p0, LX/6rC;->a:LX/6qn;

    invoke-interface {p1, v0}, LX/6qn;->a(Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V

    .line 1151797
    :cond_0
    goto :goto_0

    .line 1151798
    :pswitch_2
    const/4 v0, -0x1

    if-ne p3, v0, :cond_1

    .line 1151799
    const-string v0, "contact_infos"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1151800
    iget-object p1, p0, LX/6rC;->a:LX/6qn;

    invoke-interface {p1, v0}, LX/6qn;->a(Ljava/util/List;)V

    .line 1151801
    :cond_1
    goto :goto_0

    .line 1151802
    :pswitch_3
    const/4 v0, -0x1

    if-ne p3, v0, :cond_2

    .line 1151803
    const-string v0, "contact_info"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1151804
    iget-object p1, p0, LX/6rC;->a:LX/6qn;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {p1, v0}, LX/6qn;->a(Ljava/util/List;)V

    .line 1151805
    :cond_2
    goto :goto_0

    .line 1151806
    :pswitch_4
    const/4 v0, -0x1

    if-ne p3, v0, :cond_4

    .line 1151807
    const-string v0, "extra_shipping_option_id"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1151808
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->k()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/ShippingOption;

    .line 1151809
    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingOption;->a()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_3

    .line 1151810
    iget-object p2, p0, LX/6rC;->a:LX/6qn;

    invoke-interface {p2, v0}, LX/6qn;->a(Lcom/facebook/payments/shipping/model/ShippingOption;)V

    .line 1151811
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1151812
    :cond_4
    goto :goto_0

    .line 1151813
    :pswitch_5
    const/4 v0, -0x1

    if-ne p3, v0, :cond_5

    .line 1151814
    const-string v0, "payments_picker_option_id"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1151815
    const-string v1, "collected_data_key"

    invoke-virtual {p4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1151816
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(Ljava/lang/String;)Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    move-result-object v2

    .line 1151817
    iget-object v2, v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->f:LX/0Px;

    invoke-static {v2}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v2

    new-instance v3, LX/6rA;

    invoke-direct {v3, p0, v0}, LX/6rA;-><init>(LX/6rC;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0wv;->a(LX/0Rl;)LX/0wv;

    move-result-object v0

    invoke-virtual {v0}, LX/0wv;->b()LX/0Px;

    move-result-object v0

    .line 1151818
    invoke-static {p0, p1, v1, v0}, LX/6rC;->b(LX/6rC;Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;LX/0Px;)V

    .line 1151819
    :cond_5
    goto/16 :goto_0

    .line 1151820
    :pswitch_6
    const/4 v0, -0x1

    if-ne p3, v0, :cond_8

    .line 1151821
    const-string v0, "extra_options"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1151822
    const-string v1, "extra_collected_data_key"

    invoke-virtual {p4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1151823
    const-string v2, "extra_new_options"

    invoke-virtual {p4, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1151824
    invoke-static {v1, v2}, LX/6rC;->a(Ljava/lang/String;Ljava/util/List;)LX/0Px;

    move-result-object v2

    .line 1151825
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1151826
    invoke-virtual {v3, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1151827
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(Ljava/lang/String;)Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->f:LX/0Px;

    invoke-virtual {v3, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1151828
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(Ljava/lang/String;)Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    move-result-object v4

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1151829
    invoke-static {v4}, LX/6rY;->a(Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)LX/6rY;

    move-result-object v5

    .line 1151830
    iput-object v3, v5, LX/6rY;->f:LX/0Px;

    .line 1151831
    move-object v5, v5

    .line 1151832
    invoke-virtual {v5}, LX/6rY;->a()Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    move-result-object v5

    move-object v5, v5

    .line 1151833
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 1151834
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    iget-object p2, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->v:LX/0Px;

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result p3

    .line 1151835
    const/4 v3, 0x0

    move v4, v3

    :goto_2
    if-ge v4, p3, :cond_7

    invoke-virtual {p2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    .line 1151836
    iget-object p4, v3, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p4

    if-nez p4, :cond_6

    .line 1151837
    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1151838
    :cond_6
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 1151839
    :cond_7
    invoke-virtual {v6, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1151840
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    invoke-static {v3}, LX/6qZ;->a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)LX/6qZ;

    move-result-object v3

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 1151841
    iput-object v4, v3, LX/6qZ;->w:LX/0Px;

    .line 1151842
    move-object v3, v3

    .line 1151843
    invoke-virtual {v3}, LX/6qZ;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    .line 1151844
    iget-object v4, p0, LX/6rC;->a:LX/6qn;

    invoke-interface {v4, v3}, LX/6qn;->a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    .line 1151845
    invoke-static {v1, v0}, LX/6rC;->a(Ljava/lang/String;Ljava/util/List;)LX/0Px;

    move-result-object v0

    invoke-static {p0, p1, v1, v0}, LX/6rC;->b(LX/6rC;Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;LX/0Px;)V

    .line 1151846
    :cond_8
    goto/16 :goto_0

    .line 1151847
    :pswitch_7
    const/4 v0, -0x1

    if-ne p3, v0, :cond_a

    .line 1151848
    const-string v0, "shipping_address"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1151849
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->h()LX/0am;

    move-result-object v1

    invoke-static {v1}, LX/47j;->a(LX/0am;)Z

    move-result v1

    if-nez v1, :cond_9

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/MailingAddress;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->h()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/MailingAddress;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1151850
    :cond_9
    iget-object v1, p0, LX/6rC;->a:LX/6qn;

    invoke-interface {v1, v0}, LX/6qn;->a(Lcom/facebook/payments/shipping/model/MailingAddress;)V

    .line 1151851
    :cond_a
    goto/16 :goto_0

    .line 1151852
    :pswitch_8
    const/4 v0, -0x1

    if-ne p3, v0, :cond_b

    .line 1151853
    const-string v0, "selected_payment_method"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 1151854
    if-eqz v0, :cond_b

    .line 1151855
    iget-object v1, p0, LX/6rC;->a:LX/6qn;

    invoke-interface {v1, v0}, LX/6qn;->a(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V

    .line 1151856
    :cond_b
    goto/16 :goto_0

    .line 1151857
    :pswitch_9
    const/4 v0, -0x1

    if-ne p3, v0, :cond_c

    .line 1151858
    const-string v0, "extra_note"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1151859
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->w:Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    iget-object v1, v1, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 1151860
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    invoke-static {v2}, LX/6qZ;->a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)LX/6qZ;

    move-result-object v2

    new-instance v3, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    invoke-virtual {v1, v0}, Lcom/facebook/payments/form/model/FormFieldAttributes;->a(Ljava/lang/String;)Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;-><init>(Lcom/facebook/payments/form/model/FormFieldAttributes;)V

    .line 1151861
    iput-object v3, v2, LX/6qZ;->x:Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    .line 1151862
    move-object v0, v2

    .line 1151863
    invoke-virtual {v0}, LX/6qZ;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    .line 1151864
    iget-object v1, p0, LX/6rC;->a:LX/6qn;

    invoke-interface {v1, v0}, LX/6qn;->a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    .line 1151865
    :cond_c
    goto/16 :goto_0

    .line 1151866
    :pswitch_a
    const/4 v0, -0x1

    if-ne p3, v0, :cond_d

    .line 1151867
    const-string v0, "extra_currency_amount"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1151868
    iget-object v1, p0, LX/6rC;->a:LX/6qn;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v0}, LX/6qn;->a(Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)V

    .line 1151869
    :cond_d
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_8
        :pswitch_8
        :pswitch_4
        :pswitch_7
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method
