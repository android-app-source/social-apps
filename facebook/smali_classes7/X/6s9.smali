.class public LX/6s9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6rr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6rr",
        "<",
        "Lcom/facebook/payments/checkout/configuration/model/PaymentMethodCheckoutPurchaseInfoExtension;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1152946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152947
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1152948
    const-string v0, "identifier"

    invoke-virtual {p2, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6rc;->forValue(Ljava/lang/String;)LX/6rc;

    move-result-object v0

    .line 1152949
    sget-object v1, LX/6rc;->PAYMENT_METHOD:LX/6rc;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152950
    new-instance v0, Lcom/facebook/payments/checkout/configuration/model/PaymentMethodCheckoutPurchaseInfoExtension;

    const-string v1, "allow_change_billing_country"

    invoke-virtual {p2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->g(LX/0lF;)Z

    move-result v1

    invoke-direct {v0, v1}, Lcom/facebook/payments/checkout/configuration/model/PaymentMethodCheckoutPurchaseInfoExtension;-><init>(Z)V

    return-object v0

    .line 1152951
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
