.class public LX/8Yj;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1356654
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 1356655
    return-void
.end method

.method public static a(Landroid/content/Context;LX/1GQ;LX/0pi;LX/0pq;LX/1Ft;)LX/1Ha;
    .locals 12
    .annotation runtime Lcom/facebook/inject/ContextScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation build Lcom/facebook/richdocument/fonts/FontFileCache;
    .end annotation

    .prologue
    .line 1356656
    new-instance v1, LX/1Hj;

    const-wide/32 v2, 0x40000

    const-wide/32 v4, 0x500000

    const-wide/32 v6, 0xa00000

    invoke-direct/range {v1 .. v7}, LX/1Hj;-><init>(JJJ)V

    .line 1356657
    new-instance v3, LX/1Hg;

    const/4 v0, 0x1

    new-instance v2, LX/8Yi;

    invoke-direct {v2, p0}, LX/8Yi;-><init>(Landroid/content/Context;)V

    const-string v4, "custom_fonts"

    invoke-direct {v3, v0, v2, v4, p1}, LX/1Hg;-><init>(ILX/1Gd;Ljava/lang/String;LX/1GQ;)V

    .line 1356658
    new-instance v2, LX/1Hk;

    new-instance v4, LX/1Gh;

    invoke-direct {v4}, LX/1Gh;-><init>()V

    const-string v0, "custom_fonts_file"

    invoke-virtual {p2, v0}, LX/0pi;->a(Ljava/lang/String;)LX/1GE;

    move-result-object v6

    invoke-interface/range {p4 .. p4}, LX/1Ft;->a()Ljava/util/concurrent/Executor;

    move-result-object v10

    const/4 v11, 0x0

    move-object v5, v1

    move-object v7, p1

    move-object v8, p3

    move-object v9, p0

    invoke-direct/range {v2 .. v11}, LX/1Hk;-><init>(LX/1Hh;LX/1GU;LX/1Hj;LX/1GE;LX/1GQ;LX/0pr;Landroid/content/Context;Ljava/util/concurrent/Executor;Z)V

    return-object v2
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 1356659
    return-void
.end method
