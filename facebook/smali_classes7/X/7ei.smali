.class public final LX/7ei;
.super LX/7eh;
.source ""


# instance fields
.field public final synthetic d:Landroid/app/PendingIntent;

.field public final synthetic e:LX/7e6;

.field public final synthetic f:LX/7ej;


# direct methods
.method public constructor <init>(LX/7ej;LX/2wX;Landroid/app/PendingIntent;LX/7e6;)V
    .locals 0

    iput-object p1, p0, LX/7ei;->f:LX/7ej;

    iput-object p3, p0, LX/7ei;->d:Landroid/app/PendingIntent;

    iput-object p4, p0, LX/7ei;->e:LX/7e6;

    invoke-direct {p0, p2}, LX/7eh;-><init>(LX/2wX;)V

    return-void
.end method


# virtual methods
.method public final b(LX/2wK;)V
    .locals 12

    check-cast p1, LX/7ef;

    iget-object v0, p0, LX/7ei;->d:Landroid/app/PendingIntent;

    iget-object v1, p0, LX/7ei;->e:LX/7e6;

    const/4 v3, 0x0

    new-instance v2, Lcom/google/android/gms/nearby/messages/internal/SubscribeRequest;

    iget-object v4, v1, LX/7e6;->c:Lcom/google/android/gms/nearby/messages/Strategy;

    move-object v4, v4

    new-instance v5, LX/7em;

    invoke-direct {v5, p0}, LX/7em;-><init>(LX/2wh;)V

    move-object v5, v5

    iget-object v6, v1, LX/7e6;->d:Lcom/google/android/gms/nearby/messages/MessageFilter;

    move-object v6, v6

    const/4 v8, 0x0

    iget-object v7, v1, LX/7e6;->e:LX/7e4;

    move-object v7, v7

    if-nez v7, :cond_0

    const/4 v9, 0x0

    :goto_0
    move-object v10, v9

    iget-boolean v11, v1, LX/7e6;->b:Z

    move-object v7, v0

    move-object v9, v3

    invoke-direct/range {v2 .. v11}, Lcom/google/android/gms/nearby/messages/internal/SubscribeRequest;-><init>(Landroid/os/IBinder;Lcom/google/android/gms/nearby/messages/Strategy;Landroid/os/IBinder;Lcom/google/android/gms/nearby/messages/MessageFilter;Landroid/app/PendingIntent;I[BLandroid/os/IBinder;Z)V

    invoke-virtual {p1}, LX/2wI;->m()Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, LX/7eP;

    invoke-interface {v3, v2}, LX/7eP;->a(Lcom/google/android/gms/nearby/messages/internal/SubscribeRequest;)V

    return-void

    :cond_0
    new-instance v9, LX/7ee;

    invoke-direct {v9, v7}, LX/7ee;-><init>(LX/7e4;)V

    goto :goto_0
.end method
