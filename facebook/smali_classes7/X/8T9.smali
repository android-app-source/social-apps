.class public LX/8T9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1348078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1348079
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1348080
    iput-object v0, p0, LX/8T9;->a:LX/0Ot;

    .line 1348081
    return-void
.end method

.method public static a(LX/0QB;)LX/8T9;
    .locals 4

    .prologue
    .line 1348113
    const-class v1, LX/8T9;

    monitor-enter v1

    .line 1348114
    :try_start_0
    sget-object v0, LX/8T9;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1348115
    sput-object v2, LX/8T9;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1348116
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1348117
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1348118
    new-instance v3, LX/8T9;

    invoke-direct {v3}, LX/8T9;-><init>()V

    .line 1348119
    const-class p0, Landroid/content/Context;

    invoke-interface {v0, p0}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object p0

    .line 1348120
    iput-object p0, v3, LX/8T9;->a:LX/0Ot;

    .line 1348121
    move-object v0, v3

    .line 1348122
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1348123
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/8T9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1348124
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1348125
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/8Tp;)V
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 1348082
    sget-object v0, LX/8T8;->a:[I

    invoke-virtual {p1}, LX/8Tp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move v1, v2

    move v3, v2

    .line 1348083
    :goto_0
    if-eq v3, v2, :cond_0

    if-eq v1, v2, :cond_0

    .line 1348084
    iget-object v0, p0, LX/8T9;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/quicksilver/QuicksilverActivity;

    if-eqz v0, :cond_0

    .line 1348085
    iget-object v0, p0, LX/8T9;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicksilver/QuicksilverActivity;

    invoke-virtual {v0, v3, v1}, Lcom/facebook/quicksilver/QuicksilverActivity;->a(II)V

    .line 1348086
    :cond_0
    return-void

    .line 1348087
    :pswitch_0
    const v1, 0x7f082374

    .line 1348088
    const v0, 0x7f0a0719

    move v3, v1

    move v1, v0

    .line 1348089
    goto :goto_0

    .line 1348090
    :pswitch_1
    const v1, 0x7f082375

    .line 1348091
    const v0, 0x7f0a0719

    move v3, v1

    move v1, v0

    .line 1348092
    goto :goto_0

    .line 1348093
    :pswitch_2
    const v1, 0x7f082376

    .line 1348094
    const v0, 0x7f0a0705

    move v3, v1

    move v1, v0

    .line 1348095
    goto :goto_0

    .line 1348096
    :pswitch_3
    const v1, 0x7f082378

    .line 1348097
    const v0, 0x7f0a0705

    move v3, v1

    move v1, v0

    .line 1348098
    goto :goto_0

    .line 1348099
    :pswitch_4
    const v1, 0x7f082379

    .line 1348100
    const v0, 0x7f0a0705

    move v3, v1

    move v1, v0

    .line 1348101
    goto :goto_0

    .line 1348102
    :pswitch_5
    const v1, 0x7f082377

    .line 1348103
    const v0, 0x7f0a0705

    move v3, v1

    move v1, v0

    .line 1348104
    goto :goto_0

    .line 1348105
    :pswitch_6
    const v1, 0x7f08237b

    .line 1348106
    const v0, 0x7f0a0705

    move v3, v1

    move v1, v0

    .line 1348107
    goto :goto_0

    .line 1348108
    :pswitch_7
    const v1, 0x7f08237a

    .line 1348109
    const v0, 0x7f0a0719

    move v3, v1

    move v1, v0

    .line 1348110
    goto :goto_0

    .line 1348111
    :pswitch_8
    const v1, 0x7f08237c

    .line 1348112
    const v0, 0x7f0a0719

    move v3, v1

    move v1, v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
