.class public LX/71p;
.super Lcom/facebook/payments/ui/PaymentsComponentViewGroup;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/6vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/payments/ui/PaymentsComponentViewGroup;",
        "Landroid/view/View$OnClickListener;",
        "LX/6vq",
        "<",
        "LX/71o;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/71o;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1163696
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;)V

    .line 1163697
    const v0, 0x7f031328

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1163698
    const v0, 0x7f0d2c71

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1163699
    invoke-virtual {p0}, LX/71p;->getContext()Landroid/content/Context;

    move-result-object v1

    const p1, 0x7f081e65

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1163700
    return-void
.end method


# virtual methods
.method public onClick()V
    .locals 0

    .prologue
    .line 1163701
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x217fcedc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1163702
    invoke-virtual {p0}, LX/71p;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/71p;->a:LX/71o;

    iget-object v2, v2, LX/71o;->a:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-static {v1, v2}, Lcom/facebook/payments/shipping/form/ShippingAddressActivity;->a(Landroid/content/Context;Lcom/facebook/payments/shipping/model/ShippingParams;)Landroid/content/Intent;

    move-result-object v1

    .line 1163703
    const/16 v2, 0x65

    invoke-virtual {p0, v1, v2}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a(Landroid/content/Intent;I)V

    .line 1163704
    const v1, 0x18180666

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
