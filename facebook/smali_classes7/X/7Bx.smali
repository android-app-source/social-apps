.class public LX/7Bx;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/7Bx;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1179690
    const-string v6, "search_bootstrap"

    const/16 v7, 0x23

    new-instance v0, LX/7Bm;

    invoke-direct {v0}, LX/7Bm;-><init>()V

    new-instance v1, LX/7Bs;

    invoke-direct {v1}, LX/7Bs;-><init>()V

    new-instance v2, LX/7Bo;

    invoke-direct {v2}, LX/7Bo;-><init>()V

    new-instance v3, LX/7Bq;

    invoke-direct {v3}, LX/7Bq;-><init>()V

    new-instance v4, LX/7Bw;

    invoke-direct {v4}, LX/7Bw;-><init>()V

    new-instance v5, LX/7Bu;

    invoke-direct {v5}, LX/7Bu;-><init>()V

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, v6, v7, v0}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 1179691
    iput-object p1, p0, LX/7Bx;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1179692
    return-void
.end method

.method public static a(LX/0QB;)LX/7Bx;
    .locals 4

    .prologue
    .line 1179677
    sget-object v0, LX/7Bx;->b:LX/7Bx;

    if-nez v0, :cond_1

    .line 1179678
    const-class v1, LX/7Bx;

    monitor-enter v1

    .line 1179679
    :try_start_0
    sget-object v0, LX/7Bx;->b:LX/7Bx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1179680
    if-eqz v2, :cond_0

    .line 1179681
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1179682
    new-instance p0, LX/7Bx;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/7Bx;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1179683
    move-object v0, p0

    .line 1179684
    sput-object v0, LX/7Bx;->b:LX/7Bx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1179685
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1179686
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1179687
    :cond_1
    sget-object v0, LX/7Bx;->b:LX/7Bx;

    return-object v0

    .line 1179688
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1179689
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    .prologue
    .line 1179674
    invoke-super {p0, p1, p2, p3}, LX/0Tv;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 1179675
    iget-object v0, p0, LX/7Bx;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3Qm;->b:LX/0Tn;

    sget-object v2, LX/3Qm;->c:LX/0Tn;

    invoke-static {v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;)V

    .line 1179676
    return-void
.end method
