.class public LX/7RU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/media/AudioRecord;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private e:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field public f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/7RV;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/concurrent/ExecutorService;

.field public h:LX/1bJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1206820
    const-class v0, LX/7RU;

    sput-object v0, LX/7RU;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 1

    .prologue
    .line 1206816
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1206817
    iput-object p1, p0, LX/7RU;->g:Ljava/util/concurrent/ExecutorService;

    .line 1206818
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/7RU;->f:Ljava/util/ArrayList;

    .line 1206819
    return-void
.end method

.method public static b([BI)D
    .locals 8

    .prologue
    .line 1206810
    const-wide/16 v2, 0x0

    .line 1206811
    int-to-double v4, p1

    .line 1206812
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 1206813
    aget-byte v1, p0, v0

    aget-byte v6, p0, v0

    mul-int/2addr v1, v6

    int-to-double v6, v1

    div-double/2addr v6, v4

    add-double/2addr v2, v6

    .line 1206814
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1206815
    :cond_0
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 7

    .prologue
    .line 1206804
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7RU;->a:Landroid/media/AudioRecord;

    if-nez v0, :cond_0

    .line 1206805
    new-instance v1, Landroid/media/AudioRecord;

    sget v2, LX/7Rd;->a:I

    sget v3, LX/7Rd;->b:I

    sget v4, LX/7Rd;->c:I

    sget v5, LX/7Rd;->d:I

    sget v6, LX/7Rd;->e:I

    invoke-direct/range {v1 .. v6}, Landroid/media/AudioRecord;-><init>(IIIII)V

    .line 1206806
    move-object v0, v1

    .line 1206807
    iput-object v0, p0, LX/7RU;->a:Landroid/media/AudioRecord;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1206808
    :cond_0
    monitor-exit p0

    return-void

    .line 1206809
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/7RV;)Z
    .locals 1

    .prologue
    .line 1206803
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7RU;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 1206799
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/7RU;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1206800
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/7RU;->d:Ljava/util/concurrent/atomic/AtomicBoolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1206801
    monitor-exit p0

    return-void

    .line 1206802
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/7RV;)Z
    .locals 1

    .prologue
    .line 1206763
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7RU;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 1206793
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7RU;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-eqz v0, :cond_0

    .line 1206794
    iget-object v0, p0, LX/7RU;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1206795
    :cond_0
    iget-object v0, p0, LX/7RU;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-eqz v0, :cond_1

    .line 1206796
    iget-object v0, p0, LX/7RU;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1206797
    :cond_1
    monitor-exit p0

    return-void

    .line 1206798
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 1

    .prologue
    .line 1206786
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7RU;->a:Landroid/media/AudioRecord;

    if-eqz v0, :cond_1

    .line 1206787
    iget-object v0, p0, LX/7RU;->a:Landroid/media/AudioRecord;

    .line 1206788
    if-eqz v0, :cond_0

    .line 1206789
    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 1206790
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/7RU;->a:Landroid/media/AudioRecord;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1206791
    :cond_1
    monitor-exit p0

    return-void

    .line 1206792
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 2

    .prologue
    .line 1206782
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/7RU;->f()V

    .line 1206783
    iget-object v0, p0, LX/7RU;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1206784
    monitor-exit p0

    return-void

    .line 1206785
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .locals 3

    .prologue
    .line 1206772
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7RU;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/7RU;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 1206773
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1206774
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/7RU;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1206775
    invoke-virtual {p0}, LX/7RU;->a()V

    .line 1206776
    iget-object v0, p0, LX/7RU;->a:Landroid/media/AudioRecord;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/7RU;->a:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    if-nez v0, :cond_2

    .line 1206777
    invoke-virtual {p0}, LX/7RU;->d()V

    .line 1206778
    invoke-virtual {p0}, LX/7RU;->a()V

    .line 1206779
    :cond_2
    iget-object v0, p0, LX/7RU;->a:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    .line 1206780
    iget-object v0, p0, LX/7RU;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/video/videostreaming/LiveStreamingAudioRecorder$1;

    invoke-direct {v1, p0}, Lcom/facebook/video/videostreaming/LiveStreamingAudioRecorder$1;-><init>(LX/7RU;)V

    const v2, 0xdf281ec

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, LX/7RU;->e:Ljava/util/concurrent/Future;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1206781
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()V
    .locals 3

    .prologue
    .line 1206764
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/7RU;->e:Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1206765
    :try_start_1
    iget-object v0, p0, LX/7RU;->e:Ljava/util/concurrent/Future;

    const v1, -0x30c9490c

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1206766
    :cond_0
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, LX/7RU;->e:Ljava/util/concurrent/Future;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1206767
    monitor-exit p0

    return-void

    .line 1206768
    :catch_0
    move-exception v0

    .line 1206769
    :goto_1
    :try_start_3
    sget-object v1, LX/7RU;->b:Ljava/lang/Class;

    const-string v2, "Ran into an exception while draining audio"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1206770
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1206771
    :catch_1
    move-exception v0

    goto :goto_1
.end method
