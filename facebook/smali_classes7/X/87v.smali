.class public LX/87v;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1300743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300744
    iput-object p1, p0, LX/87v;->a:LX/0tX;

    .line 1300745
    iput-object p2, p0, LX/87v;->b:Ljava/util/concurrent/Executor;

    .line 1300746
    iput-object p3, p0, LX/87v;->c:Ljava/util/concurrent/Executor;

    .line 1300747
    return-void
.end method

.method public static b(LX/0QB;)LX/87v;
    .locals 4

    .prologue
    .line 1300748
    new-instance v3, LX/87v;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-direct {v3, v0, v1, v2}, LX/87v;-><init>(LX/0tX;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    .line 1300749
    return-object v3
.end method


# virtual methods
.method public final a(ZLX/87u;)V
    .locals 9
    .param p2    # LX/87u;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1300750
    if-eqz p1, :cond_0

    sget-object v0, LX/0zS;->d:LX/0zS;

    :goto_0
    new-instance v1, LX/87s;

    invoke-direct {v1, p0, p2}, LX/87s;-><init>(LX/87v;LX/87u;)V

    .line 1300751
    new-instance v5, LX/88P;

    invoke-direct {v5}, LX/88P;-><init>()V

    move-object v5, v5

    .line 1300752
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    const-wide/16 v7, 0x384

    invoke-virtual {v5, v7, v8}, LX/0zO;->a(J)LX/0zO;

    move-result-object v5

    const/4 v6, 0x1

    .line 1300753
    iput-boolean v6, v5, LX/0zO;->p:Z

    .line 1300754
    move-object v5, v5

    .line 1300755
    iget-object v6, p0, LX/87v;->a:LX/0tX;

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    move-object v2, v5

    .line 1300756
    new-instance v3, LX/87t;

    invoke-direct {v3, p0, v1}, LX/87t;-><init>(LX/87v;LX/0TF;)V

    iget-object v4, p0, LX/87v;->c:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1300757
    return-void

    .line 1300758
    :cond_0
    sget-object v0, LX/0zS;->a:LX/0zS;

    goto :goto_0
.end method
