.class public final LX/7XT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1218373
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1218374
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 1218375
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const/4 p3, 0x0

    .line 1218376
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1218377
    invoke-virtual {p0, v1, p3}, LX/15i;->g(II)I

    move-result v2

    .line 1218378
    if-eqz v2, :cond_0

    .line 1218379
    const-string v2, "friendly_names"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218380
    invoke-virtual {p0, v1, p3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1218381
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1218382
    if-eqz v2, :cond_1

    .line 1218383
    const-string p3, "matcher"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218384
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1218385
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1218386
    if-eqz v2, :cond_2

    .line 1218387
    const-string p3, "replacer"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1218388
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1218389
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1218390
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1218391
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1218392
    return-void
.end method
