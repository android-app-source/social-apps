.class public final LX/7ya;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/7yc;

.field public final b:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public final c:J


# direct methods
.method public constructor <init>(LX/7yc;Lcom/google/common/util/concurrent/SettableFuture;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;>;>;J)V"
        }
    .end annotation

    .prologue
    .line 1279745
    iput-object p1, p0, LX/7ya;->a:LX/7yc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1279746
    iput-object p2, p0, LX/7ya;->b:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1279747
    iput-wide p3, p0, LX/7ya;->c:J

    .line 1279748
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/base/tagging/FaceBox;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1279749
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1279750
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1279751
    invoke-virtual {v0}, Lcom/facebook/photos/base/tagging/FaceBox;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1279752
    :cond_0
    iget-object v0, p0, LX/7ya;->a:LX/7yc;

    iget-object v0, v0, LX/7yc;->d:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/facerec/manager/FaceRecManager$TagSuggestFetchCompletedListenerImpl$1;

    invoke-direct {v2, p0, v1}, Lcom/facebook/facerec/manager/FaceRecManager$TagSuggestFetchCompletedListenerImpl$1;-><init>(LX/7ya;Ljava/util/List;)V

    const v1, -0x1ed6b89a

    invoke-static {v0, v2, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1279753
    return-void
.end method
