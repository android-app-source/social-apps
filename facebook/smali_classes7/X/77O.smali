.class public LX/77O;
.super LX/2g7;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/77O;


# instance fields
.field private final a:Landroid/net/ConnectivityManager;

.field public final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/net/ConnectivityManager;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171793
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1171794
    iput-object p1, p0, LX/77O;->a:Landroid/net/ConnectivityManager;

    .line 1171795
    iput-object p2, p0, LX/77O;->b:Landroid/content/Context;

    .line 1171796
    return-void
.end method

.method public static a(LX/0QB;)LX/77O;
    .locals 5

    .prologue
    .line 1171768
    sget-object v0, LX/77O;->c:LX/77O;

    if-nez v0, :cond_1

    .line 1171769
    const-class v1, LX/77O;

    monitor-enter v1

    .line 1171770
    :try_start_0
    sget-object v0, LX/77O;->c:LX/77O;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1171771
    if-eqz v2, :cond_0

    .line 1171772
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1171773
    new-instance p0, LX/77O;

    invoke-static {v0}, LX/0nI;->b(LX/0QB;)Landroid/net/ConnectivityManager;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {p0, v3, v4}, LX/77O;-><init>(Landroid/net/ConnectivityManager;Landroid/content/Context;)V

    .line 1171774
    move-object v0, p0

    .line 1171775
    sput-object v0, LX/77O;->c:LX/77O;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1171776
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1171777
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1171778
    :cond_1
    sget-object v0, LX/77O;->c:LX/77O;

    return-object v0

    .line 1171779
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1171780
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 5
    .param p1    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1171781
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1171782
    iget-object v0, p0, LX/77O;->a:Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 1171783
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 1171784
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p1, 0x11

    if-ge v4, p1, :cond_4

    .line 1171785
    iget-object v4, p0, LX/77O;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string p1, "airplane_mode_on"

    invoke-static {v4, p1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_3

    .line 1171786
    :cond_0
    :goto_0
    move v0, v0

    .line 1171787
    if-nez v0, :cond_1

    move v0, v1

    .line 1171788
    :goto_1
    iget-object v3, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    if-ne v0, v3, :cond_2

    :goto_2
    return v1

    :cond_1
    move v0, v2

    .line 1171789
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1171790
    goto :goto_2

    :cond_3
    move v0, v3

    .line 1171791
    goto :goto_0

    .line 1171792
    :cond_4
    iget-object v4, p0, LX/77O;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string p1, "airplane_mode_on"

    invoke-static {v4, p1, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-nez v4, :cond_0

    move v0, v3

    goto :goto_0
.end method
