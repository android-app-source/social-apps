.class public final LX/6yI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V
    .locals 0

    .prologue
    .line 1159666
    iput-object p1, p0, LX/6yI;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 1159654
    iget-object v0, p0, LX/6yI;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    if-eqz v0, :cond_0

    .line 1159655
    iget-object v0, p0, LX/6yI;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    invoke-virtual {v0}, LX/6y3;->a()V

    .line 1159656
    :cond_0
    iget-object v0, p0, LX/6yI;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->r:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    .line 1159657
    iget-object v1, p0, LX/6yI;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->e:LX/6zC;

    iget-object v2, p0, LX/6yI;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-static {v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)LX/6zD;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6zC;->a(LX/6z8;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1159658
    iget-object v0, p0, LX/6yI;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->k:LX/73a;

    iget-object v1, p0, LX/6yI;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->u:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v1}, LX/73a;->a(Landroid/view/View;)V

    .line 1159659
    :goto_0
    iget-object v0, p0, LX/6yI;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    iget-object v1, p0, LX/6yI;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-virtual {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/6y3;->a(Z)V

    .line 1159660
    return-void

    .line 1159661
    :cond_1
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    .line 1159662
    invoke-static {v0}, LX/6yU;->a(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v2

    invoke-static {v2}, LX/6zC;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)I

    move-result v2

    move v0, v2

    .line 1159663
    if-ge v1, v0, :cond_2

    .line 1159664
    iget-object v0, p0, LX/6yI;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    goto :goto_0

    .line 1159665
    :cond_2
    iget-object v0, p0, LX/6yI;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1159667
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1159653
    return-void
.end method
