.class public LX/6me;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;


# instance fields
.field public final batchId:Ljava/lang/Long;

.field public final fbTraceMeta:Ljava/lang/String;

.field public final responses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6md;",
            ">;"
        }
    .end annotation
.end field

.field public final supported:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 1146053
    new-instance v0, LX/1sv;

    const-string v1, "SendMessageResponseBatch"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/6me;->b:LX/1sv;

    .line 1146054
    new-instance v0, LX/1sw;

    const-string v1, "batchId"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6me;->c:LX/1sw;

    .line 1146055
    new-instance v0, LX/1sw;

    const-string v1, "fbTraceMeta"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6me;->d:LX/1sw;

    .line 1146056
    new-instance v0, LX/1sw;

    const-string v1, "responses"

    const/16 v2, 0xf

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6me;->e:LX/1sw;

    .line 1146057
    new-instance v0, LX/1sw;

    const-string v1, "supported"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/6me;->f:LX/1sw;

    .line 1146058
    sput-boolean v4, LX/6me;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/6md;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1146059
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1146060
    iput-object p1, p0, LX/6me;->batchId:Ljava/lang/Long;

    .line 1146061
    iput-object p2, p0, LX/6me;->fbTraceMeta:Ljava/lang/String;

    .line 1146062
    iput-object p3, p0, LX/6me;->responses:Ljava/util/List;

    .line 1146063
    iput-object p4, p0, LX/6me;->supported:Ljava/lang/Boolean;

    .line 1146064
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1146065
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1146066
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v1, v0

    .line 1146067
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1146068
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SendMessageResponseBatch"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1146069
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146070
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146071
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146072
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146073
    const-string v4, "batchId"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146074
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146075
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146076
    iget-object v4, p0, LX/6me;->batchId:Ljava/lang/Long;

    if-nez v4, :cond_6

    .line 1146077
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146078
    :goto_3
    iget-object v4, p0, LX/6me;->fbTraceMeta:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 1146079
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146080
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146081
    const-string v4, "fbTraceMeta"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146082
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146083
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146084
    iget-object v4, p0, LX/6me;->fbTraceMeta:Ljava/lang/String;

    if-nez v4, :cond_7

    .line 1146085
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146086
    :cond_0
    :goto_4
    iget-object v4, p0, LX/6me;->responses:Ljava/util/List;

    if-eqz v4, :cond_1

    .line 1146087
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146088
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146089
    const-string v4, "responses"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146090
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146091
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146092
    iget-object v4, p0, LX/6me;->responses:Ljava/util/List;

    if-nez v4, :cond_8

    .line 1146093
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146094
    :cond_1
    :goto_5
    iget-object v4, p0, LX/6me;->supported:Ljava/lang/Boolean;

    if-eqz v4, :cond_2

    .line 1146095
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146096
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146097
    const-string v4, "supported"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146098
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146099
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146100
    iget-object v0, p0, LX/6me;->supported:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    .line 1146101
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146102
    :cond_2
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146103
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1146104
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1146105
    :cond_3
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 1146106
    :cond_4
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 1146107
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1146108
    :cond_6
    iget-object v4, p0, LX/6me;->batchId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 1146109
    :cond_7
    iget-object v4, p0, LX/6me;->fbTraceMeta:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 1146110
    :cond_8
    iget-object v4, p0, LX/6me;->responses:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 1146111
    :cond_9
    iget-object v0, p0, LX/6me;->supported:Ljava/lang/Boolean;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1146112
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1146113
    iget-object v0, p0, LX/6me;->batchId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1146114
    sget-object v0, LX/6me;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146115
    iget-object v0, p0, LX/6me;->batchId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1146116
    :cond_0
    iget-object v0, p0, LX/6me;->fbTraceMeta:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1146117
    iget-object v0, p0, LX/6me;->fbTraceMeta:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1146118
    sget-object v0, LX/6me;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146119
    iget-object v0, p0, LX/6me;->fbTraceMeta:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 1146120
    :cond_1
    iget-object v0, p0, LX/6me;->responses:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 1146121
    iget-object v0, p0, LX/6me;->responses:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 1146122
    sget-object v0, LX/6me;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146123
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/6me;->responses:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1146124
    iget-object v0, p0, LX/6me;->responses:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6md;

    .line 1146125
    invoke-virtual {v0, p1}, LX/6md;->a(LX/1su;)V

    goto :goto_0

    .line 1146126
    :cond_2
    iget-object v0, p0, LX/6me;->supported:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1146127
    iget-object v0, p0, LX/6me;->supported:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1146128
    sget-object v0, LX/6me;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1146129
    iget-object v0, p0, LX/6me;->supported:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 1146130
    :cond_3
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1146131
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1146132
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1146133
    if-nez p1, :cond_1

    .line 1146134
    :cond_0
    :goto_0
    return v0

    .line 1146135
    :cond_1
    instance-of v1, p1, LX/6me;

    if-eqz v1, :cond_0

    .line 1146136
    check-cast p1, LX/6me;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1146137
    if-nez p1, :cond_3

    .line 1146138
    :cond_2
    :goto_1
    move v0, v2

    .line 1146139
    goto :goto_0

    .line 1146140
    :cond_3
    iget-object v0, p0, LX/6me;->batchId:Ljava/lang/Long;

    if-eqz v0, :cond_c

    move v0, v1

    .line 1146141
    :goto_2
    iget-object v3, p1, LX/6me;->batchId:Ljava/lang/Long;

    if-eqz v3, :cond_d

    move v3, v1

    .line 1146142
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1146143
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146144
    iget-object v0, p0, LX/6me;->batchId:Ljava/lang/Long;

    iget-object v3, p1, LX/6me;->batchId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1146145
    :cond_5
    iget-object v0, p0, LX/6me;->fbTraceMeta:Ljava/lang/String;

    if-eqz v0, :cond_e

    move v0, v1

    .line 1146146
    :goto_4
    iget-object v3, p1, LX/6me;->fbTraceMeta:Ljava/lang/String;

    if-eqz v3, :cond_f

    move v3, v1

    .line 1146147
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1146148
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146149
    iget-object v0, p0, LX/6me;->fbTraceMeta:Ljava/lang/String;

    iget-object v3, p1, LX/6me;->fbTraceMeta:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1146150
    :cond_7
    iget-object v0, p0, LX/6me;->responses:Ljava/util/List;

    if-eqz v0, :cond_10

    move v0, v1

    .line 1146151
    :goto_6
    iget-object v3, p1, LX/6me;->responses:Ljava/util/List;

    if-eqz v3, :cond_11

    move v3, v1

    .line 1146152
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 1146153
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146154
    iget-object v0, p0, LX/6me;->responses:Ljava/util/List;

    iget-object v3, p1, LX/6me;->responses:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1146155
    :cond_9
    iget-object v0, p0, LX/6me;->supported:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    move v0, v1

    .line 1146156
    :goto_8
    iget-object v3, p1, LX/6me;->supported:Ljava/lang/Boolean;

    if-eqz v3, :cond_13

    move v3, v1

    .line 1146157
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 1146158
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1146159
    iget-object v0, p0, LX/6me;->supported:Ljava/lang/Boolean;

    iget-object v3, p1, LX/6me;->supported:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_b
    move v2, v1

    .line 1146160
    goto :goto_1

    :cond_c
    move v0, v2

    .line 1146161
    goto :goto_2

    :cond_d
    move v3, v2

    .line 1146162
    goto :goto_3

    :cond_e
    move v0, v2

    .line 1146163
    goto :goto_4

    :cond_f
    move v3, v2

    .line 1146164
    goto :goto_5

    :cond_10
    move v0, v2

    .line 1146165
    goto :goto_6

    :cond_11
    move v3, v2

    .line 1146166
    goto :goto_7

    :cond_12
    move v0, v2

    .line 1146167
    goto :goto_8

    :cond_13
    move v3, v2

    .line 1146168
    goto :goto_9
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1146169
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1146170
    sget-boolean v0, LX/6me;->a:Z

    .line 1146171
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/6me;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1146172
    return-object v0
.end method
