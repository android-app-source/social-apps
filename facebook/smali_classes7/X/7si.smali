.class public final LX/7si;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1265601
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1265602
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1265603
    :goto_0
    return v1

    .line 1265604
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1265605
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1265606
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1265607
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1265608
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1265609
    const-string v4, "additional_charges"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1265610
    invoke-static {p0, p1}, LX/7sL;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1265611
    :cond_2
    const-string v4, "seating_map_image"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1265612
    invoke-static {p0, p1}, LX/7sh;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1265613
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1265614
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1265615
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1265616
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1265617
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1265618
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1265619
    if-eqz v0, :cond_0

    .line 1265620
    const-string v1, "additional_charges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265621
    invoke-static {p0, v0, p2, p3}, LX/7sL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1265622
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1265623
    if-eqz v0, :cond_1

    .line 1265624
    const-string v1, "seating_map_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1265625
    invoke-static {p0, v0, p2}, LX/7sh;->a(LX/15i;ILX/0nX;)V

    .line 1265626
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1265627
    return-void
.end method
