.class public LX/77d;
.super LX/77c;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/77d;


# direct methods
.method public constructor <init>(LX/0SF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171976
    invoke-direct {p0, p1}, LX/77c;-><init>(LX/0SF;)V

    .line 1171977
    return-void
.end method

.method public static a(LX/0QB;)LX/77d;
    .locals 4

    .prologue
    .line 1171979
    sget-object v0, LX/77d;->a:LX/77d;

    if-nez v0, :cond_1

    .line 1171980
    const-class v1, LX/77d;

    monitor-enter v1

    .line 1171981
    :try_start_0
    sget-object v0, LX/77d;->a:LX/77d;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1171982
    if-eqz v2, :cond_0

    .line 1171983
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1171984
    new-instance p0, LX/77d;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SF;

    invoke-direct {p0, v3}, LX/77d;-><init>(LX/0SF;)V

    .line 1171985
    move-object v0, p0

    .line 1171986
    sput-object v0, LX/77d;->a:LX/77d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1171987
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1171988
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1171989
    :cond_1
    sget-object v0, LX/77d;->a:LX/77d;

    return-object v0

    .line 1171990
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1171991
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JJ)Z
    .locals 1

    .prologue
    .line 1171978
    cmp-long v0, p1, p3

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
