.class public LX/7D7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0

    .prologue
    .line 1182402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1182403
    iput p1, p0, LX/7D7;->a:I

    .line 1182404
    iput p2, p0, LX/7D7;->b:I

    .line 1182405
    iput p3, p0, LX/7D7;->c:I

    .line 1182406
    iput p4, p0, LX/7D7;->d:I

    .line 1182407
    return-void
.end method


# virtual methods
.method public final a(I)LX/7D7;
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 1182394
    if-nez p1, :cond_0

    .line 1182395
    :goto_0
    return-object p0

    .line 1182396
    :cond_0
    iget v0, p0, LX/7D7;->a:I

    sub-int v1, v0, p1

    .line 1182397
    iget v0, p0, LX/7D7;->a:I

    if-gez v0, :cond_1

    .line 1182398
    const/4 p0, 0x0

    goto :goto_0

    .line 1182399
    :cond_1
    iget v0, p0, LX/7D7;->c:I

    int-to-double v2, v0

    int-to-double v4, p1

    invoke-static {v8, v9, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 1182400
    iget v0, p0, LX/7D7;->d:I

    int-to-double v4, v0

    int-to-double v6, p1

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 1182401
    new-instance v0, LX/7D7;

    iget v4, p0, LX/7D7;->b:I

    invoke-direct {v0, v1, v4, v2, v3}, LX/7D7;-><init>(IIII)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1182367
    if-ne p1, p0, :cond_1

    .line 1182368
    :cond_0
    :goto_0
    return v0

    .line 1182369
    :cond_1
    instance-of v2, p1, LX/7D7;

    if-nez v2, :cond_2

    move v0, v1

    .line 1182370
    goto :goto_0

    .line 1182371
    :cond_2
    check-cast p1, LX/7D7;

    .line 1182372
    iget v2, p0, LX/7D7;->a:I

    move v2, v2

    .line 1182373
    iget v3, p1, LX/7D7;->a:I

    move v3, v3

    .line 1182374
    if-ne v2, v3, :cond_3

    .line 1182375
    iget v2, p0, LX/7D7;->b:I

    move v2, v2

    .line 1182376
    iget v3, p1, LX/7D7;->b:I

    move v3, v3

    .line 1182377
    if-ne v2, v3, :cond_3

    .line 1182378
    iget v2, p0, LX/7D7;->c:I

    move v2, v2

    .line 1182379
    iget v3, p1, LX/7D7;->c:I

    move v3, v3

    .line 1182380
    if-ne v2, v3, :cond_3

    .line 1182381
    iget v2, p0, LX/7D7;->d:I

    move v2, v2

    .line 1182382
    iget v3, p1, LX/7D7;->d:I

    move v3, v3

    .line 1182383
    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1182385
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 1182386
    iget v2, p0, LX/7D7;->a:I

    move v2, v2

    .line 1182387
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 1182388
    iget v2, p0, LX/7D7;->b:I

    move v2, v2

    .line 1182389
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 1182390
    iget v2, p0, LX/7D7;->c:I

    move v2, v2

    .line 1182391
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 1182392
    iget v2, p0, LX/7D7;->d:I

    move v2, v2

    .line 1182393
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1182384
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, LX/7D7;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/7D7;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/7D7;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/7D7;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
