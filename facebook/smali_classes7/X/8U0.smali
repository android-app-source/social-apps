.class public final LX/8U0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1349607
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 1349608
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1349609
    :goto_0
    return v1

    .line 1349610
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_6

    .line 1349611
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1349612
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1349613
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v10, :cond_0

    .line 1349614
    const-string v11, "is_new_player"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1349615
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v9, v4

    move v4, v2

    goto :goto_1

    .line 1349616
    :cond_1
    const-string v11, "rank"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1349617
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v8, v3

    move v3, v2

    goto :goto_1

    .line 1349618
    :cond_2
    const-string v11, "score"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1349619
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1349620
    :cond_3
    const-string v11, "score_int"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1349621
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 1349622
    :cond_4
    const-string v11, "user"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1349623
    invoke-static {p0, p1}, LX/8Tz;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1349624
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1349625
    :cond_6
    const/4 v10, 0x5

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1349626
    if-eqz v4, :cond_7

    .line 1349627
    invoke-virtual {p1, v1, v9}, LX/186;->a(IZ)V

    .line 1349628
    :cond_7
    if-eqz v3, :cond_8

    .line 1349629
    invoke-virtual {p1, v2, v8, v1}, LX/186;->a(III)V

    .line 1349630
    :cond_8
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 1349631
    if-eqz v0, :cond_9

    .line 1349632
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6, v1}, LX/186;->a(III)V

    .line 1349633
    :cond_9
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1349634
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1349635
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1349636
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1349637
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/8U0;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1349638
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1349639
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1349640
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1349641
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1349642
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1349643
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1349644
    invoke-static {p0, p1}, LX/8U0;->a(LX/15w;LX/186;)I

    move-result v1

    .line 1349645
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1349646
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1349647
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1349648
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 1349649
    if-eqz v0, :cond_0

    .line 1349650
    const-string v1, "is_new_player"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1349651
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1349652
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1349653
    if-eqz v0, :cond_1

    .line 1349654
    const-string v1, "rank"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1349655
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1349656
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1349657
    if-eqz v0, :cond_2

    .line 1349658
    const-string v1, "score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1349659
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1349660
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1349661
    if-eqz v0, :cond_3

    .line 1349662
    const-string v1, "score_int"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1349663
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1349664
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1349665
    if-eqz v0, :cond_4

    .line 1349666
    const-string v1, "user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1349667
    invoke-static {p0, v0, p2}, LX/8Tz;->a(LX/15i;ILX/0nX;)V

    .line 1349668
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1349669
    return-void
.end method
