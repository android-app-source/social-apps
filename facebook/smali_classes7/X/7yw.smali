.class public final LX/7yw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/7yt;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/7yu;

.field public final d:LX/7zA;

.field public final e:LX/7ys;

.field public final f:LX/7yz;

.field public final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/7yt;Ljava/util/Map;LX/7yu;)V
    .locals 6
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/7yu;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7yt;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/7yu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1280464
    new-instance v4, LX/7ys;

    invoke-direct {v4}, LX/7ys;-><init>()V

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/7yw;-><init>(LX/7yt;Ljava/util/Map;LX/7yu;LX/7ys;Ljava/lang/String;)V

    .line 1280465
    return-void
.end method

.method public constructor <init>(LX/7yt;Ljava/util/Map;LX/7yu;LX/7ys;)V
    .locals 6
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/7yu;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7yt;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/7yu;",
            "LX/7ys;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1280446
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/7yw;-><init>(LX/7yt;Ljava/util/Map;LX/7yu;LX/7ys;Ljava/lang/String;)V

    .line 1280447
    return-void
.end method

.method private constructor <init>(LX/7yt;Ljava/util/Map;LX/7yu;LX/7ys;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/7yu;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7yt;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/7yu;",
            "LX/7ys;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1280448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1280449
    iput-object p1, p0, LX/7yw;->a:LX/7yt;

    .line 1280450
    if-eqz p2, :cond_0

    :goto_0
    iput-object p2, p0, LX/7yw;->b:Ljava/util/Map;

    .line 1280451
    if-eqz p3, :cond_1

    :goto_1
    iput-object p3, p0, LX/7yw;->c:LX/7yu;

    .line 1280452
    new-instance v0, LX/7zA;

    iget-object v1, p0, LX/7yw;->c:LX/7yu;

    invoke-direct {v0, v1}, LX/7zA;-><init>(LX/7yu;)V

    iput-object v0, p0, LX/7yw;->d:LX/7zA;

    .line 1280453
    iput-object p4, p0, LX/7yw;->e:LX/7ys;

    .line 1280454
    new-instance v0, LX/7yz;

    invoke-direct {v0, p4}, LX/7yz;-><init>(LX/7ys;)V

    iput-object v0, p0, LX/7yw;->f:LX/7yz;

    .line 1280455
    iput-object p5, p0, LX/7yw;->g:Ljava/lang/String;

    .line 1280456
    return-void

    .line 1280457
    :cond_0
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    goto :goto_0

    .line 1280458
    :cond_1
    new-instance p3, LX/7yu;

    invoke-direct {p3}, LX/7yu;-><init>()V

    goto :goto_1
.end method

.method public constructor <init>(LX/7yt;Ljava/util/Map;LX/7yu;Ljava/lang/String;)V
    .locals 6
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/7yu;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7yt;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/7yu;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1280459
    new-instance v4, LX/7ys;

    invoke-direct {v4}, LX/7ys;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/7yw;-><init>(LX/7yt;Ljava/util/Map;LX/7yu;LX/7ys;Ljava/lang/String;)V

    .line 1280460
    return-void
.end method


# virtual methods
.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1280461
    iget-object v0, p0, LX/7yw;->a:LX/7yt;

    invoke-virtual {v0}, LX/7yt;->getUriPathElement()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1280462
    iget-object v0, p0, LX/7yw;->d:LX/7zA;

    invoke-virtual {v0}, LX/7zA;->c()V

    .line 1280463
    return-void
.end method
