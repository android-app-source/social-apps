.class public LX/7GY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Pt;


# static fields
.field public static final a:LX/6Pr;

.field public static final b:LX/6Pr;

.field public static final c:LX/6Pr;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1189561
    new-instance v0, LX/6Pr;

    const-string v1, "Delta"

    const-string v2, "Deltas from the sync protocol"

    const/16 v3, 0xff

    const/16 v4, 0xc8

    invoke-static {v3, v5, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LX/6Pr;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, LX/7GY;->a:LX/6Pr;

    .line 1189562
    new-instance v0, LX/6Pr;

    const-string v1, "Sync Exception"

    const-string v2, "Uncaught exceptions from the sync protocol"

    const/high16 v3, -0x10000

    invoke-direct {v0, v1, v2, v3}, LX/6Pr;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, LX/7GY;->b:LX/6Pr;

    .line 1189563
    new-instance v0, LX/6Pr;

    const-string v1, "Sync network"

    const-string v2, "Sync connection events (i.e. get_diffs)"

    const v3, -0xffff01

    invoke-direct {v0, v1, v2, v3}, LX/6Pr;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, LX/7GY;->c:LX/6Pr;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1189564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189565
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/6Pr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1189566
    sget-object v0, LX/7GY;->a:LX/6Pr;

    sget-object v1, LX/7GY;->b:LX/6Pr;

    sget-object v2, LX/7GY;->c:LX/6Pr;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
