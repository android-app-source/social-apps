.class public LX/7Cq;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:F

.field private static final b:LX/7Cp;

.field private static final c:LX/7Cp;

.field private static final d:LX/7Cp;

.field private static final e:LX/7Cp;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 1181884
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, LX/7Cq;->a:F

    .line 1181885
    new-instance v0, LX/7Cp;

    new-array v1, v5, [F

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, LX/7Cp;-><init>([F)V

    sput-object v0, LX/7Cq;->b:LX/7Cp;

    .line 1181886
    new-instance v0, LX/7Cp;

    new-array v1, v5, [F

    sget v2, LX/7Cq;->a:F

    aput v2, v1, v6

    aput v4, v1, v7

    aput v4, v1, v8

    const/4 v2, 0x3

    sget v3, LX/7Cq;->a:F

    aput v3, v1, v2

    invoke-direct {v0, v1}, LX/7Cp;-><init>([F)V

    sput-object v0, LX/7Cq;->c:LX/7Cp;

    .line 1181887
    new-instance v0, LX/7Cp;

    new-array v1, v5, [F

    fill-array-data v1, :array_1

    invoke-direct {v0, v1}, LX/7Cp;-><init>([F)V

    sput-object v0, LX/7Cq;->d:LX/7Cp;

    .line 1181888
    new-instance v0, LX/7Cp;

    new-array v1, v5, [F

    sget v2, LX/7Cq;->a:F

    aput v2, v1, v6

    aput v4, v1, v7

    aput v4, v1, v8

    const/4 v2, 0x3

    sget v3, LX/7Cq;->a:F

    neg-float v3, v3

    aput v3, v1, v2

    invoke-direct {v0, v1}, LX/7Cp;-><init>([F)V

    sput-object v0, LX/7Cq;->e:LX/7Cp;

    return-void

    .line 1181889
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
    .end array-data

    .line 1181890
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1181883
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(FF)F
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/high16 v2, 0x43b40000    # 360.0f

    .line 1181891
    invoke-static {p0, v1}, LX/7Cq;->a(FZ)F

    move-result v0

    .line 1181892
    invoke-static {p1, v1}, LX/7Cq;->a(FZ)F

    move-result v1

    .line 1181893
    sub-float v0, v1, v0

    .line 1181894
    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 1181895
    sub-float/2addr v0, v2

    .line 1181896
    :cond_0
    :goto_0
    return v0

    .line 1181897
    :cond_1
    const/high16 v1, -0x3ccc0000    # -180.0f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 1181898
    add-float/2addr v0, v2

    goto :goto_0
.end method

.method public static a(FIF)F
    .locals 2

    .prologue
    .line 1181882
    mul-float v0, p0, p2

    int-to-float v1, p1

    div-float/2addr v0, v1

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v1

    return v0
.end method

.method public static a(FZ)F
    .locals 3

    .prologue
    const/high16 v1, 0x43340000    # 180.0f

    const/high16 v2, 0x43b40000    # 360.0f

    .line 1181874
    rem-float v0, p0, v2

    .line 1181875
    if-eqz p1, :cond_0

    .line 1181876
    add-float/2addr v0, v1

    .line 1181877
    :cond_0
    cmpl-float v1, v0, v1

    if-lez v1, :cond_2

    .line 1181878
    sub-float/2addr v0, v2

    .line 1181879
    :cond_1
    :goto_0
    return v0

    .line 1181880
    :cond_2
    const/high16 v1, -0x3ccc0000    # -180.0f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 1181881
    add-float/2addr v0, v2

    goto :goto_0
.end method

.method public static a(I)LX/7Cp;
    .locals 1

    .prologue
    .line 1181868
    packed-switch p0, :pswitch_data_0

    .line 1181869
    sget-object v0, LX/7Cq;->b:LX/7Cp;

    .line 1181870
    :goto_0
    return-object v0

    .line 1181871
    :pswitch_0
    sget-object v0, LX/7Cq;->c:LX/7Cp;

    goto :goto_0

    .line 1181872
    :pswitch_1
    sget-object v0, LX/7Cq;->d:LX/7Cp;

    goto :goto_0

    .line 1181873
    :pswitch_2
    sget-object v0, LX/7Cq;->e:LX/7Cp;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
