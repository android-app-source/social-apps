.class public final LX/6rI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Du;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:LX/6sW;

.field public d:LX/6Ex;

.field private e:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/6qh;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;LX/6sW;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1152035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152036
    iput-object p1, p0, LX/6rI;->a:Landroid/content/Context;

    .line 1152037
    iput-object p2, p0, LX/6rI;->b:Ljava/util/concurrent/Executor;

    .line 1152038
    iput-object p3, p0, LX/6rI;->c:LX/6sW;

    .line 1152039
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ZLX/6qh;)V
    .locals 3
    .param p3    # LX/6qh;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1151937
    if-eqz p2, :cond_0

    .line 1151938
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1151939
    :cond_0
    new-instance v0, LX/31Y;

    invoke-direct {v0, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v1, 0x7f081d4e

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    new-instance v2, LX/6rH;

    invoke-direct {v2, p2, p3}, LX/6rH;-><init>(ZLX/6qh;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    new-instance v1, LX/6rG;

    invoke-direct {v1, p2, p3}, LX/6rG;-><init>(ZLX/6qh;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 1151940
    return-void
.end method

.method public static a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6sd;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 1152017
    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->u()LX/0P1;

    move-result-object v6

    .line 1152018
    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->A:LX/0m9;

    if-nez v0, :cond_2

    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    move-object v3, v0

    .line 1152019
    :goto_0
    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v7, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->v:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v4, v5

    .line 1152020
    :goto_1
    if-ge v4, v8, :cond_4

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    .line 1152021
    iget-object v1, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    invoke-virtual {v6, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1152022
    iget-object v1, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    invoke-virtual {v6, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Px;

    .line 1152023
    iget-object v2, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    const-string v9, "shipping_option"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1152024
    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    iget-object v2, v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    .line 1152025
    iput-object v2, p1, LX/6sd;->o:Ljava/lang/String;

    .line 1152026
    :cond_0
    :goto_2
    iget-object v2, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    iget-object v0, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    invoke-virtual {v3, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1152027
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 1152028
    :cond_2
    invoke-interface {p0}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->A:LX/0m9;

    invoke-virtual {v0}, LX/0m9;->I()LX/0m9;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    .line 1152029
    :cond_3
    iget-object v2, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a:Ljava/lang/String;

    const-string v9, "mailing_address"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1152030
    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;

    iget-object v2, v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    .line 1152031
    iput-object v2, p1, LX/6sd;->n:Ljava/lang/String;

    .line 1152032
    goto :goto_2

    .line 1152033
    :cond_4
    iput-object v3, p1, LX/6sd;->h:LX/0m9;

    .line 1152034
    return-void
.end method

.method public static b(LX/0QB;)LX/6rI;
    .locals 4

    .prologue
    .line 1152015
    new-instance v3, LX/6rI;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/6sW;->b(LX/0QB;)LX/6sW;

    move-result-object v2

    check-cast v2, LX/6sW;

    invoke-direct {v3, v0, v1, v2}, LX/6rI;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;LX/6sW;)V

    .line 1152016
    return-object v3
.end method

.method public static c(LX/6qh;)V
    .locals 2

    .prologue
    .line 1152012
    new-instance v0, LX/73T;

    sget-object v1, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v0, v1}, LX/73T;-><init>(LX/73S;)V

    .line 1152013
    invoke-virtual {p0, v0}, LX/6qh;->a(LX/73T;)V

    .line 1152014
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4

    .prologue
    .line 1151954
    iget-object v0, p0, LX/6rI;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151955
    iget-object v0, p0, LX/6rI;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1151956
    :goto_0
    return-object v0

    .line 1151957
    :cond_0
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    .line 1151958
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v2, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    invoke-static {v0, v2}, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xg;)LX/6sd;

    move-result-object v0

    iget-object v2, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->p:Ljava/lang/String;

    .line 1151959
    iput-object v2, v0, LX/6sd;->c:Ljava/lang/String;

    .line 1151960
    move-object v0, v0

    .line 1151961
    iget-object v2, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->q:Ljava/lang/String;

    .line 1151962
    iput-object v2, v0, LX/6sd;->d:Ljava/lang/String;

    .line 1151963
    move-object v0, v0

    .line 1151964
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->v()Ljava/lang/String;

    move-result-object v2

    .line 1151965
    iput-object v2, v0, LX/6sd;->r:Ljava/lang/String;

    .line 1151966
    move-object v0, v0

    .line 1151967
    iget-object v2, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->r:Ljava/lang/String;

    .line 1151968
    iput-object v2, v0, LX/6sd;->g:Ljava/lang/String;

    .line 1151969
    move-object v0, v0

    .line 1151970
    iget-object v2, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->A:LX/0m9;

    .line 1151971
    iput-object v2, v0, LX/6sd;->h:LX/0m9;

    .line 1151972
    move-object v0, v0

    .line 1151973
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->f()Ljava/lang/String;

    move-result-object v2

    .line 1151974
    iput-object v2, v0, LX/6sd;->p:Ljava/lang/String;

    .line 1151975
    move-object v0, v0

    .line 1151976
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->g()Ljava/lang/String;

    move-result-object v2

    .line 1151977
    iput-object v2, v0, LX/6sd;->q:Ljava/lang/String;

    .line 1151978
    move-object v0, v0

    .line 1151979
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1151980
    iput-object v2, v0, LX/6sd;->f:Ljava/lang/String;

    .line 1151981
    move-object v2, v0

    .line 1151982
    iget-object v0, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    sget-object v3, LX/6rp;->CONTACT_NAME:LX/6rp;

    invoke-virtual {v0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1151983
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->o()Lcom/facebook/payments/contactinfo/model/ContactInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->c()Ljava/lang/String;

    move-result-object v0

    .line 1151984
    iput-object v0, v2, LX/6sd;->l:Ljava/lang/String;

    .line 1151985
    :cond_1
    iget-object v0, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    sget-object v3, LX/6rp;->CONTACT_INFO:LX/6rp;

    invoke-virtual {v0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1151986
    iget-object v0, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    sget-object v3, LX/6vb;->EMAIL:LX/6vb;

    invoke-virtual {v0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1151987
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->l()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v0}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 1151988
    iput-object v0, v2, LX/6sd;->k:Ljava/lang/String;

    .line 1151989
    :cond_2
    iget-object v0, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    sget-object v3, LX/6vb;->PHONE_NUMBER:LX/6vb;

    invoke-virtual {v0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1151990
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->m()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v0}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 1151991
    iput-object v0, v2, LX/6sd;->m:Ljava/lang/String;

    .line 1151992
    :cond_3
    iget-object v0, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    sget-object v3, LX/6rp;->PAYMENT_METHOD:LX/6rp;

    invoke-virtual {v0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1151993
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->s()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 1151994
    iput-object v0, v2, LX/6sd;->i:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 1151995
    :cond_4
    iget-object v0, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    sget-object v3, LX/6rp;->MAILING_ADDRESS:LX/6rp;

    invoke-virtual {v0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1151996
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->h()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/MailingAddress;->a()Ljava/lang/String;

    move-result-object v0

    .line 1151997
    iput-object v0, v2, LX/6sd;->n:Ljava/lang/String;

    .line 1151998
    :cond_5
    iget-object v0, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    sget-object v3, LX/6rp;->SHIPPING_OPTION:LX/6rp;

    invoke-virtual {v0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1151999
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->j()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/ShippingOption;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingOption;->a()Ljava/lang/String;

    move-result-object v0

    .line 1152000
    iput-object v0, v2, LX/6sd;->o:Ljava/lang/String;

    .line 1152001
    :cond_6
    iget-object v0, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    sget-object v3, LX/6rp;->CHECKOUT_OPTIONS:LX/6rp;

    invoke-virtual {v0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1152002
    invoke-static {p1, v2}, LX/6rI;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6sd;)V

    .line 1152003
    :cond_7
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->g:LX/0Px;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1152004
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->g:LX/0Px;

    invoke-static {v0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(LX/0Px;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    .line 1152005
    iput-object v0, v2, LX/6sd;->e:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1152006
    :cond_8
    iget-object v0, p0, LX/6rI;->c:LX/6sW;

    invoke-virtual {v2}, LX/6sd;->a()Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6rI;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1152007
    iget-object v0, p0, LX/6rI;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/6rF;

    invoke-direct {v2, p0, v1}, LX/6rF;-><init>(LX/6rI;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    iget-object v1, p0, LX/6rI;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1152008
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->n:Z

    if-eqz v0, :cond_9

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->o:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1152009
    iget-object v0, p0, LX/6rI;->d:LX/6Ex;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->o:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/6Ex;->a(Ljava/lang/String;)V

    .line 1152010
    :cond_9
    iget-object v0, p0, LX/6rI;->d:LX/6Ex;

    invoke-interface {v0}, LX/6Ex;->a()V

    .line 1152011
    iget-object v0, p0, LX/6rI;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1151948
    iget-object v0, p0, LX/6rI;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1151949
    iget-object v0, p0, LX/6rI;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1151950
    iput-object v2, p0, LX/6rI;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1151951
    :cond_0
    iput-object v2, p0, LX/6rI;->d:LX/6Ex;

    .line 1151952
    iput-object v2, p0, LX/6rI;->f:LX/6qh;

    .line 1151953
    return-void
.end method

.method public final a(LX/6Ex;)V
    .locals 0

    .prologue
    .line 1151946
    iput-object p1, p0, LX/6rI;->d:LX/6Ex;

    .line 1151947
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1151944
    iput-object p1, p0, LX/6rI;->f:LX/6qh;

    .line 1151945
    return-void
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;)Z
    .locals 1

    .prologue
    .line 1151943
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Lcom/facebook/payments/checkout/model/CheckoutData;)Z
    .locals 1

    .prologue
    .line 1151942
    const/4 v0, 0x0

    return v0
.end method

.method public final d(Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 0

    .prologue
    .line 1151941
    return-void
.end method
