.class public final enum LX/7WD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7WD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7WD;

.field public static final enum CLOSE_OPTIN:LX/7WD;

.field public static final enum DEFAULT_BEHAVIOR:LX/7WD;

.field public static final enum DO_NOTHING:LX/7WD;

.field public static final enum PRIMARY_BUTTON_ACTION:LX/7WD;

.field public static final enum SECONDARY_BUTTON_ACTION:LX/7WD;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1215944
    new-instance v0, LX/7WD;

    const-string v1, "CLOSE_OPTIN"

    invoke-direct {v0, v1, v2}, LX/7WD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7WD;->CLOSE_OPTIN:LX/7WD;

    .line 1215945
    new-instance v0, LX/7WD;

    const-string v1, "DO_NOTHING"

    invoke-direct {v0, v1, v3}, LX/7WD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7WD;->DO_NOTHING:LX/7WD;

    .line 1215946
    new-instance v0, LX/7WD;

    const-string v1, "PRIMARY_BUTTON_ACTION"

    invoke-direct {v0, v1, v4}, LX/7WD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7WD;->PRIMARY_BUTTON_ACTION:LX/7WD;

    .line 1215947
    new-instance v0, LX/7WD;

    const-string v1, "SECONDARY_BUTTON_ACTION"

    invoke-direct {v0, v1, v5}, LX/7WD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7WD;->SECONDARY_BUTTON_ACTION:LX/7WD;

    .line 1215948
    new-instance v0, LX/7WD;

    const-string v1, "DEFAULT_BEHAVIOR"

    invoke-direct {v0, v1, v6}, LX/7WD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7WD;->DEFAULT_BEHAVIOR:LX/7WD;

    .line 1215949
    const/4 v0, 0x5

    new-array v0, v0, [LX/7WD;

    sget-object v1, LX/7WD;->CLOSE_OPTIN:LX/7WD;

    aput-object v1, v0, v2

    sget-object v1, LX/7WD;->DO_NOTHING:LX/7WD;

    aput-object v1, v0, v3

    sget-object v1, LX/7WD;->PRIMARY_BUTTON_ACTION:LX/7WD;

    aput-object v1, v0, v4

    sget-object v1, LX/7WD;->SECONDARY_BUTTON_ACTION:LX/7WD;

    aput-object v1, v0, v5

    sget-object v1, LX/7WD;->DEFAULT_BEHAVIOR:LX/7WD;

    aput-object v1, v0, v6

    sput-object v0, LX/7WD;->$VALUES:[LX/7WD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1215943
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/7WD;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1215932
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1215933
    sget-object v0, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivityBase;->p:Ljava/lang/String;

    const-string v3, "Encountered unexpected BackButtonBehavior string: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v1

    invoke-static {v0, v3, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1215934
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 1215935
    :sswitch_0
    const-string v3, "close_optin"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v3, "do_nothing"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string v3, "primary_button_action"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v3, "secondary_button_action"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v3, "default_behavior"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    .line 1215936
    :pswitch_0
    sget-object v0, LX/7WD;->CLOSE_OPTIN:LX/7WD;

    goto :goto_1

    .line 1215937
    :pswitch_1
    sget-object v0, LX/7WD;->DO_NOTHING:LX/7WD;

    goto :goto_1

    .line 1215938
    :pswitch_2
    sget-object v0, LX/7WD;->PRIMARY_BUTTON_ACTION:LX/7WD;

    goto :goto_1

    .line 1215939
    :pswitch_3
    sget-object v0, LX/7WD;->SECONDARY_BUTTON_ACTION:LX/7WD;

    goto :goto_1

    .line 1215940
    :pswitch_4
    sget-object v0, LX/7WD;->DEFAULT_BEHAVIOR:LX/7WD;

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3e291fe7 -> :sswitch_1
        0x1e82c211 -> :sswitch_0
        0x379f6c66 -> :sswitch_2
        0x53f68690 -> :sswitch_4
        0x5f32e058 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/7WD;
    .locals 1

    .prologue
    .line 1215942
    const-class v0, LX/7WD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7WD;

    return-object v0
.end method

.method public static values()[LX/7WD;
    .locals 1

    .prologue
    .line 1215941
    sget-object v0, LX/7WD;->$VALUES:[LX/7WD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7WD;

    return-object v0
.end method
