.class public final LX/75I;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6Z0;

.field private final b:LX/0So;

.field private c:Landroid/content/Context;

.field private d:Lcom/facebook/photos/base/media/PhotoItem;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6Z0;LX/0So;Lcom/facebook/photos/base/media/PhotoItem;Z)V
    .locals 0

    .prologue
    .line 1169161
    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 1169162
    iput-object p1, p0, LX/75I;->c:Landroid/content/Context;

    .line 1169163
    iput-object p4, p0, LX/75I;->d:Lcom/facebook/photos/base/media/PhotoItem;

    .line 1169164
    iput-object p2, p0, LX/75I;->a:LX/6Z0;

    .line 1169165
    iput-object p3, p0, LX/75I;->b:LX/0So;

    .line 1169166
    iput-boolean p5, p0, LX/75I;->e:Z

    .line 1169167
    return-void
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1169168
    iget-object v0, p0, LX/75I;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, LX/75I;->a:LX/6Z0;

    iget-object v1, v1, LX/6Z0;->e:Landroid/net/Uri;

    new-array v2, v6, [Ljava/lang/String;

    sget-object v3, LX/6Yx;->d:LX/0U1;

    .line 1169169
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1169170
    aput-object v3, v2, v7

    const/4 v5, 0x0

    move-object v3, p1

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1169171
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 1169172
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move v0, v6

    .line 1169173
    :goto_0
    return v0

    .line 1169174
    :cond_0
    if-eqz v0, :cond_1

    .line 1169175
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    move v0, v7

    .line 1169176
    goto :goto_0
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1169177
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1169178
    iget-object v2, p0, LX/75I;->d:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-static {v2}, LX/75E;->a(Lcom/facebook/ipc/media/MediaItem;)Ljava/lang/String;

    move-result-object v2

    .line 1169179
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1169180
    sget-object v4, LX/6Yx;->b:LX/0U1;

    .line 1169181
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1169182
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1169183
    const-string v4, " = ? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1169184
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1169185
    new-array v4, v0, [Ljava/lang/String;

    aput-object v2, v4, v1

    .line 1169186
    invoke-direct {p0, v3, v4}, LX/75I;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    .line 1169187
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 1169188
    sget-object v7, LX/6Yx;->b:LX/0U1;

    .line 1169189
    iget-object v8, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v8

    .line 1169190
    invoke-virtual {v6, v7, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169191
    sget-object v2, LX/6Yx;->d:LX/0U1;

    .line 1169192
    iget-object v7, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v7

    .line 1169193
    iget-boolean v7, p0, LX/75I;->e:Z

    if-eqz v7, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1169194
    sget-object v0, LX/6Yx;->f:LX/0U1;

    .line 1169195
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1169196
    iget-object v1, p0, LX/75I;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1169197
    if-eqz v5, :cond_1

    .line 1169198
    iget-object v0, p0, LX/75I;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, LX/75I;->a:LX/6Z0;

    iget-object v1, v1, LX/6Z0;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1, v6, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1169199
    :goto_1
    const/4 v0, 0x0

    return-object v0

    :cond_0
    move v0, v1

    .line 1169200
    goto :goto_0

    .line 1169201
    :cond_1
    iget-object v0, p0, LX/75I;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, LX/75I;->a:LX/6Z0;

    iget-object v1, v1, LX/6Z0;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_1
.end method
