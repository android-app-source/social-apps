.class public final LX/84W;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/lang/Void;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/2iQ;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:J

.field public final synthetic f:LX/2dp;


# direct methods
.method public constructor <init>(LX/2dp;ZLX/2iQ;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 1290956
    iput-object p1, p0, LX/84W;->f:LX/2dp;

    iput-boolean p2, p0, LX/84W;->a:Z

    iput-object p3, p0, LX/84W;->b:LX/2iQ;

    iput-object p4, p0, LX/84W;->c:Ljava/lang/String;

    iput-object p5, p0, LX/84W;->d:Ljava/lang/String;

    iput-wide p6, p0, LX/84W;->e:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1290953
    iget-boolean v0, p0, LX/84W;->a:Z

    if-eqz v0, :cond_0

    const-string v1, "send"

    .line 1290954
    :goto_0
    iget-object v0, p0, LX/84W;->f:LX/2dp;

    iget-object v0, v0, LX/2dp;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2dj;

    iget-object v2, p0, LX/84W;->b:LX/2iQ;

    iget-object v3, p0, LX/84W;->c:Ljava/lang/String;

    iget-object v4, p0, LX/84W;->d:Ljava/lang/String;

    iget-wide v6, p0, LX/84W;->e:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/2dj;->a(Ljava/lang/String;LX/2iQ;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 1290955
    :cond_0
    const-string v1, "cancel"

    goto :goto_0
.end method
