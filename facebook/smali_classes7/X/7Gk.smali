.class public final LX/7Gk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/7Gl;

.field public final b:Ljava/lang/String;

.field private final c:LX/7Gl;


# direct methods
.method public constructor <init>(LX/7Gl;Ljava/lang/String;LX/7Gl;)V
    .locals 0

    .prologue
    .line 1189709
    iput-object p1, p0, LX/7Gk;->a:LX/7Gl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189710
    iput-object p2, p0, LX/7Gk;->b:Ljava/lang/String;

    .line 1189711
    iput-object p3, p0, LX/7Gk;->c:LX/7Gl;

    .line 1189712
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/Editable;)Z
    .locals 6

    .prologue
    const/16 v5, 0x20

    const/4 v3, -0x1

    const/4 v0, 0x0

    .line 1189713
    invoke-interface {p1, p0}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    .line 1189714
    invoke-interface {p1, p0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    .line 1189715
    if-eq v1, v3, :cond_0

    if-ne v2, v3, :cond_1

    .line 1189716
    :cond_0
    :goto_0
    return v0

    .line 1189717
    :cond_1
    iget-object v3, p0, LX/7Gk;->b:Ljava/lang/String;

    invoke-interface {p1, v1, v2}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1189718
    iget-object v3, p0, LX/7Gk;->c:LX/7Gl;

    invoke-virtual {v3, p1}, LX/7Gl;->a(Landroid/text/Editable;)I

    move-result v3

    .line 1189719
    if-le v1, v3, :cond_2

    .line 1189720
    add-int/lit8 v1, v1, -0x1

    invoke-interface {p1, v1}, Landroid/text/Editable;->charAt(I)C

    move-result v1

    if-ne v1, v5, :cond_0

    .line 1189721
    :cond_2
    iget-object v1, p0, LX/7Gk;->c:LX/7Gl;

    invoke-virtual {v1, p1}, LX/7Gl;->b(Landroid/text/Editable;)I

    move-result v1

    .line 1189722
    if-ge v2, v1, :cond_3

    .line 1189723
    invoke-interface {p1, v2}, Landroid/text/Editable;->charAt(I)C

    move-result v1

    if-ne v1, v5, :cond_0

    .line 1189724
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
