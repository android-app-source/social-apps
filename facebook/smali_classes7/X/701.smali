.class public LX/701;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6vm;


# instance fields
.field public final a:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

.field public final b:Z

.field public final c:Landroid/content/Intent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:I

.field public final e:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;


# direct methods
.method public constructor <init>(LX/702;)V
    .locals 1

    .prologue
    .line 1161738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161739
    iget-object v0, p1, LX/702;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    move-object v0, v0

    .line 1161740
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    iput-object v0, p0, LX/701;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 1161741
    iget-boolean v0, p1, LX/702;->b:Z

    move v0, v0

    .line 1161742
    iput-boolean v0, p0, LX/701;->b:Z

    .line 1161743
    iget-object v0, p1, LX/702;->c:Landroid/content/Intent;

    move-object v0, v0

    .line 1161744
    iput-object v0, p0, LX/701;->c:Landroid/content/Intent;

    .line 1161745
    iget v0, p1, LX/702;->d:I

    move v0, v0

    .line 1161746
    iput v0, p0, LX/701;->d:I

    .line 1161747
    iget-object v0, p1, LX/702;->e:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-object v0, v0

    .line 1161748
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, LX/701;->e:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1161749
    return-void
.end method

.method public static newBuilder()LX/702;
    .locals 1

    .prologue
    .line 1161736
    new-instance v0, LX/702;

    invoke-direct {v0}, LX/702;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()LX/71I;
    .locals 1

    .prologue
    .line 1161737
    sget-object v0, LX/71I;->EXISTING_PAYMENT_METHOD:LX/71I;

    return-object v0
.end method
