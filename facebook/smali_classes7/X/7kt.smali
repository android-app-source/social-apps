.class public LX/7kt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/7kt;


# instance fields
.field private final a:LX/74o;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/74o;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/annotation/MaxNumberPhotosPerUpload;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/74o;",
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1232634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1232635
    iput-object p1, p0, LX/7kt;->a:LX/74o;

    .line 1232636
    iput-object p2, p0, LX/7kt;->b:LX/0Or;

    .line 1232637
    return-void
.end method

.method public static a(LX/0QB;)LX/7kt;
    .locals 5

    .prologue
    .line 1232638
    sget-object v0, LX/7kt;->c:LX/7kt;

    if-nez v0, :cond_1

    .line 1232639
    const-class v1, LX/7kt;

    monitor-enter v1

    .line 1232640
    :try_start_0
    sget-object v0, LX/7kt;->c:LX/7kt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1232641
    if-eqz v2, :cond_0

    .line 1232642
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1232643
    new-instance v4, LX/7kt;

    invoke-static {v0}, LX/74o;->a(LX/0QB;)LX/74o;

    move-result-object v3

    check-cast v3, LX/74o;

    const/16 p0, 0x15d6

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/7kt;-><init>(LX/74o;LX/0Or;)V

    .line 1232644
    move-object v0, v4

    .line 1232645
    sput-object v0, LX/7kt;->c:LX/7kt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1232646
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1232647
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1232648
    :cond_1
    sget-object v0, LX/7kt;->c:LX/7kt;

    return-object v0

    .line 1232649
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1232650
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Px;Z)LX/7ks;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;Z)",
            "LX/7ks;"
        }
    .end annotation

    .prologue
    .line 1232651
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1232652
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1232653
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_3

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1232654
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1232655
    if-eqz p2, :cond_0

    .line 1232656
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1232657
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1232658
    :cond_0
    sget-object v0, LX/7kr;->NONEXISTANT_PHOTO:LX/7kr;

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1232659
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    .line 1232660
    if-nez p2, :cond_2

    invoke-static {v1}, LX/74o;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1232661
    sget-object v0, LX/7kr;->NONEXISTANT_PHOTO:LX/7kr;

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1232662
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    iget-object v1, p0, LX/7kt;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lt v6, v1, :cond_4

    .line 1232663
    sget-object v0, LX/7kr;->TOO_MANY_PHOTOS:LX/7kr;

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1232664
    :cond_3
    new-instance v0, LX/7ks;

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7ks;-><init>(LX/0Px;LX/0Px;)V

    return-object v0

    .line 1232665
    :cond_4
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
