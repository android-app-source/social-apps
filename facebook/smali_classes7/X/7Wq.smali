.class public LX/7Wq;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public final a:LX/0Xl;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Xl;)V
    .locals 1
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1216804
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1216805
    iput-object p2, p0, LX/7Wq;->a:LX/0Xl;

    .line 1216806
    new-instance v0, LX/7Wp;

    invoke-direct {v0, p0}, LX/7Wp;-><init>(LX/7Wq;)V

    invoke-virtual {p0, v0}, LX/7Wq;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1216807
    const v0, 0x7f080e99

    invoke-virtual {p0, v0}, LX/7Wq;->setTitle(I)V

    .line 1216808
    return-void
.end method

.method public static b(LX/0QB;)LX/7Wq;
    .locals 3

    .prologue
    .line 1216809
    new-instance v2, LX/7Wq;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v1

    check-cast v1, LX/0Xl;

    invoke-direct {v2, v0, v1}, LX/7Wq;-><init>(Landroid/content/Context;LX/0Xl;)V

    .line 1216810
    return-object v2
.end method
