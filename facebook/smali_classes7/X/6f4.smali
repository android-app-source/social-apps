.class public final LX/6f4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:J

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:LX/3CE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3CE",
            "<",
            "LX/2MK;",
            "LX/5zj;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/6f5;)V
    .locals 4

    .prologue
    .line 1119045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119046
    iget-wide v2, p1, LX/6f5;->c:J

    move-wide v0, v2

    .line 1119047
    iput-wide v0, p0, LX/6f4;->a:J

    .line 1119048
    iget v0, p1, LX/6f5;->d:I

    move v0, v0

    .line 1119049
    iput v0, p0, LX/6f4;->b:I

    .line 1119050
    iget v0, p1, LX/6f5;->e:I

    move v0, v0

    .line 1119051
    iput v0, p0, LX/6f4;->c:I

    .line 1119052
    iget v0, p1, LX/6f5;->f:I

    move v0, v0

    .line 1119053
    iput v0, p0, LX/6f4;->d:I

    .line 1119054
    iget v0, p1, LX/6f5;->g:I

    move v0, v0

    .line 1119055
    iput v0, p0, LX/6f4;->e:I

    .line 1119056
    iget v0, p1, LX/6f5;->h:I

    move v0, v0

    .line 1119057
    iput v0, p0, LX/6f4;->f:I

    .line 1119058
    iget v0, p1, LX/6f5;->i:I

    move v0, v0

    .line 1119059
    iput v0, p0, LX/6f4;->g:I

    .line 1119060
    iget v0, p1, LX/6f5;->j:I

    move v0, v0

    .line 1119061
    iput v0, p0, LX/6f4;->h:I

    .line 1119062
    iget v0, p1, LX/6f5;->k:I

    move v0, v0

    .line 1119063
    iput v0, p0, LX/6f4;->i:I

    .line 1119064
    iget-object v0, p1, LX/6f5;->a:LX/0Xu;

    move-object v0, v0

    .line 1119065
    invoke-static {v0}, LX/3CE;->b(LX/0Xu;)LX/3CE;

    move-result-object v0

    iput-object v0, p0, LX/6f4;->j:LX/3CE;

    .line 1119066
    iget-object v0, p1, LX/6f5;->b:Ljava/util/List;

    move-object v0, v0

    .line 1119067
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/6f4;->k:LX/0Px;

    .line 1119068
    return-void
.end method
