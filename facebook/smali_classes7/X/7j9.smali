.class public LX/7j9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1229216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1229217
    iput-object p1, p0, LX/7j9;->a:Landroid/content/res/Resources;

    .line 1229218
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;)LX/7iu;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1229219
    if-eqz p0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->VISIBLE:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    if-ne p0, v0, :cond_1

    .line 1229220
    :cond_0
    const/4 v0, 0x0

    .line 1229221
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->PRODUCT_REJECTED:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    if-ne p0, v0, :cond_2

    sget-object v0, LX/7iu;->WARN:LX/7iu;

    goto :goto_0

    :cond_2
    sget-object v0, LX/7iu;->NOTIFY:LX/7iu;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/7j9;
    .locals 2

    .prologue
    .line 1229222
    new-instance v1, LX/7j9;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/7j9;-><init>(Landroid/content/res/Resources;)V

    .line 1229223
    move-object v0, v1

    .line 1229224
    return-object v0
.end method


# virtual methods
.method public final b(Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1229225
    if-nez p1, :cond_0

    .line 1229226
    :goto_0
    return-object v0

    .line 1229227
    :cond_0
    sget-object v1, LX/7j8;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1229228
    :pswitch_0
    iget-object v0, p0, LX/7j9;->a:Landroid/content/res/Resources;

    const v1, 0x7f0814bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1229229
    :pswitch_1
    iget-object v0, p0, LX/7j9;->a:Landroid/content/res/Resources;

    const v1, 0x7f0814bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c(Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1229230
    if-nez p1, :cond_0

    .line 1229231
    :goto_0
    return-object v0

    .line 1229232
    :cond_0
    sget-object v1, LX/7j8;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1229233
    :pswitch_0
    iget-object v0, p0, LX/7j9;->a:Landroid/content/res/Resources;

    const v1, 0x7f0814be

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1229234
    :pswitch_1
    iget-object v0, p0, LX/7j9;->a:Landroid/content/res/Resources;

    const v1, 0x7f0814c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1229235
    :pswitch_2
    iget-object v0, p0, LX/7j9;->a:Landroid/content/res/Resources;

    const v1, 0x7f0814c2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1229236
    :pswitch_3
    iget-object v0, p0, LX/7j9;->a:Landroid/content/res/Resources;

    const v1, 0x7f0814c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1229237
    :pswitch_4
    iget-object v0, p0, LX/7j9;->a:Landroid/content/res/Resources;

    const v1, 0x7f0814c3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
