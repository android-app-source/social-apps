.class public final LX/7CX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field public final synthetic a:LX/7CY;

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(LX/7CY;)V
    .locals 1

    .prologue
    .line 1181006
    iput-object p1, p0, LX/7CX;->a:LX/7CY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1181007
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7CX;->b:Z

    return-void
.end method

.method public static a$redex0(LX/7CX;)V
    .locals 1

    .prologue
    .line 1181027
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7CX;->c:Z

    .line 1181028
    return-void
.end method


# virtual methods
.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 1181029
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1181008
    iget-boolean v0, p0, LX/7CX;->c:Z

    if-eqz v0, :cond_0

    .line 1181009
    :goto_0
    return-void

    .line 1181010
    :cond_0
    iget-object v0, p0, LX/7CX;->a:LX/7CY;

    iget-object v0, v0, LX/7CY;->e:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1181011
    iget-object v0, p0, LX/7CX;->a:LX/7CY;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v2

    .line 1181012
    iput v3, v0, LX/7CY;->l:F

    .line 1181013
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "proximity cm changed to "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/7CX;->a:LX/7CY;

    iget v3, v3, LX/7CY;->l:F

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, LX/7CX;->a:LX/7CY;

    iget-object v3, v3, LX/7CY;->g:Landroid/hardware/Sensor;

    invoke-virtual {v3}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1181014
    iget-boolean v0, p0, LX/7CX;->b:Z

    if-eqz v0, :cond_3

    .line 1181015
    iget-object v0, p0, LX/7CX;->a:LX/7CY;

    iget-boolean v0, v0, LX/7CY;->k:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1181016
    iput-boolean v2, p0, LX/7CX;->b:Z

    .line 1181017
    iget-object v0, p0, LX/7CX;->a:LX/7CY;

    iget-boolean v0, v0, LX/7CY;->m:Z

    if-nez v0, :cond_1

    move v2, v1

    :cond_1
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1181018
    iget-object v0, p0, LX/7CX;->a:LX/7CY;

    .line 1181019
    iput-boolean v1, v0, LX/7CY;->m:Z

    .line 1181020
    iget-object v0, p0, LX/7CX;->a:LX/7CY;

    iget-object v0, v0, LX/7CY;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/7CX;->a:LX/7CY;

    iget-object v1, v1, LX/7CY;->i:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    const v4, -0x5592f21

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1181021
    goto :goto_1

    .line 1181022
    :cond_3
    iget-object v0, p0, LX/7CX;->a:LX/7CY;

    iget-boolean v0, v0, LX/7CY;->m:Z

    if-eqz v0, :cond_4

    .line 1181023
    iget-object v0, p0, LX/7CX;->a:LX/7CY;

    .line 1181024
    iput-boolean v2, v0, LX/7CY;->m:Z

    .line 1181025
    iget-object v0, p0, LX/7CX;->a:LX/7CY;

    iget-object v0, v0, LX/7CY;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/7CX;->a:LX/7CY;

    iget-object v1, v1, LX/7CY;->i:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1181026
    :cond_4
    iget-object v0, p0, LX/7CX;->a:LX/7CY;

    invoke-static {v0}, LX/7CY;->c(LX/7CY;)V

    goto :goto_0
.end method
