.class public LX/6mA;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/6bX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1143345
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1143346
    invoke-direct {p0, p1}, LX/6mA;->a(Landroid/content/Context;)V

    .line 1143347
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1143378
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1143379
    invoke-direct {p0, p1}, LX/6mA;->a(Landroid/content/Context;)V

    .line 1143380
    if-eqz p2, :cond_0

    .line 1143381
    invoke-direct {p0, p2}, LX/6mA;->setupAttributes(Landroid/util/AttributeSet;)V

    .line 1143382
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1143373
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1143374
    invoke-direct {p0, p1}, LX/6mA;->a(Landroid/content/Context;)V

    .line 1143375
    if-eqz p2, :cond_0

    .line 1143376
    invoke-direct {p0, p2}, LX/6mA;->setupAttributes(Landroid/util/AttributeSet;)V

    .line 1143377
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1143368
    const-class v0, LX/6mA;

    invoke-static {v0, p0}, LX/6mA;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1143369
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1143370
    const v1, 0x7f031662

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1143371
    const v0, 0x7f0d0499

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/6mA;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1143372
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/6mA;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/6mA;

    invoke-static {v0}, LX/6bc;->b(LX/0QB;)LX/6bc;

    move-result-object v1

    check-cast v1, LX/6bc;

    invoke-static {v0}, LX/6bZ;->b(LX/0QB;)LX/6bZ;

    move-result-object v2

    check-cast v2, LX/6bZ;

    invoke-static {v0}, LX/2u8;->b(LX/0QB;)LX/2u8;

    move-result-object v3

    check-cast v3, LX/2u8;

    invoke-static {v0}, LX/6bb;->b(LX/0QB;)LX/6bb;

    move-result-object p1

    check-cast p1, LX/6bb;

    invoke-static {v1, v2, v3, p1}, LX/6bd;->a(LX/6bc;LX/6bZ;LX/2u8;LX/6bb;)LX/6bX;

    move-result-object v1

    move-object v0, v1

    check-cast v0, LX/6bX;

    iput-object v0, p0, LX/6mA;->a:LX/6bX;

    return-void
.end method

.method private setupAttributes(Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1143356
    invoke-virtual {p0}, LX/6mA;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->ActionLinkButton:[I

    invoke-virtual {v0, p1, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1143357
    :try_start_0
    const/16 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 1143358
    if-eqz v0, :cond_0

    .line 1143359
    invoke-virtual {p0}, LX/6mA;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1143360
    if-eqz v0, :cond_0

    .line 1143361
    invoke-virtual {p0, v0}, LX/6mA;->setText(Ljava/lang/String;)V

    .line 1143362
    :cond_0
    const/16 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1143363
    if-eqz v0, :cond_1

    .line 1143364
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/6mA;->setOnClickUri(Landroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1143365
    :cond_1
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1143366
    return-void

    .line 1143367
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method


# virtual methods
.method public setActionLink(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;)V
    .locals 1

    .prologue
    .line 1143352
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1143353
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/6mA;->setText(Ljava/lang/String;)V

    .line 1143354
    invoke-virtual {p1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/6mA;->setOnClickUri(Landroid/net/Uri;)V

    .line 1143355
    return-void
.end method

.method public setOnClickUri(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1143350
    new-instance v0, LX/6m9;

    invoke-direct {v0, p0, p1}, LX/6m9;-><init>(LX/6mA;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, LX/6mA;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1143351
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1143348
    iget-object v0, p0, LX/6mA;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1143349
    return-void
.end method
