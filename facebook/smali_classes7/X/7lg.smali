.class public LX/7lg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/composer/protocol/PostReviewParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1235428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1235429
    return-void
.end method

.method public static a(LX/0QB;)LX/7lg;
    .locals 1

    .prologue
    .line 1235430
    new-instance v0, LX/7lg;

    invoke-direct {v0}, LX/7lg;-><init>()V

    .line 1235431
    move-object v0, v0

    .line 1235432
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 10

    .prologue
    .line 1235409
    check-cast p1, Lcom/facebook/composer/protocol/PostReviewParams;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1235410
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1235411
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1235412
    iget-object v0, p1, Lcom/facebook/composer/protocol/PostReviewParams;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1235413
    iget-object v0, p1, Lcom/facebook/composer/protocol/PostReviewParams;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1235414
    iget-object v0, p1, Lcom/facebook/composer/protocol/PostReviewParams;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1235415
    iget-wide v6, p1, Lcom/facebook/composer/protocol/PostReviewParams;->b:J

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1235416
    iget v0, p1, Lcom/facebook/composer/protocol/PostReviewParams;->e:I

    if-lez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1235417
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "rating"

    iget v2, p1, Lcom/facebook/composer/protocol/PostReviewParams;->e:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1235418
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "review"

    iget-object v2, p1, Lcom/facebook/composer/protocol/PostReviewParams;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1235419
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "privacy"

    iget-object v2, p1, Lcom/facebook/composer/protocol/PostReviewParams;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1235420
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "surface"

    iget-object v2, p1, Lcom/facebook/composer/protocol/PostReviewParams;->g:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1235421
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "mechanism"

    iget-object v2, p1, Lcom/facebook/composer/protocol/PostReviewParams;->f:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1235422
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "return_type"

    const-string v2, "CONTACT_RECOMMENDATION_FIELD"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1235423
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "photo_count"

    iget v2, p1, Lcom/facebook/composer/protocol/PostReviewParams;->i:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1235424
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1235425
    new-instance v0, LX/14N;

    const-string v1, "postRating"

    const-string v2, "POST"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v6, p1, Lcom/facebook/composer/protocol/PostReviewParams;->b:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/open_graph_ratings"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    :cond_0
    move v0, v2

    .line 1235426
    goto/16 :goto_0

    :cond_1
    move v1, v2

    .line 1235427
    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1235407
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1235408
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
