.class public final LX/8JO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/tagging/shared/TagTypeahead;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/tagging/shared/TagTypeahead;)V
    .locals 0

    .prologue
    .line 1328474
    iput-object p1, p0, LX/8JO;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 1328475
    iget-object v0, p0, LX/8JO;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    iget-object v1, v0, Lcom/facebook/photos/tagging/shared/TagTypeahead;->j:Landroid/widget/EditText;

    iget-object v0, p0, LX/8JO;->a:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    invoke-virtual {v0}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p2, :cond_0

    const v0, 0x7f0a05c0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 1328476
    return-void

    .line 1328477
    :cond_0
    const v0, 0x7f0a05bf

    goto :goto_0
.end method
