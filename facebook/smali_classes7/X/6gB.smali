.class public LX/6gB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1121443
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1121444
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/6gB;->a:Ljava/util/List;

    return-void
.end method

.method public static newBuilder()LX/6gB;
    .locals 1

    .prologue
    .line 1121445
    new-instance v0, LX/6gB;

    invoke-direct {v0}, LX/6gB;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)LX/6gB;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/6gB;"
        }
    .end annotation

    .prologue
    .line 1121441
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LX/6gB;->a:Ljava/util/List;

    .line 1121442
    return-object p0
.end method

.method public final e()Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;
    .locals 1

    .prologue
    .line 1121440
    new-instance v0, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/momentsinvite/model/MomentsInviteData;-><init>(LX/6gB;)V

    return-object v0
.end method
