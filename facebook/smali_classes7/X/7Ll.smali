.class public final enum LX/7Ll;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/7Ll;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/7Ll;

.field public static final enum GUIDE_OFF:LX/7Ll;

.field public static final enum GUIDE_ON:LX/7Ll;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1198410
    new-instance v0, LX/7Ll;

    const-string v1, "GUIDE_ON"

    invoke-direct {v0, v1, v2}, LX/7Ll;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Ll;->GUIDE_ON:LX/7Ll;

    .line 1198411
    new-instance v0, LX/7Ll;

    const-string v1, "GUIDE_OFF"

    invoke-direct {v0, v1, v3}, LX/7Ll;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/7Ll;->GUIDE_OFF:LX/7Ll;

    .line 1198412
    const/4 v0, 0x2

    new-array v0, v0, [LX/7Ll;

    sget-object v1, LX/7Ll;->GUIDE_ON:LX/7Ll;

    aput-object v1, v0, v2

    sget-object v1, LX/7Ll;->GUIDE_OFF:LX/7Ll;

    aput-object v1, v0, v3

    sput-object v0, LX/7Ll;->$VALUES:[LX/7Ll;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1198413
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/7Ll;
    .locals 1

    .prologue
    .line 1198414
    const-class v0, LX/7Ll;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/7Ll;

    return-object v0
.end method

.method public static values()[LX/7Ll;
    .locals 1

    .prologue
    .line 1198415
    sget-object v0, LX/7Ll;->$VALUES:[LX/7Ll;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Ll;

    return-object v0
.end method
