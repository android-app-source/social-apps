.class public LX/7mY;
.super LX/7mV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7mV",
        "<",
        "LX/7ml;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final e:Ljava/lang/String;

.field private static volatile h:LX/7mY;


# instance fields
.field private final f:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field private final g:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1237172
    const-class v0, LX/7mY;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7mY;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/0SG;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1237173
    invoke-direct {p0, p2}, LX/7mV;-><init>(LX/0SG;)V

    .line 1237174
    iput-object p1, p0, LX/7mY;->f:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1237175
    iput-object p3, p0, LX/7mY;->g:LX/0ad;

    .line 1237176
    return-void
.end method

.method public static a(LX/0QB;)LX/7mY;
    .locals 6

    .prologue
    .line 1237177
    sget-object v0, LX/7mY;->h:LX/7mY;

    if-nez v0, :cond_1

    .line 1237178
    const-class v1, LX/7mY;

    monitor-enter v1

    .line 1237179
    :try_start_0
    sget-object v0, LX/7mY;->h:LX/7mY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1237180
    if-eqz v2, :cond_0

    .line 1237181
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1237182
    new-instance p0, LX/7mY;

    invoke-static {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/7mY;-><init>(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/0SG;LX/0ad;)V

    .line 1237183
    move-object v0, p0

    .line 1237184
    sput-object v0, LX/7mY;->h:LX/7mY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1237185
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1237186
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1237187
    :cond_1
    sget-object v0, LX/7mY;->h:LX/7mY;

    return-object v0

    .line 1237188
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1237189
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()J
    .locals 2

    .prologue
    .line 1237190
    const-wide/32 v0, 0x127500

    return-wide v0
.end method

.method public final c()LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/7ml;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1237191
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1237192
    iget-object v0, p0, LX/7mY;->g:LX/0ad;

    sget-short v2, LX/1aO;->as:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1237193
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1237194
    :goto_0
    return-object v0

    .line 1237195
    :cond_0
    iget-object v0, p0, LX/7mY;->f:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PendingStory;

    .line 1237196
    invoke-static {v0}, LX/7md;->a(Lcom/facebook/composer/publish/common/PendingStory;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1237197
    new-instance v6, LX/7ml;

    sget-object v7, LX/7mm;->POST:LX/7mm;

    invoke-direct {v6, v0, v7}, LX/7ml;-><init>(Lcom/facebook/composer/publish/common/PendingStory;LX/7mm;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1237198
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1237199
    :cond_2
    :try_start_0
    invoke-virtual {p0}, LX/7mV;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const v2, 0x40179f6e

    invoke-static {v0, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7ml;

    .line 1237200
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1237201
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 1237202
    :catch_0
    move-exception v0

    .line 1237203
    :goto_3
    sget-object v1, LX/7mY;->e:Ljava/lang/String;

    const-string v2, "Failed to add extra stories"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1237204
    :cond_3
    new-instance v0, LX/7mX;

    invoke-direct {v0, p0}, LX/7mX;-><init>(LX/7mY;)V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1237205
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1237206
    :catch_1
    move-exception v0

    goto :goto_3
.end method
