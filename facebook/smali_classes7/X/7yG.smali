.class public final synthetic LX/7yG;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1278838
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->values()[Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/7yG;->a:[I

    :try_start_0
    sget-object v0, LX/7yG;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->PER_TICKET:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    sget-object v0, LX/7yG;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->PER_ORDER:Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLEventRegistrationTargetTypeEnum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    return-void

    :catch_0
    goto :goto_1

    :catch_1
    goto :goto_0
.end method
