.class public LX/7CS;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;


# instance fields
.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1180914
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "GN"

    aput-object v1, v0, v3

    const-string v1, "KN"

    aput-object v1, v0, v4

    const-string v1, "PN"

    aput-object v1, v0, v5

    const-string v1, "WR"

    aput-object v1, v0, v6

    const-string v1, "PS"

    aput-object v1, v0, v7

    sput-object v0, LX/7CS;->a:[Ljava/lang/String;

    .line 1180915
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "L"

    aput-object v1, v0, v3

    const-string v1, "R"

    aput-object v1, v0, v4

    const-string v1, "N"

    aput-object v1, v0, v5

    const-string v1, "M"

    aput-object v1, v0, v6

    const-string v1, "B"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "H"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "F"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "V"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "W"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, " "

    aput-object v2, v0, v1

    sput-object v0, LX/7CS;->b:[Ljava/lang/String;

    .line 1180916
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "ES"

    aput-object v1, v0, v3

    const-string v1, "EP"

    aput-object v1, v0, v4

    const-string v1, "EB"

    aput-object v1, v0, v5

    const-string v1, "EL"

    aput-object v1, v0, v6

    const-string v1, "EY"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "IB"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "IL"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "IN"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "IE"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "EI"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "ER"

    aput-object v2, v0, v1

    sput-object v0, LX/7CS;->c:[Ljava/lang/String;

    .line 1180917
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "L"

    aput-object v1, v0, v3

    const-string v1, "T"

    aput-object v1, v0, v4

    const-string v1, "K"

    aput-object v1, v0, v5

    const-string v1, "S"

    aput-object v1, v0, v6

    const-string v1, "N"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "M"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "B"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Z"

    aput-object v2, v0, v1

    sput-object v0, LX/7CS;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1180911
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1180912
    const/16 v0, 0x14

    iput v0, p0, LX/7CS;->e:I

    .line 1180913
    return-void
.end method

.method public static a(LX/7CS;Ljava/lang/String;LX/7CR;I)I
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/16 v8, 0x58

    const/16 v7, 0x4b

    const/16 v6, 0x53

    const/4 v2, 0x2

    .line 1180852
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1180853
    const/4 v3, 0x4

    const-string v4, "CHIA"

    invoke-static {p1, p3, v3, v4}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1180854
    :cond_0
    :goto_0
    move v0, v0

    .line 1180855
    if-eqz v0, :cond_1

    .line 1180856
    invoke-virtual {p2, v7}, LX/7CR;->a(C)V

    .line 1180857
    add-int/lit8 v0, p3, 0x2

    .line 1180858
    :goto_1
    return v0

    .line 1180859
    :cond_1
    if-nez p3, :cond_2

    const/4 v0, 0x6

    const-string v1, "CAESAR"

    invoke-static {p1, p3, v0, v1}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1180860
    invoke-virtual {p2, v6}, LX/7CR;->a(C)V

    .line 1180861
    add-int/lit8 v0, p3, 0x2

    goto :goto_1

    .line 1180862
    :cond_2
    const-string v0, "CH"

    invoke-static {p1, p3, v2, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1180863
    const/16 v4, 0x58

    const/16 v3, 0x4b

    .line 1180864
    if-lez p3, :cond_12

    const/4 v0, 0x4

    const-string v1, "CHAE"

    invoke-static {p1, p3, v0, v1}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1180865
    invoke-virtual {p2, v3, v4}, LX/7CR;->a(CC)V

    .line 1180866
    add-int/lit8 v0, p3, 0x2

    .line 1180867
    :goto_2
    move v0, v0

    .line 1180868
    goto :goto_1

    .line 1180869
    :cond_3
    const-string v0, "CZ"

    invoke-static {p1, p3, v2, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    add-int/lit8 v0, p3, -0x2

    const/4 v1, 0x4

    const-string v3, "WICZ"

    invoke-static {p1, v0, v1, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1180870
    invoke-virtual {p2, v6, v8}, LX/7CR;->a(CC)V

    .line 1180871
    add-int/lit8 v0, p3, 0x2

    goto :goto_1

    .line 1180872
    :cond_4
    add-int/lit8 v0, p3, 0x1

    const/4 v1, 0x3

    const-string v3, "CIA"

    invoke-static {p1, v0, v1, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1180873
    invoke-virtual {p2, v8}, LX/7CR;->a(C)V

    .line 1180874
    add-int/lit8 v0, p3, 0x3

    goto :goto_1

    .line 1180875
    :cond_5
    const-string v0, "CC"

    invoke-static {p1, p3, v2, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    if-ne p3, v9, :cond_6

    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x4d

    if-eq v0, v1, :cond_7

    .line 1180876
    :cond_6
    invoke-static {p1, p2, p3}, LX/7CS;->b(Ljava/lang/String;LX/7CR;I)I

    move-result v0

    goto :goto_1

    .line 1180877
    :cond_7
    const-string v3, "CK"

    const-string v4, "CG"

    const-string v5, "CQ"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1180878
    invoke-virtual {p2, v7}, LX/7CR;->a(C)V

    .line 1180879
    add-int/lit8 v0, p3, 0x2

    goto/16 :goto_1

    .line 1180880
    :cond_8
    const-string v3, "CI"

    const-string v4, "CE"

    const-string v5, "CY"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1180881
    const/4 v2, 0x3

    const-string v3, "CIO"

    const-string v4, "CIE"

    const-string v5, "CIA"

    move-object v0, p1

    move v1, p3

    invoke-static/range {v0 .. v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1180882
    invoke-virtual {p2, v6, v8}, LX/7CR;->a(CC)V

    .line 1180883
    :goto_3
    add-int/lit8 v0, p3, 0x2

    goto/16 :goto_1

    .line 1180884
    :cond_9
    invoke-virtual {p2, v6}, LX/7CR;->a(C)V

    goto :goto_3

    .line 1180885
    :cond_a
    invoke-virtual {p2, v7}, LX/7CR;->a(C)V

    .line 1180886
    add-int/lit8 v1, p3, 0x1

    const-string v3, " C"

    const-string v4, " Q"

    const-string v5, " G"

    move-object v0, p1

    invoke-static/range {v0 .. v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1180887
    add-int/lit8 v0, p3, 0x3

    goto/16 :goto_1

    .line 1180888
    :cond_b
    add-int/lit8 v4, p3, 0x1

    const-string v6, "C"

    const-string v7, "K"

    const-string v8, "Q"

    move-object v3, p1

    move v5, v9

    invoke-static/range {v3 .. v8}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    add-int/lit8 v0, p3, 0x1

    const-string v1, "CE"

    const-string v3, "CI"

    invoke-static {p1, v0, v2, v1, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 1180889
    add-int/lit8 v0, p3, 0x2

    goto/16 :goto_1

    .line 1180890
    :cond_c
    add-int/lit8 v0, p3, 0x1

    goto/16 :goto_1

    .line 1180891
    :cond_d
    if-gt p3, v0, :cond_e

    move v0, v1

    .line 1180892
    goto/16 :goto_0

    .line 1180893
    :cond_e
    add-int/lit8 v3, p3, -0x2

    invoke-static {p1, v3}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v3

    invoke-static {v3}, LX/7CS;->a(C)Z

    move-result v3

    if-eqz v3, :cond_f

    move v0, v1

    .line 1180894
    goto/16 :goto_0

    .line 1180895
    :cond_f
    add-int/lit8 v3, p3, -0x1

    const/4 v4, 0x3

    const-string v5, "ACH"

    invoke-static {p1, v3, v4, v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_10

    move v0, v1

    .line 1180896
    goto/16 :goto_0

    .line 1180897
    :cond_10
    add-int/lit8 v3, p3, 0x2

    invoke-static {p1, v3}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v3

    .line 1180898
    const/16 v4, 0x49

    if-eq v3, v4, :cond_11

    const/16 v4, 0x45

    if-ne v3, v4, :cond_0

    :cond_11
    add-int/lit8 v3, p3, -0x2

    const/4 v4, 0x6

    const-string v5, "BACHER"

    const-string p0, "MACHER"

    invoke-static {p1, v3, v4, v5, p0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 1180899
    :cond_12
    invoke-static {p1, p3}, LX/7CS;->b(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1180900
    invoke-virtual {p2, v3}, LX/7CR;->a(C)V

    .line 1180901
    add-int/lit8 v0, p3, 0x2

    goto/16 :goto_2

    .line 1180902
    :cond_13
    invoke-static {p1, p3}, LX/7CS;->c(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1180903
    invoke-virtual {p2, v3}, LX/7CR;->a(C)V

    .line 1180904
    add-int/lit8 v0, p3, 0x2

    goto/16 :goto_2

    .line 1180905
    :cond_14
    if-lez p3, :cond_16

    .line 1180906
    const/4 v0, 0x0

    const/4 v1, 0x2

    const-string v2, "MC"

    invoke-static {p1, v0, v1, v2}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1180907
    invoke-virtual {p2, v3}, LX/7CR;->a(C)V

    .line 1180908
    :goto_4
    add-int/lit8 v0, p3, 0x2

    goto/16 :goto_2

    .line 1180909
    :cond_15
    invoke-virtual {p2, v4, v3}, LX/7CR;->a(CC)V

    goto :goto_4

    .line 1180910
    :cond_16
    invoke-virtual {p2, v4}, LX/7CR;->a(C)V

    goto :goto_4
.end method

.method public static a(LX/7CS;Ljava/lang/String;LX/7CR;IZ)I
    .locals 6

    .prologue
    .line 1180824
    add-int/lit8 v0, p3, 0x1

    invoke-static {p1, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x48

    if-ne v0, v1, :cond_0

    .line 1180825
    invoke-static {p1, p2, p3}, LX/7CS;->e(Ljava/lang/String;LX/7CR;I)I

    move-result v0

    .line 1180826
    :goto_0
    return v0

    .line 1180827
    :cond_0
    add-int/lit8 v0, p3, 0x1

    invoke-static {p1, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x4e

    if-ne v0, v1, :cond_3

    .line 1180828
    const/4 v0, 0x1

    if-ne p3, v0, :cond_1

    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    invoke-static {v0}, LX/7CS;->a(C)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p4, :cond_1

    .line 1180829
    const-string v0, "KN"

    const-string v1, "N"

    invoke-virtual {p2, v0, v1}, LX/7CR;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180830
    :goto_1
    add-int/lit8 v0, p3, 0x2

    goto :goto_0

    .line 1180831
    :cond_1
    add-int/lit8 v0, p3, 0x2

    const/4 v1, 0x2

    const-string v2, "EY"

    invoke-static {p1, v0, v1, v2}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    add-int/lit8 v0, p3, 0x1

    invoke-static {p1, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x59

    if-eq v0, v1, :cond_2

    if-nez p4, :cond_2

    .line 1180832
    const-string v0, "N"

    const-string v1, "KN"

    invoke-virtual {p2, v0, v1}, LX/7CR;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1180833
    :cond_2
    const-string v0, "KN"

    invoke-virtual {p2, v0}, LX/7CR;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 1180834
    :cond_3
    add-int/lit8 v0, p3, 0x1

    const/4 v1, 0x2

    const-string v2, "LI"

    invoke-static {p1, v0, v1, v2}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    if-nez p4, :cond_4

    .line 1180835
    const-string v0, "KL"

    const-string v1, "L"

    invoke-virtual {p2, v0, v1}, LX/7CR;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180836
    :goto_2
    add-int/lit8 v0, p3, 0x2

    goto :goto_0

    .line 1180837
    :cond_4
    if-nez p3, :cond_6

    add-int/lit8 v0, p3, 0x1

    invoke-static {p1, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x59

    if-eq v0, v1, :cond_5

    add-int/lit8 v0, p3, 0x1

    const/4 v1, 0x2

    sget-object v2, LX/7CS;->c:[Ljava/lang/String;

    invoke-static {p1, v0, v1, v2}, LX/7CS;->a(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1180838
    :cond_5
    const/16 v0, 0x4b

    const/16 v1, 0x4a

    invoke-virtual {p2, v0, v1}, LX/7CR;->a(CC)V

    goto :goto_2

    .line 1180839
    :cond_6
    add-int/lit8 v0, p3, 0x1

    const/4 v1, 0x2

    const-string v2, "ER"

    invoke-static {p1, v0, v1, v2}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    add-int/lit8 v0, p3, 0x1

    invoke-static {p1, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x59

    if-ne v0, v1, :cond_8

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x6

    const-string v3, "DANGER"

    const-string v4, "RANGER"

    const-string v5, "MANGER"

    move-object v0, p1

    invoke-static/range {v0 .. v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    add-int/lit8 v0, p3, -0x1

    const/4 v1, 0x1

    const-string v2, "E"

    const-string v3, "I"

    invoke-static {p1, v0, v1, v2, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    add-int/lit8 v0, p3, -0x1

    const/4 v1, 0x3

    const-string v2, "RGY"

    const-string v3, "OGY"

    invoke-static {p1, v0, v1, v2, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1180840
    const/16 v0, 0x4b

    const/16 v1, 0x4a

    invoke-virtual {p2, v0, v1}, LX/7CR;->a(CC)V

    goto :goto_2

    .line 1180841
    :cond_8
    add-int/lit8 v1, p3, 0x1

    const/4 v2, 0x1

    const-string v3, "E"

    const-string v4, "I"

    const-string v5, "Y"

    move-object v0, p1

    invoke-static/range {v0 .. v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    add-int/lit8 v0, p3, -0x1

    const/4 v1, 0x4

    const-string v2, "AGGI"

    const-string v3, "OGGI"

    invoke-static {p1, v0, v1, v2, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1180842
    add-int/lit8 v0, p3, 0x1

    invoke-static {p1, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x47

    if-ne v0, v1, :cond_9

    .line 1180843
    add-int/lit8 v0, p3, 0x2

    .line 1180844
    const/16 v1, 0x4b

    invoke-virtual {p2, v1}, LX/7CR;->a(C)V

    goto/16 :goto_0

    .line 1180845
    :cond_9
    add-int/lit8 v0, p3, 0x1

    .line 1180846
    const/16 v1, 0x4b

    invoke-virtual {p2, v1}, LX/7CR;->a(C)V

    goto/16 :goto_0

    .line 1180847
    :cond_a
    const/4 v0, 0x0

    const/4 v1, 0x4

    const-string v2, "VAN "

    const-string v3, "VON "

    invoke-static {p1, v0, v1, v2, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x0

    const/4 v1, 0x3

    const-string v2, "SCH"

    invoke-static {p1, v0, v1, v2}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    add-int/lit8 v0, p3, 0x1

    const/4 v1, 0x2

    const-string v2, "ET"

    invoke-static {p1, v0, v1, v2}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 1180848
    add-int/lit8 v0, p3, 0x1

    const/4 v1, 0x3

    const-string v2, "IER"

    invoke-static {p1, v0, v1, v2}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1180849
    const/16 v0, 0x4a

    invoke-virtual {p2, v0}, LX/7CR;->a(C)V

    goto/16 :goto_2

    .line 1180850
    :cond_b
    const/16 v0, 0x4a

    const/16 v1, 0x4b

    invoke-virtual {p2, v0, v1}, LX/7CR;->a(CC)V

    goto/16 :goto_2

    .line 1180851
    :cond_c
    const/16 v0, 0x4b

    invoke-virtual {p2, v0}, LX/7CR;->a(C)V

    goto/16 :goto_2
.end method

.method public static a(C)Z
    .locals 2

    .prologue
    .line 1180823
    const-string v0, "AEIOUY"

    invoke-virtual {v0, p0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;IILjava/lang/String;)Z
    .locals 2

    .prologue
    .line 1180822
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    invoke-static {p0, p1, p2, v0}, LX/7CS;->a(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1180821
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    const/4 v1, 0x1

    aput-object p4, v0, v1

    invoke-static {p0, p1, p2, v0}, LX/7CS;->a(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1180820
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    const/4 v1, 0x1

    aput-object p4, v0, v1

    const/4 v1, 0x2

    aput-object p5, v0, v1

    invoke-static {p0, p1, p2, v0}, LX/7CS;->a(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1180819
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    const/4 v1, 0x1

    aput-object p4, v0, v1

    const/4 v1, 0x2

    aput-object p5, v0, v1

    const/4 v1, 0x3

    aput-object p6, v0, v1

    invoke-static {p0, p1, p2, v0}, LX/7CS;->a(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1180818
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    const/4 v1, 0x1

    aput-object p4, v0, v1

    const/4 v1, 0x2

    aput-object p5, v0, v1

    const/4 v1, 0x3

    aput-object p6, v0, v1

    const/4 v1, 0x4

    aput-object p7, v0, v1

    const/4 v1, 0x5

    aput-object p8, v0, v1

    invoke-static {p0, p1, p2, v0}, LX/7CS;->a(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/lang/String;II[Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1180811
    if-ltz p1, :cond_0

    add-int v0, p1, p2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v0, v2, :cond_0

    .line 1180812
    add-int v0, p1, p2

    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    move v0, v1

    .line 1180813
    :goto_0
    array-length v3, p3

    if-ge v0, v3, :cond_0

    .line 1180814
    aget-object v3, p3, v0

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1180815
    const/4 v1, 0x1

    .line 1180816
    :cond_0
    return v1

    .line 1180817
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;LX/7CR;I)I
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 1180803
    add-int/lit8 v1, p2, 0x2

    const-string v3, "I"

    const-string v4, "E"

    const-string v5, "H"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 v0, p2, 0x2

    const/4 v1, 0x2

    const-string v3, "HU"

    invoke-static {p0, v0, v1, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1180804
    if-ne p2, v2, :cond_0

    add-int/lit8 v0, p2, -0x1

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x41

    if-eq v0, v1, :cond_1

    :cond_0
    add-int/lit8 v0, p2, -0x1

    const/4 v1, 0x5

    const-string v2, "UCCEE"

    const-string v3, "UCCES"

    invoke-static {p0, v0, v1, v2, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1180805
    const/16 v0, 0x58

    invoke-virtual {p1, v0}, LX/7CR;->a(C)V

    .line 1180806
    :goto_0
    add-int/lit8 v0, p2, 0x3

    .line 1180807
    :goto_1
    return v0

    .line 1180808
    :cond_1
    const-string v0, "KS"

    invoke-virtual {p1, v0}, LX/7CR;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1180809
    :cond_2
    const/16 v0, 0x4b

    invoke-virtual {p1, v0}, LX/7CR;->a(C)V

    .line 1180810
    add-int/lit8 v0, p2, 0x2

    goto :goto_1
.end method

.method public static b(Ljava/lang/String;LX/7CR;IZ)I
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    const/16 v4, 0x48

    const/4 v3, 0x4

    const/16 v6, 0x4a

    .line 1180786
    const-string v0, "JOSE"

    invoke-static {p0, p2, v3, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "SAN "

    invoke-static {p0, v5, v3, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1180787
    if-nez p2, :cond_1

    const-string v0, "JOSE"

    invoke-static {p0, p2, v3, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1180788
    const/16 v0, 0x41

    invoke-virtual {p1, v6, v0}, LX/7CR;->a(CC)V

    .line 1180789
    :cond_0
    :goto_0
    add-int/lit8 v0, p2, 0x1

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v6, :cond_5

    .line 1180790
    add-int/lit8 v0, p2, 0x2

    .line 1180791
    :goto_1
    return v0

    .line 1180792
    :cond_1
    add-int/lit8 v0, p2, -0x1

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    invoke-static {v0}, LX/7CS;->a(C)Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez p3, :cond_3

    add-int/lit8 v0, p2, 0x1

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x41

    if-eq v0, v1, :cond_2

    add-int/lit8 v0, p2, 0x1

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x4f

    if-ne v0, v1, :cond_3

    .line 1180793
    :cond_2
    invoke-virtual {p1, v6, v4}, LX/7CR;->a(CC)V

    goto :goto_0

    .line 1180794
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_4

    .line 1180795
    const/16 v0, 0x20

    invoke-virtual {p1, v6, v0}, LX/7CR;->a(CC)V

    goto :goto_0

    .line 1180796
    :cond_4
    add-int/lit8 v0, p2, 0x1

    sget-object v1, LX/7CS;->d:[Ljava/lang/String;

    invoke-static {p0, v0, v2, v1}, LX/7CS;->a(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    add-int/lit8 v1, p2, -0x1

    const-string v3, "S"

    const-string v4, "K"

    const-string v5, "L"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1180797
    invoke-virtual {p1, v6}, LX/7CR;->a(C)V

    goto :goto_0

    .line 1180798
    :cond_5
    add-int/lit8 v0, p2, 0x1

    goto :goto_1

    .line 1180799
    :cond_6
    if-nez p2, :cond_7

    add-int/lit8 v0, p2, 0x4

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_8

    :cond_7
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eq v0, v3, :cond_8

    const-string v0, "SAN "

    invoke-static {p0, v5, v3, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1180800
    invoke-virtual {p1, v6, v4}, LX/7CR;->a(CC)V

    .line 1180801
    :goto_2
    add-int/lit8 v0, p2, 0x1

    goto :goto_1

    .line 1180802
    :cond_8
    invoke-virtual {p1, v4}, LX/7CR;->a(C)V

    goto :goto_2
.end method

.method public static b(Ljava/lang/String;I)Z
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x0

    .line 1180578
    if-nez p1, :cond_1

    add-int/lit8 v0, p1, 0x1

    const-string v1, "HARAC"

    const-string v2, "HARIS"

    invoke-static {p0, v0, v8, v1, v2}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    add-int/lit8 v1, p1, 0x1

    const/4 v2, 0x3

    const-string v3, "HOR"

    const-string v4, "HYM"

    const-string v5, "HIA"

    const-string v6, "HEM"

    move-object v0, p0

    invoke-static/range {v0 .. v6}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "CHORE"

    invoke-static {p0, v7, v8, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v7

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;I)Z
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 1180579
    const/4 v0, 0x4

    const-string v1, "VAN "

    const-string v2, "VON "

    invoke-static {p0, v7, v0, v1, v2}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x3

    const-string v1, "SCH"

    invoke-static {p0, v7, v0, v1}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    add-int/lit8 v1, p1, -0x2

    const/4 v2, 0x6

    const-string v3, "ORCHES"

    const-string v4, "ARCHIT"

    const-string v5, "ORCHID"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    add-int/lit8 v0, p1, 0x2

    const-string v1, "T"

    const-string v2, "S"

    invoke-static {p0, v0, v8, v1, v2}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    add-int/lit8 v1, p1, -0x1

    const-string v3, "A"

    const-string v4, "O"

    const-string v5, "U"

    const-string v6, "E"

    move-object v0, p0

    move v2, v8

    invoke-static/range {v0 .. v6}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    add-int/lit8 v0, p1, 0x2

    sget-object v1, LX/7CS;->b:[Ljava/lang/String;

    invoke-static {p0, v0, v8, v1}, LX/7CS;->a(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_2

    :cond_1
    move v0, v8

    :goto_0
    return v0

    :cond_2
    move v0, v7

    goto :goto_0
.end method

.method public static d(LX/7CS;Ljava/lang/String;LX/7CR;IZ)I
    .locals 10

    .prologue
    const/4 v4, 0x3

    const/4 v9, 0x2

    const/16 v8, 0x58

    const/4 v2, 0x1

    const/16 v7, 0x53

    .line 1180580
    add-int/lit8 v0, p3, -0x1

    const-string v1, "ISL"

    const-string v3, "YSL"

    invoke-static {p1, v0, v4, v1, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1180581
    add-int/lit8 v0, p3, 0x1

    .line 1180582
    :goto_0
    return v0

    .line 1180583
    :cond_0
    if-nez p3, :cond_1

    const/4 v0, 0x5

    const-string v1, "SUGAR"

    invoke-static {p1, p3, v0, v1}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1180584
    invoke-virtual {p2, v8, v7}, LX/7CR;->a(CC)V

    .line 1180585
    add-int/lit8 v0, p3, 0x1

    goto :goto_0

    .line 1180586
    :cond_1
    const-string v0, "SH"

    invoke-static {p1, p3, v9, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1180587
    add-int/lit8 v1, p3, 0x1

    const/4 v2, 0x4

    const-string v3, "HEIM"

    const-string v4, "HOEK"

    const-string v5, "HOLM"

    const-string v6, "HOLZ"

    move-object v0, p1

    invoke-static/range {v0 .. v6}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1180588
    invoke-virtual {p2, v7}, LX/7CR;->a(C)V

    .line 1180589
    :goto_1
    add-int/lit8 v0, p3, 0x2

    goto :goto_0

    .line 1180590
    :cond_2
    invoke-virtual {p2, v8}, LX/7CR;->a(C)V

    goto :goto_1

    .line 1180591
    :cond_3
    const-string v0, "SIO"

    const-string v1, "SIA"

    invoke-static {p1, p3, v4, v0, v1}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x4

    const-string v1, "SIAN"

    invoke-static {p1, p3, v0, v1}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1180592
    if-nez p3, :cond_4

    add-int/lit8 v1, p3, 0x1

    const-string v3, "M"

    const-string v4, "N"

    const-string v5, "L"

    const-string v6, "W"

    move-object v0, p1

    invoke-static/range {v0 .. v6}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    :cond_4
    add-int/lit8 v0, p3, 0x1

    const-string v1, "Z"

    invoke-static {p1, v0, v2, v1}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1180593
    const-string v0, "SC"

    invoke-static {p1, p3, v9, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1180594
    invoke-static {p1, p2, p3}, LX/7CS;->i(Ljava/lang/String;LX/7CR;I)I

    move-result v0

    goto :goto_0

    .line 1180595
    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p3, v0, :cond_6

    add-int/lit8 v0, p3, -0x2

    const-string v1, "AI"

    const-string v3, "OI"

    invoke-static {p1, v0, v9, v1, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1180596
    invoke-virtual {p2, v7}, LX/7CR;->c(C)V

    .line 1180597
    :goto_2
    add-int/lit8 v0, p3, 0x1

    const-string v1, "S"

    const-string v3, "Z"

    invoke-static {p1, v0, v2, v1, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    add-int/lit8 v0, p3, 0x2

    goto/16 :goto_0

    .line 1180598
    :cond_6
    invoke-virtual {p2, v7}, LX/7CR;->a(C)V

    goto :goto_2

    .line 1180599
    :cond_7
    add-int/lit8 v0, p3, 0x1

    goto/16 :goto_0

    .line 1180600
    :cond_8
    invoke-virtual {p2, v7, v8}, LX/7CR;->a(CC)V

    .line 1180601
    add-int/lit8 v0, p3, 0x1

    const-string v1, "Z"

    invoke-static {p1, v0, v2, v1}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    add-int/lit8 v0, p3, 0x2

    goto/16 :goto_0

    :cond_9
    add-int/lit8 v0, p3, 0x1

    goto/16 :goto_0

    .line 1180602
    :cond_a
    if-eqz p4, :cond_b

    .line 1180603
    invoke-virtual {p2, v7}, LX/7CR;->a(C)V

    .line 1180604
    :goto_3
    add-int/lit8 v0, p3, 0x3

    goto/16 :goto_0

    .line 1180605
    :cond_b
    invoke-virtual {p2, v7, v8}, LX/7CR;->a(CC)V

    goto :goto_3
.end method

.method public static d(Ljava/lang/String;LX/7CR;I)I
    .locals 6

    .prologue
    const/16 v3, 0x54

    const/4 v2, 0x2

    .line 1180606
    const-string v0, "DG"

    invoke-static {p0, p2, v2, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1180607
    add-int/lit8 v1, p2, 0x2

    const/4 v2, 0x1

    const-string v3, "I"

    const-string v4, "E"

    const-string v5, "Y"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1180608
    const/16 v0, 0x4a

    invoke-virtual {p1, v0}, LX/7CR;->a(C)V

    .line 1180609
    add-int/lit8 v0, p2, 0x3

    .line 1180610
    :goto_0
    return v0

    .line 1180611
    :cond_0
    const-string v0, "TK"

    invoke-virtual {p1, v0}, LX/7CR;->a(Ljava/lang/String;)V

    .line 1180612
    add-int/lit8 v0, p2, 0x2

    goto :goto_0

    .line 1180613
    :cond_1
    const-string v0, "DT"

    const-string v1, "DD"

    invoke-static {p0, p2, v2, v0, v1}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1180614
    invoke-virtual {p1, v3}, LX/7CR;->a(C)V

    .line 1180615
    add-int/lit8 v0, p2, 0x2

    goto :goto_0

    .line 1180616
    :cond_2
    invoke-virtual {p1, v3}, LX/7CR;->a(C)V

    .line 1180617
    add-int/lit8 v0, p2, 0x1

    goto :goto_0
.end method

.method private static e(Ljava/lang/String;LX/7CR;I)I
    .locals 12

    .prologue
    const/16 v9, 0x49

    const/4 v6, 0x2

    const/16 v8, 0x4b

    const/4 v2, 0x1

    .line 1180618
    if-lez p2, :cond_1

    add-int/lit8 v0, p2, -0x1

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    invoke-static {v0}, LX/7CS;->a(C)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1180619
    invoke-virtual {p1, v8}, LX/7CR;->a(C)V

    .line 1180620
    :cond_0
    add-int/lit8 v0, p2, 0x2

    .line 1180621
    :goto_0
    return v0

    .line 1180622
    :cond_1
    if-nez p2, :cond_3

    .line 1180623
    add-int/lit8 v0, p2, 0x2

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v9, :cond_2

    .line 1180624
    const/16 v0, 0x4a

    invoke-virtual {p1, v0}, LX/7CR;->a(C)V

    .line 1180625
    :goto_1
    add-int/lit8 v0, p2, 0x2

    goto :goto_0

    .line 1180626
    :cond_2
    invoke-virtual {p1, v8}, LX/7CR;->a(C)V

    goto :goto_1

    .line 1180627
    :cond_3
    if-le p2, v2, :cond_4

    add-int/lit8 v1, p2, -0x2

    const-string v3, "B"

    const-string v4, "H"

    const-string v5, "D"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_4
    if-le p2, v6, :cond_5

    add-int/lit8 v1, p2, -0x3

    const-string v3, "B"

    const-string v4, "H"

    const-string v5, "D"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    const/4 v0, 0x3

    if-le p2, v0, :cond_6

    add-int/lit8 v0, p2, -0x4

    const-string v1, "B"

    const-string v3, "H"

    invoke-static {p0, v0, v2, v1, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1180628
    :cond_6
    if-le p2, v6, :cond_8

    add-int/lit8 v0, p2, -0x1

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x55

    if-ne v0, v1, :cond_8

    add-int/lit8 v1, p2, -0x3

    const-string v3, "C"

    const-string v4, "G"

    const-string v5, "L"

    const-string v6, "R"

    const-string v7, "T"

    move-object v0, p0

    .line 1180629
    const/4 v10, 0x5

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v3, v10, v11

    const/4 v11, 0x1

    aput-object v4, v10, v11

    const/4 v11, 0x2

    aput-object v5, v10, v11

    const/4 v11, 0x3

    aput-object v6, v10, v11

    const/4 v11, 0x4

    aput-object v7, v10, v11

    invoke-static {v0, v1, v2, v10}, LX/7CS;->a(Ljava/lang/String;II[Ljava/lang/String;)Z

    move-result v10

    move v0, v10

    .line 1180630
    if-eqz v0, :cond_8

    .line 1180631
    const/16 v0, 0x46

    invoke-virtual {p1, v0}, LX/7CR;->a(C)V

    .line 1180632
    :cond_7
    :goto_2
    add-int/lit8 v0, p2, 0x2

    goto/16 :goto_0

    .line 1180633
    :cond_8
    if-lez p2, :cond_7

    add-int/lit8 v0, p2, -0x1

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    if-eq v0, v9, :cond_7

    .line 1180634
    invoke-virtual {p1, v8}, LX/7CR;->a(C)V

    goto :goto_2
.end method

.method public static e(Ljava/lang/String;LX/7CR;IZ)I
    .locals 6

    .prologue
    .line 1180635
    add-int/lit8 v0, p2, 0x1

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x48

    if-ne v0, v1, :cond_0

    .line 1180636
    const/16 v0, 0x4a

    invoke-virtual {p1, v0}, LX/7CR;->a(C)V

    .line 1180637
    add-int/lit8 v0, p2, 0x2

    .line 1180638
    :goto_0
    return v0

    .line 1180639
    :cond_0
    add-int/lit8 v1, p2, 0x1

    const/4 v2, 0x2

    const-string v3, "ZO"

    const-string v4, "ZI"

    const-string v5, "ZA"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p3, :cond_1

    if-lez p2, :cond_1

    add-int/lit8 v0, p2, -0x1

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x54

    if-ne v0, v1, :cond_2

    .line 1180640
    :cond_1
    const/16 v0, 0x53

    invoke-virtual {p1, v0}, LX/7CR;->a(C)V

    .line 1180641
    :goto_1
    add-int/lit8 v0, p2, 0x1

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x5a

    if-ne v0, v1, :cond_3

    add-int/lit8 v0, p2, 0x2

    goto :goto_0

    .line 1180642
    :cond_2
    const-string v0, "S"

    const-string v1, "TS"

    invoke-virtual {p1, v0, v1}, LX/7CR;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1180643
    :cond_3
    add-int/lit8 v0, p2, 0x1

    goto :goto_0
.end method

.method public static f(Ljava/lang/String;I)C
    .locals 1

    .prologue
    .line 1180644
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge p1, v0, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Ljava/lang/String;LX/7CR;I)I
    .locals 9

    .prologue
    const/16 v1, 0x4c

    .line 1180645
    add-int/lit8 v0, p2, 0x1

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1180646
    const/4 v4, 0x4

    const/4 v8, 0x1

    .line 1180647
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    if-ne p2, v2, :cond_2

    add-int/lit8 v3, p2, -0x1

    const-string v5, "ILLO"

    const-string v6, "ILLA"

    const-string v7, "ALLE"

    move-object v2, p0

    invoke-static/range {v2 .. v7}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v8

    :goto_0
    move v0, v2

    .line 1180648
    if-eqz v0, :cond_0

    .line 1180649
    invoke-virtual {p1, v1}, LX/7CR;->b(C)V

    .line 1180650
    :goto_1
    add-int/lit8 v0, p2, 0x2

    .line 1180651
    :goto_2
    return v0

    .line 1180652
    :cond_0
    invoke-virtual {p1, v1}, LX/7CR;->a(C)V

    goto :goto_1

    .line 1180653
    :cond_1
    add-int/lit8 v0, p2, 0x1

    .line 1180654
    invoke-virtual {p1, v1}, LX/7CR;->a(C)V

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    const/4 v3, 0x2

    const-string v5, "AS"

    const-string v6, "OS"

    invoke-static {p0, v2, v3, v5, v6}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const-string v3, "A"

    const-string v5, "O"

    invoke-static {p0, v2, v8, v3, v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    add-int/lit8 v2, p2, -0x1

    const-string v3, "ALLE"

    invoke-static {p0, v2, v4, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, v8

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static i(Ljava/lang/String;LX/7CR;I)I
    .locals 12

    .prologue
    const/16 v11, 0x58

    const/16 v10, 0x53

    const/4 v9, 0x3

    const/4 v2, 0x2

    .line 1180655
    add-int/lit8 v0, p2, 0x2

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x48

    if-ne v0, v1, :cond_3

    .line 1180656
    add-int/lit8 v1, p2, 0x3

    const-string v3, "OO"

    const-string v4, "ER"

    const-string v5, "EN"

    const-string v6, "UY"

    const-string v7, "ED"

    const-string v8, "EM"

    move-object v0, p0

    invoke-static/range {v0 .. v8}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1180657
    add-int/lit8 v0, p2, 0x3

    const-string v1, "ER"

    const-string v3, "EN"

    invoke-static {p0, v0, v2, v1, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1180658
    const-string v0, "X"

    const-string v1, "SK"

    invoke-virtual {p1, v0, v1}, LX/7CR;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180659
    :goto_0
    add-int/lit8 v0, p2, 0x3

    return v0

    .line 1180660
    :cond_0
    const-string v0, "SK"

    invoke-virtual {p1, v0}, LX/7CR;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1180661
    :cond_1
    if-nez p2, :cond_2

    invoke-static {p0, v9}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    invoke-static {v0}, LX/7CS;->a(C)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p0, v9}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    const/16 v1, 0x57

    if-eq v0, v1, :cond_2

    .line 1180662
    invoke-virtual {p1, v11, v10}, LX/7CR;->a(CC)V

    goto :goto_0

    .line 1180663
    :cond_2
    invoke-virtual {p1, v11}, LX/7CR;->a(C)V

    goto :goto_0

    .line 1180664
    :cond_3
    add-int/lit8 v1, p2, 0x2

    const/4 v2, 0x1

    const-string v3, "I"

    const-string v4, "E"

    const-string v5, "Y"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1180665
    invoke-virtual {p1, v10}, LX/7CR;->a(C)V

    goto :goto_0

    .line 1180666
    :cond_4
    const-string v0, "SK"

    invoke-virtual {p1, v0}, LX/7CR;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static j(Ljava/lang/String;LX/7CR;I)I
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v4, 0x54

    const/4 v3, 0x3

    .line 1180667
    const-string v0, "TION"

    invoke-static {p0, p2, v7, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1180668
    const/16 v0, 0x58

    invoke-virtual {p1, v0}, LX/7CR;->a(C)V

    .line 1180669
    add-int/lit8 v0, p2, 0x3

    .line 1180670
    :goto_0
    return v0

    .line 1180671
    :cond_0
    const-string v0, "TIA"

    const-string v1, "TCH"

    invoke-static {p0, p2, v3, v0, v1}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1180672
    const/16 v0, 0x58

    invoke-virtual {p1, v0}, LX/7CR;->a(C)V

    .line 1180673
    add-int/lit8 v0, p2, 0x3

    goto :goto_0

    .line 1180674
    :cond_1
    const-string v0, "TH"

    invoke-static {p0, p2, v6, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "TTH"

    invoke-static {p0, p2, v3, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1180675
    invoke-virtual {p1, v4}, LX/7CR;->a(C)V

    .line 1180676
    add-int/lit8 v0, p2, 0x1

    const/4 v1, 0x1

    const-string v2, "T"

    const-string v3, "D"

    invoke-static {p0, v0, v1, v2, v3}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 v0, p2, 0x2

    goto :goto_0

    :cond_2
    add-int/lit8 v0, p2, 0x1

    goto :goto_0

    .line 1180677
    :cond_3
    add-int/lit8 v0, p2, 0x2

    const-string v1, "OM"

    const-string v2, "AM"

    invoke-static {p0, v0, v6, v1, v2}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "VAN "

    const-string v1, "VON "

    invoke-static {p0, v5, v7, v0, v1}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "SCH"

    invoke-static {p0, v5, v3, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1180678
    const/16 v0, 0x30

    invoke-virtual {p1, v0, v4}, LX/7CR;->a(CC)V

    .line 1180679
    :goto_1
    add-int/lit8 v0, p2, 0x2

    goto :goto_0

    .line 1180680
    :cond_4
    invoke-virtual {p1, v4}, LX/7CR;->a(C)V

    goto :goto_1
.end method

.method public static k(Ljava/lang/String;LX/7CR;I)I
    .locals 8

    .prologue
    const/16 v7, 0x46

    const/16 v2, 0x41

    const/4 v1, 0x2

    .line 1180681
    const-string v0, "WR"

    invoke-static {p0, p2, v1, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1180682
    const/16 v0, 0x52

    invoke-virtual {p1, v0}, LX/7CR;->a(C)V

    .line 1180683
    add-int/lit8 v0, p2, 0x2

    .line 1180684
    :goto_0
    return v0

    .line 1180685
    :cond_0
    if-nez p2, :cond_3

    add-int/lit8 v0, p2, 0x1

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    invoke-static {v0}, LX/7CS;->a(C)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "WH"

    invoke-static {p0, p2, v1, v0}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1180686
    :cond_1
    add-int/lit8 v0, p2, 0x1

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    invoke-static {v0}, LX/7CS;->a(C)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1180687
    invoke-virtual {p1, v2, v7}, LX/7CR;->a(CC)V

    .line 1180688
    :goto_1
    add-int/lit8 v0, p2, 0x1

    goto :goto_0

    .line 1180689
    :cond_2
    invoke-virtual {p1, v2}, LX/7CR;->a(C)V

    goto :goto_1

    .line 1180690
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_4

    add-int/lit8 v0, p2, -0x1

    invoke-static {p0, v0}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v0

    invoke-static {v0}, LX/7CS;->a(C)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_4
    add-int/lit8 v1, p2, -0x1

    const/4 v2, 0x5

    const-string v3, "EWSKI"

    const-string v4, "EWSKY"

    const-string v5, "OWSKI"

    const-string v6, "OWSKY"

    move-object v0, p0

    invoke-static/range {v0 .. v6}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x0

    const/4 v1, 0x3

    const-string v2, "SCH"

    invoke-static {p0, v0, v1, v2}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1180691
    const/4 v0, 0x4

    const-string v1, "WICZ"

    const-string v2, "WITZ"

    invoke-static {p0, p2, v0, v1, v2}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1180692
    const-string v0, "TS"

    const-string v1, "FX"

    invoke-virtual {p1, v0, v1}, LX/7CR;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180693
    add-int/lit8 v0, p2, 0x4

    goto :goto_0

    .line 1180694
    :cond_5
    add-int/lit8 v0, p2, 0x1

    goto :goto_0

    .line 1180695
    :cond_6
    invoke-virtual {p1, v7}, LX/7CR;->c(C)V

    .line 1180696
    add-int/lit8 v0, p2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    .line 1180697
    const/4 v0, 0x0

    const/16 v9, 0x4e

    const/16 v8, 0x4b

    const/16 v7, 0x46

    .line 1180698
    const/4 v1, 0x0

    .line 1180699
    if-nez p1, :cond_11

    .line 1180700
    :cond_0
    :goto_0
    move-object v2, v1

    .line 1180701
    if-nez v2, :cond_1

    .line 1180702
    const/4 v1, 0x0

    .line 1180703
    :goto_1
    move-object v0, v1

    .line 1180704
    return-object v0

    .line 1180705
    :cond_1
    const/16 v1, 0x57

    invoke-virtual {v2, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-gez v1, :cond_2

    const/16 v1, 0x4b

    invoke-virtual {v2, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-gez v1, :cond_2

    const-string v1, "CZ"

    invoke-virtual {v2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_2

    const-string v1, "WITZ"

    invoke-virtual {v2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_12

    :cond_2
    const/4 v1, 0x1

    :goto_2
    move v3, v1

    .line 1180706
    const/4 v4, 0x0

    .line 1180707
    move v1, v4

    .line 1180708
    :goto_3
    sget-object v5, LX/7CS;->a:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_3

    .line 1180709
    sget-object v5, LX/7CS;->a:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 1180710
    const/4 v4, 0x1

    .line 1180711
    :cond_3
    move v1, v4

    .line 1180712
    if-eqz v1, :cond_4

    const/4 v1, 0x1

    .line 1180713
    :goto_4
    new-instance v4, LX/7CR;

    .line 1180714
    iget v5, p0, LX/7CS;->e:I

    move v5, v5

    .line 1180715
    invoke-direct {v4, p0, v5}, LX/7CR;-><init>(LX/7CS;I)V

    .line 1180716
    :goto_5
    iget-object v5, v4, LX/7CR;->b:Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    iget v6, v4, LX/7CR;->d:I

    if-lt v5, v6, :cond_14

    iget-object v5, v4, LX/7CR;->c:Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    iget v6, v4, LX/7CR;->d:I

    if-lt v5, v6, :cond_14

    const/4 v5, 0x1

    :goto_6
    move v5, v5

    .line 1180717
    if-nez v5, :cond_f

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-gt v1, v5, :cond_f

    .line 1180718
    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 1180719
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1180720
    :cond_4
    const/4 v1, 0x0

    goto :goto_4

    .line 1180721
    :pswitch_0
    if-nez v1, :cond_5

    .line 1180722
    const/16 v5, 0x41

    invoke-virtual {v4, v5}, LX/7CR;->a(C)V

    .line 1180723
    :cond_5
    add-int/lit8 v5, v1, 0x1

    move v1, v5

    .line 1180724
    goto :goto_5

    .line 1180725
    :pswitch_1
    const/16 v5, 0x50

    invoke-virtual {v4, v5}, LX/7CR;->a(C)V

    .line 1180726
    add-int/lit8 v5, v1, 0x1

    invoke-static {v2, v5}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v5

    const/16 v6, 0x42

    if-ne v5, v6, :cond_6

    add-int/lit8 v1, v1, 0x2

    goto :goto_5

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1180727
    :pswitch_2
    invoke-static {p0, v2, v4, v1}, LX/7CS;->a(LX/7CS;Ljava/lang/String;LX/7CR;I)I

    move-result v1

    goto :goto_5

    .line 1180728
    :pswitch_3
    invoke-static {v2, v4, v1}, LX/7CS;->d(Ljava/lang/String;LX/7CR;I)I

    move-result v1

    goto :goto_5

    .line 1180729
    :pswitch_4
    invoke-virtual {v4, v7}, LX/7CR;->a(C)V

    .line 1180730
    add-int/lit8 v5, v1, 0x1

    invoke-static {v2, v5}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v5

    if-ne v5, v7, :cond_7

    add-int/lit8 v1, v1, 0x2

    goto :goto_5

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1180731
    :pswitch_5
    invoke-static {p0, v2, v4, v1, v3}, LX/7CS;->a(LX/7CS;Ljava/lang/String;LX/7CR;IZ)I

    move-result v1

    goto :goto_5

    .line 1180732
    :pswitch_6
    if-eqz v1, :cond_8

    add-int/lit8 v5, v1, -0x1

    invoke-static {v2, v5}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v5

    invoke-static {v5}, LX/7CS;->a(C)Z

    move-result v5

    if-eqz v5, :cond_15

    :cond_8
    add-int/lit8 v5, v1, 0x1

    invoke-static {v2, v5}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v5

    invoke-static {v5}, LX/7CS;->a(C)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 1180733
    const/16 v5, 0x48

    invoke-virtual {v4, v5}, LX/7CR;->a(C)V

    .line 1180734
    add-int/lit8 v5, v1, 0x2

    .line 1180735
    :goto_7
    move v1, v5

    .line 1180736
    goto/16 :goto_5

    .line 1180737
    :pswitch_7
    invoke-static {v2, v4, v1, v3}, LX/7CS;->b(Ljava/lang/String;LX/7CR;IZ)I

    move-result v1

    goto/16 :goto_5

    .line 1180738
    :pswitch_8
    invoke-virtual {v4, v8}, LX/7CR;->a(C)V

    .line 1180739
    add-int/lit8 v5, v1, 0x1

    invoke-static {v2, v5}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v5

    if-ne v5, v8, :cond_9

    add-int/lit8 v1, v1, 0x2

    goto/16 :goto_5

    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_5

    .line 1180740
    :pswitch_9
    invoke-static {v2, v4, v1}, LX/7CS;->g(Ljava/lang/String;LX/7CR;I)I

    move-result v1

    goto/16 :goto_5

    .line 1180741
    :pswitch_a
    const/16 v5, 0x4d

    invoke-virtual {v4, v5}, LX/7CR;->a(C)V

    .line 1180742
    const/4 v5, 0x1

    .line 1180743
    add-int/lit8 v6, v1, 0x1

    invoke-static {v2, v6}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v6

    const/16 v10, 0x4d

    if-ne v6, v10, :cond_16

    :cond_a
    :goto_8
    move v5, v5

    .line 1180744
    if-eqz v5, :cond_b

    add-int/lit8 v1, v1, 0x2

    goto/16 :goto_5

    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_5

    .line 1180745
    :pswitch_b
    invoke-virtual {v4, v9}, LX/7CR;->a(C)V

    .line 1180746
    add-int/lit8 v5, v1, 0x1

    invoke-static {v2, v5}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v5

    if-ne v5, v9, :cond_c

    add-int/lit8 v1, v1, 0x2

    goto/16 :goto_5

    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_5

    .line 1180747
    :pswitch_c
    add-int/lit8 v5, v1, 0x1

    invoke-static {v2, v5}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v5

    const/16 v6, 0x48

    if-ne v5, v6, :cond_18

    .line 1180748
    const/16 v5, 0x46

    invoke-virtual {v4, v5}, LX/7CR;->a(C)V

    .line 1180749
    add-int/lit8 v5, v1, 0x2

    .line 1180750
    :goto_9
    move v1, v5

    .line 1180751
    goto/16 :goto_5

    .line 1180752
    :pswitch_d
    invoke-virtual {v4, v8}, LX/7CR;->a(C)V

    .line 1180753
    add-int/lit8 v5, v1, 0x1

    invoke-static {v2, v5}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v5

    const/16 v6, 0x51

    if-ne v5, v6, :cond_d

    add-int/lit8 v1, v1, 0x2

    goto/16 :goto_5

    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_5

    .line 1180754
    :pswitch_e
    const/4 p1, 0x2

    const/16 v11, 0x52

    .line 1180755
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne v1, v5, :cond_1a

    if-nez v3, :cond_1a

    add-int/lit8 v5, v1, -0x2

    const-string v6, "IE"

    invoke-static {v2, v5, p1, v6}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1a

    add-int/lit8 v5, v1, -0x4

    const-string v6, "ME"

    const-string v10, "MA"

    invoke-static {v2, v5, p1, v6, v10}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1a

    .line 1180756
    invoke-virtual {v4, v11}, LX/7CR;->c(C)V

    .line 1180757
    :goto_a
    add-int/lit8 v5, v1, 0x1

    invoke-static {v2, v5}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v5

    if-ne v5, v11, :cond_1b

    add-int/lit8 v5, v1, 0x2

    :goto_b
    move v1, v5

    .line 1180758
    goto/16 :goto_5

    .line 1180759
    :pswitch_f
    invoke-static {p0, v2, v4, v1, v3}, LX/7CS;->d(LX/7CS;Ljava/lang/String;LX/7CR;IZ)I

    move-result v1

    goto/16 :goto_5

    .line 1180760
    :pswitch_10
    invoke-static {v2, v4, v1}, LX/7CS;->j(Ljava/lang/String;LX/7CR;I)I

    move-result v1

    goto/16 :goto_5

    .line 1180761
    :pswitch_11
    invoke-virtual {v4, v7}, LX/7CR;->a(C)V

    .line 1180762
    add-int/lit8 v5, v1, 0x1

    invoke-static {v2, v5}, LX/7CS;->f(Ljava/lang/String;I)C

    move-result v5

    const/16 v6, 0x56

    if-ne v5, v6, :cond_e

    add-int/lit8 v1, v1, 0x2

    goto/16 :goto_5

    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_5

    .line 1180763
    :pswitch_12
    invoke-static {v2, v4, v1}, LX/7CS;->k(Ljava/lang/String;LX/7CR;I)I

    move-result v1

    goto/16 :goto_5

    .line 1180764
    :pswitch_13
    if-nez v1, :cond_1c

    .line 1180765
    const/16 v5, 0x53

    invoke-virtual {v4, v5}, LX/7CR;->a(C)V

    .line 1180766
    add-int/lit8 v5, v1, 0x1

    .line 1180767
    :goto_c
    move v1, v5

    .line 1180768
    goto/16 :goto_5

    .line 1180769
    :pswitch_14
    invoke-static {v2, v4, v1, v3}, LX/7CS;->e(Ljava/lang/String;LX/7CR;IZ)I

    move-result v1

    goto/16 :goto_5

    .line 1180770
    :cond_f
    if-eqz v0, :cond_10

    .line 1180771
    iget-object v1, v4, LX/7CR;->c:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1180772
    goto/16 :goto_1

    .line 1180773
    :cond_10
    iget-object v1, v4, LX/7CR;->b:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1180774
    goto/16 :goto_1

    .line 1180775
    :cond_11
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 1180776
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_12
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 1180777
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3

    :cond_14
    const/4 v5, 0x0

    goto/16 :goto_6

    .line 1180778
    :cond_15
    add-int/lit8 v5, v1, 0x1

    goto/16 :goto_7

    :cond_16
    add-int/lit8 v6, v1, -0x1

    const/4 v10, 0x3

    const-string p1, "UMB"

    invoke-static {v2, v6, v10, p1}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_17

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-eq v6, v10, :cond_a

    add-int/lit8 v6, v1, 0x2

    const/4 v10, 0x2

    const-string p1, "ER"

    invoke-static {v2, v6, v10, p1}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_a

    :cond_17
    const/4 v5, 0x0

    goto/16 :goto_8

    .line 1180779
    :cond_18
    const/16 v5, 0x50

    invoke-virtual {v4, v5}, LX/7CR;->a(C)V

    .line 1180780
    add-int/lit8 v5, v1, 0x1

    const/4 v6, 0x1

    const-string v10, "P"

    const-string p1, "B"

    invoke-static {v2, v5, v6, v10, p1}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_19

    add-int/lit8 v5, v1, 0x2

    goto/16 :goto_9

    :cond_19
    add-int/lit8 v5, v1, 0x1

    goto/16 :goto_9

    .line 1180781
    :cond_1a
    invoke-virtual {v4, v11}, LX/7CR;->a(C)V

    goto/16 :goto_a

    .line 1180782
    :cond_1b
    add-int/lit8 v5, v1, 0x1

    goto/16 :goto_b

    .line 1180783
    :cond_1c
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne v1, v5, :cond_1d

    add-int/lit8 v5, v1, -0x3

    const/4 v6, 0x3

    const-string v10, "IAU"

    const-string v11, "EAU"

    invoke-static {v2, v5, v6, v10, v11}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1e

    add-int/lit8 v5, v1, -0x2

    const/4 v6, 0x2

    const-string v10, "AU"

    const-string v11, "OU"

    invoke-static {v2, v5, v6, v10, v11}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1e

    .line 1180784
    :cond_1d
    const-string v5, "KS"

    invoke-virtual {v4, v5}, LX/7CR;->a(Ljava/lang/String;)V

    .line 1180785
    :cond_1e
    add-int/lit8 v5, v1, 0x1

    const/4 v6, 0x1

    const-string v10, "C"

    const-string v11, "X"

    invoke-static {v2, v5, v6, v10, v11}, LX/7CS;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1f

    add-int/lit8 v5, v1, 0x2

    goto/16 :goto_c

    :cond_1f
    add-int/lit8 v5, v1, 0x1

    goto/16 :goto_c

    :pswitch_data_0
    .packed-switch 0x41
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_14
    .end packed-switch
.end method
