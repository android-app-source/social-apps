.class public LX/7wE;
.super LX/6E8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E8",
        "<",
        "Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;",
        "LX/7wB;",
        ">;"
    }
.end annotation


# instance fields
.field private l:LX/6qh;


# direct methods
.method public constructor <init>(Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;)V
    .locals 0

    .prologue
    .line 1275540
    invoke-direct {p0, p1}, LX/6E8;-><init>(LX/6E6;)V

    .line 1275541
    return-void
.end method

.method private a(LX/7wB;)V
    .locals 2

    .prologue
    .line 1275542
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;

    .line 1275543
    iget-object v1, p0, LX/7wE;->l:LX/6qh;

    invoke-virtual {v0, v1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1275544
    iget-object v1, p1, LX/7wB;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;->setInfoMessage(Ljava/lang/String;)V

    .line 1275545
    iget-object v1, p1, LX/7wB;->b:LX/3Ab;

    invoke-virtual {v0, v1}, Lcom/facebook/events/tickets/checkout/EventTicketingTermsAndPoliciesCheckoutView;->setTermsAndPolicies(LX/3Ab;)V

    .line 1275546
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/6E2;)V
    .locals 0

    .prologue
    .line 1275539
    check-cast p1, LX/7wB;

    invoke-direct {p0, p1}, LX/7wE;->a(LX/7wB;)V

    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1275537
    iput-object p1, p0, LX/7wE;->l:LX/6qh;

    .line 1275538
    return-void
.end method
