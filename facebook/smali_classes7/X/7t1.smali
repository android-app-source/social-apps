.class public final LX/7t1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 32

    .prologue
    .line 1266404
    const/16 v28, 0x0

    .line 1266405
    const/16 v27, 0x0

    .line 1266406
    const/16 v26, 0x0

    .line 1266407
    const/16 v25, 0x0

    .line 1266408
    const/16 v24, 0x0

    .line 1266409
    const/16 v23, 0x0

    .line 1266410
    const/16 v22, 0x0

    .line 1266411
    const/16 v21, 0x0

    .line 1266412
    const/16 v20, 0x0

    .line 1266413
    const/16 v19, 0x0

    .line 1266414
    const/16 v18, 0x0

    .line 1266415
    const/16 v17, 0x0

    .line 1266416
    const/16 v16, 0x0

    .line 1266417
    const/4 v15, 0x0

    .line 1266418
    const/4 v14, 0x0

    .line 1266419
    const/4 v13, 0x0

    .line 1266420
    const/4 v12, 0x0

    .line 1266421
    const/4 v11, 0x0

    .line 1266422
    const/4 v10, 0x0

    .line 1266423
    const/4 v9, 0x0

    .line 1266424
    const/4 v8, 0x0

    .line 1266425
    const/4 v7, 0x0

    .line 1266426
    const/4 v6, 0x0

    .line 1266427
    const/4 v5, 0x0

    .line 1266428
    const/4 v4, 0x0

    .line 1266429
    const/4 v3, 0x0

    .line 1266430
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v29

    sget-object v30, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-eq v0, v1, :cond_1

    .line 1266431
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1266432
    const/4 v3, 0x0

    .line 1266433
    :goto_0
    return v3

    .line 1266434
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1266435
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v29

    sget-object v30, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-eq v0, v1, :cond_17

    .line 1266436
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v29

    .line 1266437
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1266438
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_1

    if-eqz v29, :cond_1

    .line 1266439
    const-string v30, "__type__"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_2

    const-string v30, "__typename"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_3

    .line 1266440
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v28

    goto :goto_1

    .line 1266441
    :cond_3
    const-string v30, "allow_multi_select"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_4

    .line 1266442
    const/4 v7, 0x1

    .line 1266443
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto :goto_1

    .line 1266444
    :cond_4
    const-string v30, "available_time_slots"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_5

    .line 1266445
    invoke-static/range {p0 .. p1}, LX/7tC;->a(LX/15w;LX/186;)I

    move-result v26

    goto :goto_1

    .line 1266446
    :cond_5
    const-string v30, "available_times"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_6

    .line 1266447
    invoke-static/range {p0 .. p1}, LX/7su;->a(LX/15w;LX/186;)I

    move-result v25

    goto :goto_1

    .line 1266448
    :cond_6
    const-string v30, "default_value"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_7

    .line 1266449
    invoke-static/range {p0 .. p1}, LX/7sz;->a(LX/15w;LX/186;)I

    move-result v24

    goto :goto_1

    .line 1266450
    :cond_7
    const-string v30, "description"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_8

    .line 1266451
    invoke-static/range {p0 .. p1}, LX/7tB;->a(LX/15w;LX/186;)I

    move-result v23

    goto :goto_1

    .line 1266452
    :cond_8
    const-string v30, "disable_autofill"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_9

    .line 1266453
    const/4 v6, 0x1

    .line 1266454
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto/16 :goto_1

    .line 1266455
    :cond_9
    const-string v30, "fields"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_a

    .line 1266456
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 1266457
    :cond_a
    const-string v30, "form_field_id"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_b

    .line 1266458
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto/16 :goto_1

    .line 1266459
    :cond_b
    const-string v30, "form_field_type"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_c

    .line 1266460
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    goto/16 :goto_1

    .line 1266461
    :cond_c
    const-string v30, "heading"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_d

    .line 1266462
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto/16 :goto_1

    .line 1266463
    :cond_d
    const-string v30, "is_optional"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_e

    .line 1266464
    const/4 v5, 0x1

    .line 1266465
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto/16 :goto_1

    .line 1266466
    :cond_e
    const-string v30, "is_weekly_view"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_f

    .line 1266467
    const/4 v4, 0x1

    .line 1266468
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto/16 :goto_1

    .line 1266469
    :cond_f
    const-string v30, "items"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_10

    .line 1266470
    invoke-static/range {p0 .. p1}, LX/7tA;->b(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1266471
    :cond_10
    const-string v30, "max_selected"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_11

    .line 1266472
    const/4 v3, 0x1

    .line 1266473
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    goto/16 :goto_1

    .line 1266474
    :cond_11
    const-string v30, "prefill_values"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_12

    .line 1266475
    invoke-static/range {p0 .. p1}, LX/7t0;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1266476
    :cond_12
    const-string v30, "semantic_tag"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_13

    .line 1266477
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    goto/16 :goto_1

    .line 1266478
    :cond_13
    const-string v30, "style"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_14

    .line 1266479
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    goto/16 :goto_1

    .line 1266480
    :cond_14
    const-string v30, "time_end"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_15

    .line 1266481
    invoke-static/range {p0 .. p1}, LX/7sv;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1266482
    :cond_15
    const-string v30, "time_selected"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_16

    .line 1266483
    invoke-static/range {p0 .. p1}, LX/7sw;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 1266484
    :cond_16
    const-string v30, "time_start"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_0

    .line 1266485
    invoke-static/range {p0 .. p1}, LX/7sx;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1266486
    :cond_17
    const/16 v29, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1266487
    const/16 v29, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v29

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1266488
    if-eqz v7, :cond_18

    .line 1266489
    const/4 v7, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1266490
    :cond_18
    const/4 v7, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1266491
    const/4 v7, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1266492
    const/4 v7, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1266493
    const/4 v7, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1266494
    if-eqz v6, :cond_19

    .line 1266495
    const/4 v6, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1266496
    :cond_19
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1266497
    const/16 v6, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1266498
    const/16 v6, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1266499
    const/16 v6, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1266500
    if-eqz v5, :cond_1a

    .line 1266501
    const/16 v5, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1266502
    :cond_1a
    if-eqz v4, :cond_1b

    .line 1266503
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1266504
    :cond_1b
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 1266505
    if-eqz v3, :cond_1c

    .line 1266506
    const/16 v3, 0xe

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14, v4}, LX/186;->a(III)V

    .line 1266507
    :cond_1c
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1266508
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1266509
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1266510
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1266511
    const/16 v3, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1266512
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1266513
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
