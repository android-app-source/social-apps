.class public LX/79m;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/79m;


# instance fields
.field private final a:LX/0kL;

.field private final b:LX/1Sk;

.field public final c:LX/79x;

.field private final d:LX/0W3;


# direct methods
.method public constructor <init>(LX/0kL;LX/1Sk;LX/79x;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1174404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1174405
    iput-object p1, p0, LX/79m;->a:LX/0kL;

    .line 1174406
    iput-object p2, p0, LX/79m;->b:LX/1Sk;

    .line 1174407
    iput-object p3, p0, LX/79m;->c:LX/79x;

    .line 1174408
    iput-object p4, p0, LX/79m;->d:LX/0W3;

    .line 1174409
    return-void
.end method

.method public static a(LX/0QB;)LX/79m;
    .locals 7

    .prologue
    .line 1174442
    sget-object v0, LX/79m;->e:LX/79m;

    if-nez v0, :cond_1

    .line 1174443
    const-class v1, LX/79m;

    monitor-enter v1

    .line 1174444
    :try_start_0
    sget-object v0, LX/79m;->e:LX/79m;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1174445
    if-eqz v2, :cond_0

    .line 1174446
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1174447
    new-instance p0, LX/79m;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v3

    check-cast v3, LX/0kL;

    invoke-static {v0}, LX/1Sk;->a(LX/0QB;)LX/1Sk;

    move-result-object v4

    check-cast v4, LX/1Sk;

    invoke-static {v0}, LX/79x;->b(LX/0QB;)LX/79x;

    move-result-object v5

    check-cast v5, LX/79x;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v6

    check-cast v6, LX/0W3;

    invoke-direct {p0, v3, v4, v5, v6}, LX/79m;-><init>(LX/0kL;LX/1Sk;LX/79x;LX/0W3;)V

    .line 1174448
    move-object v0, p0

    .line 1174449
    sput-object v0, LX/79m;->e:LX/79m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1174450
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1174451
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1174452
    :cond_1
    sget-object v0, LX/79m;->e:LX/79m;

    return-object v0

    .line 1174453
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1174454
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1174455
    iget-object v0, p0, LX/79m;->a:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080d26

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1174456
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 1174410
    iget-object v0, p0, LX/79m;->d:LX/0W3;

    sget-wide v4, LX/0X5;->ef:J

    invoke-interface {v0, v4, v5, v3}, LX/0W4;->a(JZ)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1174411
    iget-object v0, p0, LX/79m;->b:LX/1Sk;

    .line 1174412
    iget-object v1, v0, LX/1Sk;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/1Sk;->e(LX/1Sk;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1174413
    :cond_0
    invoke-static {v0}, LX/1Sk;->d(LX/1Sk;)V

    .line 1174414
    :cond_1
    iget-object v1, v0, LX/1Sk;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v0, v1

    .line 1174415
    add-int/lit8 v2, v0, 0x1

    .line 1174416
    iget-object v0, p0, LX/79m;->b:LX/1Sk;

    .line 1174417
    iget-object v1, v0, LX/1Sk;->b:LX/79k;

    invoke-static {v0, v2, v1}, LX/1Sk;->a$redex0(LX/1Sk;ILX/79k;)V

    .line 1174418
    iget-object v0, p0, LX/79m;->b:LX/1Sk;

    .line 1174419
    iget-object v1, v0, LX/1Sk;->b:LX/79k;

    if-eqz v1, :cond_2

    invoke-static {v0}, LX/1Sk;->e(LX/1Sk;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1174420
    :cond_2
    invoke-static {v0}, LX/1Sk;->d(LX/1Sk;)V

    .line 1174421
    :cond_3
    iget-object v1, v0, LX/1Sk;->b:LX/79k;

    move-object v0, v1

    .line 1174422
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "<b>"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f080d25

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "</b>"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1174423
    if-le v2, v6, :cond_9

    .line 1174424
    sget-object v4, LX/79k;->PAST_WEEK:LX/79k;

    invoke-virtual {v0, v4}, LX/79k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f080d2d

    .line 1174425
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "<br/>"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v3

    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1174426
    :goto_1
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-static {p1, v0, v3}, LX/3oW;->a(Landroid/view/View;Ljava/lang/CharSequence;I)LX/3oW;

    move-result-object v4

    .line 1174427
    iget-object v0, v4, LX/3oW;->d:Landroid/support/design/widget/Snackbar$SnackbarLayout;

    move-object v0, v0

    .line 1174428
    check-cast v0, Landroid/view/ViewGroup;

    move v2, v3

    .line 1174429
    :goto_2
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v2, v1, :cond_6

    .line 1174430
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1174431
    instance-of v5, v1, Landroid/widget/TextView;

    if-eqz v5, :cond_4

    .line 1174432
    check-cast v1, Landroid/widget/TextView;

    .line 1174433
    const/4 v5, -0x1

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1174434
    const/4 v5, 0x0

    const v6, 0x3f99999a    # 1.2f

    invoke-virtual {v1, v5, v6}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 1174435
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 1174436
    :cond_5
    const v0, 0x7f080d2e

    goto :goto_0

    .line 1174437
    :cond_6
    iget-object v0, p0, LX/79m;->d:LX/0W3;

    sget-wide v6, LX/0X5;->eg:J

    invoke-interface {v0, v6, v7, v3}, LX/0W4;->a(JZ)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1174438
    const v0, 0x7f080d2f

    new-instance v1, LX/79l;

    invoke-direct {v1, p0}, LX/79l;-><init>(LX/79m;)V

    invoke-virtual {v4, v0, v1}, LX/3oW;->a(ILandroid/view/View$OnClickListener;)LX/3oW;

    .line 1174439
    :cond_7
    invoke-virtual {v4}, LX/3oW;->b()V

    .line 1174440
    :goto_3
    return-void

    .line 1174441
    :cond_8
    iget-object v0, p0, LX/79m;->a:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080d25

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_3

    :cond_9
    move-object v0, v1

    goto :goto_1
.end method
