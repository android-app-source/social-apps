.class public final LX/8Uj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Map;

.field public final synthetic b:LX/8Ul;


# direct methods
.method public constructor <init>(LX/8Ul;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 1351231
    iput-object p1, p0, LX/8Uj;->b:LX/8Ul;

    iput-object p2, p0, LX/8Uj;->a:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1351227
    iget-object v0, p0, LX/8Uj;->b:LX/8Ul;

    iget-object v0, v0, LX/8Ul;->c:LX/8TD;

    sget-object v1, LX/8TE;->INSTANT_GAME_ADD_SCORE:LX/8TE;

    const/4 v2, 0x0

    iget-object v3, p0, LX/8Uj;->a:Ljava/util/Map;

    invoke-virtual {v0, v1, v2, p1, v3}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;Ljava/util/Map;)V

    .line 1351228
    iget-object v0, p0, LX/8Uj;->b:LX/8Ul;

    iget-object v0, v0, LX/8Ul;->f:LX/8Sp;

    if-eqz v0, :cond_0

    .line 1351229
    iget-object v0, p0, LX/8Uj;->b:LX/8Ul;

    iget-object v0, v0, LX/8Ul;->f:LX/8Sp;

    invoke-virtual {v0}, LX/8Sp;->a()V

    .line 1351230
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1351201
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1351202
    if-nez p1, :cond_1

    .line 1351203
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Empty result received"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/8Uj;->onFailure(Ljava/lang/Throwable;)V

    .line 1351204
    :cond_0
    :goto_0
    return-void

    .line 1351205
    :cond_1
    iget-object v0, p0, LX/8Uj;->b:LX/8Ul;

    iget-object v0, v0, LX/8Ul;->c:LX/8TD;

    sget-object v1, LX/8TE;->INSTANT_GAME_ADD_SCORE:LX/8TE;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, LX/8Uj;->a:Ljava/util/Map;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/8TD;->a(LX/8TE;ZLjava/lang/Throwable;Ljava/util/Map;)V

    .line 1351206
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1351207
    check-cast v0, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;

    .line 1351208
    iget-object v1, p0, LX/8Uj;->b:LX/8Ul;

    .line 1351209
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->k()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    move-result-object v2

    if-nez v2, :cond_7

    .line 1351210
    :cond_2
    const/4 v2, 0x0

    .line 1351211
    :goto_1
    move-object v1, v2

    .line 1351212
    iget-object v2, p0, LX/8Uj;->b:LX/8Ul;

    iget-object v2, v2, LX/8Ul;->b:LX/8TS;

    iget-object v3, p0, LX/8Uj;->b:LX/8Ul;

    .line 1351213
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->k()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    move-result-object v4

    if-nez v4, :cond_8

    .line 1351214
    :cond_3
    const/4 v4, 0x0

    .line 1351215
    :goto_2
    move-object v3, v4

    .line 1351216
    invoke-virtual {v2, v1, v3}, LX/8TS;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 1351217
    iget-object v2, p0, LX/8Uj;->b:LX/8Ul;

    const/4 v4, 0x0

    .line 1351218
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->j()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;

    move-result-object v3

    if-nez v3, :cond_9

    :cond_4
    move-object v3, v4

    .line 1351219
    :goto_3
    move-object v2, v3

    .line 1351220
    iget-object v3, p0, LX/8Uj;->b:LX/8Ul;

    const/4 v5, 0x0

    .line 1351221
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->a()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    move-result-object v4

    if-nez v4, :cond_a

    :cond_5
    move-object v4, v5

    .line 1351222
    :goto_4
    move-object v0, v4

    .line 1351223
    iget-object v3, p0, LX/8Uj;->b:LX/8Ul;

    iget-object v3, v3, LX/8Ul;->f:LX/8Sp;

    if-eqz v3, :cond_0

    .line 1351224
    if-eqz v0, :cond_6

    .line 1351225
    iget-object v3, p0, LX/8Uj;->b:LX/8Ul;

    iget-object v3, v3, LX/8Ul;->f:LX/8Sp;

    invoke-virtual {v3, v1, v2, v0}, LX/8Sp;->a(Ljava/util/List;Ljava/util/List;LX/8Vb;)V

    goto :goto_0

    .line 1351226
    :cond_6
    iget-object v0, p0, LX/8Uj;->b:LX/8Ul;

    iget-object v0, v0, LX/8Ul;->f:LX/8Sp;

    invoke-virtual {v0}, LX/8Sp;->a()V

    goto :goto_0

    :cond_7
    iget-object v2, v1, LX/8Ul;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8Tn;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->k()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8Tn;->a(Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;)Ljava/util/List;

    move-result-object v2

    goto :goto_1

    :cond_8
    iget-object v4, v3, LX/8Ul;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->k()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;

    move-result-object v4

    invoke-static {v4}, LX/8Tn;->b(Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntriesFragmentModel;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_9
    iget-object v3, v2, LX/8Ul;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8Tn;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->j()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;

    move-result-object p1

    invoke-virtual {v3, p1, v4}, LX/8Tn;->a(Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameFriendLeaderboardEntriesFragmentModel;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    goto :goto_3

    :cond_a
    iget-object v4, v3, LX/8Ul;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/8Tn;

    invoke-virtual {v0}, Lcom/facebook/quicksilver/graphql/InstantGameAddScoreMutationModels$InstantGameAddScoreModel;->a()Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;

    move-result-object p1

    invoke-virtual {v4, p1, v5}, LX/8Tn;->a(Lcom/facebook/quicksilver/graphql/GameLeaderboardQueriesModels$InstantGameLeaderboardEntryFragmentModel;Ljava/lang/String;)LX/8Vb;

    move-result-object v4

    goto :goto_4
.end method
