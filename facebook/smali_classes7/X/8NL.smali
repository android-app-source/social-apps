.class public final enum LX/8NL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/8NL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/8NL;

.field public static final enum PHOTO_REVIEW:LX/8NL;

.field public static final enum PHOTO_STORY:LX/8NL;

.field public static final enum SINGLE_PHOTO:LX/8NL;

.field public static final enum TARGET_POST:LX/8NL;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1337007
    new-instance v0, LX/8NL;

    const-string v1, "TARGET_POST"

    invoke-direct {v0, v1, v2}, LX/8NL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8NL;->TARGET_POST:LX/8NL;

    .line 1337008
    new-instance v0, LX/8NL;

    const-string v1, "SINGLE_PHOTO"

    invoke-direct {v0, v1, v3}, LX/8NL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8NL;->SINGLE_PHOTO:LX/8NL;

    .line 1337009
    new-instance v0, LX/8NL;

    const-string v1, "PHOTO_STORY"

    invoke-direct {v0, v1, v4}, LX/8NL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8NL;->PHOTO_STORY:LX/8NL;

    .line 1337010
    new-instance v0, LX/8NL;

    const-string v1, "PHOTO_REVIEW"

    invoke-direct {v0, v1, v5}, LX/8NL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/8NL;->PHOTO_REVIEW:LX/8NL;

    .line 1337011
    const/4 v0, 0x4

    new-array v0, v0, [LX/8NL;

    sget-object v1, LX/8NL;->TARGET_POST:LX/8NL;

    aput-object v1, v0, v2

    sget-object v1, LX/8NL;->SINGLE_PHOTO:LX/8NL;

    aput-object v1, v0, v3

    sget-object v1, LX/8NL;->PHOTO_STORY:LX/8NL;

    aput-object v1, v0, v4

    sget-object v1, LX/8NL;->PHOTO_REVIEW:LX/8NL;

    aput-object v1, v0, v5

    sput-object v0, LX/8NL;->$VALUES:[LX/8NL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1337012
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/8NL;
    .locals 1

    .prologue
    .line 1337013
    const-class v0, LX/8NL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/8NL;

    return-object v0
.end method

.method public static values()[LX/8NL;
    .locals 1

    .prologue
    .line 1337014
    sget-object v0, LX/8NL;->$VALUES:[LX/8NL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8NL;

    return-object v0
.end method
