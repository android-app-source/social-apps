.class public final LX/8Lt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7mc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7mc",
        "<",
        "LX/7mj;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8M0;


# direct methods
.method public constructor <init>(LX/8M0;)V
    .locals 0

    .prologue
    .line 1334077
    iput-object p1, p0, LX/8Lt;->a:LX/8M0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1334065
    iget-object v0, p0, LX/8Lt;->a:LX/8M0;

    invoke-static {v0}, LX/8M0;->l(LX/8M0;)V

    .line 1334066
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1334067
    check-cast p1, LX/7mj;

    .line 1334068
    invoke-virtual {p1}, LX/7mj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/8K6;->SCHEDULED_SECTION:LX/8K6;

    move-object v1, v0

    .line 1334069
    :goto_0
    iget-object v0, p0, LX/8Lt;->a:LX/8M0;

    iget-object v0, v0, LX/8M0;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1334070
    iget-object v0, p0, LX/8Lt;->a:LX/8M0;

    iget-object v0, v0, LX/8M0;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Lc;

    invoke-virtual {v0, p1, v1}, LX/8Lc;->a(LX/7mi;LX/8K6;)V

    .line 1334071
    :cond_0
    return-void

    .line 1334072
    :cond_1
    sget-object v0, LX/8K6;->DRAFT_SECTION:LX/8K6;

    move-object v1, v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1334073
    check-cast p1, LX/7mj;

    .line 1334074
    iget-object v0, p0, LX/8Lt;->a:LX/8M0;

    iget-object v0, v0, LX/8M0;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1334075
    iget-object v0, p0, LX/8Lt;->a:LX/8M0;

    iget-object v0, v0, LX/8M0;->s:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Lc;

    invoke-virtual {v0, p1}, LX/8Lc;->a(LX/7mi;)V

    .line 1334076
    :cond_0
    return-void
.end method
