.class public final LX/8SJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/spinner/AudienceSpinner;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/spinner/AudienceSpinner;)V
    .locals 0

    .prologue
    .line 1346443
    iput-object p1, p0, LX/8SJ;->a:Lcom/facebook/privacy/spinner/AudienceSpinner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1346444
    iget-object v0, p0, LX/8SJ;->a:Lcom/facebook/privacy/spinner/AudienceSpinner;

    iget-object v0, v0, Lcom/facebook/privacy/spinner/AudienceSpinner;->d:LX/8SP;

    if-eqz v0, :cond_0

    .line 1346445
    iget-object v0, p0, LX/8SJ;->a:Lcom/facebook/privacy/spinner/AudienceSpinner;

    iget-object v0, v0, Lcom/facebook/privacy/spinner/AudienceSpinner;->d:LX/8SP;

    invoke-virtual {v0, p3}, LX/8SP;->a(I)LX/1oT;

    move-result-object v1

    .line 1346446
    iget-object v0, p0, LX/8SJ;->a:Lcom/facebook/privacy/spinner/AudienceSpinner;

    iget-object v0, v0, Lcom/facebook/privacy/spinner/AudienceSpinner;->d:LX/8SP;

    .line 1346447
    iget-object v2, v0, LX/8SP;->f:LX/1oT;

    move-object v0, v2

    .line 1346448
    invoke-static {v0, v1}, LX/2cA;->b(LX/1oU;LX/1oU;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1346449
    :cond_0
    :goto_0
    return-void

    .line 1346450
    :cond_1
    iget-object v0, p0, LX/8SJ;->a:Lcom/facebook/privacy/spinner/AudienceSpinner;

    .line 1346451
    invoke-static {v0, v1}, Lcom/facebook/privacy/spinner/AudienceSpinner;->a$redex0(Lcom/facebook/privacy/spinner/AudienceSpinner;LX/1oT;)V

    .line 1346452
    iget-object v0, p0, LX/8SJ;->a:Lcom/facebook/privacy/spinner/AudienceSpinner;

    iget-object v0, v0, Lcom/facebook/privacy/spinner/AudienceSpinner;->l:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 1346453
    iget-object v0, p0, LX/8SJ;->a:Lcom/facebook/privacy/spinner/AudienceSpinner;

    iget-object v0, v0, Lcom/facebook/privacy/spinner/AudienceSpinner;->l:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8SN;

    .line 1346454
    if-eqz v0, :cond_0

    .line 1346455
    iget-object v2, p0, LX/8SJ;->a:Lcom/facebook/privacy/spinner/AudienceSpinner;

    iget-object v2, v2, Lcom/facebook/privacy/spinner/AudienceSpinner;->d:LX/8SP;

    .line 1346456
    iget-object p0, v2, LX/8SP;->g:Ljava/lang/String;

    move-object v2, p0

    .line 1346457
    invoke-interface {v0, v1, v2}, LX/8SN;->a(LX/1oT;Ljava/lang/String;)V

    goto :goto_0
.end method
