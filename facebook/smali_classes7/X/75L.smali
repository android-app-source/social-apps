.class public LX/75L;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/75L;


# instance fields
.field private final a:LX/6Z0;

.field private final b:LX/0So;


# direct methods
.method public constructor <init>(LX/6Z0;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1169242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1169243
    iput-object p1, p0, LX/75L;->a:LX/6Z0;

    .line 1169244
    iput-object p2, p0, LX/75L;->b:LX/0So;

    .line 1169245
    return-void
.end method

.method public static a(LX/0QB;)LX/75L;
    .locals 5

    .prologue
    .line 1169246
    sget-object v0, LX/75L;->c:LX/75L;

    if-nez v0, :cond_1

    .line 1169247
    const-class v1, LX/75L;

    monitor-enter v1

    .line 1169248
    :try_start_0
    sget-object v0, LX/75L;->c:LX/75L;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1169249
    if-eqz v2, :cond_0

    .line 1169250
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1169251
    new-instance p0, LX/75L;

    invoke-static {v0}, LX/6Z0;->a(LX/0QB;)LX/6Z0;

    move-result-object v3

    check-cast v3, LX/6Z0;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {p0, v3, v4}, LX/75L;-><init>(LX/6Z0;LX/0So;)V

    .line 1169252
    move-object v0, p0

    .line 1169253
    sput-object v0, LX/75L;->c:LX/75L;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1169254
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1169255
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1169256
    :cond_1
    sget-object v0, LX/75L;->c:LX/75L;

    return-object v0

    .line 1169257
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1169258
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/photos/base/media/PhotoItem;J)V
    .locals 9

    .prologue
    .line 1169239
    new-instance v1, LX/75K;

    iget-object v3, p0, LX/75L;->a:LX/6Z0;

    iget-object v4, p0, LX/75L;->b:LX/0So;

    move-object v2, p1

    move-object v5, p2

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, LX/75K;-><init>(Landroid/content/Context;LX/6Z0;LX/0So;Lcom/facebook/photos/base/media/PhotoItem;J)V

    .line 1169240
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, p1, v0}, LX/3nE;->a(Landroid/content/Context;[Ljava/lang/Object;)LX/3nE;

    .line 1169241
    return-void
.end method
