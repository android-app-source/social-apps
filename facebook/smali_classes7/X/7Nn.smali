.class public final LX/7Nn;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/player/plugins/Video360Plugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/plugins/Video360Plugin;)V
    .locals 0

    .prologue
    .line 1200657
    iput-object p1, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1200658
    const/4 v12, 0x0

    .line 1200659
    iget-object v0, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1200660
    iget-object v0, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->o()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1200661
    iget-object v0, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->M:LX/3IO;

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/plugins/Video360Plugin;->a(LX/3IO;I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-boolean v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->J:Z

    if-nez v0, :cond_3

    .line 1200662
    const/4 v11, 0x1

    .line 1200663
    iget-object v0, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->c:LX/1C2;

    iget-object v1, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v2, v2, LX/2oy;->j:LX/2pb;

    invoke-virtual {v2}, LX/2pb;->s()LX/04D;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1C2;->b(Ljava/lang/String;LX/04D;)LX/1C2;

    .line 1200664
    iget-object v0, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->c:LX/1C2;

    iget-object v1, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v2, v2, Lcom/facebook/video/player/plugins/Video360Plugin;->M:LX/3IO;

    iget-wide v2, v2, LX/3IO;->g:J

    iget-object v4, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v4, v4, Lcom/facebook/video/player/plugins/Video360Plugin;->M:LX/3IO;

    iget v4, v4, LX/3IO;->d:F

    float-to-int v4, v4

    iget-object v5, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v5, v5, Lcom/facebook/video/player/plugins/Video360Plugin;->M:LX/3IO;

    iget v5, v5, LX/3IO;->e:F

    float-to-int v5, v5

    iget-object v6, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v6, v6, Lcom/facebook/video/player/plugins/Video360Plugin;->M:LX/3IO;

    iget v6, v6, LX/3IO;->f:F

    float-to-int v6, v6

    iget-object v7, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v7, v7, LX/2oy;->j:LX/2pb;

    invoke-virtual {v7}, LX/2pb;->h()I

    move-result v7

    div-int/lit16 v7, v7, 0x3e8

    iget-object v8, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v8, v8, Lcom/facebook/video/player/plugins/Video360Plugin;->M:LX/3IO;

    iget v8, v8, LX/3IO;->c:F

    iget-object v9, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v9, v9, Lcom/facebook/video/player/plugins/Video360Plugin;->M:LX/3IO;

    iget v9, v9, LX/3IO;->b:F

    float-to-int v9, v9

    iget-object v10, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v10, v10, LX/2oy;->j:LX/2pb;

    invoke-virtual {v10}, LX/2pb;->s()LX/04D;

    move-result-object v10

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;JIIIIFILX/04D;)LX/1C2;

    move v0, v11

    .line 1200665
    :goto_0
    iget-object v1, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    .line 1200666
    iput-boolean v12, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->J:Z

    .line 1200667
    :goto_1
    iget-object v1, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-boolean v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->G:Z

    if-eqz v1, :cond_0

    .line 1200668
    iget-object v1, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->B:Landroid/os/Handler;

    iget-object v2, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v2, v2, Lcom/facebook/video/player/plugins/Video360Plugin;->K:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3e8

    const v3, 0x5e9ba2b8

    invoke-static {v1, v2, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1200669
    :cond_0
    iget-object v1, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-boolean v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->H:Z

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    .line 1200670
    iget-object v0, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->c:LX/1C2;

    iget-object v1, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v2, v2, LX/2oy;->j:LX/2pb;

    invoke-virtual {v2}, LX/2pb;->s()LX/04D;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1C2;->b(Ljava/lang/String;LX/04D;)LX/1C2;

    .line 1200671
    :cond_1
    iget-object v0, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-boolean v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->I:Z

    if-eqz v0, :cond_2

    .line 1200672
    iget-object v0, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v0, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->c:LX/1C2;

    iget-object v1, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v1, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->O:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, p0, LX/7Nn;->a:Lcom/facebook/video/player/plugins/Video360Plugin;

    iget-object v2, v2, LX/2oy;->j:LX/2pb;

    invoke-virtual {v2}, LX/2pb;->s()LX/04D;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1C2;->c(Ljava/lang/String;LX/04D;)LX/1C2;

    .line 1200673
    :cond_2
    const/4 v0, 0x0

    return-object v0

    :cond_3
    move v0, v12

    goto :goto_0

    :cond_4
    move v0, v12

    goto :goto_1
.end method
