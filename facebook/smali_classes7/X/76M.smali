.class public LX/76M;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Z

.field public final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final c:LX/76L;

.field public final d:Ljava/lang/Exception;

.field public final e:J


# direct methods
.method private constructor <init>(ZLjava/lang/Object;LX/76L;Ljava/lang/Exception;J)V
    .locals 1
    .param p3    # LX/76L;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Exception;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTT;",
            "LX/76L;",
            "Ljava/lang/Exception;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 1170976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1170977
    iput-boolean p1, p0, LX/76M;->a:Z

    .line 1170978
    iput-object p2, p0, LX/76M;->b:Ljava/lang/Object;

    .line 1170979
    iput-object p3, p0, LX/76M;->c:LX/76L;

    .line 1170980
    iput-object p4, p0, LX/76M;->d:Ljava/lang/Exception;

    .line 1170981
    iput-wide p5, p0, LX/76M;->e:J

    .line 1170982
    return-void
.end method

.method public static a(LX/76L;J)LX/76M;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/76L;",
            "J)",
            "LX/76M",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1170983
    new-instance v1, LX/76M;

    const/4 v2, 0x0

    move-object v4, p0

    move-object v5, v3

    move-wide v6, p1

    invoke-direct/range {v1 .. v7}, LX/76M;-><init>(ZLjava/lang/Object;LX/76L;Ljava/lang/Exception;J)V

    return-object v1
.end method

.method public static a(Ljava/lang/Exception;J)LX/76M;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Exception;",
            "J)",
            "LX/76M",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1170984
    new-instance v1, LX/76M;

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget-object v4, LX/76L;->MQTT_EXCEPTION:LX/76L;

    move-object v5, p0

    move-wide v6, p1

    invoke-direct/range {v1 .. v7}, LX/76M;-><init>(ZLjava/lang/Object;LX/76L;Ljava/lang/Exception;J)V

    return-object v1
.end method

.method public static a(Ljava/lang/Object;J)LX/76M;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;J)",
            "LX/76M",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1170985
    new-instance v1, LX/76M;

    const/4 v2, 0x1

    move-object v3, p0

    move-object v5, v4

    move-wide v6, p1

    invoke-direct/range {v1 .. v7}, LX/76M;-><init>(ZLjava/lang/Object;LX/76L;Ljava/lang/Exception;J)V

    return-object v1
.end method


# virtual methods
.method public final a()Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1170986
    iget-boolean v0, p0, LX/76M;->a:Z

    if-eqz v0, :cond_0

    .line 1170987
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1170988
    :goto_0
    return-object v0

    .line 1170989
    :cond_0
    iget-object v0, p0, LX/76M;->c:LX/76L;

    sget-object v1, LX/76L;->MQTT_EXCEPTION:LX/76L;

    if-ne v0, v1, :cond_1

    .line 1170990
    sget-object v0, LX/1nY;->MQTT_SEND_FAILURE:LX/1nY;

    iget-object v1, p0, LX/76M;->d:Ljava/lang/Exception;

    invoke-static {v1}, LX/3d6;->bundleForException(Ljava/lang/Throwable;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v2, p0, LX/76M;->d:Ljava/lang/Exception;

    invoke-static {v0, v1, v2}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Landroid/os/Bundle;Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 1170991
    :cond_1
    sget-object v0, LX/1nY;->MQTT_SEND_FAILURE:LX/1nY;

    iget-object v1, p0, LX/76M;->c:LX/76L;

    invoke-virtual {v1}, LX/76L;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method
