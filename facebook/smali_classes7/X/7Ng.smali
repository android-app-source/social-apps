.class public LX/7Ng;
.super Lcom/facebook/video/player/plugins/Video360Plugin;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1200575
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/7Ng;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1200576
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1200577
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/7Ng;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200578
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1200579
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/video/player/plugins/Video360Plugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1200580
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7Ng;->a:Z

    .line 1200581
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/7Nf;

    invoke-direct {v1, p0}, LX/7Nf;-><init>(LX/7Ng;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1200582
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 4

    .prologue
    .line 1200583
    invoke-virtual {p0}, LX/7Ng;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 1200584
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const-wide v0, 0x3ffc71c71c71c71cL    # 1.7777777777777777

    .line 1200585
    :goto_0
    invoke-virtual {p1}, LX/2pa;->g()LX/2pZ;

    move-result-object v2

    .line 1200586
    iput-wide v0, v2, LX/2pZ;->e:D

    .line 1200587
    move-object v0, v2

    .line 1200588
    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 1200589
    invoke-super {p0, v0, p2}, Lcom/facebook/video/player/plugins/Video360Plugin;->a(LX/2pa;Z)V

    .line 1200590
    return-void

    .line 1200591
    :cond_0
    const-wide/high16 v0, 0x3fe2000000000000L    # 0.5625

    goto :goto_0
.end method
