.class public LX/77M;
.super LX/2g7;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/77M;


# instance fields
.field private final a:LX/13O;

.field private final b:LX/0SG;


# direct methods
.method public constructor <init>(LX/13O;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1171735
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1171736
    iput-object p1, p0, LX/77M;->a:LX/13O;

    .line 1171737
    iput-object p2, p0, LX/77M;->b:LX/0SG;

    .line 1171738
    return-void
.end method

.method public static a(LX/0QB;)LX/77M;
    .locals 5

    .prologue
    .line 1171739
    sget-object v0, LX/77M;->c:LX/77M;

    if-nez v0, :cond_1

    .line 1171740
    const-class v1, LX/77M;

    monitor-enter v1

    .line 1171741
    :try_start_0
    sget-object v0, LX/77M;->c:LX/77M;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1171742
    if-eqz v2, :cond_0

    .line 1171743
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1171744
    new-instance p0, LX/77M;

    invoke-static {v0}, LX/13O;->a(LX/0QB;)LX/13O;

    move-result-object v3

    check-cast v3, LX/13O;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/77M;-><init>(LX/13O;LX/0SG;)V

    .line 1171745
    move-object v0, p0

    .line 1171746
    sput-object v0, LX/77M;->c:LX/77M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1171747
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1171748
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1171749
    :cond_1
    sget-object v0, LX/77M;->c:LX/77M;

    return-object v0

    .line 1171750
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1171751
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1171752
    iget-object v1, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1171753
    if-eqz p3, :cond_0

    iget-object v1, p3, Lcom/facebook/interstitial/manager/InterstitialTrigger;->a:Lcom/facebook/interstitial/manager/InterstitialTriggerContext;

    if-nez v1, :cond_1

    .line 1171754
    :cond_0
    :goto_0
    return v0

    .line 1171755
    :cond_1
    sget-object v1, LX/77X;->THREAD_ACTIVITY:LX/77X;

    invoke-virtual {v1}, LX/77X;->toEventName()Ljava/lang/String;

    move-result-object v1

    .line 1171756
    iget-object v2, p3, Lcom/facebook/interstitial/manager/InterstitialTrigger;->a:Lcom/facebook/interstitial/manager/InterstitialTriggerContext;

    const-string v3, "target_user_id"

    invoke-virtual {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1171757
    iget-object v3, p0, LX/77M;->a:LX/13O;

    invoke-virtual {v3, v1, v2}, LX/13O;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 1171758
    iget-object v4, p0, LX/77M;->a:LX/13O;

    invoke-virtual {v4, v1, v2}, LX/13O;->b(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v4

    .line 1171759
    iget-object v1, p0, LX/77M;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v6

    sub-long v4, v6, v4

    .line 1171760
    sget-wide v6, LX/3QM;->b:J

    .line 1171761
    iget-object v1, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-le v3, v1, :cond_0

    cmp-long v1, v4, v6

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
