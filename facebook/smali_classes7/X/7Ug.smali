.class public final LX/7Ug;
.super Landroid/animation/AnimatorListenerAdapter;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/widget/soundwave/SoundWaveView;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/soundwave/SoundWaveView;)V
    .locals 0

    .prologue
    .line 1213275
    iput-object p1, p0, LX/7Ug;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1213276
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 1213277
    iget-object v0, p0, LX/7Ug;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    iget-object v0, v0, Lcom/facebook/widget/soundwave/SoundWaveView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Uh;

    .line 1213278
    iget-object p1, v0, LX/7Uh;->c:Landroid/animation/AnimatorSet;

    move-object v0, p1

    .line 1213279
    if-nez v0, :cond_0

    .line 1213280
    :cond_1
    return-void

    .line 1213281
    :cond_2
    iget-object v0, p0, LX/7Ug;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    iget-object v0, v0, Lcom/facebook/widget/soundwave/SoundWaveView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Uh;

    .line 1213282
    iget-object p0, v0, LX/7Uh;->c:Landroid/animation/AnimatorSet;

    move-object v0, p0

    .line 1213283
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method
