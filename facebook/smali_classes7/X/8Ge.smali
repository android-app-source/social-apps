.class public LX/8Ge;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0xi;

.field private final b:LX/0wW;

.field public c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/0wd;",
            ">;"
        }
    .end annotation
.end field

.field public d:F

.field private e:F

.field public f:F

.field public g:F

.field public h:F

.field public i:LX/9dH;

.field public j:Z


# direct methods
.method public constructor <init>(LX/0wW;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1319857
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1319858
    new-instance v0, LX/8Gd;

    invoke-direct {v0, p0}, LX/8Gd;-><init>(LX/8Ge;)V

    iput-object v0, p0, LX/8Ge;->a:LX/0xi;

    .line 1319859
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/8Ge;->c:LX/0am;

    .line 1319860
    const/high16 v0, 0x42a00000    # 80.0f

    iput v0, p0, LX/8Ge;->g:F

    .line 1319861
    const/high16 v0, 0x41700000    # 15.0f

    iput v0, p0, LX/8Ge;->h:F

    .line 1319862
    iput-object p1, p0, LX/8Ge;->b:LX/0wW;

    .line 1319863
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1319864
    iget-object v0, p0, LX/8Ge;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1319865
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8Ge;->j:Z

    .line 1319866
    iget-object v0, p0, LX/8Ge;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wd;

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1319867
    :cond_0
    return-void
.end method

.method public final a(LX/9dH;FF)V
    .locals 9

    .prologue
    const-wide v6, 0x3fa99999a0000000L    # 0.05000000074505806

    .line 1319868
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/8Ge;->j:Z

    .line 1319869
    iput-object p1, p0, LX/8Ge;->i:LX/9dH;

    .line 1319870
    iput p3, p0, LX/8Ge;->d:F

    .line 1319871
    iput p2, p0, LX/8Ge;->e:F

    .line 1319872
    iget v0, p0, LX/8Ge;->e:F

    iput v0, p0, LX/8Ge;->f:F

    .line 1319873
    iget-object v0, p0, LX/8Ge;->b:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    iget v1, p0, LX/8Ge;->g:F

    float-to-double v2, v1

    iget v1, p0, LX/8Ge;->h:F

    float-to-double v4, v1

    invoke-static {v2, v3, v4, v5}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    .line 1319874
    iput-wide v6, v0, LX/0wd;->l:D

    .line 1319875
    move-object v0, v0

    .line 1319876
    iput-wide v6, v0, LX/0wd;->k:D

    .line 1319877
    move-object v0, v0

    .line 1319878
    iget-object v1, p0, LX/8Ge;->a:LX/0xi;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/8Ge;->c:LX/0am;

    .line 1319879
    return-void
.end method
