.class public LX/7Wg;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1216742
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1216743
    iput-object p1, p0, LX/7Wg;->a:Landroid/content/Context;

    .line 1216744
    iput-object p2, p0, LX/7Wg;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1216745
    new-instance v0, LX/7Wf;

    invoke-direct {v0, p0}, LX/7Wf;-><init>(LX/7Wg;)V

    invoke-virtual {p0, v0}, LX/7Wg;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1216746
    const v0, 0x7f080e9f

    invoke-virtual {p0, v0}, LX/7Wg;->setTitle(I)V

    .line 1216747
    return-void
.end method

.method public static a(LX/0QB;)LX/7Wg;
    .locals 3

    .prologue
    .line 1216748
    new-instance v2, LX/7Wg;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v2, v0, v1}, LX/7Wg;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    .line 1216749
    move-object v0, v2

    .line 1216750
    return-object v0
.end method
