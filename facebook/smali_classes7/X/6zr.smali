.class public final synthetic LX/6zr;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1161532
    invoke-static {}, LX/6zQ;->values()[LX/6zQ;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/6zr;->c:[I

    :try_start_0
    sget-object v0, LX/6zr;->c:[I

    sget-object v1, LX/6zQ;->UNKNOWN:LX/6zQ;

    invoke-virtual {v1}, LX/6zQ;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_9

    :goto_0
    :try_start_1
    sget-object v0, LX/6zr;->c:[I

    sget-object v1, LX/6zQ;->NEW_CREDIT_CARD:LX/6zQ;

    invoke-virtual {v1}, LX/6zQ;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_8

    :goto_1
    :try_start_2
    sget-object v0, LX/6zr;->c:[I

    sget-object v1, LX/6zQ;->NEW_PAYPAL:LX/6zQ;

    invoke-virtual {v1}, LX/6zQ;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_7

    .line 1161533
    :goto_2
    invoke-static {}, LX/6zU;->values()[LX/6zU;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/6zr;->b:[I

    :try_start_3
    sget-object v0, LX/6zr;->b:[I

    sget-object v1, LX/6zU;->PAYPAL_BILLING_AGREEMENT:LX/6zU;

    invoke-virtual {v1}, LX/6zU;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_6

    :goto_3
    :try_start_4
    sget-object v0, LX/6zr;->b:[I

    sget-object v1, LX/6zU;->CREDIT_CARD:LX/6zU;

    invoke-virtual {v1}, LX/6zU;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_5

    .line 1161534
    :goto_4
    invoke-static {}, LX/708;->values()[LX/708;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/6zr;->a:[I

    :try_start_5
    sget-object v0, LX/6zr;->a:[I

    sget-object v1, LX/708;->COUNTRY_SELECTOR:LX/708;

    invoke-virtual {v1}, LX/708;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4

    :goto_5
    :try_start_6
    sget-object v0, LX/6zr;->a:[I

    sget-object v1, LX/708;->SELECT_PAYMENT_METHOD:LX/708;

    invoke-virtual {v1}, LX/708;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_3

    :goto_6
    :try_start_7
    sget-object v0, LX/6zr;->a:[I

    sget-object v1, LX/708;->SINGLE_ROW_DIVIDER:LX/708;

    invoke-virtual {v1}, LX/708;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2

    :goto_7
    :try_start_8
    sget-object v0, LX/6zr;->a:[I

    sget-object v1, LX/708;->NEW_PAYMENT_OPTION:LX/708;

    invoke-virtual {v1}, LX/708;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1

    :goto_8
    :try_start_9
    sget-object v0, LX/6zr;->a:[I

    sget-object v1, LX/708;->SECURITY_FOOTER:LX/708;

    invoke-virtual {v1}, LX/708;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_0

    :goto_9
    return-void

    :catch_0
    goto :goto_9

    :catch_1
    goto :goto_8

    :catch_2
    goto :goto_7

    :catch_3
    goto :goto_6

    :catch_4
    goto :goto_5

    :catch_5
    goto :goto_4

    :catch_6
    goto :goto_3

    :catch_7
    goto :goto_2

    :catch_8
    goto :goto_1

    :catch_9
    goto :goto_0
.end method
