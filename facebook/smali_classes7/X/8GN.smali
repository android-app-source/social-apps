.class public LX/8GN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/8GD;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

.field private e:LX/2Qx;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/8GD;LX/0Or;Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;LX/2Qx;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/creativeediting/abtest/IsSwipeableFiltersEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/8GD;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;",
            "LX/2Qx;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1319308
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1319309
    iput-object p1, p0, LX/8GN;->a:Landroid/content/Context;

    .line 1319310
    iput-object p2, p0, LX/8GN;->b:LX/8GD;

    .line 1319311
    iput-object p3, p0, LX/8GN;->c:LX/0Or;

    .line 1319312
    iput-object p4, p0, LX/8GN;->d:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    .line 1319313
    iput-object p5, p0, LX/8GN;->e:LX/2Qx;

    .line 1319314
    return-void
.end method

.method public static a(LX/0QB;)LX/8GN;
    .locals 1

    .prologue
    .line 1319307
    invoke-static {p0}, LX/8GN;->b(LX/0QB;)LX/8GN;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/net/Uri;FFLX/5jE;II)Lcom/facebook/photos/creativeediting/model/StickerParams;
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v3, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v6, 0x42c80000    # 100.0f

    .line 1319263
    iget-object v0, p3, LX/5jE;->a:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    move-object v0, v0

    .line 1319264
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;->WIDTH:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    if-ne v0, v1, :cond_0

    .line 1319265
    iget v0, p3, LX/5jE;->b:F

    move v0, v0

    .line 1319266
    div-float v1, v0, v6

    .line 1319267
    int-to-float v0, p4

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 1319268
    int-to-float v0, v0

    div-float/2addr v0, p1

    int-to-float v2, p5

    div-float/2addr v0, v2

    .line 1319269
    :goto_0
    sget-object v2, LX/8GM;->a:[I

    .line 1319270
    iget-object v4, p3, LX/5jE;->c:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-object v4, v4

    .line 1319271
    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    move v2, v3

    .line 1319272
    :goto_1
    sget-object v4, LX/8GM;->b:[I

    .line 1319273
    iget-object v5, p3, LX/5jE;->d:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-object v5, v5

    .line 1319274
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 1319275
    :goto_2
    new-instance v4, LX/5jG;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, p0, v5}, LX/5jG;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 1319276
    iput p2, v4, LX/5jG;->d:F

    .line 1319277
    move-object v4, v4

    .line 1319278
    iput v2, v4, LX/5jG;->e:F

    .line 1319279
    move-object v2, v4

    .line 1319280
    iput v3, v2, LX/5jG;->f:F

    .line 1319281
    move-object v2, v2

    .line 1319282
    iput v1, v2, LX/5jG;->g:F

    .line 1319283
    move-object v1, v2

    .line 1319284
    iput v0, v1, LX/5jG;->h:F

    .line 1319285
    move-object v0, v1

    .line 1319286
    invoke-virtual {v0}, LX/5jG;->a()Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-result-object v0

    return-object v0

    .line 1319287
    :cond_0
    iget v0, p3, LX/5jE;->b:F

    move v0, v0

    .line 1319288
    div-float/2addr v0, v6

    .line 1319289
    int-to-float v1, p5

    mul-float/2addr v1, v0

    float-to-int v1, v1

    .line 1319290
    int-to-float v1, v1

    mul-float/2addr v1, p1

    int-to-float v2, p4

    div-float/2addr v1, v2

    goto :goto_0

    .line 1319291
    :pswitch_0
    iget v2, p3, LX/5jE;->e:F

    move v2, v2

    .line 1319292
    div-float/2addr v2, v6

    .line 1319293
    goto :goto_1

    .line 1319294
    :pswitch_1
    sub-float v2, v7, v1

    div-float/2addr v2, v8

    .line 1319295
    goto :goto_1

    .line 1319296
    :pswitch_2
    iget v2, p3, LX/5jE;->e:F

    move v2, v2

    .line 1319297
    div-float/2addr v2, v6

    sub-float v2, v7, v2

    sub-float/2addr v2, v1

    .line 1319298
    goto :goto_1

    .line 1319299
    :pswitch_3
    iget v3, p3, LX/5jE;->f:F

    move v3, v3

    .line 1319300
    div-float/2addr v3, v6

    .line 1319301
    goto :goto_2

    .line 1319302
    :pswitch_4
    sub-float v3, v7, v0

    div-float/2addr v3, v8

    .line 1319303
    goto :goto_2

    .line 1319304
    :pswitch_5
    iget v3, p3, LX/5jE;->f:F

    move v3, v3

    .line 1319305
    div-float/2addr v3, v6

    sub-float v3, v7, v3

    sub-float/2addr v3, v0

    .line 1319306
    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(Landroid/net/Uri;FLX/5jL;)Lcom/facebook/photos/creativeediting/model/StickerParams;
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v1, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v6, 0x42c80000    # 100.0f

    .line 1319225
    iget v0, p2, LX/5jL;->a:F

    move v0, v0

    .line 1319226
    div-float v2, v0, v6

    .line 1319227
    iget v0, p2, LX/5jL;->b:F

    move v0, v0

    .line 1319228
    div-float v3, v0, v6

    .line 1319229
    sget-object v0, LX/8GM;->a:[I

    .line 1319230
    iget-object v4, p2, LX/5jL;->c:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-object v4, v4

    .line 1319231
    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 1319232
    :goto_0
    sget-object v4, LX/8GM;->b:[I

    .line 1319233
    iget-object v5, p2, LX/5jL;->d:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-object v5, v5

    .line 1319234
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 1319235
    :goto_1
    new-instance v4, LX/5jG;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, p0, v5}, LX/5jG;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 1319236
    iput p1, v4, LX/5jG;->d:F

    .line 1319237
    move-object v4, v4

    .line 1319238
    iput v0, v4, LX/5jG;->e:F

    .line 1319239
    move-object v0, v4

    .line 1319240
    iput v1, v0, LX/5jG;->f:F

    .line 1319241
    move-object v0, v0

    .line 1319242
    iput v3, v0, LX/5jG;->g:F

    .line 1319243
    move-object v0, v0

    .line 1319244
    iput v2, v0, LX/5jG;->h:F

    .line 1319245
    move-object v0, v0

    .line 1319246
    invoke-virtual {v0}, LX/5jG;->a()Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-result-object v0

    return-object v0

    .line 1319247
    :pswitch_0
    iget v0, p2, LX/5jL;->e:F

    move v0, v0

    .line 1319248
    div-float/2addr v0, v6

    .line 1319249
    goto :goto_0

    .line 1319250
    :pswitch_1
    sub-float v0, v7, v3

    div-float/2addr v0, v8

    .line 1319251
    goto :goto_0

    .line 1319252
    :pswitch_2
    iget v0, p2, LX/5jL;->e:F

    move v0, v0

    .line 1319253
    div-float/2addr v0, v6

    sub-float v0, v7, v0

    sub-float/2addr v0, v3

    .line 1319254
    goto :goto_0

    .line 1319255
    :pswitch_3
    iget v1, p2, LX/5jL;->f:F

    move v1, v1

    .line 1319256
    div-float/2addr v1, v6

    .line 1319257
    goto :goto_1

    .line 1319258
    :pswitch_4
    sub-float v1, v7, v2

    div-float/2addr v1, v8

    .line 1319259
    goto :goto_1

    .line 1319260
    :pswitch_5
    iget v1, p2, LX/5jL;->f:F

    move v1, v1

    .line 1319261
    div-float/2addr v1, v6

    sub-float v1, v7, v1

    sub-float/2addr v1, v2

    .line 1319262
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Lcom/facebook/photos/creativeediting/model/StickerParams;LX/3rL;)Lcom/facebook/photos/creativeediting/model/StickerParams;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            "LX/3rL",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 1319204
    iget-object v0, p1, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v1, v0

    .line 1319205
    iget-object v0, p1, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    .line 1319206
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 1319207
    cmpl-float v3, v1, v0

    if-nez v3, :cond_0

    .line 1319208
    :goto_0
    return-object p0

    .line 1319209
    :cond_0
    cmpg-float v3, v1, v0

    if-gez v3, :cond_2

    .line 1319210
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->n()F

    move-result v3

    .line 1319211
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->f()F

    move-result v4

    .line 1319212
    mul-float/2addr v0, v4

    div-float v1, v0, v1

    .line 1319213
    sub-float v0, v5, v3

    sub-float/2addr v0, v4

    .line 1319214
    cmpl-float v0, v0, v6

    if-nez v0, :cond_1

    sub-float v0, v5, v1

    .line 1319215
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->m()F

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->e()F

    move-result v4

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->m()F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v1, v0

    invoke-virtual {v2, v3, v0, v4, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1319216
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->b()Landroid/graphics/PointF;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->c()F

    move-result v1

    invoke-virtual {p0, v2, v0, v1}, Lcom/facebook/photos/creativeediting/model/StickerParams;->a(Landroid/graphics/RectF;Landroid/graphics/PointF;F)LX/362;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-object p0, v0

    goto :goto_0

    .line 1319217
    :cond_1
    sub-float v0, v5, v1

    mul-float/2addr v0, v3

    sub-float v3, v5, v4

    div-float/2addr v0, v3

    goto :goto_1

    .line 1319218
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->m()F

    move-result v3

    .line 1319219
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->e()F

    move-result v4

    .line 1319220
    mul-float/2addr v1, v4

    div-float/2addr v1, v0

    .line 1319221
    sub-float v0, v5, v3

    sub-float/2addr v0, v4

    .line 1319222
    cmpl-float v0, v0, v6

    if-nez v0, :cond_3

    sub-float v0, v5, v1

    .line 1319223
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->n()F

    move-result v3

    add-float/2addr v1, v0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->f()F

    move-result v4

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/StickerParams;->n()F

    move-result v5

    add-float/2addr v4, v5

    invoke-virtual {v2, v0, v3, v1, v4}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_2

    .line 1319224
    :cond_3
    sub-float v0, v5, v1

    mul-float/2addr v0, v3

    sub-float v3, v5, v4

    div-float/2addr v0, v3

    goto :goto_3
.end method

.method public static a(LX/0Px;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ")",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1319194
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    move-object v0, v2

    .line 1319195
    :goto_0
    return-object v0

    .line 1319196
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1319197
    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1319198
    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1319199
    iget-object v4, v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v0, v4

    .line 1319200
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1319201
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v1

    rem-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    goto :goto_0

    .line 1319202
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 1319203
    goto :goto_0
.end method

.method public static a(LX/0Px;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1319186
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1319187
    iget-object v4, v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1319188
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1319189
    :goto_1
    return-object v0

    .line 1319190
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1319191
    :cond_1
    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1319192
    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    goto :goto_1

    .line 1319193
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1319183
    new-instance v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1319184
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1319185
    sget-object v3, LX/5jI;->FRAME:LX/5jI;

    move-object v2, p0

    move-object v4, p0

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;-><init>(Ljava/util/List;Ljava/lang/String;LX/5jI;Ljava/lang/String;Lcom/facebook/videocodec/effects/model/ColorFilter;Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;)V

    return-object v0
.end method

.method private static a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;)Lcom/facebook/videocodec/effects/model/ColorFilter;
    .locals 8
    .param p0    # Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1319180
    if-nez p0, :cond_0

    .line 1319181
    const/4 v0, 0x0

    .line 1319182
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/videocodec/effects/model/ColorFilter;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->j()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->b()D

    move-result-wide v4

    double-to-float v3, v4

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->c()D

    move-result-wide v4

    double-to-float v4, v4

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->e()D

    move-result-wide v6

    double-to-float v5, v6

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;->aY_()Z

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/videocodec/effects/model/ColorFilter;-><init>(Ljava/lang/String;FFFFZ)V

    goto :goto_0
.end method

.method private static a(II)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1319178
    sub-int v1, p0, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/16 v2, 0x8

    if-ge v1, v2, :cond_1

    .line 1319179
    :cond_0
    :goto_0
    return v0

    :cond_1
    int-to-float v1, p0

    int-to-float v2, p1

    div-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(JJ)Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    .line 1319172
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 1319173
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1319174
    cmp-long v1, p0, v6

    if-nez v1, :cond_1

    cmp-long v1, p2, v6

    if-nez v1, :cond_1

    .line 1319175
    :cond_0
    :goto_0
    return v0

    .line 1319176
    :cond_1
    cmp-long v1, p0, v2

    if-gtz v1, :cond_2

    cmp-long v1, p2, v2

    if-gtz v1, :cond_0

    .line 1319177
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(II)F
    .locals 2

    .prologue
    .line 1319086
    int-to-float v0, p0

    int-to-float v1, p1

    div-float/2addr v0, v1

    return v0
.end method

.method public static b(LX/0Px;Ljava/lang/String;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1319087
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1319088
    iget-object v3, v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1319089
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1319090
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->a()LX/0Px;

    move-result-object v0

    .line 1319091
    :goto_1
    return-object v0

    .line 1319092
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1319093
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1319094
    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/8GN;
    .locals 6

    .prologue
    .line 1319095
    new-instance v0, LX/8GN;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 1319096
    new-instance v3, LX/8GD;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-direct {v3, v2}, LX/8GD;-><init>(LX/0Zb;)V

    .line 1319097
    move-object v2, v3

    .line 1319098
    check-cast v2, LX/8GD;

    const/16 v3, 0x154b

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(LX/0QB;)Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    invoke-static {p0}, LX/2Qx;->a(LX/0QB;)LX/2Qx;

    move-result-object v5

    check-cast v5, LX/2Qx;

    invoke-direct/range {v0 .. v5}, LX/8GN;-><init>(Landroid/content/Context;LX/8GD;LX/0Or;Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;LX/2Qx;)V

    .line 1319099
    return-object v0
.end method

.method public static b(LX/0Px;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ")",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1319100
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    move-object v0, v2

    .line 1319101
    :goto_0
    return-object v0

    .line 1319102
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1319103
    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1319104
    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1319105
    iget-object v4, v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v0, v4

    .line 1319106
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1319107
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v1

    rem-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    goto :goto_0

    .line 1319108
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 1319109
    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1319110
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1319111
    const-string v0, ""

    .line 1319112
    :goto_0
    return-object v0

    .line 1319113
    :cond_0
    invoke-static {p1}, LX/5iL;->getValue(Ljava/lang/String;)LX/5iL;

    move-result-object v0

    .line 1319114
    sget-object v1, LX/8GM;->c:[I

    invoke-virtual {v0}, LX/5iL;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1319115
    const-string v0, ""

    goto :goto_0

    .line 1319116
    :pswitch_0
    iget-object v0, p0, LX/8GN;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0813c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1319117
    :pswitch_1
    iget-object v0, p0, LX/8GN;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0813c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1319118
    :pswitch_2
    iget-object v0, p0, LX/8GN;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0813c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1319119
    :pswitch_3
    iget-object v0, p0, LX/8GN;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0813c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1319120
    :pswitch_4
    iget-object v0, p0, LX/8GN;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0813c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1319121
    :pswitch_5
    iget-object v0, p0, LX/8GN;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0813ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1319122
    :pswitch_6
    iget-object v0, p0, LX/8GN;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0813cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1319123
    :pswitch_7
    iget-object v0, p0, LX/8GN;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0813cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1319124
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 1319125
    iget-object v0, p0, LX/8GN;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1319126
    invoke-static {}, LX/5iL;->values()[LX/5iL;

    move-result-object v9

    array-length v10, v9

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v10, :cond_1

    aget-object v4, v9, v7

    .line 1319127
    new-instance v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1319128
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1319129
    invoke-virtual {v4}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/5jI;->FILTER:LX/5jI;

    invoke-virtual {v4}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, LX/8GN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;-><init>(Ljava/util/List;Ljava/lang/String;LX/5jI;Ljava/lang/String;Lcom/facebook/videocodec/effects/model/ColorFilter;Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;)V

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1319130
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 1319131
    :cond_0
    invoke-virtual {p0}, LX/8GN;->b()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1319132
    :cond_1
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;II)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
            "II)",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1319133
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 1319134
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v9, :cond_1

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1319135
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, LX/8GN;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;IILjava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    .line 1319136
    if-eqz v0, :cond_0

    .line 1319137
    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1319138
    :goto_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 1319139
    :cond_0
    iget-object v0, p0, LX/8GN;->b:LX/8GD;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 1319140
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/8GB;->FRAME_IGNORED_ASSET_MISSING:LX/8GB;

    invoke-virtual {v3}, LX/8GB;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v3, LX/8GC;->FRAME_ID:LX/8GC;

    invoke-virtual {v3}, LX/8GC;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1319141
    const-string v3, "composer"

    .line 1319142
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1319143
    iget-object v3, v0, LX/8GD;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1319144
    goto :goto_1

    .line 1319145
    :cond_1
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;IILjava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 10
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1319146
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 1319147
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aX_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1319148
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aX_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    .line 1319149
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v9, :cond_1

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel;

    .line 1319150
    invoke-static {p2, p3}, LX/8GN;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1319151
    new-instance v3, LX/5jE;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel;->e()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel;->c()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImagePortraitSizeModel;

    move-result-object v1

    invoke-direct {v3, v0, v1}, LX/5jE;-><init>(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$PortraitAnchoringModel;Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImagePortraitSizeModel;)V

    .line 1319152
    :goto_1
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageModel;

    move-result-object v1

    .line 1319153
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1319154
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageModel;->c()I

    move-result v4

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageModel;->a()I

    move-result v1

    invoke-static {v4, v1}, LX/8GN;->b(II)F

    move-result v1

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel;->ba_()D

    move-result-wide v4

    double-to-float v2, v4

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, LX/8GN;->a(Landroid/net/Uri;FFLX/5jE;II)Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-result-object v0

    .line 1319155
    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1319156
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 1319157
    :cond_0
    new-instance v3, LX/5jE;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel;->d()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$LandscapeAnchoringModel;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel;->b()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageLandscapeSizeModel;

    move-result-object v1

    invoke-direct {v3, v0, v1}, LX/5jE;-><init>(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$LandscapeAnchoringModel;Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImageLandscapeSizeModel;)V

    goto :goto_1

    .line 1319158
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aW_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1319159
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->aW_()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v3, v0

    :goto_2
    if-ge v3, v5, :cond_4

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;

    .line 1319160
    invoke-static {p2, p3}, LX/8GN;->a(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1319161
    new-instance v1, LX/5jL;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->e()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$PortraitAnchoringModel;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->l()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextPortraitSizeModel;

    move-result-object v6

    invoke-direct {v1, v2, v6}, LX/5jL;-><init>(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$PortraitAnchoringModel;Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextPortraitSizeModel;)V

    .line 1319162
    :goto_3
    invoke-static {p2, p3}, LX/8GN;->a(II)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    .line 1319163
    :goto_4
    iget-object v6, p0, LX/8GN;->d:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v0, v8, v2}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, LX/8GN;->a(Landroid/net/Uri;FLX/5jL;)Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-result-object v0

    .line 1319164
    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1319165
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 1319166
    :cond_2
    new-instance v1, LX/5jL;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->d()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$LandscapeAnchoringModel;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel;->k()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextLandscapeSizeModel;

    move-result-object v6

    invoke-direct {v1, v2, v6}, LX/5jL;-><init>(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$LandscapeAnchoringModel;Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$TextLandscapeSizeModel;)V

    goto :goto_3

    .line 1319167
    :cond_3
    const/4 v2, 0x2

    goto :goto_4

    .line 1319168
    :cond_4
    new-instance v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    sget-object v3, LX/5jI;->FRAME:LX/5jI;

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->d()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;

    move-result-object v2

    invoke-static {v2}, LX/8GN;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;)Lcom/facebook/videocodec/effects/model/ColorFilter;

    move-result-object v5

    const/4 v6, 0x0

    move-object v2, p5

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;-><init>(Ljava/util/List;Ljava/lang/String;LX/5jI;Ljava/lang/String;Lcom/facebook/videocodec/effects/model/ColorFilter;Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;)V

    return-object v0
.end method

.method public final b()Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1319169
    new-instance v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1319170
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1319171
    sget-object v2, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v2}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/5jI;->FILTER:LX/5jI;

    sget-object v4, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v4}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, LX/8GN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;-><init>(Ljava/util/List;Ljava/lang/String;LX/5jI;Ljava/lang/String;Lcom/facebook/videocodec/effects/model/ColorFilter;Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;)V

    return-object v0
.end method
