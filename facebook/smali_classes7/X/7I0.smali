.class public LX/7I0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:F

.field public final k:Z

.field public final l:Z

.field public final m:Z

.field public final n:I

.field public final o:Z

.field public final p:Z

.field public final q:Z

.field public final r:J

.field public final s:J

.field public final t:LX/0p3;

.field public final u:Z

.field public final v:Z


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 5

    .prologue
    const/high16 v3, 0x40000

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1191463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1191464
    sget-short v0, LX/0ws;->w:S

    invoke-interface {p1, v0, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/7I0;->a:Z

    .line 1191465
    sget v0, LX/0ws;->G:I

    const v1, 0x7d000

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/7I0;->b:I

    .line 1191466
    sget-char v0, LX/0ws;->L:C

    const-string v1, "bandwidth"

    invoke-interface {p1, v0, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/7I0;->c:Ljava/lang/String;

    .line 1191467
    sget v0, LX/0ws;->J:I

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/7I0;->d:I

    .line 1191468
    sget v0, LX/0ws;->F:I

    invoke-interface {p1, v0, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/7I0;->e:I

    .line 1191469
    sget v0, LX/0ws;->H:I

    const/high16 v1, 0x100000

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/7I0;->f:I

    .line 1191470
    sget v0, LX/0ws;->D:I

    const/16 v1, 0xa

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/7I0;->g:I

    .line 1191471
    sget v0, LX/0ws;->E:I

    const/4 v1, 0x2

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/7I0;->h:I

    .line 1191472
    sget v0, LX/0ws;->M:I

    const/16 v1, 0x8

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/7I0;->i:I

    .line 1191473
    sget v0, LX/0ws;->u:F

    const/high16 v1, 0x40000000    # 2.0f

    invoke-interface {p1, v0, v1}, LX/0ad;->a(FF)F

    move-result v0

    iput v0, p0, LX/7I0;->j:F

    .line 1191474
    sget-short v0, LX/0ws;->B:S

    invoke-interface {p1, v0, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/7I0;->k:Z

    .line 1191475
    sget-short v0, LX/0ws;->A:S

    invoke-interface {p1, v0, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/7I0;->l:Z

    .line 1191476
    sget-short v0, LX/0ws;->y:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/7I0;->m:Z

    .line 1191477
    sget v0, LX/0ws;->N:I

    const/16 v1, 0xf

    invoke-interface {p1, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/7I0;->n:I

    .line 1191478
    sget-short v0, LX/0ws;->x:S

    invoke-interface {p1, v0, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/7I0;->o:Z

    .line 1191479
    sget-short v0, LX/0ws;->t:S

    invoke-interface {p1, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/7I0;->p:Z

    .line 1191480
    sget-short v0, LX/0ws;->C:S

    invoke-interface {p1, v0, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/7I0;->q:Z

    .line 1191481
    sget-wide v0, LX/0ws;->I:J

    const-wide/16 v2, 0x1388

    invoke-interface {p1, v0, v1, v2, v3}, LX/0ad;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/7I0;->r:J

    .line 1191482
    sget-wide v0, LX/0ws;->v:J

    const-wide/16 v2, 0x1000

    invoke-interface {p1, v0, v1, v2, v3}, LX/0ad;->a(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/7I0;->s:J

    .line 1191483
    const-class v0, LX/0p3;

    sget-char v1, LX/0ws;->K:C

    const-string v2, "GOOD"

    invoke-interface {p1, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/0p3;->GOOD:LX/0p3;

    .line 1191484
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1191485
    :goto_0
    move-object v0, v2

    .line 1191486
    check-cast v0, LX/0p3;

    iput-object v0, p0, LX/7I0;->t:LX/0p3;

    .line 1191487
    sget-short v0, LX/0ws;->z:S

    invoke-interface {p1, v0, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/7I0;->u:Z

    .line 1191488
    sget-short v0, LX/0fe;->o:S

    invoke-interface {p1, v0, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/7I0;->v:Z

    .line 1191489
    return-void

    :catch_0
    goto :goto_0
.end method
