.class public final LX/7vC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:J

.field public B:Z

.field public C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public E:Z

.field public F:Ljava/lang/String;

.field public G:Ljava/lang/String;

.field public H:Z

.field public I:Ljava/util/Date;

.field public J:Ljava/util/Date;

.field public K:Ljava/util/TimeZone;

.field public L:Z

.field public M:LX/7vL;

.field public N:J

.field public O:Ljava/lang/String;

.field public P:Ljava/lang/String;

.field public Q:Ljava/util/TimeZone;

.field public R:I

.field public S:Ljava/lang/String;

.field public T:Landroid/net/Uri;

.field public U:Ljava/lang/String;

.field public V:Landroid/net/Uri;

.field public W:Landroid/net/Uri;

.field public X:Landroid/net/Uri;

.field public Y:Ljava/lang/String;

.field public Z:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "LX/7vK;",
            ">;"
        }
    .end annotation
.end field

.field public a:Ljava/lang/String;

.field public aa:Z

.field public ab:Ljava/lang/String;

.field public ac:I

.field public ad:Ljava/lang/String;

.field public ae:I

.field public af:Ljava/lang/String;

.field public ag:I

.field public ah:Lcom/facebook/events/model/EventUser;

.field public ai:Ljava/lang/String;

.field public aj:Ljava/lang/String;

.field public ak:Ljava/lang/String;

.field public al:Ljava/lang/String;

.field public am:Ljava/lang/String;

.field public an:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

.field public ao:I

.field public b:Ljava/lang/String;

.field public c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

.field public d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

.field public e:Lcom/facebook/events/model/EventType;

.field public f:Lcom/facebook/events/model/PrivacyKind;

.field public g:Lcom/facebook/events/model/PrivacyType;

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:LX/03R;

.field public l:Z

.field public m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

.field public n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

.field public o:Ljava/lang/String;

.field public p:I

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field public y:Z

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1273867
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1273868
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/7vC;->k:LX/03R;

    .line 1273869
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7vC;->B:Z

    .line 1273870
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, LX/7vC;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1273871
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7vC;->L:Z

    .line 1273872
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/7vC;->N:J

    .line 1273873
    const-class v0, LX/7vK;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LX/7vC;->Z:Ljava/util/EnumSet;

    .line 1273874
    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/model/Event;)V
    .locals 4

    .prologue
    .line 1273736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1273737
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/7vC;->k:LX/03R;

    .line 1273738
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7vC;->B:Z

    .line 1273739
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, LX/7vC;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1273740
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/7vC;->L:Z

    .line 1273741
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/7vC;->N:J

    .line 1273742
    const-class v0, LX/7vK;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LX/7vC;->Z:Ljava/util/EnumSet;

    .line 1273743
    iget-object v0, p1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1273744
    iput-object v0, p0, LX/7vC;->a:Ljava/lang/String;

    .line 1273745
    iget-object v0, p1, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1273746
    iput-object v0, p0, LX/7vC;->b:Ljava/lang/String;

    .line 1273747
    iget-object v0, p1, Lcom/facebook/events/model/Event;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-object v0, v0

    .line 1273748
    iput-object v0, p0, LX/7vC;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1273749
    iget-object v0, p1, Lcom/facebook/events/model/Event;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-object v0, v0

    .line 1273750
    iput-object v0, p0, LX/7vC;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1273751
    iget-object v0, p1, Lcom/facebook/events/model/Event;->e:Lcom/facebook/events/model/EventType;

    move-object v0, v0

    .line 1273752
    iput-object v0, p0, LX/7vC;->e:Lcom/facebook/events/model/EventType;

    .line 1273753
    iget-object v0, p1, Lcom/facebook/events/model/Event;->g:Lcom/facebook/events/model/PrivacyKind;

    move-object v0, v0

    .line 1273754
    iput-object v0, p0, LX/7vC;->f:Lcom/facebook/events/model/PrivacyKind;

    .line 1273755
    iget-object v0, p1, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v0

    .line 1273756
    iput-object v0, p0, LX/7vC;->g:Lcom/facebook/events/model/PrivacyType;

    .line 1273757
    iget-object v0, p1, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v0, v0

    .line 1273758
    iput-object v0, p0, LX/7vC;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1273759
    iget-object v0, p1, Lcom/facebook/events/model/Event;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-object v0, v0

    .line 1273760
    iput-object v0, p0, LX/7vC;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    .line 1273761
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->h:Z

    move v0, v0

    .line 1273762
    iput-boolean v0, p0, LX/7vC;->h:Z

    .line 1273763
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->j:Z

    move v0, v0

    .line 1273764
    iput-boolean v0, p0, LX/7vC;->j:Z

    .line 1273765
    iget-object v0, p1, Lcom/facebook/events/model/Event;->k:LX/03R;

    move-object v0, v0

    .line 1273766
    invoke-virtual {p0, v0}, LX/7vC;->a(LX/03R;)LX/7vC;

    .line 1273767
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->l:Z

    move v0, v0

    .line 1273768
    iput-boolean v0, p0, LX/7vC;->l:Z

    .line 1273769
    iget-object v0, p1, Lcom/facebook/events/model/Event;->o:Ljava/lang/String;

    move-object v0, v0

    .line 1273770
    iput-object v0, p0, LX/7vC;->o:Ljava/lang/String;

    .line 1273771
    iget v0, p1, Lcom/facebook/events/model/Event;->p:I

    move v0, v0

    .line 1273772
    iput v0, p0, LX/7vC;->p:I

    .line 1273773
    iget-object v0, p1, Lcom/facebook/events/model/Event;->q:LX/0Px;

    move-object v0, v0

    .line 1273774
    iput-object v0, p0, LX/7vC;->q:LX/0Px;

    .line 1273775
    iget-object v0, p1, Lcom/facebook/events/model/Event;->r:Ljava/lang/String;

    move-object v0, v0

    .line 1273776
    iput-object v0, p0, LX/7vC;->r:Ljava/lang/String;

    .line 1273777
    iget-object v0, p1, Lcom/facebook/events/model/Event;->s:Ljava/lang/String;

    move-object v0, v0

    .line 1273778
    iput-object v0, p0, LX/7vC;->s:Ljava/lang/String;

    .line 1273779
    iget-object v0, p1, Lcom/facebook/events/model/Event;->t:Ljava/lang/String;

    move-object v0, v0

    .line 1273780
    iput-object v0, p0, LX/7vC;->t:Ljava/lang/String;

    .line 1273781
    iget-object v0, p1, Lcom/facebook/events/model/Event;->u:Ljava/lang/String;

    move-object v0, v0

    .line 1273782
    iput-object v0, p0, LX/7vC;->u:Ljava/lang/String;

    .line 1273783
    iget-object v0, p1, Lcom/facebook/events/model/Event;->v:Ljava/lang/String;

    move-object v0, v0

    .line 1273784
    iput-object v0, p0, LX/7vC;->v:Ljava/lang/String;

    .line 1273785
    iget-object v0, p1, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    move-object v0, v0

    .line 1273786
    iput-object v0, p0, LX/7vC;->w:Ljava/lang/String;

    .line 1273787
    iget-object v0, p1, Lcom/facebook/events/model/Event;->x:Ljava/lang/String;

    move-object v0, v0

    .line 1273788
    iput-object v0, p0, LX/7vC;->x:Ljava/lang/String;

    .line 1273789
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->y:Z

    move v0, v0

    .line 1273790
    iput-boolean v0, p0, LX/7vC;->y:Z

    .line 1273791
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->B:Z

    move v0, v0

    .line 1273792
    iput-boolean v0, p0, LX/7vC;->B:Z

    .line 1273793
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    .line 1273794
    iput-object v0, p0, LX/7vC;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1273795
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->E:Z

    move v0, v0

    .line 1273796
    iput-boolean v0, p0, LX/7vC;->E:Z

    .line 1273797
    iget-object v0, p1, Lcom/facebook/events/model/Event;->F:Ljava/lang/String;

    move-object v0, v0

    .line 1273798
    iput-object v0, p0, LX/7vC;->F:Ljava/lang/String;

    .line 1273799
    iget-object v0, p1, Lcom/facebook/events/model/Event;->G:Ljava/lang/String;

    move-object v0, v0

    .line 1273800
    iput-object v0, p0, LX/7vC;->G:Ljava/lang/String;

    .line 1273801
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->H:Z

    move v0, v0

    .line 1273802
    iput-boolean v0, p0, LX/7vC;->H:Z

    .line 1273803
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v0

    .line 1273804
    iput-object v0, p0, LX/7vC;->I:Ljava/util/Date;

    .line 1273805
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v0

    .line 1273806
    iput-object v0, p0, LX/7vC;->J:Ljava/util/Date;

    .line 1273807
    iget-object v0, p1, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    move-object v0, v0

    .line 1273808
    iput-object v0, p0, LX/7vC;->K:Ljava/util/TimeZone;

    .line 1273809
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->N:Z

    move v0, v0

    .line 1273810
    iput-boolean v0, p0, LX/7vC;->L:Z

    .line 1273811
    iget-wide v2, p1, Lcom/facebook/events/model/Event;->P:J

    move-wide v0, v2

    .line 1273812
    iput-wide v0, p0, LX/7vC;->N:J

    .line 1273813
    iget-object v0, p1, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v0, v0

    .line 1273814
    iput-object v0, p0, LX/7vC;->O:Ljava/lang/String;

    .line 1273815
    iget-object v0, p1, Lcom/facebook/events/model/Event;->O:LX/7vL;

    move-object v0, v0

    .line 1273816
    iput-object v0, p0, LX/7vC;->M:LX/7vL;

    .line 1273817
    iget-object v0, p1, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v0, v0

    .line 1273818
    iput-object v0, p0, LX/7vC;->P:Ljava/lang/String;

    .line 1273819
    iget-object v0, p1, Lcom/facebook/events/model/Event;->S:Ljava/util/TimeZone;

    move-object v0, v0

    .line 1273820
    iput-object v0, p0, LX/7vC;->Q:Ljava/util/TimeZone;

    .line 1273821
    iget v0, p1, Lcom/facebook/events/model/Event;->T:I

    move v0, v0

    .line 1273822
    iput v0, p0, LX/7vC;->R:I

    .line 1273823
    iget-object v0, p1, Lcom/facebook/events/model/Event;->U:Ljava/lang/String;

    move-object v0, v0

    .line 1273824
    iput-object v0, p0, LX/7vC;->S:Ljava/lang/String;

    .line 1273825
    iget-object v0, p1, Lcom/facebook/events/model/Event;->V:Landroid/net/Uri;

    move-object v0, v0

    .line 1273826
    iput-object v0, p0, LX/7vC;->T:Landroid/net/Uri;

    .line 1273827
    iget-object v0, p1, Lcom/facebook/events/model/Event;->W:Ljava/lang/String;

    move-object v0, v0

    .line 1273828
    iput-object v0, p0, LX/7vC;->U:Ljava/lang/String;

    .line 1273829
    iget-object v0, p1, Lcom/facebook/events/model/Event;->X:Landroid/net/Uri;

    move-object v0, v0

    .line 1273830
    iput-object v0, p0, LX/7vC;->V:Landroid/net/Uri;

    .line 1273831
    iget-object v0, p1, Lcom/facebook/events/model/Event;->Y:Landroid/net/Uri;

    move-object v0, v0

    .line 1273832
    iput-object v0, p0, LX/7vC;->W:Landroid/net/Uri;

    .line 1273833
    iget-object v0, p1, Lcom/facebook/events/model/Event;->Z:Landroid/net/Uri;

    move-object v0, v0

    .line 1273834
    iput-object v0, p0, LX/7vC;->X:Landroid/net/Uri;

    .line 1273835
    iget-object v0, p1, Lcom/facebook/events/model/Event;->aa:Ljava/lang/String;

    move-object v0, v0

    .line 1273836
    iput-object v0, p0, LX/7vC;->Y:Ljava/lang/String;

    .line 1273837
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->ac:Z

    move v0, v0

    .line 1273838
    iput-boolean v0, p0, LX/7vC;->aa:Z

    .line 1273839
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->ag()Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/7vC;->a(Ljava/util/EnumSet;)LX/7vC;

    .line 1273840
    iget-object v0, p1, Lcom/facebook/events/model/Event;->ad:Ljava/lang/String;

    move-object v0, v0

    .line 1273841
    iput-object v0, p0, LX/7vC;->ab:Ljava/lang/String;

    .line 1273842
    iget v0, p1, Lcom/facebook/events/model/Event;->ae:I

    move v0, v0

    .line 1273843
    iput v0, p0, LX/7vC;->ac:I

    .line 1273844
    iget-object v0, p1, Lcom/facebook/events/model/Event;->af:Ljava/lang/String;

    move-object v0, v0

    .line 1273845
    iput-object v0, p0, LX/7vC;->ad:Ljava/lang/String;

    .line 1273846
    iget v0, p1, Lcom/facebook/events/model/Event;->ag:I

    move v0, v0

    .line 1273847
    iput v0, p0, LX/7vC;->ae:I

    .line 1273848
    iget-object v0, p1, Lcom/facebook/events/model/Event;->ah:Ljava/lang/String;

    move-object v0, v0

    .line 1273849
    iput-object v0, p0, LX/7vC;->af:Ljava/lang/String;

    .line 1273850
    iget v0, p1, Lcom/facebook/events/model/Event;->ai:I

    move v0, v0

    .line 1273851
    iput v0, p0, LX/7vC;->ag:I

    .line 1273852
    iget-object v0, p1, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v0, v0

    .line 1273853
    iput-object v0, p0, LX/7vC;->ah:Lcom/facebook/events/model/EventUser;

    .line 1273854
    iget-object v0, p1, Lcom/facebook/events/model/Event;->ak:Ljava/lang/String;

    move-object v0, v0

    .line 1273855
    iput-object v0, p0, LX/7vC;->ai:Ljava/lang/String;

    .line 1273856
    iget-object v0, p1, Lcom/facebook/events/model/Event;->am:Ljava/lang/String;

    move-object v0, v0

    .line 1273857
    iput-object v0, p0, LX/7vC;->ak:Ljava/lang/String;

    .line 1273858
    iget-object v0, p1, Lcom/facebook/events/model/Event;->an:Ljava/lang/String;

    move-object v0, v0

    .line 1273859
    iput-object v0, p0, LX/7vC;->al:Ljava/lang/String;

    .line 1273860
    iget-object v0, p1, Lcom/facebook/events/model/Event;->ao:Ljava/lang/String;

    move-object v0, v0

    .line 1273861
    iput-object v0, p0, LX/7vC;->am:Ljava/lang/String;

    .line 1273862
    iget-object v0, p1, Lcom/facebook/events/model/Event;->ap:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-object v0, v0

    .line 1273863
    iput-object v0, p0, LX/7vC;->an:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1273864
    iget v0, p1, Lcom/facebook/events/model/Event;->aq:I

    move v0, v0

    .line 1273865
    iput v0, p0, LX/7vC;->ao:I

    .line 1273866
    return-void
.end method


# virtual methods
.method public final a()LX/7vC;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1273714
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/7vC;->N:J

    .line 1273715
    iput-object v2, p0, LX/7vC;->O:Ljava/lang/String;

    .line 1273716
    iput-object v2, p0, LX/7vC;->M:LX/7vL;

    .line 1273717
    iput-object v2, p0, LX/7vC;->P:Ljava/lang/String;

    .line 1273718
    iput-object v2, p0, LX/7vC;->Q:Ljava/util/TimeZone;

    .line 1273719
    const/4 v0, 0x0

    iput v0, p0, LX/7vC;->R:I

    .line 1273720
    iput-object v2, p0, LX/7vC;->S:Ljava/lang/String;

    .line 1273721
    return-object p0
.end method

.method public final a(DD)LX/7vC;
    .locals 1

    .prologue
    .line 1273734
    new-instance v0, LX/7vL;

    invoke-direct {v0, p1, p2, p3, p4}, LX/7vL;-><init>(DD)V

    iput-object v0, p0, LX/7vC;->M:LX/7vL;

    .line 1273735
    return-object p0
.end method

.method public final a(J)LX/7vC;
    .locals 1

    .prologue
    .line 1273732
    iput-wide p1, p0, LX/7vC;->A:J

    .line 1273733
    return-object p0
.end method

.method public final a(LX/03R;)LX/7vC;
    .locals 0

    .prologue
    .line 1273729
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1273730
    iput-object p1, p0, LX/7vC;->k:LX/03R;

    .line 1273731
    return-object p0
.end method

.method public final a(Ljava/util/EnumSet;)LX/7vC;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "LX/7vK;",
            ">;)",
            "LX/7vC;"
        }
    .end annotation

    .prologue
    .line 1273725
    if-nez p1, :cond_0

    .line 1273726
    iget-object v0, p0, LX/7vC;->Z:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->clear()V

    .line 1273727
    :goto_0
    return-object p0

    .line 1273728
    :cond_0
    iput-object p1, p0, LX/7vC;->Z:Ljava/util/EnumSet;

    goto :goto_0
.end method

.method public final b(J)LX/7vC;
    .locals 1

    .prologue
    .line 1273723
    iput-wide p1, p0, LX/7vC;->N:J

    .line 1273724
    return-object p0
.end method

.method public final b()Lcom/facebook/events/model/Event;
    .locals 2

    .prologue
    .line 1273722
    new-instance v0, Lcom/facebook/events/model/Event;

    invoke-direct {v0, p0}, Lcom/facebook/events/model/Event;-><init>(LX/7vC;)V

    return-object v0
.end method
