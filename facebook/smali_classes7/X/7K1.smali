.class public LX/7K1;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:LX/0So;

.field public final d:Landroid/net/Uri;

.field public e:I

.field public f:I

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:I

.field public j:I

.field public k:Z

.field public l:J

.field public final m:LX/0Kx;

.field public final n:LX/0Ji;

.field public o:LX/0GT;

.field public p:LX/0GT;

.field public q:I

.field public r:Ljava/lang/String;

.field public s:Lcom/facebook/video/engine/texview/exo/ExoVideoPlayer;

.field public final t:LX/7Jz;

.field private final u:LX/0Jp;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1194369
    const-class v0, LX/7K1;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/7K1;->a:Ljava/lang/String;

    .line 1194370
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "STATE_BUFFERING"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "STATE_ENDED"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "STATE_IDLE"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "STATE_PREPARING"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "STATE_READY"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/7K1;->b:LX/0P1;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;LX/1m6;ZLX/7Jw;LX/0So;)V
    .locals 8
    .param p1    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1m6;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 1194371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1194372
    iput v2, p0, LX/7K1;->e:I

    .line 1194373
    iput v2, p0, LX/7K1;->f:I

    .line 1194374
    new-instance v0, LX/7Jz;

    invoke-direct {v0, p0}, LX/7Jz;-><init>(LX/7K1;)V

    iput-object v0, p0, LX/7K1;->t:LX/7Jz;

    .line 1194375
    new-instance v0, LX/7Jx;

    invoke-direct {v0, p0}, LX/7Jx;-><init>(LX/7K1;)V

    iput-object v0, p0, LX/7K1;->u:LX/0Jp;

    .line 1194376
    iput-object p7, p0, LX/7K1;->c:LX/0So;

    .line 1194377
    iput-object p1, p0, LX/7K1;->d:Landroid/net/Uri;

    .line 1194378
    invoke-interface {p7}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/7K1;->l:J

    .line 1194379
    const/16 v0, 0x1f4

    const/16 v1, 0x7d0

    invoke-static {v2, v0, v1}, LX/0Kw;->a(III)LX/0Kx;

    move-result-object v0

    iput-object v0, p0, LX/7K1;->m:LX/0Kx;

    .line 1194380
    iget-object v0, p0, LX/7K1;->m:LX/0Kx;

    iget-object v1, p0, LX/7K1;->t:LX/7Jz;

    invoke-interface {v0, v1}, LX/0Kx;->a(LX/0KW;)V

    .line 1194381
    new-instance v5, LX/7K0;

    invoke-direct {v5, p0}, LX/7K0;-><init>(LX/7K1;)V

    new-instance v6, LX/7Jy;

    invoke-direct {v6, p0, p1}, LX/7Jy;-><init>(LX/7K1;Landroid/net/Uri;)V

    move-object v0, p6

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v7, p5

    invoke-virtual/range {v0 .. v7}, LX/7Jw;->a(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;LX/1m6;LX/0Jy;LX/0KZ;Z)LX/0Ji;

    move-result-object v0

    iput-object v0, p0, LX/7K1;->n:LX/0Ji;

    .line 1194382
    iget-object v0, p0, LX/7K1;->n:LX/0Ji;

    iget-object v1, p0, LX/7K1;->u:LX/0Jp;

    invoke-virtual {v0, v1}, LX/0Ji;->a(LX/0Jp;)V

    .line 1194383
    return-void
.end method

.method public static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1194384
    sget-object v0, LX/7K1;->b:LX/0P1;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static a$redex0(LX/7K1;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 1194385
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/7K1;->k:Z

    .line 1194386
    return-void
.end method


# virtual methods
.method public final g()V
    .locals 2

    .prologue
    .line 1194387
    iget-object v0, p0, LX/7K1;->m:LX/0Kx;

    iget-object v1, p0, LX/7K1;->t:LX/7Jz;

    invoke-interface {v0, v1}, LX/0Kx;->b(LX/0KW;)V

    .line 1194388
    iget-object v0, p0, LX/7K1;->m:LX/0Kx;

    invoke-interface {v0}, LX/0Kx;->d()V

    .line 1194389
    return-void
.end method
