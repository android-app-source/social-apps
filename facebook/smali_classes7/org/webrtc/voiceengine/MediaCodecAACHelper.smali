.class public Lorg/webrtc/voiceengine/MediaCodecAACHelper;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static final a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1221650
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/webrtc/voiceengine/MediaCodecAACHelper;->a:[I

    return-void

    :array_0
    .array-data 4
        0x17700
        0x15888
        0xfa00
        0xbb80
        0xac44
        0x7d00
        0x5dc0
        0x5622
        0x3e80
        0x2ee0
        0x2b11
        0x1f40
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1221648
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1221649
    return-void
.end method

.method public static a(LX/7fG;)I
    .locals 2

    .prologue
    .line 1221644
    sget-object v0, LX/7fH;->a:[I

    invoke-virtual {p0}, LX/7fG;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1221645
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported profile level"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1221646
    :pswitch_0
    const/4 v0, 0x1

    .line 1221647
    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(III)Ljava/nio/ByteBuffer;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 1221634
    move v0, v1

    move v2, v3

    .line 1221635
    :goto_0
    sget-object v4, Lorg/webrtc/voiceengine/MediaCodecAACHelper;->a:[I

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 1221636
    sget-object v4, Lorg/webrtc/voiceengine/MediaCodecAACHelper;->a:[I

    aget v4, v4, v0

    if-ne v4, p0, :cond_0

    move v2, v0

    .line 1221637
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1221638
    :cond_1
    if-ne v2, v3, :cond_2

    .line 1221639
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported sampling rate"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1221640
    :cond_2
    const/4 v0, 0x2

    new-array v0, v0, [B

    .line 1221641
    shl-int/lit8 v3, p2, 0x3

    shr-int/lit8 v4, v2, 0x1

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 1221642
    const/4 v1, 0x1

    shl-int/lit8 v2, v2, 0x7

    and-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    shl-int/lit8 v3, p1, 0x3

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 1221643
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static findDecoder(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1221608
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 1221609
    :goto_0
    return-object v4

    :cond_0
    move v2, v3

    move-object v1, v4

    .line 1221610
    :goto_1
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1221611
    invoke-static {v2}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v5

    .line 1221612
    invoke-virtual {v5}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1221613
    invoke-virtual {v5}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    move v0, v3

    :goto_2
    if-ge v0, v7, :cond_4

    aget-object v8, v6, v0

    .line 1221614
    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1221615
    invoke-virtual {v5}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1221616
    :goto_3
    if-eqz v0, :cond_3

    .line 1221617
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "[AAC]Found candidate encoder "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1221618
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_1

    .line 1221619
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move-object v4, v1

    .line 1221620
    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_4

    :cond_4
    move-object v0, v4

    goto :goto_3
.end method

.method public static findEncoder(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1221621
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 1221622
    :goto_0
    return-object v4

    :cond_0
    move v2, v3

    move-object v1, v4

    .line 1221623
    :goto_1
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1221624
    invoke-static {v2}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v5

    .line 1221625
    invoke-virtual {v5}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1221626
    invoke-virtual {v5}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    move v0, v3

    :goto_2
    if-ge v0, v7, :cond_4

    aget-object v8, v6, v0

    .line 1221627
    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1221628
    invoke-virtual {v5}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1221629
    :goto_3
    if-eqz v0, :cond_3

    .line 1221630
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "[AAC]Found candidate encoder "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1221631
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_1

    .line 1221632
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move-object v4, v1

    .line 1221633
    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_4

    :cond_4
    move-object v0, v4

    goto :goto_3
.end method
