.class public final Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;
.super Ljava/lang/Thread;
.source ""


# instance fields
.field public final synthetic a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

.field public volatile b:Z


# direct methods
.method public constructor <init>(Lorg/webrtc/voiceengine/WebRtcAudioTrack;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1221957
    iput-object p1, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    .line 1221958
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 1221959
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->b:Z

    .line 1221960
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1221961
    const/16 v0, -0x13

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 1221962
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v0, v0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v3, "AudioTrackThread %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {}, LX/7fJ;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v0, v3, v4}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221963
    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v0, v0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v4, 0x3

    if-ne v0, v4, :cond_1

    move v0, v1

    :goto_0
    const-string v4, "AudioTrack is not int play state in processing thread"

    new-array v5, v2, [Ljava/lang/Object;

    .line 1221964
    invoke-static {v3, v0, v4, v5}, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a$redex0(Lorg/webrtc/voiceengine/WebRtcAudioTrack;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1221965
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v0, v0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    .line 1221966
    :goto_1
    iget-boolean v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->b:Z

    if-eqz v0, :cond_4

    .line 1221967
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v4, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-wide v4, v4, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->c:J

    invoke-static {v0, v3, v4, v5}, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a(Lorg/webrtc/voiceengine/WebRtcAudioTrack;IJ)V

    .line 1221968
    iget-object v4, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v0, v0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-gt v3, v0, :cond_2

    move v0, v1

    :goto_2
    const-string v5, "Invalid buffer size %d > %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    iget-object v7, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v7, v7, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    .line 1221969
    invoke-static {v4, v0, v5, v6}, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a$redex0(Lorg/webrtc/voiceengine/WebRtcAudioTrack;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1221970
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v0, v4, :cond_6

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 1221971
    if-eqz v0, :cond_3

    .line 1221972
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v0, v0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    iget-object v4, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v4, v4, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v4, v3, v2}, Landroid/media/AudioTrack;->write(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 1221973
    :goto_4
    if-eq v0, v3, :cond_0

    .line 1221974
    iget-object v4, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v4, v4, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v5, "AudioTrack.write failed: %d"

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v4, v5, v6}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221975
    const/4 v4, -0x3

    if-ne v0, v4, :cond_0

    .line 1221976
    iput-boolean v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->b:Z

    .line 1221977
    :cond_0
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v0, v0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    goto :goto_1

    :cond_1
    move v0, v2

    .line 1221978
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1221979
    goto :goto_2

    .line 1221980
    :cond_3
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v0, v0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    iget-object v4, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v4, v4, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    iget-object v5, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v5, v5, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v5

    invoke-virtual {v0, v4, v5, v3}, Landroid/media/AudioTrack;->write([BII)I

    move-result v0

    goto :goto_4

    .line 1221981
    :cond_4
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v0, v0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->stop()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1221982
    :goto_5
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v0, v0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->flush()V

    .line 1221983
    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v0, v0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_6
    const-string v4, "AudioTrack is not stopped properly %d"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v5, v5, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v5}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 1221984
    invoke-static {v3, v0, v4, v1}, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a$redex0(Lorg/webrtc/voiceengine/WebRtcAudioTrack;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1221985
    return-void

    .line 1221986
    :catch_0
    move-exception v0

    .line 1221987
    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v3, v3, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v4, "AudioTrack.stop illegal state"

    invoke-virtual {v3, v4, v0}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 1221988
    :catch_1
    move-exception v0

    .line 1221989
    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->a:Lorg/webrtc/voiceengine/WebRtcAudioTrack;

    iget-object v3, v3, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v4, "AudioTrack.stop unknown exception"

    invoke-virtual {v3, v4, v0}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_5
    move v0, v2

    .line 1221990
    goto :goto_6

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_3
.end method
