.class public Lorg/webrtc/voiceengine/WebRtcEngineLogger;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public a:I

.field public b:LX/7fI;


# direct methods
.method public constructor <init>(LX/7fI;)V
    .locals 1

    .prologue
    .line 1222101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1222102
    const/4 v0, 0x0

    iput v0, p0, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a:I

    .line 1222103
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->b:LX/7fI;

    .line 1222104
    iput-object p1, p0, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->b:LX/7fI;

    .line 1222105
    return-void
.end method

.method private varargs a(ILjava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1222082
    iget v0, p0, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a:I

    if-ge p1, v0, :cond_0

    .line 1222083
    :goto_0
    return-void

    .line 1222084
    :cond_0
    invoke-static {p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1222085
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->b:LX/7fI;

    invoke-interface {v1, p1, v0}, LX/7fI;->a(ILjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1222094
    const/16 v0, 0x40

    .line 1222095
    iget v1, p0, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a:I

    if-ge v0, v1, :cond_0

    .line 1222096
    :goto_0
    return-void

    .line 1222097
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1222098
    const-string v1, "%s (%s)"

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1222099
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->b:LX/7fI;

    invoke-interface {v2, v0, v1}, LX/7fI;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 1222100
    :cond_1
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->b:LX/7fI;

    invoke-interface {v1, v0, p1}, LX/7fI;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public final varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1222092
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1, p2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 1222093
    return-void
.end method

.method public final varargs b(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1222090
    const/16 v0, 0x20

    invoke-direct {p0, v0, p1, p2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 1222091
    return-void
.end method

.method public final varargs c(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1222088
    const/16 v0, 0x40

    invoke-direct {p0, v0, p1, p2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 1222089
    return-void
.end method

.method public setNativeTraceLevel(I)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1222086
    iput p1, p0, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a:I

    .line 1222087
    return-void
.end method
