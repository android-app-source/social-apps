.class public Lorg/webrtc/voiceengine/WebRtcAudioManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7fI;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private final a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

.field private final c:J

.field private final d:Landroid/content/Context;

.field private final e:Landroid/media/AudioManager;

.field private f:Z

.field private g:Z

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1221691
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "MODE_NORMAL"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "MODE_RINGTONE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "MODE_IN_CALL"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "MODE_IN_COMMUNICATION"

    aput-object v2, v0, v1

    sput-object v0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;J)V
    .locals 8
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1221692
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1221693
    new-instance v0, Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    invoke-direct {v0, p0}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;-><init>(LX/7fI;)V

    iput-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    .line 1221694
    iput-boolean v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->f:Z

    .line 1221695
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ctor"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/7fJ;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221696
    iput-object p1, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->d:Landroid/content/Context;

    .line 1221697
    iput-wide p2, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->c:J

    .line 1221698
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->e:Landroid/media/AudioManager;

    .line 1221699
    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->a()V

    .line 1221700
    iget v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->h:I

    iget v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->i:I

    iget-boolean v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->g:Z

    iget v4, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->j:I

    iget v5, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->k:I

    move-object v0, p0

    move-wide v6, p2

    invoke-direct/range {v0 .. v7}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->nativeCacheAudioParameters(IIZIIJ)V

    .line 1221701
    return-void
.end method

.method private static a(II)I
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 1221702
    mul-int/lit8 v1, p1, 0x2

    .line 1221703
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1221704
    const/4 v0, 0x4

    .line 1221705
    :goto_0
    invoke-static {p0, v0, v2}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v0

    div-int/2addr v0, v1

    :goto_1
    return v0

    .line 1221706
    :cond_0
    if-ne p1, v2, :cond_1

    .line 1221707
    const/16 v0, 0xc

    goto :goto_0

    .line 1221708
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1221657
    const/4 v0, 0x1

    iput v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->i:I

    .line 1221658
    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->b()I

    move-result v0

    iput v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->h:I

    .line 1221659
    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->isLowLatencyOutputSupported()Z

    move-result v0

    iput-boolean v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->g:Z

    .line 1221660
    iget-boolean v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->g:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->d()I

    move-result v0

    :goto_0
    iput v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->j:I

    .line 1221661
    iget v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->h:I

    iget v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->i:I

    invoke-direct {p0, v0, v1}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->b(II)I

    move-result v0

    iput v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->k:I

    .line 1221662
    return-void

    .line 1221663
    :cond_0
    iget v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->h:I

    iget v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->i:I

    invoke-static {v0, v1}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->a(II)I

    move-result v0

    goto :goto_0
.end method

.method private varargs a(ZLjava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1221709
    if-nez p1, :cond_1

    .line 1221710
    invoke-static {p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1221711
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 1221712
    if-eqz v1, :cond_0

    .line 1221713
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 1221714
    :cond_0
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221715
    :cond_1
    return-void
.end method

.method private b()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1221716
    sget-object v0, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    const-string v1, "goldfish"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    const-string v1, "generic_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1221717
    if-eqz v0, :cond_0

    .line 1221718
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "Running emulator, overriding sample rate to 8 kHz."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221719
    const/16 v0, 0x1f40

    .line 1221720
    :goto_1
    return v0

    .line 1221721
    :cond_0
    invoke-static {}, LX/7fJ;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1221722
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Default sample rate is overriden to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/7fJ;->g()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Hz"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221723
    invoke-static {}, LX/7fJ;->g()I

    move-result v0

    goto :goto_1

    .line 1221724
    :cond_1
    invoke-static {}, LX/7fJ;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1221725
    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->c()I

    move-result v0

    .line 1221726
    :goto_2
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sample rate is set to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Hz"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1221727
    :cond_2
    invoke-static {}, LX/7fJ;->g()I

    move-result v0

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(II)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1221728
    mul-int/lit8 v3, p2, 0x2

    .line 1221729
    if-ne p2, v1, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "numChannels is %d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-direct {p0, v0, v4, v1}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1221730
    const/16 v0, 0x10

    const/4 v1, 0x2

    invoke-static {p1, v0, v1}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v0

    div-int/2addr v0, v3

    return v0

    :cond_0
    move v0, v2

    .line 1221731
    goto :goto_0
.end method

.method private c()I
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 1221732
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->e:Landroid/media/AudioManager;

    const-string v1, "android.media.property.OUTPUT_SAMPLE_RATE"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1221733
    if-nez v0, :cond_0

    invoke-static {}, LX/7fJ;->g()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private d()I
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/16 v0, 0x100

    .line 1221683
    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->isLowLatencyOutputSupported()Z

    move-result v1

    const-string v2, "low latency output not supported"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {p0, v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1221684
    invoke-static {}, LX/7fJ;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1221685
    :cond_0
    :goto_0
    return v0

    .line 1221686
    :cond_1
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->e:Landroid/media/AudioManager;

    const-string v2, "android.media.property.OUTPUT_FRAMES_PER_BUFFER"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1221687
    if-eqz v1, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private dispose()V
    .locals 3
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1221688
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "dispose"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/7fJ;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221689
    iget-boolean v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->f:Z

    if-nez v0, :cond_0

    .line 1221690
    :cond_0
    return-void
.end method

.method private getAudioManagerState()I
    .locals 5
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1221672
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getAudioManagerState"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/7fJ;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221673
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->e:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1221674
    :goto_0
    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->e:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1221675
    :goto_1
    shl-int/lit8 v1, v1, 0x1

    or-int/2addr v0, v1

    .line 1221676
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->e:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->getMode()I

    move-result v1

    and-int/lit8 v1, v1, 0x7

    shl-int/lit8 v1, v1, 0x4

    or-int/2addr v0, v1

    .line 1221677
    return v0

    :cond_0
    move v0, v2

    .line 1221678
    goto :goto_0

    .line 1221679
    :cond_1
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->e:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isBluetoothScoOn()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1221680
    const/4 v1, 0x3

    goto :goto_1

    .line 1221681
    :cond_2
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->e:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1221682
    const/4 v1, 0x4

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method private getAudioMode()I
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1221671
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->e:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v0

    return v0
.end method

.method private init()Z
    .locals 6
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1221666
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "init"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/7fJ;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221667
    iget-boolean v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->f:Z

    if-eqz v0, :cond_0

    .line 1221668
    :goto_0
    return v4

    .line 1221669
    :cond_0
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "audio mode is: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lorg/webrtc/voiceengine/WebRtcAudioManager;->b:[Ljava/lang/String;

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->e:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->getMode()I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221670
    iput-boolean v4, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->f:Z

    goto :goto_0
.end method

.method private isCommunicationModeEnabled()Z
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1221665
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->e:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isLowLatencyOutputSupported()Z
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1221664
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.audio.low_latency"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private native nativeCacheAudioParameters(IIZIIJ)V
.end method

.method private native nativeEngineTrace(ILjava/lang/String;)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private unmuteMicrophone()V
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1221653
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unmuteMicrophone"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/7fJ;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221654
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->e:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1221655
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioManager;->e:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    .line 1221656
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 1221651
    invoke-direct {p0, p1, p2}, Lorg/webrtc/voiceengine/WebRtcAudioManager;->nativeEngineTrace(ILjava/lang/String;)V

    .line 1221652
    return-void
.end method
