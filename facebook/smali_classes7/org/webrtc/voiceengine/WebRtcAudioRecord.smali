.class public Lorg/webrtc/voiceengine/WebRtcAudioRecord;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7fI;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public final a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

.field public final b:J

.field private final c:Landroid/content/Context;

.field public d:Ljava/nio/ByteBuffer;

.field public e:Landroid/media/AudioRecord;

.field private f:Lorg/webrtc/voiceengine/WebRtcAudioRecord$AudioRecordThread;

.field private g:Landroid/media/audiofx/AcousticEchoCanceler;

.field private h:Landroid/media/audiofx/AutomaticGainControl;

.field private i:Landroid/media/audiofx/NoiseSuppressor;

.field private j:Z

.field private k:Z

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1221865
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1221866
    new-instance v0, Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    invoke-direct {v0, p0}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;-><init>(LX/7fI;)V

    iput-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    .line 1221867
    iput-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->d:Ljava/nio/ByteBuffer;

    .line 1221868
    iput-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    .line 1221869
    iput-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->f:Lorg/webrtc/voiceengine/WebRtcAudioRecord$AudioRecordThread;

    .line 1221870
    iput-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->g:Landroid/media/audiofx/AcousticEchoCanceler;

    .line 1221871
    iput-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->h:Landroid/media/audiofx/AutomaticGainControl;

    .line 1221872
    iput-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->i:Landroid/media/audiofx/NoiseSuppressor;

    .line 1221873
    iput-boolean v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->j:Z

    .line 1221874
    iput-boolean v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->k:Z

    .line 1221875
    iput-boolean v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->l:Z

    .line 1221876
    iput-object p1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->c:Landroid/content/Context;

    .line 1221877
    iput-wide p2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->b:J

    .line 1221878
    return-void
.end method

.method private a()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1221879
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->g:Landroid/media/audiofx/AcousticEchoCanceler;

    if-eqz v0, :cond_0

    .line 1221880
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->g:Landroid/media/audiofx/AcousticEchoCanceler;

    invoke-virtual {v0}, Landroid/media/audiofx/AcousticEchoCanceler;->release()V

    .line 1221881
    iput-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->g:Landroid/media/audiofx/AcousticEchoCanceler;

    .line 1221882
    :cond_0
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->h:Landroid/media/audiofx/AutomaticGainControl;

    if-eqz v0, :cond_1

    .line 1221883
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->h:Landroid/media/audiofx/AutomaticGainControl;

    invoke-virtual {v0}, Landroid/media/audiofx/AutomaticGainControl;->release()V

    .line 1221884
    iput-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->h:Landroid/media/audiofx/AutomaticGainControl;

    .line 1221885
    :cond_1
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->i:Landroid/media/audiofx/NoiseSuppressor;

    if-eqz v0, :cond_2

    .line 1221886
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->i:Landroid/media/audiofx/NoiseSuppressor;

    invoke-virtual {v0}, Landroid/media/audiofx/NoiseSuppressor;->release()V

    .line 1221887
    iput-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->i:Landroid/media/audiofx/NoiseSuppressor;

    .line 1221888
    :cond_2
    return-void
.end method

.method public static synthetic a(Lorg/webrtc/voiceengine/WebRtcAudioRecord;IJ)V
    .locals 0

    .prologue
    .line 1221889
    invoke-direct {p0, p1, p2, p3}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->nativeDataIsRecorded(IJ)V

    return-void
.end method

.method public static varargs a$redex0(Lorg/webrtc/voiceengine/WebRtcAudioRecord;ZLjava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1221890
    if-nez p1, :cond_1

    .line 1221891
    invoke-static {p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1221892
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 1221893
    if-eqz v1, :cond_0

    .line 1221894
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 1221895
    :cond_0
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221896
    :cond_1
    return-void
.end method

.method private b()Z
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1221897
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->g:Landroid/media/audiofx/AcousticEchoCanceler;

    if-nez v2, :cond_3

    .line 1221898
    :try_start_0
    invoke-static {}, Landroid/media/audiofx/AcousticEchoCanceler;->isAvailable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1221899
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "AcousticEchoCanceler api is not supported"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221900
    :cond_0
    :goto_0
    return v0

    .line 1221901
    :cond_1
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    if-eqz v2, :cond_0

    .line 1221902
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->getAudioSessionId()I

    move-result v2

    invoke-static {v2}, Landroid/media/audiofx/AcousticEchoCanceler;->create(I)Landroid/media/audiofx/AcousticEchoCanceler;

    move-result-object v2

    iput-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->g:Landroid/media/audiofx/AcousticEchoCanceler;

    .line 1221903
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->g:Landroid/media/audiofx/AcousticEchoCanceler;

    if-nez v2, :cond_2

    .line 1221904
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "AcousticEchoCanceler.create failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1221905
    :catch_0
    move-exception v1

    .line 1221906
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v3, "AEC Creation exception"

    invoke-virtual {v2, v3, v1}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1221907
    :cond_2
    :try_start_1
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->g:Landroid/media/audiofx/AcousticEchoCanceler;

    invoke-virtual {v2}, Landroid/media/audiofx/AcousticEchoCanceler;->getDescriptor()Landroid/media/audiofx/AudioEffect$Descriptor;

    move-result-object v2

    .line 1221908
    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v4, "AcousticEchoCanceler name: %s implementor: %s uuid: %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v2, Landroid/media/audiofx/AudioEffect$Descriptor;->name:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, v2, Landroid/media/audiofx/AudioEffect$Descriptor;->implementor:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget-object v2, v2, Landroid/media/audiofx/AudioEffect$Descriptor;->uuid:Ljava/util/UUID;

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1221909
    :cond_3
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->g:Landroid/media/audiofx/AcousticEchoCanceler;

    if-eqz v2, :cond_4

    .line 1221910
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v3, "Default AEC: %b"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->g:Landroid/media/audiofx/AcousticEchoCanceler;

    invoke-virtual {v5}, Landroid/media/audiofx/AcousticEchoCanceler;->getEnabled()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {v2, v3, v4}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221911
    iget-boolean v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->j:Z

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->g:Landroid/media/audiofx/AcousticEchoCanceler;

    invoke-virtual {v3}, Landroid/media/audiofx/AcousticEchoCanceler;->getEnabled()Z

    move-result v3

    if-eq v2, v3, :cond_4

    .line 1221912
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->g:Landroid/media/audiofx/AcousticEchoCanceler;

    iget-boolean v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->j:Z

    invoke-virtual {v2, v3}, Landroid/media/audiofx/AcousticEchoCanceler;->setEnabled(Z)I

    move-result v2

    .line 1221913
    if-eqz v2, :cond_4

    .line 1221914
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "AcousticEchoCanceler.setEnabled failed"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1221915
    goto :goto_0
.end method

.method private c()Z
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1221916
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->h:Landroid/media/audiofx/AutomaticGainControl;

    if-nez v2, :cond_3

    .line 1221917
    :try_start_0
    invoke-static {}, Landroid/media/audiofx/AutomaticGainControl;->isAvailable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1221918
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "AutomaticGainControl api is not supported"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221919
    :cond_0
    :goto_0
    return v0

    .line 1221920
    :cond_1
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    if-eqz v2, :cond_0

    .line 1221921
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->getAudioSessionId()I

    move-result v2

    invoke-static {v2}, Landroid/media/audiofx/AutomaticGainControl;->create(I)Landroid/media/audiofx/AutomaticGainControl;

    move-result-object v2

    iput-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->h:Landroid/media/audiofx/AutomaticGainControl;

    .line 1221922
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->h:Landroid/media/audiofx/AutomaticGainControl;

    if-nez v2, :cond_2

    .line 1221923
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "AutomaticGainControl.create failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1221924
    :catch_0
    move-exception v1

    .line 1221925
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v3, "AGC Creation exception"

    invoke-virtual {v2, v3, v1}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1221926
    :cond_2
    :try_start_1
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->h:Landroid/media/audiofx/AutomaticGainControl;

    invoke-virtual {v2}, Landroid/media/audiofx/AutomaticGainControl;->getDescriptor()Landroid/media/audiofx/AudioEffect$Descriptor;

    move-result-object v2

    .line 1221927
    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v4, "AutomaticGainControl name: %s implementor: %s uuid: %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v2, Landroid/media/audiofx/AudioEffect$Descriptor;->name:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, v2, Landroid/media/audiofx/AudioEffect$Descriptor;->implementor:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget-object v2, v2, Landroid/media/audiofx/AudioEffect$Descriptor;->uuid:Ljava/util/UUID;

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1221928
    :cond_3
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->h:Landroid/media/audiofx/AutomaticGainControl;

    if-eqz v2, :cond_4

    .line 1221929
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v3, "Default AGC: %b"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->h:Landroid/media/audiofx/AutomaticGainControl;

    invoke-virtual {v5}, Landroid/media/audiofx/AutomaticGainControl;->getEnabled()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {v2, v3, v4}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221930
    iget-boolean v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->k:Z

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->h:Landroid/media/audiofx/AutomaticGainControl;

    invoke-virtual {v3}, Landroid/media/audiofx/AutomaticGainControl;->getEnabled()Z

    move-result v3

    if-eq v2, v3, :cond_4

    .line 1221931
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->h:Landroid/media/audiofx/AutomaticGainControl;

    iget-boolean v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->k:Z

    invoke-virtual {v2, v3}, Landroid/media/audiofx/AutomaticGainControl;->setEnabled(Z)I

    move-result v2

    .line 1221932
    if-eqz v2, :cond_4

    .line 1221933
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "AutomaticGainControl.setEnabled failed"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1221934
    goto :goto_0
.end method

.method private d()Z
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1221935
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->i:Landroid/media/audiofx/NoiseSuppressor;

    if-nez v2, :cond_3

    .line 1221936
    :try_start_0
    invoke-static {}, Landroid/media/audiofx/NoiseSuppressor;->isAvailable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1221937
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "NoiseSuppressor api is not supported"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221938
    :cond_0
    :goto_0
    return v0

    .line 1221939
    :cond_1
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    if-eqz v2, :cond_0

    .line 1221940
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->getAudioSessionId()I

    move-result v2

    invoke-static {v2}, Landroid/media/audiofx/NoiseSuppressor;->create(I)Landroid/media/audiofx/NoiseSuppressor;

    move-result-object v2

    iput-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->i:Landroid/media/audiofx/NoiseSuppressor;

    .line 1221941
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->i:Landroid/media/audiofx/NoiseSuppressor;

    if-nez v2, :cond_2

    .line 1221942
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "NoiseSuppressor.create failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1221943
    :catch_0
    move-exception v1

    .line 1221944
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v3, "NS Creation exception"

    invoke-virtual {v2, v3, v1}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1221945
    :cond_2
    :try_start_1
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->i:Landroid/media/audiofx/NoiseSuppressor;

    invoke-virtual {v2}, Landroid/media/audiofx/NoiseSuppressor;->getDescriptor()Landroid/media/audiofx/AudioEffect$Descriptor;

    move-result-object v2

    .line 1221946
    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v4, "NoiseSuppressor name: %s implementor: %s uuid: %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v2, Landroid/media/audiofx/AudioEffect$Descriptor;->name:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, v2, Landroid/media/audiofx/AudioEffect$Descriptor;->implementor:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget-object v2, v2, Landroid/media/audiofx/AudioEffect$Descriptor;->uuid:Ljava/util/UUID;

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1221947
    :cond_3
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->i:Landroid/media/audiofx/NoiseSuppressor;

    if-eqz v2, :cond_4

    .line 1221948
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v3, "Default NS: %b"

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->i:Landroid/media/audiofx/NoiseSuppressor;

    invoke-virtual {v5}, Landroid/media/audiofx/NoiseSuppressor;->getEnabled()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {v2, v3, v4}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221949
    iget-boolean v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->l:Z

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->i:Landroid/media/audiofx/NoiseSuppressor;

    invoke-virtual {v3}, Landroid/media/audiofx/NoiseSuppressor;->getEnabled()Z

    move-result v3

    if-eq v2, v3, :cond_4

    .line 1221950
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->i:Landroid/media/audiofx/NoiseSuppressor;

    iget-boolean v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->l:Z

    invoke-virtual {v2, v3}, Landroid/media/audiofx/NoiseSuppressor;->setEnabled(Z)I

    move-result v2

    .line 1221951
    if-eqz v2, :cond_4

    .line 1221952
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "NoiseSuppressor.setEnabled failed"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1221953
    goto :goto_0
.end method

.method private e()Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1221954
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->g:Landroid/media/audiofx/AcousticEchoCanceler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->g:Landroid/media/audiofx/AcousticEchoCanceler;

    invoke-virtual {v0}, Landroid/media/audiofx/AcousticEchoCanceler;->getEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1221955
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->h:Landroid/media/audiofx/AutomaticGainControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->h:Landroid/media/audiofx/AutomaticGainControl;

    invoke-virtual {v0}, Landroid/media/audiofx/AutomaticGainControl;->getEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1221956
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->i:Landroid/media/audiofx/NoiseSuppressor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->i:Landroid/media/audiofx/NoiseSuppressor;

    invoke-virtual {v0}, Landroid/media/audiofx/NoiseSuppressor;->getEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native nativeCacheDirectRecordBufferAddress(Ljava/nio/ByteBuffer;J)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private native nativeCacheRecordAudioParameters(IIZIJ)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private native nativeDataIsRecorded(IJ)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private native nativeEngineTrace(ILjava/lang/String;)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 1221760
    invoke-direct {p0, p1, p2}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->nativeEngineTrace(ILjava/lang/String;)V

    .line 1221761
    return-void
.end method

.method public enableBuiltInAEC(Z)Z
    .locals 5
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1221762
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "enableBuiltInAEC(%b)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221763
    iput-boolean p1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->j:Z

    .line 1221764
    invoke-static {}, LX/7fJ;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1221765
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->b()Z

    move-result v0

    goto :goto_0
.end method

.method public enableBuiltInAGC(Z)Z
    .locals 5
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1221766
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "enableBuiltInAGC(%b)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221767
    iput-boolean p1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->k:Z

    .line 1221768
    invoke-static {}, LX/7fJ;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1221769
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->c()Z

    move-result v0

    goto :goto_0
.end method

.method public enableBuiltInNS(Z)Z
    .locals 5
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1221770
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "enableBuiltInNS(%b)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221771
    iput-boolean p1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->l:Z

    .line 1221772
    invoke-static {}, LX/7fJ;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1221773
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->d()Z

    move-result v0

    goto :goto_0
.end method

.method public initRecording(II)I
    .locals 12
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v10, 0x7

    const/4 v11, 0x2

    const/4 v8, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1221774
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "initRecording(sampleRate=%d, channels=%d)"

    new-array v2, v11, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221775
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->c:Landroid/content/Context;

    const-string v1, "android.permission.RECORD_AUDIO"

    .line 1221776
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v2

    if-nez v2, :cond_a

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 1221777
    if-nez v0, :cond_0

    .line 1221778
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "RECORD_AUDIO permission is missing"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v8

    .line 1221779
    :goto_1
    return v0

    .line 1221780
    :cond_0
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    if-eqz v0, :cond_1

    .line 1221781
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "initRecording() called twice without stopRecording()"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221782
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 1221783
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    .line 1221784
    :cond_1
    mul-int/lit8 v0, p2, 0x2

    .line 1221785
    div-int/lit8 v9, p1, 0x64

    .line 1221786
    mul-int/2addr v0, v9

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->d:Ljava/nio/ByteBuffer;

    .line 1221787
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "mByteBuffer.capacity: %d"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221788
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->d:Ljava/nio/ByteBuffer;

    iget-wide v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->b:J

    invoke-direct {p0, v0, v2, v3}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->nativeCacheDirectRecordBufferAddress(Ljava/nio/ByteBuffer;J)V

    .line 1221789
    const/16 v0, 0x10

    invoke-static {p1, v0, v11}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v0

    .line 1221790
    if-eq v0, v8, :cond_2

    const/4 v1, -0x2

    if-ne v0, v1, :cond_3

    .line 1221791
    :cond_2
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "AudioRecord.getMinBufferSize failed: %d"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v8

    .line 1221792
    goto :goto_1

    .line 1221793
    :cond_3
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "AudioRecord.getMinBufferSize: %d"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221794
    mul-int/lit8 v0, v0, 0x2

    .line 1221795
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 1221796
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "bufferSizeInBytes: %d"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221797
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_b

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 1221798
    if-nez v0, :cond_9

    move v1, v6

    .line 1221799
    :goto_3
    :try_start_0
    new-instance v0, Landroid/media/AudioRecord;

    const/16 v3, 0x10

    const/4 v4, 0x2

    move v2, p1

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1221800
    :goto_4
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    if-nez v0, :cond_4

    .line 1221801
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "Failed to create a new AudioRecord instance"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v8

    .line 1221802
    goto/16 :goto_1

    .line 1221803
    :catch_0
    move-exception v0

    .line 1221804
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v3, "new AudioRecord illegal argument"

    invoke-virtual {v2, v3, v0}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 1221805
    :catch_1
    move-exception v0

    .line 1221806
    iget-object v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v3, "new AudioRecord unknown exception"

    invoke-virtual {v2, v3, v0}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 1221807
    :cond_4
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    if-eq v0, v6, :cond_5

    .line 1221808
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "AudioRecord is not intialized (%d)"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v3}, Landroid/media/AudioRecord;->getState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221809
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 1221810
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    move v0, v8

    .line 1221811
    goto/16 :goto_1

    .line 1221812
    :cond_5
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v0

    if-ne v0, v6, :cond_7

    move v0, v6

    :goto_5
    const-string v2, "Incorrect recording state %d"

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v4}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {p0, v0, v2, v3}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a$redex0(Lorg/webrtc/voiceengine/WebRtcAudioRecord;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1221813
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getAudioSource()I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v6

    :goto_6
    const-string v2, "Incorrect audio source %d != %d"

    new-array v3, v11, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    iget-object v4, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v4}, Landroid/media/AudioRecord;->getAudioSource()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {p0, v0, v2, v3}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a$redex0(Lorg/webrtc/voiceengine/WebRtcAudioRecord;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1221814
    if-ne v1, v10, :cond_6

    invoke-static {}, LX/7fJ;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1221815
    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->b()Z

    .line 1221816
    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->c()Z

    .line 1221817
    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->d()Z

    .line 1221818
    :cond_6
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "AudioRecord audio format: %d channels: %d sample rate: %d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v3}, Landroid/media/AudioRecord;->getAudioFormat()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v3}, Landroid/media/AudioRecord;->getChannelCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v3}, Landroid/media/AudioRecord;->getSampleRate()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221819
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getSampleRate()I

    move-result v2

    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getChannelCount()I

    move-result v3

    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->c:Landroid/content/Context;

    invoke-static {v0}, LX/7fJ;->a(Landroid/content/Context;)Z

    move-result v4

    iget-wide v6, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->b:J

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->nativeCacheRecordAudioParameters(IIZIJ)V

    move v0, v9

    .line 1221820
    goto/16 :goto_1

    :cond_7
    move v0, v7

    .line 1221821
    goto/16 :goto_5

    :cond_8
    move v0, v7

    .line 1221822
    goto :goto_6

    :cond_9
    move v1, v10

    goto/16 :goto_3

    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method public isBuiltInAECEnabled()Z
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1221823
    invoke-static {}, LX/7fJ;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1221824
    const/4 v0, 0x0

    .line 1221825
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e()Z

    move-result v0

    goto :goto_0
.end method

.method public isBuiltInAGCEnabled()Z
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1221826
    invoke-static {}, LX/7fJ;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1221827
    const/4 v0, 0x0

    .line 1221828
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->f()Z

    move-result v0

    goto :goto_0
.end method

.method public isBuiltInNSEnabled()Z
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1221829
    invoke-static {}, LX/7fJ;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1221830
    const/4 v0, 0x0

    .line 1221831
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->g()Z

    move-result v0

    goto :goto_0
.end method

.method public setNativeTraceLevel(I)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1221832
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    .line 1221833
    iput p1, v0, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a:I

    .line 1221834
    return-void
.end method

.method public startRecording()Z
    .locals 5
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1221835
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v3, "startRecording"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221836
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    if-nez v0, :cond_0

    .line 1221837
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "AudioRecord is null"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221838
    :goto_0
    return v2

    .line 1221839
    :cond_0
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1221840
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "AudioRecord is not in recording state after startRecording"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v3, v4}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a$redex0(Lorg/webrtc/voiceengine/WebRtcAudioRecord;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1221841
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->f:Lorg/webrtc/voiceengine/WebRtcAudioRecord$AudioRecordThread;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    const-string v3, "Recording thread was not destroyed properly"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v3, v2}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a$redex0(Lorg/webrtc/voiceengine/WebRtcAudioRecord;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1221842
    new-instance v0, Lorg/webrtc/voiceengine/WebRtcAudioRecord$AudioRecordThread;

    const-string v2, "AudioRecordJavaThread"

    invoke-direct {v0, p0, v2}, Lorg/webrtc/voiceengine/WebRtcAudioRecord$AudioRecordThread;-><init>(Lorg/webrtc/voiceengine/WebRtcAudioRecord;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->f:Lorg/webrtc/voiceengine/WebRtcAudioRecord$AudioRecordThread;

    .line 1221843
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->f:Lorg/webrtc/voiceengine/WebRtcAudioRecord$AudioRecordThread;

    invoke-virtual {v0}, Lorg/webrtc/voiceengine/WebRtcAudioRecord$AudioRecordThread;->start()V

    move v2, v1

    .line 1221844
    goto :goto_0

    .line 1221845
    :catch_0
    move-exception v0

    .line 1221846
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v3, "AudioRecord.startRecording() illegal state"

    invoke-virtual {v1, v3, v0}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1221847
    :catch_1
    move-exception v0

    .line 1221848
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v3, "AudioRecord.startRecording() unkonwn excption"

    invoke-virtual {v1, v3, v0}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1221849
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1221850
    goto :goto_2
.end method

.method public stopRecording()Z
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1221851
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "stopRecording"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221852
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->f:Lorg/webrtc/voiceengine/WebRtcAudioRecord$AudioRecordThread;

    if-eqz v0, :cond_1

    .line 1221853
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->f:Lorg/webrtc/voiceengine/WebRtcAudioRecord$AudioRecordThread;

    .line 1221854
    const/4 v1, 0x0

    iput-boolean v1, v0, Lorg/webrtc/voiceengine/WebRtcAudioRecord$AudioRecordThread;->b:Z

    .line 1221855
    :goto_0
    invoke-virtual {v0}, Lorg/webrtc/voiceengine/WebRtcAudioRecord$AudioRecordThread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1221856
    :try_start_0
    invoke-virtual {v0}, Lorg/webrtc/voiceengine/WebRtcAudioRecord$AudioRecordThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1221857
    :catch_0
    goto :goto_0

    .line 1221858
    :cond_0
    iput-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->f:Lorg/webrtc/voiceengine/WebRtcAudioRecord$AudioRecordThread;

    .line 1221859
    :cond_1
    invoke-static {}, LX/7fJ;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1221860
    invoke-direct {p0}, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->a()V

    .line 1221861
    :cond_2
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    if-eqz v0, :cond_3

    .line 1221862
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 1221863
    iput-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioRecord;->e:Landroid/media/AudioRecord;

    .line 1221864
    :cond_3
    const/4 v0, 0x1

    return v0
.end method
