.class public Lorg/webrtc/voiceengine/WebRtcAudioTrack;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7fI;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public final a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

.field private final b:Landroid/content/Context;

.field public final c:J

.field public d:Ljava/nio/ByteBuffer;

.field public e:Landroid/media/AudioTrack;

.field private f:Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1222065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1222066
    new-instance v0, Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    invoke-direct {v0, p0}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;-><init>(LX/7fI;)V

    iput-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    .line 1222067
    iput-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->d:Ljava/nio/ByteBuffer;

    .line 1222068
    iput-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    .line 1222069
    iput-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->f:Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;

    .line 1222070
    iput-object p1, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->b:Landroid/content/Context;

    .line 1222071
    iput-wide p2, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->c:J

    .line 1222072
    return-void
.end method

.method public static synthetic a(Lorg/webrtc/voiceengine/WebRtcAudioTrack;IJ)V
    .locals 0

    .prologue
    .line 1222057
    invoke-direct {p0, p1, p2, p3}, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->nativeGetPlayoutData(IJ)V

    return-void
.end method

.method public static varargs a$redex0(Lorg/webrtc/voiceengine/WebRtcAudioTrack;ZLjava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1222058
    if-nez p1, :cond_1

    .line 1222059
    invoke-static {p2, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1222060
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 1222061
    if-eqz v1, :cond_0

    .line 1222062
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 1222063
    :cond_0
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1222064
    :cond_1
    return-void
.end method

.method private native nativeCacheDirectPlayoutBufferAddress(Ljava/nio/ByteBuffer;J)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private native nativeCachePlayoutAudioParameters(IIZIJ)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private native nativeEngineTrace(ILjava/lang/String;)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private native nativeGetPlayoutData(IJ)V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 1222055
    invoke-direct {p0, p1, p2}, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->nativeEngineTrace(ILjava/lang/String;)V

    .line 1222056
    return-void
.end method

.method public initPlayout(II)Z
    .locals 11
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v4, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1222022
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "initPlayout(sampleRate=%d, channels=%d)"

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1222023
    mul-int/lit8 v0, p2, 0x2

    .line 1222024
    div-int/lit8 v1, p1, 0x64

    mul-int/2addr v0, v1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->d:Ljava/nio/ByteBuffer;

    .line 1222025
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "mByteBuffer.capacity: %d"

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1222026
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->d:Ljava/nio/ByteBuffer;

    iget-wide v2, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->c:J

    invoke-direct {p0, v0, v2, v3}, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->nativeCacheDirectPlayoutBufferAddress(Ljava/nio/ByteBuffer;J)V

    .line 1222027
    invoke-static {p1, v4, v9}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v0

    .line 1222028
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "AudioTrack.getMinBufferSize: %d"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1222029
    mul-int/lit8 v5, v0, 0x2

    .line 1222030
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    .line 1222031
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "initPlayout() called twice without stopPlayout()"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1222032
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V

    .line 1222033
    iput-object v10, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    .line 1222034
    :cond_0
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    if-ge v0, v5, :cond_1

    move v0, v8

    :goto_0
    const-string v1, "Buffer size too small %d >= %d"

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {p0, v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a$redex0(Lorg/webrtc/voiceengine/WebRtcAudioTrack;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1222035
    :try_start_0
    new-instance v0, Landroid/media/AudioTrack;

    const/4 v1, 0x0

    const/4 v3, 0x4

    const/4 v4, 0x2

    const/4 v6, 0x1

    move v2, p1

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    iput-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1222036
    :goto_1
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    if-nez v0, :cond_2

    .line 1222037
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "Failed to create a new AudioTrack instance"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1222038
    :goto_2
    return v7

    :cond_1
    move v0, v7

    .line 1222039
    goto :goto_0

    .line 1222040
    :catch_0
    move-exception v0

    .line 1222041
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "new AudioTrack illegal argument"

    invoke-virtual {v1, v2, v0}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1222042
    :catch_1
    move-exception v0

    .line 1222043
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v2, "new AudioTrack Unknown excption"

    invoke-virtual {v1, v2, v0}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1222044
    :cond_2
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getState()I

    move-result v0

    if-eq v0, v8, :cond_3

    .line 1222045
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "AudioTrack is not initialized (%d)"

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v3}, Landroid/media/AudioTrack;->getState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1222046
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V

    .line 1222047
    iput-object v10, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    goto :goto_2

    .line 1222048
    :cond_3
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    if-ne v0, v8, :cond_4

    move v0, v8

    :goto_3
    const-string v1, "Incorrect play state %d"

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v3}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {p0, v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a$redex0(Lorg/webrtc/voiceengine/WebRtcAudioTrack;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1222049
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getStreamType()I

    move-result v0

    if-nez v0, :cond_5

    move v0, v8

    :goto_4
    const-string v1, "Incorrect stream typei %d"

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v3}, Landroid/media/AudioTrack;->getStreamType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {p0, v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a$redex0(Lorg/webrtc/voiceengine/WebRtcAudioTrack;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1222050
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "AudioTrack audio format: %d channels: %d sample rate: %d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v3}, Landroid/media/AudioTrack;->getAudioFormat()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v3}, Landroid/media/AudioTrack;->getChannelCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    iget-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v3}, Landroid/media/AudioTrack;->getSampleRate()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1222051
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getSampleRate()I

    move-result v2

    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getChannelCount()I

    move-result v3

    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->b:Landroid/content/Context;

    invoke-static {v0}, LX/7fJ;->a(Landroid/content/Context;)Z

    move-result v4

    iget-wide v6, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->c:J

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->nativeCachePlayoutAudioParameters(IIZIJ)V

    move v7, v8

    .line 1222052
    goto/16 :goto_2

    :cond_4
    move v0, v7

    .line 1222053
    goto :goto_3

    :cond_5
    move v0, v7

    .line 1222054
    goto :goto_4
.end method

.method public setNativeTraceLevel(I)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1222019
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    .line 1222020
    iput p1, v0, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a:I

    .line 1222021
    return-void
.end method

.method public startPlayout()Z
    .locals 5
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1222003
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v3, "startPlayout"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1222004
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    if-nez v0, :cond_0

    .line 1222005
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "AudioTrack is null"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1222006
    :goto_0
    return v2

    .line 1222007
    :cond_0
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1222008
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "AudioTrack is not in play state after play()"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v3, v4}, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a$redex0(Lorg/webrtc/voiceengine/WebRtcAudioTrack;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1222009
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->f:Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    const-string v3, "Playout thread was not destroyed properly"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v3, v2}, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a$redex0(Lorg/webrtc/voiceengine/WebRtcAudioTrack;ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1222010
    new-instance v0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;

    const-string v2, "AudioTrackJavaThread"

    invoke-direct {v0, p0, v2}, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;-><init>(Lorg/webrtc/voiceengine/WebRtcAudioTrack;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->f:Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;

    .line 1222011
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->f:Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;

    invoke-virtual {v0}, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->start()V

    move v2, v1

    .line 1222012
    goto :goto_0

    .line 1222013
    :catch_0
    move-exception v0

    .line 1222014
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v3, "AudioTrack.play illegal state"

    invoke-virtual {v1, v3, v0}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1222015
    :catch_1
    move-exception v0

    .line 1222016
    iget-object v1, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v3, "AudioTrack.play unknown exception"

    invoke-virtual {v1, v3, v0}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1222017
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1222018
    goto :goto_2
.end method

.method public stopPlayout()Z
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1221991
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->a:Lorg/webrtc/voiceengine/WebRtcEngineLogger;

    const-string v1, "stopPlayout"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lorg/webrtc/voiceengine/WebRtcEngineLogger;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221992
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->f:Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;

    if-eqz v0, :cond_1

    .line 1221993
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->f:Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;

    .line 1221994
    const/4 v1, 0x0

    iput-boolean v1, v0, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->b:Z

    .line 1221995
    :goto_0
    invoke-virtual {v0}, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1221996
    :try_start_0
    invoke-virtual {v0}, Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1221997
    :catch_0
    goto :goto_0

    .line 1221998
    :cond_0
    iput-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->f:Lorg/webrtc/voiceengine/WebRtcAudioTrack$AudioTrackThread;

    .line 1221999
    :cond_1
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    if-eqz v0, :cond_2

    .line 1222000
    iget-object v0, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V

    .line 1222001
    iput-object v3, p0, Lorg/webrtc/voiceengine/WebRtcAudioTrack;->e:Landroid/media/AudioTrack;

    .line 1222002
    :cond_2
    const/4 v0, 0x1

    return v0
.end method
