.class public Lorg/webrtc/voiceengine/MediaCodecAAC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Z

.field public c:Z

.field private d:I

.field private e:Landroid/media/MediaCodec;

.field private f:[Ljava/nio/ByteBuffer;

.field private g:[Ljava/nio/ByteBuffer;

.field public h:LX/7fG;


# direct methods
.method public constructor <init>(IZ)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1221570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1221571
    iput-boolean v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->b:Z

    .line 1221572
    iput-boolean v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->c:Z

    .line 1221573
    iput v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->d:I

    .line 1221574
    iput-object v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    .line 1221575
    iput-object v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->f:[Ljava/nio/ByteBuffer;

    .line 1221576
    iput-object v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->g:[Ljava/nio/ByteBuffer;

    .line 1221577
    if-eqz p2, :cond_0

    const-string v0, "MediaCodecAAC(Encoder)"

    :goto_0
    iput-object v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    .line 1221578
    iput-boolean p2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->c:Z

    .line 1221579
    iput-boolean v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->b:Z

    .line 1221580
    invoke-static {}, LX/7fG;->values()[LX/7fG;

    move-result-object v0

    aget-object v0, v0, p1

    iput-object v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->h:LX/7fG;

    .line 1221581
    return-void

    .line 1221582
    :cond_0
    const-string v0, "MediaCodecAAC(Decoder)"

    goto :goto_0
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1221592
    iget-boolean v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->c:Z

    if-eqz v0, :cond_0

    .line 1221593
    const-string v0, "audio/mp4a-latm"

    invoke-static {v0}, Lorg/webrtc/voiceengine/MediaCodecAACHelper;->findEncoder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1221594
    :goto_0
    if-nez v0, :cond_1

    .line 1221595
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "[AAC]Can not find AAC codec"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1221596
    :cond_0
    const-string v0, "audio/mp4a-latm"

    invoke-static {v0}, Lorg/webrtc/voiceengine/MediaCodecAACHelper;->findDecoder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1221597
    :cond_1
    iget-boolean v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->c:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    iget-object v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->h:LX/7fG;

    invoke-static {v1}, Lorg/webrtc/voiceengine/MediaCodecAACHelper;->a(LX/7fG;)I

    .line 1221598
    return-object v0
.end method

.method private a(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Z
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1221583
    :try_start_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1221584
    invoke-virtual {p2, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 1221585
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1221586
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;
    :try_end_0
    .catch Ljava/nio/BufferOverflowException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1221587
    const/4 v0, 0x1

    .line 1221588
    :goto_0
    return v0

    .line 1221589
    :catch_0
    move-exception v0

    .line 1221590
    iget-object v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v2, "[AAC]Buffer overflow"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1221591
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1221460
    :try_start_0
    invoke-static {p0}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1221461
    :goto_0
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isAACSupported(Z)Z
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1221565
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_1

    .line 1221566
    :cond_0
    :goto_0
    return v0

    .line 1221567
    :cond_1
    if-eqz p0, :cond_2

    .line 1221568
    const-string v2, "audio/mp4a-latm"

    invoke-static {v2}, Lorg/webrtc/voiceengine/MediaCodecAACHelper;->findEncoder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1221569
    :cond_2
    const-string v2, "audio/mp4a-latm"

    invoke-static {v2}, Lorg/webrtc/voiceengine/MediaCodecAACHelper;->findDecoder(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public dequeueInputBuffer()I
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1221560
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v2, v3}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1221561
    :goto_0
    return v0

    .line 1221562
    :catch_0
    move-exception v0

    .line 1221563
    iget-object v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v2, "[AAC]dequeueIntputBuffer failed %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221564
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;)I
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1221547
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, p1, v2, v3}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v0

    .line 1221548
    const/4 v1, -0x3

    if-ne v0, v1, :cond_2

    .line 1221549
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 1221550
    iget-object v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->f:[Ljava/nio/ByteBuffer;

    .line 1221551
    :cond_0
    invoke-virtual {p0, p1}, Lorg/webrtc/voiceengine/MediaCodecAAC;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;)I

    move-result v0

    .line 1221552
    :cond_1
    :goto_0
    return v0

    .line 1221553
    :cond_2
    const/4 v1, -0x2

    if-ne v0, v1, :cond_3

    .line 1221554
    invoke-virtual {p0, p1}, Lorg/webrtc/voiceengine/MediaCodecAAC;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;)I

    move-result v0

    goto :goto_0

    .line 1221555
    :cond_3
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-gez v0, :cond_1

    .line 1221556
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[AAC]dequeueOutputBuffer: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1221557
    :catch_0
    move-exception v0

    .line 1221558
    iget-object v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v2, "[AAC]dequeueOutputBuffer failed %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221559
    const/4 v0, -0x4

    goto :goto_0
.end method

.method public getInputBuffer(I)Ljava/nio/ByteBuffer;
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1221599
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 1221600
    :try_start_0
    iget-object v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    invoke-virtual {v1, p1}, Landroid/media/MediaCodec;->getInputBuffer(I)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1221601
    :goto_0
    return-object v0

    .line 1221602
    :catch_0
    move-exception v1

    .line 1221603
    iget-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v3, "[AAC]getInputBuffer failed %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1221604
    :cond_0
    iget-object v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->g:[Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_1

    .line 1221605
    iget-object v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->g:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, p1

    goto :goto_0

    .line 1221606
    :cond_1
    iget-object v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v2, "[AAC]Invalid input buffers"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getOutputBuffer(I)Ljava/nio/ByteBuffer;
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1221539
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 1221540
    :try_start_0
    iget-object v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    invoke-virtual {v1, p1}, Landroid/media/MediaCodec;->getOutputBuffer(I)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1221541
    :goto_0
    return-object v0

    .line 1221542
    :catch_0
    move-exception v1

    .line 1221543
    iget-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v3, "[AAC]getOutputBuffer failed %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1221544
    :cond_0
    iget-object v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->f:[Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_1

    .line 1221545
    iget-object v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->f:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, p1

    goto :goto_0

    .line 1221546
    :cond_1
    iget-object v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v2, "[AAC]Invalid output buffers"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public initialize(I)Z
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1221511
    iget-boolean v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->b:Z

    if-eqz v2, :cond_0

    .line 1221512
    :goto_0
    return v0

    .line 1221513
    :cond_0
    iput p1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->d:I

    .line 1221514
    :try_start_0
    invoke-direct {p0}, Lorg/webrtc/voiceengine/MediaCodecAAC;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/webrtc/voiceengine/MediaCodecAAC;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v2

    iput-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    .line 1221515
    iget-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    if-nez v2, :cond_1

    .line 1221516
    iget-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v3, "[AAC]Can not create media codec"

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1221517
    goto :goto_0

    .line 1221518
    :cond_1
    const/4 v5, 0x1

    .line 1221519
    const-string v2, "audio/mp4a-latm"

    invoke-static {v2, p1, v5}, Landroid/media/MediaFormat;->createAudioFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v2

    .line 1221520
    const-string v3, "aac-profile"

    iget-object v4, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->h:LX/7fG;

    invoke-static {v4}, Lorg/webrtc/voiceengine/MediaCodecAACHelper;->a(LX/7fG;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1221521
    iget-boolean v3, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->c:Z

    if-eqz v3, :cond_4

    .line 1221522
    const-string v3, "bitrate"

    const v4, 0x17700

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1221523
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[AAC]Codec Format: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1221524
    move-object v3, v2

    .line 1221525
    iget-object v4, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-boolean v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->c:Z

    if-eqz v2, :cond_3

    move v2, v0

    :goto_2
    invoke-virtual {v4, v3, v5, v6, v2}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 1221526
    iget-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->start()V

    .line 1221527
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_2

    .line 1221528
    iget-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->g:[Ljava/nio/ByteBuffer;

    .line 1221529
    iget-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->f:[Ljava/nio/ByteBuffer;

    .line 1221530
    iget-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[AAC]Input buffers: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->g:[Ljava/nio/ByteBuffer;

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1221531
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[AAC]Output buffers: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->f:[Ljava/nio/ByteBuffer;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1221532
    :cond_2
    iput-boolean v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->b:Z

    goto/16 :goto_0

    :cond_3
    move v2, v1

    .line 1221533
    goto :goto_2

    .line 1221534
    :catch_0
    move-exception v2

    .line 1221535
    iget-object v3, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v4, "[AAC]initialize failed %s"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 1221536
    goto/16 :goto_0

    .line 1221537
    :cond_4
    iget-object v3, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->h:LX/7fG;

    invoke-static {v3}, Lorg/webrtc/voiceengine/MediaCodecAACHelper;->a(LX/7fG;)I

    move-result v3

    invoke-static {p1, v5, v3}, Lorg/webrtc/voiceengine/MediaCodecAACHelper;->a(III)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1221538
    const-string v4, "csd-0"

    invoke-virtual {v2, v4, v3}, Landroid/media/MediaFormat;->setByteBuffer(Ljava/lang/String;Ljava/nio/ByteBuffer;)V

    goto :goto_1
.end method

.method public process(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)I
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 1221479
    invoke-virtual {p0}, Lorg/webrtc/voiceengine/MediaCodecAAC;->dequeueInputBuffer()I

    move-result v2

    .line 1221480
    if-gez v2, :cond_0

    .line 1221481
    iget-object v3, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v4, "[AAC]Fail to dequeue input buffer %d"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v3, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221482
    :goto_0
    return v0

    .line 1221483
    :cond_0
    invoke-virtual {p0, v2}, Lorg/webrtc/voiceengine/MediaCodecAAC;->getInputBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1221484
    if-nez v3, :cond_1

    .line 1221485
    iget-object v3, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v4, "[AAC]Failed to get input buffer %d"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v3, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1221486
    :cond_1
    invoke-direct {p0, p1, v3}, Lorg/webrtc/voiceengine/MediaCodecAAC;->a(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1221487
    iget-object v4, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v5, "[AAC]Failed to copy input buffer %d, %s => %s"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v7

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v8

    invoke-static {v4, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1221488
    :cond_2
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    const-wide/16 v4, 0x0

    invoke-virtual {p0, v2, v3, v4, v5}, Lorg/webrtc/voiceengine/MediaCodecAAC;->queueInputBuffer(IIJ)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1221489
    iget-object v3, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v4, "[AAC]Failed to queue input buffer %d"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v3, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1221490
    :cond_3
    new-instance v2, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v2}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 1221491
    invoke-virtual {p0, v2}, Lorg/webrtc/voiceengine/MediaCodecAAC;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;)I

    move-result v3

    .line 1221492
    if-ne v3, v0, :cond_4

    move v0, v1

    .line 1221493
    goto :goto_0

    .line 1221494
    :cond_4
    if-gez v3, :cond_5

    .line 1221495
    iget-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v4, "[AAC]Invalid output buffer index %d"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v1

    invoke-static {v2, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1221496
    :cond_5
    invoke-virtual {p0, v3}, Lorg/webrtc/voiceengine/MediaCodecAAC;->getOutputBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1221497
    if-nez v4, :cond_6

    .line 1221498
    iget-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v4, "[AAC]Failed to get output buffer %d"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v1

    invoke-static {v2, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1221499
    :cond_6
    iget v5, v2, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_9

    .line 1221500
    iget v3, v2, Landroid/media/MediaCodec$BufferInfo;->size:I

    if-eq v3, v8, :cond_7

    .line 1221501
    iget-object v3, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v4, "Incorrect codec config size %d"

    new-array v5, v7, [Ljava/lang/Object;

    iget v2, v2, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v3, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1221502
    :cond_7
    iget v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->d:I

    iget-object v3, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->h:LX/7fG;

    invoke-static {v3}, Lorg/webrtc/voiceengine/MediaCodecAACHelper;->a(LX/7fG;)I

    move-result v3

    invoke-static {v2, v7, v3}, Lorg/webrtc/voiceengine/MediaCodecAACHelper;->a(III)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1221503
    invoke-virtual {v4, v2}, Ljava/nio/ByteBuffer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 1221504
    iget-object v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v2, "Incorrect codec config"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    move v0, v1

    .line 1221505
    goto/16 :goto_0

    .line 1221506
    :cond_9
    invoke-direct {p0, v4, p2}, Lorg/webrtc/voiceengine/MediaCodecAAC;->a(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 1221507
    iget-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v5, "[AAC]Failed to copy output buffer %d, %s => %s"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v1

    aput-object v4, v6, v7

    aput-object p2, v6, v8

    invoke-static {v2, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1221508
    :cond_a
    invoke-virtual {p0, v3}, Lorg/webrtc/voiceengine/MediaCodecAAC;->releaseOutputBuffer(I)Z

    move-result v2

    if-nez v2, :cond_b

    .line 1221509
    iget-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v4, "[AAC]Failed to release output buffer %d"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v1

    invoke-static {v2, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1221510
    :cond_b
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    goto/16 :goto_0
.end method

.method public queueInputBuffer(IIJ)Z
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1221474
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    const/4 v2, 0x0

    const/4 v6, 0x0

    move v1, p1

    move v3, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v7

    .line 1221475
    :goto_0
    return v0

    .line 1221476
    :catch_0
    move-exception v0

    .line 1221477
    iget-object v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v2, "[AAC]queueInputBuffer failed %s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v8

    .line 1221478
    goto :goto_0
.end method

.method public release()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1221467
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 1221468
    iget-object v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1221469
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    .line 1221470
    iput-boolean v4, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->b:Z

    .line 1221471
    return-void

    .line 1221472
    :catch_0
    move-exception v0

    .line 1221473
    iget-object v1, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v2, "[AAC]Media codec release failed %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public releaseOutputBuffer(I)Z
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1221462
    :try_start_0
    iget-object v2, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->e:Landroid/media/MediaCodec;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1221463
    :goto_0
    return v0

    .line 1221464
    :catch_0
    move-exception v2

    .line 1221465
    iget-object v3, p0, Lorg/webrtc/voiceengine/MediaCodecAAC;->a:Ljava/lang/String;

    const-string v4, "[AAC]releaseOutputBuffer failed %s"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 1221466
    goto :goto_0
.end method
