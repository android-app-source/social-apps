.class public final Lorg/webrtc/videoengine/MediaCodecVideoEncoder$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/concurrent/CountDownLatch;

.field public final synthetic b:Lorg/webrtc/videoengine/MediaCodecVideoEncoder;


# direct methods
.method public constructor <init>(Lorg/webrtc/videoengine/MediaCodecVideoEncoder;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 1220571
    iput-object p1, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$1;->b:Lorg/webrtc/videoengine/MediaCodecVideoEncoder;

    iput-object p2, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$1;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1220572
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$1;->b:Lorg/webrtc/videoengine/MediaCodecVideoEncoder;

    iget-object v0, v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 1220573
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$1;->b:Lorg/webrtc/videoengine/MediaCodecVideoEncoder;

    iget-object v0, v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1220574
    :goto_0
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$1;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 1220575
    return-void

    .line 1220576
    :catch_0
    move-exception v0

    .line 1220577
    const-string v1, "MediaCodecVideoEncoder"

    const-string v2, "Media encoder release failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
