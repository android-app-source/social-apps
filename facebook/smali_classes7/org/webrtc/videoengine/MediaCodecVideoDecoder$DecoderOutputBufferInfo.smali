.class public final Lorg/webrtc/videoengine/MediaCodecVideoDecoder$DecoderOutputBufferInfo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private final mFirstPacketTimeMs:J
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private final mIndex:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private final mOffset:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private final mSize:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private final mTimestampMs:J
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>(IIIJJ)V
    .locals 0

    .prologue
    .line 1220401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1220402
    iput p1, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder$DecoderOutputBufferInfo;->mIndex:I

    .line 1220403
    iput p2, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder$DecoderOutputBufferInfo;->mOffset:I

    .line 1220404
    iput p3, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder$DecoderOutputBufferInfo;->mSize:I

    .line 1220405
    iput-wide p4, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder$DecoderOutputBufferInfo;->mTimestampMs:J

    .line 1220406
    iput-wide p6, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder$DecoderOutputBufferInfo;->mFirstPacketTimeMs:J

    .line 1220407
    return-void
.end method
