.class public final Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public final bitrateAdjustment:Z
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final codecName:Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final colorFormat:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;IZ)V
    .locals 0

    .prologue
    .line 1220578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1220579
    iput-object p1, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;->codecName:Ljava/lang/String;

    .line 1220580
    iput p2, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;->colorFormat:I

    .line 1220581
    iput-boolean p3, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;->bitrateAdjustment:Z

    .line 1220582
    return-void
.end method
