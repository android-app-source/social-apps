.class public Lorg/webrtc/videoengine/MediaCodecVideoDecoder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static c:I

.field private static final f:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;

.field private static final h:[Ljava/lang/String;

.field private static final i:[Ljava/lang/String;

.field private static final j:[Ljava/lang/String;

.field private static final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Z

.field public b:Landroid/media/MediaCodec;

.field private d:[Ljava/nio/ByteBuffer;

.field private e:[Ljava/nio/ByteBuffer;

.field private l:Z

.field private m:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/7f7;",
            ">;"
        }
    .end annotation
.end field

.field private mColorFormat:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private mHeight:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private mInputBuffer:Ljava/nio/ByteBuffer;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private mOutputBuffer:Ljava/nio/ByteBuffer;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private mSliceHeight:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private mStride:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private mWidth:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private n:LX/7f6;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1220518
    sput v2, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->c:I

    .line 1220519
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "OMX.qcom."

    aput-object v1, v0, v2

    const-string v1, "OMX.Nvidia."

    aput-object v1, v0, v3

    const-string v1, "OMX.Exynos."

    aput-object v1, v0, v4

    const-string v1, "OMX.Intel."

    aput-object v1, v0, v5

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->f:[Ljava/lang/String;

    .line 1220520
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "OMX.google."

    aput-object v1, v0, v2

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->g:[Ljava/lang/String;

    .line 1220521
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "OMX.qcom."

    aput-object v1, v0, v2

    const-string v1, "OMX.Exynos."

    aput-object v1, v0, v3

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->h:[Ljava/lang/String;

    .line 1220522
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "OMX.qcom."

    aput-object v1, v0, v2

    const-string v1, "OMX.Exynos."

    aput-object v1, v0, v3

    const-string v1, "OMX.Intel."

    aput-object v1, v0, v4

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->i:[Ljava/lang/String;

    .line 1220523
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "OMX.google."

    aput-object v1, v0, v2

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->j:[Ljava/lang/String;

    .line 1220524
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Integer;

    const/16 v1, 0x13

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const/16 v1, 0x15

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const v1, 0x7fa30c00

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const v1, 0x7fa30c01

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    const v1, 0x7fa30c02

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const v2, 0x7fa30c03

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const v2, 0x7fa30c04

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->k:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1220526
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->m:Ljava/util/Queue;

    .line 1220527
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->a:Z

    .line 1220528
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->n:LX/7f6;

    .line 1220529
    return-void
.end method

.method private static a(Ljava/lang/String;[Ljava/lang/String;)LX/7f6;
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1220495
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    move-object v0, v3

    .line 1220496
    :goto_0
    return-object v0

    :cond_0
    move v1, v2

    .line 1220497
    :goto_1
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 1220498
    invoke-static {v1}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v5

    .line 1220499
    invoke-virtual {v5}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1220500
    invoke-virtual {v5}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object v4

    array-length v6, v4

    move v0, v2

    :goto_2
    if-ge v0, v6, :cond_8

    aget-object v7, v4, v0

    .line 1220501
    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1220502
    invoke-virtual {v5}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1220503
    :goto_3
    if-eqz v4, :cond_5

    .line 1220504
    array-length v6, p1

    move v0, v2

    :goto_4
    if-ge v0, v6, :cond_7

    aget-object v7, p1, v0

    .line 1220505
    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1220506
    const/4 v0, 0x1

    .line 1220507
    :goto_5
    if-eqz v0, :cond_5

    .line 1220508
    invoke-virtual {v5, p0}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v5

    .line 1220509
    sget-object v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 1220510
    iget-object v8, v5, Landroid/media/MediaCodecInfo$CodecCapabilities;->colorFormats:[I

    array-length v9, v8

    move v0, v2

    :goto_6
    if-ge v0, v9, :cond_1

    aget v10, v8, v0

    .line 1220511
    if-ne v10, v7, :cond_4

    .line 1220512
    new-instance v0, LX/7f6;

    invoke-direct {v0, v4, v10}, LX/7f6;-><init>(Ljava/lang/String;I)V

    goto :goto_0

    .line 1220513
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1220514
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1220515
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1220516
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_6
    move-object v0, v3

    .line 1220517
    goto :goto_0

    :cond_7
    move v0, v2

    goto :goto_5

    :cond_8
    move-object v4, v3

    goto :goto_3
.end method

.method private dequeueInputBuffer()I
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220483
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    if-nez v0, :cond_1

    .line 1220484
    const/4 v0, -0x3

    .line 1220485
    :cond_0
    :goto_0
    return v0

    .line 1220486
    :cond_1
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    const-wide/32 v2, 0x7a120

    invoke-virtual {v0, v2, v3}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v0

    .line 1220487
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mInputBuffer:Ljava/nio/ByteBuffer;

    .line 1220488
    if-ltz v0, :cond_0

    .line 1220489
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_2

    .line 1220490
    iget-object v1, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    invoke-virtual {v1, v0}, Landroid/media/MediaCodec;->getInputBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mInputBuffer:Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1220491
    :catch_0
    move-exception v0

    .line 1220492
    const-string v1, "MediaCodecVideoDecoder"

    const-string v2, "dequeueIntputBuffer failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1220493
    const/4 v0, -0x2

    goto :goto_0

    .line 1220494
    :cond_2
    :try_start_1
    iget-object v1, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->d:[Ljava/nio/ByteBuffer;

    aget-object v1, v1, v0

    iput-object v1, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mInputBuffer:Ljava/nio/ByteBuffer;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private dequeueOutputBuffer(I)Lorg/webrtc/videoengine/MediaCodecVideoDecoder$DecoderOutputBufferInfo;
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/16 v8, 0x15

    const/4 v7, -0x2

    const/4 v6, -0x3

    .line 1220444
    iput-object v2, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mOutputBuffer:Ljava/nio/ByteBuffer;

    .line 1220445
    new-instance v3, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v3}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 1220446
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    int-to-long v4, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v1

    .line 1220447
    :goto_0
    if-eq v1, v6, :cond_0

    if-ne v1, v7, :cond_a

    .line 1220448
    :cond_0
    if-ne v1, v6, :cond_2

    .line 1220449
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v8, :cond_1

    .line 1220450
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->e:[Ljava/nio/ByteBuffer;

    .line 1220451
    :cond_1
    :goto_1
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    int-to-long v4, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v1

    goto :goto_0

    .line 1220452
    :cond_2
    if-ne v1, v7, :cond_1

    .line 1220453
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v4

    .line 1220454
    const-string v0, "width"

    invoke-virtual {v4, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    .line 1220455
    const-string v1, "height"

    invoke-virtual {v4, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    .line 1220456
    const-string v5, "crop-left"

    invoke-virtual {v4, v5}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "crop-right"

    invoke-virtual {v4, v5}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1220457
    const-string v0, "crop-right"

    invoke-virtual {v4, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const-string v5, "crop-left"

    invoke-virtual {v4, v5}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v5

    sub-int/2addr v0, v5

    .line 1220458
    :cond_3
    const-string v5, "crop-top"

    invoke-virtual {v4, v5}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "crop-bottom"

    invoke-virtual {v4, v5}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1220459
    const-string v1, "crop-bottom"

    invoke-virtual {v4, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const-string v5, "crop-top"

    invoke-virtual {v4, v5}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v5

    sub-int/2addr v1, v5

    .line 1220460
    :cond_4
    iget-boolean v5, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->l:Z

    if-eqz v5, :cond_6

    iget v5, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mWidth:I

    if-ne v0, v5, :cond_5

    iget v5, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mHeight:I

    if-eq v1, v5, :cond_6

    .line 1220461
    :cond_5
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected size change. Configured "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "*"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". New "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "*"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1220462
    :cond_6
    iput v1, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mHeight:I

    .line 1220463
    iput v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mWidth:I

    .line 1220464
    const-string v0, "color-format"

    invoke-virtual {v4, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1220465
    const-string v0, "color-format"

    invoke-virtual {v4, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mColorFormat:I

    .line 1220466
    sget-object v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->k:Ljava/util/List;

    iget v1, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mColorFormat:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1220467
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Non supported color format"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mColorFormat:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1220468
    :cond_7
    const-string v0, "stride"

    invoke-virtual {v4, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1220469
    const-string v0, "stride"

    invoke-virtual {v4, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mStride:I

    .line 1220470
    :cond_8
    const-string v0, "slice-height"

    invoke-virtual {v4, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1220471
    const-string v0, "slice-height"

    invoke-virtual {v4, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mSliceHeight:I

    .line 1220472
    :cond_9
    iget v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mWidth:I

    iget v1, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mStride:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mStride:I

    .line 1220473
    iget v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mHeight:I

    iget v1, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mSliceHeight:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mSliceHeight:I

    goto/16 :goto_1

    .line 1220474
    :cond_a
    if-ltz v1, :cond_c

    .line 1220475
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->l:Z

    .line 1220476
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_b

    .line 1220477
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0, v1}, Landroid/media/MediaCodec;->getOutputBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mOutputBuffer:Ljava/nio/ByteBuffer;

    .line 1220478
    :goto_2
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/7f7;

    .line 1220479
    new-instance v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder$DecoderOutputBufferInfo;

    iget v2, v3, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v3, v3, Landroid/media/MediaCodec$BufferInfo;->size:I

    iget-wide v4, v6, LX/7f7;->a:J

    iget-wide v6, v6, LX/7f7;->b:J

    invoke-direct/range {v0 .. v7}, Lorg/webrtc/videoengine/MediaCodecVideoDecoder$DecoderOutputBufferInfo;-><init>(IIIJJ)V

    .line 1220480
    :goto_3
    return-object v0

    .line 1220481
    :cond_b
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->e:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, v1

    iput-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mOutputBuffer:Ljava/nio/ByteBuffer;

    goto :goto_2

    :cond_c
    move-object v0, v2

    .line 1220482
    goto :goto_3
.end method

.method private initDecode(Ljava/lang/String;IIZ)Z
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1220530
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Java initDecode: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " x "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1220531
    sget v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->c:I

    const/4 v3, 0x3

    if-le v0, v3, :cond_0

    .line 1220532
    const-string v0, "MediaCodecVideoDecoder"

    const-string v3, "MediaCodec has irrecoverably failed too many times: %d"

    new-array v2, v2, [Ljava/lang/Object;

    sget v4, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-static {v0, v3, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 1220533
    :goto_0
    return v0

    .line 1220534
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v0, v3, :cond_1

    move v0, v1

    .line 1220535
    goto :goto_0

    .line 1220536
    :cond_1
    iget-boolean v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->a:Z

    if-eqz v0, :cond_2

    .line 1220537
    const-string v0, "MediaCodecVideoDecoder"

    const-string v2, "Already inited, forgot to release?"

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1220538
    goto :goto_0

    .line 1220539
    :cond_2
    const-string v0, "video/x-vnd.on2.vp8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1220540
    if-eqz p4, :cond_3

    sget-object v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->g:[Ljava/lang/String;

    .line 1220541
    :goto_1
    invoke-static {p1, v0}, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->a(Ljava/lang/String;[Ljava/lang/String;)LX/7f6;

    move-result-object v0

    iput-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->n:LX/7f6;

    .line 1220542
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->n:LX/7f6;

    if-nez v0, :cond_7

    .line 1220543
    const-string v0, "MediaCodecVideoDecoder"

    const-string v3, "Cannot find HW decoder for %s."

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-static {v0, v3, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 1220544
    goto :goto_0

    .line 1220545
    :cond_3
    sget-object v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->f:[Ljava/lang/String;

    goto :goto_1

    .line 1220546
    :cond_4
    const-string v0, "video/avc"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1220547
    if-eqz p4, :cond_5

    sget-object v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->j:[Ljava/lang/String;

    goto :goto_1

    :cond_5
    sget-object v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->i:[Ljava/lang/String;

    goto :goto_1

    .line 1220548
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Non supported codec "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1220549
    :cond_7
    :try_start_0
    iput p2, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mWidth:I

    .line 1220550
    iput p3, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mHeight:I

    .line 1220551
    iput p2, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mStride:I

    .line 1220552
    iput p3, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mSliceHeight:I

    .line 1220553
    invoke-static {p1, p2, p3}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v0

    .line 1220554
    const-string v3, "color-format"

    iget-object v4, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->n:LX/7f6;

    iget v4, v4, LX/7f6;->b:I

    invoke-virtual {v0, v3, v4}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1220555
    iget-object v3, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->n:LX/7f6;

    iget-object v3, v3, LX/7f6;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v3

    iput-object v3, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    .line 1220556
    iget-object v3, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    if-nez v3, :cond_8

    move v0, v1

    .line 1220557
    goto/16 :goto_0

    .line 1220558
    :cond_8
    iget-object v3, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v0, v4, v5, v6}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 1220559
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V

    .line 1220560
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->n:LX/7f6;

    iget v0, v0, LX/7f6;->b:I

    iput v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mColorFormat:I

    .line 1220561
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->e:[Ljava/nio/ByteBuffer;

    .line 1220562
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->d:[Ljava/nio/ByteBuffer;

    .line 1220563
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1220564
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->l:Z

    .line 1220565
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->a:Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move v0, v2

    .line 1220566
    goto/16 :goto_0

    .line 1220567
    :catch_0
    move-exception v0

    .line 1220568
    :goto_2
    const-string v2, "MediaCodecVideoDecoder"

    const-string v3, "initDecode failed"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 1220569
    goto/16 :goto_0

    .line 1220570
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static isH264HwSupported()Z
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220443
    const-string v0, "video/avc"

    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->i:[Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->a(Ljava/lang/String;[Ljava/lang/String;)LX/7f6;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isVp8HwSupported()Z
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220442
    const-string v0, "video/x-vnd.on2.vp8"

    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->f:[Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->a(Ljava/lang/String;[Ljava/lang/String;)LX/7f6;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isVp9HwSupported()Z
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220441
    const-string v0, "video/x-vnd.on2.vp9"

    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->h:[Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->a(Ljava/lang/String;[Ljava/lang/String;)LX/7f6;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private queueInputBuffer(IIJJJ)Z
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220432
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mInputBuffer:Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1220433
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->mInputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 1220434
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->m:Ljava/util/Queue;

    new-instance v1, LX/7f7;

    invoke-direct {v1, p5, p6, p7, p8}, LX/7f7;-><init>(JJ)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1220435
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    const/4 v2, 0x0

    const/4 v6, 0x0

    move v1, p1

    move v3, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1220436
    const/4 v0, 0x1

    .line 1220437
    :goto_0
    return v0

    .line 1220438
    :catch_0
    move-exception v0

    .line 1220439
    const-string v1, "MediaCodecVideoDecoder"

    const-string v2, "decode failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1220440
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private release()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220416
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 1220417
    new-instance v1, Lorg/webrtc/videoengine/MediaCodecVideoDecoder$1;

    invoke-direct {v1, p0, v0}, Lorg/webrtc/videoengine/MediaCodecVideoDecoder$1;-><init>(Lorg/webrtc/videoengine/MediaCodecVideoDecoder;Ljava/util/concurrent/CountDownLatch;)V

    .line 1220418
    const v2, 0xb573529

    invoke-static {v1, v2}, LX/00l;->a(Ljava/lang/Runnable;I)Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1220419
    const-wide/16 v2, 0x1388

    invoke-static {v0, v2, v3}, LX/7ez;->a(Ljava/util/concurrent/CountDownLatch;J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1220420
    const-string v0, "MediaCodecVideoDecoder"

    const-string v1, "Media decoder release timeout"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220421
    sget v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->c:I

    .line 1220422
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    .line 1220423
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->a:Z

    .line 1220424
    return-void
.end method

.method private releaseOutputBuffer(I)Z
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1220425
    iget-object v1, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    if-nez v1, :cond_0

    .line 1220426
    const-string v1, "MediaCodecVideoDecoder"

    const-string v2, "mMediaCodec is null when release."

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220427
    :goto_0
    return v0

    .line 1220428
    :cond_0
    :try_start_0
    iget-object v1, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1220429
    const/4 v0, 0x1

    goto :goto_0

    .line 1220430
    :catch_0
    move-exception v1

    .line 1220431
    const-string v2, "MediaCodecVideoDecoder"

    const-string v3, "releaseOutputBuffer failed"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
