.class public final Lorg/webrtc/videoengine/MediaCodecVideoEncoder$OutputBufferInfo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public final buffer:Ljava/nio/ByteBuffer;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final index:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final isKeyFrame:Z
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final presentationTimestampUs:J
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjava/nio/ByteBuffer;ZJ)V
    .locals 0

    .prologue
    .line 1220588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1220589
    iput p1, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$OutputBufferInfo;->index:I

    .line 1220590
    iput-object p2, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$OutputBufferInfo;->buffer:Ljava/nio/ByteBuffer;

    .line 1220591
    iput-boolean p3, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$OutputBufferInfo;->isKeyFrame:Z

    .line 1220592
    iput-wide p4, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$OutputBufferInfo;->presentationTimestampUs:J

    .line 1220593
    return-void
.end method
