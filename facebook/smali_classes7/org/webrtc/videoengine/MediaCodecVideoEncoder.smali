.class public Lorg/webrtc/videoengine/MediaCodecVideoEncoder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static a:Lorg/webrtc/videoengine/MediaCodecVideoEncoder;

.field private static b:LX/7f9;

.field private static c:I

.field public static d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final m:LX/7f8;

.field private static final n:LX/7f8;

.field private static final o:LX/7f8;

.field public static final p:[LX/7f8;

.field private static final q:LX/7f8;

.field private static final r:LX/7f8;

.field public static final s:[LX/7f8;

.field private static final t:LX/7f8;

.field private static final u:LX/7f8;

.field public static final v:[LX/7f8;

.field private static final w:[Ljava/lang/String;

.field private static final x:[I

.field public static final y:[I


# instance fields
.field private A:Z

.field private B:Ljava/nio/ByteBuffer;

.field private colorFormat:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private e:Ljava/lang/Thread;

.field public f:Landroid/media/MediaCodec;

.field private g:[Ljava/nio/ByteBuffer;

.field private h:LX/7f1;

.field private i:I

.field private j:I

.field private k:Landroid/view/Surface;

.field private l:LX/7f3;

.field private z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/16 v6, 0x17

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1220757
    const/4 v0, 0x0

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->a:Lorg/webrtc/videoengine/MediaCodecVideoEncoder;

    .line 1220758
    const/4 v0, 0x0

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->b:LX/7f9;

    .line 1220759
    sput v3, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->c:I

    .line 1220760
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->d:Ljava/util/Set;

    .line 1220761
    new-instance v0, LX/7f8;

    const-string v1, "OMX.qcom."

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2, v3}, LX/7f8;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->m:LX/7f8;

    .line 1220762
    new-instance v0, LX/7f8;

    const-string v1, "OMX.Exynos."

    invoke-direct {v0, v1, v6, v3}, LX/7f8;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->n:LX/7f8;

    .line 1220763
    new-instance v0, LX/7f8;

    const-string v1, "OMX.Intel."

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2, v3}, LX/7f8;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->o:LX/7f8;

    .line 1220764
    new-array v0, v7, [LX/7f8;

    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->m:LX/7f8;

    aput-object v1, v0, v3

    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->n:LX/7f8;

    aput-object v1, v0, v4

    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->o:LX/7f8;

    aput-object v1, v0, v5

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->p:[LX/7f8;

    .line 1220765
    new-instance v0, LX/7f8;

    const-string v1, "OMX.qcom."

    invoke-direct {v0, v1, v6, v3}, LX/7f8;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->q:LX/7f8;

    .line 1220766
    new-instance v0, LX/7f8;

    const-string v1, "OMX.Exynos."

    invoke-direct {v0, v1, v6, v3}, LX/7f8;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->r:LX/7f8;

    .line 1220767
    new-array v0, v5, [LX/7f8;

    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->q:LX/7f8;

    aput-object v1, v0, v3

    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->r:LX/7f8;

    aput-object v1, v0, v4

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->s:[LX/7f8;

    .line 1220768
    new-instance v0, LX/7f8;

    const-string v1, "OMX.qcom."

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2, v3}, LX/7f8;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->t:LX/7f8;

    .line 1220769
    new-instance v0, LX/7f8;

    const-string v1, "OMX.Exynos."

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2, v4}, LX/7f8;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->u:LX/7f8;

    .line 1220770
    new-array v0, v5, [LX/7f8;

    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->t:LX/7f8;

    aput-object v1, v0, v3

    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->u:LX/7f8;

    aput-object v1, v0, v4

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->v:[LX/7f8;

    .line 1220771
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "SAMSUNG-SGH-I337"

    aput-object v1, v0, v3

    const-string v1, "Nexus 7"

    aput-object v1, v0, v4

    const-string v1, "Nexus 4"

    aput-object v1, v0, v5

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->w:[Ljava/lang/String;

    .line 1220772
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->x:[I

    .line 1220773
    new-array v0, v4, [I

    const v1, 0x7f000789

    aput v1, v0, v3

    sput-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->y:[I

    return-void

    .line 1220774
    :array_0
    .array-data 4
        0x13
        0x15
        0x7fa30c00
        0x7fa30c04
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1220821
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1220822
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->B:Ljava/nio/ByteBuffer;

    .line 1220823
    return-void
.end method

.method private checkOnMediaCodecThread()V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220820
    return-void
.end method

.method public static createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220818
    :try_start_0
    invoke-static {p0}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1220819
    :goto_0
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static findHwEncoder(Ljava/lang/String;[LX/7f8;[I)Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220775
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 1220776
    const/4 v0, 0x0

    .line 1220777
    :goto_0
    return-object v0

    .line 1220778
    :cond_0
    const-string v0, "video/avc"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1220779
    sget-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->w:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1220780
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1220781
    const-string v0, "MediaCodecVideoEncoder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Model: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has black listed H.264 encoder."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220782
    const/4 v0, 0x0

    goto :goto_0

    .line 1220783
    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v1

    if-ge v0, v1, :cond_a

    .line 1220784
    invoke-static {v0}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v5

    .line 1220785
    invoke-virtual {v5}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1220786
    const/4 v1, 0x0

    .line 1220787
    invoke-virtual {v5}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v4, :cond_c

    aget-object v6, v3, v2

    .line 1220788
    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1220789
    invoke-virtual {v5}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    .line 1220790
    :goto_3
    if-eqz v4, :cond_9

    .line 1220791
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Found candidate encoder "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1220792
    const/4 v2, 0x0

    .line 1220793
    const/4 v1, 0x0

    .line 1220794
    array-length v6, p1

    const/4 v3, 0x0

    :goto_4
    if-ge v3, v6, :cond_b

    aget-object v7, p1, v3

    .line 1220795
    iget-object v8, v7, LX/7f8;->a:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1220796
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    iget v9, v7, LX/7f8;->b:I

    if-ge v8, v9, :cond_4

    .line 1220797
    const-string v7, "MediaCodecVideoEncoder"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Codec "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " is disabled due to SDK version "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220798
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1220799
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1220800
    :cond_4
    iget-boolean v2, v7, LX/7f8;->c:Z

    if-eqz v2, :cond_5

    .line 1220801
    const-string v1, "MediaCodecVideoEncoder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Codec "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " does not use frame timestamps."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220802
    const/4 v1, 0x1

    .line 1220803
    :cond_5
    const/4 v2, 0x1

    move v3, v1

    move v1, v2

    .line 1220804
    :goto_5
    if-eqz v1, :cond_9

    .line 1220805
    invoke-virtual {v5, p0}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v5

    .line 1220806
    iget-object v2, v5, Landroid/media/MediaCodecInfo$CodecCapabilities;->colorFormats:[I

    array-length v6, v2

    const/4 v1, 0x0

    :goto_6
    if-ge v1, v6, :cond_6

    aget v7, v2, v1

    .line 1220807
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "   Color: 0x"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1220808
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1220809
    :cond_6
    array-length v6, p2

    const/4 v1, 0x0

    move v2, v1

    :goto_7
    if-ge v2, v6, :cond_9

    aget v7, p2, v2

    .line 1220810
    iget-object v8, v5, Landroid/media/MediaCodecInfo$CodecCapabilities;->colorFormats:[I

    array-length v9, v8

    const/4 v1, 0x0

    :goto_8
    if-ge v1, v9, :cond_8

    aget v10, v8, v1

    .line 1220811
    if-ne v10, v7, :cond_7

    .line 1220812
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Found target encoder for mime "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". Color: 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v10}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1220813
    new-instance v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;

    invoke-direct {v0, v4, v10, v3}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;-><init>(Ljava/lang/String;IZ)V

    goto/16 :goto_0

    .line 1220814
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 1220815
    :cond_8
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_7

    .line 1220816
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 1220817
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_b
    move v3, v1

    move v1, v2

    goto :goto_5

    :cond_c
    move-object v4, v1

    goto/16 :goto_3
.end method

.method public static isH264HwSupported()Z
    .locals 3
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220756
    sget-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->d:Ljava/util/Set;

    const-string v1, "video/avc"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "video/avc"

    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->v:[LX/7f8;

    sget-object v2, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->x:[I

    invoke-static {v0, v1, v2}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->findHwEncoder(Ljava/lang/String;[LX/7f8;[I)Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isVp8HwSupported()Z
    .locals 3
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220755
    sget-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->d:Ljava/util/Set;

    const-string v1, "video/x-vnd.on2.vp8"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "video/x-vnd.on2.vp8"

    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->p:[LX/7f8;

    sget-object v2, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->x:[I

    invoke-static {v0, v1, v2}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->findHwEncoder(Ljava/lang/String;[LX/7f8;[I)Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isVp9HwSupported()Z
    .locals 3
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220754
    sget-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->d:Ljava/util/Set;

    const-string v1, "video/x-vnd.on2.vp9"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "video/x-vnd.on2.vp9"

    sget-object v1, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->s:[LX/7f8;

    sget-object v2, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->x:[I

    invoke-static {v0, v1, v2}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->findHwEncoder(Ljava/lang/String;[LX/7f8;[I)Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setRates(II)Z
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220824
    mul-int/lit16 v0, p1, 0x3e8

    .line 1220825
    iget-boolean v1, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->A:Z

    if-eqz v1, :cond_0

    if-lez p2, :cond_0

    .line 1220826
    mul-int/lit8 v0, v0, 0x1e

    div-int/2addr v0, p2

    .line 1220827
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setRates: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    div-int/lit16 v2, v0, 0x3e8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " kbps. Fps: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1220828
    :goto_0
    :try_start_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1220829
    const-string v2, "video-bitrate"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1220830
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    invoke-virtual {v0, v1}, Landroid/media/MediaCodec;->setParameters(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1220831
    const/4 v0, 0x1

    .line 1220832
    :goto_1
    return v0

    .line 1220833
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setRates: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1220834
    :catch_0
    move-exception v0

    .line 1220835
    const-string v1, "MediaCodecVideoEncoder"

    const-string v2, "setRates failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1220836
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public dequeueInputBuffer()I
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220749
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1220750
    :goto_0
    return v0

    .line 1220751
    :catch_0
    move-exception v0

    .line 1220752
    const-string v1, "MediaCodecVideoEncoder"

    const-string v2, "dequeueIntputBuffer failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1220753
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public dequeueOutputBuffer()Lorg/webrtc/videoengine/MediaCodecVideoEncoder$OutputBufferInfo;
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v10, -0x1

    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 1220709
    :try_start_0
    new-instance v4, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v4}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 1220710
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v4, v8, v9}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v1

    .line 1220711
    if-ltz v1, :cond_0

    .line 1220712
    iget v0, v4, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    move v0, v3

    .line 1220713
    :goto_0
    if-eqz v0, :cond_0

    .line 1220714
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Config frame generated. Offset: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v4, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". Size: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, v4, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1220715
    iget v0, v4, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->B:Ljava/nio/ByteBuffer;

    .line 1220716
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->g:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, v1

    iget v2, v4, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1220717
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->g:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, v1

    iget v2, v4, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v5, v4, Landroid/media/MediaCodec$BufferInfo;->size:I

    add-int/2addr v2, v5

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 1220718
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->B:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->g:[Ljava/nio/ByteBuffer;

    aget-object v2, v2, v1

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 1220719
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 1220720
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v4, v8, v9}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v1

    .line 1220721
    :cond_0
    if-ltz v1, :cond_4

    .line 1220722
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->g:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1220723
    iget v0, v4, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1220724
    iget v0, v4, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v2, v4, Landroid/media/MediaCodec$BufferInfo;->size:I

    add-int/2addr v0, v2

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 1220725
    iget v0, v4, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 1220726
    :goto_1
    if-eqz v3, :cond_3

    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->z:Ljava/lang/String;

    const-string v2, "video/avc"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1220727
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Appending config frame of size "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->B:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " to output buffer with offset "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, v4, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", size "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, v4, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1220728
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->B:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iget v2, v4, Landroid/media/MediaCodec$BufferInfo;->size:I

    add-int/2addr v0, v2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1220729
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->B:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1220730
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->B:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 1220731
    invoke-virtual {v2, v5}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 1220732
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1220733
    new-instance v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$OutputBufferInfo;

    const/4 v3, 0x1

    iget-wide v4, v4, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-direct/range {v0 .. v5}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$OutputBufferInfo;-><init>(ILjava/nio/ByteBuffer;ZJ)V

    .line 1220734
    :goto_2
    return-object v0

    :cond_1
    move v0, v7

    .line 1220735
    goto/16 :goto_0

    :cond_2
    move v3, v7

    .line 1220736
    goto :goto_1

    .line 1220737
    :cond_3
    new-instance v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$OutputBufferInfo;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v2

    iget-wide v4, v4, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-direct/range {v0 .. v5}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$OutputBufferInfo;-><init>(ILjava/nio/ByteBuffer;ZJ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1220738
    :catch_0
    move-exception v0

    .line 1220739
    const-string v1, "MediaCodecVideoEncoder"

    const-string v2, "dequeueOutputBuffer failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1220740
    new-instance v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$OutputBufferInfo;

    const-wide/16 v4, -0x1

    move v1, v10

    move-object v2, v6

    move v3, v7

    invoke-direct/range {v0 .. v5}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$OutputBufferInfo;-><init>(ILjava/nio/ByteBuffer;ZJ)V

    goto :goto_2

    .line 1220741
    :cond_4
    const/4 v0, -0x3

    if-ne v1, v0, :cond_5

    .line 1220742
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->g:[Ljava/nio/ByteBuffer;

    .line 1220743
    invoke-virtual {p0}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->dequeueOutputBuffer()Lorg/webrtc/videoengine/MediaCodecVideoEncoder$OutputBufferInfo;

    move-result-object v0

    goto :goto_2

    .line 1220744
    :cond_5
    const/4 v0, -0x2

    if-ne v1, v0, :cond_6

    .line 1220745
    invoke-virtual {p0}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->dequeueOutputBuffer()Lorg/webrtc/videoengine/MediaCodecVideoEncoder$OutputBufferInfo;

    move-result-object v0

    goto :goto_2

    .line 1220746
    :cond_6
    if-ne v1, v10, :cond_7

    move-object v0, v6

    .line 1220747
    goto :goto_2

    .line 1220748
    :cond_7
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dequeueOutputBuffer: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public encodeBuffer(ZIIJ)Z
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1220699
    if-eqz p1, :cond_0

    .line 1220700
    :try_start_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1220701
    const-string v1, "request-sync"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1220702
    iget-object v1, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    invoke-virtual {v1, v0}, Landroid/media/MediaCodec;->setParameters(Landroid/os/Bundle;)V

    .line 1220703
    :cond_0
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    const/4 v2, 0x0

    const/4 v6, 0x0

    move v1, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1220704
    const/4 v0, 0x1

    .line 1220705
    :goto_0
    return v0

    .line 1220706
    :catch_0
    move-exception v0

    .line 1220707
    const-string v1, "MediaCodecVideoEncoder"

    const-string v2, "encodeBuffer failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v7

    .line 1220708
    goto :goto_0
.end method

.method public encodeTexture(ZI[FJ)Z
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1220594
    if-eqz p1, :cond_0

    .line 1220595
    :try_start_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1220596
    const-string v1, "request-sync"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1220597
    iget-object v1, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    invoke-virtual {v1, v0}, Landroid/media/MediaCodec;->setParameters(Landroid/os/Bundle;)V

    .line 1220598
    :cond_0
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->h:LX/7f1;

    invoke-virtual {v0}, LX/7f1;->b()V

    .line 1220599
    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 1220600
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->l:LX/7f3;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget v5, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->i:I

    iget v6, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->j:I

    move v1, p2

    move-object v2, p3

    const p2, 0x8d65

    .line 1220601
    const-string p1, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 interp_tc;\n\nuniform samplerExternalOES oes_tex;\n\nvoid main() {\n  gl_FragColor = texture2D(oes_tex, interp_tc);\n}\n"

    invoke-static {v0, p1, v2}, LX/7f3;->a(LX/7f3;Ljava/lang/String;[F)V

    .line 1220602
    const p1, 0x84c0

    invoke-static {p1}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 1220603
    invoke-static {p2, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1220604
    invoke-static {v3, v4, v5, v6}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1220605
    const/4 p1, 0x5

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 1220606
    const/4 p1, 0x0

    invoke-static {p2, p1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1220607
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->h:LX/7f1;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p4, p5}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/7f1;->a(J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1220608
    const/4 v0, 0x1

    .line 1220609
    :goto_0
    return v0

    .line 1220610
    :catch_0
    move-exception v0

    .line 1220611
    const-string v1, "MediaCodecVideoEncoder"

    const-string v2, "encodeTexture failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v7

    .line 1220612
    goto :goto_0
.end method

.method public getInputBuffers()[Ljava/nio/ByteBuffer;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220696
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1220697
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Input buffers: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1220698
    return-object v0
.end method

.method public initEncode(Ljava/lang/String;IIIII)Z
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 1220657
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Java initEncode: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " x "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". @ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " kbps. Fps: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Encode from texture : false"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1220658
    iput p2, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->i:I

    .line 1220659
    iput p3, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->j:I

    .line 1220660
    iget-object v3, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->e:Ljava/lang/Thread;

    if-eqz v3, :cond_0

    .line 1220661
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Forgot to release()?"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1220662
    :cond_0
    const-string v3, "video/x-vnd.on2.vp8"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1220663
    const-string v2, "video/x-vnd.on2.vp8"

    sget-object v3, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->p:[LX/7f8;

    sget-object v4, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->x:[I

    invoke-static {v2, v3, v4}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->findHwEncoder(Ljava/lang/String;[LX/7f8;[I)Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;

    move-result-object v2

    .line 1220664
    :cond_1
    :goto_0
    if-nez v2, :cond_4

    .line 1220665
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not find HW encoder for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1220666
    :cond_2
    const-string v3, "video/x-vnd.on2.vp9"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1220667
    const-string v2, "video/x-vnd.on2.vp9"

    sget-object v3, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->s:[LX/7f8;

    sget-object v4, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->x:[I

    invoke-static {v2, v3, v4}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->findHwEncoder(Ljava/lang/String;[LX/7f8;[I)Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;

    move-result-object v2

    goto :goto_0

    .line 1220668
    :cond_3
    const-string v3, "video/avc"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1220669
    const-string v2, "video/avc"

    sget-object v3, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->v:[LX/7f8;

    sget-object v4, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->x:[I

    invoke-static {v2, v3, v4}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->findHwEncoder(Ljava/lang/String;[LX/7f8;[I)Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;

    move-result-object v2

    goto :goto_0

    .line 1220670
    :cond_4
    sput-object p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->a:Lorg/webrtc/videoengine/MediaCodecVideoEncoder;

    .line 1220671
    iget v3, v2, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;->colorFormat:I

    iput v3, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->colorFormat:I

    .line 1220672
    iget-boolean v3, v2, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;->bitrateAdjustment:Z

    iput-boolean v3, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->A:Z

    .line 1220673
    iget-boolean v3, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->A:Z

    if-eqz v3, :cond_5

    .line 1220674
    const/16 p5, 0x1e

    .line 1220675
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Color format: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->colorFormat:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Bitrate adjustment: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->A:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1220676
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    iput-object v3, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->e:Ljava/lang/Thread;

    .line 1220677
    :try_start_0
    invoke-static {p1, p2, p3}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v3

    .line 1220678
    const-string v4, "bitrate"

    mul-int/lit16 v5, p4, 0x3e8

    invoke-virtual {v3, v4, v5}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1220679
    const-string v4, "bitrate-mode"

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1220680
    const-string v4, "color-format"

    iget v5, v2, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;->colorFormat:I

    invoke-virtual {v3, v4, v5}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1220681
    const-string v4, "frame-rate"

    invoke-virtual {v3, v4, p5}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1220682
    const-string v4, "i-frame-interval"

    invoke-virtual {v3, v4, p6}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1220683
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "  Format: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1220684
    iget-object v2, v2, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$EncoderProperties;->codecName:Ljava/lang/String;

    invoke-static {v2}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v2

    iput-object v2, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    .line 1220685
    iput-object p1, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->z:Ljava/lang/String;

    .line 1220686
    iget-object v2, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    if-nez v2, :cond_6

    .line 1220687
    const-string v1, "MediaCodecVideoEncoder"

    const-string v2, "Can not create media encoder"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220688
    :goto_1
    return v0

    .line 1220689
    :cond_6
    iget-object v2, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 1220690
    iget-object v2, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->start()V

    .line 1220691
    iget-object v2, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->g:[Ljava/nio/ByteBuffer;

    .line 1220692
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Output buffers: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->g:[Ljava/nio/ByteBuffer;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 1220693
    goto :goto_1

    .line 1220694
    :catch_0
    move-exception v1

    .line 1220695
    const-string v2, "MediaCodecVideoEncoder"

    const-string v3, "initEncode failed"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public release()V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1220618
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 1220619
    new-instance v1, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$1;

    invoke-direct {v1, p0, v0}, Lorg/webrtc/videoengine/MediaCodecVideoEncoder$1;-><init>(Lorg/webrtc/videoengine/MediaCodecVideoEncoder;Ljava/util/concurrent/CountDownLatch;)V

    .line 1220620
    const v2, -0x67fc2e19

    invoke-static {v1, v2}, LX/00l;->a(Ljava/lang/Runnable;I)Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1220621
    const-wide/16 v2, 0x1388

    invoke-static {v0, v2, v3}, LX/7ez;->a(Ljava/util/concurrent/CountDownLatch;J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1220622
    const-string v0, "MediaCodecVideoEncoder"

    const-string v1, "Media encoder release timeout"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220623
    sget v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->c:I

    .line 1220624
    sget-object v0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->b:LX/7f9;

    if-eqz v0, :cond_0

    .line 1220625
    const-string v0, "MediaCodecVideoEncoder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invoke codec error callback. Errors: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220626
    :cond_0
    iput-object v4, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    .line 1220627
    iput-object v4, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->e:Ljava/lang/Thread;

    .line 1220628
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->l:LX/7f3;

    if-eqz v0, :cond_3

    .line 1220629
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->l:LX/7f3;

    .line 1220630
    iget-object v1, v0, LX/7f3;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7f2;

    .line 1220631
    iget-object v1, v1, LX/7f2;->a:LX/7f4;

    const/4 v5, -0x1

    .line 1220632
    iget v3, v1, LX/7f4;->a:I

    if-eq v3, v5, :cond_1

    .line 1220633
    iget v3, v1, LX/7f4;->a:I

    invoke-static {v3}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    .line 1220634
    iput v5, v1, LX/7f4;->a:I

    .line 1220635
    :cond_1
    goto :goto_0

    .line 1220636
    :cond_2
    iget-object v1, v0, LX/7f3;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 1220637
    iput-object v4, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->l:LX/7f3;

    .line 1220638
    :cond_3
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->h:LX/7f1;

    if-eqz v0, :cond_5

    .line 1220639
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->h:LX/7f1;

    .line 1220640
    invoke-static {v0}, LX/7f1;->d(LX/7f1;)V

    .line 1220641
    iget-object v1, v0, LX/7f1;->j:Landroid/opengl/EGLSurface;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-eq v1, v2, :cond_4

    .line 1220642
    iget-object v1, v0, LX/7f1;->i:Landroid/opengl/EGLDisplay;

    iget-object v2, v0, LX/7f1;->j:Landroid/opengl/EGLSurface;

    invoke-static {v1, v2}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 1220643
    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    iput-object v1, v0, LX/7f1;->j:Landroid/opengl/EGLSurface;

    .line 1220644
    :cond_4
    invoke-static {v0}, LX/7f1;->e(LX/7f1;)V

    .line 1220645
    iget-object v1, v0, LX/7f1;->i:Landroid/opengl/EGLDisplay;

    iget-object v2, v0, LX/7f1;->g:Landroid/opengl/EGLContext;

    invoke-static {v1, v2}, Landroid/opengl/EGL14;->eglDestroyContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;)Z

    .line 1220646
    invoke-static {}, Landroid/opengl/EGL14;->eglReleaseThread()Z

    .line 1220647
    iget-object v1, v0, LX/7f1;->i:Landroid/opengl/EGLDisplay;

    invoke-static {v1}, Landroid/opengl/EGL14;->eglTerminate(Landroid/opengl/EGLDisplay;)Z

    .line 1220648
    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    iput-object v1, v0, LX/7f1;->g:Landroid/opengl/EGLContext;

    .line 1220649
    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    iput-object v1, v0, LX/7f1;->i:Landroid/opengl/EGLDisplay;

    .line 1220650
    const/4 v1, 0x0

    iput-object v1, v0, LX/7f1;->h:Landroid/opengl/EGLConfig;

    .line 1220651
    iput-object v4, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->h:LX/7f1;

    .line 1220652
    :cond_5
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->k:Landroid/view/Surface;

    if-eqz v0, :cond_6

    .line 1220653
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->k:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 1220654
    iput-object v4, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->k:Landroid/view/Surface;

    .line 1220655
    :cond_6
    sput-object v4, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->a:Lorg/webrtc/videoengine/MediaCodecVideoEncoder;

    .line 1220656
    return-void
.end method

.method public releaseOutputBuffer(I)Z
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1220613
    :try_start_0
    iget-object v1, p0, Lorg/webrtc/videoengine/MediaCodecVideoEncoder;->f:Landroid/media/MediaCodec;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1220614
    const/4 v0, 0x1

    .line 1220615
    :goto_0
    return v0

    .line 1220616
    :catch_0
    move-exception v1

    .line 1220617
    const-string v2, "MediaCodecVideoEncoder"

    const-string v3, "releaseOutputBuffer failed"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
