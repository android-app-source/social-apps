.class public Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;
.super Landroid/opengl/GLSurfaceView;
.source ""

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Z

.field private c:Z

.field private d:Z

.field private final e:Ljava/util/concurrent/locks/ReentrantLock;

.field private f:J

.field private g:I

.field private h:I

.field public i:I

.field public j:I

.field public k:Z

.field private final l:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

.field private m:I

.field private n:I

.field public o:LX/7fD;

.field public p:LX/7fE;

.field private q:I

.field private final r:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1221051
    const-class v0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    sput-object v0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1220994
    invoke-direct {p0, p1}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    .line 1220995
    iput-boolean v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->b:Z

    .line 1220996
    iput-boolean v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->c:Z

    .line 1220997
    iput-boolean v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->d:Z

    .line 1220998
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    .line 1220999
    iput-wide v4, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->f:J

    .line 1221000
    iput v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->g:I

    .line 1221001
    iput v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->h:I

    .line 1221002
    iput v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->i:I

    .line 1221003
    iput v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->j:I

    .line 1221004
    iput-boolean v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->k:Z

    .line 1221005
    sget-object v0, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->INSTANCE:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-object v0, v0

    .line 1221006
    iput-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->l:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    .line 1221007
    iput-object v2, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->o:LX/7fD;

    .line 1221008
    iput-object v2, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->p:LX/7fE;

    .line 1221009
    iput v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->q:I

    .line 1221010
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->r:Ljava/util/concurrent/atomic/AtomicLong;

    .line 1221011
    invoke-direct {p0, v1, v1}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->c(II)V

    .line 1221012
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1221013
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1221014
    iput-boolean v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->b:Z

    .line 1221015
    iput-boolean v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->c:Z

    .line 1221016
    iput-boolean v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->d:Z

    .line 1221017
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    .line 1221018
    iput-wide v4, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->f:J

    .line 1221019
    iput v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->g:I

    .line 1221020
    iput v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->h:I

    .line 1221021
    iput v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->i:I

    .line 1221022
    iput v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->j:I

    .line 1221023
    iput-boolean v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->k:Z

    .line 1221024
    sget-object v0, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->INSTANCE:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-object v0, v0

    .line 1221025
    iput-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->l:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    .line 1221026
    iput-object v2, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->o:LX/7fD;

    .line 1221027
    iput-object v2, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->p:LX/7fE;

    .line 1221028
    iput v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->q:I

    .line 1221029
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->r:Ljava/util/concurrent/atomic/AtomicLong;

    .line 1221030
    invoke-direct {p0, v1, v1}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->c(II)V

    .line 1221031
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZII)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1221032
    invoke-direct {p0, p1}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    .line 1221033
    iput-boolean v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->b:Z

    .line 1221034
    iput-boolean v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->c:Z

    .line 1221035
    iput-boolean v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->d:Z

    .line 1221036
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    .line 1221037
    iput-wide v4, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->f:J

    .line 1221038
    iput v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->g:I

    .line 1221039
    iput v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->h:I

    .line 1221040
    iput v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->i:I

    .line 1221041
    iput v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->j:I

    .line 1221042
    iput-boolean v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->k:Z

    .line 1221043
    sget-object v0, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->INSTANCE:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-object v0, v0

    .line 1221044
    iput-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->l:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    .line 1221045
    iput-object v2, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->o:LX/7fD;

    .line 1221046
    iput-object v2, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->p:LX/7fE;

    .line 1221047
    iput v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->q:I

    .line 1221048
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->r:Ljava/util/concurrent/atomic/AtomicLong;

    .line 1221049
    invoke-direct {p0, p3, p4}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->c(II)V

    .line 1221050
    return-void
.end method

.method private native CreateOpenGLNative(JIII)I
.end method

.method private native DrawNative(J)V
.end method

.method public static UseOpenGL2(Ljava/lang/Object;)Z
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1221068
    const-class v0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private b(II)V
    .locals 0

    .prologue
    .line 1221052
    iput p1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->n:I

    .line 1221053
    iput p2, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->m:I

    .line 1221054
    return-void
.end method

.method public static b(Ljava/lang/String;Ljavax/microedition/khronos/egl/EGL10;)V
    .locals 5

    .prologue
    .line 1221055
    :goto_0
    invoke-interface {p1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    const/16 v1, 0x3000

    if-eq v0, v1, :cond_0

    .line 1221056
    sget-object v1, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->a:Ljava/lang/Class;

    const-string v2, "%s: EGL error: 0x%x"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1221057
    :cond_0
    return-void
.end method

.method private c(II)V
    .locals 7

    .prologue
    const/4 v1, 0x5

    const/4 v4, 0x0

    .line 1221058
    new-instance v0, LX/7fC;

    invoke-direct {v0}, LX/7fC;-><init>()V

    invoke-virtual {p0, v0}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->setEGLContextFactory(Landroid/opengl/GLSurfaceView$EGLContextFactory;)V

    .line 1221059
    new-instance v0, LX/7fB;

    const/4 v2, 0x6

    move v3, v1

    move v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, LX/7fB;-><init>(IIIIII)V

    invoke-virtual {p0, v0}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V

    .line 1221060
    invoke-virtual {p0, p0}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 1221061
    invoke-virtual {p0, v4}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->setRenderMode(I)V

    .line 1221062
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->r:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 1221063
    return-void
.end method

.method public static d(Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;II)V
    .locals 0

    .prologue
    .line 1221064
    iput p1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->g:I

    .line 1221065
    iput p2, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->h:I

    .line 1221066
    invoke-virtual {p0}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->requestLayout()V

    .line 1221067
    return-void
.end method


# virtual methods
.method public DeRegisterNativeObject()V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220981
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1220982
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->d:Z

    .line 1220983
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->c:Z

    .line 1220984
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1220985
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1220986
    return-void

    .line 1220987
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public ReDraw(II)V
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220988
    iget v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->i:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->j:I

    if-eq v0, p2, :cond_1

    .line 1220989
    :cond_0
    new-instance v0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView$1;

    invoke-direct {v0, p0, p1, p2}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView$1;-><init>(Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;II)V

    invoke-virtual {p0, v0}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->post(Ljava/lang/Runnable;)Z

    .line 1220990
    :cond_1
    iget-boolean v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->b:Z

    if-eqz v0, :cond_2

    .line 1220991
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->r:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->l:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->now()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 1220992
    invoke-virtual {p0}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->requestRender()V

    .line 1220993
    :cond_2
    return-void
.end method

.method public RegisterNativeObject(J)V
    .locals 3
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1220917
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1220918
    :try_start_0
    iput-wide p1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->f:J

    .line 1220919
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1220920
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1220921
    return-void

    .line 1220922
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 1220923
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->r:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 1220924
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 1220925
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->k:Z

    .line 1220926
    invoke-static {p0, p1, p2}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->d(Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;II)V

    .line 1220927
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1220928
    iget-boolean v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->b:Z

    if-eqz v0, :cond_0

    .line 1220929
    invoke-virtual {p0}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->requestRender()V

    .line 1220930
    :cond_0
    return-void
.end method

.method public getLastRedrawTime()J
    .locals 2

    .prologue
    .line 1220931
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->r:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public final onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 7

    .prologue
    .line 1220932
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1220933
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    :try_start_0
    invoke-interface {p1, v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glClearColor(FFFF)V

    .line 1220934
    const/16 v0, 0x4100

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glClear(I)V

    .line 1220935
    iget-boolean v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    .line 1220936
    :cond_0
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1220937
    :cond_1
    :goto_0
    return-void

    .line 1220938
    :cond_2
    :try_start_1
    iget-boolean v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->c:Z

    if-nez v0, :cond_4

    .line 1220939
    iget-wide v2, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->f:J

    iget v4, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->q:I

    iget v5, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->n:I

    iget v6, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->m:I

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->CreateOpenGLNative(JIII)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_3

    .line 1220940
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    .line 1220941
    :cond_3
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->c:Z

    .line 1220942
    :cond_4
    iget-wide v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->f:J

    invoke-direct {p0, v0, v1}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->DrawNative(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1220943
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1220944
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->o:LX/7fD;

    if-eqz v0, :cond_1

    .line 1220945
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->o:LX/7fD;

    invoke-interface {v0}, LX/7fD;->a()V

    .line 1220946
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->o:LX/7fD;

    goto :goto_0

    .line 1220947
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    .line 1220948
    iget v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->g:I

    if-lez v0, :cond_0

    iget v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->h:I

    if-lez v0, :cond_0

    iget v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->q:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1220949
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/opengl/GLSurfaceView;->onMeasure(II)V

    .line 1220950
    :goto_0
    return-void

    .line 1220951
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1220952
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1220953
    if-lez v1, :cond_2

    if-gtz v0, :cond_3

    .line 1220954
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/opengl/GLSurfaceView;->onMeasure(II)V

    goto :goto_0

    .line 1220955
    :cond_3
    mul-int/lit8 v2, v1, 0x64

    div-int/2addr v2, v0

    .line 1220956
    iget v3, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->g:I

    mul-int/lit8 v3, v3, 0x64

    iget v4, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->h:I

    div-int/2addr v3, v4

    .line 1220957
    sub-int v4, v2, v3

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    const/16 v5, 0xa

    if-gt v4, v5, :cond_4

    .line 1220958
    invoke-virtual {p0, v1, v0}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->setMeasuredDimension(II)V

    goto :goto_0

    .line 1220959
    :cond_4
    if-le v2, v3, :cond_5

    .line 1220960
    iget v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->g:I

    mul-int/2addr v1, v0

    iget v2, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->h:I

    div-int/2addr v1, v2

    .line 1220961
    :goto_1
    invoke-virtual {p0, v1, v0}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->setMeasuredDimension(II)V

    goto :goto_0

    .line 1220962
    :cond_5
    iget v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->h:I

    mul-int/2addr v0, v1

    iget v2, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->g:I

    div-int/2addr v0, v2

    goto :goto_1
.end method

.method public final onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 2

    .prologue
    .line 1220963
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->b:Z

    .line 1220964
    invoke-direct {p0, p2, p3}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->b(II)V

    .line 1220965
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1220966
    :try_start_0
    iget-boolean v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->d:Z

    if-eqz v0, :cond_0

    .line 1220967
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1220968
    :cond_0
    iget-object v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1220969
    return-void

    .line 1220970
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->e:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 0

    .prologue
    .line 1220971
    return-void
.end method

.method public setOneShotDrawListener(LX/7fD;)V
    .locals 0

    .prologue
    .line 1220972
    iput-object p1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->o:LX/7fD;

    .line 1220973
    return-void
.end method

.method public setScaleType(I)V
    .locals 1

    .prologue
    .line 1220974
    iget v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->q:I

    if-eq v0, p1, :cond_0

    .line 1220975
    iput p1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->q:I

    .line 1220976
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->c:Z

    .line 1220977
    invoke-virtual {p0}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->requestLayout()V

    .line 1220978
    :cond_0
    return-void
.end method

.method public setVideoSizeChangedListener(LX/7fE;)V
    .locals 0

    .prologue
    .line 1220979
    iput-object p1, p0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->p:LX/7fE;

    .line 1220980
    return-void
.end method
