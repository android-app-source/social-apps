.class public Lorg/webrtc/videoengine/VideoCaptureAndroid;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/Camera$ErrorCallback;
.implements Landroid/hardware/Camera$PreviewCallback;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static b:Landroid/graphics/SurfaceTexture;

.field private static c:LX/EDx;

.field private static d:Z

.field private static e:Lorg/webrtc/videoengine/VideoCaptureAndroid;


# instance fields
.field public f:Landroid/graphics/SurfaceTexture;

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:Landroid/hardware/Camera;

.field private n:I

.field private o:I

.field private p:Lorg/webrtc/videoengine/VideoCaptureAndroid$CameraThread;

.field private q:Landroid/os/Handler;

.field private r:LX/7fF;

.field private s:I

.field private final t:I

.field private final u:Landroid/hardware/Camera$CameraInfo;

.field private final v:Landroid/view/Display;

.field private final w:J

.field private final x:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1221362
    const-class v0, Lorg/webrtc/videoengine/VideoCaptureAndroid;

    sput-object v0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IJ)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1221335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1221336
    sget-object v0, LX/7fF;->STOPPED:LX/7fF;

    iput-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->r:LX/7fF;

    .line 1221337
    const/4 v0, 0x3

    iput v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->x:I

    .line 1221338
    iput p2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->t:I

    .line 1221339
    iput-wide p3, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->w:J

    .line 1221340
    invoke-static {p2}, Lorg/webrtc/videoengine/VideoCaptureDeviceInfoAndroid;->a(I)Landroid/hardware/Camera$CameraInfo;

    move-result-object v0

    iput-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->u:Landroid/hardware/Camera$CameraInfo;

    .line 1221341
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->v:Landroid/view/Display;

    .line 1221342
    return-void
.end method

.method private native OnOrientationChanged(JI)V
.end method

.method private native ProvideBGRACameraFrame([BIIIJ)V
.end method

.method private native ProvideCameraFrame([BIIIJ)V
.end method

.method private a(I)I
    .locals 7

    .prologue
    const/16 v6, 0x13b

    const/16 v5, 0xe1

    const/16 v4, 0x87

    const/16 v3, 0x2d

    const/4 v0, 0x0

    .line 1221343
    iget-object v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->u:Landroid/hardware/Camera$CameraInfo;

    iget v1, v1, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1221344
    iget-object v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->u:Landroid/hardware/Camera$CameraInfo;

    iget v1, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int/2addr v1, p1

    add-int/lit16 v1, v1, 0x168

    rem-int/lit16 v1, v1, 0x168

    .line 1221345
    :goto_0
    if-le v1, v3, :cond_0

    if-le v1, v6, :cond_2

    .line 1221346
    :cond_0
    :goto_1
    return v0

    .line 1221347
    :cond_1
    iget-object v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->u:Landroid/hardware/Camera$CameraInfo;

    iget v1, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v1, p1

    rem-int/lit16 v1, v1, 0x168

    goto :goto_0

    .line 1221348
    :cond_2
    if-le v1, v3, :cond_3

    if-gt v1, v4, :cond_3

    .line 1221349
    const/16 v0, 0x5a

    goto :goto_1

    .line 1221350
    :cond_3
    if-le v1, v4, :cond_4

    if-gt v1, v5, :cond_4

    .line 1221351
    const/16 v0, 0xb4

    goto :goto_1

    .line 1221352
    :cond_4
    if-le v1, v5, :cond_0

    if-gt v1, v6, :cond_0

    .line 1221353
    const/16 v0, 0x10e

    goto :goto_1
.end method

.method public static declared-synchronized a()Landroid/graphics/SurfaceTexture;
    .locals 2

    .prologue
    .line 1221354
    const-class v0, Lorg/webrtc/videoengine/VideoCaptureAndroid;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lorg/webrtc/videoengine/VideoCaptureAndroid;->b:Landroid/graphics/SurfaceTexture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static a(Ljava/util/List;II)Landroid/hardware/Camera$Size;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;II)",
            "Landroid/hardware/Camera$Size;"
        }
    .end annotation

    .prologue
    .line 1221355
    const/4 v2, -0x1

    .line 1221356
    const/4 v1, 0x0

    .line 1221357
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 1221358
    iget v3, v0, Landroid/hardware/Camera$Size;->width:I

    iget v5, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v3, v5

    mul-int v5, p1, p2

    sub-int/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 1221359
    if-ltz v2, :cond_0

    if-le v2, v3, :cond_2

    :cond_0
    move v1, v3

    :goto_1
    move v2, v1

    move-object v1, v0

    .line 1221360
    goto :goto_0

    .line 1221361
    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    move v1, v2

    goto :goto_1
.end method

.method public static declared-synchronized a(LX/EDx;)V
    .locals 2

    .prologue
    .line 1221285
    const-class v0, Lorg/webrtc/videoengine/VideoCaptureAndroid;

    monitor-enter v0

    :try_start_0
    sput-object p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->c:LX/EDx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1221286
    monitor-exit v0

    return-void

    .line 1221287
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static a(Landroid/graphics/SurfaceTexture;)V
    .locals 2

    .prologue
    .line 1221363
    const-class v1, Lorg/webrtc/videoengine/VideoCaptureAndroid;

    monitor-enter v1

    .line 1221364
    :try_start_0
    sput-object p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->b:Landroid/graphics/SurfaceTexture;

    .line 1221365
    sget-object v0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->e:Lorg/webrtc/videoengine/VideoCaptureAndroid;

    .line 1221366
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1221367
    if-eqz v0, :cond_0

    .line 1221368
    sget-object v1, Lorg/webrtc/videoengine/VideoCaptureAndroid;->b:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->b(Landroid/graphics/SurfaceTexture;)V

    .line 1221369
    :cond_0
    return-void

    .line 1221370
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static a(Landroid/hardware/Camera$Parameters;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    .line 1221371
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_0

    .line 1221372
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->isVideoStabilizationSupported()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1221373
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->isVideoStabilizationSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1221374
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/hardware/Camera$Parameters;->setVideoStabilization(Z)V

    .line 1221375
    :cond_0
    return-void
.end method

.method public static a(Z)V
    .locals 0

    .prologue
    .line 1221376
    sput-boolean p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->d:Z

    .line 1221377
    return-void
.end method

.method public static a([BII)V
    .locals 8

    .prologue
    .line 1221378
    const-class v2, Lorg/webrtc/videoengine/VideoCaptureAndroid;

    monitor-enter v2

    .line 1221379
    :try_start_0
    sget-object v1, Lorg/webrtc/videoengine/VideoCaptureAndroid;->e:Lorg/webrtc/videoengine/VideoCaptureAndroid;

    .line 1221380
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1221381
    if-eqz v1, :cond_0

    .line 1221382
    array-length v3, p0

    iget-wide v6, v1, Lorg/webrtc/videoengine/VideoCaptureAndroid;->w:J

    move-object v2, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v1 .. v7}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->ProvideBGRACameraFrame([BIIIJ)V

    .line 1221383
    :cond_0
    return-void

    .line 1221384
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(IIII)Z
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1221288
    iput p1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->g:I

    .line 1221289
    iput p2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->h:I

    .line 1221290
    iput p3, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->i:I

    .line 1221291
    iput p4, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->j:I

    .line 1221292
    iget-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->f:Landroid/graphics/SurfaceTexture;

    if-nez v2, :cond_1

    .line 1221293
    sget-object v1, LX/7fF;->WAITING_FOR_SURFACE:LX/7fF;

    iput-object v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->r:LX/7fF;

    .line 1221294
    :cond_0
    :goto_0
    return v0

    .line 1221295
    :cond_1
    invoke-direct {p0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->c()I

    move-result v2

    iput v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->k:I

    .line 1221296
    const/4 v2, -0x1

    iput v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->s:I

    .line 1221297
    iput v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->l:I

    .line 1221298
    :try_start_0
    iget v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->t:I

    const v3, -0x11a1c043

    invoke-static {v2, v3}, LX/0J2;->a(II)Landroid/hardware/Camera;

    move-result-object v2

    iput-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    .line 1221299
    iget-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    .line 1221300
    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v3

    invoke-static {v3, p1, p2}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(Ljava/util/List;II)Landroid/hardware/Camera$Size;

    move-result-object v3

    .line 1221301
    if-eqz v3, :cond_2

    .line 1221302
    iget p1, v3, Landroid/hardware/Camera$Size;->width:I

    .line 1221303
    iget p2, v3, Landroid/hardware/Camera$Size;->height:I

    .line 1221304
    :cond_2
    invoke-virtual {v2, p1, p2}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 1221305
    iput p1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->n:I

    .line 1221306
    iput p2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->o:I

    .line 1221307
    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    move-result-object v3

    invoke-static {v3, p4}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(Ljava/util/List;I)[I

    move-result-object v3

    .line 1221308
    if-eqz v3, :cond_3

    .line 1221309
    const/4 v4, 0x0

    aget p3, v3, v4

    .line 1221310
    const/4 v4, 0x1

    aget p4, v3, v4

    .line 1221311
    :cond_3
    invoke-virtual {v2, p3, p4}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    .line 1221312
    invoke-static {v2}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(Landroid/hardware/Camera$Parameters;)V

    .line 1221313
    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/hardware/Camera$Parameters;->setPreviewFormat(I)V

    .line 1221314
    iget-object v3, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    invoke-virtual {v3, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 1221315
    mul-int v2, p1, p2

    const/16 v3, 0x11

    invoke-static {v3}, Landroid/graphics/ImageFormat;->getBitsPerPixel(I)I

    move-result v3

    mul-int/2addr v2, v3

    div-int/lit8 v3, v2, 0x8

    .line 1221316
    if-lez v3, :cond_4

    move v2, v1

    .line 1221317
    :goto_1
    const/4 v4, 0x3

    if-ge v2, v4, :cond_4

    .line 1221318
    iget-object v4, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    new-array v5, v3, [B

    invoke-virtual {v4, v5}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    .line 1221319
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1221320
    :cond_4
    iget-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    invoke-virtual {v2, p0}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    .line 1221321
    iget-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    iget-object v3, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->f:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    .line 1221322
    iget-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    invoke-virtual {v2, p0}, Landroid/hardware/Camera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    .line 1221323
    invoke-static {p0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->e(Lorg/webrtc/videoengine/VideoCaptureAndroid;)V

    .line 1221324
    iget-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    const v3, 0x2cc2b9ab

    invoke-static {v2, v3}, LX/0J2;->b(Landroid/hardware/Camera;I)V

    .line 1221325
    sget-object v2, LX/7fF;->CAPTURING:LX/7fF;

    iput-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->r:LX/7fF;

    .line 1221326
    sget-object v2, Lorg/webrtc/videoengine/VideoCaptureAndroid;->c:LX/EDx;

    if-eqz v2, :cond_0

    .line 1221327
    sget-object v2, Lorg/webrtc/videoengine/VideoCaptureAndroid;->c:LX/EDx;

    iget v3, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->n:I

    iget v4, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->o:I

    invoke-virtual {v2, v3, v4}, LX/EDx;->a(II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 1221328
    :catch_0
    move-exception v0

    .line 1221329
    :goto_2
    sget-object v2, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a:Ljava/lang/Class;

    const-string v3, "startCapture failed"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1221330
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    if-eqz v0, :cond_5

    .line 1221331
    invoke-static {p0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->d(Lorg/webrtc/videoengine/VideoCaptureAndroid;)Z

    :cond_5
    move v0, v1

    .line 1221332
    goto/16 :goto_0

    .line 1221333
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static synthetic a(Lorg/webrtc/videoengine/VideoCaptureAndroid;IIII)Z
    .locals 1

    .prologue
    .line 1221334
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(IIII)Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/util/List;I)[I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[I>;I)[I"
        }
    .end annotation

    .prologue
    .line 1221168
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 1221169
    const/4 v2, 0x1

    aget v2, v0, v2

    if-gt p1, v2, :cond_0

    .line 1221170
    :goto_0
    return-object v0

    .line 1221171
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1221172
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    goto :goto_0

    .line 1221173
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/util/concurrent/Exchanger;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Exchanger",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 1221174
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1221175
    :catch_0
    move-exception v0

    .line 1221176
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static declared-synchronized b()V
    .locals 2

    .prologue
    .line 1221177
    const-class v1, Lorg/webrtc/videoengine/VideoCaptureAndroid;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->c:LX/EDx;

    if-eqz v0, :cond_0

    .line 1221178
    sget-object v0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->c:LX/EDx;

    invoke-virtual {v0}, LX/EDx;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1221179
    :cond_0
    monitor-exit v1

    return-void

    .line 1221180
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized b(Landroid/graphics/SurfaceTexture;)V
    .locals 3

    .prologue
    .line 1221181
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->q:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1221182
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->q:Landroid/os/Handler;

    new-instance v1, Lorg/webrtc/videoengine/VideoCaptureAndroid$3;

    invoke-direct {v1, p0, p1}, Lorg/webrtc/videoengine/VideoCaptureAndroid$3;-><init>(Lorg/webrtc/videoengine/VideoCaptureAndroid;Landroid/graphics/SurfaceTexture;)V

    const v2, -0x6d1f4aff

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1221183
    :cond_0
    monitor-exit p0

    return-void

    .line 1221184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1221185
    iget-object v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->v:Landroid/view/Display;

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1221186
    :goto_0
    :pswitch_0
    return v0

    .line 1221187
    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_0

    .line 1221188
    :pswitch_2
    const/16 v0, 0xb4

    goto :goto_0

    .line 1221189
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static declared-synchronized c(Lorg/webrtc/videoengine/VideoCaptureAndroid;)V
    .locals 2

    .prologue
    .line 1221190
    const-class v0, Lorg/webrtc/videoengine/VideoCaptureAndroid;

    monitor-enter v0

    :try_start_0
    sput-object p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->e:Lorg/webrtc/videoengine/VideoCaptureAndroid;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1221191
    monitor-exit v0

    return-void

    .line 1221192
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static c(Lorg/webrtc/videoengine/VideoCaptureAndroid;Landroid/graphics/SurfaceTexture;)V
    .locals 4

    .prologue
    .line 1221193
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->r:LX/7fF;

    sget-object v1, LX/7fF;->STOPPED:LX/7fF;

    if-eq v0, v1, :cond_1

    .line 1221194
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->f:Landroid/graphics/SurfaceTexture;

    if-eq v0, p1, :cond_1

    .line 1221195
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->f:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 1221196
    invoke-static {p0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->d(Lorg/webrtc/videoengine/VideoCaptureAndroid;)Z

    .line 1221197
    :cond_0
    iput-object p1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->f:Landroid/graphics/SurfaceTexture;

    .line 1221198
    iget v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->g:I

    iget v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->h:I

    iget v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->i:I

    iget v3, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->j:I

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(IIII)Z

    .line 1221199
    :cond_1
    return-void
.end method

.method public static d(Lorg/webrtc/videoengine/VideoCaptureAndroid;)Z
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1221200
    iget-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    if-nez v2, :cond_0

    .line 1221201
    sget-object v1, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a:Ljava/lang/Class;

    const-string v2, "Camera is already stopped!"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1221202
    sget-object v1, LX/7fF;->STOPPED:LX/7fF;

    iput-object v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->r:LX/7fF;

    .line 1221203
    :goto_0
    return v0

    .line 1221204
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->r:LX/7fF;

    sget-object v3, LX/7fF;->CAPTURING:LX/7fF;

    if-ne v2, v3, :cond_1

    .line 1221205
    iget-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    const v3, 0x1b8682af

    invoke-static {v2, v3}, LX/0J2;->c(Landroid/hardware/Camera;I)V

    .line 1221206
    iget-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    .line 1221207
    :cond_1
    sget-object v2, LX/7fF;->STOPPED:LX/7fF;

    iput-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->r:LX/7fF;

    .line 1221208
    iget-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    .line 1221209
    iget-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    .line 1221210
    iget-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    const v3, -0x6ffe487f

    invoke-static {v2, v3}, LX/0J2;->a(Landroid/hardware/Camera;I)V

    .line 1221211
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    .line 1221212
    const/4 v2, 0x0

    iput v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->n:I

    .line 1221213
    const/4 v2, 0x0

    iput v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->o:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1221214
    :catch_0
    move-exception v0

    .line 1221215
    :goto_1
    sget-object v2, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a:Ljava/lang/Class;

    const-string v3, "Failed to stop mCamera"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 1221216
    goto :goto_0

    .line 1221217
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static e(Lorg/webrtc/videoengine/VideoCaptureAndroid;)V
    .locals 4

    .prologue
    .line 1221218
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->f:Landroid/graphics/SurfaceTexture;

    if-nez v0, :cond_1

    .line 1221219
    :cond_0
    :goto_0
    return-void

    .line 1221220
    :cond_1
    invoke-direct {p0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->f()I

    move-result v1

    .line 1221221
    iget v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->l:I

    if-eq v1, v0, :cond_0

    .line 1221222
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 1221223
    iput v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->l:I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1221224
    :catch_0
    move-exception v0

    .line 1221225
    sget-object v2, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a:Ljava/lang/Class;

    const-string v3, "Failed to set preview orientation"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1221226
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    const v2, 0x382bf48b

    invoke-static {v0, v2}, LX/0J2;->c(Landroid/hardware/Camera;I)V

    .line 1221227
    :try_start_1
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 1221228
    iput v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->l:I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1221229
    :goto_1
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    const v1, -0x1fbbd815

    invoke-static {v0, v1}, LX/0J2;->b(Landroid/hardware/Camera;I)V

    goto :goto_0

    .line 1221230
    :catch_1
    move-exception v0

    .line 1221231
    sget-object v1, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a:Ljava/lang/Class;

    const-string v2, "Failed to set display orientation after retrying"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private f()I
    .locals 2

    .prologue
    .line 1221232
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->u:Landroid/hardware/Camera$CameraInfo;

    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1221233
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->u:Landroid/hardware/Camera$CameraInfo;

    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iget v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->k:I

    add-int/2addr v0, v1

    rsub-int v0, v0, 0x2d0

    rem-int/lit16 v0, v0, 0x168

    .line 1221234
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->u:Landroid/hardware/Camera$CameraInfo;

    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/lit16 v0, v0, 0x168

    iget v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->k:I

    sub-int/2addr v0, v1

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0
.end method

.method private declared-synchronized startCapture(IIII)Z
    .locals 10
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1221235
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->p:Lorg/webrtc/videoengine/VideoCaptureAndroid$CameraThread;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->q:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 1221236
    :cond_0
    sget-object v0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a:Ljava/lang/Class;

    const-string v1, "Camera thread already started!"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v8

    .line 1221237
    :goto_0
    monitor-exit p0

    return v0

    .line 1221238
    :cond_1
    :try_start_1
    invoke-static {p0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->c(Lorg/webrtc/videoengine/VideoCaptureAndroid;)V

    .line 1221239
    sget-boolean v0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->d:Z

    if-eqz v0, :cond_2

    move v0, v7

    .line 1221240
    goto :goto_0

    .line 1221241
    :cond_2
    new-instance v0, Ljava/util/concurrent/Exchanger;

    invoke-direct {v0}, Ljava/util/concurrent/Exchanger;-><init>()V

    .line 1221242
    new-instance v1, Lorg/webrtc/videoengine/VideoCaptureAndroid$CameraThread;

    invoke-direct {v1, p0, v0}, Lorg/webrtc/videoengine/VideoCaptureAndroid$CameraThread;-><init>(Lorg/webrtc/videoengine/VideoCaptureAndroid;Ljava/util/concurrent/Exchanger;)V

    iput-object v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->p:Lorg/webrtc/videoengine/VideoCaptureAndroid$CameraThread;

    .line 1221243
    iget-object v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->p:Lorg/webrtc/videoengine/VideoCaptureAndroid$CameraThread;

    invoke-virtual {v1}, Lorg/webrtc/videoengine/VideoCaptureAndroid$CameraThread;->start()V

    .line 1221244
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->b(Ljava/util/concurrent/Exchanger;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->q:Landroid/os/Handler;

    .line 1221245
    new-instance v6, Ljava/util/concurrent/Exchanger;

    invoke-direct {v6}, Ljava/util/concurrent/Exchanger;-><init>()V

    .line 1221246
    iget-object v9, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->q:Landroid/os/Handler;

    new-instance v0, Lorg/webrtc/videoengine/VideoCaptureAndroid$1;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lorg/webrtc/videoengine/VideoCaptureAndroid$1;-><init>(Lorg/webrtc/videoengine/VideoCaptureAndroid;IIIILjava/util/concurrent/Exchanger;)V

    const v1, 0xb40a3ae

    invoke-static {v9, v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1221247
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v6, v0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->b(Ljava/util/concurrent/Exchanger;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1221248
    const/4 v0, 0x0

    invoke-static {v0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->c(Lorg/webrtc/videoengine/VideoCaptureAndroid;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v8

    .line 1221249
    goto :goto_0

    :cond_3
    move v0, v7

    .line 1221250
    goto :goto_0

    .line 1221251
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized stopCapture()Z
    .locals 4
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1221252
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->c(Lorg/webrtc/videoengine/VideoCaptureAndroid;)V

    .line 1221253
    sget-boolean v0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1221254
    const/4 v0, 0x1

    .line 1221255
    :goto_0
    monitor-exit p0

    return v0

    .line 1221256
    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/concurrent/Exchanger;

    invoke-direct {v0}, Ljava/util/concurrent/Exchanger;-><init>()V

    .line 1221257
    iget-object v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->q:Landroid/os/Handler;

    new-instance v2, Lorg/webrtc/videoengine/VideoCaptureAndroid$2;

    invoke-direct {v2, p0, v0}, Lorg/webrtc/videoengine/VideoCaptureAndroid$2;-><init>(Lorg/webrtc/videoengine/VideoCaptureAndroid;Ljava/util/concurrent/Exchanger;)V

    const v3, -0x70d4e5da

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1221258
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->b(Ljava/util/concurrent/Exchanger;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 1221259
    :try_start_2
    iget-object v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->p:Lorg/webrtc/videoengine/VideoCaptureAndroid$CameraThread;

    invoke-virtual {v1}, Lorg/webrtc/videoengine/VideoCaptureAndroid$CameraThread;->join()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1221260
    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->q:Landroid/os/Handler;

    .line 1221261
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->p:Lorg/webrtc/videoengine/VideoCaptureAndroid$CameraThread;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1221262
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1221263
    :catch_0
    move-exception v0

    .line 1221264
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final onError(ILandroid/hardware/Camera;)V
    .locals 5

    .prologue
    .line 1221265
    sget-object v0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a:Ljava/lang/Class;

    const-string v1, "Camera error detected : %d. Stopping capture"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1221266
    invoke-static {}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->b()V

    .line 1221267
    return-void
.end method

.method public final onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 8

    .prologue
    .line 1221268
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->p:Lorg/webrtc/videoengine/VideoCaptureAndroid$CameraThread;

    if-eq v0, v1, :cond_1

    .line 1221269
    sget-object v0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a:Ljava/lang/Class;

    const-string v1, "Camera callback not on mCamera thread?!?"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1221270
    :cond_0
    :goto_0
    return-void

    .line 1221271
    :cond_1
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 1221272
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    if-eq v0, p2, :cond_2

    .line 1221273
    sget-object v0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a:Ljava/lang/Class;

    const-string v1, "Unexpected mCamera in callback!"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 1221274
    :cond_2
    if-nez p1, :cond_3

    .line 1221275
    sget-object v0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a:Ljava/lang/Class;

    const-string v1, "Camera does not return data to onPreviewFrame"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 1221276
    :cond_3
    invoke-direct {p0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->c()I

    move-result v0

    .line 1221277
    iget v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->k:I

    if-eq v0, v1, :cond_4

    .line 1221278
    iget-object v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->q:Landroid/os/Handler;

    new-instance v2, Lorg/webrtc/videoengine/VideoCaptureAndroid$4;

    invoke-direct {v2, p0}, Lorg/webrtc/videoengine/VideoCaptureAndroid$4;-><init>(Lorg/webrtc/videoengine/VideoCaptureAndroid;)V

    const v3, -0x397b9c42

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1221279
    :cond_4
    iget v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->s:I

    if-ltz v1, :cond_5

    iget v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->k:I

    if-eq v0, v1, :cond_6

    .line 1221280
    :cond_5
    rsub-int v1, v0, 0x168

    rem-int/lit16 v1, v1, 0x168

    invoke-direct {p0, v1}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(I)I

    move-result v1

    iput v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->s:I

    .line 1221281
    iget-wide v2, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->w:J

    iget v1, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->s:I

    invoke-direct {p0, v2, v3, v1}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->OnOrientationChanged(JI)V

    .line 1221282
    :cond_6
    iput v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->k:I

    .line 1221283
    array-length v3, p1

    iget v4, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->n:I

    iget v5, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->o:I

    iget-wide v6, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->w:J

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->ProvideCameraFrame([BIIIJ)V

    .line 1221284
    iget-object v0, p0, Lorg/webrtc/videoengine/VideoCaptureAndroid;->m:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    goto :goto_0
.end method
