.class public final Lorg/webrtc/videoengine/MediaCodecVideoDecoder$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/concurrent/CountDownLatch;

.field public final synthetic b:Lorg/webrtc/videoengine/MediaCodecVideoDecoder;


# direct methods
.method public constructor <init>(Lorg/webrtc/videoengine/MediaCodecVideoDecoder;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 1220400
    iput-object p1, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder$1;->b:Lorg/webrtc/videoengine/MediaCodecVideoDecoder;

    iput-object p2, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder$1;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1220394
    :try_start_0
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder$1;->b:Lorg/webrtc/videoengine/MediaCodecVideoDecoder;

    iget-object v0, v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 1220395
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder$1;->b:Lorg/webrtc/videoengine/MediaCodecVideoDecoder;

    iget-object v0, v0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1220396
    :goto_0
    iget-object v0, p0, Lorg/webrtc/videoengine/MediaCodecVideoDecoder$1;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 1220397
    return-void

    .line 1220398
    :catch_0
    move-exception v0

    .line 1220399
    const-string v1, "MediaCodecVideoDecoder"

    const-string v2, "Media decoder release failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
