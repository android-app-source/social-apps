.class public final Lcom/facebook/feed/viewstate/UnseenStoryManager$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/1dr;


# direct methods
.method public constructor <init>(LX/1dr;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1288403
    iput-object p1, p0, Lcom/facebook/feed/viewstate/UnseenStoryManager$1;->b:LX/1dr;

    iput-object p2, p0, Lcom/facebook/feed/viewstate/UnseenStoryManager$1;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 1288404
    iget-object v0, p0, Lcom/facebook/feed/viewstate/UnseenStoryManager$1;->b:LX/1dr;

    iget-object v1, p0, Lcom/facebook/feed/viewstate/UnseenStoryManager$1;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1288405
    invoke-static {v1}, LX/1dr;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v2

    .line 1288406
    iget-object v4, v0, LX/1dr;->a:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1288407
    :goto_0
    iget-object v3, v0, LX/1dr;->b:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1288408
    return-void

    .line 1288409
    :cond_0
    iget-object v4, v0, LX/1dr;->a:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 1288410
    const/4 v5, 0x0

    move v6, v5

    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v6, v5, :cond_2

    .line 1288411
    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/82e;

    .line 1288412
    iget-object v7, v5, LX/82e;->d:LX/82d;

    sget-object v8, LX/82d;->TRANSITION_FINISHED:LX/82d;

    if-eq v7, v8, :cond_1

    iget-object v7, v5, LX/82e;->d:LX/82d;

    sget-object v8, LX/82d;->TRANSITION_RUNNING:LX/82d;

    if-ne v7, v8, :cond_3

    .line 1288413
    :cond_1
    :goto_2
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 1288414
    :cond_2
    iget-object v4, v0, LX/1dr;->a:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1288415
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    iput-wide v7, v5, LX/82e;->a:J

    .line 1288416
    sget-object v7, LX/82d;->TRANSITION_RUNNING:LX/82d;

    iput-object v7, v5, LX/82e;->d:LX/82d;

    .line 1288417
    invoke-virtual {v5}, LX/82e;->invalidateSelf()V

    goto :goto_2
.end method
