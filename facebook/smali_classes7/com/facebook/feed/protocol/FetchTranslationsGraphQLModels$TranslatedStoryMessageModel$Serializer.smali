.class public final Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1285801
    const-class v0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;

    new-instance v1, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1285802
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1285803
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1285804
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1285805
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1285806
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1285807
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1285808
    if-eqz v2, :cond_0

    .line 1285809
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1285810
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1285811
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1285812
    if-eqz v2, :cond_1

    .line 1285813
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1285814
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1285815
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1285816
    if-eqz v2, :cond_2

    .line 1285817
    const-string p0, "translation"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1285818
    invoke-static {v1, v2, p1, p2}, LX/59Z;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1285819
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1285820
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1285821
    check-cast p1, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel$Serializer;->a(Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;LX/0nX;LX/0my;)V

    return-void
.end method
