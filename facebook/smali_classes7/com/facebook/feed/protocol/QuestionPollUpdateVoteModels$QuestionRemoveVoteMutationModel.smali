.class public final Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xf9f6730
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1287273
    const-class v0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1287272
    const-class v0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1287251
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1287252
    return-void
.end method

.method private a()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getQuestion"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1287270
    iget-object v0, p0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel;->e:Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel;->e:Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;

    .line 1287271
    iget-object v0, p0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel;->e:Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1287264
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1287265
    invoke-direct {p0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel;->a()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1287266
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1287267
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1287268
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1287269
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1287256
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1287257
    invoke-direct {p0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel;->a()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1287258
    invoke-direct {p0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel;->a()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;

    .line 1287259
    invoke-direct {p0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel;->a()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1287260
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel;

    .line 1287261
    iput-object v0, v1, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel;->e:Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;

    .line 1287262
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1287263
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1287253
    new-instance v0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionRemoveVoteMutationModel;-><init>()V

    .line 1287254
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1287255
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1287250
    const v0, 0xbcd7965

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1287249
    const v0, 0x37bb4939

    return v0
.end method
