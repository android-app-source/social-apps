.class public final Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1285012
    const-class v0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;

    new-instance v1, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1285013
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1285014
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1285015
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1285016
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1285017
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1285018
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1285019
    if-eqz v2, :cond_0

    .line 1285020
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1285021
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1285022
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1285023
    if-eqz v2, :cond_1

    .line 1285024
    const-string p0, "cache_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1285025
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1285026
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1285027
    if-eqz v2, :cond_2

    .line 1285028
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1285029
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1285030
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1285031
    if-eqz v2, :cond_3

    .line 1285032
    const-string p0, "suggested_users"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1285033
    invoke-static {v1, v2, p1, p2}, LX/83E;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1285034
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1285035
    if-eqz v2, :cond_4

    .line 1285036
    const-string p0, "target_group"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1285037
    invoke-static {v1, v2, p1}, LX/813;->a(LX/15i;ILX/0nX;)V

    .line 1285038
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1285039
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1285040
    check-cast p1, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$Serializer;->a(Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
