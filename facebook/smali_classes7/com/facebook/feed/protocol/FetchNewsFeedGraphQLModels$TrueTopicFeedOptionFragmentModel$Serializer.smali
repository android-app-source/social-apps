.class public final Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1284500
    const-class v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;

    new-instance v1, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1284501
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1284499
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1284468
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1284469
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1284470
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1284471
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1284472
    if-eqz v2, :cond_0

    .line 1284473
    const-string p0, "icon_dominant_color"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1284474
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1284475
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1284476
    if-eqz v2, :cond_1

    .line 1284477
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1284478
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1284479
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1284480
    if-eqz v2, :cond_2

    .line 1284481
    const-string p0, "is_checked"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1284482
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1284483
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1284484
    if-eqz v2, :cond_4

    .line 1284485
    const-string p0, "option_icon"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1284486
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1284487
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1284488
    if-eqz p0, :cond_3

    .line 1284489
    const-string p2, "uri"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1284490
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1284491
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1284492
    :cond_4
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1284493
    if-eqz v2, :cond_5

    .line 1284494
    const-string p0, "topic_feed_option"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1284495
    invoke-static {v1, v2, p1}, LX/80r;->a(LX/15i;ILX/0nX;)V

    .line 1284496
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1284497
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1284498
    check-cast p1, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$Serializer;->a(Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
