.class public final Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1282271
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1282272
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1282269
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1282270
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1282311
    if-nez p1, :cond_0

    .line 1282312
    :goto_0
    return v0

    .line 1282313
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1282314
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1282315
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1282316
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1282317
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1282318
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1282319
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1282320
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v1

    .line 1282321
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1282322
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1282323
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1282324
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1282325
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1282326
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1282327
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1282328
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1282329
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x14cb680c -> :sswitch_1
        0x182a8f86 -> :sswitch_0
        0x313555ab -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1282310
    new-instance v0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1282307
    sparse-switch p0, :sswitch_data_0

    .line 1282308
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1282309
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x14cb680c -> :sswitch_0
        0x182a8f86 -> :sswitch_0
        0x313555ab -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1282306
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1282304
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;->b(I)V

    .line 1282305
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1282299
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1282300
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1282301
    :cond_0
    iput-object p1, p0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1282302
    iput p2, p0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;->b:I

    .line 1282303
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1282298
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1282297
    new-instance v0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1282294
    iget v0, p0, LX/1vt;->c:I

    .line 1282295
    move v0, v0

    .line 1282296
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1282291
    iget v0, p0, LX/1vt;->c:I

    .line 1282292
    move v0, v0

    .line 1282293
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1282288
    iget v0, p0, LX/1vt;->b:I

    .line 1282289
    move v0, v0

    .line 1282290
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1282285
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1282286
    move-object v0, v0

    .line 1282287
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1282276
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1282277
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1282278
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1282279
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1282280
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1282281
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1282282
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1282283
    invoke-static {v3, v9, v2}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1282284
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1282273
    iget v0, p0, LX/1vt;->c:I

    .line 1282274
    move v0, v0

    .line 1282275
    return v0
.end method
