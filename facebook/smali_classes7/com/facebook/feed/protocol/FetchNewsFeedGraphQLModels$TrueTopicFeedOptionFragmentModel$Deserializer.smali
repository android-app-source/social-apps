.class public final Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1284413
    const-class v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;

    new-instance v1, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1284414
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1284415
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1284416
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1284417
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1284418
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1284419
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1284420
    :goto_0
    move v1, v2

    .line 1284421
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1284422
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1284423
    new-instance v1, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;

    invoke-direct {v1}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;-><init>()V

    .line 1284424
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1284425
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1284426
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1284427
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1284428
    :cond_0
    return-object v1

    .line 1284429
    :cond_1
    const-string v10, "is_checked"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1284430
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v6, v1

    move v1, v3

    .line 1284431
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_7

    .line 1284432
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1284433
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1284434
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, p0, :cond_2

    if-eqz v9, :cond_2

    .line 1284435
    const-string v10, "icon_dominant_color"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1284436
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1284437
    :cond_3
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1284438
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1284439
    :cond_4
    const-string v10, "option_icon"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1284440
    const/4 v9, 0x0

    .line 1284441
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v10, :cond_d

    .line 1284442
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1284443
    :goto_2
    move v5, v9

    .line 1284444
    goto :goto_1

    .line 1284445
    :cond_5
    const-string v10, "topic_feed_option"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1284446
    invoke-static {p1, v0}, LX/80r;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1284447
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1284448
    :cond_7
    const/4 v9, 0x5

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1284449
    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1284450
    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1284451
    if-eqz v1, :cond_8

    .line 1284452
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v6}, LX/186;->a(IZ)V

    .line 1284453
    :cond_8
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1284454
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1284455
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto/16 :goto_1

    .line 1284456
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1284457
    :cond_b
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_c

    .line 1284458
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1284459
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1284460
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v10, :cond_b

    .line 1284461
    const-string p0, "uri"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 1284462
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 1284463
    :cond_c
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1284464
    invoke-virtual {v0, v9, v5}, LX/186;->b(II)V

    .line 1284465
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    goto :goto_2

    :cond_d
    move v5, v9

    goto :goto_3
.end method
