.class public final Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x67aa8b1c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1282051
    const-class v0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1282050
    const-class v0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1282017
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1282018
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1282041
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1282042
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1282043
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x240e3aa4

    invoke-static {v2, v1, v3}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1282044
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1282045
    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1282046
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1282047
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->g:I

    invoke-virtual {p1, v0, v1, v4}, LX/186;->a(III)V

    .line 1282048
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1282049
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1282039
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->e:Ljava/util/List;

    .line 1282040
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1282024
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1282025
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1282026
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1282027
    if-eqz v1, :cond_2

    .line 1282028
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;

    .line 1282029
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1282030
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1282031
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x240e3aa4

    invoke-static {v2, v0, v3}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1282032
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1282033
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;

    .line 1282034
    iput v3, v0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->f:I

    move-object v1, v0

    .line 1282035
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1282036
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    .line 1282037
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object p0, v1

    .line 1282038
    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1282052
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1282053
    const/4 v0, 0x1

    const v1, -0x240e3aa4

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->f:I

    .line 1282054
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->g:I

    .line 1282055
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1282021
    new-instance v0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;-><init>()V

    .line 1282022
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1282023
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1282020
    const v0, 0x24f839aa

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1282019
    const v0, 0x5da46d71

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1282015
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1282016
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 1282013
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1282014
    iget v0, p0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;->g:I

    return v0
.end method
