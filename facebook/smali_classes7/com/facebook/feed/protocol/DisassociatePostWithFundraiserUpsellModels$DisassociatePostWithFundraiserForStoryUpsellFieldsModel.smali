.class public final Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x55f2507b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1281836
    const-class v0, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1281837
    const-class v0, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1281817
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1281818
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1281834
    iget-object v0, p0, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;->e:Ljava/lang/String;

    .line 1281835
    iget-object v0, p0, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$StoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1281832
    iget-object v0, p0, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;->f:Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$StoryModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$StoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$StoryModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;->f:Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$StoryModel;

    .line 1281833
    iget-object v0, p0, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;->f:Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$StoryModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1281838
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1281839
    invoke-direct {p0}, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1281840
    invoke-direct {p0}, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;->j()Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$StoryModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1281841
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1281842
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1281843
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1281844
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1281845
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1281824
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1281825
    invoke-direct {p0}, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;->j()Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$StoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1281826
    invoke-direct {p0}, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;->j()Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$StoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$StoryModel;

    .line 1281827
    invoke-direct {p0}, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;->j()Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$StoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1281828
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;

    .line 1281829
    iput-object v0, v1, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;->f:Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$StoryModel;

    .line 1281830
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1281831
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1281821
    new-instance v0, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;-><init>()V

    .line 1281822
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1281823
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1281820
    const v0, -0x52f43828

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1281819
    const v0, 0x46974d53

    return v0
.end method
