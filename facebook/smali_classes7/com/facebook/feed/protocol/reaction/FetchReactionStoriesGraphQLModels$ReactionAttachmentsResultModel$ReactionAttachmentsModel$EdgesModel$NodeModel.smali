.class public final Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x13653c49
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1287764
    const-class v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1287765
    const-class v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1287766
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1287767
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1287768
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1287769
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1287770
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1287771
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1287772
    invoke-direct {p0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1287773
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1287774
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1287775
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1287776
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1287777
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1287778
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1287779
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1287780
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1287781
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1287782
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1287783
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;

    .line 1287784
    iput-object v0, v1, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1287785
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1287786
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1287787
    iget-object v0, p0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1287788
    iget-object v0, p0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;->f:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1287789
    new-instance v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;-><init>()V

    .line 1287790
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1287791
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1287792
    const v0, -0x15fd53a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1287793
    const v0, -0x6400c191

    return v0
.end method
