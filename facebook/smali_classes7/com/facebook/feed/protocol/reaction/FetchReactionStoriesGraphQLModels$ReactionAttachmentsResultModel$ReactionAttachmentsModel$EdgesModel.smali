.class public final Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x696b7339
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1287802
    const-class v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1287801
    const-class v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1287826
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1287827
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1287818
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1287819
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1287820
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1287821
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1287822
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1287823
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1287824
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1287825
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1287810
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1287811
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1287812
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;

    .line 1287813
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1287814
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;

    .line 1287815
    iput-object v0, v1, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->e:Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;

    .line 1287816
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1287817
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1287828
    iget-object v0, p0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->e:Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->e:Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;

    .line 1287829
    iget-object v0, p0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->e:Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel$NodeModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1287807
    new-instance v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;-><init>()V

    .line 1287808
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1287809
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1287806
    const v0, 0x2da38be5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1287805
    const v0, -0x6449203f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1287803
    iget-object v0, p0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->f:Ljava/lang/String;

    .line 1287804
    iget-object v0, p0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel$EdgesModel;->f:Ljava/lang/String;

    return-object v0
.end method
