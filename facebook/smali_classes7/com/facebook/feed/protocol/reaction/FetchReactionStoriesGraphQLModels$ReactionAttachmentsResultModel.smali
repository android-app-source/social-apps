.class public final Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x17b7aa95
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1287884
    const-class v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1287885
    const-class v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1287886
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1287887
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1287888
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1287889
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;->a()Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1287890
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1287891
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1287892
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1287893
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1287894
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1287895
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;->a()Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1287896
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;->a()Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;

    .line 1287897
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;->a()Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1287898
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;

    .line 1287899
    iput-object v0, v1, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;->e:Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;

    .line 1287900
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1287901
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1287902
    iget-object v0, p0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;->e:Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;->e:Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;

    .line 1287903
    iget-object v0, p0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;->e:Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel$ReactionAttachmentsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1287904
    new-instance v0, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/reaction/FetchReactionStoriesGraphQLModels$ReactionAttachmentsResultModel;-><init>()V

    .line 1287905
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1287906
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1287907
    const v0, 0x6e300364

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1287908
    const v0, -0x3334afd4

    return v0
.end method
