.class public final Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x60db43dd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1285859
    const-class v0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1285858
    const-class v0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1285856
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1285857
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1285853
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1285854
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1285855
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1285851
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->f:Ljava/lang/String;

    .line 1285852
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1285841
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1285842
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1285843
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1285844
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->j()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1285845
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1285846
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1285847
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1285848
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1285849
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1285850
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1285833
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1285834
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->j()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1285835
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->j()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    .line 1285836
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->j()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1285837
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;

    .line 1285838
    iput-object v0, v1, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    .line 1285839
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1285840
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1285860
    new-instance v0, LX/81P;

    invoke-direct {v0, p1}, LX/81P;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1285824
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1285822
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1285823
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1285825
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1285826
    new-instance v0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;-><init>()V

    .line 1285827
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1285828
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1285829
    const v0, 0x7ce65418

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1285832
    const v0, 0x252222

    return v0
.end method

.method public final j()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1285830
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    .line 1285831
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchTranslationsGraphQLModels$TranslatedStoryMessageModel;->g:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    return-object v0
.end method
