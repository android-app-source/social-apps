.class public final Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7837104e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$TopicFeedOptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1284619
    const-class v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1284618
    const-class v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1284616
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1284617
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1284610
    iput-boolean p1, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->g:Z

    .line 1284611
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1284612
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1284613
    if-eqz v0, :cond_0

    .line 1284614
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1284615
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1284608
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->e:Ljava/lang/String;

    .line 1284609
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1284606
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->f:Ljava/lang/String;

    .line 1284607
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Z
    .locals 2

    .prologue
    .line 1284604
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1284605
    iget-boolean v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->g:Z

    return v0
.end method

.method private m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOptionIcon"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1284602
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1284603
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private n()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$TopicFeedOptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1284600
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->i:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$TopicFeedOptionModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$TopicFeedOptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$TopicFeedOptionModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->i:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$TopicFeedOptionModel;

    .line 1284601
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->i:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$TopicFeedOptionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1284552
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1284553
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1284554
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1284555
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->m()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x2dacac9f

    invoke-static {v3, v2, v4}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1284556
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->n()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$TopicFeedOptionModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1284557
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1284558
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1284559
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1284560
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1284561
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1284562
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1284563
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1284564
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1284585
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1284586
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1284587
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2dacac9f

    invoke-static {v2, v0, v3}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1284588
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1284589
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;

    .line 1284590
    iput v3, v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->h:I

    move-object v1, v0

    .line 1284591
    :cond_0
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->n()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$TopicFeedOptionModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1284592
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->n()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$TopicFeedOptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$TopicFeedOptionModel;

    .line 1284593
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->n()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$TopicFeedOptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1284594
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;

    .line 1284595
    iput-object v0, v1, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->i:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel$TopicFeedOptionModel;

    .line 1284596
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1284597
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1284598
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 1284599
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1284584
    new-instance v0, LX/80p;

    invoke-direct {v0, p1}, LX/80p;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1284583
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1284579
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1284580
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->g:Z

    .line 1284581
    const/4 v0, 0x3

    const v1, -0x2dacac9f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->h:I

    .line 1284582
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1284573
    const-string v0, "is_checked"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1284574
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1284575
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1284576
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 1284577
    :goto_0
    return-void

    .line 1284578
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1284570
    const-string v0, "is_checked"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1284571
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;->a(Z)V

    .line 1284572
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1284567
    new-instance v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$TrueTopicFeedOptionFragmentModel;-><init>()V

    .line 1284568
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1284569
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1284566
    const v0, -0x3161b799

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1284565
    const v0, -0x201e1d4c

    return v0
.end method
