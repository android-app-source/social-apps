.class public final Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1285906
    const-class v0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;

    new-instance v1, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1285907
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1285875
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1285876
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1285877
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1285878
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1285879
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1285880
    :goto_0
    move v1, v2

    .line 1285881
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1285882
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1285883
    new-instance v1, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;

    invoke-direct {v1}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;-><init>()V

    .line 1285884
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1285885
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1285886
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1285887
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1285888
    :cond_0
    return-object v1

    .line 1285889
    :cond_1
    const-string p0, "play_count"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1285890
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v4, v1

    move v1, v3

    .line 1285891
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_6

    .line 1285892
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1285893
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1285894
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v7, :cond_2

    .line 1285895
    const-string p0, "__type__"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    const-string p0, "__typename"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1285896
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v6

    goto :goto_1

    .line 1285897
    :cond_4
    const-string p0, "feedback"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1285898
    invoke-static {p1, v0}, LX/81Z;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1285899
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1285900
    :cond_6
    const/4 v7, 0x3

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1285901
    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1285902
    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1285903
    if-eqz v1, :cond_7

    .line 1285904
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4, v2}, LX/186;->a(III)V

    .line 1285905
    :cond_7
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto :goto_1
.end method
