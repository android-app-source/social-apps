.class public final Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x623c1b89
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1281476
    const-class v0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1281505
    const-class v0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1281503
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1281504
    return-void
.end method

.method private j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1281501
    iget-object v0, p0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    .line 1281502
    iget-object v0, p0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1281493
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1281494
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->c(Ljava/util/List;)I

    move-result v0

    .line 1281495
    invoke-direct {p0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1281496
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1281497
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1281498
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1281499
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1281500
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1281491
    iget-object v0, p0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;->e:Ljava/util/List;

    .line 1281492
    iget-object v0, p0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1281483
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1281484
    invoke-direct {p0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1281485
    invoke-direct {p0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    .line 1281486
    invoke-direct {p0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1281487
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;

    .line 1281488
    iput-object v0, v1, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;->f:Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    .line 1281489
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1281490
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic b()LX/57w;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1281482
    invoke-direct {p0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1281479
    new-instance v0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;-><init>()V

    .line 1281480
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1281481
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1281478
    const v0, 0x4e06f579    # 5.6605856E8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1281477
    const v0, -0x4b900828

    return v0
.end method
