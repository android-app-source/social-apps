.class public final Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x16d93cc0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$TargetGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1285139
    const-class v0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1285138
    const-class v0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1285136
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1285137
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1285133
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1285134
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1285135
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1285131
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->f:Ljava/lang/String;

    .line 1285132
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1285129
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->g:Ljava/lang/String;

    .line 1285130
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$TargetGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1285127
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->i:Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$TargetGroupModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$TargetGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$TargetGroupModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->i:Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$TargetGroupModel;

    .line 1285128
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->i:Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$TargetGroupModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1285088
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1285089
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1285090
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1285091
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1285092
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->j()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1285093
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->n()Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$TargetGroupModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1285094
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1285095
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1285096
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1285097
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1285098
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1285099
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1285100
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1285101
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1285114
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1285115
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->j()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1285116
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->j()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;

    .line 1285117
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->j()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1285118
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;

    .line 1285119
    iput-object v0, v1, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->h:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;

    .line 1285120
    :cond_0
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->n()Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$TargetGroupModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1285121
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->n()Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$TargetGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$TargetGroupModel;

    .line 1285122
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->n()Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$TargetGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1285123
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;

    .line 1285124
    iput-object v0, v1, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->i:Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel$TargetGroupModel;

    .line 1285125
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1285126
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1285113
    new-instance v0, LX/810;

    invoke-direct {v0, p1}, LX/810;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1285112
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1285110
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1285111
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1285109
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1285106
    new-instance v0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;-><init>()V

    .line 1285107
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1285108
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1285105
    const v0, 0x1e8cd3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1285104
    const v0, 0x252222

    return v0
.end method

.method public final j()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1285102
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->h:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->h:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;

    .line 1285103
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteQueryModel;->h:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel;

    return-object v0
.end method
