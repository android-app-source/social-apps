.class public final Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x206e253e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1286353
    const-class v0, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1286352
    const-class v0, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1286329
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1286330
    return-void
.end method

.method private a()Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1286350
    iget-object v0, p0, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel;->e:Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel;->e:Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;

    .line 1286351
    iget-object v0, p0, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel;->e:Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1286344
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1286345
    invoke-direct {p0}, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel;->a()Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1286346
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1286347
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1286348
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1286349
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1286336
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1286337
    invoke-direct {p0}, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel;->a()Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1286338
    invoke-direct {p0}, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel;->a()Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;

    .line 1286339
    invoke-direct {p0}, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel;->a()Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1286340
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel;

    .line 1286341
    iput-object v0, v1, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel;->e:Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;

    .line 1286342
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1286343
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1286333
    new-instance v0, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel;-><init>()V

    .line 1286334
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1286335
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1286332
    const v0, 0x5839df64

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1286331
    const v0, -0x4c62c188

    return v0
.end method
