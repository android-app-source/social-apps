.class public final Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5fbfe9cc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1286161
    const-class v0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1286160
    const-class v0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1286158
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1286159
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1286155
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1286156
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1286157
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1286146
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1286147
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1286148
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->a()Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1286149
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1286150
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1286151
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1286152
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->g:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1286153
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1286154
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1286132
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1286133
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->a()Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1286134
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->a()Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;

    .line 1286135
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->a()Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1286136
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;

    .line 1286137
    iput-object v0, v1, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->f:Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;

    .line 1286138
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1286139
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1286145
    new-instance v0, LX/81V;

    invoke-direct {v0, p1}, LX/81V;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1286162
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->f:Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->f:Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;

    .line 1286163
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->f:Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1286142
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1286143
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->g:I

    .line 1286144
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1286140
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1286141
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1286131
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1286128
    new-instance v0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;-><init>()V

    .line 1286129
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1286130
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1286127
    const v0, 0x6d372ecf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1286126
    const v0, 0x252222

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 1286124
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1286125
    iget v0, p0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->g:I

    return v0
.end method
