.class public final Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2bf6751a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1281645
    const-class v0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1281644
    const-class v0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1281642
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1281643
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1281640
    iget-object v0, p0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;->e:Ljava/lang/String;

    .line 1281641
    iget-object v0, p0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1281632
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1281633
    invoke-direct {p0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1281634
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;->a()Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1281635
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1281636
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1281637
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1281638
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1281639
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1281624
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1281625
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;->a()Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1281626
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;->a()Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;

    .line 1281627
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;->a()Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1281628
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;

    .line 1281629
    iput-object v0, v1, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;->f:Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;

    .line 1281630
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1281631
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1281617
    iget-object v0, p0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;->f:Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;->f:Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;

    .line 1281618
    iget-object v0, p0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;->f:Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1281621
    new-instance v0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;-><init>()V

    .line 1281622
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1281623
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1281620
    const v0, 0x681a47be

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1281619
    const v0, 0x24dfec5d

    return v0
.end method
