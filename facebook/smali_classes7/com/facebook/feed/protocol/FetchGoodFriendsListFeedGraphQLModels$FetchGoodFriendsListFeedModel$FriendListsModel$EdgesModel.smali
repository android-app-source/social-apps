.class public final Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x31dba47b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1283404
    const-class v0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1283422
    const-class v0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1283420
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1283421
    return-void
.end method

.method private a()Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1283418
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel;->e:Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel;->e:Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel;

    .line 1283419
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel;->e:Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1283423
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1283424
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel;->a()Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1283425
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1283426
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1283427
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1283428
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1283410
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1283411
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel;->a()Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1283412
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel;->a()Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel;

    .line 1283413
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel;->a()Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1283414
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel;

    .line 1283415
    iput-object v0, v1, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel;->e:Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel;

    .line 1283416
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1283417
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1283407
    new-instance v0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel;-><init>()V

    .line 1283408
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1283409
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1283406
    const v0, -0x739a56ac

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1283405
    const v0, -0x65f3ae2c

    return v0
.end method
