.class public final Lcom/facebook/feed/protocol/FetchPaginatedGroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feed/protocol/FetchPaginatedGroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1285235
    const-class v0, Lcom/facebook/feed/protocol/FetchPaginatedGroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinQueryModel;

    new-instance v1, Lcom/facebook/feed/protocol/FetchPaginatedGroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1285236
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1285216
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feed/protocol/FetchPaginatedGroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1285217
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1285218
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1285219
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1285220
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1285221
    if-eqz v2, :cond_0

    .line 1285222
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1285223
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1285224
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1285225
    if-eqz v2, :cond_1

    .line 1285226
    const-string v3, "all_groups"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1285227
    invoke-static {v1, v2, p1, p2}, LX/830;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1285228
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1285229
    if-eqz v2, :cond_2

    .line 1285230
    const-string v3, "gap_rule"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1285231
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1285232
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1285233
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1285234
    check-cast p1, Lcom/facebook/feed/protocol/FetchPaginatedGroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feed/protocol/FetchPaginatedGroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinQueryModel$Serializer;->a(Lcom/facebook/feed/protocol/FetchPaginatedGroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
