.class public final Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x40e24b9f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1285612
    const-class v0, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1285611
    const-class v0, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1285609
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1285610
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1285606
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1285607
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1285608
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1285597
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1285598
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1285599
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;->a()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1285600
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1285601
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1285602
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1285603
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;->g:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1285604
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1285605
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1285589
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1285590
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;->a()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1285591
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;->a()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;

    .line 1285592
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;->a()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1285593
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;

    .line 1285594
    iput-object v0, v1, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;->f:Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;

    .line 1285595
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1285596
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1285575
    new-instance v0, LX/81H;

    invoke-direct {v0, p1}, LX/81H;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1285587
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;->f:Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;->f:Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;

    .line 1285588
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;->f:Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1285584
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1285585
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;->g:I

    .line 1285586
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1285582
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1285583
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1285581
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1285578
    new-instance v0, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/FetchSaleGroupsNearYouGraphQLModels$SaleGroupsNearYouQueryModel;-><init>()V

    .line 1285579
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1285580
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1285577
    const v0, -0x68f9976c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1285576
    const v0, 0x252222

    return v0
.end method
