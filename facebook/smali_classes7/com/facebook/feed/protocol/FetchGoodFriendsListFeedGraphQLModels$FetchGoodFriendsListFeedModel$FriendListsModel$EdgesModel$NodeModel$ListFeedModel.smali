.class public final Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1d74b5c7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel$ListFeedEdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1283356
    const-class v0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1283355
    const-class v0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1283353
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1283354
    return-void
.end method

.method private a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel$ListFeedEdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1283351
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel$ListFeedEdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;->e:Ljava/util/List;

    .line 1283352
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1283357
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1283358
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1283343
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1283344
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1283345
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1283346
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1283347
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1283348
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1283349
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1283350
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1283330
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1283331
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1283332
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1283333
    if-eqz v1, :cond_2

    .line 1283334
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;

    .line 1283335
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1283336
    :goto_0
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1283337
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1283338
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1283339
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;

    .line 1283340
    iput-object v0, v1, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1283341
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1283342
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1283327
    new-instance v0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel$EdgesModel$NodeModel$ListFeedModel;-><init>()V

    .line 1283328
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1283329
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1283326
    const v0, -0x16a7fdbb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1283325
    const v0, -0x35eadd48    # -2443438.0f

    return v0
.end method
