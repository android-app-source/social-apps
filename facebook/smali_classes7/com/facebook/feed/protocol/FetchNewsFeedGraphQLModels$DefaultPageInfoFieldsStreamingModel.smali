.class public final Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/80n;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x36eecc3f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1284361
    const-class v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1284360
    const-class v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1284358
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1284359
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1284348
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1284349
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1284350
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1284351
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1284352
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1284353
    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->f:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 1284354
    const/4 v0, 0x2

    iget-boolean v2, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->g:Z

    invoke-virtual {p1, v0, v2}, LX/186;->a(IZ)V

    .line 1284355
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1284356
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1284357
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1284345
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1284346
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1284347
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1284343
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->e:Ljava/lang/String;

    .line 1284344
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1284339
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1284340
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->f:Z

    .line 1284341
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->g:Z

    .line 1284342
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1284336
    new-instance v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;-><init>()V

    .line 1284337
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1284338
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1284334
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1284335
    iget-boolean v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->f:Z

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1284328
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1284329
    iget-boolean v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->g:Z

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1284332
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->h:Ljava/lang/String;

    .line 1284333
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1284331
    const v0, -0x43c8108a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1284330
    const v0, 0x370fbffd

    return v0
.end method
