.class public final Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5300b970
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1287208
    const-class v0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1287207
    const-class v0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1287205
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1287206
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1287197
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1287198
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1287199
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->k()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1287200
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1287201
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1287202
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1287203
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1287204
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1287189
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1287190
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->k()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1287191
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->k()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;

    .line 1287192
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->k()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1287193
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;

    .line 1287194
    iput-object v0, v1, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->f:Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;

    .line 1287195
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1287196
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1287209
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1287186
    new-instance v0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;-><init>()V

    .line 1287187
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1287188
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1287185
    const v0, 0x72cc56ab

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1287180
    const v0, -0x41a35ffa

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1287183
    iget-object v0, p0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->e:Ljava/lang/String;

    .line 1287184
    iget-object v0, p0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOptions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1287181
    iget-object v0, p0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->f:Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->f:Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;

    .line 1287182
    iget-object v0, p0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->f:Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;

    return-object v0
.end method
