.class public final Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x55570df5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel$ActionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1282477
    const-class v0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1282478
    const-class v0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1282479
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1282480
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1282483
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1282484
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1282485
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x14cb680c

    invoke-static {v2, v1, v3}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1282486
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x313555ab

    invoke-static {v3, v2, v4}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1282487
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1282488
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1282489
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1282490
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1282491
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1282492
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1282493
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel$ActionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1282481
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel$ActionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->e:Ljava/util/List;

    .line 1282482
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1282456
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1282457
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1282458
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1282459
    if-eqz v1, :cond_3

    .line 1282460
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;

    .line 1282461
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1282462
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1282463
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x14cb680c

    invoke-static {v2, v0, v3}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1282464
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1282465
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;

    .line 1282466
    iput v3, v0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->f:I

    move-object v1, v0

    .line 1282467
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1282468
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x313555ab

    invoke-static {v2, v0, v3}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1282469
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1282470
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;

    .line 1282471
    iput v3, v0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->g:I

    move-object v1, v0

    .line 1282472
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1282473
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    .line 1282474
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1282475
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 1282476
    goto :goto_1

    :cond_3
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1282442
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1282443
    const/4 v0, 0x1

    const v1, -0x14cb680c

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->f:I

    .line 1282444
    const/4 v0, 0x2

    const v1, 0x313555ab

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->g:I

    .line 1282445
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->h:Z

    .line 1282446
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1282453
    new-instance v0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;-><init>()V

    .line 1282454
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1282455
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1282452
    const v0, 0x798b8807

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1282451
    const v0, 0x459d96cd

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCurrentAction"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1282449
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1282450
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFeedbackText"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1282447
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1282448
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
