.class public final Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1281753
    const-class v0, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;

    new-instance v1, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1281754
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1281755
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1281756
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1281757
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1281758
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1281759
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1281760
    if-eqz v2, :cond_0

    .line 1281761
    const-string p0, "client_mutation_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1281762
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1281763
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1281764
    if-eqz v2, :cond_1

    .line 1281765
    const-string p0, "story"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1281766
    invoke-static {v1, v2, p1}, LX/806;->a(LX/15i;ILX/0nX;)V

    .line 1281767
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1281768
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1281769
    check-cast p1, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel$Serializer;->a(Lcom/facebook/feed/protocol/DisassociatePostWithFundraiserUpsellModels$DisassociatePostWithFundraiserForStoryUpsellFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
