.class public final Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1286122
    const-class v0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;

    new-instance v1, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1286123
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1286103
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1286105
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1286106
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1286107
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1286108
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1286109
    if-eqz v2, :cond_0

    .line 1286110
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1286111
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1286112
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1286113
    if-eqz v2, :cond_1

    .line 1286114
    const-string v3, "feedback"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1286115
    invoke-static {v1, v2, p1, p2}, LX/81Z;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1286116
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1286117
    if-eqz v2, :cond_2

    .line 1286118
    const-string v3, "play_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1286119
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1286120
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1286121
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1286104
    check-cast p1, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$Serializer;->a(Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
