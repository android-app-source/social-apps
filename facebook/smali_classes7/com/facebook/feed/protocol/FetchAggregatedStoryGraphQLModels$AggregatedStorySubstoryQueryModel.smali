.class public final Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x12142c37
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1282136
    const-class v0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1282135
    const-class v0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1282133
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1282134
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1282130
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1282131
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1282132
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1282122
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1282123
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1282124
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;->a()Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1282125
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1282126
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1282127
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1282128
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1282129
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1282114
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1282115
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;->a()Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1282116
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;->a()Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;

    .line 1282117
    invoke-virtual {p0}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;->a()Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1282118
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;

    .line 1282119
    iput-object v0, v1, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;->f:Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;

    .line 1282120
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1282121
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1282137
    new-instance v0, LX/80A;

    invoke-direct {v0, p1}, LX/80A;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllSubstories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1282112
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;->f:Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;->f:Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;

    .line 1282113
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;->f:Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1282110
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1282111
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1282109
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1282106
    new-instance v0, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel;-><init>()V

    .line 1282107
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1282108
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1282105
    const v0, -0x5d8c25c4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1282104
    const v0, 0x252222

    return v0
.end method
