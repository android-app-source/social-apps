.class public final Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x45d294c0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1283495
    const-class v0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1283498
    const-class v0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1283496
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1283497
    return-void
.end method

.method private a()Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1283487
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel;->e:Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel;

    iput-object v0, p0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel;->e:Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel;

    .line 1283488
    iget-object v0, p0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel;->e:Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1283489
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1283490
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel;->a()Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1283491
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1283492
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1283493
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1283494
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1283474
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1283475
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel;->a()Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1283476
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel;->a()Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel;

    .line 1283477
    invoke-direct {p0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel;->a()Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1283478
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel;

    .line 1283479
    iput-object v0, v1, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel;->e:Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel$FriendListsModel;

    .line 1283480
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1283481
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1283484
    new-instance v0, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/FetchGoodFriendsListFeedGraphQLModels$FetchGoodFriendsListFeedModel;-><init>()V

    .line 1283485
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1283486
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1283482
    const v0, -0x1c0c891d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1283483
    const v0, -0x6747e1ce

    return v0
.end method
