.class public final Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1286270
    const-class v0, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1286269
    const-class v0, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1286286
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1286287
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1286288
    iget-object v0, p0, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;->e:Ljava/lang/String;

    .line 1286289
    iget-object v0, p0, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1286277
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1286278
    invoke-direct {p0}, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1286279
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1286280
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1286281
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1286282
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1286283
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1286284
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1286285
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1286276
    invoke-direct {p0}, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1286273
    new-instance v0, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;

    invoke-direct {v0}, Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel$AymtChannelModel;-><init>()V

    .line 1286274
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1286275
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1286272
    const v0, 0x6cd8f22f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1286271
    const v0, -0x6aa17dc

    return v0
.end method
