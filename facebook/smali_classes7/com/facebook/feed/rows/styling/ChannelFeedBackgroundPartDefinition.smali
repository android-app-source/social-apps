.class public Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/82O;",
        "Landroid/graphics/drawable/Drawable;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/1V2;

.field private final c:LX/82P;

.field private final d:LX/1V8;

.field private final e:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V2;LX/82P;LX/1V8;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1288139
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1288140
    iput-object p1, p0, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->a:Landroid/content/Context;

    .line 1288141
    iput-object p2, p0, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->b:LX/1V2;

    .line 1288142
    iput-object p3, p0, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->c:LX/82P;

    .line 1288143
    iput-object p4, p0, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->d:LX/1V8;

    .line 1288144
    iput-object p5, p0, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->e:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 1288145
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;
    .locals 9

    .prologue
    .line 1288146
    const-class v1, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    monitor-enter v1

    .line 1288147
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1288148
    sput-object v2, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1288149
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1288150
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1288151
    new-instance v3, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1V2;->a(LX/0QB;)LX/1V2;

    move-result-object v5

    check-cast v5, LX/1V2;

    invoke-static {v0}, LX/82P;->a(LX/0QB;)LX/82P;

    move-result-object v6

    check-cast v6, LX/82P;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v7

    check-cast v7, LX/1V8;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;-><init>(Landroid/content/Context;LX/1V2;LX/82P;LX/1V8;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;)V

    .line 1288152
    move-object v0, v3

    .line 1288153
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1288154
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1288155
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1288156
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1288157
    check-cast p2, LX/82O;

    check-cast p3, LX/1Ps;

    .line 1288158
    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    .line 1288159
    const/4 v0, 0x0

    iget-object v1, p2, LX/82O;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->b:LX/1V2;

    invoke-interface {p3}, LX/1Ps;->f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v4

    invoke-interface {p3}, LX/1Ps;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v5

    invoke-interface {p3}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v6

    invoke-interface {p3}, LX/1Ps;->i()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {p3}, LX/1Ps;->j()Ljava/lang/Object;

    move-result-object v8

    invoke-static/range {v0 .. v8}, LX/1X7;->a(ILcom/facebook/feed/rows/core/props/FeedProps;LX/1X9;LX/1V2;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)LX/1X9;

    move-result-object v0

    .line 1288160
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->d:LX/1V8;

    iget-object v3, p2, LX/82O;->b:LX/1Ua;

    iget-object v4, p0, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->a:Landroid/content/Context;

    move-object v5, v10

    invoke-static/range {v0 .. v5}, LX/1X7;->a(LX/1X9;ILX/1V8;LX/1Ua;Landroid/content/Context;Landroid/graphics/Rect;)V

    .line 1288161
    const/4 v1, 0x0

    const/4 v2, -0x1

    const/4 v3, -0x1

    iget-object v4, p0, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->c:LX/82P;

    iget-object v5, p0, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->a:Landroid/content/Context;

    iget-object v7, p2, LX/82O;->b:LX/1Ua;

    iget-object v8, p0, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->d:LX/1V8;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-object v6, v10

    invoke-static/range {v0 .. v9}, LX/1X7;->a(LX/1X9;IIILX/1dp;Landroid/content/Context;Landroid/graphics/Rect;LX/1Ua;LX/1V8;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1288162
    iget-object v1, p0, Lcom/facebook/feed/rows/styling/ChannelFeedBackgroundPartDefinition;->e:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    new-instance v2, LX/1ds;

    iget v3, v10, Landroid/graphics/Rect;->left:I

    iget v4, v10, Landroid/graphics/Rect;->top:I

    iget v5, v10, Landroid/graphics/Rect;->right:I

    iget v6, v10, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v2, v3, v4, v5, v6}, LX/1ds;-><init>(IIII)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1288163
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2cff3acf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1288164
    check-cast p2, Landroid/graphics/drawable/Drawable;

    .line 1288165
    invoke-virtual {p4, p2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1288166
    const/16 v1, 0x1f

    const v2, -0x52e1b202

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
