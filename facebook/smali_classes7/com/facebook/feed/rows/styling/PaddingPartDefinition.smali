.class public Lcom/facebook/feed/rows/styling/PaddingPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/3aw;",
        "Landroid/graphics/Rect;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/1V8;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1288205
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1288206
    iput-object p1, p0, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->a:Landroid/content/Context;

    .line 1288207
    iput-object p2, p0, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->b:LX/1V8;

    .line 1288208
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/styling/PaddingPartDefinition;
    .locals 5

    .prologue
    .line 1288209
    const-class v1, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    monitor-enter v1

    .line 1288210
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1288211
    sput-object v2, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1288212
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1288213
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1288214
    new-instance p0, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v4

    check-cast v4, LX/1V8;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;-><init>(Landroid/content/Context;LX/1V8;)V

    .line 1288215
    move-object v0, p0

    .line 1288216
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1288217
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1288218
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1288219
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1288220
    check-cast p2, LX/3aw;

    .line 1288221
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 1288222
    iget-object v0, p2, LX/3aw;->c:LX/1X9;

    iget v1, p2, LX/3aw;->d:I

    iget-object v2, p0, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->b:LX/1V8;

    iget-object v3, p2, LX/3aw;->b:LX/1Ua;

    iget-object v4, p0, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->a:Landroid/content/Context;

    invoke-static/range {v0 .. v5}, LX/1X7;->a(LX/1X9;ILX/1V8;LX/1Ua;Landroid/content/Context;Landroid/graphics/Rect;)V

    .line 1288223
    return-object v5
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x15ba0732

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1288224
    check-cast p2, Landroid/graphics/Rect;

    .line 1288225
    iget v1, p2, Landroid/graphics/Rect;->left:I

    iget v2, p2, Landroid/graphics/Rect;->top:I

    iget p0, p2, Landroid/graphics/Rect;->right:I

    iget p1, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p4, v1, v2, p0, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 1288226
    const/16 v1, 0x1f

    const v2, 0x5908e8ea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
