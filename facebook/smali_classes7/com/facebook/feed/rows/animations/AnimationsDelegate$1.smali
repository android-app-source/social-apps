.class public final Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/24J;

.field public final synthetic b:Ljava/lang/Object;

.field public final synthetic c:Ljava/lang/Object;

.field public final synthetic d:Landroid/view/View;

.field public final synthetic e:J

.field public final synthetic f:J

.field public final synthetic g:LX/82K;

.field public final synthetic h:LX/24C;


# direct methods
.method public constructor <init>(LX/24C;LX/24J;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;JJLX/82K;)V
    .locals 0

    .prologue
    .line 1288120
    iput-object p1, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->h:LX/24C;

    iput-object p2, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->a:LX/24J;

    iput-object p3, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->b:Ljava/lang/Object;

    iput-object p4, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->c:Ljava/lang/Object;

    iput-object p5, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->d:Landroid/view/View;

    iput-wide p6, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->e:J

    iput-wide p8, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->f:J

    iput-object p10, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->g:LX/82K;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1288113
    iget-object v0, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->a:LX/24J;

    iget-object v1, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->b:Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->c:Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->d:Landroid/view/View;

    invoke-interface {v0, v1, v2, v3}, LX/24J;->a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)Ljava/lang/Runnable;

    move-result-object v0

    .line 1288114
    if-eqz v0, :cond_0

    .line 1288115
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1288116
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->h:LX/24C;

    iget-object v0, v0, LX/24C;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->e:J

    iget-wide v4, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->f:J

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 1288117
    iget-object v0, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->g:LX/82K;

    iget-object v1, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->c:Ljava/lang/Object;

    iput-object v1, v0, LX/82K;->a:Ljava/lang/Object;

    .line 1288118
    iget-object v0, p0, Lcom/facebook/feed/rows/animations/AnimationsDelegate$1;->g:LX/82K;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, LX/82K;->b:J

    .line 1288119
    :cond_1
    return-void
.end method
