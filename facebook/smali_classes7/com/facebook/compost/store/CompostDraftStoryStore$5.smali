.class public final Lcom/facebook/compost/store/CompostDraftStoryStore$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/7mW;


# direct methods
.method public constructor <init>(LX/7mW;)V
    .locals 0

    .prologue
    .line 1237045
    iput-object p1, p0, Lcom/facebook/compost/store/CompostDraftStoryStore$5;->a:LX/7mW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1237046
    iget-object v0, p0, Lcom/facebook/compost/store/CompostDraftStoryStore$5;->a:LX/7mW;

    iget-object v0, v0, LX/7mW;->f:LX/7mO;

    iget-object v1, p0, Lcom/facebook/compost/store/CompostDraftStoryStore$5;->a:LX/7mW;

    invoke-static {v1}, LX/7mW;->g(LX/7mW;)J

    move-result-wide v2

    .line 1237047
    sget-object v1, LX/7mL;->b:LX/0U1;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0U1;->b(Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 1237048
    iget-object v4, v0, LX/7mO;->a:LX/7mK;

    invoke-virtual {v4}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "draft_story"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v6, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    move v0, v1

    .line 1237049
    iget-object v1, p0, Lcom/facebook/compost/store/CompostDraftStoryStore$5;->a:LX/7mW;

    iget-object v1, v1, LX/7mW;->h:LX/1RW;

    .line 1237050
    iget-object v2, v1, LX/1RW;->a:LX/0Zb;

    const-string v3, "discard_expired"

    invoke-static {v1, v3}, LX/1RW;->p(LX/1RW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "drafts"

    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1237051
    iget-object v0, p0, Lcom/facebook/compost/store/CompostDraftStoryStore$5;->a:LX/7mW;

    iget-object v1, v0, LX/7mW;->l:LX/1RX;

    iget-object v0, p0, Lcom/facebook/compost/store/CompostDraftStoryStore$5;->a:LX/7mW;

    iget-object v0, v0, LX/7mV;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, LX/1RX;->a(Z)V

    .line 1237052
    return-void

    .line 1237053
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
