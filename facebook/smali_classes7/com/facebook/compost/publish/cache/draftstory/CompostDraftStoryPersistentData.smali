.class public Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentDataDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentDataSerializer;
.end annotation


# instance fields
.field public final mGraphQLStory:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "story_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mScheduledTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "schedule_time"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1236867
    const-class v0, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentDataDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1236876
    const-class v0, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentDataSerializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1236868
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1236869
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;->mGraphQLStory:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1236870
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;->mScheduledTime:J

    .line 1236871
    return-void
.end method

.method private constructor <init>(Lcom/facebook/graphql/model/GraphQLStory;J)V
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1236872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1236873
    iput-object p1, p0, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;->mGraphQLStory:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1236874
    iput-wide p2, p0, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;->mScheduledTime:J

    .line 1236875
    return-void
.end method

.method public static a(LX/7mj;)Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;
    .locals 6
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1236861
    new-instance v0, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;

    .line 1236862
    iget-object v1, p0, LX/7mi;->a:Lcom/facebook/graphql/model/GraphQLStory;

    move-object v1, v1

    .line 1236863
    iget-wide v4, p0, LX/7mj;->c:J

    move-wide v2, v4

    .line 1236864
    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;-><init>(Lcom/facebook/graphql/model/GraphQLStory;J)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1236865
    iget-object v0, p0, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;->mGraphQLStory:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1236866
    iget-wide v0, p0, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;->mScheduledTime:J

    return-wide v0
.end method
