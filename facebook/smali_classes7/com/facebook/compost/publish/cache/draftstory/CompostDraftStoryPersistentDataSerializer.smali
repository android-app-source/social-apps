.class public Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1236899
    const-class v0, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;

    new-instance v1, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentDataSerializer;

    invoke-direct {v1}, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1236900
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1236901
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1236902
    if-nez p0, :cond_0

    .line 1236903
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1236904
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1236905
    invoke-static {p0, p1, p2}, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentDataSerializer;->b(Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;LX/0nX;LX/0my;)V

    .line 1236906
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1236907
    return-void
.end method

.method private static b(Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1236908
    const-string v0, "story_data"

    iget-object v1, p0, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;->mGraphQLStory:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1236909
    const-string v0, "schedule_time"

    iget-wide v2, p0, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;->mScheduledTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1236910
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1236911
    check-cast p1, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;

    invoke-static {p1, p2, p3}, Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentDataSerializer;->a(Lcom/facebook/compost/publish/cache/draftstory/CompostDraftStoryPersistentData;LX/0nX;LX/0my;)V

    return-void
.end method
