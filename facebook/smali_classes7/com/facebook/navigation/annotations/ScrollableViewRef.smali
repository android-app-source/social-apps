.class public interface abstract annotation Lcom/facebook/navigation/annotations/ScrollableViewRef;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lcom/facebook/navigation/annotations/ScrollableViewRef;
        type = .enum LX/8Cz;->RECYCLER_VIEW:LX/8Cz;
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->SOURCE:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->TYPE:Ljava/lang/annotation/ElementType;
    }
.end annotation


# virtual methods
.method public abstract id()Ljava/lang/String;
.end method

.method public abstract type()LX/8Cz;
.end method
