.class public Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field public static final p:Ljava/lang/String;

.field private static final q:LX/0Tn;


# instance fields
.field public r:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:LX/7iD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private v:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1227191
    const-class v0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->p:Ljava/lang/String;

    .line 1227192
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->q:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1227162
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1227163
    return-void
.end method

.method private static a(Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/7iD;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/Executor;LX/17Y;)V
    .locals 0

    .prologue
    .line 1227190
    iput-object p1, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->r:LX/03V;

    iput-object p2, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p3, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->t:LX/7iD;

    iput-object p4, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->u:Lcom/facebook/content/SecureContextHelper;

    iput-object p5, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->v:Ljava/util/concurrent/Executor;

    iput-object p6, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->w:LX/17Y;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;

    invoke-static {v6}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {v6}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    new-instance v5, LX/7iD;

    invoke-static {v6}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v6}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-direct {v5, v3, v4}, LX/7iD;-><init>(LX/0tX;LX/1Ck;)V

    move-object v3, v5

    check-cast v3, LX/7iD;

    invoke-static {v6}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v6}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static {v6}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v6

    check-cast v6, LX/17Y;

    invoke-static/range {v0 .. v6}, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->a(Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/7iD;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/Executor;LX/17Y;)V

    return-void
.end method

.method public static c(Ljava/lang/String;)LX/0Tn;
    .locals 3

    .prologue
    .line 1227189
    sget-object v0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->q:LX/0Tn;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 1227164
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1227165
    invoke-static {p0, p0}, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1227166
    invoke-virtual {p0}, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1227167
    const-string v1, "shortcut_id_key"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1227168
    invoke-static {v0}, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->c(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    .line 1227169
    iget-object v1, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-string v2, "url"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1227170
    if-nez v0, :cond_0

    .line 1227171
    invoke-virtual {p0}, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->finish()V

    .line 1227172
    :cond_0
    iget-object v1, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->t:LX/7iD;

    const-string v2, "HOMESCREEN"

    .line 1227173
    new-instance v3, LX/4GS;

    invoke-direct {v3}, LX/4GS;-><init>()V

    .line 1227174
    const-string v4, "url"

    invoke-virtual {v3, v4, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227175
    move-object v3, v3

    .line 1227176
    const-string v4, "source"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227177
    move-object v3, v3

    .line 1227178
    new-instance v4, LX/7iE;

    invoke-direct {v4}, LX/7iE;-><init>()V

    move-object v4, v4

    .line 1227179
    const-string v5, "request_param"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/7iE;

    .line 1227180
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v4

    .line 1227181
    iget-object v5, v1, LX/7iD;->b:LX/1Ck;

    const-string v6, "instant_experience_url_query"

    iget-object p1, v1, LX/7iD;->a:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    new-instance p1, LX/7iC;

    invoke-direct {p1, v1, v4}, LX/7iC;-><init>(LX/7iD;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v5, v6, v3, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1227182
    move-object v0, v4

    .line 1227183
    new-instance v1, LX/4BY;

    invoke-direct {v1, p0}, LX/4BY;-><init>(Landroid/content/Context;)V

    .line 1227184
    invoke-virtual {v1}, LX/4BY;->show()V

    .line 1227185
    const v2, 0x7f0823cb

    invoke-virtual {p0, v2}, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1227186
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/4BY;->a(Z)V

    .line 1227187
    new-instance v2, LX/7iA;

    invoke-direct {v2, p0, v1}, LX/7iA;-><init>(Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;LX/4BY;)V

    iget-object v1, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceShortcutExternalActivity;->v:Ljava/util/concurrent/Executor;

    invoke-static {v0, v2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1227188
    return-void
.end method
