.class public final Lcom/facebook/browserextensions/core/navigation/graphql/InstantExperiencesUrlQueryGraphQLModels$InstantExperienceUrlQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1daa3441
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/browserextensions/core/navigation/graphql/InstantExperiencesUrlQueryGraphQLModels$InstantExperienceUrlQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/browserextensions/core/navigation/graphql/InstantExperiencesUrlQueryGraphQLModels$InstantExperienceUrlQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1227358
    const-class v0, Lcom/facebook/browserextensions/core/navigation/graphql/InstantExperiencesUrlQueryGraphQLModels$InstantExperienceUrlQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1227357
    const-class v0, Lcom/facebook/browserextensions/core/navigation/graphql/InstantExperiencesUrlQueryGraphQLModels$InstantExperienceUrlQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1227355
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1227356
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1227339
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1227340
    invoke-virtual {p0}, Lcom/facebook/browserextensions/core/navigation/graphql/InstantExperiencesUrlQueryGraphQLModels$InstantExperienceUrlQueryModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1227341
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1227342
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1227343
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1227344
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1227352
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1227353
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1227354
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1227350
    iget-object v0, p0, Lcom/facebook/browserextensions/core/navigation/graphql/InstantExperiencesUrlQueryGraphQLModels$InstantExperienceUrlQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browserextensions/core/navigation/graphql/InstantExperiencesUrlQueryGraphQLModels$InstantExperienceUrlQueryModel;->e:Ljava/lang/String;

    .line 1227351
    iget-object v0, p0, Lcom/facebook/browserextensions/core/navigation/graphql/InstantExperiencesUrlQueryGraphQLModels$InstantExperienceUrlQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1227347
    new-instance v0, Lcom/facebook/browserextensions/core/navigation/graphql/InstantExperiencesUrlQueryGraphQLModels$InstantExperienceUrlQueryModel;

    invoke-direct {v0}, Lcom/facebook/browserextensions/core/navigation/graphql/InstantExperiencesUrlQueryGraphQLModels$InstantExperienceUrlQueryModel;-><init>()V

    .line 1227348
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1227349
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1227346
    const v0, -0x1700e7c4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1227345
    const v0, 0x5f891364

    return v0
.end method
