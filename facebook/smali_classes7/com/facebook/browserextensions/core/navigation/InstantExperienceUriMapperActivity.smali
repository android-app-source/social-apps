.class public Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field private static final t:Ljava/lang/String;


# instance fields
.field public p:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/3i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/7iJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1227193
    const-class v0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->t:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1227194
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;LX/03V;LX/3i4;LX/7iJ;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 1227195
    iput-object p1, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->p:LX/03V;

    iput-object p2, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->q:LX/3i4;

    iput-object p3, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->r:LX/7iJ;

    iput-object p4, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->s:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;

    invoke-static {v3}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {v3}, LX/3i4;->a(LX/0QB;)LX/3i4;

    move-result-object v1

    check-cast v1, LX/3i4;

    new-instance p1, LX/7iJ;

    const-class v2, Landroid/content/Context;

    invoke-interface {v3, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v3}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p1, v2, v4}, LX/7iJ;-><init>(Landroid/content/Context;LX/03V;)V

    move-object v2, p1

    check-cast v2, LX/7iJ;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->a(Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;LX/03V;LX/3i4;LX/7iJ;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private d(Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 14

    .prologue
    const/4 v8, 0x0

    .line 1227196
    :try_start_0
    const-string v0, "ix_params"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1227197
    const-string v0, "hash"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1227198
    const-string v0, "offer_params"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "offer_params"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "UTF-8"

    invoke-static {v0, v3}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 1227199
    :goto_0
    iget-object v3, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->r:LX/7iJ;

    invoke-virtual {v3, v2, v1}, LX/7iJ;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 1227200
    if-nez v1, :cond_1

    .line 1227201
    :goto_1
    return-object v8

    :cond_0
    move-object v0, v8

    .line 1227202
    goto :goto_0

    .line 1227203
    :cond_1
    const-string v1, "offer_params"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1227204
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1227205
    const-string v0, "notif_trigger"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1227206
    const-string v0, "notif_trigger"

    const-string v3, "notif_trigger"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1227207
    :cond_2
    const-string v0, "notif_medium"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1227208
    const-string v0, "notif_medium"

    const-string v3, "notif_medium"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1227209
    :cond_3
    const-string v0, "rule"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1227210
    const-string v0, "rule"

    const-string v3, "rule"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_4
    move-object v9, v1

    .line 1227211
    :goto_2
    :try_start_2
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1227212
    const-string v0, "feature_list"

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 1227213
    const-string v0, "whitelisted_domains"

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1227214
    new-instance v0, LX/7i7;

    const-string v1, "site_uri"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "app_id"

    invoke-virtual {v10, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "app_name"

    invoke-virtual {v10, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "page_id"

    invoke-virtual {v10, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "page_scoped_id"

    invoke-virtual {v10, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "source"

    sget-object v7, LX/7i6;->ORGANIC_SHARE:LX/7i6;

    invoke-virtual {v7}, LX/7i6;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v10, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/7i6;->valueOf(Ljava/lang/String;)LX/7i6;

    move-result-object v6

    new-instance v7, Ljava/util/ArrayList;

    const-string v13, ","

    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct/range {v0 .. v7}, LX/7i7;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/7i6;Ljava/util/List;)V

    const-string v1, "page_name"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1227215
    iput-object v1, v0, LX/7i7;->e:Ljava/lang/String;

    .line 1227216
    move-object v0, v0

    .line 1227217
    const-string v1, "app_scoped_id"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1227218
    iput-object v1, v0, LX/7i7;->f:Ljava/lang/String;

    .line 1227219
    move-object v0, v0

    .line 1227220
    iput-object v9, v0, LX/7i7;->k:Lorg/json/JSONObject;

    .line 1227221
    move-object v0, v0

    .line 1227222
    const-string v1, "splash_screen_bg_color"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1227223
    iput-object v1, v0, LX/7i7;->m:Ljava/lang/String;

    .line 1227224
    move-object v0, v0

    .line 1227225
    const-string v1, "webview_chrome_manifest"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1227226
    iput-object v1, v0, LX/7i7;->u:Ljava/lang/String;

    .line 1227227
    move-object v0, v0

    .line 1227228
    const-string v1, "splash_screen_logo"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1227229
    iput-object v1, v0, LX/7i7;->l:Ljava/lang/String;

    .line 1227230
    move-object v0, v0

    .line 1227231
    const-string v1, "privacy_url"

    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1227232
    iput-object v1, v0, LX/7i7;->j:Ljava/lang/String;

    .line 1227233
    move-object v0, v0

    .line 1227234
    const-string v1, "is_copy_link_enabled"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1227235
    iput-boolean v1, v0, LX/7i7;->r:Z

    .line 1227236
    move-object v0, v0

    .line 1227237
    const-string v1, "is_add_to_home_screen_enabled"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1227238
    iput-boolean v1, v0, LX/7i7;->n:Z

    .line 1227239
    move-object v0, v0

    .line 1227240
    const-string v1, "is_manage_permissions_enabled"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1227241
    iput-boolean v1, v0, LX/7i7;->s:Z

    .line 1227242
    move-object v0, v0

    .line 1227243
    const-string v1, "is_autofill_settings_enabled"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1227244
    iput-boolean v1, v0, LX/7i7;->t:Z

    .line 1227245
    move-object v0, v0

    .line 1227246
    const-string v1, "is_scrollable_autofill_bar_enabled"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1227247
    iput-boolean v1, v0, LX/7i7;->q:Z

    .line 1227248
    move-object v0, v0

    .line 1227249
    const-string v1, "is_webview_chrome_enabled"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1227250
    iput-boolean v1, v0, LX/7i7;->p:Z

    .line 1227251
    move-object v0, v0

    .line 1227252
    const-string v1, "is_payment_enabled"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1227253
    iput-boolean v1, v0, LX/7i7;->w:Z

    .line 1227254
    move-object v0, v0

    .line 1227255
    const-string v1, "is_product_history_enabled"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1227256
    iput-boolean v1, v0, LX/7i7;->x:Z

    .line 1227257
    move-object v0, v0

    .line 1227258
    iget-object v1, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->q:LX/3i4;

    invoke-virtual {v1, p0, v0}, LX/3i4;->a(Landroid/content/Context;LX/7i7;)Landroid/content/Intent;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v8

    goto/16 :goto_1

    .line 1227259
    :catch_0
    move-exception v0

    .line 1227260
    iget-object v1, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->p:LX/03V;

    sget-object v3, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->t:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_5
    move-object v9, v8

    goto/16 :goto_2

    .line 1227261
    :catch_1
    move-exception v0

    .line 1227262
    iget-object v1, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->p:LX/03V;

    sget-object v2, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->t:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 1227263
    :catch_2
    goto/16 :goto_1
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1227264
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1227265
    invoke-static {p0, p0}, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1227266
    invoke-virtual {p0}, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1227267
    const-string v1, "ix_params"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1227268
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1227269
    iget-object v0, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->p:LX/03V;

    sget-object v1, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->t:Ljava/lang/String;

    const-string v2, "Intent lacks ix_params"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227270
    invoke-virtual {p0}, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->finish()V

    .line 1227271
    :goto_0
    return-void

    .line 1227272
    :cond_0
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->d(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    .line 1227273
    if-nez v1, :cond_1

    .line 1227274
    iget-object v1, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->p:LX/03V;

    sget-object v2, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->t:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to open for url:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "extra_launch_uri"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227275
    invoke-virtual {p0}, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->finish()V

    goto :goto_0

    .line 1227276
    :cond_1
    iget-object v0, p0, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1227277
    invoke-virtual {p0}, Lcom/facebook/browserextensions/core/navigation/InstantExperienceUriMapperActivity;->finish()V

    goto :goto_0
.end method
