.class public Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Co;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final c:Landroid/content/Context;

.field public final d:LX/03V;

.field public final e:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

.field private final f:LX/1HI;

.field public final g:LX/1FZ;

.field public final h:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1227134
    const-class v0, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->a:Ljava/lang/String;

    .line 1227135
    const-class v0, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/03V;Lcom/facebook/common/shortcuts/InstallShortcutHelper;LX/1HI;LX/1FZ;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1227123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1227124
    iput-object p1, p0, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->c:Landroid/content/Context;

    .line 1227125
    iput-object p2, p0, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->d:LX/03V;

    .line 1227126
    iput-object p3, p0, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->e:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    .line 1227127
    iput-object p4, p0, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->f:LX/1HI;

    .line 1227128
    iput-object p5, p0, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->g:LX/1FZ;

    .line 1227129
    iput-object p6, p0, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1227130
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;
    .locals 7

    .prologue
    .line 1227132
    new-instance v0, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->b(LX/0QB;)Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    move-result-object v3

    check-cast v3, Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v4

    check-cast v4, LX/1HI;

    invoke-static {p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v5

    check-cast v5, LX/1FZ;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;-><init>(Landroid/content/Context;LX/03V;Lcom/facebook/common/shortcuts/InstallShortcutHelper;LX/1HI;LX/1FZ;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1227133
    return-object v0
.end method


# virtual methods
.method public final a()LX/6Eo;
    .locals 1

    .prologue
    .line 1227136
    sget-object v0, LX/6Eo;->ADD_TO_HOME_SCREEN:LX/6Eo;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1227131
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1227114
    const-string v0, "JS_BRIDGE_APP_ICON_URL"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1227115
    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    .line 1227116
    if-nez v0, :cond_0

    .line 1227117
    iget-object v0, p0, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->d:LX/03V;

    sget-object v1, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->a:Ljava/lang/String;

    const-string v2, "Could not generate ImageRequest from URI"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227118
    :goto_0
    return-void

    .line 1227119
    :cond_0
    iget-object v1, p0, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->f:LX/1HI;

    sget-object v2, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 1227120
    new-instance v1, LX/7i9;

    invoke-direct {v1, p0, p2, p1}, LX/7i9;-><init>(Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 1227121
    sget-object v2, LX/1fo;->a:LX/1fo;

    move-object v2, v2

    .line 1227122
    invoke-interface {v0, v1, v2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
