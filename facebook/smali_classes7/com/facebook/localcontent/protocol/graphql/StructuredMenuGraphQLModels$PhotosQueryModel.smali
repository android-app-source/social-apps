.class public final Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x42e66c30
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1308765
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1308766
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1308769
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1308770
    return-void
.end method

.method private j()Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1308767
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel;->e:Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel;

    iput-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel;->e:Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel;

    .line 1308768
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel;->e:Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1308758
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1308759
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel;->j()Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1308760
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1308761
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1308762
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1308763
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1308749
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1308750
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel;->j()Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1308751
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel;->j()Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel;

    .line 1308752
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel;->j()Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1308753
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel;

    .line 1308754
    iput-object v0, v1, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel;->e:Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel;

    .line 1308755
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1308756
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1308764
    new-instance v0, LX/8B9;

    invoke-direct {v0, p1}, LX/8B9;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1308757
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel;->j()Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1308747
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1308748
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1308746
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1308743
    new-instance v0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel;

    invoke-direct {v0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel;-><init>()V

    .line 1308744
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1308745
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1308742
    const v0, -0x5438a2ce

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1308741
    const v0, 0x25d6af

    return v0
.end method
