.class public final Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1307833
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1307834
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1307831
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1307832
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const v2, 0x1d0658c4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1307783
    if-nez p1, :cond_0

    .line 1307784
    :goto_0
    return v0

    .line 1307785
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1307786
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1307787
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1307788
    invoke-static {p0, v1, v2, p3}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1307789
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1307790
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1307791
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1307792
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 1307793
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v2

    .line 1307794
    invoke-virtual {p0, p1, v7}, LX/15i;->b(II)Z

    move-result v3

    .line 1307795
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1307796
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 1307797
    invoke-virtual {p3, v6, v2}, LX/186;->a(IZ)V

    .line 1307798
    invoke-virtual {p3, v7, v3}, LX/186;->a(IZ)V

    .line 1307799
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1307800
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1307801
    const v2, 0x23392750

    const/4 v4, 0x0

    .line 1307802
    if-nez v1, :cond_1

    move v3, v4

    .line 1307803
    :goto_1
    move v1, v3

    .line 1307804
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1307805
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1307806
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1307807
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1307808
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1307809
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1307810
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1307811
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1307812
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1307813
    invoke-static {p0, v1, v2, p3}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1307814
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v2

    .line 1307815
    invoke-virtual {p0, p1, v7}, LX/15i;->b(II)Z

    move-result v3

    .line 1307816
    invoke-virtual {p0, p1, v8}, LX/15i;->b(II)Z

    move-result v4

    .line 1307817
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1307818
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1307819
    invoke-virtual {p3, v6, v2}, LX/186;->a(IZ)V

    .line 1307820
    invoke-virtual {p3, v7, v3}, LX/186;->a(IZ)V

    .line 1307821
    invoke-virtual {p3, v8, v4}, LX/186;->a(IZ)V

    .line 1307822
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1307823
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v5

    .line 1307824
    if-nez v5, :cond_2

    const/4 v3, 0x0

    .line 1307825
    :goto_2
    if-ge v4, v5, :cond_3

    .line 1307826
    invoke-virtual {p0, v1, v4}, LX/15i;->q(II)I

    move-result v7

    .line 1307827
    invoke-static {p0, v7, v2, p3}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v7

    aput v7, v3, v4

    .line 1307828
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1307829
    :cond_2
    new-array v3, v5, [I

    goto :goto_2

    .line 1307830
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x4cec9ab0 -> :sswitch_2
        0x1d0658c4 -> :sswitch_1
        0x23392750 -> :sswitch_3
        0x45254d0e -> :sswitch_4
        0x49a189ff -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1307782
    new-instance v0, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const v1, 0x1d0658c4

    const/4 v0, 0x0

    .line 1307766
    sparse-switch p2, :sswitch_data_0

    .line 1307767
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1307768
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1307769
    invoke-static {p0, v0, v1, p3}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1307770
    :goto_0
    :sswitch_1
    return-void

    .line 1307771
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1307772
    const v1, 0x23392750

    .line 1307773
    if-eqz v0, :cond_0

    .line 1307774
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1307775
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 1307776
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1307777
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1307778
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1307779
    :cond_0
    goto :goto_0

    .line 1307780
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1307781
    invoke-static {p0, v0, v1, p3}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x4cec9ab0 -> :sswitch_2
        0x1d0658c4 -> :sswitch_1
        0x23392750 -> :sswitch_1
        0x45254d0e -> :sswitch_3
        0x49a189ff -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1307760
    if-eqz p1, :cond_0

    .line 1307761
    invoke-static {p0, p1, p2}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1307762
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;

    .line 1307763
    if-eq v0, v1, :cond_0

    .line 1307764
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1307765
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1307759
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1307757
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1307758
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1307752
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1307753
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1307754
    :cond_0
    iput-object p1, p0, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1307755
    iput p2, p0, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;->b:I

    .line 1307756
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1307751
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1307726
    new-instance v0, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1307748
    iget v0, p0, LX/1vt;->c:I

    .line 1307749
    move v0, v0

    .line 1307750
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1307745
    iget v0, p0, LX/1vt;->c:I

    .line 1307746
    move v0, v0

    .line 1307747
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1307742
    iget v0, p0, LX/1vt;->b:I

    .line 1307743
    move v0, v0

    .line 1307744
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1307739
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1307740
    move-object v0, v0

    .line 1307741
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1307730
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1307731
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1307732
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1307733
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1307734
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1307735
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1307736
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1307737
    invoke-static {v3, v9, v2}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1307738
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1307727
    iget v0, p0, LX/1vt;->c:I

    .line 1307728
    move v0, v0

    .line 1307729
    return v0
.end method
