.class public final Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4a232bda
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1307231
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1307230
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1307228
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1307229
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1307225
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1307226
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1307227
    return-void
.end method

.method public static a(Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;)Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 1307210
    if-nez p0, :cond_0

    .line 1307211
    const/4 p0, 0x0

    .line 1307212
    :goto_0
    return-object p0

    .line 1307213
    :cond_0
    instance-of v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    if-eqz v0, :cond_1

    .line 1307214
    check-cast p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    goto :goto_0

    .line 1307215
    :cond_1
    new-instance v0, LX/8AY;

    invoke-direct {v0}, LX/8AY;-><init>()V

    .line 1307216
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8AY;->a:Ljava/lang/String;

    .line 1307217
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->c()Z

    move-result v1

    iput-boolean v1, v0, LX/8AY;->b:Z

    .line 1307218
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8AY;->c:Ljava/lang/String;

    .line 1307219
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8AY;->d:Ljava/lang/String;

    .line 1307220
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eH_()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/8AY;->e:LX/15i;

    iput v1, v0, LX/8AY;->f:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1307221
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eI_()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iput-object v2, v0, LX/8AY;->g:LX/15i;

    iput v1, v0, LX/8AY;->h:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1307222
    invoke-virtual {v0}, LX/8AY;->a()Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    move-result-object p0

    goto :goto_0

    .line 1307223
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1307224
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1307204
    iput-boolean p1, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->f:Z

    .line 1307205
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1307206
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1307207
    if-eqz v0, :cond_0

    .line 1307208
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1307209
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1307189
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1307190
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1307191
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1307192
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1307193
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eH_()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x2c262842

    invoke-static {v4, v3, v5}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1307194
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eI_()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, 0xb40934

    invoke-static {v5, v4, v6}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1307195
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1307196
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1307197
    const/4 v0, 0x1

    iget-boolean v5, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->f:Z

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1307198
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1307199
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1307200
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1307201
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1307202
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1307203
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1307173
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1307174
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eH_()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1307175
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eH_()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2c262842

    invoke-static {v2, v0, v3}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1307176
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eH_()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1307177
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    .line 1307178
    iput v3, v0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->i:I

    move-object v1, v0

    .line 1307179
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eI_()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1307180
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eI_()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0xb40934

    invoke-static {v2, v0, v3}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1307181
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->eI_()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1307182
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    .line 1307183
    iput v3, v0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->j:I

    move-object v1, v0

    .line 1307184
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1307185
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1307186
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1307187
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 1307188
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1307172
    new-instance v0, LX/8AZ;

    invoke-direct {v0, p1}, LX/8AZ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1307171
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1307232
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1307233
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->f:Z

    .line 1307234
    const/4 v0, 0x4

    const v1, 0x2c262842

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->i:I

    .line 1307235
    const/4 v0, 0x5

    const v1, 0xb40934

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->j:I

    .line 1307236
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1307145
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1307146
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1307147
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1307148
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1307149
    :goto_0
    return-void

    .line 1307150
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1307152
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1307153
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->a(Z)V

    .line 1307154
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1307155
    new-instance v0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    invoke-direct {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;-><init>()V

    .line 1307156
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1307157
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1307158
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->e:Ljava/lang/String;

    .line 1307159
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1307160
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1307161
    iget-boolean v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->f:Z

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1307162
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->g:Ljava/lang/String;

    .line 1307163
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1307151
    const v0, -0x365edc06

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1307164
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->h:Ljava/lang/String;

    .line 1307165
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final eH_()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerDoesNotLikeSentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1307166
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1307167
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final eI_()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerLikesSentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1307168
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1307169
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1307170
    const v0, 0xb717b40

    return v0
.end method
