.class public final Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7d54a62e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1308186
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1308187
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1308188
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1308189
    return-void
.end method

.method private a()Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1308190
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel;->e:Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel;->e:Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;

    .line 1308191
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel;->e:Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1308192
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1308193
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel;->a()Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1308194
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1308195
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1308196
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1308197
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1308198
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1308199
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel;->a()Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1308200
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel;->a()Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;

    .line 1308201
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel;->a()Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1308202
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel;

    .line 1308203
    iput-object v0, v1, Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel;->e:Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;

    .line 1308204
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1308205
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1308206
    new-instance v0, Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel;

    invoke-direct {v0}, Lcom/facebook/localcontent/protocol/graphql/PageMenuVisibilityMutationModels$PageMenuVisibilityMutationModel;-><init>()V

    .line 1308207
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1308208
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1308209
    const v0, -0x38bad8a0    # -50471.375f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1308210
    const v0, -0x6a7e43dd

    return v0
.end method
