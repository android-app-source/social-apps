.class public final Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1306004
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1306005
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1306096
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1306097
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1306065
    if-nez p1, :cond_0

    move v0, v1

    .line 1306066
    :goto_0
    return v0

    .line 1306067
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1306068
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1306069
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1306070
    const v2, 0x79426577

    const/4 v5, 0x0

    .line 1306071
    if-nez v0, :cond_1

    move v3, v5

    .line 1306072
    :goto_1
    move v0, v3

    .line 1306073
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1306074
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1306075
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1306076
    :sswitch_1
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;

    .line 1306077
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1306078
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1306079
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1306080
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1306081
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1306082
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1306083
    invoke-virtual {p0, p1, v4}, LX/15i;->b(II)Z

    move-result v2

    .line 1306084
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1306085
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1306086
    invoke-virtual {p3, v4, v2}, LX/186;->a(IZ)V

    .line 1306087
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1306088
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    .line 1306089
    if-nez p1, :cond_2

    const/4 v3, 0x0

    .line 1306090
    :goto_2
    if-ge v5, p1, :cond_3

    .line 1306091
    invoke-virtual {p0, v0, v5}, LX/15i;->q(II)I

    move-result p2

    .line 1306092
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v3, v5

    .line 1306093
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1306094
    :cond_2
    new-array v3, p1, [I

    goto :goto_2

    .line 1306095
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v3, v5}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2e042ba9 -> :sswitch_0
        0x4471a6ab -> :sswitch_2
        0x79426577 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1306064
    new-instance v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1306059
    if-eqz p0, :cond_0

    .line 1306060
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1306061
    if-eq v0, p0, :cond_0

    .line 1306062
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1306063
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1306046
    sparse-switch p2, :sswitch_data_0

    .line 1306047
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1306048
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1306049
    const v1, 0x79426577

    .line 1306050
    if-eqz v0, :cond_0

    .line 1306051
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1306052
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1306053
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1306054
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1306055
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1306056
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 1306057
    :sswitch_2
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;

    .line 1306058
    invoke-static {v0, p3}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2e042ba9 -> :sswitch_0
        0x4471a6ab -> :sswitch_1
        0x79426577 -> :sswitch_2
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1306040
    if-eqz p1, :cond_0

    .line 1306041
    invoke-static {p0, p1, p2}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1306042
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;

    .line 1306043
    if-eq v0, v1, :cond_0

    .line 1306044
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1306045
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1306039
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1306037
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1306038
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1306032
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1306033
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1306034
    :cond_0
    iput-object p1, p0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1306035
    iput p2, p0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;->b:I

    .line 1306036
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1306031
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1306030
    new-instance v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1306027
    iget v0, p0, LX/1vt;->c:I

    .line 1306028
    move v0, v0

    .line 1306029
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1306024
    iget v0, p0, LX/1vt;->c:I

    .line 1306025
    move v0, v0

    .line 1306026
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1306021
    iget v0, p0, LX/1vt;->b:I

    .line 1306022
    move v0, v0

    .line 1306023
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1306018
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1306019
    move-object v0, v0

    .line 1306020
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1306009
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1306010
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1306011
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1306012
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1306013
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1306014
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1306015
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1306016
    invoke-static {v3, v9, v2}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1306017
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1306006
    iget v0, p0, LX/1vt;->c:I

    .line 1306007
    move v0, v0

    .line 1306008
    return v0
.end method
