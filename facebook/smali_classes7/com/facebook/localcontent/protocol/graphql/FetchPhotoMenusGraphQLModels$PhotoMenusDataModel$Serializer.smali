.class public final Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1306358
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel;

    new-instance v1, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1306359
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1306357
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel;LX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 1306328
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1306329
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v4, 0x1

    .line 1306330
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1306331
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1306332
    if-eqz v2, :cond_3

    .line 1306333
    const-string v3, "page_photo_menus"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1306334
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1306335
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1306336
    if-eqz v3, :cond_2

    .line 1306337
    const-string v5, "nodes"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1306338
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1306339
    const/4 v5, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v6

    if-ge v5, v6, :cond_1

    .line 1306340
    invoke-virtual {v1, v3, v5}, LX/15i;->q(II)I

    move-result v6

    .line 1306341
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1306342
    const/4 p0, 0x0

    invoke-virtual {v1, v6, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1306343
    if-eqz p0, :cond_0

    .line 1306344
    const-string v2, "page_photo_menu_photos"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1306345
    invoke-static {v1, p0, p1, p2}, LX/8AJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1306346
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1306347
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1306348
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1306349
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1306350
    :cond_3
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1306351
    if-eqz v2, :cond_4

    .line 1306352
    const-string v2, "viewer_profile_permissions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1306353
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1306354
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1306355
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1306356
    check-cast p1, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$Serializer;->a(Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
