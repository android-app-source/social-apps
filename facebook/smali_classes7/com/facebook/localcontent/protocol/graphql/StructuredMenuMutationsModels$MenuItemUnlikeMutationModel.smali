.class public final Lcom/facebook/localcontent/protocol/graphql/StructuredMenuMutationsModels$MenuItemUnlikeMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x28535e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/localcontent/protocol/graphql/StructuredMenuMutationsModels$MenuItemUnlikeMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/localcontent/protocol/graphql/StructuredMenuMutationsModels$MenuItemUnlikeMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1308989
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuMutationsModels$MenuItemUnlikeMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1309008
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuMutationsModels$MenuItemUnlikeMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1309006
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1309007
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1309004
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuMutationsModels$MenuItemUnlikeMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuMutationsModels$MenuItemUnlikeMutationModel;->e:Ljava/lang/String;

    .line 1309005
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuMutationsModels$MenuItemUnlikeMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1308998
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1308999
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuMutationsModels$MenuItemUnlikeMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1309000
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1309001
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1309002
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1309003
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1308995
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1308996
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1308997
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1308992
    new-instance v0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuMutationsModels$MenuItemUnlikeMutationModel;

    invoke-direct {v0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuMutationsModels$MenuItemUnlikeMutationModel;-><init>()V

    .line 1308993
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1308994
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1308991
    const v0, 0x2e63ed17

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1308990
    const v0, -0x60230123

    return v0
.end method
