.class public final Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1306701
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1306702
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1306795
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1306796
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1306752
    if-nez p1, :cond_0

    .line 1306753
    :goto_0
    return v0

    .line 1306754
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1306755
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1306756
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1306757
    const v2, -0x3749127d

    const/4 v4, 0x0

    .line 1306758
    if-nez v1, :cond_1

    move v3, v4

    .line 1306759
    :goto_1
    move v1, v3

    .line 1306760
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v2

    .line 1306761
    invoke-virtual {p0, p1, v7}, LX/15i;->p(II)I

    move-result v3

    .line 1306762
    const v4, 0x7a935224

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1306763
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1306764
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1306765
    invoke-virtual {p3, v6, v2}, LX/186;->a(IZ)V

    .line 1306766
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 1306767
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1306768
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    move-result-object v1

    .line 1306769
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1306770
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1306771
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1306772
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1306773
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1306774
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1306775
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1306776
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1306777
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1306778
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 1306779
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 1306780
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 1306781
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1306782
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    move-result-object v1

    .line 1306783
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1306784
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1306785
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1306786
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1306787
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v5

    .line 1306788
    if-nez v5, :cond_2

    const/4 v3, 0x0

    .line 1306789
    :goto_2
    if-ge v4, v5, :cond_3

    .line 1306790
    invoke-virtual {p0, v1, v4}, LX/15i;->q(II)I

    move-result p2

    .line 1306791
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v3, v4

    .line 1306792
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1306793
    :cond_2
    new-array v3, v5, [I

    goto :goto_2

    .line 1306794
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3749127d -> :sswitch_1
        -0x1f10707 -> :sswitch_0
        0x7a935224 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1306751
    new-instance v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 4

    .prologue
    .line 1306738
    sparse-switch p2, :sswitch_data_0

    .line 1306739
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1306740
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1306741
    const v1, -0x3749127d

    .line 1306742
    if-eqz v0, :cond_0

    .line 1306743
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result v3

    .line 1306744
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 1306745
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1306746
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1306747
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1306748
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1306749
    const v1, 0x7a935224

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1306750
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x3749127d -> :sswitch_1
        -0x1f10707 -> :sswitch_0
        0x7a935224 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1306732
    if-eqz p1, :cond_0

    .line 1306733
    invoke-static {p0, p1, p2}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1306734
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;

    .line 1306735
    if-eq v0, v1, :cond_0

    .line 1306736
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1306737
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1306731
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1306729
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1306730
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1306797
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1306798
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1306799
    :cond_0
    iput-object p1, p0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1306800
    iput p2, p0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;->b:I

    .line 1306801
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1306728
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1306727
    new-instance v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1306724
    iget v0, p0, LX/1vt;->c:I

    .line 1306725
    move v0, v0

    .line 1306726
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1306721
    iget v0, p0, LX/1vt;->c:I

    .line 1306722
    move v0, v0

    .line 1306723
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1306718
    iget v0, p0, LX/1vt;->b:I

    .line 1306719
    move v0, v0

    .line 1306720
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1306715
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1306716
    move-object v0, v0

    .line 1306717
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1306706
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1306707
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1306708
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1306709
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1306710
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1306711
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1306712
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1306713
    invoke-static {v3, v9, v2}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1306714
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1306703
    iget v0, p0, LX/1vt;->c:I

    .line 1306704
    move v0, v0

    .line 1306705
    return v0
.end method
