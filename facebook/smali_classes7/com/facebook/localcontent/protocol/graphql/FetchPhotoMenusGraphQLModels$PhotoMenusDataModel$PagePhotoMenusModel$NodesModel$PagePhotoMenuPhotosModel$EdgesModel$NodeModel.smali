.class public final Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b04342e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1306244
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1306243
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1306208
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1306209
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1306241
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1306242
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1306232
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1306233
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1306234
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1306235
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1306236
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1306237
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1306238
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->g:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1306239
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1306240
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1306224
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1306225
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1306226
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1306227
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1306228
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;

    .line 1306229
    iput-object v0, v1, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1306230
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1306231
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1306223
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1306220
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1306221
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->g:J

    .line 1306222
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1306217
    new-instance v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;-><init>()V

    .line 1306218
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1306219
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1306215
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    .line 1306216
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1306214
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 1306212
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1306213
    iget-wide v0, p0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->g:J

    return-wide v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1306211
    const v0, -0x631bb5c0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1306210
    const v0, 0x4984e12

    return v0
.end method
