.class public final Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1306665
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel;

    new-instance v1, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1306666
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1306613
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1306615
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1306616
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1306617
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1306618
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1306619
    if-eqz v2, :cond_9

    .line 1306620
    const-string v3, "photos_by_category"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1306621
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1306622
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1306623
    if-eqz v3, :cond_5

    .line 1306624
    const-string v4, "available_categories"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1306625
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1306626
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_4

    .line 1306627
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    const/4 p2, 0x0

    .line 1306628
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1306629
    invoke-virtual {v1, p0, p2}, LX/15i;->g(II)I

    move-result v0

    .line 1306630
    if-eqz v0, :cond_0

    .line 1306631
    const-string v0, "category"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1306632
    invoke-virtual {v1, p0, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1306633
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1306634
    if-eqz v0, :cond_1

    .line 1306635
    const-string p2, "secondary_upload_message"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1306636
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1306637
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1306638
    if-eqz v0, :cond_2

    .line 1306639
    const-string p2, "title"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1306640
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1306641
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1306642
    if-eqz v0, :cond_3

    .line 1306643
    const-string p2, "upload_message"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1306644
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1306645
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1306646
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1306647
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1306648
    :cond_5
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 1306649
    if-eqz v3, :cond_6

    .line 1306650
    const-string v4, "enable_photo_uploads"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1306651
    invoke-virtual {p1, v3}, LX/0nX;->a(Z)V

    .line 1306652
    :cond_6
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1306653
    if-eqz v3, :cond_8

    .line 1306654
    const-string v4, "primary_category"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1306655
    const/4 p0, 0x0

    .line 1306656
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1306657
    invoke-virtual {v1, v3, p0}, LX/15i;->g(II)I

    move-result v4

    .line 1306658
    if-eqz v4, :cond_7

    .line 1306659
    const-string v4, "category"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1306660
    invoke-virtual {v1, v3, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1306661
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1306662
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1306663
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1306664
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1306614
    check-cast p1, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel$Serializer;->a(Lcom/facebook/localcontent/protocol/graphql/FetchPhotosByCategoryGraphQLModels$AvailableCategoriesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
