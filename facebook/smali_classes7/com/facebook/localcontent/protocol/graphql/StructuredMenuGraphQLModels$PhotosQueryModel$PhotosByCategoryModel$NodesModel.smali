.class public final Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x74f19db0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1308670
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1308671
    const-class v0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1308672
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1308673
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1308674
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1308675
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1308676
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1308677
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1308678
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1308679
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1308680
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1308681
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1308682
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1308683
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1308684
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1308685
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1308661
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1308662
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1308663
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1308664
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1308665
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;

    .line 1308666
    iput-object v0, v1, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1308667
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1308668
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1308669
    invoke-virtual {p0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1308658
    new-instance v0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;-><init>()V

    .line 1308659
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1308660
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1308651
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->e:Ljava/lang/String;

    .line 1308652
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1308656
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->f:Ljava/lang/String;

    .line 1308657
    iget-object v0, p0, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1308655
    invoke-direct {p0}, Lcom/facebook/localcontent/protocol/graphql/StructuredMenuGraphQLModels$PhotosQueryModel$PhotosByCategoryModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1308654
    const v0, 0x3141dabd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1308653
    const v0, 0x4984e12

    return v0
.end method
