.class public final Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1307087
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1307088
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1307036
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1307037
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1307073
    if-nez p1, :cond_0

    .line 1307074
    :goto_0
    return v0

    .line 1307075
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1307076
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1307077
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1307078
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1307079
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1307080
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1307081
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1307082
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1307083
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1307084
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1307085
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1307086
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xb40934 -> :sswitch_1
        0x2c262842 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1307072
    new-instance v0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1307069
    sparse-switch p0, :sswitch_data_0

    .line 1307070
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1307071
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        0xb40934 -> :sswitch_0
        0x2c262842 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1307068
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1307066
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;->b(I)V

    .line 1307067
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1307061
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1307062
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1307063
    :cond_0
    iput-object p1, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;->a:LX/15i;

    .line 1307064
    iput p2, p0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;->b:I

    .line 1307065
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1307060
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1307059
    new-instance v0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1307056
    iget v0, p0, LX/1vt;->c:I

    .line 1307057
    move v0, v0

    .line 1307058
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1307053
    iget v0, p0, LX/1vt;->c:I

    .line 1307054
    move v0, v0

    .line 1307055
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1307050
    iget v0, p0, LX/1vt;->b:I

    .line 1307051
    move v0, v0

    .line 1307052
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1307047
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1307048
    move-object v0, v0

    .line 1307049
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1307038
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1307039
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1307040
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1307041
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1307042
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1307043
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1307044
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1307045
    invoke-static {v3, v9, v2}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1307046
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1307033
    iget v0, p0, LX/1vt;->c:I

    .line 1307034
    move v0, v0

    .line 1307035
    return v0
.end method
