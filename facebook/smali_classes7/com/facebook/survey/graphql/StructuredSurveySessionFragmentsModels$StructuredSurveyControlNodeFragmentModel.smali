.class public final Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/7Fc;
.implements LX/7Fb;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7584966c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I

.field private i:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:I

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1187133
    const-class v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1187132
    const-class v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1187130
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1187131
    return-void
.end method

.method private l()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCompositeControlNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1187128
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->i:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;

    iput-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->i:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;

    .line 1187129
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->i:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1187108
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1187109
    invoke-virtual {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1187110
    invoke-virtual {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->k()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v1

    .line 1187111
    invoke-direct {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->l()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1187112
    invoke-virtual {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->b()LX/0Px;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 1187113
    invoke-virtual {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->dF_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1187114
    invoke-virtual {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->j()LX/0Px;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/util/List;)I

    move-result v5

    .line 1187115
    const/16 v6, 0xa

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1187116
    iget v6, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->e:I

    invoke-virtual {p1, v7, v6, v7}, LX/186;->a(III)V

    .line 1187117
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1187118
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1187119
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->h:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 1187120
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1187121
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1187122
    const/4 v0, 0x6

    iget v1, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->k:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 1187123
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1187124
    const/16 v0, 0x8

    iget v1, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->m:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 1187125
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1187126
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1187127
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1187090
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1187091
    invoke-virtual {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->k()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1187092
    invoke-virtual {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->k()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1187093
    if-eqz v1, :cond_3

    .line 1187094
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;

    .line 1187095
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->g:LX/3Sb;

    move-object v1, v0

    .line 1187096
    :goto_0
    invoke-direct {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->l()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1187097
    invoke-direct {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->l()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;

    .line 1187098
    invoke-direct {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->l()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1187099
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;

    .line 1187100
    iput-object v0, v1, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->i:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;

    .line 1187101
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1187102
    invoke-virtual {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1187103
    if-eqz v2, :cond_1

    .line 1187104
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;

    .line 1187105
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->j:Ljava/util/List;

    move-object v1, v0

    .line 1187106
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1187107
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final synthetic a()LX/7Fc;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCompositeControlNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1187089
    invoke-direct {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->l()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1187083
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1187084
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->e:I

    .line 1187085
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->h:I

    .line 1187086
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->k:I

    .line 1187087
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->m:I

    .line 1187088
    return-void
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCompositePageNodes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1187081
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyNestedControlNodeFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->j:Ljava/util/List;

    .line 1187082
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1187078
    new-instance v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;

    invoke-direct {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;-><init>()V

    .line 1187079
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1187080
    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1187076
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1187077
    iget v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->e:I

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1187062
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->f:Ljava/lang/String;

    .line 1187063
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final dF_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1187074
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->l:Ljava/lang/String;

    .line 1187075
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final dG_()I
    .locals 2

    .prologue
    .line 1187072
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1187073
    iget v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->m:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1187071
    const v0, 0x6b735e3f

    return v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 1187069
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1187070
    iget v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->k:I

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1187068
    const v0, 0x970a7d4

    return v0
.end method

.method public final j()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1187066
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->n:Ljava/util/List;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->n:Ljava/util/List;

    .line 1187067
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->n:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getBranchResponseMaps"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1187064
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->g:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x2

    const v4, 0x65e361fb

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->g:LX/3Sb;

    .line 1187065
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveyControlNodeFragmentModel;->g:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method
