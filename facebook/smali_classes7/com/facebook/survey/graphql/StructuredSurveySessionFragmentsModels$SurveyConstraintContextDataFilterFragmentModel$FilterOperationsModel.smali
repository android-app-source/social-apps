.class public final Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5e0bf171
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:D

.field private j:D


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1187723
    const-class v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1187722
    const-class v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1187720
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1187721
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1187718
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->f:Ljava/lang/String;

    .line 1187719
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 1187704
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1187705
    invoke-virtual {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1187706
    invoke-direct {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1187707
    invoke-virtual {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1187708
    invoke-virtual {p0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1187709
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1187710
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1187711
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1187712
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1187713
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1187714
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->i:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1187715
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->j:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1187716
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1187717
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1187701
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1187702
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1187703
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1187698
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1187699
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1187700
    :cond_0
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1187724
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1187725
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->i:D

    .line 1187726
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->j:D

    .line 1187727
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1187695
    new-instance v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;

    invoke-direct {v0}, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;-><init>()V

    .line 1187696
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1187697
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1187694
    const v0, -0x2e79b3ee

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1187693
    const v0, -0x50b46d89

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1187691
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->g:Ljava/lang/String;

    .line 1187692
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1187685
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->h:Ljava/lang/String;

    .line 1187686
    iget-object v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final l()D
    .locals 2

    .prologue
    .line 1187689
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1187690
    iget-wide v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->i:D

    return-wide v0
.end method

.method public final m()D
    .locals 2

    .prologue
    .line 1187687
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1187688
    iget-wide v0, p0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$SurveyConstraintContextDataFilterFragmentModel$FilterOperationsModel;->j:D

    return-wide v0
.end method
