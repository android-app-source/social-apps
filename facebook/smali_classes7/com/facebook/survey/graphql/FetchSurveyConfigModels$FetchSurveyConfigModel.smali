.class public final Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6e3d581d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$StructuredSurveyModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1186825
    const-class v0, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1186824
    const-class v0, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1186822
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1186823
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1186819
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1186820
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1186821
    :cond_0
    iget-object v0, p0, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private j()Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$StructuredSurveyModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1186817
    iget-object v0, p0, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->f:Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$StructuredSurveyModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$StructuredSurveyModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$StructuredSurveyModel;

    iput-object v0, p0, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->f:Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$StructuredSurveyModel;

    .line 1186818
    iget-object v0, p0, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->f:Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$StructuredSurveyModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1186815
    iget-object v0, p0, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->g:Ljava/lang/String;

    .line 1186816
    iget-object v0, p0, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1186805
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1186806
    invoke-direct {p0}, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1186807
    invoke-direct {p0}, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->j()Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$StructuredSurveyModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1186808
    invoke-direct {p0}, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1186809
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1186810
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1186811
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1186812
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1186813
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1186814
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1186797
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1186798
    invoke-direct {p0}, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->j()Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$StructuredSurveyModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1186799
    invoke-direct {p0}, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->j()Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$StructuredSurveyModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$StructuredSurveyModel;

    .line 1186800
    invoke-direct {p0}, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->j()Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$StructuredSurveyModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1186801
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;

    .line 1186802
    iput-object v0, v1, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;->f:Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$StructuredSurveyModel;

    .line 1186803
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1186804
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1186788
    new-instance v0, LX/7FX;

    invoke-direct {v0, p1}, LX/7FX;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1186795
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1186796
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1186794
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1186791
    new-instance v0, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;

    invoke-direct {v0}, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;-><init>()V

    .line 1186792
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1186793
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1186790
    const v0, -0xb1dfdd1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1186789
    const v0, 0x252222

    return v0
.end method
