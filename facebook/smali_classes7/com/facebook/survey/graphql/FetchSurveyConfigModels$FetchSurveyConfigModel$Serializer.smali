.class public final Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1186745
    const-class v0, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;

    new-instance v1, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1186746
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1186744
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1186727
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1186728
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1186729
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1186730
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1186731
    if-eqz v2, :cond_0

    .line 1186732
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1186733
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1186734
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1186735
    if-eqz v2, :cond_1

    .line 1186736
    const-string p0, "structured_survey"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1186737
    invoke-static {v1, v2, p1}, LX/7FZ;->a(LX/15i;ILX/0nX;)V

    .line 1186738
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1186739
    if-eqz v2, :cond_2

    .line 1186740
    const-string p0, "survey_start_url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1186741
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1186742
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1186743
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1186726
    check-cast p1, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel$Serializer;->a(Lcom/facebook/survey/graphql/FetchSurveyConfigModels$FetchSurveyConfigModel;LX/0nX;LX/0my;)V

    return-void
.end method
