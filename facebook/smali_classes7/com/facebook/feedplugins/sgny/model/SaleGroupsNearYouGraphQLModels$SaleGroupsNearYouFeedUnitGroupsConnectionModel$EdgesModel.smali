.class public final Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xf351570
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1290217
    const-class v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1290216
    const-class v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1290214
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1290215
    return-void
.end method

.method private j()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290212
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;->e:Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;->e:Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    .line 1290213
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;->e:Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1290204
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1290205
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;->j()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1290206
    invoke-virtual {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1290207
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1290208
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1290209
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1290210
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1290211
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1290196
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1290197
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;->j()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1290198
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;->j()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    .line 1290199
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;->j()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1290200
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;

    .line 1290201
    iput-object v0, v1, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;->e:Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    .line 1290202
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1290203
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290195
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;->j()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1290192
    new-instance v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;-><init>()V

    .line 1290193
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1290194
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290190
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;->f:Ljava/lang/String;

    .line 1290191
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1290188
    const v0, -0x6034cbf6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1290189
    const v0, 0x5ed9a1ef

    return v0
.end method
