.class public final Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x61ca0b01
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1290180
    const-class v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1290179
    const-class v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1290177
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1290178
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 4

    .prologue
    .line 1290170
    iput-object p1, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1290171
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1290172
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1290173
    if-eqz v0, :cond_0

    .line 1290174
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x6

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1290175
    :cond_0
    return-void

    .line 1290176
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1290080
    iput-object p1, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    .line 1290081
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1290082
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1290083
    if-eqz v0, :cond_0

    .line 1290084
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1290085
    :cond_0
    return-void
.end method

.method private l()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290168
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->f:Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->f:Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    .line 1290169
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->f:Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290166
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1290167
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290164
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1290165
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1290145
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1290146
    invoke-virtual {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x7eb2518b

    invoke-static {v1, v0, v2}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1290147
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1290148
    invoke-virtual {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1290149
    invoke-virtual {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->eA_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1290150
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1290151
    invoke-virtual {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1290152
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1290153
    const/16 v7, 0x8

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1290154
    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 1290155
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1290156
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->g:I

    invoke-virtual {p1, v0, v1, v8}, LX/186;->a(III)V

    .line 1290157
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1290158
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1290159
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1290160
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1290161
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1290162
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1290163
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1290120
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1290121
    invoke-virtual {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1290122
    invoke-virtual {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7eb2518b

    invoke-static {v2, v0, v3}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1290123
    invoke-virtual {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->b()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1290124
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    .line 1290125
    iput v3, v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->e:I

    move-object v1, v0

    .line 1290126
    :cond_0
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1290127
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    .line 1290128
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1290129
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    .line 1290130
    iput-object v0, v1, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->f:Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    .line 1290131
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1290132
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1290133
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1290134
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    .line 1290135
    iput-object v0, v1, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1290136
    :cond_2
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1290137
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1290138
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1290139
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    .line 1290140
    iput-object v0, v1, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1290141
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1290142
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    .line 1290143
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    move-object p0, v1

    .line 1290144
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1290119
    new-instance v0, LX/83I;

    invoke-direct {v0, p1}, LX/83I;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290118
    invoke-virtual {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1290114
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1290115
    const v0, 0x7eb2518b

    invoke-static {p1, p2, v2, v0}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->e:I

    .line 1290116
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->g:I

    .line 1290117
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1290104
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1290105
    invoke-virtual {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->eA_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1290106
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1290107
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    .line 1290108
    :goto_0
    return-void

    .line 1290109
    :cond_0
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1290110
    invoke-virtual {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1290111
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1290112
    const/4 v0, 0x6

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1290113
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1290099
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1290100
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->a(Ljava/lang/String;)V

    .line 1290101
    :cond_0
    :goto_0
    return-void

    .line 1290102
    :cond_1
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1290103
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    goto :goto_0
.end method

.method public final b()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1290097
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1290098
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1290094
    new-instance v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;-><init>()V

    .line 1290095
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1290096
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290093
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l()Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 1290091
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1290092
    iget v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->g:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1290090
    const v0, 0x3273f1a8

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290088
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    .line 1290089
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final eA_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290086
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    .line 1290087
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic eB_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290079
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1290078
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290076
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1290077
    iget-object v0, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method public final synthetic k()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1290075
    invoke-direct {p0}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method
