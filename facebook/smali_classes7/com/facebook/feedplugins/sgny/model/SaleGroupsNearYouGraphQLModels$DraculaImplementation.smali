.class public final Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1289858
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1289859
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1289860
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1289861
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1289862
    if-nez p1, :cond_0

    move v0, v1

    .line 1289863
    :goto_0
    return v0

    .line 1289864
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1289865
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1289866
    :pswitch_0
    const-class v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    .line 1289867
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1289868
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1289869
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1289870
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7eb2518b
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1289877
    new-instance v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1289838
    if-eqz p0, :cond_0

    .line 1289839
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1289840
    if-eq v0, p0, :cond_0

    .line 1289841
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1289842
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1289871
    packed-switch p2, :pswitch_data_0

    .line 1289872
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1289873
    :pswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$SaleGroupsNearYouFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    .line 1289874
    invoke-static {v0, p3}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1289875
    return-void

    :pswitch_data_0
    .packed-switch 0x7eb2518b
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1289876
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1289851
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1289852
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1289853
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1289854
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1289855
    :cond_0
    iput-object p1, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1289856
    iput p2, p0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;->b:I

    .line 1289857
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1289850
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1289849
    new-instance v0, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1289846
    iget v0, p0, LX/1vt;->c:I

    .line 1289847
    move v0, v0

    .line 1289848
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1289843
    iget v0, p0, LX/1vt;->c:I

    .line 1289844
    move v0, v0

    .line 1289845
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1289835
    iget v0, p0, LX/1vt;->b:I

    .line 1289836
    move v0, v0

    .line 1289837
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1289832
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1289833
    move-object v0, v0

    .line 1289834
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1289823
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1289824
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1289825
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1289826
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1289827
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1289828
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1289829
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1289830
    invoke-static {v3, v9, v2}, Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feedplugins/sgny/model/SaleGroupsNearYouGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1289831
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1289820
    iget v0, p0, LX/1vt;->c:I

    .line 1289821
    move v0, v0

    .line 1289822
    return v0
.end method
