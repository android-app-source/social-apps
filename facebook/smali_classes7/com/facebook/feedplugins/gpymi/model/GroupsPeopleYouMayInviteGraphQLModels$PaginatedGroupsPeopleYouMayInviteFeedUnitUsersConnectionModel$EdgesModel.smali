.class public final Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x66192d66
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1289605
    const-class v0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1289604
    const-class v0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1289602
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1289603
    return-void
.end method

.method private j()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1289600
    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->e:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->e:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    .line 1289601
    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->e:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    return-object v0
.end method

.method private k()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1289598
    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->g:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->g:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;

    .line 1289599
    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->g:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;

    return-object v0
.end method

.method private l()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1289596
    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->h:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->h:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;

    .line 1289597
    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->h:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1289582
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1289583
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->j()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1289584
    invoke-virtual {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1289585
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->k()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1289586
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->l()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1289587
    invoke-virtual {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1289588
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1289589
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1289590
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1289591
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1289592
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1289593
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1289594
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1289595
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1289564
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1289565
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->j()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1289566
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->j()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    .line 1289567
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->j()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1289568
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;

    .line 1289569
    iput-object v0, v1, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->e:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    .line 1289570
    :cond_0
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->k()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1289571
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->k()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;

    .line 1289572
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->k()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1289573
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;

    .line 1289574
    iput-object v0, v1, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->g:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;

    .line 1289575
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->l()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1289576
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->l()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;

    .line 1289577
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->l()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1289578
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;

    .line 1289579
    iput-object v0, v1, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->h:Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;

    .line 1289580
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1289581
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1289563
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->j()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$NodeModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1289552
    new-instance v0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;-><init>()V

    .line 1289553
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1289554
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1289561
    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->f:Ljava/lang/String;

    .line 1289562
    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1289560
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->k()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$SocialContextModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1289559
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->l()Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel$TargetGroupModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1289558
    const v0, 0x69a6016d

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1289556
    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->i:Ljava/lang/String;

    .line 1289557
    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupsPeopleYouMayInviteGraphQLModels$PaginatedGroupsPeopleYouMayInviteFeedUnitUsersConnectionModel$EdgesModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1289555
    const v0, 0x40771b24

    return v0
.end method
