.class public final Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x28535e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1289329
    const-class v0, Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1289330
    const-class v0, Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1289331
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1289332
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1289333
    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel;->e:Ljava/lang/String;

    .line 1289334
    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1289335
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1289336
    invoke-direct {p0}, Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1289337
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1289338
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1289339
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1289340
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1289341
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1289342
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1289343
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1289344
    new-instance v0, Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel;-><init>()V

    .line 1289345
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1289346
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1289347
    const v0, 0x7f29beb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1289348
    const v0, 0x32f3c7ba

    return v0
.end method
