.class public final Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1289327
    const-class v0, Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel;

    new-instance v1, Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1289328
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1289326
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1289316
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1289317
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1289318
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1289319
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1289320
    if-eqz p0, :cond_0

    .line 1289321
    const-string p2, "client_mutation_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1289322
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1289323
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1289324
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1289325
    check-cast p1, Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel$Serializer;->a(Lcom/facebook/feedplugins/gpymi/model/GroupMemberSuggestionXoutMutationModels$GroupMemberSuggestionXoutMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
