.class public final Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4168f78e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1288693
    const-class v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1288692
    const-class v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1288690
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1288691
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288688
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1288689
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1288682
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1288683
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1288684
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1288685
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1288686
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1288687
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1288674
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1288675
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1288676
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1288677
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1288678
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;

    .line 1288679
    iput-object v0, v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1288680
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1288681
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288694
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1288665
    new-instance v0, LX/82p;

    invoke-direct {v0, p1}, LX/82p;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1288666
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1288667
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1288668
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1288669
    new-instance v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel$NodesModel;-><init>()V

    .line 1288670
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1288671
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1288672
    const v0, -0x44440bba

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1288673
    const v0, 0x285feb

    return v0
.end method
