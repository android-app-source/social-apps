.class public final Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5feddb8a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1288918
    const-class v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1288947
    const-class v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1288945
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1288946
    return-void
.end method

.method private j()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288943
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;->e:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;->e:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    .line 1288944
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;->e:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1288935
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1288936
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;->j()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1288937
    invoke-virtual {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1288938
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1288939
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1288940
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1288941
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1288942
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1288927
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1288928
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;->j()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1288929
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;->j()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    .line 1288930
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;->j()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1288931
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;

    .line 1288932
    iput-object v0, v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;->e:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    .line 1288933
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1288934
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288926
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;->j()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1288923
    new-instance v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;-><init>()V

    .line 1288924
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1288925
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288921
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;->f:Ljava/lang/String;

    .line 1288922
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1288920
    const v0, 0x3f249ce3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1288919
    const v0, -0x2d52cf72

    return v0
.end method
