.class public final Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3e13bebc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1288838
    const-class v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1288839
    const-class v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1288840
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1288841
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 4

    .prologue
    .line 1288818
    iput-object p1, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1288819
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1288820
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1288821
    if-eqz v0, :cond_0

    .line 1288822
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x8

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1288823
    :cond_0
    return-void

    .line 1288824
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1288842
    iput-object p1, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    .line 1288843
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1288844
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1288845
    if-eqz v0, :cond_0

    .line 1288846
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1288847
    :cond_0
    return-void
.end method

.method private n()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288848
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->f:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->f:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;

    .line 1288849
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->f:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;

    return-object v0
.end method

.method private o()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288850
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->g:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->g:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    .line 1288851
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->g:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    return-object v0
.end method

.method private p()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288909
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->k:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->k:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;

    .line 1288910
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->k:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288852
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1288853
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288854
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1288855
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 1288856
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1288857
    invoke-virtual {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1288858
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->n()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1288859
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->o()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1288860
    invoke-virtual {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->ey_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1288861
    invoke-virtual {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->ez_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1288862
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->p()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1288863
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1288864
    invoke-virtual {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 1288865
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1288866
    const/16 v9, 0xa

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1288867
    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 1288868
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1288869
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1288870
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->h:I

    invoke-virtual {p1, v0, v1, v10}, LX/186;->a(III)V

    .line 1288871
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1288872
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1288873
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1288874
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1288875
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1288876
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1288877
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1288878
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1288879
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1288880
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->n()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1288881
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->n()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;

    .line 1288882
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->n()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1288883
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    .line 1288884
    iput-object v0, v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->f:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;

    .line 1288885
    :cond_0
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->o()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1288886
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->o()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    .line 1288887
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->o()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1288888
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    .line 1288889
    iput-object v0, v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->g:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    .line 1288890
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->p()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1288891
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->p()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;

    .line 1288892
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->p()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1288893
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    .line 1288894
    iput-object v0, v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->k:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;

    .line 1288895
    :cond_2
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1288896
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1288897
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1288898
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    .line 1288899
    iput-object v0, v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1288900
    :cond_3
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1288901
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1288902
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1288903
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    .line 1288904
    iput-object v0, v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->n:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1288905
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1288906
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    :cond_5
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1288907
    new-instance v0, LX/82q;

    invoke-direct {v0, p1}, LX/82q;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288908
    invoke-virtual {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->ey_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1288825
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1288826
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->h:I

    .line 1288827
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1288828
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1288829
    invoke-virtual {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->ez_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1288830
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1288831
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    .line 1288832
    :goto_0
    return-void

    .line 1288833
    :cond_0
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1288834
    invoke-virtual {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1288835
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1288836
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1288837
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1288793
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1288794
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->a(Ljava/lang/String;)V

    .line 1288795
    :cond_0
    :goto_0
    return-void

    .line 1288796
    :cond_1
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1288797
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    goto :goto_0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288798
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1288799
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1288800
    new-instance v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;-><init>()V

    .line 1288801
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1288802
    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288803
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->n()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288804
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->o()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$GroupMembersModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1288805
    const v0, -0x148aa51a

    return v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 1288806
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1288807
    iget v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->h:I

    return v0
.end method

.method public final ey_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288808
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    .line 1288809
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final ez_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288810
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    .line 1288811
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1288812
    const v0, 0x41e065f

    return v0
.end method

.method public final synthetic j()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288813
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->p()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$ParentGroupModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288814
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288815
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1288816
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method public final synthetic m()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288817
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel;->r()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method
