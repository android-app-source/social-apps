.class public final Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2894b340
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1288607
    const-class v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1288608
    const-class v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1288591
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1288592
    return-void
.end method

.method private j()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288605
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;->e:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;->e:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    .line 1288606
    iget-object v0, p0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;->e:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1288599
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1288600
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;->j()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1288601
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1288602
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1288603
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1288604
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1288609
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1288610
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;->j()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1288611
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;->j()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    .line 1288612
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;->j()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1288613
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;

    .line 1288614
    iput-object v0, v1, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;->e:Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    .line 1288615
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1288616
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1288598
    invoke-direct {p0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;->j()Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel$PhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1288595
    new-instance v0, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/egolistview/model/GroupsYouShouldJoinGraphQLModels$PaginatedGroupsYouShouldJoinFeedUnitGroupsConnectionModel$EdgesModel$NodeModel$CoverPhotoModel;-><init>()V

    .line 1288596
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1288597
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1288594
    const v0, 0x31a7d9ce

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1288593
    const v0, 0x1da3a91b

    return v0
.end method
