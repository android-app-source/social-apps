.class public Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:LX/4gI;

.field public final c:Z

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/8AB;

.field public final f:Z

.field private final g:Z

.field public final h:Z

.field public final i:Z

.field private final j:Z

.field public final k:Z

.field public final l:Z

.field private final m:Z

.field private final n:Z

.field public final o:Z

.field public final p:Z

.field public final q:Z

.field public final r:Z

.field public final s:Z

.field public final t:Z

.field public final u:I

.field public final v:I

.field public final w:Z

.field public final x:I

.field public final y:I

.field public final z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1305728
    new-instance v0, LX/8A5;

    invoke-direct {v0}, LX/8A5;-><init>()V

    sput-object v0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/8A6;)V
    .locals 1

    .prologue
    .line 1305729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305730
    iget-boolean v0, p1, LX/8A6;->a:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->a:Z

    .line 1305731
    iget-object v0, p1, LX/8A6;->b:LX/4gI;

    iput-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->b:LX/4gI;

    .line 1305732
    iget-boolean v0, p1, LX/8A6;->c:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->c:Z

    .line 1305733
    iget-object v0, p1, LX/8A6;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->d:LX/0Px;

    .line 1305734
    iget-object v0, p1, LX/8A6;->e:LX/8AB;

    iput-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->e:LX/8AB;

    .line 1305735
    iget-boolean v0, p1, LX/8A6;->f:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->f:Z

    .line 1305736
    iget-boolean v0, p1, LX/8A6;->g:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->g:Z

    .line 1305737
    iget-boolean v0, p1, LX/8A6;->h:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->h:Z

    .line 1305738
    iget-boolean v0, p1, LX/8A6;->i:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->i:Z

    .line 1305739
    iget-boolean v0, p1, LX/8A6;->j:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->j:Z

    .line 1305740
    iget-boolean v0, p1, LX/8A6;->k:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->k:Z

    .line 1305741
    iget-boolean v0, p1, LX/8A6;->l:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->l:Z

    .line 1305742
    iget-boolean v0, p1, LX/8A6;->s:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->s:Z

    .line 1305743
    iget-boolean v0, p1, LX/8A6;->m:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->m:Z

    .line 1305744
    iget v0, p1, LX/8A6;->u:I

    iput v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->u:I

    .line 1305745
    iget v0, p1, LX/8A6;->v:I

    iput v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->v:I

    .line 1305746
    iget-boolean v0, p1, LX/8A6;->n:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->n:Z

    .line 1305747
    iget-boolean v0, p1, LX/8A6;->o:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->o:Z

    .line 1305748
    iget-boolean v0, p1, LX/8A6;->p:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->p:Z

    .line 1305749
    iget-boolean v0, p1, LX/8A6;->q:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->q:Z

    .line 1305750
    iget-boolean v0, p1, LX/8A6;->w:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->w:Z

    .line 1305751
    iget v0, p1, LX/8A6;->x:I

    iput v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->x:I

    .line 1305752
    iget v0, p1, LX/8A6;->y:I

    iput v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->y:I

    .line 1305753
    iget-boolean v0, p1, LX/8A6;->z:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->z:Z

    .line 1305754
    iget-boolean v0, p1, LX/8A6;->r:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->r:Z

    .line 1305755
    iget-boolean v0, p1, LX/8A6;->t:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->t:Z

    .line 1305756
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1305697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305698
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->a:Z

    .line 1305699
    invoke-static {}, LX/4gI;->values()[LX/4gI;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->b:LX/4gI;

    .line 1305700
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->c:Z

    .line 1305701
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1305702
    const-class v1, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1305703
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->d:LX/0Px;

    .line 1305704
    invoke-static {}, LX/8AB;->values()[LX/8AB;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->e:LX/8AB;

    .line 1305705
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->f:Z

    .line 1305706
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->g:Z

    .line 1305707
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->h:Z

    .line 1305708
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->i:Z

    .line 1305709
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->j:Z

    .line 1305710
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->k:Z

    .line 1305711
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->l:Z

    .line 1305712
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->m:Z

    .line 1305713
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->u:I

    .line 1305714
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->v:I

    .line 1305715
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->n:Z

    .line 1305716
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->o:Z

    .line 1305717
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->p:Z

    .line 1305718
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->q:Z

    .line 1305719
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->w:Z

    .line 1305720
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->x:I

    .line 1305721
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->y:I

    .line 1305722
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->z:Z

    .line 1305723
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->r:Z

    .line 1305724
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->s:Z

    .line 1305725
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->t:Z

    .line 1305726
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1305727
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1305676
    instance-of v1, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    if-nez v1, :cond_1

    .line 1305677
    :cond_0
    :goto_0
    return v0

    .line 1305678
    :cond_1
    check-cast p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    .line 1305679
    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->a:Z

    move v1, v1

    .line 1305680
    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->a:Z

    move v2, v2

    .line 1305681
    if-ne v1, v2, :cond_0

    .line 1305682
    iget-object v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->b:LX/4gI;

    move-object v1, v1

    .line 1305683
    iget-object v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->b:LX/4gI;

    move-object v2, v2

    .line 1305684
    if-ne v1, v2, :cond_0

    .line 1305685
    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->c:Z

    move v1, v1

    .line 1305686
    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->c:Z

    move v2, v2

    .line 1305687
    if-ne v1, v2, :cond_0

    .line 1305688
    iget-object v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->d:LX/0Px;

    move-object v1, v1

    .line 1305689
    iget-object v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->d:LX/0Px;

    move-object v2, v2

    .line 1305690
    invoke-virtual {v1, v2}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1305691
    iget-object v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->e:LX/8AB;

    move-object v1, v1

    .line 1305692
    iget-object v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->e:LX/8AB;

    move-object v2, v2

    .line 1305693
    if-ne v1, v2, :cond_0

    .line 1305694
    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->f:Z

    move v1, v1

    .line 1305695
    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->f:Z

    move v2, v2

    .line 1305696
    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->g:Z

    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->g:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->i:Z

    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->i:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->j:Z

    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->j:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->k:Z

    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->k:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->l:Z

    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->l:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->m:Z

    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->m:Z

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->u:I

    iget v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->u:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->v:I

    iget v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->v:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->n:Z

    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->n:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->o:Z

    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->o:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->p:Z

    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->p:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->q:Z

    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->q:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->w:Z

    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->w:Z

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->x:I

    iget v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->x:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->y:I

    iget v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->y:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->z:Z

    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->z:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->r:Z

    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->r:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->t:Z

    iget-boolean v2, p1, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->t:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1305675
    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->b:LX/4gI;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->d:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->e:LX/8AB;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->u:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->v:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->o:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->p:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->q:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->w:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->x:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->y:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->z:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->r:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-boolean v2, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->t:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1305674
    iget v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->u:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1305647
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305648
    iget-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->b:LX/4gI;

    invoke-virtual {v0}, LX/4gI;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1305649
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305650
    iget-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1305651
    iget-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->e:LX/8AB;

    invoke-virtual {v0}, LX/8AB;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1305652
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305653
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305654
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->h:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305655
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->i:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305656
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->j:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305657
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->k:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305658
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->l:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305659
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->m:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305660
    iget v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->u:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1305661
    iget v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->v:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1305662
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->n:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305663
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->o:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305664
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->p:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305665
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->q:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305666
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->w:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305667
    iget v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->x:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1305668
    iget v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->y:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1305669
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->z:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305670
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->r:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305671
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->s:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305672
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->t:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305673
    return-void
.end method
