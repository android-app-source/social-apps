.class public Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/8A9;

.field public final c:J

.field public final d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

.field public final e:Z

.field public final f:Z

.field public final g:Z

.field public final h:Z

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1305931
    new-instance v0, LX/8A7;

    invoke-direct {v0}, LX/8A7;-><init>()V

    sput-object v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/8AA;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1305910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305911
    iget-object v0, p1, LX/8AA;->d:LX/8A6;

    .line 1305912
    new-instance v3, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    invoke-direct {v3, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;-><init>(LX/8A6;)V

    move-object v0, v3

    .line 1305913
    iput-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    .line 1305914
    iget-object v3, p1, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iget-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    .line 1305915
    iget-boolean v4, v0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->c:Z

    move v0, v4

    .line 1305916
    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v4, p1, LX/8AA;->b:LX/8A9;

    sget-object v5, LX/8A9;->LAUNCH_COMPOSER:LX/8A9;

    if-ne v4, v5, :cond_1

    .line 1305917
    :goto_1
    if-nez v1, :cond_2

    .line 1305918
    :goto_2
    move-object v0, v3

    .line 1305919
    iput-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1305920
    iget-object v0, p1, LX/8AA;->b:LX/8A9;

    iput-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->b:LX/8A9;

    .line 1305921
    iget-wide v0, p1, LX/8AA;->c:J

    iput-wide v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->c:J

    .line 1305922
    iget-boolean v0, p1, LX/8AA;->e:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->e:Z

    .line 1305923
    iget-boolean v0, p1, LX/8AA;->f:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->f:Z

    .line 1305924
    iget-boolean v0, p1, LX/8AA;->g:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->g:Z

    .line 1305925
    iget-boolean v0, p1, LX/8AA;->h:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->h:Z

    .line 1305926
    iget-object v0, p1, LX/8AA;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->i:Ljava/lang/String;

    .line 1305927
    return-void

    :cond_0
    move v0, v2

    .line 1305928
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    .line 1305929
    :cond_2
    const-string v2, "A composer configuration must be provided in order to launch the composer"

    invoke-static {v3, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1305930
    invoke-static {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->a(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v4

    sget-object v5, LX/5RI;->MEDIA_PICKER:LX/5RI;

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->setEntryPicker(LX/5RI;)Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setLaunchLoggingParams(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisableFriendTagging()Z

    move-result v2

    if-nez v2, :cond_3

    if-eqz v0, :cond_4

    :cond_3
    const/4 v2, 0x1

    :goto_3
    invoke-virtual {v4, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    goto :goto_3
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1305888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305889
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1305890
    invoke-static {}, LX/8A9;->values()[LX/8A9;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->b:LX/8A9;

    .line 1305891
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->c:J

    .line 1305892
    const-class v0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    iput-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    .line 1305893
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->e:Z

    .line 1305894
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->f:Z

    .line 1305895
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->g:Z

    .line 1305896
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->h:Z

    .line 1305897
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->i:Ljava/lang/String;

    .line 1305898
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1305909
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1305899
    iget-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1305900
    iget-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->b:LX/8A9;

    invoke-virtual {v0}, LX/8A9;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1305901
    iget-wide v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1305902
    iget-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1305903
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305904
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305905
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305906
    iget-boolean v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->h:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305907
    iget-object v0, p0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1305908
    return-void
.end method
