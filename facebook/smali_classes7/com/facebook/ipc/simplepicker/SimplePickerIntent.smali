.class public Lcom/facebook/ipc/simplepicker/SimplePickerIntent;
.super Landroid/content/Intent;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1305757
    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 1305764
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/8AA;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1305758
    new-instance v0, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;

    invoke-direct {v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;-><init>()V

    .line 1305759
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.facebook.photos.simplepicker.launcher.SimplePickerLauncherActivity"

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1305760
    const-string v1, "extra_simple_picker_launcher_settings"

    invoke-virtual {p1}, LX/8AA;->w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1305761
    if-eqz p2, :cond_0

    .line 1305762
    const-string v1, "extra_simple_picker_launcher_waterfall_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1305763
    :cond_0
    return-object v0
.end method
