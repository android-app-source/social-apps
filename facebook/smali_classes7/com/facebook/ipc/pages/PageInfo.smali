.class public Lcom/facebook/ipc/pages/PageInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/pages/PageInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:LX/8A4;

.field public final accessToken:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "access_token"
    .end annotation
.end field

.field public final pageId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_id"
    .end annotation
.end field

.field public final pageName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final pageUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_url"
    .end annotation
.end field

.field public final permission:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "perms"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final squareProfilePicUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "square_pic_url"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1305473
    const-class v0, Lcom/facebook/ipc/pages/PageInfoDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1305474
    new-instance v0, LX/89x;

    invoke-direct {v0}, LX/89x;-><init>()V

    sput-object v0, Lcom/facebook/ipc/pages/PageInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1305475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305476
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/ipc/pages/PageInfo;->pageId:J

    .line 1305477
    iput-object v2, p0, Lcom/facebook/ipc/pages/PageInfo;->pageName:Ljava/lang/String;

    .line 1305478
    iput-object v2, p0, Lcom/facebook/ipc/pages/PageInfo;->accessToken:Ljava/lang/String;

    .line 1305479
    iput-object v2, p0, Lcom/facebook/ipc/pages/PageInfo;->squareProfilePicUrl:Ljava/lang/String;

    .line 1305480
    iput-object v2, p0, Lcom/facebook/ipc/pages/PageInfo;->permission:Ljava/util/List;

    .line 1305481
    iput-object v2, p0, Lcom/facebook/ipc/pages/PageInfo;->pageUrl:Ljava/lang/String;

    .line 1305482
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1305483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305484
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/pages/PageInfo;->pageId:J

    .line 1305485
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/pages/PageInfo;->pageName:Ljava/lang/String;

    .line 1305486
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/pages/PageInfo;->accessToken:Ljava/lang/String;

    .line 1305487
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/pages/PageInfo;->squareProfilePicUrl:Ljava/lang/String;

    .line 1305488
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/pages/PageInfo;->permission:Ljava/util/List;

    .line 1305489
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/pages/PageInfo;->pageUrl:Ljava/lang/String;

    .line 1305490
    return-void
.end method


# virtual methods
.method public final a()LX/8A4;
    .locals 2

    .prologue
    .line 1305491
    iget-object v0, p0, Lcom/facebook/ipc/pages/PageInfo;->a:LX/8A4;

    if-nez v0, :cond_0

    .line 1305492
    new-instance v0, LX/8A4;

    iget-object v1, p0, Lcom/facebook/ipc/pages/PageInfo;->permission:Ljava/util/List;

    invoke-direct {v0, v1}, LX/8A4;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/facebook/ipc/pages/PageInfo;->a:LX/8A4;

    .line 1305493
    :cond_0
    iget-object v0, p0, Lcom/facebook/ipc/pages/PageInfo;->a:LX/8A4;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1305494
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1305495
    iget-wide v0, p0, Lcom/facebook/ipc/pages/PageInfo;->pageId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1305496
    iget-object v0, p0, Lcom/facebook/ipc/pages/PageInfo;->pageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1305497
    iget-object v0, p0, Lcom/facebook/ipc/pages/PageInfo;->accessToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1305498
    iget-object v0, p0, Lcom/facebook/ipc/pages/PageInfo;->squareProfilePicUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1305499
    iget-object v0, p0, Lcom/facebook/ipc/pages/PageInfo;->permission:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1305500
    iget-object v0, p0, Lcom/facebook/ipc/pages/PageInfo;->pageUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1305501
    return-void
.end method
