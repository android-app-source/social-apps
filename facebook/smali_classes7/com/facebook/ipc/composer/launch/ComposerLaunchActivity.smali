.class public Lcom/facebook/ipc/composer/launch/ComposerLaunchActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1304924
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Landroid/content/Intent;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1304925
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/ipc/composer/launch/ComposerLaunchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "extra_ComposerLaunchActivity_session_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_ComposerLaunchActivity_configuration"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/ipc/composer/launch/ComposerLaunchActivity;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v0

    check-cast v0, LX/1Kf;

    iput-object v0, p0, Lcom/facebook/ipc/composer/launch/ComposerLaunchActivity;->p:LX/1Kf;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1304926
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1304927
    invoke-static {p0, p0}, Lcom/facebook/ipc/composer/launch/ComposerLaunchActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1304928
    if-nez p1, :cond_0

    .line 1304929
    iget-object v1, p0, Lcom/facebook/ipc/composer/launch/ComposerLaunchActivity;->p:LX/1Kf;

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/launch/ComposerLaunchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "extra_ComposerLaunchActivity_session_id"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/launch/ComposerLaunchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "extra_ComposerLaunchActivity_configuration"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1304930
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1304931
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1304932
    invoke-virtual {p0, p2, p3}, Lcom/facebook/ipc/composer/launch/ComposerLaunchActivity;->setResult(ILandroid/content/Intent;)V

    .line 1304933
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/launch/ComposerLaunchActivity;->finish()V

    .line 1304934
    return-void

    .line 1304935
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
