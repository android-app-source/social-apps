.class public Lcom/facebook/ipc/composer/intent/ComposerConfiguration_BuilderDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/common/json/FbJsonField;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1304179
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    new-instance v1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration_BuilderDeserializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration_BuilderDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1304180
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1304181
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    .line 1304182
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    invoke-virtual {p0, v0}, Lcom/facebook/common/json/FbJsonDeserializer;->init(Ljava/lang/Class;)V

    .line 1304183
    return-void
.end method


# virtual methods
.method public final getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1304184
    const-class v3, Lcom/facebook/ipc/composer/intent/ComposerConfiguration_BuilderDeserializer;

    monitor-enter v3

    .line 1304185
    :try_start_0
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration_BuilderDeserializer;->a:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 1304186
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration_BuilderDeserializer;->a:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1304187
    :cond_0
    const/4 v0, -0x1

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_1
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1304188
    invoke-super {p0, p1}, Lcom/facebook/common/json/FbJsonDeserializer;->getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v3

    .line 1304189
    :goto_1
    return-object v0

    .line 1304190
    :cond_2
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration_BuilderDeserializer;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/json/FbJsonField;

    .line 1304191
    if-eqz v0, :cond_0

    .line 1304192
    monitor-exit v3

    goto :goto_1

    .line 1304193
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1304194
    :sswitch_0
    :try_start_3
    const-string v2, "allow_dynamic_text_style"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v1, "allow_feed_only_post"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string v1, "allow_rich_text_style"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "allow_target_selection"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v1, "attached_story"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v1, "cache_id"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v1, "can_viewer_edit_post_media"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v1, "commerce_info"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v1, "composer_type"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v1, "disable_attach_to_album"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v1, "disable_friend_tagging"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xa

    goto/16 :goto_0

    :sswitch_b
    const-string v1, "disable_mentions"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xb

    goto/16 :goto_0

    :sswitch_c
    const-string v1, "disable_photos"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v1, "edit_post_feature_capabilities"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v1, "external_ref_name"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v1, "group_commerce_categories"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v1, "hide_keyboard_if_reached_minimum_height"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v1, "initial_app_attribution"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v1, "initial_attachments"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v1, "initial_composer_call_to_action"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v1, "initial_date_info"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string v1, "initial_location_info"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string v1, "initial_page_data"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x16

    goto/16 :goto_0

    :sswitch_17
    const-string v1, "initial_privacy_override"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x17

    goto/16 :goto_0

    :sswitch_18
    const-string v1, "initial_rating"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x18

    goto/16 :goto_0

    :sswitch_19
    const-string v1, "initial_rich_text_style"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x19

    goto/16 :goto_0

    :sswitch_1a
    const-string v1, "initial_share_params"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x1a

    goto/16 :goto_0

    :sswitch_1b
    const-string v1, "initial_slideshow_data"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x1b

    goto/16 :goto_0

    :sswitch_1c
    const-string v1, "initial_sticker_data"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x1c

    goto/16 :goto_0

    :sswitch_1d
    const-string v1, "initial_storyline_data"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x1d

    goto/16 :goto_0

    :sswitch_1e
    const-string v1, "initial_tagged_users"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x1e

    goto/16 :goto_0

    :sswitch_1f
    const-string v1, "initial_target_album"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x1f

    goto/16 :goto_0

    :sswitch_20
    const-string v1, "initial_target_data"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x20

    goto/16 :goto_0

    :sswitch_21
    const-string v1, "initial_text"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x21

    goto/16 :goto_0

    :sswitch_22
    const-string v1, "is_edit"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x22

    goto/16 :goto_0

    :sswitch_23
    const-string v1, "is_edit_privacy_enabled"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x23

    goto/16 :goto_0

    :sswitch_24
    const-string v1, "is_edit_tag_enabled"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x24

    goto/16 :goto_0

    :sswitch_25
    const-string v1, "is_fire_and_forget"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x25

    goto/16 :goto_0

    :sswitch_26
    const-string v1, "is_group_member_bio_post"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x26

    goto/16 :goto_0

    :sswitch_27
    const-string v1, "is_placelist_post"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x27

    goto/16 :goto_0

    :sswitch_28
    const-string v1, "is_throwback_post"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x28

    goto/16 :goto_0

    :sswitch_29
    const-string v1, "launch_logging_params"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x29

    goto/16 :goto_0

    :sswitch_2a
    const-string v1, "legacy_api_story_id"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x2a

    goto/16 :goto_0

    :sswitch_2b
    const-string v1, "minutiae_object_tag"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x2b

    goto/16 :goto_0

    :sswitch_2c
    const-string v1, "nectar_module"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x2c

    goto/16 :goto_0

    :sswitch_2d
    const-string v1, "og_mechanism"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x2d

    goto/16 :goto_0

    :sswitch_2e
    const-string v1, "og_surface"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x2e

    goto/16 :goto_0

    :sswitch_2f
    const-string v1, "platform_configuration"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x2f

    goto/16 :goto_0

    :sswitch_30
    const-string v1, "plugin_config"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x30

    goto/16 :goto_0

    :sswitch_31
    const-string v1, "product_item_attachment"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x31

    goto/16 :goto_0

    :sswitch_32
    const-string v1, "reaction_surface"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x32

    goto/16 :goto_0

    :sswitch_33
    const-string v1, "reaction_unit_id"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x33

    goto/16 :goto_0

    :sswitch_34
    const-string v1, "should_picker_support_live_camera"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x34

    goto/16 :goto_0

    :sswitch_35
    const-string v1, "should_post_to_marketplace_by_default"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x35

    goto/16 :goto_0

    :sswitch_36
    const-string v1, "show_page_voice_switcher"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x36

    goto/16 :goto_0

    :sswitch_37
    const-string v1, "souvenir_unique_id"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x37

    goto/16 :goto_0

    :sswitch_38
    const-string v1, "story_id"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x38

    goto/16 :goto_0

    :sswitch_39
    const-string v1, "use_inspiration_cam"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x39

    goto/16 :goto_0

    :sswitch_3a
    const-string v1, "use_optimistic_posting"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x3a

    goto/16 :goto_0

    :sswitch_3b
    const-string v1, "use_publish_experiment"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v0, 0x3b

    goto/16 :goto_0

    .line 1304195
    :pswitch_0
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setAllowDynamicTextStyle"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 1304196
    :goto_2
    :try_start_4
    sget-object v1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration_BuilderDeserializer;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1304197
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 1304198
    :pswitch_1
    :try_start_5
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setAllowFeedOnlyPost"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 1304199
    :pswitch_2
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setAllowRichTextStyle"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 1304200
    :pswitch_3
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setAllowTargetSelection"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 1304201
    :pswitch_4
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setAttachedStory"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/graphql/model/GraphQLStory;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 1304202
    :pswitch_5
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setCacheId"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 1304203
    :pswitch_6
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setCanViewerEditPostMedia"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304204
    :pswitch_7
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setCommerceInfo"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304205
    :pswitch_8
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setComposerType"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, LX/2rt;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304206
    :pswitch_9
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setDisableAttachToAlbum"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304207
    :pswitch_a
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setDisableFriendTagging"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304208
    :pswitch_b
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setDisableMentions"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304209
    :pswitch_c
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setDisablePhotos"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304210
    :pswitch_d
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setEditPostFeatureCapabilities"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, LX/0Px;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const-class v1, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    invoke-static {v0, v1}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;Ljava/lang/Class;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304211
    :pswitch_e
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setExternalRefName"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304212
    :pswitch_f
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setGroupCommerceCategories"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, LX/0Px;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const-class v1, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;

    invoke-static {v0, v1}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;Ljava/lang/Class;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304213
    :pswitch_10
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setHideKeyboardIfReachedMinimumHeight"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304214
    :pswitch_11
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialAppAttribution"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/share/model/ComposerAppAttribution;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304215
    :pswitch_12
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialAttachments"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, LX/0Px;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const-class v1, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-static {v0, v1}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;Ljava/lang/Class;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304216
    :pswitch_13
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialComposerCallToAction"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304217
    :pswitch_14
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialDateInfo"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304218
    :pswitch_15
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialLocationInfo"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304219
    :pswitch_16
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialPageData"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304220
    :pswitch_17
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialPrivacyOverride"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304221
    :pswitch_18
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialRating"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304222
    :pswitch_19
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialRichTextStyle"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304223
    :pswitch_1a
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialShareParams"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304224
    :pswitch_1b
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialSlideshowData"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304225
    :pswitch_1c
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialStickerData"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/model/ComposerStickerData;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304226
    :pswitch_1d
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialStorylineData"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304227
    :pswitch_1e
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialTaggedUsers"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, LX/0Px;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const-class v1, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    invoke-static {v0, v1}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;Ljava/lang/Class;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304228
    :pswitch_1f
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialTargetAlbum"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/graphql/model/GraphQLAlbum;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304229
    :pswitch_20
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialTargetData"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304230
    :pswitch_21
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setInitialText"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304231
    :pswitch_22
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setIsEdit"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304232
    :pswitch_23
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setIsEditPrivacyEnabled"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304233
    :pswitch_24
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setIsEditTagEnabled"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304234
    :pswitch_25
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setIsFireAndForget"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304235
    :pswitch_26
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setIsGroupMemberBioPost"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304236
    :pswitch_27
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setIsPlacelistPost"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304237
    :pswitch_28
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setIsThrowbackPost"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304238
    :pswitch_29
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setLaunchLoggingParams"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304239
    :pswitch_2a
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setLegacyApiStoryId"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304240
    :pswitch_2b
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setMinutiaeObjectTag"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304241
    :pswitch_2c
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setNectarModule"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304242
    :pswitch_2d
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setOgMechanism"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304243
    :pswitch_2e
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setOgSurface"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304244
    :pswitch_2f
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setPlatformConfiguration"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304245
    :pswitch_30
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setPluginConfig"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304246
    :pswitch_31
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setProductItemAttachment"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304247
    :pswitch_32
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setReactionSurface"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304248
    :pswitch_33
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setReactionUnitId"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304249
    :pswitch_34
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setShouldPickerSupportLiveCamera"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304250
    :pswitch_35
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setShouldPostToMarketplaceByDefault"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304251
    :pswitch_36
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setShowPageVoiceSwitcher"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304252
    :pswitch_37
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setSouvenirUniqueId"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304253
    :pswitch_38
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setStoryId"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304254
    :pswitch_39
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setUseInspirationCam"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304255
    :pswitch_3a
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setUseOptimisticPosting"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto/16 :goto_2

    .line 1304256
    :pswitch_3b
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const-string v1, "setUsePublishExperiment"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Method;)Lcom/facebook/common/json/FbJsonField;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    goto/16 :goto_2

    .line 1304257
    :catch_0
    move-exception v0

    .line 1304258
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7a1f7f8e -> :sswitch_29
        -0x7575c7c0 -> :sswitch_f
        -0x74b686e3 -> :sswitch_20
        -0x731276b6 -> :sswitch_23
        -0x6dcff166 -> :sswitch_4
        -0x62f4e52e -> :sswitch_2c
        -0x62a7032b -> :sswitch_0
        -0x5fb76be1 -> :sswitch_16
        -0x5a2a4058 -> :sswitch_1
        -0x5653eef2 -> :sswitch_9
        -0x55f03604 -> :sswitch_6
        -0x4e85b6ba -> :sswitch_2e
        -0x4e26de32 -> :sswitch_30
        -0x4e07ba17 -> :sswitch_25
        -0x4b2ebde5 -> :sswitch_1d
        -0x48fbdf99 -> :sswitch_19
        -0x42536d57 -> :sswitch_d
        -0x364f25d1 -> :sswitch_27
        -0x30083dbe -> :sswitch_39
        -0x2e141c8a -> :sswitch_1b
        -0x2ce58cc0 -> :sswitch_b
        -0x2b7e20ff -> :sswitch_1a
        -0x26e6dbc2 -> :sswitch_35
        -0x25aad779 -> :sswitch_28
        -0x247b44cb -> :sswitch_12
        -0x223fdca4 -> :sswitch_1f
        -0x1c943c6e -> :sswitch_7
        -0x19d68508 -> :sswitch_5
        -0x18686eb8 -> :sswitch_21
        -0x123a1b9a -> :sswitch_11
        -0x11600e20 -> :sswitch_2a
        -0xc2a0181 -> :sswitch_31
        -0x407d6c3 -> :sswitch_15
        0x5e03497 -> :sswitch_32
        0xc9b7694 -> :sswitch_3
        0x1151a290 -> :sswitch_1e
        0x1d8c92e5 -> :sswitch_3b
        0x1d95d35c -> :sswitch_24
        0x2e50fdb2 -> :sswitch_2d
        0x33f8ecdd -> :sswitch_13
        0x36a2e67d -> :sswitch_2b
        0x3b9fc0ab -> :sswitch_e
        0x4df79e3e -> :sswitch_17
        0x5013bc2c -> :sswitch_10
        0x59013928 -> :sswitch_3a
        0x5c5462f8 -> :sswitch_18
        0x5cc6d764 -> :sswitch_14
        0x5fa9ff19 -> :sswitch_37
        0x6124cdb8 -> :sswitch_c
        0x62c312bc -> :sswitch_36
        0x634241a0 -> :sswitch_33
        0x66396a2a -> :sswitch_2f
        0x6662d8a5 -> :sswitch_38
        0x7176de87 -> :sswitch_1c
        0x72d63af9 -> :sswitch_8
        0x7565fa43 -> :sswitch_34
        0x79a5c26c -> :sswitch_2
        0x7badce67 -> :sswitch_26
        0x7c15c3ff -> :sswitch_22
        0x7e141e4b -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
    .end packed-switch
.end method
