.class public Lcom/facebook/ipc/composer/intent/PlatformConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/intent/PlatformConfigurationDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/intent/PlatformConfigurationSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/intent/PlatformConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final captionForShareLink:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "caption_for_share_link"
    .end annotation
.end field

.field public final dataFailuresFatal:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "data_failures_fatal"
    .end annotation
.end field

.field public final descriptionForShareLink:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "description_for_share_link"
    .end annotation
.end field

.field public final hashtag:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "hashtag"
    .end annotation
.end field

.field public final insightsPlatformRef:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "insights_platform_ref"
    .end annotation
.end field

.field public final nameForShareLink:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name_for_share_link"
    .end annotation
.end field

.field public final ogActionJsonForRobotext:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "og_action_json_for_robotext"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final ogActionType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "og_action_type"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final pictureForShareLink:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "picture_for_share_link"
    .end annotation
.end field

.field public final platformSharePreview:Lcom/facebook/ipc/composer/intent/SharePreview;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "platform_share_preview"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1304665
    const-class v0, Lcom/facebook/ipc/composer/intent/PlatformConfigurationDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1304664
    const-class v0, Lcom/facebook/ipc/composer/intent/PlatformConfigurationSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1304663
    new-instance v0, LX/31u;

    invoke-direct {v0}, LX/31u;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1304623
    new-instance v0, LX/2ro;

    invoke-direct {v0}, LX/2ro;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;-><init>(LX/2ro;)V

    .line 1304624
    return-void
.end method

.method public constructor <init>(LX/2ro;)V
    .locals 1

    .prologue
    .line 1304651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304652
    iget-boolean v0, p1, LX/2ro;->c:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->dataFailuresFatal:Z

    .line 1304653
    iget-object v0, p1, LX/2ro;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->insightsPlatformRef:Ljava/lang/String;

    .line 1304654
    iget-object v0, p1, LX/2ro;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->hashtag:Ljava/lang/String;

    .line 1304655
    iget-object v0, p1, LX/2ro;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->ogActionJsonForRobotext:Ljava/lang/String;

    .line 1304656
    iget-object v0, p1, LX/2ro;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->ogActionType:Ljava/lang/String;

    .line 1304657
    iget-object v0, p1, LX/2ro;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->nameForShareLink:Ljava/lang/String;

    .line 1304658
    iget-object v0, p1, LX/2ro;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->captionForShareLink:Ljava/lang/String;

    .line 1304659
    iget-object v0, p1, LX/2ro;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->pictureForShareLink:Ljava/lang/String;

    .line 1304660
    iget-object v0, p1, LX/2ro;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->descriptionForShareLink:Ljava/lang/String;

    .line 1304661
    iget-object v0, p1, LX/2ro;->j:Lcom/facebook/ipc/composer/intent/SharePreview;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->platformSharePreview:Lcom/facebook/ipc/composer/intent/SharePreview;

    .line 1304662
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1304639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304640
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->dataFailuresFatal:Z

    .line 1304641
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->insightsPlatformRef:Ljava/lang/String;

    .line 1304642
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->hashtag:Ljava/lang/String;

    .line 1304643
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->ogActionJsonForRobotext:Ljava/lang/String;

    .line 1304644
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->ogActionType:Ljava/lang/String;

    .line 1304645
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->nameForShareLink:Ljava/lang/String;

    .line 1304646
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->captionForShareLink:Ljava/lang/String;

    .line 1304647
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->pictureForShareLink:Ljava/lang/String;

    .line 1304648
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->descriptionForShareLink:Ljava/lang/String;

    .line 1304649
    const-class v0, Lcom/facebook/ipc/composer/intent/SharePreview;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/SharePreview;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->platformSharePreview:Lcom/facebook/ipc/composer/intent/SharePreview;

    .line 1304650
    return-void
.end method


# virtual methods
.method public final a()LX/2ro;
    .locals 1

    .prologue
    .line 1304638
    new-instance v0, LX/2ro;

    invoke-direct {v0, p0}, LX/2ro;-><init>(Lcom/facebook/ipc/composer/intent/PlatformConfiguration;)V

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1304637
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->ogActionJsonForRobotext:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->ogActionType:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1304636
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1304625
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->dataFailuresFatal:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1304626
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->insightsPlatformRef:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304627
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->hashtag:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304628
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->ogActionJsonForRobotext:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304629
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->ogActionType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304630
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->nameForShareLink:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304631
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->captionForShareLink:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304632
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->pictureForShareLink:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304633
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->descriptionForShareLink:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304634
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->platformSharePreview:Lcom/facebook/ipc/composer/intent/SharePreview;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1304635
    return-void
.end method
