.class public Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1304778
    const-class v0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    new-instance v1, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1304779
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1304777
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1304771
    if-nez p0, :cond_0

    .line 1304772
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1304773
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1304774
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfigSerializer;->b(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/0nX;LX/0my;)V

    .line 1304775
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1304776
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1304767
    const-string v0, "persist_key"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->mPersistenceKey:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304768
    const-string v0, "data"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->mData:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304769
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1304770
    check-cast p1, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfigSerializer;->a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/0nX;LX/0my;)V

    return-void
.end method
