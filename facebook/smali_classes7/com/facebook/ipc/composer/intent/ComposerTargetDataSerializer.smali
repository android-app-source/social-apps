.class public Lcom/facebook/ipc/composer/intent/ComposerTargetDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/intent/ComposerTargetData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1304613
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    new-instance v1, Lcom/facebook/ipc/composer/intent/ComposerTargetDataSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/intent/ComposerTargetDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1304614
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1304606
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1304607
    if-nez p0, :cond_0

    .line 1304608
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1304609
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1304610
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerTargetDataSerializer;->b(Lcom/facebook/ipc/composer/intent/ComposerTargetData;LX/0nX;LX/0my;)V

    .line 1304611
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1304612
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/intent/ComposerTargetData;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1304600
    const-string v0, "target_id"

    iget-wide v2, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1304601
    const-string v0, "target_type"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304602
    const-string v0, "target_name"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304603
    const-string v0, "target_profile_pic_url"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetProfilePicUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304604
    const-string v0, "target_privacy"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetPrivacy:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304605
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1304599
    check-cast p1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/intent/ComposerTargetDataSerializer;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;LX/0nX;LX/0my;)V

    return-void
.end method
