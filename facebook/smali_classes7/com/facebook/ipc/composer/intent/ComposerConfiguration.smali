.class public Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/intent/ComposerConfigurationSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:LX/89C;


# instance fields
.field public final A:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final B:Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final C:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final D:Lcom/facebook/ipc/composer/model/ComposerStickerData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final E:Lcom/facebook/ipc/composer/model/ComposerStorylineData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final F:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field

.field public final G:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final H:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public final I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public final J:Z

.field public final K:Z

.field public final L:Z

.field public final M:Z

.field public final N:Z

.field public final O:Z

.field public final P:Z

.field public final Q:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

.field public final R:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final S:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final T:Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final U:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final V:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final X:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final Y:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final Z:Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final aa:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final ab:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final ac:Z

.field public final ad:Z

.field public final ae:Z

.field public final af:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final ag:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final ah:Z

.field public final ai:Z

.field public final aj:Z

.field public final b:Z

.field public final c:Z

.field public final d:Z

.field public final e:Z

.field public final f:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Z

.field public final i:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

.field public final j:LX/2rt;

.field public final k:Z

.field public final l:Z

.field public final m:Z

.field public final n:Z

.field public final o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;",
            ">;"
        }
    .end annotation
.end field

.field public final p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groupcommerce/model/GroupCommerceCategory;",
            ">;"
        }
    .end annotation
.end field

.field public final r:Z

.field public final s:Lcom/facebook/share/model/ComposerAppAttribution;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final u:Lcom/facebook/ipc/composer/model/ComposerCallToAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final v:Lcom/facebook/ipc/composer/model/ComposerDateInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final w:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

.field public final x:Lcom/facebook/ipc/composer/intent/ComposerPageData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final y:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final z:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1304063
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1304062
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfigurationSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1304060
    new-instance v0, LX/891;

    invoke-direct {v0}, LX/891;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 1304061
    new-instance v0, LX/89C;

    invoke-direct {v0}, LX/89C;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a:LX/89C;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1303900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1303901
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->b:Z

    .line 1303902
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->c:Z

    .line 1303903
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->d:Z

    .line 1303904
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->e:Z

    .line 1303905
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    .line 1303906
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1303907
    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    .line 1303908
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->g:Ljava/lang/String;

    .line 1303909
    :goto_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->h:Z

    .line 1303910
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->i:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    .line 1303911
    invoke-static {}, LX/2rt;->values()[LX/2rt;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->j:LX/2rt;

    .line 1303912
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->k:Z

    .line 1303913
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->l:Z

    .line 1303914
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->m:Z

    .line 1303915
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->n:Z

    .line 1303916
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v3, v0, [Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    move v0, v2

    .line 1303917
    :goto_b
    array-length v4, v3

    if-ge v0, v4, :cond_b

    .line 1303918
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->values()[Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    aget-object v4, v4, v5

    .line 1303919
    aput-object v4, v3, v0

    .line 1303920
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_0
    move v0, v2

    .line 1303921
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1303922
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 1303923
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1303924
    goto :goto_3

    .line 1303925
    :cond_4
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->f:Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_4

    .line 1303926
    :cond_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->g:Ljava/lang/String;

    goto :goto_5

    :cond_6
    move v0, v2

    .line 1303927
    goto :goto_6

    :cond_7
    move v0, v2

    .line 1303928
    goto :goto_7

    :cond_8
    move v0, v2

    .line 1303929
    goto :goto_8

    :cond_9
    move v0, v2

    .line 1303930
    goto :goto_9

    :cond_a
    move v0, v2

    .line 1303931
    goto :goto_a

    .line 1303932
    :cond_b
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->o:LX/0Px;

    .line 1303933
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_c

    .line 1303934
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->p:Ljava/lang/String;

    .line 1303935
    :goto_c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/groupcommerce/model/GroupCommerceCategory;

    move v3, v2

    .line 1303936
    :goto_d
    array-length v0, v4

    if-ge v3, v0, :cond_d

    .line 1303937
    sget-object v0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;

    .line 1303938
    aput-object v0, v4, v3

    .line 1303939
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_d

    .line 1303940
    :cond_c
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->p:Ljava/lang/String;

    goto :goto_c

    .line 1303941
    :cond_d
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->q:LX/0Px;

    .line 1303942
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_e

    move v0, v1

    :goto_e
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->r:Z

    .line 1303943
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_f

    .line 1303944
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->s:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1303945
    :goto_f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/composer/attachments/ComposerAttachment;

    move v3, v2

    .line 1303946
    :goto_10
    array-length v0, v4

    if-ge v3, v0, :cond_10

    .line 1303947
    sget-object v0, Lcom/facebook/composer/attachments/ComposerAttachment;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1303948
    aput-object v0, v4, v3

    .line 1303949
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_10

    :cond_e
    move v0, v2

    .line 1303950
    goto :goto_e

    .line 1303951
    :cond_f
    sget-object v0, Lcom/facebook/share/model/ComposerAppAttribution;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->s:Lcom/facebook/share/model/ComposerAppAttribution;

    goto :goto_f

    .line 1303952
    :cond_10
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->t:LX/0Px;

    .line 1303953
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_11

    .line 1303954
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->u:Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    .line 1303955
    :goto_11
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_12

    .line 1303956
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->v:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    .line 1303957
    :goto_12
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->w:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1303958
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_13

    .line 1303959
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 1303960
    :goto_13
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_14

    .line 1303961
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->y:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1303962
    :goto_14
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->z:I

    .line 1303963
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_15

    .line 1303964
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->A:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1303965
    :goto_15
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_16

    .line 1303966
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->B:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1303967
    :goto_16
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_17

    .line 1303968
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->C:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 1303969
    :goto_17
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_18

    .line 1303970
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->D:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 1303971
    :goto_18
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_19

    .line 1303972
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->E:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    .line 1303973
    :goto_19
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move v3, v2

    .line 1303974
    :goto_1a
    array-length v0, v4

    if-ge v3, v0, :cond_1a

    .line 1303975
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1303976
    aput-object v0, v4, v3

    .line 1303977
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1a

    .line 1303978
    :cond_11
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->u:Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    goto :goto_11

    .line 1303979
    :cond_12
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->v:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    goto :goto_12

    .line 1303980
    :cond_13
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    goto :goto_13

    .line 1303981
    :cond_14
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->y:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    goto :goto_14

    .line 1303982
    :cond_15
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->A:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    goto :goto_15

    .line 1303983
    :cond_16
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->B:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    goto :goto_16

    .line 1303984
    :cond_17
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->C:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    goto :goto_17

    .line 1303985
    :cond_18
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerStickerData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerStickerData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->D:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    goto :goto_18

    .line 1303986
    :cond_19
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->E:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    goto :goto_19

    .line 1303987
    :cond_1a
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->F:LX/0Px;

    .line 1303988
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1b

    .line 1303989
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->G:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1303990
    :goto_1b
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->H:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1303991
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1303992
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1c

    move v0, v1

    :goto_1c
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->J:Z

    .line 1303993
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1d

    move v0, v1

    :goto_1d
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->K:Z

    .line 1303994
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1e

    move v0, v1

    :goto_1e
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->L:Z

    .line 1303995
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1f

    move v0, v1

    :goto_1f
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->M:Z

    .line 1303996
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_20

    move v0, v1

    :goto_20
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->N:Z

    .line 1303997
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_21

    move v0, v1

    :goto_21
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->O:Z

    .line 1303998
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_22

    move v0, v1

    :goto_22
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->P:Z

    .line 1303999
    sget-object v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Q:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    .line 1304000
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_23

    .line 1304001
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->R:Ljava/lang/String;

    .line 1304002
    :goto_23
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_24

    .line 1304003
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->S:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 1304004
    :goto_24
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_25

    .line 1304005
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->T:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1304006
    :goto_25
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_26

    .line 1304007
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->U:Ljava/lang/String;

    .line 1304008
    :goto_26
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_27

    .line 1304009
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->V:Ljava/lang/String;

    .line 1304010
    :goto_27
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_28

    .line 1304011
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->W:Ljava/lang/String;

    .line 1304012
    :goto_28
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_29

    .line 1304013
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->X:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    .line 1304014
    :goto_29
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2a

    .line 1304015
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Y:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    .line 1304016
    :goto_2a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2b

    .line 1304017
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Z:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1304018
    :goto_2b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2c

    .line 1304019
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aa:Ljava/lang/String;

    .line 1304020
    :goto_2c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2d

    .line 1304021
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ab:Ljava/lang/String;

    .line 1304022
    :goto_2d
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2e

    move v0, v1

    :goto_2e
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ac:Z

    .line 1304023
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2f

    move v0, v1

    :goto_2f
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ad:Z

    .line 1304024
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_30

    move v0, v1

    :goto_30
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ae:Z

    .line 1304025
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_31

    .line 1304026
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->af:Ljava/lang/String;

    .line 1304027
    :goto_31
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_32

    .line 1304028
    iput-object v6, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ag:Ljava/lang/String;

    .line 1304029
    :goto_32
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_33

    move v0, v1

    :goto_33
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ah:Z

    .line 1304030
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_34

    move v0, v1

    :goto_34
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ai:Z

    .line 1304031
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_35

    :goto_35
    iput-boolean v1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aj:Z

    .line 1304032
    return-void

    .line 1304033
    :cond_1b
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->G:Lcom/facebook/graphql/model/GraphQLAlbum;

    goto/16 :goto_1b

    :cond_1c
    move v0, v2

    .line 1304034
    goto/16 :goto_1c

    :cond_1d
    move v0, v2

    .line 1304035
    goto/16 :goto_1d

    :cond_1e
    move v0, v2

    .line 1304036
    goto/16 :goto_1e

    :cond_1f
    move v0, v2

    .line 1304037
    goto/16 :goto_1f

    :cond_20
    move v0, v2

    .line 1304038
    goto/16 :goto_20

    :cond_21
    move v0, v2

    .line 1304039
    goto/16 :goto_21

    :cond_22
    move v0, v2

    .line 1304040
    goto/16 :goto_22

    .line 1304041
    :cond_23
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->R:Ljava/lang/String;

    goto/16 :goto_23

    .line 1304042
    :cond_24
    sget-object v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->S:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    goto/16 :goto_24

    .line 1304043
    :cond_25
    sget-object v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->T:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    goto/16 :goto_25

    .line 1304044
    :cond_26
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->U:Ljava/lang/String;

    goto/16 :goto_26

    .line 1304045
    :cond_27
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->V:Ljava/lang/String;

    goto/16 :goto_27

    .line 1304046
    :cond_28
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->W:Ljava/lang/String;

    goto/16 :goto_28

    .line 1304047
    :cond_29
    sget-object v0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->X:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    goto/16 :goto_29

    .line 1304048
    :cond_2a
    sget-object v0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Y:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    goto/16 :goto_2a

    .line 1304049
    :cond_2b
    sget-object v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Z:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    goto/16 :goto_2b

    .line 1304050
    :cond_2c
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aa:Ljava/lang/String;

    goto/16 :goto_2c

    .line 1304051
    :cond_2d
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ab:Ljava/lang/String;

    goto/16 :goto_2d

    :cond_2e
    move v0, v2

    .line 1304052
    goto/16 :goto_2e

    :cond_2f
    move v0, v2

    .line 1304053
    goto/16 :goto_2f

    :cond_30
    move v0, v2

    .line 1304054
    goto/16 :goto_30

    .line 1304055
    :cond_31
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->af:Ljava/lang/String;

    goto/16 :goto_31

    .line 1304056
    :cond_32
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ag:Ljava/lang/String;

    goto/16 :goto_32

    :cond_33
    move v0, v2

    .line 1304057
    goto/16 :goto_33

    :cond_34
    move v0, v2

    .line 1304058
    goto/16 :goto_34

    :cond_35
    move v1, v2

    .line 1304059
    goto/16 :goto_35
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V
    .locals 3

    .prologue
    .line 1303829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1303830
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->b:Z

    .line 1303831
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->j:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->c:Z

    .line 1303832
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->k:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->d:Z

    .line 1303833
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->l:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->e:Z

    .line 1303834
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->m:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1303835
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->g:Ljava/lang/String;

    .line 1303836
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->o:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->h:Z

    .line 1303837
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->p:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->i:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    .line 1303838
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->q:LX/2rt;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rt;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->j:LX/2rt;

    .line 1303839
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->r:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->k:Z

    .line 1303840
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->s:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->l:Z

    .line 1303841
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->t:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->m:Z

    .line 1303842
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->u:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->n:Z

    .line 1303843
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->v:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->o:LX/0Px;

    .line 1303844
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->p:Ljava/lang/String;

    .line 1303845
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->x:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->q:LX/0Px;

    .line 1303846
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->y:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->r:Z

    .line 1303847
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->z:Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->s:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1303848
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->A:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->t:LX/0Px;

    .line 1303849
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->B:Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->u:Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    .line 1303850
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->C:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->v:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    .line 1303851
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->D:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->w:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1303852
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->E:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 1303853
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->F:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->y:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1303854
    iget v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->G:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->z:I

    .line 1303855
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->H:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->A:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1303856
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->I:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->B:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1303857
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->J:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->C:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 1303858
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->K:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->D:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 1303859
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->L:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->E:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    .line 1303860
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->M:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->F:LX/0Px;

    .line 1303861
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->N:Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->G:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1303862
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->O:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->H:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1303863
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->P:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1303864
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->Q:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->J:Z

    .line 1303865
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->R:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->K:Z

    .line 1303866
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->S:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->L:Z

    .line 1303867
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->T:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->M:Z

    .line 1303868
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->U:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->N:Z

    .line 1303869
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->V:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->O:Z

    .line 1303870
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->W:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->P:Z

    .line 1303871
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->X:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Q:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    .line 1303872
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->Y:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->R:Ljava/lang/String;

    .line 1303873
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->Z:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->S:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 1303874
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->aa:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->T:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1303875
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ab:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->U:Ljava/lang/String;

    .line 1303876
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ac:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->V:Ljava/lang/String;

    .line 1303877
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ad:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->W:Ljava/lang/String;

    .line 1303878
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ae:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->X:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    .line 1303879
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->af:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Y:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    .line 1303880
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ag:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Z:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1303881
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ah:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aa:Ljava/lang/String;

    .line 1303882
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ai:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ab:Ljava/lang/String;

    .line 1303883
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->aj:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ac:Z

    .line 1303884
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ak:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ad:Z

    .line 1303885
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->al:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ae:Z

    .line 1303886
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->am:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->af:Ljava/lang/String;

    .line 1303887
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->an:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ag:Ljava/lang/String;

    .line 1303888
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ao:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ah:Z

    .line 1303889
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ap:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ai:Z

    .line 1303890
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->aq:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aj:Z

    .line 1303891
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v1

    sget-object v2, LX/2rt;->SHARE:LX/2rt;

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1303892
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v1

    sget-object v2, LX/2rt;->RECOMMENDATION:LX/2rt;

    if-ne v1, v2, :cond_1

    .line 1303893
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getOgMechanism()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1303894
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getOgSurface()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1303895
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1303896
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-wide v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1303897
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1303898
    :cond_1
    return-void

    .line 1303899
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 2

    .prologue
    .line 1303828
    new-instance v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;-><init>(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 2

    .prologue
    .line 1303827
    new-instance v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    invoke-direct {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303826
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->S:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    return-object v0
.end method

.method public canViewerEditPostMedia()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "can_viewer_edit_post_media"
    .end annotation

    .prologue
    .line 1303690
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->h:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1303824
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1303697
    if-ne p0, p1, :cond_1

    .line 1303698
    :cond_0
    :goto_0
    return v0

    .line 1303699
    :cond_1
    instance-of v2, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    if-nez v2, :cond_2

    move v0, v1

    .line 1303700
    goto :goto_0

    .line 1303701
    :cond_2
    check-cast p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1303702
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->b:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->b:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1303703
    goto :goto_0

    .line 1303704
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->c:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->c:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1303705
    goto :goto_0

    .line 1303706
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->d:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->d:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1303707
    goto :goto_0

    .line 1303708
    :cond_5
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->e:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->e:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1303709
    goto :goto_0

    .line 1303710
    :cond_6
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->f:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->f:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1303711
    goto :goto_0

    .line 1303712
    :cond_7
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1303713
    goto :goto_0

    .line 1303714
    :cond_8
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->h:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->h:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1303715
    goto :goto_0

    .line 1303716
    :cond_9
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->i:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->i:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 1303717
    goto :goto_0

    .line 1303718
    :cond_a
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->j:LX/2rt;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->j:LX/2rt;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 1303719
    goto :goto_0

    .line 1303720
    :cond_b
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->k:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->k:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1303721
    goto :goto_0

    .line 1303722
    :cond_c
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->l:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->l:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1303723
    goto :goto_0

    .line 1303724
    :cond_d
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->m:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->m:Z

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 1303725
    goto :goto_0

    .line 1303726
    :cond_e
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->n:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->n:Z

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 1303727
    goto :goto_0

    .line 1303728
    :cond_f
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->o:LX/0Px;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->o:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 1303729
    goto/16 :goto_0

    .line 1303730
    :cond_10
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->p:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->p:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    .line 1303731
    goto/16 :goto_0

    .line 1303732
    :cond_11
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->q:LX/0Px;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->q:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    move v0, v1

    .line 1303733
    goto/16 :goto_0

    .line 1303734
    :cond_12
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->r:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->r:Z

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 1303735
    goto/16 :goto_0

    .line 1303736
    :cond_13
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->s:Lcom/facebook/share/model/ComposerAppAttribution;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->s:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    move v0, v1

    .line 1303737
    goto/16 :goto_0

    .line 1303738
    :cond_14
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->t:LX/0Px;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->t:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    move v0, v1

    .line 1303739
    goto/16 :goto_0

    .line 1303740
    :cond_15
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->u:Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->u:Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move v0, v1

    .line 1303741
    goto/16 :goto_0

    .line 1303742
    :cond_16
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->v:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->v:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    move v0, v1

    .line 1303743
    goto/16 :goto_0

    .line 1303744
    :cond_17
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->w:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->w:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    move v0, v1

    .line 1303745
    goto/16 :goto_0

    .line 1303746
    :cond_18
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    move v0, v1

    .line 1303747
    goto/16 :goto_0

    .line 1303748
    :cond_19
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->y:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->y:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    move v0, v1

    .line 1303749
    goto/16 :goto_0

    .line 1303750
    :cond_1a
    iget v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->z:I

    iget v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->z:I

    if-eq v2, v3, :cond_1b

    move v0, v1

    .line 1303751
    goto/16 :goto_0

    .line 1303752
    :cond_1b
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->A:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->A:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    move v0, v1

    .line 1303753
    goto/16 :goto_0

    .line 1303754
    :cond_1c
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->B:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->B:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    move v0, v1

    .line 1303755
    goto/16 :goto_0

    .line 1303756
    :cond_1d
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->C:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->C:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    move v0, v1

    .line 1303757
    goto/16 :goto_0

    .line 1303758
    :cond_1e
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->D:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->D:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    move v0, v1

    .line 1303759
    goto/16 :goto_0

    .line 1303760
    :cond_1f
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->E:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->E:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    move v0, v1

    .line 1303761
    goto/16 :goto_0

    .line 1303762
    :cond_20
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->F:LX/0Px;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->F:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_21

    move v0, v1

    .line 1303763
    goto/16 :goto_0

    .line 1303764
    :cond_21
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->G:Lcom/facebook/graphql/model/GraphQLAlbum;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->G:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    move v0, v1

    .line 1303765
    goto/16 :goto_0

    .line 1303766
    :cond_22
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->H:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->H:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    move v0, v1

    .line 1303767
    goto/16 :goto_0

    .line 1303768
    :cond_23
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_24

    move v0, v1

    .line 1303769
    goto/16 :goto_0

    .line 1303770
    :cond_24
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->J:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->J:Z

    if-eq v2, v3, :cond_25

    move v0, v1

    .line 1303771
    goto/16 :goto_0

    .line 1303772
    :cond_25
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->K:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->K:Z

    if-eq v2, v3, :cond_26

    move v0, v1

    .line 1303773
    goto/16 :goto_0

    .line 1303774
    :cond_26
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->L:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->L:Z

    if-eq v2, v3, :cond_27

    move v0, v1

    .line 1303775
    goto/16 :goto_0

    .line 1303776
    :cond_27
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->M:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->M:Z

    if-eq v2, v3, :cond_28

    move v0, v1

    .line 1303777
    goto/16 :goto_0

    .line 1303778
    :cond_28
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->N:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->N:Z

    if-eq v2, v3, :cond_29

    move v0, v1

    .line 1303779
    goto/16 :goto_0

    .line 1303780
    :cond_29
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->O:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->O:Z

    if-eq v2, v3, :cond_2a

    move v0, v1

    .line 1303781
    goto/16 :goto_0

    .line 1303782
    :cond_2a
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->P:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->P:Z

    if-eq v2, v3, :cond_2b

    move v0, v1

    .line 1303783
    goto/16 :goto_0

    .line 1303784
    :cond_2b
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Q:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Q:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2c

    move v0, v1

    .line 1303785
    goto/16 :goto_0

    .line 1303786
    :cond_2c
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->R:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->R:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2d

    move v0, v1

    .line 1303787
    goto/16 :goto_0

    .line 1303788
    :cond_2d
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->S:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->S:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2e

    move v0, v1

    .line 1303789
    goto/16 :goto_0

    .line 1303790
    :cond_2e
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->T:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->T:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2f

    move v0, v1

    .line 1303791
    goto/16 :goto_0

    .line 1303792
    :cond_2f
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->U:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->U:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_30

    move v0, v1

    .line 1303793
    goto/16 :goto_0

    .line 1303794
    :cond_30
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->V:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->V:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_31

    move v0, v1

    .line 1303795
    goto/16 :goto_0

    .line 1303796
    :cond_31
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->W:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->W:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_32

    move v0, v1

    .line 1303797
    goto/16 :goto_0

    .line 1303798
    :cond_32
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->X:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->X:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_33

    move v0, v1

    .line 1303799
    goto/16 :goto_0

    .line 1303800
    :cond_33
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Y:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Y:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_34

    move v0, v1

    .line 1303801
    goto/16 :goto_0

    .line 1303802
    :cond_34
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Z:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Z:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_35

    move v0, v1

    .line 1303803
    goto/16 :goto_0

    .line 1303804
    :cond_35
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aa:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aa:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_36

    move v0, v1

    .line 1303805
    goto/16 :goto_0

    .line 1303806
    :cond_36
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ab:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ab:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_37

    move v0, v1

    .line 1303807
    goto/16 :goto_0

    .line 1303808
    :cond_37
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ac:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ac:Z

    if-eq v2, v3, :cond_38

    move v0, v1

    .line 1303809
    goto/16 :goto_0

    .line 1303810
    :cond_38
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ad:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ad:Z

    if-eq v2, v3, :cond_39

    move v0, v1

    .line 1303811
    goto/16 :goto_0

    .line 1303812
    :cond_39
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ae:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ae:Z

    if-eq v2, v3, :cond_3a

    move v0, v1

    .line 1303813
    goto/16 :goto_0

    .line 1303814
    :cond_3a
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->af:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->af:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3b

    move v0, v1

    .line 1303815
    goto/16 :goto_0

    .line 1303816
    :cond_3b
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ag:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ag:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3c

    move v0, v1

    .line 1303817
    goto/16 :goto_0

    .line 1303818
    :cond_3c
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ah:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ah:Z

    if-eq v2, v3, :cond_3d

    move v0, v1

    .line 1303819
    goto/16 :goto_0

    .line 1303820
    :cond_3d
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ai:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ai:Z

    if-eq v2, v3, :cond_3e

    move v0, v1

    .line 1303821
    goto/16 :goto_0

    .line 1303822
    :cond_3e
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aj:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aj:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1303823
    goto/16 :goto_0
.end method

.method public getAllowDynamicTextStyle()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "allow_dynamic_text_style"
    .end annotation

    .prologue
    .line 1303696
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->b:Z

    return v0
.end method

.method public getAllowFeedOnlyPost()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "allow_feed_only_post"
    .end annotation

    .prologue
    .line 1303695
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->c:Z

    return v0
.end method

.method public getAllowRichTextStyle()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "allow_rich_text_style"
    .end annotation

    .prologue
    .line 1303694
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->d:Z

    return v0
.end method

.method public getAllowTargetSelection()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "allow_target_selection"
    .end annotation

    .prologue
    .line 1303693
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->e:Z

    return v0
.end method

.method public getAttachedStory()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attached_story"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303692
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->f:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public getCacheId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cache_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303691
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "commerce_info"
    .end annotation

    .prologue
    .line 1304081
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->i:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    return-object v0
.end method

.method public getComposerType()LX/2rt;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_type"
    .end annotation

    .prologue
    .line 1304064
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->j:LX/2rt;

    return-object v0
.end method

.method public getEditPostFeatureCapabilities()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "edit_post_feature_capabilities"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1304080
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->o:LX/0Px;

    return-object v0
.end method

.method public getExternalRefName()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "external_ref_name"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1304079
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->p:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupCommerceCategories()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "group_commerce_categories"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groupcommerce/model/GroupCommerceCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1304078
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->q:LX/0Px;

    return-object v0
.end method

.method public getInitialAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_app_attribution"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1304077
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->s:Lcom/facebook/share/model/ComposerAppAttribution;

    return-object v0
.end method

.method public getInitialAttachments()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_attachments"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1304076
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->t:LX/0Px;

    return-object v0
.end method

.method public getInitialComposerCallToAction()Lcom/facebook/ipc/composer/model/ComposerCallToAction;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_composer_call_to_action"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1304075
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->u:Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    return-object v0
.end method

.method public getInitialDateInfo()Lcom/facebook/ipc/composer/model/ComposerDateInfo;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_date_info"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1304074
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->v:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    return-object v0
.end method

.method public getInitialLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_location_info"
    .end annotation

    .prologue
    .line 1304073
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->w:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    return-object v0
.end method

.method public getInitialPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_page_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303493
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    return-object v0
.end method

.method public getInitialPrivacyOverride()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_privacy_override"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1304072
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->y:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    return-object v0
.end method

.method public getInitialRating()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_rating"
    .end annotation

    .prologue
    .line 1304071
    iget v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->z:I

    return v0
.end method

.method public getInitialRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_rich_text_style"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1304070
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->A:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    return-object v0
.end method

.method public getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_share_params"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1304069
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->B:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    return-object v0
.end method

.method public getInitialSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_slideshow_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1304068
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->C:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    return-object v0
.end method

.method public getInitialStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_sticker_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1304067
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->D:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    return-object v0
.end method

.method public getInitialStorylineData()Lcom/facebook/ipc/composer/model/ComposerStorylineData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_storyline_data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1304066
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->E:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    return-object v0
.end method

.method public getInitialTaggedUsers()LX/0Px;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_tagged_users"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1303825
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->F:LX/0Px;

    return-object v0
.end method

.method public getInitialTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_target_album"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1304065
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->G:Lcom/facebook/graphql/model/GraphQLAlbum;

    return-object v0
.end method

.method public getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_target_data"
    .end annotation

    .prologue
    .line 1303494
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->H:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    return-object v0
.end method

.method public getInitialText()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_text"
    .end annotation

    .prologue
    .line 1303492
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "launch_logging_params"
    .end annotation

    .prologue
    .line 1303491
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Q:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    return-object v0
.end method

.method public getLegacyApiStoryId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "legacy_api_story_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303490
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->R:Ljava/lang/String;

    return-object v0
.end method

.method public getMinutiaeObjectTag()Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "minutiae_object_tag"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303489
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->T:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    return-object v0
.end method

.method public getNectarModule()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "nectar_module"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303488
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->U:Ljava/lang/String;

    return-object v0
.end method

.method public getOgMechanism()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "og_mechanism"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303487
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->V:Ljava/lang/String;

    return-object v0
.end method

.method public getOgSurface()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "og_surface"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303486
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->W:Ljava/lang/String;

    return-object v0
.end method

.method public getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "platform_configuration"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303485
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->X:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    return-object v0
.end method

.method public getPluginConfig()Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "plugin_config"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303484
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Y:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    return-object v0
.end method

.method public getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "product_item_attachment"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303483
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Z:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    return-object v0
.end method

.method public getReactionSurface()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reaction_surface"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303482
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aa:Ljava/lang/String;

    return-object v0
.end method

.method public getReactionUnitId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reaction_unit_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303481
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ab:Ljava/lang/String;

    return-object v0
.end method

.method public getShouldPickerSupportLiveCamera()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_picker_support_live_camera"
    .end annotation

    .prologue
    .line 1303480
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ac:Z

    return v0
.end method

.method public getSouvenirUniqueId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "souvenir_unique_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303479
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->af:Ljava/lang/String;

    return-object v0
.end method

.method public getStoryId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "story_id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1303478
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ag:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1303477
    const/16 v0, 0x3d

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->f:Lcom/facebook/graphql/model/GraphQLStory;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->i:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->j:LX/2rt;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->m:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->o:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->p:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->q:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->r:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->s:Lcom/facebook/share/model/ComposerAppAttribution;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->t:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->u:Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->v:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->w:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->y:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    iget v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->z:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x19

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->A:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->B:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->C:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->D:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->E:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->F:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->G:Lcom/facebook/graphql/model/GraphQLAlbum;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->H:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->J:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x23

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->K:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x24

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->L:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x25

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->M:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x26

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->N:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x27

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->O:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x28

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->P:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x29

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Q:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->R:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->S:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->T:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->U:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->V:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->W:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->X:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Y:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Z:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aa:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ab:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ac:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x36

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ad:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x37

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ae:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x38

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->af:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ag:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ah:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ai:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aj:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isEdit()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_edit"
    .end annotation

    .prologue
    .line 1303681
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->J:Z

    return v0
.end method

.method public isEditPrivacyEnabled()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_edit_privacy_enabled"
    .end annotation

    .prologue
    .line 1303689
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->K:Z

    return v0
.end method

.method public isEditTagEnabled()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_edit_tag_enabled"
    .end annotation

    .prologue
    .line 1303688
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->L:Z

    return v0
.end method

.method public isFireAndForget()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_fire_and_forget"
    .end annotation

    .prologue
    .line 1303687
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->M:Z

    return v0
.end method

.method public isGroupMemberBioPost()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_group_member_bio_post"
    .end annotation

    .prologue
    .line 1303686
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->N:Z

    return v0
.end method

.method public isPlacelistPost()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_placelist_post"
    .end annotation

    .prologue
    .line 1303685
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->O:Z

    return v0
.end method

.method public isThrowbackPost()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_throwback_post"
    .end annotation

    .prologue
    .line 1303684
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->P:Z

    return v0
.end method

.method public shouldDisableAttachToAlbum()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "disable_attach_to_album"
    .end annotation

    .prologue
    .line 1303683
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->k:Z

    return v0
.end method

.method public shouldDisableFriendTagging()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "disable_friend_tagging"
    .end annotation

    .prologue
    .line 1303682
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->l:Z

    return v0
.end method

.method public shouldDisableMentions()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "disable_mentions"
    .end annotation

    .prologue
    .line 1303495
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->m:Z

    return v0
.end method

.method public shouldDisablePhotos()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "disable_photos"
    .end annotation

    .prologue
    .line 1303680
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->n:Z

    return v0
.end method

.method public shouldHideKeyboardIfReachedMinimumHeight()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "hide_keyboard_if_reached_minimum_height"
    .end annotation

    .prologue
    .line 1303679
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->r:Z

    return v0
.end method

.method public shouldPostToMarketplaceByDefault()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_post_to_marketplace_by_default"
    .end annotation

    .prologue
    .line 1303678
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ad:Z

    return v0
.end method

.method public shouldShowPageVoiceSwitcher()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "show_page_voice_switcher"
    .end annotation

    .prologue
    .line 1303677
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ae:Z

    return v0
.end method

.method public shouldUseInspirationCam()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "use_inspiration_cam"
    .end annotation

    .prologue
    .line 1303676
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ah:Z

    return v0
.end method

.method public shouldUseOptimisticPosting()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "use_optimistic_posting"
    .end annotation

    .prologue
    .line 1303675
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ai:Z

    return v0
.end method

.method public shouldUsePublishExperiment()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "use_publish_experiment"
    .end annotation

    .prologue
    .line 1303674
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aj:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1303496
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303497
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->c:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303498
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->d:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303499
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->e:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303500
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->f:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_4

    .line 1303501
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303502
    :goto_4
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->g:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 1303503
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303504
    :goto_5
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->h:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303505
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->i:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1303506
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->j:LX/2rt;

    invoke-virtual {v0}, LX/2rt;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303507
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->k:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303508
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->l:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303509
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->m:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303510
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->n:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303511
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->o:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303512
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->o:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_b
    if-ge v3, v4, :cond_b

    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->o:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;

    .line 1303513
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303514
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_b

    :cond_0
    move v0, v2

    .line 1303515
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1303516
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1303517
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1303518
    goto :goto_3

    .line 1303519
    :cond_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303520
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->f:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    goto :goto_4

    .line 1303521
    :cond_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303522
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_5

    :cond_6
    move v0, v2

    .line 1303523
    goto :goto_6

    :cond_7
    move v0, v2

    .line 1303524
    goto :goto_7

    :cond_8
    move v0, v2

    .line 1303525
    goto :goto_8

    :cond_9
    move v0, v2

    .line 1303526
    goto :goto_9

    :cond_a
    move v0, v2

    .line 1303527
    goto :goto_a

    .line 1303528
    :cond_b
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->p:Ljava/lang/String;

    if-nez v0, :cond_c

    .line 1303529
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303530
    :goto_c
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->q:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303531
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->q:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_d
    if-ge v3, v4, :cond_d

    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->q:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;

    .line 1303532
    invoke-virtual {v0, p1, p2}, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1303533
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_d

    .line 1303534
    :cond_c
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303535
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_c

    .line 1303536
    :cond_d
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->r:Z

    if-eqz v0, :cond_e

    move v0, v1

    :goto_e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303537
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->s:Lcom/facebook/share/model/ComposerAppAttribution;

    if-nez v0, :cond_f

    .line 1303538
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303539
    :goto_f
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303540
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_10
    if-ge v3, v4, :cond_10

    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->t:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1303541
    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/attachments/ComposerAttachment;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1303542
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_10

    :cond_e
    move v0, v2

    .line 1303543
    goto :goto_e

    .line 1303544
    :cond_f
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303545
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->s:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/share/model/ComposerAppAttribution;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_f

    .line 1303546
    :cond_10
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->u:Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    if-nez v0, :cond_11

    .line 1303547
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303548
    :goto_11
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->v:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    if-nez v0, :cond_12

    .line 1303549
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303550
    :goto_12
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->w:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1303551
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    if-nez v0, :cond_13

    .line 1303552
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303553
    :goto_13
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->y:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-nez v0, :cond_14

    .line 1303554
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303555
    :goto_14
    iget v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->z:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303556
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->A:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    if-nez v0, :cond_15

    .line 1303557
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303558
    :goto_15
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->B:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    if-nez v0, :cond_16

    .line 1303559
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303560
    :goto_16
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->C:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    if-nez v0, :cond_17

    .line 1303561
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303562
    :goto_17
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->D:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    if-nez v0, :cond_18

    .line 1303563
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303564
    :goto_18
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->E:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    if-nez v0, :cond_19

    .line 1303565
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303566
    :goto_19
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->F:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303567
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->F:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_1a
    if-ge v3, v4, :cond_1a

    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->F:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1303568
    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1303569
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1a

    .line 1303570
    :cond_11
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303571
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->u:Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_11

    .line 1303572
    :cond_12
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303573
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->v:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_12

    .line 1303574
    :cond_13
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303575
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_13

    .line 1303576
    :cond_14
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303577
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->y:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    goto :goto_14

    .line 1303578
    :cond_15
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303579
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->A:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_15

    .line 1303580
    :cond_16
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303581
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->B:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_16

    .line 1303582
    :cond_17
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303583
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->C:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_17

    .line 1303584
    :cond_18
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303585
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->D:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_18

    .line 1303586
    :cond_19
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303587
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->E:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerStorylineData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_19

    .line 1303588
    :cond_1a
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->G:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-nez v0, :cond_1b

    .line 1303589
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303590
    :goto_1b
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->H:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1303591
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1303592
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->J:Z

    if-eqz v0, :cond_1c

    move v0, v1

    :goto_1c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303593
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->K:Z

    if-eqz v0, :cond_1d

    move v0, v1

    :goto_1d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303594
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->L:Z

    if-eqz v0, :cond_1e

    move v0, v1

    :goto_1e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303595
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->M:Z

    if-eqz v0, :cond_1f

    move v0, v1

    :goto_1f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303596
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->N:Z

    if-eqz v0, :cond_20

    move v0, v1

    :goto_20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303597
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->O:Z

    if-eqz v0, :cond_21

    move v0, v1

    :goto_21
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303598
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->P:Z

    if-eqz v0, :cond_22

    move v0, v1

    :goto_22
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303599
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Q:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1303600
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->R:Ljava/lang/String;

    if-nez v0, :cond_23

    .line 1303601
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303602
    :goto_23
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->S:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    if-nez v0, :cond_24

    .line 1303603
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303604
    :goto_24
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->T:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-nez v0, :cond_25

    .line 1303605
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303606
    :goto_25
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->U:Ljava/lang/String;

    if-nez v0, :cond_26

    .line 1303607
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303608
    :goto_26
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->V:Ljava/lang/String;

    if-nez v0, :cond_27

    .line 1303609
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303610
    :goto_27
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->W:Ljava/lang/String;

    if-nez v0, :cond_28

    .line 1303611
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303612
    :goto_28
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->X:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    if-nez v0, :cond_29

    .line 1303613
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303614
    :goto_29
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Y:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    if-nez v0, :cond_2a

    .line 1303615
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303616
    :goto_2a
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Z:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    if-nez v0, :cond_2b

    .line 1303617
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303618
    :goto_2b
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aa:Ljava/lang/String;

    if-nez v0, :cond_2c

    .line 1303619
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303620
    :goto_2c
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ab:Ljava/lang/String;

    if-nez v0, :cond_2d

    .line 1303621
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303622
    :goto_2d
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ac:Z

    if-eqz v0, :cond_2e

    move v0, v1

    :goto_2e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303623
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ad:Z

    if-eqz v0, :cond_2f

    move v0, v1

    :goto_2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303624
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ae:Z

    if-eqz v0, :cond_30

    move v0, v1

    :goto_30
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303625
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->af:Ljava/lang/String;

    if-nez v0, :cond_31

    .line 1303626
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303627
    :goto_31
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ag:Ljava/lang/String;

    if-nez v0, :cond_32

    .line 1303628
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303629
    :goto_32
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ah:Z

    if-eqz v0, :cond_33

    move v0, v1

    :goto_33
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303630
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ai:Z

    if-eqz v0, :cond_34

    move v0, v1

    :goto_34
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303631
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aj:Z

    if-eqz v0, :cond_35

    :goto_35
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303632
    return-void

    .line 1303633
    :cond_1b
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303634
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->G:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    goto/16 :goto_1b

    :cond_1c
    move v0, v2

    .line 1303635
    goto/16 :goto_1c

    :cond_1d
    move v0, v2

    .line 1303636
    goto/16 :goto_1d

    :cond_1e
    move v0, v2

    .line 1303637
    goto/16 :goto_1e

    :cond_1f
    move v0, v2

    .line 1303638
    goto/16 :goto_1f

    :cond_20
    move v0, v2

    .line 1303639
    goto/16 :goto_20

    :cond_21
    move v0, v2

    .line 1303640
    goto/16 :goto_21

    :cond_22
    move v0, v2

    .line 1303641
    goto/16 :goto_22

    .line 1303642
    :cond_23
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303643
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->R:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_23

    .line 1303644
    :cond_24
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303645
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->S:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_24

    .line 1303646
    :cond_25
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303647
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->T:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_25

    .line 1303648
    :cond_26
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303649
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->U:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_26

    .line 1303650
    :cond_27
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303651
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->V:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_27

    .line 1303652
    :cond_28
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303653
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->W:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_28

    .line 1303654
    :cond_29
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303655
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->X:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_29

    .line 1303656
    :cond_2a
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303657
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Y:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_2a

    .line 1303658
    :cond_2b
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303659
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Z:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_2b

    .line 1303660
    :cond_2c
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303661
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aa:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_2c

    .line 1303662
    :cond_2d
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303663
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ab:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_2d

    :cond_2e
    move v0, v2

    .line 1303664
    goto/16 :goto_2e

    :cond_2f
    move v0, v2

    .line 1303665
    goto/16 :goto_2f

    :cond_30
    move v0, v2

    .line 1303666
    goto/16 :goto_30

    .line 1303667
    :cond_31
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303668
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->af:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_31

    .line 1303669
    :cond_32
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1303670
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ag:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_32

    :cond_33
    move v0, v2

    .line 1303671
    goto/16 :goto_33

    :cond_34
    move v0, v2

    .line 1303672
    goto/16 :goto_34

    :cond_35
    move v1, v2

    .line 1303673
    goto/16 :goto_35
.end method
