.class public Lcom/facebook/ipc/composer/intent/SharePreview;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/intent/SharePreviewDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/intent/SharePreviewSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/intent/SharePreview;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final imageHeight:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "image_height"
    .end annotation
.end field

.field public final imageUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "image_url"
    .end annotation
.end field

.field public final imageWidth:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "image_width"
    .end annotation
.end field

.field public final isOverride:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_override"
    .end annotation
.end field

.field public final subTitle:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sub_title"
    .end annotation
.end field

.field public final summary:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "summary"
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "title"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1304868
    const-class v0, Lcom/facebook/ipc/composer/intent/SharePreviewDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1304869
    const-class v0, Lcom/facebook/ipc/composer/intent/SharePreviewSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1304785
    new-instance v0, LX/89N;

    invoke-direct {v0}, LX/89N;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/intent/SharePreview;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304866
    new-instance v0, LX/89O;

    invoke-direct {v0}, LX/89O;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/ipc/composer/intent/SharePreview;-><init>(LX/89O;)V

    .line 1304867
    return-void
.end method

.method public constructor <init>(LX/89O;)V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304857
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304858
    iget-object v0, p1, LX/89O;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->title:Ljava/lang/String;

    .line 1304859
    iget-object v0, p1, LX/89O;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->subTitle:Ljava/lang/String;

    .line 1304860
    iget-object v0, p1, LX/89O;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->summary:Ljava/lang/String;

    .line 1304861
    iget-object v0, p1, LX/89O;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->imageUrl:Ljava/lang/String;

    .line 1304862
    iget-boolean v0, p1, LX/89O;->e:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->isOverride:Z

    .line 1304863
    iget v0, p1, LX/89O;->f:I

    iput v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->imageWidth:I

    .line 1304864
    iget v0, p1, LX/89O;->g:I

    iput v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->imageHeight:I

    .line 1304865
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304871
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->title:Ljava/lang/String;

    .line 1304872
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->subTitle:Ljava/lang/String;

    .line 1304873
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->summary:Ljava/lang/String;

    .line 1304874
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->imageUrl:Ljava/lang/String;

    .line 1304875
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->isOverride:Z

    .line 1304876
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->imageWidth:I

    .line 1304877
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->imageHeight:I

    .line 1304878
    return-void
.end method

.method private static a(Lcom/facebook/share/model/LinksPreview;)Lcom/facebook/ipc/composer/intent/SharePreview;
    .locals 6
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1304831
    invoke-virtual {p0}, Lcom/facebook/share/model/LinksPreview;->a()Lcom/facebook/share/model/LinksPreview$Media;

    move-result-object v5

    .line 1304832
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview;->name:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview;->description:Ljava/lang/String;

    move-object v4, v0

    .line 1304833
    :goto_0
    const/4 v0, 0x0

    .line 1304834
    if-eqz v5, :cond_1

    .line 1304835
    iget-object v3, v5, Lcom/facebook/share/model/LinksPreview$Media;->src:Ljava/lang/String;

    .line 1304836
    iget v2, v5, Lcom/facebook/share/model/LinksPreview$Media;->width:I

    .line 1304837
    iget v0, v5, Lcom/facebook/share/model/LinksPreview$Media;->height:I

    .line 1304838
    :goto_1
    new-instance v5, LX/89O;

    invoke-direct {v5}, LX/89O;-><init>()V

    .line 1304839
    iput-object v4, v5, LX/89O;->a:Ljava/lang/String;

    .line 1304840
    move-object v4, v5

    .line 1304841
    iget-object v5, p0, Lcom/facebook/share/model/LinksPreview;->description:Ljava/lang/String;

    .line 1304842
    iput-object v5, v4, LX/89O;->b:Ljava/lang/String;

    .line 1304843
    move-object v4, v4

    .line 1304844
    iget-object v5, p0, Lcom/facebook/share/model/LinksPreview;->caption:Ljava/lang/String;

    .line 1304845
    iput-object v5, v4, LX/89O;->c:Ljava/lang/String;

    .line 1304846
    move-object v4, v4

    .line 1304847
    iput-object v3, v4, LX/89O;->d:Ljava/lang/String;

    .line 1304848
    move-object v3, v4

    .line 1304849
    iput-boolean v1, v3, LX/89O;->e:Z

    .line 1304850
    move-object v1, v3

    .line 1304851
    iput v2, v1, LX/89O;->f:I

    .line 1304852
    move-object v1, v1

    .line 1304853
    iput v0, v1, LX/89O;->g:I

    .line 1304854
    move-object v0, v1

    .line 1304855
    invoke-virtual {v0}, LX/89O;->a()Lcom/facebook/ipc/composer/intent/SharePreview;

    move-result-object v0

    return-object v0

    .line 1304856
    :cond_0
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview;->name:Ljava/lang/String;

    move-object v4, v0

    goto :goto_0

    :cond_1
    move v2, v1

    move-object v3, v0

    move v0, v1

    goto :goto_1
.end method

.method public static a(Lcom/facebook/share/model/LinksPreview;Lcom/facebook/ipc/composer/intent/SharePreview;)Lcom/facebook/ipc/composer/intent/SharePreview;
    .locals 7
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304796
    if-nez p1, :cond_0

    .line 1304797
    invoke-static {p0}, Lcom/facebook/ipc/composer/intent/SharePreview;->a(Lcom/facebook/share/model/LinksPreview;)Lcom/facebook/ipc/composer/intent/SharePreview;

    move-result-object v0

    .line 1304798
    :goto_0
    return-object v0

    .line 1304799
    :cond_0
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/SharePreview;->title:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/SharePreview;->title:Ljava/lang/String;

    .line 1304800
    :goto_1
    iget-object v1, p1, Lcom/facebook/ipc/composer/intent/SharePreview;->subTitle:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/facebook/ipc/composer/intent/SharePreview;->subTitle:Ljava/lang/String;

    .line 1304801
    :goto_2
    iget-object v2, p1, Lcom/facebook/ipc/composer/intent/SharePreview;->summary:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p1, Lcom/facebook/ipc/composer/intent/SharePreview;->summary:Ljava/lang/String;

    .line 1304802
    :goto_3
    iget-object v5, p1, Lcom/facebook/ipc/composer/intent/SharePreview;->imageUrl:Ljava/lang/String;

    .line 1304803
    iget v4, p1, Lcom/facebook/ipc/composer/intent/SharePreview;->imageWidth:I

    .line 1304804
    iget v3, p1, Lcom/facebook/ipc/composer/intent/SharePreview;->imageHeight:I

    .line 1304805
    if-nez v5, :cond_1

    .line 1304806
    invoke-virtual {p0}, Lcom/facebook/share/model/LinksPreview;->a()Lcom/facebook/share/model/LinksPreview$Media;

    move-result-object v6

    .line 1304807
    if-eqz v6, :cond_1

    .line 1304808
    iget-object v5, v6, Lcom/facebook/share/model/LinksPreview$Media;->src:Ljava/lang/String;

    .line 1304809
    iget v4, v6, Lcom/facebook/share/model/LinksPreview$Media;->width:I

    .line 1304810
    iget v3, v6, Lcom/facebook/share/model/LinksPreview$Media;->height:I

    .line 1304811
    :cond_1
    new-instance v6, LX/89O;

    invoke-direct {v6}, LX/89O;-><init>()V

    .line 1304812
    iput-object v0, v6, LX/89O;->a:Ljava/lang/String;

    .line 1304813
    move-object v0, v6

    .line 1304814
    iput-object v1, v0, LX/89O;->b:Ljava/lang/String;

    .line 1304815
    move-object v0, v0

    .line 1304816
    iput-object v2, v0, LX/89O;->c:Ljava/lang/String;

    .line 1304817
    move-object v0, v0

    .line 1304818
    iput-object v5, v0, LX/89O;->d:Ljava/lang/String;

    .line 1304819
    move-object v0, v0

    .line 1304820
    const/4 v1, 0x0

    .line 1304821
    iput-boolean v1, v0, LX/89O;->e:Z

    .line 1304822
    move-object v0, v0

    .line 1304823
    iput v4, v0, LX/89O;->f:I

    .line 1304824
    move-object v0, v0

    .line 1304825
    iput v3, v0, LX/89O;->g:I

    .line 1304826
    move-object v0, v0

    .line 1304827
    invoke-virtual {v0}, LX/89O;->a()Lcom/facebook/ipc/composer/intent/SharePreview;

    move-result-object v0

    goto :goto_0

    .line 1304828
    :cond_2
    iget-object v0, p0, Lcom/facebook/share/model/LinksPreview;->name:Ljava/lang/String;

    goto :goto_1

    .line 1304829
    :cond_3
    iget-object v1, p0, Lcom/facebook/share/model/LinksPreview;->description:Ljava/lang/String;

    goto :goto_2

    .line 1304830
    :cond_4
    iget-object v2, p0, Lcom/facebook/share/model/LinksPreview;->caption:Ljava/lang/String;

    goto :goto_3
.end method


# virtual methods
.method public final describeContents()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304795
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    const/16 v2, 0x27

    .line 1304794
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SharePreview{title=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subTitle=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->subTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", summary=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->summary:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", imageUrl=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->imageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304786
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304787
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->subTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304788
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->summary:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304789
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->imageUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304790
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->isOverride:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1304791
    iget v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->imageWidth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1304792
    iget v0, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->imageHeight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1304793
    return-void
.end method
