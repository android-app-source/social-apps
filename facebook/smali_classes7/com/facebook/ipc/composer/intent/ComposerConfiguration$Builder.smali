.class public final Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/intent/ComposerConfiguration_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

.field private static final b:LX/2rt;

.field private static final c:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

.field private static final d:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field private static final e:LX/899;

.field private static final f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field private static final g:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

.field private static final h:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;


# instance fields
.field public A:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public B:Lcom/facebook/ipc/composer/model/ComposerCallToAction;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/ipc/composer/model/ComposerDateInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

.field public E:Lcom/facebook/ipc/composer/intent/ComposerPageData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:I

.field public H:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Lcom/facebook/ipc/composer/model/ComposerStickerData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/ipc/composer/model/ComposerStorylineData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field

.field public N:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public O:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public P:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public Q:Z

.field public R:Z

.field public S:Z

.field public T:Z

.field public U:Z

.field public V:Z

.field public W:Z

.field public X:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

.field public Y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aa:Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ae:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ag:Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ai:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:Z

.field public ak:Z

.field public al:Z

.field public am:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:Z

.field public ap:Z

.field public aq:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Z

.field public p:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

.field public q:LX/2rt;

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;",
            ">;"
        }
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groupcommerce/model/GroupCommerceCategory;",
            ">;"
        }
    .end annotation
.end field

.field public y:Z

.field public z:Lcom/facebook/share/model/ComposerAppAttribution;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1303436
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1303414
    new-instance v0, LX/894;

    invoke-direct {v0}, LX/894;-><init>()V

    .line 1303415
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v0

    move-object v0, v0

    .line 1303416
    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    .line 1303417
    new-instance v0, LX/896;

    invoke-direct {v0}, LX/896;-><init>()V

    .line 1303418
    sget-object v0, LX/2rt;->STATUS:LX/2rt;

    move-object v0, v0

    .line 1303419
    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->b:LX/2rt;

    .line 1303420
    new-instance v0, LX/897;

    invoke-direct {v0}, LX/897;-><init>()V

    .line 1303421
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v0

    invoke-virtual {v0}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    move-object v0, v0

    .line 1303422
    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->c:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1303423
    new-instance v0, LX/898;

    invoke-direct {v0}, LX/898;-><init>()V

    .line 1303424
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v0, v0

    .line 1303425
    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->d:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1303426
    new-instance v0, LX/899;

    invoke-direct {v0}, LX/899;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->e:LX/899;

    .line 1303427
    new-instance v0, LX/899;

    invoke-direct {v0}, LX/899;-><init>()V

    .line 1303428
    sget-object v0, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-object v0, v0

    .line 1303429
    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1303430
    new-instance v0, LX/895;

    invoke-direct {v0}, LX/895;-><init>()V

    .line 1303431
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v0

    move-object v0, v0

    .line 1303432
    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->g:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    .line 1303433
    new-instance v0, LX/89A;

    invoke-direct {v0}, LX/89A;-><init>()V

    .line 1303434
    new-instance v0, LX/2ro;

    invoke-direct {v0}, LX/2ro;-><init>()V

    invoke-virtual {v0}, LX/2ro;->a()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    move-object v0, v0

    .line 1303435
    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->h:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1303397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1303398
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->p:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    .line 1303399
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->b:LX/2rt;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->q:LX/2rt;

    .line 1303400
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1303401
    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->v:LX/0Px;

    .line 1303402
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1303403
    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->x:LX/0Px;

    .line 1303404
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1303405
    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->A:LX/0Px;

    .line 1303406
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->c:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->D:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1303407
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1303408
    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->M:LX/0Px;

    .line 1303409
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->d:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->O:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1303410
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->P:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1303411
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->g:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->X:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    .line 1303412
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->h:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ae:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    .line 1303413
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V
    .locals 1

    .prologue
    .line 1303270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1303271
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1303272
    instance-of v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    if-eqz v0, :cond_0

    .line 1303273
    check-cast p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1303274
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->b:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->i:Z

    .line 1303275
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->c:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->j:Z

    .line 1303276
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->d:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->k:Z

    .line 1303277
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->e:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->l:Z

    .line 1303278
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->f:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->m:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1303279
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->n:Ljava/lang/String;

    .line 1303280
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->h:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->o:Z

    .line 1303281
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->i:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->p:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    .line 1303282
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->j:LX/2rt;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->q:LX/2rt;

    .line 1303283
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->k:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->r:Z

    .line 1303284
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->l:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->s:Z

    .line 1303285
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->m:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->t:Z

    .line 1303286
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->n:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->u:Z

    .line 1303287
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->o:LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->v:LX/0Px;

    .line 1303288
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->w:Ljava/lang/String;

    .line 1303289
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->q:LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->x:LX/0Px;

    .line 1303290
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->r:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->y:Z

    .line 1303291
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->s:Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->z:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1303292
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->t:LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->A:LX/0Px;

    .line 1303293
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->u:Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->B:Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    .line 1303294
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->v:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->C:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    .line 1303295
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->w:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->D:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1303296
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->E:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 1303297
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->y:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->F:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1303298
    iget v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->z:I

    iput v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->G:I

    .line 1303299
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->A:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->H:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1303300
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->B:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->I:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1303301
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->C:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->J:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 1303302
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->D:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->K:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 1303303
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->E:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->L:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    .line 1303304
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->F:LX/0Px;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->M:LX/0Px;

    .line 1303305
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->G:Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->N:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1303306
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->H:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->O:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1303307
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->I:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->P:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1303308
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->J:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->Q:Z

    .line 1303309
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->K:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->R:Z

    .line 1303310
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->L:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->S:Z

    .line 1303311
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->M:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->T:Z

    .line 1303312
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->N:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->U:Z

    .line 1303313
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->O:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->V:Z

    .line 1303314
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->P:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->W:Z

    .line 1303315
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Q:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->X:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    .line 1303316
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->R:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->Y:Ljava/lang/String;

    .line 1303317
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->S:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->Z:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 1303318
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->T:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->aa:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1303319
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->U:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ab:Ljava/lang/String;

    .line 1303320
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->V:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ac:Ljava/lang/String;

    .line 1303321
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->W:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ad:Ljava/lang/String;

    .line 1303322
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->X:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ae:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    .line 1303323
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Y:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->af:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    .line 1303324
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->Z:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ag:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1303325
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aa:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ah:Ljava/lang/String;

    .line 1303326
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ab:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ai:Ljava/lang/String;

    .line 1303327
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ac:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->aj:Z

    .line 1303328
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ad:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ak:Z

    .line 1303329
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ae:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->al:Z

    .line 1303330
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->af:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->am:Ljava/lang/String;

    .line 1303331
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ag:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->an:Ljava/lang/String;

    .line 1303332
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ah:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ao:Z

    .line 1303333
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->ai:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ap:Z

    .line 1303334
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->aj:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->aq:Z

    .line 1303335
    :goto_0
    return-void

    .line 1303336
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAllowDynamicTextStyle()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->i:Z

    .line 1303337
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAllowFeedOnlyPost()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->j:Z

    .line 1303338
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAllowRichTextStyle()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->k:Z

    .line 1303339
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAllowTargetSelection()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->l:Z

    .line 1303340
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAttachedStory()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->m:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1303341
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCacheId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->n:Ljava/lang/String;

    .line 1303342
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->canViewerEditPostMedia()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->o:Z

    .line 1303343
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->p:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    .line 1303344
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->q:LX/2rt;

    .line 1303345
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisableAttachToAlbum()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->r:Z

    .line 1303346
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisableFriendTagging()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->s:Z

    .line 1303347
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisableMentions()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->t:Z

    .line 1303348
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisablePhotos()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->u:Z

    .line 1303349
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getEditPostFeatureCapabilities()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->v:LX/0Px;

    .line 1303350
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getExternalRefName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->w:Ljava/lang/String;

    .line 1303351
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getGroupCommerceCategories()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->x:LX/0Px;

    .line 1303352
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldHideKeyboardIfReachedMinimumHeight()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->y:Z

    .line 1303353
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->z:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1303354
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAttachments()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->A:LX/0Px;

    .line 1303355
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialComposerCallToAction()Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->B:Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    .line 1303356
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialDateInfo()Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->C:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    .line 1303357
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->D:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1303358
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->E:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 1303359
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialPrivacyOverride()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->F:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1303360
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialRating()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->G:I

    .line 1303361
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->H:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1303362
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->I:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1303363
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->J:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 1303364
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->K:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 1303365
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialStorylineData()Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->L:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    .line 1303366
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTaggedUsers()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->M:LX/0Px;

    .line 1303367
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->N:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1303368
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->O:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1303369
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialText()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->P:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1303370
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->Q:Z

    .line 1303371
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditPrivacyEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->R:Z

    .line 1303372
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditTagEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->S:Z

    .line 1303373
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isFireAndForget()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->T:Z

    .line 1303374
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isGroupMemberBioPost()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->U:Z

    .line 1303375
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isPlacelistPost()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->V:Z

    .line 1303376
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isThrowbackPost()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->W:Z

    .line 1303377
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->X:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    .line 1303378
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLegacyApiStoryId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->Y:Ljava/lang/String;

    .line 1303379
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->Z:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 1303380
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getMinutiaeObjectTag()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->aa:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1303381
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getNectarModule()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ab:Ljava/lang/String;

    .line 1303382
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getOgMechanism()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ac:Ljava/lang/String;

    .line 1303383
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getOgSurface()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ad:Ljava/lang/String;

    .line 1303384
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ae:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    .line 1303385
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPluginConfig()Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->af:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    .line 1303386
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ag:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1303387
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getReactionSurface()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ah:Ljava/lang/String;

    .line 1303388
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getReactionUnitId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ai:Ljava/lang/String;

    .line 1303389
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getShouldPickerSupportLiveCamera()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->aj:Z

    .line 1303390
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldPostToMarketplaceByDefault()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ak:Z

    .line 1303391
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldShowPageVoiceSwitcher()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->al:Z

    .line 1303392
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getSouvenirUniqueId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->am:Ljava/lang/String;

    .line 1303393
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getStoryId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->an:Ljava/lang/String;

    .line 1303394
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldUseInspirationCam()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ao:Z

    .line 1303395
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldUseOptimisticPosting()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ap:Z

    .line 1303396
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldUsePublishExperiment()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->aq:Z

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1303268
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->Z:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 1303269
    return-object p0
.end method

.method public final a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 2

    .prologue
    .line 1303267
    new-instance v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;-><init>(Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V

    return-object v0
.end method

.method public setAllowDynamicTextStyle(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "allow_dynamic_text_style"
    .end annotation

    .prologue
    .line 1303247
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->i:Z

    .line 1303248
    return-object p0
.end method

.method public setAllowFeedOnlyPost(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "allow_feed_only_post"
    .end annotation

    .prologue
    .line 1303263
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->j:Z

    .line 1303264
    return-object p0
.end method

.method public setAllowRichTextStyle(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "allow_rich_text_style"
    .end annotation

    .prologue
    .line 1303261
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->k:Z

    .line 1303262
    return-object p0
.end method

.method public setAllowTargetSelection(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "allow_target_selection"
    .end annotation

    .prologue
    .line 1303259
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->l:Z

    .line 1303260
    return-object p0
.end method

.method public setAttachedStory(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attached_story"
    .end annotation

    .prologue
    .line 1303257
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->m:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1303258
    return-object p0
.end method

.method public setCacheId(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cache_id"
    .end annotation

    .prologue
    .line 1303255
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->n:Ljava/lang/String;

    .line 1303256
    return-object p0
.end method

.method public setCanViewerEditPostMedia(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "can_viewer_edit_post_media"
    .end annotation

    .prologue
    .line 1303253
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->o:Z

    .line 1303254
    return-object p0
.end method

.method public setCommerceInfo(Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "commerce_info"
    .end annotation

    .prologue
    .line 1303251
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->p:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    .line 1303252
    return-object p0
.end method

.method public setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_type"
    .end annotation

    .prologue
    .line 1303249
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->q:LX/2rt;

    .line 1303250
    return-object p0
.end method

.method public setDisableAttachToAlbum(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "disable_attach_to_album"
    .end annotation

    .prologue
    .line 1303441
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->r:Z

    .line 1303442
    return-object p0
.end method

.method public setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "disable_friend_tagging"
    .end annotation

    .prologue
    .line 1303439
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->s:Z

    .line 1303440
    return-object p0
.end method

.method public setDisableMentions(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "disable_mentions"
    .end annotation

    .prologue
    .line 1303469
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->t:Z

    .line 1303470
    return-object p0
.end method

.method public setDisablePhotos(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "disable_photos"
    .end annotation

    .prologue
    .line 1303467
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->u:Z

    .line 1303468
    return-object p0
.end method

.method public setEditPostFeatureCapabilities(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "edit_post_feature_capabilities"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLEditPostFeatureCapability;",
            ">;)",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;"
        }
    .end annotation

    .prologue
    .line 1303465
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->v:LX/0Px;

    .line 1303466
    return-object p0
.end method

.method public setExternalRefName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "external_ref_name"
    .end annotation

    .prologue
    .line 1303463
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->w:Ljava/lang/String;

    .line 1303464
    return-object p0
.end method

.method public setGroupCommerceCategories(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "group_commerce_categories"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/groupcommerce/model/GroupCommerceCategory;",
            ">;)",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;"
        }
    .end annotation

    .prologue
    .line 1303461
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->x:LX/0Px;

    .line 1303462
    return-object p0
.end method

.method public setHideKeyboardIfReachedMinimumHeight(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "hide_keyboard_if_reached_minimum_height"
    .end annotation

    .prologue
    .line 1303459
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->y:Z

    .line 1303460
    return-object p0
.end method

.method public setInitialAppAttribution(Lcom/facebook/share/model/ComposerAppAttribution;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/share/model/ComposerAppAttribution;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_app_attribution"
    .end annotation

    .prologue
    .line 1303457
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->z:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1303458
    return-object p0
.end method

.method public setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_attachments"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;"
        }
    .end annotation

    .prologue
    .line 1303455
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->A:LX/0Px;

    .line 1303456
    return-object p0
.end method

.method public setInitialComposerCallToAction(Lcom/facebook/ipc/composer/model/ComposerCallToAction;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ComposerCallToAction;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_composer_call_to_action"
    .end annotation

    .prologue
    .line 1303453
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->B:Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    .line 1303454
    return-object p0
.end method

.method public setInitialDateInfo(Lcom/facebook/ipc/composer/model/ComposerDateInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ComposerDateInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_date_info"
    .end annotation

    .prologue
    .line 1303451
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->C:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    .line 1303452
    return-object p0
.end method

.method public setInitialLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_location_info"
    .end annotation

    .prologue
    .line 1303449
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->D:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1303450
    return-object p0
.end method

.method public setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/intent/ComposerPageData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_page_data"
    .end annotation

    .prologue
    .line 1303447
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->E:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 1303448
    return-object p0
.end method

.method public setInitialPrivacyOverride(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLPrivacyOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_privacy_override"
    .end annotation

    .prologue
    .line 1303445
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->F:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1303446
    return-object p0
.end method

.method public setInitialRating(I)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_rating"
    .end annotation

    .prologue
    .line 1303443
    iput p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->G:I

    .line 1303444
    return-object p0
.end method

.method public setInitialRichTextStyle(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_rich_text_style"
    .end annotation

    .prologue
    .line 1303265
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->H:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1303266
    return-object p0
.end method

.method public setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/intent/ComposerShareParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_share_params"
    .end annotation

    .prologue
    .line 1303437
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->I:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1303438
    return-object p0
.end method

.method public setInitialSlideshowData(Lcom/facebook/ipc/composer/model/ComposerSlideshowData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_slideshow_data"
    .end annotation

    .prologue
    .line 1303198
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->J:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 1303199
    return-object p0
.end method

.method public setInitialStickerData(Lcom/facebook/ipc/composer/model/ComposerStickerData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ComposerStickerData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_sticker_data"
    .end annotation

    .prologue
    .line 1303208
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->K:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 1303209
    return-object p0
.end method

.method public setInitialStorylineData(Lcom/facebook/ipc/composer/model/ComposerStorylineData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ComposerStorylineData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_storyline_data"
    .end annotation

    .prologue
    .line 1303206
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->L:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    .line 1303207
    return-object p0
.end method

.method public setInitialTaggedUsers(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_tagged_users"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;)",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;"
        }
    .end annotation

    .prologue
    .line 1303204
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->M:LX/0Px;

    .line 1303205
    return-object p0
.end method

.method public setInitialTargetAlbum(Lcom/facebook/graphql/model/GraphQLAlbum;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLAlbum;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_target_album"
    .end annotation

    .prologue
    .line 1303202
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->N:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1303203
    return-object p0
.end method

.method public setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_target_data"
    .end annotation

    .prologue
    .line 1303200
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->O:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1303201
    return-object p0
.end method

.method public setInitialText(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "initial_text"
    .end annotation

    .prologue
    .line 1303212
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1303213
    :cond_0
    sget-object p1, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1303214
    :cond_1
    move-object v0, p1

    .line 1303215
    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->P:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1303216
    return-object p0
.end method

.method public setIsEdit(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_edit"
    .end annotation

    .prologue
    .line 1303196
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->Q:Z

    .line 1303197
    return-object p0
.end method

.method public setIsEditPrivacyEnabled(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_edit_privacy_enabled"
    .end annotation

    .prologue
    .line 1303194
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->R:Z

    .line 1303195
    return-object p0
.end method

.method public setIsEditTagEnabled(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_edit_tag_enabled"
    .end annotation

    .prologue
    .line 1303192
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->S:Z

    .line 1303193
    return-object p0
.end method

.method public setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_fire_and_forget"
    .end annotation

    .prologue
    .line 1303190
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->T:Z

    .line 1303191
    return-object p0
.end method

.method public setIsGroupMemberBioPost(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_group_member_bio_post"
    .end annotation

    .prologue
    .line 1303188
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->U:Z

    .line 1303189
    return-object p0
.end method

.method public setIsPlacelistPost(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_placelist_post"
    .end annotation

    .prologue
    .line 1303186
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->V:Z

    .line 1303187
    return-object p0
.end method

.method public setIsThrowbackPost(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_throwback_post"
    .end annotation

    .prologue
    .line 1303184
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->W:Z

    .line 1303185
    return-object p0
.end method

.method public setLaunchLoggingParams(Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "launch_logging_params"
    .end annotation

    .prologue
    .line 1303182
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->X:Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    .line 1303183
    return-object p0
.end method

.method public setLegacyApiStoryId(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "legacy_api_story_id"
    .end annotation

    .prologue
    .line 1303180
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->Y:Ljava/lang/String;

    .line 1303181
    return-object p0
.end method

.method public setMinutiaeObjectTag(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/composer/minutiae/model/MinutiaeObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "minutiae_object_tag"
    .end annotation

    .prologue
    .line 1303178
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->aa:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1303179
    return-object p0
.end method

.method public setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "nectar_module"
    .end annotation

    .prologue
    .line 1303231
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ab:Ljava/lang/String;

    .line 1303232
    return-object p0
.end method

.method public setOgMechanism(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "og_mechanism"
    .end annotation

    .prologue
    .line 1303245
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ac:Ljava/lang/String;

    .line 1303246
    return-object p0
.end method

.method public setOgSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "og_surface"
    .end annotation

    .prologue
    .line 1303243
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ad:Ljava/lang/String;

    .line 1303244
    return-object p0
.end method

.method public setPlatformConfiguration(Lcom/facebook/ipc/composer/intent/PlatformConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/intent/PlatformConfiguration;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "platform_configuration"
    .end annotation

    .prologue
    .line 1303241
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ae:Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    .line 1303242
    return-object p0
.end method

.method public setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "plugin_config"
    .end annotation

    .prologue
    .line 1303239
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->af:Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    .line 1303240
    return-object p0
.end method

.method public setProductItemAttachment(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ProductItemAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "product_item_attachment"
    .end annotation

    .prologue
    .line 1303237
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ag:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1303238
    return-object p0
.end method

.method public setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reaction_surface"
    .end annotation

    .prologue
    .line 1303235
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ah:Ljava/lang/String;

    .line 1303236
    return-object p0
.end method

.method public setReactionUnitId(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reaction_unit_id"
    .end annotation

    .prologue
    .line 1303233
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ai:Ljava/lang/String;

    .line 1303234
    return-object p0
.end method

.method public setShouldPickerSupportLiveCamera(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_picker_support_live_camera"
    .end annotation

    .prologue
    .line 1303210
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->aj:Z

    .line 1303211
    return-object p0
.end method

.method public setShouldPostToMarketplaceByDefault(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "should_post_to_marketplace_by_default"
    .end annotation

    .prologue
    .line 1303229
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ak:Z

    .line 1303230
    return-object p0
.end method

.method public setShowPageVoiceSwitcher(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "show_page_voice_switcher"
    .end annotation

    .prologue
    .line 1303227
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->al:Z

    .line 1303228
    return-object p0
.end method

.method public setSouvenirUniqueId(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "souvenir_unique_id"
    .end annotation

    .prologue
    .line 1303225
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->am:Ljava/lang/String;

    .line 1303226
    return-object p0
.end method

.method public setStoryId(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "story_id"
    .end annotation

    .prologue
    .line 1303223
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->an:Ljava/lang/String;

    .line 1303224
    return-object p0
.end method

.method public setUseInspirationCam(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "use_inspiration_cam"
    .end annotation

    .prologue
    .line 1303221
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ao:Z

    .line 1303222
    return-object p0
.end method

.method public setUseOptimisticPosting(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "use_optimistic_posting"
    .end annotation

    .prologue
    .line 1303219
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->ap:Z

    .line 1303220
    return-object p0
.end method

.method public setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "use_publish_experiment"
    .end annotation

    .prologue
    .line 1303217
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->aq:Z

    .line 1303218
    return-object p0
.end method
