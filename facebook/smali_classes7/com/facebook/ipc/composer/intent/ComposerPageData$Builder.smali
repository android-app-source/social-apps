.class public final Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/intent/ComposerPageData_BuilderDeserializer;
.end annotation


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1304294
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerPageData_BuilderDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1304290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304291
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->d:Ljava/lang/String;

    .line 1304292
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->e:Ljava/lang/String;

    .line 1304293
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/intent/ComposerPageData;)V
    .locals 1

    .prologue
    .line 1304273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304274
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1304275
    instance-of v0, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    if-eqz v0, :cond_0

    .line 1304276
    check-cast p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 1304277
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;->a:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a:Z

    .line 1304278
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;->b:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->b:Z

    .line 1304279
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;->c:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->c:Z

    .line 1304280
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->d:Ljava/lang/String;

    .line 1304281
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->e:Ljava/lang/String;

    .line 1304282
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1304283
    :goto_0
    return-void

    .line 1304284
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->hasTaggableProducts()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a:Z

    .line 1304285
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getIsOptedInSponsorTags()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->b:Z

    .line 1304286
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getIsPageVerified()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->c:Z

    .line 1304287
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->d:Ljava/lang/String;

    .line 1304288
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPageProfilePicUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->e:Ljava/lang/String;

    .line 1304289
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/composer/intent/ComposerPageData;
    .locals 2

    .prologue
    .line 1304272
    new-instance v0, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/intent/ComposerPageData;-><init>(Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;)V

    return-object v0
.end method

.method public setHasTaggableProducts(Z)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_taggable_products"
    .end annotation

    .prologue
    .line 1304295
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a:Z

    .line 1304296
    return-object p0
.end method

.method public setIsOptedInSponsorTags(Z)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_opted_in_sponsor_tags"
    .end annotation

    .prologue
    .line 1304270
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->b:Z

    .line 1304271
    return-object p0
.end method

.method public setIsPageVerified(Z)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_page_verified"
    .end annotation

    .prologue
    .line 1304268
    iput-boolean p1, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->c:Z

    .line 1304269
    return-object p0
.end method

.method public setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_name"
    .end annotation

    .prologue
    .line 1304266
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->d:Ljava/lang/String;

    .line 1304267
    return-object p0
.end method

.method public setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_profile_pic_url"
    .end annotation

    .prologue
    .line 1304264
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->e:Ljava/lang/String;

    .line 1304265
    return-object p0
.end method

.method public setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;
    .locals 0
    .param p1    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "post_as_page_viewer_context"
    .end annotation

    .prologue
    .line 1304262
    iput-object p1, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1304263
    return-object p0
.end method
