.class public Lcom/facebook/ipc/composer/intent/SharePreviewSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/intent/SharePreview;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1304922
    const-class v0, Lcom/facebook/ipc/composer/intent/SharePreview;

    new-instance v1, Lcom/facebook/ipc/composer/intent/SharePreviewSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/intent/SharePreviewSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1304923
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1304921
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/intent/SharePreview;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1304915
    if-nez p0, :cond_0

    .line 1304916
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1304917
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1304918
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/intent/SharePreviewSerializer;->b(Lcom/facebook/ipc/composer/intent/SharePreview;LX/0nX;LX/0my;)V

    .line 1304919
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1304920
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/intent/SharePreview;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1304907
    const-string v0, "title"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->title:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304908
    const-string v0, "sub_title"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->subTitle:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304909
    const-string v0, "summary"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->summary:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304910
    const-string v0, "image_url"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->imageUrl:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304911
    const-string v0, "image_width"

    iget v1, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->imageWidth:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1304912
    const-string v0, "image_height"

    iget v1, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->imageHeight:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1304913
    const-string v0, "is_override"

    iget-boolean v1, p0, Lcom/facebook/ipc/composer/intent/SharePreview;->isOverride:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304914
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1304906
    check-cast p1, Lcom/facebook/ipc/composer/intent/SharePreview;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/intent/SharePreviewSerializer;->a(Lcom/facebook/ipc/composer/intent/SharePreview;LX/0nX;LX/0my;)V

    return-void
.end method
