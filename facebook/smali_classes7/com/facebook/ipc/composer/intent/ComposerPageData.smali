.class public Lcom/facebook/ipc/composer/intent/ComposerPageData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/intent/ComposerPageData$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/intent/ComposerPageDataSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/intent/ComposerPageData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Z

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1304366
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1304365
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerPageDataSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1304364
    new-instance v0, LX/89D;

    invoke-direct {v0}, LX/89D;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1304351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304352
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->a:Z

    .line 1304353
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->b:Z

    .line 1304354
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->c:Z

    .line 1304355
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->d:Ljava/lang/String;

    .line 1304356
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->e:Ljava/lang/String;

    .line 1304357
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    .line 1304358
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1304359
    :goto_3
    return-void

    :cond_0
    move v0, v2

    .line 1304360
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1304361
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1304362
    goto :goto_2

    .line 1304363
    :cond_3
    sget-object v0, Lcom/facebook/auth/viewercontext/ViewerContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    goto :goto_3
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;)V
    .locals 1

    .prologue
    .line 1304343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304344
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->a:Z

    .line 1304345
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->b:Z

    .line 1304346
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->c:Z

    .line 1304347
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->d:Ljava/lang/String;

    .line 1304348
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->e:Ljava/lang/String;

    .line 1304349
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1304350
    return-void
.end method

.method public static a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;
    .locals 2

    .prologue
    .line 1304304
    new-instance v0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;-><init>(Lcom/facebook/ipc/composer/intent/ComposerPageData;)V

    return-object v0
.end method

.method public static newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;
    .locals 2

    .prologue
    .line 1304342
    new-instance v0, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    invoke-direct {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1304341
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1304324
    if-ne p0, p1, :cond_1

    .line 1304325
    :cond_0
    :goto_0
    return v0

    .line 1304326
    :cond_1
    instance-of v2, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    if-nez v2, :cond_2

    move v0, v1

    .line 1304327
    goto :goto_0

    .line 1304328
    :cond_2
    check-cast p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 1304329
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->a:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1304330
    goto :goto_0

    .line 1304331
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->b:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1304332
    goto :goto_0

    .line 1304333
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->c:Z

    iget-boolean v3, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1304334
    goto :goto_0

    .line 1304335
    :cond_5
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1304336
    goto :goto_0

    .line 1304337
    :cond_6
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1304338
    goto :goto_0

    .line 1304339
    :cond_7
    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-object v3, p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1304340
    goto :goto_0
.end method

.method public getIsOptedInSponsorTags()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_opted_in_sponsor_tags"
    .end annotation

    .prologue
    .line 1304303
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->b:Z

    return v0
.end method

.method public getIsPageVerified()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_page_verified"
    .end annotation

    .prologue
    .line 1304305
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->c:Z

    return v0
.end method

.method public getPageName()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_name"
    .end annotation

    .prologue
    .line 1304306
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getPageProfilePicUrl()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_profile_pic_url"
    .end annotation

    .prologue
    .line 1304307
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "post_as_page_viewer_context"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1304308
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    return-object v0
.end method

.method public hasTaggableProducts()Z
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_taggable_products"
    .end annotation

    .prologue
    .line 1304309
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->a:Z

    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1304310
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1304311
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1304312
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1304313
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->c:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1304314
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304315
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304316
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-nez v0, :cond_3

    .line 1304317
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1304318
    :goto_3
    return-void

    :cond_0
    move v0, v2

    .line 1304319
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1304320
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1304321
    goto :goto_2

    .line 1304322
    :cond_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1304323
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/auth/viewercontext/ViewerContext;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_3
.end method
