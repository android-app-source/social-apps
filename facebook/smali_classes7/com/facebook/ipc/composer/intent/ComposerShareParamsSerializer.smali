.class public Lcom/facebook/ipc/composer/intent/ComposerShareParamsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/intent/ComposerShareParams;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1304514
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    new-instance v1, Lcom/facebook/ipc/composer/intent/ComposerShareParamsSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/intent/ComposerShareParamsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1304515
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1304516
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1304508
    if-nez p0, :cond_0

    .line 1304509
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1304510
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1304511
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerShareParamsSerializer;->b(Lcom/facebook/ipc/composer/intent/ComposerShareParams;LX/0nX;LX/0my;)V

    .line 1304512
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1304513
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/intent/ComposerShareParams;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1304500
    const-string v0, "share_attachment_preview"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304501
    const-string v0, "shareable"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304502
    const-string v0, "link_for_share"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304503
    const-string v0, "share_tracking"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareTracking:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304504
    const-string v0, "quote_text"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->quoteText:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304505
    const-string v0, "reshare_context"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304506
    const-string v0, "is_meme_share"

    iget-boolean v1, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->isMemeShare:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304507
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1304499
    check-cast p1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/intent/ComposerShareParamsSerializer;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;LX/0nX;LX/0my;)V

    return-void
.end method
