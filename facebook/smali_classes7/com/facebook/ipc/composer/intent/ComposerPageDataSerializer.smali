.class public Lcom/facebook/ipc/composer/intent/ComposerPageDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/intent/ComposerPageData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1304381
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    new-instance v1, Lcom/facebook/ipc/composer/intent/ComposerPageDataSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/intent/ComposerPageDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1304382
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1304383
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/intent/ComposerPageData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1304375
    if-nez p0, :cond_0

    .line 1304376
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1304377
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1304378
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerPageDataSerializer;->b(Lcom/facebook/ipc/composer/intent/ComposerPageData;LX/0nX;LX/0my;)V

    .line 1304379
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1304380
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/intent/ComposerPageData;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1304368
    const-string v0, "has_taggable_products"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->hasTaggableProducts()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304369
    const-string v0, "is_opted_in_sponsor_tags"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getIsOptedInSponsorTags()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304370
    const-string v0, "is_page_verified"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getIsPageVerified()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304371
    const-string v0, "page_name"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304372
    const-string v0, "page_profile_pic_url"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPageProfilePicUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304373
    const-string v0, "post_as_page_viewer_context"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304374
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1304367
    check-cast p1, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/intent/ComposerPageDataSerializer;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;LX/0nX;LX/0my;)V

    return-void
.end method
