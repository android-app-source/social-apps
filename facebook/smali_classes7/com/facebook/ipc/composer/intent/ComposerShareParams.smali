.class public Lcom/facebook/ipc/composer/intent/ComposerShareParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/intent/ComposerShareParamsDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/intent/ComposerShareParamsSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/intent/ComposerShareParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "share_attachment_preview"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final isMemeShare:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_meme_share"
    .end annotation
.end field

.field public final linkForShare:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "link_for_share"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final quoteText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "quote_text"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reshare_context"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final shareTracking:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "share_tracking"
    .end annotation
.end field

.field public final shareable:Lcom/facebook/graphql/model/GraphQLEntity;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "shareable"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1304471
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerShareParamsDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1304438
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerShareParamsSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1304470
    new-instance v0, LX/2rn;

    invoke-direct {v0}, LX/2rn;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304468
    invoke-static {}, LX/89G;->a()LX/89G;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/ipc/composer/intent/ComposerShareParams;-><init>(LX/89G;)V

    .line 1304469
    return-void
.end method

.method public constructor <init>(LX/89G;)V
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304458
    iget-object v0, p1, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1304459
    iget-object v0, p1, LX/89G;->a:Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1304460
    iget-object v0, p1, LX/89G;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    .line 1304461
    iget-object v0, p1, LX/89G;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareTracking:Ljava/lang/String;

    .line 1304462
    iget-object v0, p1, LX/89G;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->quoteText:Ljava/lang/String;

    .line 1304463
    iget-object v0, p1, LX/89G;->f:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    .line 1304464
    iget-boolean v0, p1, LX/89G;->g:Z

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->isMemeShare:Z

    .line 1304465
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v0, :cond_0

    .line 1304466
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A story can have only one type of attachment: Can\'t share both a link and a shareable entity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1304467
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304449
    const-class v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1304450
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1304451
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    .line 1304452
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareTracking:Ljava/lang/String;

    .line 1304453
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->quoteText:Ljava/lang/String;

    .line 1304454
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    .line 1304455
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->isMemeShare:Z

    .line 1304456
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304447
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304439
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {p1, v0}, LX/4By;->b(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1304440
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1304441
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304442
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareTracking:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304443
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->quoteText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304444
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1304445
    iget-boolean v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->isMemeShare:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1304446
    return-void
.end method
