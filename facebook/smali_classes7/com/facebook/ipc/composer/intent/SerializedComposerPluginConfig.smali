.class public Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfigSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mData:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "data"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mPersistenceKey:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "persist_key"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1304742
    const-class v0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1304743
    const-class v0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfigSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1304720
    new-instance v0, LX/89M;

    invoke-direct {v0}, LX/89M;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1304738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304739
    const-string v0, "ComposerPluginConfig_from_json"

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->mPersistenceKey:Ljava/lang/String;

    .line 1304740
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->mData:Ljava/lang/String;

    .line 1304741
    return-void
.end method

.method private constructor <init>(LX/88e;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1304732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304733
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1304734
    invoke-interface {p1}, LX/88e;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->mPersistenceKey:Ljava/lang/String;

    .line 1304735
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->mPersistenceKey:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1304736
    iput-object p2, p0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->mData:Ljava/lang/String;

    .line 1304737
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1304728
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304729
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->mPersistenceKey:Ljava/lang/String;

    .line 1304730
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->mData:Ljava/lang/String;

    .line 1304731
    return-void
.end method

.method public static a(LX/88e;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1304744
    new-instance v0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    invoke-direct {v0, p0, p1}, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;-><init>(LX/88e;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1304727
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->mPersistenceKey:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1304726
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->mData:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1304725
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1304724
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->mPersistenceKey:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1304721
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->mPersistenceKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304722
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->mData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304723
    return-void
.end method
