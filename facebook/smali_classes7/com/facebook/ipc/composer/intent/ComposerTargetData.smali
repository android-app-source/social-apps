.class public Lcom/facebook/ipc/composer/intent/ComposerTargetData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/composer/intent/ComposerTargetDataDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/composer/intent/ComposerTargetDataSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/composer/intent/ComposerTargetData;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;


# instance fields
.field public final targetId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_id"
    .end annotation
.end field

.field public final targetName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_name"
    .end annotation
.end field

.field public final targetPrivacy:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_privacy"
    .end annotation
.end field

.field public final targetProfilePicUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_profile_pic_url"
    .end annotation
.end field

.field public final targetType:LX/2rw;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_type"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1304573
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerTargetDataDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1304546
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerTargetDataSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1304571
    new-instance v0, LX/89I;

    const-wide/16 v2, -0x1

    sget-object v1, LX/2rw;->UNDIRECTED:LX/2rw;

    invoke-direct {v0, v2, v3, v1}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1304572
    new-instance v0, LX/89H;

    invoke-direct {v0}, LX/89H;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304569
    new-instance v0, LX/89I;

    sget-object v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-direct {v0, v1}, LX/89I;-><init>(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V

    invoke-direct {p0, v0}, Lcom/facebook/ipc/composer/intent/ComposerTargetData;-><init>(LX/89I;)V

    .line 1304570
    return-void
.end method

.method public constructor <init>(LX/89I;)V
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304563
    iget-wide v0, p1, LX/89I;->a:J

    iput-wide v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    .line 1304564
    iget-object v0, p1, LX/89I;->b:LX/2rw;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    .line 1304565
    iget-object v0, p1, LX/89I;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    .line 1304566
    iget-object v0, p1, LX/89I;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetProfilePicUrl:Ljava/lang/String;

    .line 1304567
    iget-object v0, p1, LX/89I;->e:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetPrivacy:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1304568
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304556
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    .line 1304557
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2rw;->fromString(Ljava/lang/String;)LX/2rw;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    .line 1304558
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    .line 1304559
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetProfilePicUrl:Ljava/lang/String;

    .line 1304560
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    iput-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetPrivacy:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 1304561
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 1304554
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v1, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304553
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1304547
    iget-wide v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1304548
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v0}, LX/2rw;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304549
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304550
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetProfilePicUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304551
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetPrivacy:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1304552
    return-void
.end method
