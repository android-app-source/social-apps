.class public Lcom/facebook/ipc/composer/intent/ComposerConfigurationSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1304168
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    new-instance v1, Lcom/facebook/ipc/composer/intent/ComposerConfigurationSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfigurationSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1304169
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1304099
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1304100
    if-nez p0, :cond_0

    .line 1304101
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1304102
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1304103
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfigurationSerializer;->b(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;LX/0nX;LX/0my;)V

    .line 1304104
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1304105
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1304106
    const-string v0, "allow_dynamic_text_style"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAllowDynamicTextStyle()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304107
    const-string v0, "allow_feed_only_post"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAllowFeedOnlyPost()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304108
    const-string v0, "allow_rich_text_style"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAllowRichTextStyle()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304109
    const-string v0, "allow_target_selection"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAllowTargetSelection()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304110
    const-string v0, "attached_story"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAttachedStory()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304111
    const-string v0, "cache_id"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCacheId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304112
    const-string v0, "can_viewer_edit_post_media"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->canViewerEditPostMedia()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304113
    const-string v0, "commerce_info"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304114
    const-string v0, "composer_type"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304115
    const-string v0, "disable_attach_to_album"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisableAttachToAlbum()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304116
    const-string v0, "disable_friend_tagging"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisableFriendTagging()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304117
    const-string v0, "disable_mentions"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisableMentions()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304118
    const-string v0, "disable_photos"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisablePhotos()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304119
    const-string v0, "edit_post_feature_capabilities"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getEditPostFeatureCapabilities()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1304120
    const-string v0, "external_ref_name"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getExternalRefName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304121
    const-string v0, "group_commerce_categories"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getGroupCommerceCategories()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1304122
    const-string v0, "hide_keyboard_if_reached_minimum_height"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldHideKeyboardIfReachedMinimumHeight()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304123
    const-string v0, "initial_app_attribution"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304124
    const-string v0, "initial_attachments"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAttachments()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1304125
    const-string v0, "initial_composer_call_to_action"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialComposerCallToAction()Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304126
    const-string v0, "initial_date_info"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialDateInfo()Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304127
    const-string v0, "initial_location_info"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304128
    const-string v0, "initial_page_data"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304129
    const-string v0, "initial_privacy_override"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialPrivacyOverride()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304130
    const-string v0, "initial_rating"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialRating()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1304131
    const-string v0, "initial_rich_text_style"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304132
    const-string v0, "initial_share_params"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304133
    const-string v0, "initial_slideshow_data"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304134
    const-string v0, "initial_sticker_data"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304135
    const-string v0, "initial_storyline_data"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialStorylineData()Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304136
    const-string v0, "initial_tagged_users"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTaggedUsers()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1304137
    const-string v0, "initial_target_album"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304138
    const-string v0, "initial_target_data"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304139
    const-string v0, "initial_text"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialText()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304140
    const-string v0, "is_edit"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304141
    const-string v0, "is_edit_privacy_enabled"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditPrivacyEnabled()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304142
    const-string v0, "is_edit_tag_enabled"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditTagEnabled()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304143
    const-string v0, "is_fire_and_forget"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isFireAndForget()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304144
    const-string v0, "is_group_member_bio_post"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isGroupMemberBioPost()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304145
    const-string v0, "is_placelist_post"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isPlacelistPost()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304146
    const-string v0, "is_throwback_post"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isThrowbackPost()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304147
    const-string v0, "launch_logging_params"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304148
    const-string v0, "legacy_api_story_id"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLegacyApiStoryId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304149
    const-string v0, "minutiae_object_tag"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getMinutiaeObjectTag()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304150
    const-string v0, "nectar_module"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getNectarModule()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304151
    const-string v0, "og_mechanism"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getOgMechanism()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304152
    const-string v0, "og_surface"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getOgSurface()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304153
    const-string v0, "platform_configuration"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304154
    const-string v0, "plugin_config"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPluginConfig()Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304155
    const-string v0, "product_item_attachment"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304156
    const-string v0, "reaction_surface"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getReactionSurface()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304157
    const-string v0, "reaction_unit_id"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getReactionUnitId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304158
    const-string v0, "should_picker_support_live_camera"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getShouldPickerSupportLiveCamera()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304159
    const-string v0, "should_post_to_marketplace_by_default"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldPostToMarketplaceByDefault()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304160
    const-string v0, "show_page_voice_switcher"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldShowPageVoiceSwitcher()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304161
    const-string v0, "souvenir_unique_id"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getSouvenirUniqueId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304162
    const-string v0, "story_id"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getStoryId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304163
    const-string v0, "use_inspiration_cam"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldUseInspirationCam()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304164
    const-string v0, "use_optimistic_posting"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldUseOptimisticPosting()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304165
    const-string v0, "use_publish_experiment"

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldUsePublishExperiment()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304166
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1304167
    check-cast p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/intent/ComposerConfigurationSerializer;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;LX/0nX;LX/0my;)V

    return-void
.end method
