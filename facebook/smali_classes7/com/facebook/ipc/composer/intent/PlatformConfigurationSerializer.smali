.class public Lcom/facebook/ipc/composer/intent/PlatformConfigurationSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/composer/intent/PlatformConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1304696
    const-class v0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    new-instance v1, Lcom/facebook/ipc/composer/intent/PlatformConfigurationSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/composer/intent/PlatformConfigurationSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1304697
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1304698
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/intent/PlatformConfiguration;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1304699
    if-nez p0, :cond_0

    .line 1304700
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1304701
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1304702
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/composer/intent/PlatformConfigurationSerializer;->b(Lcom/facebook/ipc/composer/intent/PlatformConfiguration;LX/0nX;LX/0my;)V

    .line 1304703
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1304704
    return-void
.end method

.method private static b(Lcom/facebook/ipc/composer/intent/PlatformConfiguration;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1304705
    const-string v0, "data_failures_fatal"

    iget-boolean v1, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->dataFailuresFatal:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1304706
    const-string v0, "insights_platform_ref"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->insightsPlatformRef:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304707
    const-string v0, "hashtag"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->hashtag:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304708
    const-string v0, "og_action_json_for_robotext"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->ogActionJsonForRobotext:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304709
    const-string v0, "og_action_type"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->ogActionType:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304710
    const-string v0, "name_for_share_link"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->nameForShareLink:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304711
    const-string v0, "caption_for_share_link"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->captionForShareLink:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304712
    const-string v0, "picture_for_share_link"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->pictureForShareLink:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304713
    const-string v0, "description_for_share_link"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->descriptionForShareLink:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1304714
    const-string v0, "platform_share_preview"

    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->platformSharePreview:Lcom/facebook/ipc/composer/intent/SharePreview;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1304715
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1304716
    check-cast p1, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/composer/intent/PlatformConfigurationSerializer;->a(Lcom/facebook/ipc/composer/intent/PlatformConfiguration;LX/0nX;LX/0my;)V

    return-void
.end method
