.class public final Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1303476
    new-instance v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Deserializer;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1303474
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1303475
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 1

    .prologue
    .line 1303472
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Deserializer;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1303473
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1303471
    invoke-static {p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    return-object v0
.end method
