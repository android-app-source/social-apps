.class public Lcom/facebook/ipc/creativecam/CreativeCamResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/creativecam/CreativeCamResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1305070
    new-instance v0, LX/89X;

    invoke-direct {v0}, LX/89X;-><init>()V

    sput-object v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1305055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305056
    iput-object p1, p0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->a:Landroid/net/Uri;

    .line 1305057
    iput-object p2, p0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->b:Landroid/net/Uri;

    .line 1305058
    iput-object p3, p0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->c:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1305059
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1305065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305066
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->a:Landroid/net/Uri;

    .line 1305067
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->b:Landroid/net/Uri;

    .line 1305068
    const-class v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iput-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->c:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1305069
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1305064
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1305060
    iget-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1305061
    iget-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1305062
    iget-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->c:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1305063
    return-void
.end method
