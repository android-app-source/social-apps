.class public Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Z

.field public final d:Z

.field public final e:Z

.field public final f:LX/4gI;

.field public final g:I

.field public final h:LX/0Px;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final j:I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final k:LX/89Z;

.field public final l:LX/89U;

.field public final m:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1305039
    new-instance v0, LX/89T;

    invoke-direct {v0}, LX/89T;-><init>()V

    sput-object v0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1305022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305023
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->a:Z

    .line 1305024
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->b:Z

    .line 1305025
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->c:Z

    .line 1305026
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->d:Z

    .line 1305027
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->e:Z

    .line 1305028
    const-class v0, LX/4gI;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/4gI;

    iput-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->f:LX/4gI;

    .line 1305029
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->g:I

    .line 1305030
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    .line 1305031
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->h:LX/0Px;

    .line 1305032
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->i:Ljava/lang/String;

    .line 1305033
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->m:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1305034
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->j:I

    .line 1305035
    const-class v0, LX/89Z;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/89Z;

    iput-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->k:LX/89Z;

    .line 1305036
    const-class v0, LX/89U;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/89U;

    iput-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->l:LX/89U;

    .line 1305037
    return-void

    .line 1305038
    :cond_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private constructor <init>(ZZZZZLX/4gI;ILX/0Px;Ljava/lang/String;ILcom/facebook/ipc/composer/intent/ComposerConfiguration;LX/89Z;LX/89U;)V
    .locals 2
    .param p8    # LX/0Px;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZZZ",
            "LX/4gI;",
            "I",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration;",
            "LX/89Z;",
            "LX/89U;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1305000
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305001
    iput-boolean p1, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->a:Z

    .line 1305002
    iput-boolean p2, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->b:Z

    .line 1305003
    iput-boolean p3, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->c:Z

    .line 1305004
    iput-boolean p4, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->d:Z

    .line 1305005
    iput-boolean p5, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->e:Z

    .line 1305006
    iput-object p6, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->f:LX/4gI;

    .line 1305007
    const/4 v0, 0x1

    if-ne p7, v0, :cond_1

    iget-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->f:LX/4gI;

    invoke-virtual {v0}, LX/4gI;->supportsVideos()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1305008
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->g:I

    .line 1305009
    :goto_0
    iput-object p8, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->h:LX/0Px;

    .line 1305010
    iput-object p9, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->i:Ljava/lang/String;

    .line 1305011
    iput p10, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->j:I

    .line 1305012
    iput-object p12, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->k:LX/89Z;

    .line 1305013
    iget-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1305014
    iget-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->h:LX/0Px;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    const-string v1, "Initial frame id was set, but no frame pack was set"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1305015
    :cond_0
    iput-object p11, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->m:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1305016
    iput-object p13, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->l:LX/89U;

    .line 1305017
    return-void

    .line 1305018
    :cond_1
    if-nez p7, :cond_2

    iget-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->f:LX/4gI;

    invoke-virtual {v0}, LX/4gI;->supportsPhotos()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1305019
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->g:I

    goto :goto_0

    .line 1305020
    :cond_2
    iput p7, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->g:I

    goto :goto_0

    .line 1305021
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public synthetic constructor <init>(ZZZZZLX/4gI;ILX/0Px;Ljava/lang/String;ILcom/facebook/ipc/composer/intent/ComposerConfiguration;LX/89Z;LX/89U;B)V
    .locals 0

    .prologue
    .line 1304984
    invoke-direct/range {p0 .. p13}, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;-><init>(ZZZZZLX/4gI;ILX/0Px;Ljava/lang/String;ILcom/facebook/ipc/composer/intent/ComposerConfiguration;LX/89Z;LX/89U;)V

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1304999
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1304985
    iget-boolean v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1304986
    iget-boolean v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1304987
    iget-boolean v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1304988
    iget-boolean v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1304989
    iget-boolean v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1304990
    iget-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->f:LX/4gI;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1304991
    iget v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1304992
    iget-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->h:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 1304993
    iget-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1304994
    iget-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->m:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1304995
    iget v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->j:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1304996
    iget-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->k:LX/89Z;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1304997
    iget-object v0, p0, Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;->l:LX/89U;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1304998
    return-void
.end method
