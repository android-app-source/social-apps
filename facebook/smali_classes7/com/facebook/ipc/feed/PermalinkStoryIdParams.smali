.class public Lcom/facebook/ipc/feed/PermalinkStoryIdParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/ipc/intent/FacebookOnlyIntentParams;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/feed/PermalinkStoryIdParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:LX/89g;


# instance fields
.field public b:LX/89g;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:LX/21y;

.field private k:Ljava/lang/Boolean;

.field public l:LX/21C;

.field public m:Lcom/facebook/graphql/model/GraphQLComment;

.field public n:Lcom/facebook/graphql/model/GraphQLComment;

.field private o:Ljava/lang/Boolean;

.field public p:I

.field public q:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1305251
    sget-object v0, LX/89g;->NOTIFICATION_CACHE:LX/89g;

    sput-object v0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->a:LX/89g;

    .line 1305252
    new-instance v0, LX/89j;

    invoke-direct {v0}, LX/89j;-><init>()V

    sput-object v0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/89k;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1305230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305231
    iget-object v0, p1, LX/89k;->a:LX/89g;

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->a:LX/89g;

    :goto_0
    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->b:LX/89g;

    .line 1305232
    iget-object v0, p1, LX/89k;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->c:Ljava/lang/String;

    .line 1305233
    iget-object v0, p1, LX/89k;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->d:Ljava/lang/String;

    .line 1305234
    iget-object v0, p1, LX/89k;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->e:Ljava/lang/String;

    .line 1305235
    iget-object v0, p1, LX/89k;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->f:Ljava/lang/String;

    .line 1305236
    iget-object v0, p1, LX/89k;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->g:Ljava/lang/String;

    .line 1305237
    iget-object v0, p1, LX/89k;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->i:Ljava/lang/String;

    .line 1305238
    iget-object v0, p1, LX/89k;->i:LX/21y;

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->j:LX/21y;

    .line 1305239
    iget-object v0, p1, LX/89k;->j:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->k:Ljava/lang/Boolean;

    .line 1305240
    iget-object v0, p1, LX/89k;->k:LX/21C;

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->l:LX/21C;

    .line 1305241
    iget-object v0, p1, LX/89k;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->h:Ljava/lang/String;

    .line 1305242
    iget-object v0, p1, LX/89k;->l:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->m:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1305243
    iget-object v0, p1, LX/89k;->m:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->n:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1305244
    iget-object v0, p1, LX/89k;->n:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->o:Ljava/lang/Boolean;

    .line 1305245
    iget v0, p1, LX/89k;->o:I

    iput v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->p:I

    .line 1305246
    iget-object v0, p1, LX/89k;->p:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->q:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1305247
    return-void

    .line 1305248
    :cond_0
    iget-object v0, p1, LX/89k;->a:LX/89g;

    goto :goto_0

    .line 1305249
    :cond_1
    iget-object v0, p1, LX/89k;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_1

    .line 1305250
    :cond_2
    iget-object v0, p1, LX/89k;->n:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_2
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1305207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305208
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1305209
    if-eqz v0, :cond_0

    .line 1305210
    invoke-static {v0}, LX/89g;->valueOf(Ljava/lang/String;)LX/89g;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->b:LX/89g;

    .line 1305211
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->c:Ljava/lang/String;

    .line 1305212
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->d:Ljava/lang/String;

    .line 1305213
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->e:Ljava/lang/String;

    .line 1305214
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->f:Ljava/lang/String;

    .line 1305215
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->g:Ljava/lang/String;

    .line 1305216
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->i:Ljava/lang/String;

    .line 1305217
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1305218
    if-eqz v0, :cond_1

    .line 1305219
    invoke-static {v0}, LX/21y;->getOrder(Ljava/lang/String;)LX/21y;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->j:LX/21y;

    .line 1305220
    :cond_1
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->k:Ljava/lang/Boolean;

    .line 1305221
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1305222
    if-eqz v0, :cond_2

    .line 1305223
    invoke-static {v0}, LX/21C;->valueOf(Ljava/lang/String;)LX/21C;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->l:LX/21C;

    .line 1305224
    :cond_2
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->m:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1305225
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->n:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1305226
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->o:Ljava/lang/Boolean;

    .line 1305227
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->p:I

    .line 1305228
    const-class v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    .line 1305229
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1305253
    const/4 v0, 0x0

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 1305206
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->k:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 1305205
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->o:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1305167
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->b:LX/89g;

    move-object v0, v0

    .line 1305168
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    .line 1305169
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1305170
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1305171
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1305172
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1305173
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1305174
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1305175
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1305176
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1305177
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1305178
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1305179
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1305180
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1305181
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1305182
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->j:LX/21y;

    move-object v0, v0

    .line 1305183
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    move-object v0, v0

    .line 1305184
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1305185
    invoke-virtual {p0}, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->j()Z

    move-result v0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305186
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->l:LX/21C;

    move-object v0, v0

    .line 1305187
    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    move-object v0, v0

    .line 1305188
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1305189
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->m:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v0, v0

    .line 1305190
    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1305191
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->n:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v0, v0

    .line 1305192
    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1305193
    invoke-virtual {p0}, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->n()Z

    move-result v0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1305194
    iget v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->p:I

    move v0, v0

    .line 1305195
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1305196
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->q:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-object v0, v0

    .line 1305197
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1305198
    return-void

    .line 1305199
    :cond_0
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->b:LX/89g;

    move-object v0, v0

    .line 1305200
    invoke-virtual {v0}, LX/89g;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1305201
    :cond_1
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->j:LX/21y;

    move-object v0, v0

    .line 1305202
    invoke-virtual {v0}, LX/21y;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1305203
    :cond_2
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryIdParams;->l:LX/21C;

    move-object v0, v0

    .line 1305204
    invoke-virtual {v0}, LX/21C;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
