.class public Lcom/facebook/ipc/feed/PermalinkStoryFbIdParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/ipc/intent/FacebookOnlyIntentParams;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/feed/PermalinkStoryFbIdParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1305115
    new-instance v0, LX/89i;

    invoke-direct {v0}, LX/89i;-><init>()V

    sput-object v0, Lcom/facebook/ipc/feed/PermalinkStoryFbIdParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1305116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305117
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryFbIdParams;->a:Ljava/lang/String;

    .line 1305118
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1305119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305120
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1305121
    iput-object p1, p0, Lcom/facebook/ipc/feed/PermalinkStoryFbIdParams;->a:Ljava/lang/String;

    .line 1305122
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1305123
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1305124
    iget-object v0, p0, Lcom/facebook/ipc/feed/PermalinkStoryFbIdParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1305125
    return-void
.end method
