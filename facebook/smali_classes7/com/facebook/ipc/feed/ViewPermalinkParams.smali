.class public Lcom/facebook/ipc/feed/ViewPermalinkParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/ipc/intent/FacebookOnlyIntentParams;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/feed/ViewPermalinkParams;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/facebook/graphql/model/FeedUnit;

.field public c:Z

.field public d:Z

.field public e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1305304
    const-class v0, Lcom/facebook/ipc/feed/ViewPermalinkParams;

    sput-object v0, Lcom/facebook/ipc/feed/ViewPermalinkParams;->a:Ljava/lang/Class;

    .line 1305305
    new-instance v0, LX/89o;

    invoke-direct {v0}, LX/89o;-><init>()V

    sput-object v0, Lcom/facebook/ipc/feed/ViewPermalinkParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1305306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305307
    iput-boolean v0, p0, Lcom/facebook/ipc/feed/ViewPermalinkParams;->c:Z

    .line 1305308
    iput-boolean v0, p0, Lcom/facebook/ipc/feed/ViewPermalinkParams;->d:Z

    .line 1305309
    iput-boolean v0, p0, Lcom/facebook/ipc/feed/ViewPermalinkParams;->e:Z

    .line 1305310
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/ipc/feed/ViewPermalinkParams;->b:Lcom/facebook/graphql/model/FeedUnit;

    .line 1305311
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1305312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305313
    iput-boolean v0, p0, Lcom/facebook/ipc/feed/ViewPermalinkParams;->c:Z

    .line 1305314
    iput-boolean v0, p0, Lcom/facebook/ipc/feed/ViewPermalinkParams;->d:Z

    .line 1305315
    iput-boolean v0, p0, Lcom/facebook/ipc/feed/ViewPermalinkParams;->e:Z

    .line 1305316
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    iput-object v0, p0, Lcom/facebook/ipc/feed/ViewPermalinkParams;->b:Lcom/facebook/graphql/model/FeedUnit;

    .line 1305317
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1305318
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1305319
    iget-object v0, p0, Lcom/facebook/ipc/feed/ViewPermalinkParams;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1305320
    return-void
.end method
