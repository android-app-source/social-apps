.class public Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfigSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:F

.field private final b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:LX/89Z;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1305391
    const-class v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1305390
    const-class v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfigSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1305389
    new-instance v0, LX/89r;

    invoke-direct {v0}, LX/89r;-><init>()V

    sput-object v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1305380
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305381
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->a:F

    .line 1305382
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1305383
    iput-object v1, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1305384
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1305385
    iput-object v1, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->c:LX/89Z;

    .line 1305386
    :goto_1
    return-void

    .line 1305387
    :cond_0
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    goto :goto_0

    .line 1305388
    :cond_1
    invoke-static {}, LX/89Z;->values()[LX/89Z;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->c:LX/89Z;

    goto :goto_1
.end method

.method public constructor <init>(Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;)V
    .locals 1

    .prologue
    .line 1305375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305376
    iget v0, p1, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;->c:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->a:F

    .line 1305377
    iget-object v0, p1, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1305378
    iget-object v0, p1, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;->e:LX/89Z;

    iput-object v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->c:LX/89Z;

    .line 1305379
    return-void
.end method

.method public static newBuilder()Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;
    .locals 2

    .prologue
    .line 1305348
    new-instance v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;

    invoke-direct {v0}, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1305374
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1305363
    if-ne p0, p1, :cond_1

    .line 1305364
    :cond_0
    :goto_0
    return v0

    .line 1305365
    :cond_1
    instance-of v2, p1, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;

    if-nez v2, :cond_2

    move v0, v1

    .line 1305366
    goto :goto_0

    .line 1305367
    :cond_2
    check-cast p1, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;

    .line 1305368
    iget v2, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->a:F

    iget v3, p1, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->a:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_3

    move v0, v1

    .line 1305369
    goto :goto_0

    .line 1305370
    :cond_3
    iget-object v2, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iget-object v3, p1, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1305371
    goto :goto_0

    .line 1305372
    :cond_4
    iget-object v2, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->c:LX/89Z;

    iget-object v3, p1, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->c:LX/89Z;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1305373
    goto :goto_0
.end method

.method public getAspectRatio()F
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "aspect_ratio"
    .end annotation

    .prologue
    .line 1305362
    iget v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->a:F

    return v0
.end method

.method public getComposerConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_configuration"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1305361
    iget-object v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    return-object v0
.end method

.method public getCreativeCamSource()LX/89Z;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "creative_cam_source"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1305360
    iget-object v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->c:LX/89Z;

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1305359
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->a:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->c:LX/89Z;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1305349
    iget v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1305350
    iget-object v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    if-nez v0, :cond_0

    .line 1305351
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1305352
    :goto_0
    iget-object v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->c:LX/89Z;

    if-nez v0, :cond_1

    .line 1305353
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1305354
    :goto_1
    return-void

    .line 1305355
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1305356
    iget-object v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 1305357
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1305358
    iget-object v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->c:LX/89Z;

    invoke-virtual {v0}, LX/89Z;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method
