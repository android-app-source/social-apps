.class public final Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:F

.field private static final b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;


# instance fields
.field public c:F

.field public d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/89Z;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1305324
    const-class v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1305336
    new-instance v0, LX/89s;

    invoke-direct {v0}, LX/89s;-><init>()V

    .line 1305337
    const/high16 v0, 0x3f400000    # 0.75f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    move-object v0, v0

    .line 1305338
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sput v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;->a:F

    .line 1305339
    new-instance v0, LX/89t;

    invoke-direct {v0}, LX/89t;-><init>()V

    .line 1305340
    sget-object v0, LX/21D;->NEWSFEED:LX/21D;

    const-string v1, "inspiration"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    move-object v0, v0

    .line 1305341
    sput-object v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1305331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1305332
    sget v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;->a:F

    iput v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;->c:F

    .line 1305333
    sget-object v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1305334
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;
    .locals 2

    .prologue
    .line 1305335
    new-instance v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;-><init>(Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;)V

    return-object v0
.end method

.method public setAspectRatio(F)Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "aspect_ratio"
    .end annotation

    .prologue
    .line 1305329
    iput p1, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;->c:F

    .line 1305330
    return-object p0
.end method

.method public setComposerConfiguration(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_configuration"
    .end annotation

    .prologue
    .line 1305327
    iput-object p1, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;->d:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1305328
    return-object p0
.end method

.method public setCreativeCamSource(LX/89Z;)Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;
    .locals 0
    .param p1    # LX/89Z;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "creative_cam_source"
    .end annotation

    .prologue
    .line 1305325
    iput-object p1, p0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;->e:LX/89Z;

    .line 1305326
    return-object p0
.end method
