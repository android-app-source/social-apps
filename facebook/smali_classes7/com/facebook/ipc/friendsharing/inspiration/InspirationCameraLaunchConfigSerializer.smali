.class public Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1305398
    const-class v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;

    new-instance v1, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1305399
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1305397
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1305400
    if-nez p0, :cond_0

    .line 1305401
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1305402
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1305403
    invoke-static {p0, p1, p2}, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfigSerializer;->b(Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;LX/0nX;LX/0my;)V

    .line 1305404
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1305405
    return-void
.end method

.method private static b(Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1305393
    const-string v0, "aspect_ratio"

    invoke-virtual {p0}, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->getAspectRatio()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1305394
    const-string v0, "composer_configuration"

    invoke-virtual {p0}, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->getComposerConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1305395
    const-string v0, "creative_cam_source"

    invoke-virtual {p0}, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;->getCreativeCamSource()LX/89Z;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1305396
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1305392
    check-cast p1, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfigSerializer;->a(Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;LX/0nX;LX/0my;)V

    return-void
.end method
