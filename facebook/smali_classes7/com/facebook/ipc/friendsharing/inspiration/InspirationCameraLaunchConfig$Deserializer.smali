.class public final Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Deserializer;
.super Lcom/fasterxml/jackson/databind/JsonDeserializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonDeserializer",
        "<",
        "Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig_BuilderDeserializer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1305342
    new-instance v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig_BuilderDeserializer;

    invoke-direct {v0}, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig_BuilderDeserializer;-><init>()V

    sput-object v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Deserializer;->a:Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig_BuilderDeserializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1305343
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonDeserializer;-><init>()V

    .line 1305344
    return-void
.end method

.method private static a(LX/15w;LX/0n3;)Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;
    .locals 1

    .prologue
    .line 1305345
    sget-object v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Deserializer;->a:Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig_BuilderDeserializer;

    invoke-virtual {v0, p0, p1}, Lcom/fasterxml/jackson/databind/JsonDeserializer;->deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;

    .line 1305346
    invoke-virtual {v0}, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Builder;->a()Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1305347
    invoke-static {p1, p2}, Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig$Deserializer;->a(LX/15w;LX/0n3;)Lcom/facebook/ipc/friendsharing/inspiration/InspirationCameraLaunchConfig;

    move-result-object v0

    return-object v0
.end method
