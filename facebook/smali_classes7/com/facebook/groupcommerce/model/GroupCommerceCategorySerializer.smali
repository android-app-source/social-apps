.class public Lcom/facebook/groupcommerce/model/GroupCommerceCategorySerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groupcommerce/model/GroupCommerceCategory;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1302882
    const-class v0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;

    new-instance v1, Lcom/facebook/groupcommerce/model/GroupCommerceCategorySerializer;

    invoke-direct {v1}, Lcom/facebook/groupcommerce/model/GroupCommerceCategorySerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1302883
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1302884
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groupcommerce/model/GroupCommerceCategory;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1302885
    if-nez p0, :cond_0

    .line 1302886
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1302887
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1302888
    invoke-static {p0, p1, p2}, Lcom/facebook/groupcommerce/model/GroupCommerceCategorySerializer;->b(Lcom/facebook/groupcommerce/model/GroupCommerceCategory;LX/0nX;LX/0my;)V

    .line 1302889
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1302890
    return-void
.end method

.method private static b(Lcom/facebook/groupcommerce/model/GroupCommerceCategory;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1302891
    const-string v0, "name"

    iget-object v1, p0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;->name:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1302892
    const-string v0, "category_id"

    iget-object v1, p0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;->categoryID:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1302893
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1302894
    check-cast p1, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;

    invoke-static {p1, p2, p3}, Lcom/facebook/groupcommerce/model/GroupCommerceCategorySerializer;->a(Lcom/facebook/groupcommerce/model/GroupCommerceCategory;LX/0nX;LX/0my;)V

    return-void
.end method
