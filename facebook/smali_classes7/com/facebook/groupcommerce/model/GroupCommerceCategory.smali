.class public Lcom/facebook/groupcommerce/model/GroupCommerceCategory;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groupcommerce/model/GroupCommerceCategoryDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groupcommerce/model/GroupCommerceCategorySerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/groupcommerce/model/GroupCommerceCategory;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final categoryID:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "category_id"
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1302857
    const-class v0, Lcom/facebook/groupcommerce/model/GroupCommerceCategoryDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1302858
    const-class v0, Lcom/facebook/groupcommerce/model/GroupCommerceCategorySerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1302859
    new-instance v0, LX/88h;

    invoke-direct {v0}, LX/88h;-><init>()V

    sput-object v0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1302855
    new-instance v0, LX/88i;

    invoke-direct {v0}, LX/88i;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;-><init>(LX/88i;)V

    .line 1302856
    return-void
.end method

.method public constructor <init>(LX/88i;)V
    .locals 1

    .prologue
    .line 1302851
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1302852
    iget-object v0, p1, LX/88i;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;->name:Ljava/lang/String;

    .line 1302853
    iget-object v0, p1, LX/88i;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;->categoryID:Ljava/lang/String;

    .line 1302854
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1302843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1302844
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;->name:Ljava/lang/String;

    .line 1302845
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;->categoryID:Ljava/lang/String;

    .line 1302846
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1302850
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1302847
    iget-object v0, p0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1302848
    iget-object v0, p0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;->categoryID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1302849
    return-void
.end method
