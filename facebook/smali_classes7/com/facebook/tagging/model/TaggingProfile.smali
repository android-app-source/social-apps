.class public Lcom/facebook/tagging/model/TaggingProfile;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/facebook/tagging/model/TaggingProfile;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/user/model/Name;

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:LX/7Gr;

.field public final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field public final k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1189901
    new-instance v0, LX/7Gp;

    invoke-direct {v0}, LX/7Gp;-><init>()V

    sput-object v0, Lcom/facebook/tagging/model/TaggingProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7Gq;)V
    .locals 4

    .prologue
    .line 1189877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189878
    iget-object v0, p1, LX/7Gq;->a:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 1189879
    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    .line 1189880
    iget-wide v2, p1, LX/7Gq;->b:J

    move-wide v0, v2

    .line 1189881
    iput-wide v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    .line 1189882
    iget-object v0, p1, LX/7Gq;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1189883
    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->c:Ljava/lang/String;

    .line 1189884
    iget-object v0, p1, LX/7Gq;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1189885
    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->d:Ljava/lang/String;

    .line 1189886
    iget-object v0, p1, LX/7Gq;->e:LX/7Gr;

    move-object v0, v0

    .line 1189887
    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    .line 1189888
    iget-object v0, p1, LX/7Gq;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1189889
    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->f:Ljava/lang/String;

    .line 1189890
    iget-object v0, p1, LX/7Gq;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1189891
    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->g:Ljava/lang/String;

    .line 1189892
    iget-object v0, p1, LX/7Gq;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1189893
    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->i:Ljava/lang/String;

    .line 1189894
    iget-object v0, p1, LX/7Gq;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1189895
    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->h:Ljava/lang/String;

    .line 1189896
    iget-boolean v0, p1, LX/7Gq;->j:Z

    move v0, v0

    .line 1189897
    iput-boolean v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->k:Z

    .line 1189898
    iget-object v0, p1, LX/7Gq;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1189899
    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->j:Ljava/lang/String;

    .line 1189900
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1189863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189864
    const-class v0, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/Name;

    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    .line 1189865
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    .line 1189866
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->c:Ljava/lang/String;

    .line 1189867
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->d:Ljava/lang/String;

    .line 1189868
    invoke-static {}, LX/7Gr;->values()[LX/7Gr;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    .line 1189869
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->f:Ljava/lang/String;

    .line 1189870
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->g:Ljava/lang/String;

    .line 1189871
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->i:Ljava/lang/String;

    .line 1189872
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->h:Ljava/lang/String;

    .line 1189873
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->k:Z

    .line 1189874
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->j:Ljava/lang/String;

    .line 1189875
    return-void

    .line 1189876
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(I)LX/7Gr;
    .locals 1

    .prologue
    .line 1189859
    sparse-switch p0, :sswitch_data_0

    .line 1189860
    sget-object v0, LX/7Gr;->UNKNOWN:LX/7Gr;

    :goto_0
    return-object v0

    .line 1189861
    :sswitch_0
    sget-object v0, LX/7Gr;->USER:LX/7Gr;

    goto :goto_0

    .line 1189862
    :sswitch_1
    sget-object v0, LX/7Gr;->PAGE:LX/7Gr;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLObjectType;)LX/7Gr;
    .locals 1

    .prologue
    .line 1189856
    if-nez p0, :cond_0

    .line 1189857
    sget-object v0, LX/7Gr;->UNKNOWN:LX/7Gr;

    .line 1189858
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    invoke-static {v0}, Lcom/facebook/tagging/model/TaggingProfile;->a(I)LX/7Gr;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1189854
    invoke-static {p0}, LX/0RA;->c(Ljava/lang/Iterable;)Ljava/util/LinkedHashSet;

    move-result-object v0

    .line 1189855
    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b()J
    .locals 2

    .prologue
    .line 1189829
    iget-wide v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    return-wide v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 6

    .prologue
    .line 1189850
    check-cast p1, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1189851
    iget-wide v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1189852
    iget-wide v4, p1, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v2, v4

    .line 1189853
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1189849
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 1189846
    instance-of v0, p1, Lcom/facebook/tagging/model/TaggingProfile;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1189847
    iget-wide v4, p1, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v0, v4

    .line 1189848
    iget-wide v2, p0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1189845
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1189843
    iget-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 1189844
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1189842
    iget-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1189830
    iget-object v1, p0, Lcom/facebook/tagging/model/TaggingProfile;->a:Lcom/facebook/user/model/Name;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1189831
    iget-wide v2, p0, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 1189832
    iget-object v1, p0, Lcom/facebook/tagging/model/TaggingProfile;->c:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1189833
    iget-object v1, p0, Lcom/facebook/tagging/model/TaggingProfile;->d:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1189834
    iget-object v1, p0, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    invoke-virtual {v1}, LX/7Gr;->ordinal()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1189835
    iget-object v1, p0, Lcom/facebook/tagging/model/TaggingProfile;->f:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1189836
    iget-object v1, p0, Lcom/facebook/tagging/model/TaggingProfile;->g:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1189837
    iget-object v1, p0, Lcom/facebook/tagging/model/TaggingProfile;->i:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1189838
    iget-object v1, p0, Lcom/facebook/tagging/model/TaggingProfile;->h:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1189839
    iget-boolean v1, p0, Lcom/facebook/tagging/model/TaggingProfile;->k:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1189840
    iget-object v0, p0, Lcom/facebook/tagging/model/TaggingProfile;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1189841
    return-void
.end method
