.class public Lcom/facebook/tagging/model/TagProfile;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/tagging/model/TagProfile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final photoUri:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "photo"
    .end annotation
.end field

.field public final subtext:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "subtext"
    .end annotation
.end field

.field public final text:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text"
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field

.field public final uid:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uid"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1189910
    new-instance v0, LX/7Go;

    invoke-direct {v0}, LX/7Go;-><init>()V

    sput-object v0, Lcom/facebook/tagging/model/TagProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1189911
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189912
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/tagging/model/TagProfile;->uid:J

    .line 1189913
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/tagging/model/TagProfile;->photoUri:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/tagging/model/TagProfile;->subtext:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/tagging/model/TagProfile;->text:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/tagging/model/TagProfile;->type:Ljava/lang/String;

    .line 1189914
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1189915
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1189916
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/tagging/model/TagProfile;->uid:J

    .line 1189917
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/model/TagProfile;->type:Ljava/lang/String;

    .line 1189918
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/model/TagProfile;->text:Ljava/lang/String;

    .line 1189919
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/model/TagProfile;->subtext:Ljava/lang/String;

    .line 1189920
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tagging/model/TagProfile;->photoUri:Ljava/lang/String;

    .line 1189921
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1189922
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1189923
    iget-wide v0, p0, Lcom/facebook/tagging/model/TagProfile;->uid:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1189924
    iget-object v0, p0, Lcom/facebook/tagging/model/TagProfile;->type:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1189925
    iget-object v0, p0, Lcom/facebook/tagging/model/TagProfile;->text:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1189926
    iget-object v0, p0, Lcom/facebook/tagging/model/TagProfile;->subtext:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1189927
    iget-object v0, p0, Lcom/facebook/tagging/model/TagProfile;->photoUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1189928
    return-void
.end method
