.class public Lcom/facebook/gk/internal/OnUpgradeGkRefresher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final f:Ljava/lang/Class;

.field private static volatile g:Lcom/facebook/gk/internal/OnUpgradeGkRefresher;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/28W;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2ZJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1300501
    const-class v0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;

    sput-object v0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->f:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/28W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2ZJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1300468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1300469
    iput-object p1, p0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->a:LX/0Or;

    .line 1300470
    iput-object p2, p0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->b:LX/0Ot;

    .line 1300471
    iput-object p3, p0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->c:LX/0Ot;

    .line 1300472
    iput-object p4, p0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->d:LX/0Ot;

    .line 1300473
    iput-object p5, p0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->e:LX/0Ot;

    .line 1300474
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/gk/internal/OnUpgradeGkRefresher;
    .locals 9

    .prologue
    .line 1300488
    sget-object v0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->g:Lcom/facebook/gk/internal/OnUpgradeGkRefresher;

    if-nez v0, :cond_1

    .line 1300489
    const-class v1, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;

    monitor-enter v1

    .line 1300490
    :try_start_0
    sget-object v0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->g:Lcom/facebook/gk/internal/OnUpgradeGkRefresher;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1300491
    if-eqz v2, :cond_0

    .line 1300492
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1300493
    new-instance v3, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;

    const/16 v4, 0x15e7

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0xf9a

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2e3

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xb7d

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xab2

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;-><init>(LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1300494
    move-object v0, v3

    .line 1300495
    sput-object v0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->g:Lcom/facebook/gk/internal/OnUpgradeGkRefresher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1300496
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1300497
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1300498
    :cond_1
    sget-object v0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->g:Lcom/facebook/gk/internal/OnUpgradeGkRefresher;

    return-object v0

    .line 1300499
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1300500
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 6

    .prologue
    .line 1300475
    iget-object v0, p0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1300476
    :goto_0
    return-void

    .line 1300477
    :cond_0
    sget-object v0, LX/3g1;->c:LX/0Tn;

    const-class v1, LX/2ZJ;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1300478
    iget-object v1, p0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    invoke-interface {v2, v0, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1300479
    iget-object v0, p0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2ZJ;

    invoke-virtual {v0}, LX/2ZJ;->c()LX/2ZE;

    move-result-object v1

    .line 1300480
    :try_start_0
    iget-object v0, p0, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/28W;

    const-string v2, "onUpgradeGkRefresh"

    const-class v3, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1300481
    new-instance v4, LX/14U;

    invoke-direct {v4}, LX/14U;-><init>()V

    .line 1300482
    sget-object v5, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1300483
    iput-object v5, v4, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1300484
    move-object v4, v4

    .line 1300485
    invoke-virtual {v0, v2, v3, v1, v4}, LX/28W;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/List;LX/14U;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1300486
    :catch_0
    move-exception v0

    .line 1300487
    sget-object v1, Lcom/facebook/gk/internal/OnUpgradeGkRefresher;->f:Ljava/lang/Class;

    const-string v2, "Failed to refresh Gks on app upgrade."

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
