.class public Lcom/facebook/reportaproblem/base/bugreport/BugReportUploadService;
.super Landroid/app/IntentService;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1173584
    const-class v0, Lcom/facebook/reportaproblem/base/bugreport/BugReportUploadService;

    sput-object v0, Lcom/facebook/reportaproblem/base/bugreport/BugReportUploadService;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1173582
    const-string v0, "BugReportUploadService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 1173583
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;JLjava/util/Map;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1173528
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, p4}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 1173529
    const-string v1, "description"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1173530
    const-string v1, "category_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1173531
    const-string v1, "current_activity"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1173532
    return-object v0
.end method


# virtual methods
.method public final onHandleIntent(Landroid/content/Intent;)V
    .locals 13

    .prologue
    .line 1173533
    invoke-static {}, LX/0Pu;->a()LX/78l;

    move-result-object v6

    .line 1173534
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1173535
    if-eqz v0, :cond_5

    .line 1173536
    const-string v1, "param_key_bug_report_description"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1173537
    const-string v1, "param_key_report_directory"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1173538
    const-string v1, "param_key_current_activity"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1173539
    const-string v1, "param_key_bug_report_screenshot_files"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1173540
    const-string v2, "param_key_bug_report_activity_files"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1173541
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 1173542
    invoke-interface {v5, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1173543
    invoke-interface {v5, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1173544
    const-string v1, "param_key_category_info"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    .line 1173545
    if-nez v0, :cond_0

    .line 1173546
    invoke-virtual {v6}, LX/78l;->c()Ljava/util/List;

    move-result-object v0

    .line 1173547
    invoke-virtual {v6}, LX/78l;->f()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1173548
    invoke-virtual {v6}, LX/78l;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1173549
    :goto_0
    invoke-virtual {v6}, LX/78l;->b()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1173550
    move-object v0, v0

    .line 1173551
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;

    invoke-virtual {v0}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->b()J

    move-result-wide v0

    move-wide v2, v0

    .line 1173552
    :goto_1
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1173553
    invoke-virtual {v6}, LX/78l;->k()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MT;

    .line 1173554
    :try_start_0
    invoke-interface {v0, v9}, LX/1MT;->getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1173555
    :catch_0
    move-exception v1

    .line 1173556
    sget-object v10, Lcom/facebook/reportaproblem/base/bugreport/BugReportUploadService;->a:Ljava/lang/Class;

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Failed to create file for provider: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v10, v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 1173557
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->b()J

    move-result-wide v0

    move-wide v2, v0

    goto :goto_1

    .line 1173558
    :cond_1
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1173559
    invoke-virtual {v6}, LX/78l;->l()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ze;

    .line 1173560
    invoke-interface {v0}, LX/0Ze;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_3

    .line 1173561
    :cond_2
    :try_start_1
    const-string v0, "extra_data.txt"

    invoke-static {v9, v0, v1}, LX/4l9;->a(Ljava/io/File;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    move-result-object v0

    .line 1173562
    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1173563
    :goto_4
    invoke-virtual {v6}, LX/78l;->j()Ljava/util/Map;

    move-result-object v0

    invoke-static {v4, v8, v2, v3, v0}, Lcom/facebook/reportaproblem/base/bugreport/BugReportUploadService;->a(Ljava/lang/String;Ljava/lang/String;JLjava/util/Map;)Ljava/util/Map;

    move-result-object v4

    .line 1173564
    new-instance v0, LX/78q;

    invoke-virtual {v6}, LX/78l;->g()Ljava/lang/String;

    move-result-object v1

    .line 1173565
    sget-object v2, LX/78m;->h:Ljava/lang/String;

    move-object v2, v2

    .line 1173566
    invoke-virtual {v6}, LX/78l;->h()Ljava/lang/String;

    move-result-object v3

    invoke-direct/range {v0 .. v5}, LX/78q;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/Set;)V

    .line 1173567
    invoke-virtual {v6}, LX/78l;->i()LX/795;

    move-result-object v1

    .line 1173568
    :try_start_2
    iget-object v2, v1, LX/795;->b:LX/11H;

    iget-object v4, v1, LX/795;->c:LX/794;

    const-class v5, LX/795;

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    invoke-virtual {v2, v4, v0, v5}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1173569
    :goto_5
    invoke-virtual {v9}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 1173570
    if-eqz v1, :cond_4

    .line 1173571
    array-length v2, v1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 1173572
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1173573
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 1173574
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1173575
    :cond_4
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1173576
    :cond_5
    return-void

    .line 1173577
    :catch_1
    move-exception v0

    .line 1173578
    sget-object v1, Lcom/facebook/reportaproblem/base/bugreport/BugReportUploadService;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v7, "Failed to save background data"

    invoke-static {v1, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 1173579
    :cond_6
    invoke-virtual {v6}, LX/78l;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0

    .line 1173580
    :catch_2
    move-exception v2

    .line 1173581
    sget-object v4, LX/795;->a:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Unable to upload bug report."

    invoke-static {v4, v5, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5
.end method
