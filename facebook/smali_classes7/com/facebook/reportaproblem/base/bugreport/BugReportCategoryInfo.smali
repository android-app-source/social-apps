.class public Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1173490
    new-instance v0, LX/78o;

    invoke-direct {v0}, LX/78o;-><init>()V

    sput-object v0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1173491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1173492
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->a:Ljava/lang/String;

    .line 1173493
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->b:J

    .line 1173494
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->c:I

    .line 1173495
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JI)V
    .locals 0

    .prologue
    .line 1173496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1173497
    iput-object p1, p0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->a:Ljava/lang/String;

    .line 1173498
    iput-wide p2, p0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->b:J

    .line 1173499
    iput p4, p0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->c:I

    .line 1173500
    return-void
.end method


# virtual methods
.method public final b()J
    .locals 2

    .prologue
    .line 1173501
    iget-wide v0, p0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->b:J

    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1173502
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1173503
    iget-object v0, p0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1173504
    iget-wide v0, p0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1173505
    iget v0, p0, Lcom/facebook/reportaproblem/base/bugreport/BugReportCategoryInfo;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1173506
    return-void
.end method
