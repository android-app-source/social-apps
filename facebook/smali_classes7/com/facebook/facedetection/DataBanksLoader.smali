.class public Lcom/facebook/facedetection/DataBanksLoader;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field public static final a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1278886
    const-string v0, "fb_tracker"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1278887
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/facedetection/DataBanksLoader;->a:[I

    return-void

    :array_0
    .array-data 4
        0x5a94e
        0x33922
        0x2d352
        0x337f3
    .end array-data
.end method

.method public constructor <init>(LX/7yK;)V
    .locals 0

    .prologue
    .line 1278918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1278919
    invoke-direct {p0, p1}, Lcom/facebook/facedetection/DataBanksLoader;->a(LX/7yK;)V

    .line 1278920
    return-void
.end method

.method private a(LX/7yK;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x4

    .line 1278888
    const/4 v0, 0x4

    :try_start_0
    new-array v7, v0, [LX/7yI;

    const/4 v0, 0x0

    sget-object v1, LX/7yI;->LEFT_FULL_PROFILE:LX/7yI;

    aput-object v1, v7, v0

    const/4 v0, 0x1

    sget-object v1, LX/7yI;->LEFT_HALF_PROFILE:LX/7yI;

    aput-object v1, v7, v0

    const/4 v0, 0x2

    sget-object v1, LX/7yI;->FRONTAL_TILT_LEFT:LX/7yI;

    aput-object v1, v7, v0

    const/4 v0, 0x3

    sget-object v1, LX/7yI;->FRONTAL_MODEL:LX/7yI;

    aput-object v1, v7, v0

    .line 1278889
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/facedetection/DataBanksLoader;->init(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1278890
    :cond_0
    return-void

    .line 1278891
    :cond_1
    const/high16 v0, 0x60000

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1278892
    const-string v0, "%s.%s"

    const-string v2, "sImeta"

    const-string v3, "bin.gz.jet"

    invoke-static {v0, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x1c

    invoke-static {p1, v0, v1, v2}, Lcom/facebook/facedetection/DataBanksLoader;->a(LX/7yK;Ljava/lang/String;Ljava/nio/ByteBuffer;I)V

    .line 1278893
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/facebook/facedetection/DataBanksLoader;->loadWfsMeta(Ljava/nio/ByteBuffer;I)V

    move v3, v6

    .line 1278894
    :goto_0
    if-ge v3, v9, :cond_3

    .line 1278895
    aget-object v0, v7, v3

    .line 1278896
    const-string v2, "%s%d.%s"

    const-string v4, "sIcls"

    invoke-virtual {v0}, LX/7yI;->id()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v8, "bin.gz.jet"

    invoke-static {v2, v4, v5, v8}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1278897
    const/16 v4, 0xd02

    invoke-static {p1, v2, v1, v4}, Lcom/facebook/facedetection/DataBanksLoader;->a(LX/7yK;Ljava/lang/String;Ljava/nio/ByteBuffer;I)V

    .line 1278898
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    invoke-virtual {v0}, LX/7yI;->id()I

    move-result v4

    invoke-virtual {v0}, LX/7yI;->reflectedId()I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/facedetection/DataBanksLoader;->loadWfsClass(Ljava/nio/ByteBuffer;IIII)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1278899
    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "StageI allocation"

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 1278900
    :catch_0
    move-exception v0

    .line 1278901
    invoke-direct {p0}, Lcom/facebook/facedetection/DataBanksLoader;->dealloc()V

    .line 1278902
    throw v0

    .line 1278903
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1278904
    :cond_3
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/facedetection/DataBanksLoader;->finalizeWfsTree()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1278905
    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "finalizeWfsTree"

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    .line 1278906
    :catch_1
    move-exception v0

    .line 1278907
    invoke-direct {p0}, Lcom/facebook/facedetection/DataBanksLoader;->dealloc()V

    .line 1278908
    throw v0

    .line 1278909
    :cond_4
    const/4 v0, 0x4

    :try_start_2
    invoke-direct {p0, v0}, Lcom/facebook/facedetection/DataBanksLoader;->initStageIIData(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1278910
    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "initStageII"

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move v0, v6

    .line 1278911
    :goto_1
    if-ge v0, v9, :cond_0

    .line 1278912
    aget-object v2, v7, v0

    .line 1278913
    const-string v3, "%s%d.%s"

    const-string v4, "sIIcls"

    invoke-virtual {v2}, LX/7yI;->id()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v5, "bin.gz.jet"

    invoke-static {v3, v4, v2, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1278914
    sget-object v3, Lcom/facebook/facedetection/DataBanksLoader;->a:[I

    aget v3, v3, v0

    invoke-static {p1, v2, v1, v3}, Lcom/facebook/facedetection/DataBanksLoader;->a(LX/7yK;Ljava/lang/String;Ljava/nio/ByteBuffer;I)V

    .line 1278915
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    invoke-direct {p0, v1, v2, v0}, Lcom/facebook/facedetection/DataBanksLoader;->loadStageIIClass(Ljava/nio/ByteBuffer;II)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1278916
    new-instance v1, Ljava/lang/OutOfMemoryError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "loadStageIIClass "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1

    .line 1278917
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private static a(LX/7yK;Ljava/lang/String;Ljava/nio/ByteBuffer;I)V
    .locals 12

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 1278859
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1278860
    const/4 v1, 0x0

    .line 1278861
    :try_start_0
    new-instance v4, Ljava/io/BufferedInputStream;

    invoke-virtual {p0, p1}, LX/7yK;->a(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1278862
    :try_start_1
    new-instance v5, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v5, v4}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    const/4 v3, 0x0

    .line 1278863
    const/16 v0, 0x400

    :try_start_2
    new-array v0, v0, [B
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    move v1, v2

    .line 1278864
    :goto_0
    const/4 v6, 0x0

    const/16 v7, 0x400

    :try_start_3
    invoke-virtual {v5, v0, v6, v7}, Ljava/util/zip/GZIPInputStream;->read([BII)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    .line 1278865
    const/4 v7, 0x0

    invoke-virtual {p2, v0, v7, v6}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_6

    .line 1278866
    add-int/2addr v1, v6

    goto :goto_0

    .line 1278867
    :cond_0
    :try_start_4
    invoke-virtual {v5}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 1278868
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V

    .line 1278869
    if-eq v1, p3, :cond_4

    .line 1278870
    const-string v0, "DataBanksLoader"

    const-string v3, "corrupt data file %s, actual: %d, expected: %d"

    new-array v4, v10, [Ljava/lang/Object;

    aput-object p1, v4, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v8

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v9

    invoke-static {v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1278871
    new-instance v0, Ljava/io/IOException;

    const-string v2, "corrupt file %s: actual bytesize: %d, expected: %d"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, p1, v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1278872
    :catch_0
    move-exception v0

    move v1, v2

    :goto_1
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1278873
    :catchall_0
    move-exception v3

    move-object v11, v3

    move v3, v1

    move-object v1, v0

    move-object v0, v11

    :goto_2
    if-eqz v1, :cond_2

    :try_start_6
    invoke-virtual {v5}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :goto_3
    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1278874
    :catchall_1
    move-exception v0

    move-object v1, v4

    :goto_4
    if-eqz v1, :cond_1

    .line 1278875
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    .line 1278876
    :cond_1
    if-eq v3, p3, :cond_3

    .line 1278877
    const-string v0, "DataBanksLoader"

    const-string v1, "corrupt data file %s, actual: %d, expected: %d"

    new-array v4, v10, [Ljava/lang/Object;

    aput-object p1, v4, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v8

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v9

    invoke-static {v0, v1, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1278878
    new-instance v0, Ljava/io/IOException;

    const-string v1, "corrupt file %s: actual bytesize: %d, expected: %d"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, p1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1278879
    :catch_1
    move-exception v5

    :try_start_8
    invoke-static {v1, v5}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_2
    invoke-virtual {v5}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_3

    .line 1278880
    :cond_3
    throw v0

    .line 1278881
    :cond_4
    return-void

    .line 1278882
    :catchall_2
    move-exception v0

    move v3, v2

    goto :goto_4

    :catchall_3
    move-exception v0

    move v3, v2

    move-object v1, v4

    goto :goto_4

    :catchall_4
    move-exception v0

    move v3, v1

    move-object v1, v4

    goto :goto_4

    .line 1278883
    :catchall_5
    move-exception v0

    move-object v1, v3

    move v3, v2

    goto :goto_2

    :catchall_6
    move-exception v0

    move-object v11, v3

    move v3, v1

    move-object v1, v11

    goto :goto_2

    .line 1278884
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method private native dealloc()V
.end method

.method private native finalizeWfsTree()Z
.end method

.method private native init(I)Z
.end method

.method private native initStageIIData(I)Z
.end method

.method private native loadPointsClassifier(Ljava/nio/ByteBuffer;I)Z
.end method

.method private native loadStageIIClass(Ljava/nio/ByteBuffer;II)Z
.end method

.method private native loadWfsClass(Ljava/nio/ByteBuffer;IIII)Z
.end method

.method private native loadWfsMeta(Ljava/nio/ByteBuffer;I)V
.end method


# virtual methods
.method public final finalize()V
    .locals 0

    .prologue
    .line 1278885
    return-void
.end method
