.class public Lcom/facebook/facedetection/detector/MacerFaceDetector;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile m:Lcom/facebook/facedetection/detector/MacerFaceDetector;


# instance fields
.field public final a:LX/7yM;

.field private final b:LX/7yK;

.field private final c:LX/7yJ;

.field private final d:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final e:LX/7yN;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/1FZ;

.field private final h:LX/1HI;

.field private final i:Ljava/util/concurrent/ExecutorService;

.field private final j:LX/7yL;

.field private k:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private l:Lcom/facebook/facedetection/detector/MacerFaceDetector$NativePeer;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7yM;LX/7yK;LX/7yJ;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;LX/1HI;Ljava/util/concurrent/ExecutorService;LX/7yL;LX/7yN;LX/1FZ;)V
    .locals 1
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7yM;",
            "LX/7yK;",
            "LX/7yJ;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/1HI;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/7yL;",
            "LX/7yN;",
            "LX/1FZ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1279130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1279131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->k:Z

    .line 1279132
    iput-object p1, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->a:LX/7yM;

    .line 1279133
    iput-object p2, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->b:LX/7yK;

    .line 1279134
    iput-object p3, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->c:LX/7yJ;

    .line 1279135
    iput-object p4, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1279136
    iput-object p9, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->e:LX/7yN;

    .line 1279137
    iput-object p5, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->f:LX/0Ot;

    .line 1279138
    iput-object p6, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->h:LX/1HI;

    .line 1279139
    iput-object p7, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->i:Ljava/util/concurrent/ExecutorService;

    .line 1279140
    iput-object p8, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->j:LX/7yL;

    .line 1279141
    iput-object p10, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->g:LX/1FZ;

    .line 1279142
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/facedetection/detector/MacerFaceDetector;
    .locals 14

    .prologue
    .line 1279068
    sget-object v0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->m:Lcom/facebook/facedetection/detector/MacerFaceDetector;

    if-nez v0, :cond_1

    .line 1279069
    const-class v1, Lcom/facebook/facedetection/detector/MacerFaceDetector;

    monitor-enter v1

    .line 1279070
    :try_start_0
    sget-object v0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->m:Lcom/facebook/facedetection/detector/MacerFaceDetector;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1279071
    if-eqz v2, :cond_0

    .line 1279072
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1279073
    new-instance v3, Lcom/facebook/facedetection/detector/MacerFaceDetector;

    .line 1279074
    new-instance v6, LX/7yM;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/1tw;->a(LX/0QB;)LX/1tw;

    move-result-object v5

    check-cast v5, LX/1tw;

    invoke-direct {v6, v4, v5}, LX/7yM;-><init>(LX/0ad;LX/1tw;)V

    .line 1279075
    move-object v4, v6

    .line 1279076
    check-cast v4, LX/7yM;

    invoke-static {v0}, LX/7yK;->b(LX/0QB;)LX/7yK;

    move-result-object v5

    check-cast v5, LX/7yK;

    invoke-static {v0}, LX/7yJ;->a(LX/0QB;)LX/7yJ;

    move-result-object v6

    check-cast v6, LX/7yJ;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v7

    check-cast v7, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v8, 0x259

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v9

    check-cast v9, LX/1HI;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/7yL;->b(LX/0QB;)LX/7yL;

    move-result-object v11

    check-cast v11, LX/7yL;

    invoke-static {v0}, LX/7yN;->a(LX/0QB;)LX/7yN;

    move-result-object v12

    check-cast v12, LX/7yN;

    invoke-static {v0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v13

    check-cast v13, LX/1FZ;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/facedetection/detector/MacerFaceDetector;-><init>(LX/7yM;LX/7yK;LX/7yJ;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Ot;LX/1HI;Ljava/util/concurrent/ExecutorService;LX/7yL;LX/7yN;LX/1FZ;)V

    .line 1279077
    move-object v0, v3

    .line 1279078
    sput-object v0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->m:Lcom/facebook/facedetection/detector/MacerFaceDetector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1279079
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1279080
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1279081
    :cond_1
    sget-object v0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->m:Lcom/facebook/facedetection/detector/MacerFaceDetector;

    return-object v0

    .line 1279082
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1279083
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/facedetection/detector/MacerFaceDetector;[BLandroid/graphics/Bitmap;LX/7yQ;)Ljava/util/List;
    .locals 21
    .param p1    # [B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Landroid/graphics/Bitmap;",
            "LX/7yQ;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/7yO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1279119
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 1279120
    invoke-static/range {p1 .. p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-static {v3}, LX/7yS;->a(Ljava/nio/ByteBuffer;)LX/7yS;

    move-result-object v19

    .line 1279121
    new-instance v20, LX/7yR;

    invoke-direct/range {v20 .. v20}, LX/7yR;-><init>()V

    .line 1279122
    const/4 v3, 0x0

    move/from16 v17, v3

    :goto_0
    invoke-virtual/range {v19 .. v19}, LX/7yS;->a()I

    move-result v3

    move/from16 v0, v17

    if-ge v0, v3, :cond_2

    .line 1279123
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/7yS;->a(LX/7yR;I)LX/7yR;

    .line 1279124
    if-eqz p3, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual/range {v20 .. v20}, LX/7yR;->e()B

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    invoke-virtual/range {v20 .. v20}, LX/7yR;->e()B

    move-result v3

    const/16 v4, 0xd

    if-eq v3, v4, :cond_0

    const/4 v3, 0x1

    move v14, v3

    .line 1279125
    :goto_1
    new-instance v3, LX/7yO;

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual/range {v20 .. v20}, LX/7yR;->a()F

    move-result v5

    invoke-virtual/range {v20 .. v20}, LX/7yR;->c()F

    move-result v6

    add-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-virtual/range {v20 .. v20}, LX/7yR;->d()F

    move-result v6

    invoke-virtual/range {v20 .. v20}, LX/7yR;->b()F

    move-result v7

    add-float/2addr v6, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    invoke-virtual/range {v20 .. v20}, LX/7yR;->a()F

    move-result v7

    invoke-virtual/range {v20 .. v20}, LX/7yR;->b()F

    move-result v8

    invoke-virtual/range {v20 .. v20}, LX/7yR;->c()F

    move-result v9

    invoke-virtual/range {v20 .. v20}, LX/7yR;->d()F

    move-result v10

    const/4 v11, 0x1

    invoke-virtual/range {v20 .. v20}, LX/7yR;->e()B

    move-result v12

    invoke-virtual/range {v20 .. v20}, LX/7yR;->f()F

    move-result v13

    if-eqz v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->j:LX/7yL;

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v14, v0, v1, v2}, LX/7yL;->a(LX/7yR;Landroid/graphics/Bitmap;LX/7yQ;)[B

    move-result-object v14

    :goto_2
    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-direct/range {v3 .. v16}, LX/7yO;-><init>(FFFFFFFIIF[BII)V

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1279126
    add-int/lit8 v3, v17, 0x1

    move/from16 v17, v3

    goto :goto_0

    .line 1279127
    :cond_0
    const/4 v3, 0x0

    move v14, v3

    goto :goto_1

    .line 1279128
    :cond_1
    const/4 v14, 0x0

    goto :goto_2

    .line 1279129
    :cond_2
    return-object v18
.end method

.method private static declared-synchronized b(Lcom/facebook/facedetection/detector/MacerFaceDetector;)Lcom/facebook/facedetection/detector/MacerFaceDetector$NativePeer;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1279143
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->e:LX/7yN;

    .line 1279144
    iget-object v1, v0, LX/7yN;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1279145
    iget-object v1, v0, LX/7yN;->b:LX/0Uh;

    const/16 v2, 0x21a

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/7yN;->a:Ljava/lang/Boolean;

    .line 1279146
    :cond_0
    iget-object v1, v0, LX/7yN;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v1, v1

    .line 1279147
    if-eqz v1, :cond_5

    iget-object v1, v0, LX/7yN;->c:LX/1tw;

    invoke-virtual {v1}, LX/1tw;->a()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, LX/7yN;->c:LX/1tw;

    invoke-virtual {v1}, LX/1tw;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_1
    const/4 v1, 0x1

    :goto_0
    move v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1279148
    if-nez v0, :cond_2

    .line 1279149
    const/4 v0, 0x0

    .line 1279150
    :goto_1
    monitor-exit p0

    return-object v0

    .line 1279151
    :cond_2
    :try_start_1
    iget-boolean v0, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->k:Z

    if-eqz v0, :cond_3

    .line 1279152
    iget-object v0, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->l:Lcom/facebook/facedetection/detector/MacerFaceDetector$NativePeer;

    goto :goto_1

    .line 1279153
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->k:Z

    .line 1279154
    iget-object v0, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->l:Lcom/facebook/facedetection/detector/MacerFaceDetector$NativePeer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_4

    .line 1279155
    :try_start_2
    iget-object v0, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->b:LX/7yK;

    invoke-virtual {v0}, LX/7yK;->a()V

    .line 1279156
    new-instance v0, Lcom/facebook/facedetection/DataBanksLoader;

    iget-object v1, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->b:LX/7yK;

    invoke-direct {v0, v1}, Lcom/facebook/facedetection/DataBanksLoader;-><init>(LX/7yK;)V

    .line 1279157
    new-instance v0, Lcom/facebook/facedetection/detector/MacerFaceDetector$NativePeer;

    invoke-direct {v0}, Lcom/facebook/facedetection/detector/MacerFaceDetector$NativePeer;-><init>()V

    iput-object v0, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->l:Lcom/facebook/facedetection/detector/MacerFaceDetector$NativePeer;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/nio/BufferOverflowException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1279158
    :cond_4
    :goto_2
    :try_start_3
    iget-object v0, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->l:Lcom/facebook/facedetection/detector/MacerFaceDetector$NativePeer;

    goto :goto_1

    .line 1279159
    :catch_0
    move-exception v0

    .line 1279160
    iget-object v1, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->c:LX/7yJ;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "IOException "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/7yJ;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 1279161
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1279162
    :catch_1
    :try_start_4
    iget-object v0, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->c:LX/7yJ;

    const-string v1, "OutOfMemory"

    invoke-virtual {v0, v1}, LX/7yJ;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 1279163
    :catch_2
    iget-object v0, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->c:LX/7yJ;

    const-string v1, "BufferOverflow"

    invoke-virtual {v0, v1}, LX/7yJ;->a(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;IZ)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "IZ)",
            "Ljava/util/List",
            "<",
            "LX/7yO;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x2

    const v5, 0x3b0003

    .line 1279093
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1279094
    iget-object v0, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "MacerFaceDetector"

    const-string v2, "input bitmap is recycled"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1279095
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1279096
    :cond_0
    :goto_0
    return-object v0

    .line 1279097
    :cond_1
    if-eqz p2, :cond_2

    .line 1279098
    iget-object v0, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "MacerFaceDetector"

    const-string v2, "MacerFaceDetector supports only 0 oriented bitmaps"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1279099
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 1279100
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/16 v2, 0x40

    if-ge v1, v2, :cond_3

    .line 1279101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 1279102
    :cond_3
    invoke-static {p0}, Lcom/facebook/facedetection/detector/MacerFaceDetector;->b(Lcom/facebook/facedetection/detector/MacerFaceDetector;)Lcom/facebook/facedetection/detector/MacerFaceDetector$NativePeer;

    move-result-object v1

    .line 1279103
    if-nez v1, :cond_4

    .line 1279104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 1279105
    :cond_4
    iget-object v2, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1279106
    :try_start_0
    iget-object v2, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x3b0003

    const/16 v4, 0x3b

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1279107
    :try_start_1
    iget-object v2, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->a:LX/7yM;

    invoke-virtual {v2}, LX/7yM;->a()[B

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/facebook/facedetection/detector/MacerFaceDetector$NativePeer;->detect(Landroid/graphics/Bitmap;[B)[B

    move-result-object v2

    .line 1279108
    iget-object v1, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x3b0003

    const/16 v4, 0xd

    invoke-interface {v1, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 1279109
    if-eqz v2, :cond_5

    .line 1279110
    if-eqz p3, :cond_6

    iget-object v1, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->a:LX/7yM;

    invoke-virtual {v1}, LX/7yM;->c()LX/7yP;

    move-result-object v1

    invoke-virtual {v1}, LX/7yP;->c()LX/7yQ;

    move-result-object v1

    :goto_1
    invoke-static {p0, v2, p1, v1}, Lcom/facebook/facedetection/detector/MacerFaceDetector;->a(Lcom/facebook/facedetection/detector/MacerFaceDetector;[BLandroid/graphics/Bitmap;LX/7yQ;)Ljava/util/List;

    move-result-object v0

    .line 1279111
    iget-object v1, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x3b0003

    const/16 v3, 0xe

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1279112
    :cond_5
    :goto_2
    iget-object v1, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1279113
    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/16 :goto_0

    :cond_6
    move-object v1, v0

    .line 1279114
    goto :goto_1

    .line 1279115
    :catch_0
    move-exception v1

    .line 1279116
    :try_start_2
    iget-object v2, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->c:LX/7yJ;

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/7yJ;->a(Ljava/lang/String;)V

    .line 1279117
    iget-object v1, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x3b0003

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 1279118
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    throw v0
.end method

.method public final a(Ljava/nio/ByteBuffer;III)[B
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1279084
    invoke-static {p0}, Lcom/facebook/facedetection/detector/MacerFaceDetector;->b(Lcom/facebook/facedetection/detector/MacerFaceDetector;)Lcom/facebook/facedetection/detector/MacerFaceDetector$NativePeer;

    move-result-object v0

    .line 1279085
    if-nez v0, :cond_0

    .line 1279086
    :goto_0
    return-object v6

    .line 1279087
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->a:LX/7yM;

    invoke-virtual {v1}, LX/7yM;->b()[B

    move-result-object v5

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/facedetection/detector/MacerFaceDetector$NativePeer;->detectInFrame(Ljava/nio/ByteBuffer;III[B)[B
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_1
    move-object v6, v0

    .line 1279088
    goto :goto_0

    .line 1279089
    :catch_0
    move-exception v0

    .line 1279090
    :try_start_1
    iget-object v1, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector;->c:LX/7yJ;

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/7yJ;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v6

    .line 1279091
    goto :goto_1

    .line 1279092
    :catchall_0
    move-exception v0

    throw v0
.end method
