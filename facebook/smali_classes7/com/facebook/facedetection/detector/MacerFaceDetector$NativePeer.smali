.class public final Lcom/facebook/facedetection/detector/MacerFaceDetector$NativePeer;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1279063
    const-string v0, "fb_tracker"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1279064
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1279065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1279066
    invoke-static {}, Lcom/facebook/facedetection/detector/MacerFaceDetector$NativePeer;->initHybrid()Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facedetection/detector/MacerFaceDetector$NativePeer;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1279067
    return-void
.end method

.method private static native initHybrid()Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public native detect(Landroid/graphics/Bitmap;[B)[B
.end method

.method public native detectInFrame(Ljava/nio/ByteBuffer;III[B)[B
.end method
