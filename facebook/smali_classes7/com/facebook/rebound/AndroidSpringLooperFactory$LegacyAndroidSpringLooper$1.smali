.class public final Lcom/facebook/rebound/AndroidSpringLooperFactory$LegacyAndroidSpringLooper$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/8YF;


# direct methods
.method public constructor <init>(LX/8YF;)V
    .locals 0

    .prologue
    .line 1355966
    iput-object p1, p0, Lcom/facebook/rebound/AndroidSpringLooperFactory$LegacyAndroidSpringLooper$1;->a:LX/8YF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1355967
    iget-object v0, p0, Lcom/facebook/rebound/AndroidSpringLooperFactory$LegacyAndroidSpringLooper$1;->a:LX/8YF;

    iget-boolean v0, v0, LX/8YF;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rebound/AndroidSpringLooperFactory$LegacyAndroidSpringLooper$1;->a:LX/8YF;

    iget-object v0, v0, LX/8YD;->a:LX/8YH;

    if-nez v0, :cond_1

    .line 1355968
    :cond_0
    :goto_0
    return-void

    .line 1355969
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1355970
    iget-object v2, p0, Lcom/facebook/rebound/AndroidSpringLooperFactory$LegacyAndroidSpringLooper$1;->a:LX/8YF;

    iget-object v2, v2, LX/8YD;->a:LX/8YH;

    iget-object v3, p0, Lcom/facebook/rebound/AndroidSpringLooperFactory$LegacyAndroidSpringLooper$1;->a:LX/8YF;

    iget-wide v4, v3, LX/8YF;->e:J

    sub-long v4, v0, v4

    long-to-double v4, v4

    invoke-virtual {v2, v4, v5}, LX/8YH;->a(D)V

    .line 1355971
    iget-object v2, p0, Lcom/facebook/rebound/AndroidSpringLooperFactory$LegacyAndroidSpringLooper$1;->a:LX/8YF;

    .line 1355972
    iput-wide v0, v2, LX/8YF;->e:J

    .line 1355973
    iget-object v0, p0, Lcom/facebook/rebound/AndroidSpringLooperFactory$LegacyAndroidSpringLooper$1;->a:LX/8YF;

    iget-object v0, v0, LX/8YF;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/rebound/AndroidSpringLooperFactory$LegacyAndroidSpringLooper$1;->a:LX/8YF;

    iget-object v1, v1, LX/8YF;->c:Ljava/lang/Runnable;

    const v2, -0xf5c9377

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
