.class public Lcom/facebook/search/common/SearchDarkColoredTabProgressListenerBadgeTextView;
.super Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1179833
    invoke-direct {p0, p1}, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;-><init>(Landroid/content/Context;)V

    .line 1179834
    invoke-direct {p0}, Lcom/facebook/search/common/SearchDarkColoredTabProgressListenerBadgeTextView;->a()V

    .line 1179835
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1179836
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1179837
    invoke-direct {p0}, Lcom/facebook/search/common/SearchDarkColoredTabProgressListenerBadgeTextView;->a()V

    .line 1179838
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1179839
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1179840
    invoke-direct {p0}, Lcom/facebook/search/common/SearchDarkColoredTabProgressListenerBadgeTextView;->a()V

    .line 1179841
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1179842
    sget-object v0, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v1, LX/0xr;->MEDIUM:LX/0xr;

    invoke-virtual {p0}, Lcom/facebook/search/common/SearchDarkColoredTabProgressListenerBadgeTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 1179843
    invoke-virtual {p0}, Lcom/facebook/search/common/SearchDarkColoredTabProgressListenerBadgeTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1179844
    const v1, 0x7f0a008d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1179845
    iput v1, p0, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->b:I

    .line 1179846
    const v1, 0x7f0a00bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1179847
    iput v0, p0, Lcom/facebook/fbui/pagerindicator/ColoredTabProgressListenerBadgeTextView;->c:I

    .line 1179848
    return-void
.end method
