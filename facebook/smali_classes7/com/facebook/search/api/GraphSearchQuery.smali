.class public Lcom/facebook/search/api/GraphSearchQuery;
.super LX/7B6;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Lcom/facebook/search/api/GraphSearchQuery;


# instance fields
.field public final f:LX/7BH;

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:LX/103;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:LX/7B5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Z

.field public l:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field public m:Z

.field public n:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 1177903
    new-instance v0, LX/7B1;

    invoke-direct {v0}, LX/7B1;-><init>()V

    sput-object v0, Lcom/facebook/search/api/GraphSearchQuery;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 1177904
    new-instance v0, Lcom/facebook/search/api/GraphSearchQuery;

    const-string v1, ""

    sget-object v2, LX/7BH;->LIGHT:LX/7BH;

    const-string v3, ""

    const/4 v7, 0x0

    .line 1177905
    sget-object v5, LX/0Rg;->a:LX/0Rg;

    move-object v8, v5

    .line 1177906
    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v8}, Lcom/facebook/search/api/GraphSearchQuery;-><init>(Ljava/lang/String;LX/7BH;Ljava/lang/String;LX/103;Ljava/lang/String;LX/7B5;ZLX/0P1;)V

    sput-object v0, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    return-void
.end method

.method public constructor <init>(LX/7B3;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1177778
    invoke-direct {p0, p1}, LX/7B6;-><init>(LX/7B2;)V

    .line 1177779
    iput-boolean v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->m:Z

    .line 1177780
    iput-boolean v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->n:Z

    .line 1177781
    iget-object v0, p1, LX/7B3;->f:LX/7BH;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/7B3;->f:LX/7BH;

    :goto_0
    iput-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->f:LX/7BH;

    .line 1177782
    iget-object v0, p1, LX/7B3;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    .line 1177783
    iget-object v0, p1, LX/7B3;->i:LX/103;

    iput-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    .line 1177784
    iget-object v0, p1, LX/7B3;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    .line 1177785
    iget-object v0, p1, LX/7B3;->j:LX/7B5;

    iput-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->j:LX/7B5;

    .line 1177786
    iget-boolean v0, p1, LX/7B3;->k:Z

    iput-boolean v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->k:Z

    .line 1177787
    iget-object v0, p1, LX/7B3;->l:LX/0P1;

    iput-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    .line 1177788
    invoke-direct {p0}, Lcom/facebook/search/api/GraphSearchQuery;->p()V

    .line 1177789
    return-void

    .line 1177790
    :cond_0
    sget-object v0, LX/7BH;->LIGHT:LX/7BH;

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;LX/7BH;Ljava/lang/String;LX/103;Ljava/lang/String;LX/7B5;ZLX/0P1;)V
    .locals 3
    .param p6    # LX/7B5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/7BH;",
            "Ljava/lang/String;",
            "LX/103;",
            "Ljava/lang/String;",
            "LX/7B5;",
            "Z",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1177888
    if-eqz p7, :cond_0

    if-eqz p6, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p6}, LX/7B5;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1177889
    :goto_0
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 1177890
    invoke-direct {p0, p1, v0, v1}, LX/7B6;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0P1;)V

    .line 1177891
    iput-boolean v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->m:Z

    .line 1177892
    iput-boolean v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->n:Z

    .line 1177893
    iput-object p2, p0, Lcom/facebook/search/api/GraphSearchQuery;->f:LX/7BH;

    .line 1177894
    iput-object p3, p0, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    .line 1177895
    iput-object p4, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    .line 1177896
    iput-object p5, p0, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    .line 1177897
    iput-object p6, p0, Lcom/facebook/search/api/GraphSearchQuery;->j:LX/7B5;

    .line 1177898
    iput-boolean p7, p0, Lcom/facebook/search/api/GraphSearchQuery;->k:Z

    .line 1177899
    iput-object p8, p0, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    .line 1177900
    invoke-direct {p0}, Lcom/facebook/search/api/GraphSearchQuery;->p()V

    .line 1177901
    return-void

    :cond_0
    move-object v0, p3

    .line 1177902
    goto :goto_0
.end method

.method public synthetic constructor <init>(Ljava/lang/String;LX/7BH;Ljava/lang/String;LX/103;Ljava/lang/String;LX/7B5;ZLX/0P1;B)V
    .locals 0

    .prologue
    .line 1177887
    invoke-direct/range {p0 .. p8}, Lcom/facebook/search/api/GraphSearchQuery;-><init>(Ljava/lang/String;LX/7BH;Ljava/lang/String;LX/103;Ljava/lang/String;LX/7B5;ZLX/0P1;)V

    return-void
.end method

.method public static a(Lcom/facebook/search/api/GraphSearchQuery;)LX/7BH;
    .locals 1
    .param p0    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1177884
    if-eqz p0, :cond_0

    .line 1177885
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->f:LX/7BH;

    move-object v0, v0

    .line 1177886
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/7BH;->LIGHT:LX/7BH;

    goto :goto_0
.end method

.method public static a(LX/7BH;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;Z)Lcom/facebook/search/api/GraphSearchQuery;
    .locals 7

    .prologue
    .line 1177883
    const-string v1, ""

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7BH;Ljava/lang/String;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/7BH;Ljava/lang/String;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;)Lcom/facebook/search/api/GraphSearchQuery;
    .locals 7

    .prologue
    .line 1177882
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7BH;Ljava/lang/String;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/7BH;Ljava/lang/String;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;Z)Lcom/facebook/search/api/GraphSearchQuery;
    .locals 9

    .prologue
    .line 1177879
    new-instance v0, Lcom/facebook/search/api/GraphSearchQuery;

    .line 1177880
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v8, v1

    .line 1177881
    move-object v1, p1

    move-object v2, p0

    move-object v3, p3

    move-object v4, p2

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v8}, Lcom/facebook/search/api/GraphSearchQuery;-><init>(Ljava/lang/String;LX/7BH;Ljava/lang/String;LX/103;Ljava/lang/String;LX/7B5;ZLX/0P1;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/search/api/GraphSearchQuery;Ljava/lang/String;)Lcom/facebook/search/api/GraphSearchQuery;
    .locals 9

    .prologue
    .line 1177864
    if-nez p0, :cond_0

    .line 1177865
    sget-object p0, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    .line 1177866
    :cond_0
    new-instance v0, Lcom/facebook/search/api/GraphSearchQuery;

    .line 1177867
    iget-object v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->f:LX/7BH;

    move-object v2, v1

    .line 1177868
    iget-object v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    move-object v3, v1

    .line 1177869
    iget-object v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v4, v1

    .line 1177870
    iget-object v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v5, v1

    .line 1177871
    iget-object v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->j:LX/7B5;

    move-object v6, v1

    .line 1177872
    iget-boolean v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->k:Z

    move v7, v1

    .line 1177873
    iget-object v8, p0, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/facebook/search/api/GraphSearchQuery;-><init>(Ljava/lang/String;LX/7BH;Ljava/lang/String;LX/103;Ljava/lang/String;LX/7B5;ZLX/0P1;)V

    .line 1177874
    iget-boolean v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->m:Z

    move v1, v1

    .line 1177875
    iput-boolean v1, v0, Lcom/facebook/search/api/GraphSearchQuery;->m:Z

    .line 1177876
    iget-boolean v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->n:Z

    move v1, v1

    .line 1177877
    iput-boolean v1, v0, Lcom/facebook/search/api/GraphSearchQuery;->n:Z

    .line 1177878
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/search/api/GraphSearchQuery;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 1177861
    new-instance v0, Lcom/facebook/search/api/GraphSearchQuery;

    sget-object v2, LX/7BH;->LIGHT:LX/7BH;

    const-string v3, ""

    const/4 v7, 0x0

    .line 1177862
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v8, v1

    .line 1177863
    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v8}, Lcom/facebook/search/api/GraphSearchQuery;-><init>(Ljava/lang/String;LX/7BH;Ljava/lang/String;LX/103;Ljava/lang/String;LX/7B5;ZLX/0P1;)V

    return-object v0
.end method

.method public static b(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/api/GraphSearchQuery;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 1177832
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1177833
    sget-object v1, LX/103;->VIDEO:LX/103;

    if-eq v0, v1, :cond_0

    .line 1177834
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1177835
    sget-object v1, LX/103;->GROUP:LX/103;

    if-eq v0, v1, :cond_0

    .line 1177836
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1177837
    sget-object v1, LX/103;->USER:LX/103;

    if-eq v0, v1, :cond_0

    .line 1177838
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1177839
    sget-object v1, LX/103;->MARKETPLACE:LX/103;

    if-eq v0, v1, :cond_0

    .line 1177840
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1177841
    sget-object v1, LX/103;->COMMERCE:LX/103;

    if-ne v0, v1, :cond_1

    .line 1177842
    :cond_0
    new-instance v0, Lcom/facebook/search/api/GraphSearchQuery;

    .line 1177843
    iget-object v1, p0, LX/7B6;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1177844
    iget-object v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->f:LX/7BH;

    move-object v2, v2

    .line 1177845
    const-string v3, ""

    .line 1177846
    iget-object v5, p0, Lcom/facebook/search/api/GraphSearchQuery;->j:LX/7B5;

    move-object v6, v5

    .line 1177847
    const/4 v7, 0x0

    .line 1177848
    sget-object v5, LX/0Rg;->a:LX/0Rg;

    move-object v8, v5

    .line 1177849
    move-object v5, v4

    invoke-direct/range {v0 .. v8}, Lcom/facebook/search/api/GraphSearchQuery;-><init>(Ljava/lang/String;LX/7BH;Ljava/lang/String;LX/103;Ljava/lang/String;LX/7B5;ZLX/0P1;)V

    .line 1177850
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/facebook/search/api/GraphSearchQuery;

    .line 1177851
    iget-object v1, p0, LX/7B6;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1177852
    iget-object v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->f:LX/7BH;

    move-object v2, v2

    .line 1177853
    iget-object v3, p0, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    move-object v3, v3

    .line 1177854
    iget-object v5, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v9, v5

    .line 1177855
    iget-object v5, p0, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v5, v5

    .line 1177856
    iget-object v6, p0, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    move-object v6, v6

    .line 1177857
    if-nez v6, :cond_2

    move-object v6, v4

    .line 1177858
    :goto_1
    iget-boolean v4, p0, Lcom/facebook/search/api/GraphSearchQuery;->k:Z

    move v7, v4

    .line 1177859
    iget-object v4, p0, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    move-object v8, v4

    .line 1177860
    move-object v4, v9

    invoke-direct/range {v0 .. v8}, Lcom/facebook/search/api/GraphSearchQuery;-><init>(Ljava/lang/String;LX/7BH;Ljava/lang/String;LX/103;Ljava/lang/String;LX/7B5;ZLX/0P1;)V

    goto :goto_0

    :cond_2
    sget-object v6, LX/7B5;->SINGLE_STATE:LX/7B5;

    goto :goto_1
.end method

.method private p()V
    .locals 2

    .prologue
    .line 1177829
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    sget-object v1, LX/103;->PAGE:LX/103;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->n:Z

    .line 1177830
    return-void

    .line 1177831
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/7B4;)Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1177826
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    invoke-virtual {p1}, LX/7B4;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1177827
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    invoke-virtual {p1}, LX/7B4;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 1177828
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/7B4;Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1177824
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    invoke-virtual {p1}, LX/7B4;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    invoke-virtual {v0, v1}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    .line 1177825
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1177823
    invoke-virtual {p0}, Lcom/facebook/search/api/GraphSearchQuery;->k()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1177822
    invoke-virtual {p0}, Lcom/facebook/search/api/GraphSearchQuery;->k()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic c()LX/7B2;
    .locals 1

    .prologue
    .line 1177821
    invoke-virtual {p0}, Lcom/facebook/search/api/GraphSearchQuery;->m()LX/7B3;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1177820
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1177810
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_1

    .line 1177811
    :cond_0
    :goto_0
    return v0

    .line 1177812
    :cond_1
    check-cast p1, Lcom/facebook/search/api/GraphSearchQuery;

    .line 1177813
    iget-object v1, p0, LX/7B6;->b:Ljava/lang/String;

    iget-object v2, p1, LX/7B6;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/7B6;->c:Ljava/lang/String;

    iget-object v2, p1, LX/7B6;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->f:LX/7BH;

    iget-object v2, p1, Lcom/facebook/search/api/GraphSearchQuery;->f:LX/7BH;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    iget-object v2, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    iget-object v2, p1, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->k:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p1, Lcom/facebook/search/api/GraphSearchQuery;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1177814
    iget-boolean v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->m:Z

    move v1, v1

    .line 1177815
    iget-boolean v2, p1, Lcom/facebook/search/api/GraphSearchQuery;->m:Z

    move v2, v2

    .line 1177816
    if-ne v1, v2, :cond_0

    .line 1177817
    iget-boolean v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->n:Z

    move v1, v1

    .line 1177818
    iget-boolean v2, p1, Lcom/facebook/search/api/GraphSearchQuery;->n:Z

    move v2, v2

    .line 1177819
    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1177809
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/7B6;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/7B6;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->f:LX/7BH;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->m:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1177804
    iget-object v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    sget-object v3, LX/103;->URL:LX/103;

    if-ne v2, v3, :cond_2

    .line 1177805
    iget-object v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1177806
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1177807
    goto :goto_0

    .line 1177808
    :cond_2
    iget-object v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final m()LX/7B3;
    .locals 1

    .prologue
    .line 1177803
    new-instance v0, LX/7B3;

    invoke-direct {v0, p0, p0}, LX/7B3;-><init>(Lcom/facebook/search/api/GraphSearchQuery;Lcom/facebook/search/api/GraphSearchQuery;)V

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1177791
    iget-object v0, p0, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1177792
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->f:LX/7BH;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1177793
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1177794
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    invoke-virtual {v0}, LX/103;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1177795
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1177796
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->j:LX/7B5;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->j:LX/7B5;

    invoke-virtual {v0}, LX/7B5;->name()Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1177797
    iget-boolean v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->k:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1177798
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1177799
    iget-boolean v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->m:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1177800
    iget-boolean v0, p0, Lcom/facebook/search/api/GraphSearchQuery;->n:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1177801
    return-void

    :cond_1
    move-object v0, v1

    .line 1177802
    goto :goto_0
.end method
