.class public Lcom/facebook/search/api/SearchTypeaheadResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Z

.field public final B:Z

.field public C:Z

.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Landroid/net/Uri;

.field public final c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Landroid/net/Uri;

.field public final f:Landroid/net/Uri;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

.field public final l:Ljava/lang/String;

.field public final m:LX/7BK;

.field public final n:J

.field public final o:Z

.field public final p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

.field public final q:Z

.field public final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final s:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final t:Ljava/lang/String;

.field public final u:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/api/model/GraphSearchQueryFragment;",
            ">;"
        }
    .end annotation
.end field

.field public final v:I

.field public final w:I

.field public final x:Z

.field public final y:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1178391
    new-instance v0, LX/7BI;

    invoke-direct {v0}, LX/7BI;-><init>()V

    sput-object v0, Lcom/facebook/search/api/SearchTypeaheadResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7BL;)V
    .locals 6

    .prologue
    .line 1178317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178318
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->C:Z

    .line 1178319
    iget-object v0, p1, LX/7BL;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1178320
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->a:Ljava/lang/String;

    .line 1178321
    iget-object v0, p1, LX/7BL;->c:Landroid/net/Uri;

    move-object v0, v0

    .line 1178322
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://www.facebook.com/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1178323
    iget-wide v4, p1, LX/7BL;->n:J

    move-wide v2, v4

    .line 1178324
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->b:Landroid/net/Uri;

    .line 1178325
    iget-object v0, p1, LX/7BL;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v0, v0

    .line 1178326
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1178327
    iget-boolean v0, p1, LX/7BL;->o:Z

    move v0, v0

    .line 1178328
    iput-boolean v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->o:Z

    .line 1178329
    iget-object v0, p1, LX/7BL;->p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object v0, v0

    .line 1178330
    if-nez v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->NOT_VERIFIED:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    :goto_1
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1178331
    iget-object v0, p1, LX/7BL;->d:Landroid/net/Uri;

    move-object v0, v0

    .line 1178332
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->d:Landroid/net/Uri;

    .line 1178333
    iget-object v0, p1, LX/7BL;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 1178334
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://www.facebook.com/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1178335
    iget-wide v4, p1, LX/7BL;->n:J

    move-wide v2, v4

    .line 1178336
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->e:Landroid/net/Uri;

    .line 1178337
    iget-object v0, p1, LX/7BL;->f:Landroid/net/Uri;

    move-object v0, v0

    .line 1178338
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->f:Landroid/net/Uri;

    .line 1178339
    iget-object v0, p1, LX/7BL;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1178340
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    .line 1178341
    iget-object v0, p1, LX/7BL;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1178342
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->h:Ljava/lang/String;

    .line 1178343
    iget-object v0, p1, LX/7BL;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1178344
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->i:Ljava/lang/String;

    .line 1178345
    iget-object v0, p1, LX/7BL;->j:Ljava/lang/String;

    move-object v0, v0

    .line 1178346
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->j:Ljava/lang/String;

    .line 1178347
    iget-object v0, p1, LX/7BL;->k:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    move-object v0, v0

    .line 1178348
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->k:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    .line 1178349
    iget-object v0, p1, LX/7BL;->l:Ljava/lang/String;

    move-object v0, v0

    .line 1178350
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    .line 1178351
    iget-object v0, p1, LX/7BL;->m:LX/7BK;

    move-object v0, v0

    .line 1178352
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7BK;

    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    .line 1178353
    iget-wide v4, p1, LX/7BL;->n:J

    move-wide v0, v4

    .line 1178354
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    .line 1178355
    iget-object v0, p1, LX/7BL;->r:Ljava/util/List;

    move-object v0, v0

    .line 1178356
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->r:Ljava/util/List;

    .line 1178357
    iget-object v0, p1, LX/7BL;->s:LX/0Px;

    move-object v0, v0

    .line 1178358
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->s:LX/0Px;

    .line 1178359
    iget-object v0, p1, LX/7BL;->t:Ljava/lang/String;

    move-object v0, v0

    .line 1178360
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->t:Ljava/lang/String;

    .line 1178361
    iget-object v0, p1, LX/7BL;->u:LX/0Px;

    move-object v0, v0

    .line 1178362
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->u:LX/0Px;

    .line 1178363
    iget-boolean v0, p1, LX/7BL;->q:Z

    move v0, v0

    .line 1178364
    iput-boolean v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->q:Z

    .line 1178365
    iget v0, p1, LX/7BL;->v:I

    move v0, v0

    .line 1178366
    iput v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->v:I

    .line 1178367
    iget v0, p1, LX/7BL;->w:I

    move v0, v0

    .line 1178368
    iput v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->w:I

    .line 1178369
    iget-boolean v0, p1, LX/7BL;->x:Z

    move v0, v0

    .line 1178370
    iput-boolean v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->x:Z

    .line 1178371
    iget-object v0, p1, LX/7BL;->y:Ljava/util/Map;

    move-object v0, v0

    .line 1178372
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->y:Ljava/util/Map;

    .line 1178373
    iget-object v0, p1, LX/7BL;->z:Ljava/lang/String;

    move-object v0, v0

    .line 1178374
    iput-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->z:Ljava/lang/String;

    .line 1178375
    iget-boolean v0, p1, LX/7BL;->A:Z

    move v0, v0

    .line 1178376
    iput-boolean v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->A:Z

    .line 1178377
    iget-boolean v0, p1, LX/7BL;->B:Z

    move v0, v0

    .line 1178378
    iput-boolean v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->B:Z

    .line 1178379
    return-void

    .line 1178380
    :cond_0
    iget-object v0, p1, LX/7BL;->c:Landroid/net/Uri;

    move-object v0, v0

    .line 1178381
    goto/16 :goto_0

    .line 1178382
    :cond_1
    iget-object v0, p1, LX/7BL;->p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object v0, v0

    .line 1178383
    goto/16 :goto_1

    .line 1178384
    :cond_2
    iget-object v0, p1, LX/7BL;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 1178385
    goto/16 :goto_2
.end method

.method public static newBuilder()LX/7BL;
    .locals 1

    .prologue
    .line 1178316
    new-instance v0, LX/7BL;

    invoke-direct {v0}, LX/7BL;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1178307
    sget-object v0, LX/7BJ;->a:[I

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    invoke-virtual {v1}, LX/7BK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1178308
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1178309
    :pswitch_0
    const v0, 0x285feb

    goto :goto_0

    .line 1178310
    :pswitch_1
    const v0, 0x25d6af

    goto :goto_0

    .line 1178311
    :pswitch_2
    const v0, -0x3ff252d0

    goto :goto_0

    .line 1178312
    :pswitch_3
    const v0, 0x41e065f

    goto :goto_0

    .line 1178313
    :pswitch_4
    const v0, 0x403827a

    goto :goto_0

    .line 1178314
    :pswitch_5
    const v0, 0x30654a2e

    goto :goto_0

    .line 1178315
    :pswitch_6
    const v0, 0x361ab677

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1178386
    const/4 v1, 0x0

    .line 1178387
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->r:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1178388
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1178389
    if-eqz v0, :cond_0

    .line 1178390
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1178306
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1178302
    instance-of v1, p1, Lcom/facebook/search/api/SearchTypeaheadResult;

    if-nez v1, :cond_1

    .line 1178303
    :cond_0
    :goto_0
    return v0

    .line 1178304
    :cond_1
    check-cast p1, Lcom/facebook/search/api/SearchTypeaheadResult;

    .line 1178305
    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->b:Landroid/net/Uri;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->b:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->d:Landroid/net/Uri;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->d:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->o:Z

    iget-boolean v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->o:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->e:Landroid/net/Uri;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->e:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->f:Landroid/net/Uri;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->f:Landroid/net/Uri;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->h:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->h:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->i:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->i:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->j:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->j:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->r:Ljava/util/List;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->r:Ljava/util/List;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->t:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->t:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->q:Z

    iget-boolean v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->q:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->x:Z

    iget-boolean v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->x:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->y:Ljava/util/Map;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->y:Ljava/util/Map;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->z:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->z:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->A:Z

    iget-boolean v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->A:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->B:Z

    iget-boolean v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->B:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1178301
    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->b:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->o:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->d:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->e:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->f:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-wide v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->r:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->t:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-boolean v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->q:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-boolean v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->x:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->y:Ljava/util/Map;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->z:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-boolean v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->A:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-boolean v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->B:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1178300
    const-class v0, Lcom/facebook/search/api/SearchTypeaheadResult;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "category"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "fallbackPath"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "friendshipStatus"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "isVerified"

    iget-boolean v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->o:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "verificationStatus"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "redirectionUrl"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "path"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "photo"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->f:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "subtext"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "boldedSubtext"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "keywordType"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "keywordSource"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "text"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "type"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "uid"

    iget-wide v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "phoneNumbers"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->r:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "semantic"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "isScoped"

    iget-boolean v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->q:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "matchedPosition"

    iget v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->v:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "matchedLength"

    iget v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->w:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "isLive"

    iget-boolean v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->x:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "logInfo"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->y:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "entityId"

    iget-object v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "isConnected"

    iget-boolean v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->A:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "isMultiCompanyGroup"

    iget-boolean v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->B:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1178264
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178265
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1178266
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178267
    iget-boolean v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->o:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1178268
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178269
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->d:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1178270
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->e:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1178271
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->f:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1178272
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178273
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178274
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178275
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178276
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178277
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    if-nez v0, :cond_3

    const-string v0, ""

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178278
    iget-wide v4, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1178279
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->r:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1178280
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->s:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1178281
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178282
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->u:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1178283
    iget-boolean v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->q:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1178284
    iget v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->v:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1178285
    iget v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->w:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1178286
    iget-boolean v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->x:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1178287
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->y:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1178288
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->z:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178289
    iget-boolean v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->A:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1178290
    iget-boolean v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->B:Z

    if-eqz v0, :cond_7

    :goto_7
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1178291
    return-void

    .line 1178292
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1178293
    goto/16 :goto_1

    .line 1178294
    :cond_2
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->name()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1178295
    :cond_3
    iget-object v0, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    invoke-virtual {v0}, LX/7BK;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_4
    move v0, v2

    .line 1178296
    goto :goto_4

    :cond_5
    move v0, v2

    .line 1178297
    goto :goto_5

    :cond_6
    move v0, v2

    .line 1178298
    goto :goto_6

    :cond_7
    move v1, v2

    .line 1178299
    goto :goto_7
.end method
