.class public Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/GroupForSaleStoriesPostKind;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1177926
    new-instance v0, LX/7B7;

    invoke-direct {v0}, LX/7B7;-><init>()V

    sput-object v0, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7B8;)V
    .locals 1

    .prologue
    .line 1177927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1177928
    iget-boolean v0, p1, LX/7B8;->a:Z

    iput-boolean v0, p0, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->b:Z

    .line 1177929
    iget-boolean v0, p1, LX/7B8;->b:Z

    iput-boolean v0, p0, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->c:Z

    .line 1177930
    iget-object v0, p1, LX/7B8;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->a:Ljava/lang/String;

    .line 1177931
    iget-boolean v0, p1, LX/7B8;->c:Z

    iput-boolean v0, p0, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->d:Z

    .line 1177932
    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 1

    .prologue
    .line 1177933
    iget-boolean v0, p0, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->c:Z

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/GroupForSaleStoriesPostKind;
    .end annotation

    .prologue
    .line 1177934
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1177935
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 1177936
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1177937
    const/4 v0, 0x3

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->b:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->c:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->d:Z

    aput-boolean v2, v0, v1

    .line 1177938
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 1177939
    return-void
.end method
