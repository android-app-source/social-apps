.class public Lcom/facebook/search/api/GraphSearchQueryPlaceModifier;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/api/GraphSearchQueryPlaceModifier;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/graphql/enums/GraphQLPlaceType;

.field private final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1177956
    new-instance v0, LX/7B9;

    invoke-direct {v0}, LX/7B9;-><init>()V

    sput-object v0, Lcom/facebook/search/api/GraphSearchQueryPlaceModifier;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7BA;)V
    .locals 1

    .prologue
    .line 1177952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1177953
    iget-object v0, p1, LX/7BA;->a:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, Lcom/facebook/search/api/GraphSearchQueryPlaceModifier;->a:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 1177954
    iget-object v0, p1, LX/7BA;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/api/GraphSearchQueryPlaceModifier;->b:Ljava/lang/String;

    .line 1177955
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1177948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1177949
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, Lcom/facebook/search/api/GraphSearchQueryPlaceModifier;->a:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 1177950
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/api/GraphSearchQueryPlaceModifier;->b:Ljava/lang/String;

    .line 1177951
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1177944
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1177945
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQueryPlaceModifier;->a:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1177946
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQueryPlaceModifier;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1177947
    return-void
.end method
