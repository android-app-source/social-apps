.class public Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/7BZ;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final f:Lcom/facebook/search/api/GraphSearchQuery;

.field public final g:Ljava/lang/String;

.field public final h:I

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7BK;",
            ">;"
        }
    .end annotation
.end field

.field public final j:I

.field public final k:Ljava/lang/String;

.field public final l:Z

.field public final m:Ljava/lang/String;

.field public final n:LX/7BZ;

.field public final o:Ljava/lang/String;

.field public final p:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1178903
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->e:Ljava/util/Map;

    .line 1178904
    new-instance v0, LX/7BY;

    invoke-direct {v0}, LX/7BY;-><init>()V

    sput-object v0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7BS;)V
    .locals 1

    .prologue
    .line 1178938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178939
    iget-object v0, p1, LX/7BS;->a:Lcom/facebook/search/api/GraphSearchQuery;

    move-object v0, v0

    .line 1178940
    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    .line 1178941
    iget-object v0, p1, LX/7BS;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1178942
    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->g:Ljava/lang/String;

    .line 1178943
    iget v0, p1, LX/7BS;->c:I

    move v0, v0

    .line 1178944
    iput v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->h:I

    .line 1178945
    iget-object v0, p1, LX/7BS;->d:Ljava/util/List;

    move-object v0, v0

    .line 1178946
    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->i:Ljava/util/List;

    .line 1178947
    iget v0, p1, LX/7BS;->e:I

    move v0, v0

    .line 1178948
    iput v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->j:I

    .line 1178949
    iget-object v0, p1, LX/7BS;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1178950
    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->k:Ljava/lang/String;

    .line 1178951
    iget-boolean v0, p1, LX/7BS;->g:Z

    move v0, v0

    .line 1178952
    iput-boolean v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->l:Z

    .line 1178953
    iget-object v0, p1, LX/7BS;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1178954
    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->m:Ljava/lang/String;

    .line 1178955
    iget-object v0, p1, LX/7BS;->i:LX/7BZ;

    move-object v0, v0

    .line 1178956
    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    .line 1178957
    iget-object v0, p1, LX/7BS;->j:Ljava/lang/String;

    move-object v0, v0

    .line 1178958
    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->o:Ljava/lang/String;

    .line 1178959
    iget-object v0, p1, LX/7BS;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1178960
    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->p:Ljava/lang/String;

    .line 1178961
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1178924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178925
    const-class v0, Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/GraphSearchQuery;

    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    .line 1178926
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->g:Ljava/lang/String;

    .line 1178927
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->h:I

    .line 1178928
    const-class v0, LX/7BK;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->i:Ljava/util/List;

    .line 1178929
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->j:I

    .line 1178930
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->k:Ljava/lang/String;

    .line 1178931
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->l:Z

    .line 1178932
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->m:Ljava/lang/String;

    .line 1178933
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7BZ;->valueOf(Ljava/lang/String;)LX/7BZ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    .line 1178934
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->o:Ljava/lang/String;

    .line 1178935
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->p:Ljava/lang/String;

    .line 1178936
    return-void

    .line 1178937
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1178923
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1178962
    instance-of v1, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;

    if-nez v1, :cond_1

    .line 1178963
    :cond_0
    :goto_0
    return v0

    .line 1178964
    :cond_1
    check-cast p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;

    .line 1178965
    iget-object v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    iget-object v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->i:Ljava/util/List;

    iget-object v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->i:Ljava/util/List;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->k:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->k:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->l:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->m:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->m:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    iget-object v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->o:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->o:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->p:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->p:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 1178920
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    .line 1178921
    iget-object v3, v2, LX/7B6;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1178922
    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->i:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->m:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->o:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->p:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1178917
    const-class v0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;

    invoke-static {v0}, LX/0Qh;->toStringHelper(Ljava/lang/Class;)LX/0zA;

    move-result-object v0

    const-string v1, "queryText"

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    .line 1178918
    iget-object v3, v2, LX/7B6;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1178919
    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "typeaheadSessionId"

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "photoSize"

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->h:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "filter"

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->i:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "cached_ids"

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "limit"

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->j:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "noProfileImageUrls"

    iget-boolean v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->l:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    const-string v1, "friendlyName"

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "keywordMode"

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "rankingModel"

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "context"

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1178905
    iget-object v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1178906
    iget-object v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178907
    iget v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->h:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1178908
    iget-object v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->i:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1178909
    iget v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->j:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1178910
    iget-object v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->k:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178911
    iget-boolean v1, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->l:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1178912
    iget-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178913
    iget-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    invoke-virtual {v0}, LX/7BZ;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178914
    iget-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178915
    iget-object v0, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1178916
    return-void
.end method
