.class public final Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;
.super Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1178966
    new-instance v0, LX/7BR;

    invoke-direct {v0}, LX/7BR;-><init>()V

    sput-object v0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7BT;)V
    .locals 1

    .prologue
    .line 1178967
    invoke-direct {p0, p1}, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;-><init>(LX/7BS;)V

    .line 1178968
    iget v0, p1, LX/7BT;->a:I

    move v0, v0

    .line 1178969
    iput v0, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->a:I

    .line 1178970
    iget v0, p1, LX/7BT;->b:I

    move v0, v0

    .line 1178971
    iput v0, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->b:I

    .line 1178972
    iget v0, p1, LX/7BT;->c:I

    move v0, v0

    .line 1178973
    iput v0, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->c:I

    .line 1178974
    iget-boolean v0, p1, LX/7BT;->d:Z

    move v0, v0

    .line 1178975
    iput-boolean v0, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->d:Z

    .line 1178976
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1178977
    invoke-direct {p0, p1}, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;-><init>(Landroid/os/Parcel;)V

    .line 1178978
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->a:I

    .line 1178979
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->b:I

    .line 1178980
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->c:I

    .line 1178981
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->d:Z

    .line 1178982
    return-void

    .line 1178983
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1178984
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1178985
    instance-of v0, p1, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;

    if-nez v0, :cond_0

    move v0, v1

    .line 1178986
    :goto_0
    return v0

    :cond_0
    move-object v0, p1

    .line 1178987
    check-cast v0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;

    .line 1178988
    invoke-super {p0, p1}, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, v0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, v0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, v0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v0, v0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1178989
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    .line 1178990
    iget-object v3, v2, LX/7B6;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1178991
    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->i:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->m:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->o:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1178992
    const-class v0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;

    invoke-static {v0}, LX/0Qh;->toStringHelper(Ljava/lang/Class;)LX/0zA;

    move-result-object v0

    const-string v1, "queryText"

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    .line 1178993
    iget-object v3, v2, LX/7B6;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1178994
    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "typeaheadSessionId"

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "photoSize"

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->h:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "filter"

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->i:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "cached_ids"

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "limit"

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->j:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "noProfileImageUrls"

    iget-boolean v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->l:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    const-string v1, "friendlyName"

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "keywordMode"

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "rankingModel"

    iget-object v2, p0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "batchNum"

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->a:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "batchSize"

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->b:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "offset"

    iget v2, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->c:I

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;I)LX/0zA;

    move-result-object v0

    const-string v1, "canOverrideMaxResults"

    iget-boolean v2, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->d:Z

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Z)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1178995
    invoke-super {p0, p1, p2}, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1178996
    iget v0, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1178997
    iget v0, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1178998
    iget v0, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1178999
    iget-boolean v0, p0, Lcom/facebook/search/api/protocol/FetchBatchSearchTypeaheadResultParams;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1179000
    return-void

    .line 1179001
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
