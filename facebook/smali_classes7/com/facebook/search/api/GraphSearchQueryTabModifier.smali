.class public Lcom/facebook/search/api/GraphSearchQueryTabModifier;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/api/GraphSearchQueryTabModifier;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Z

.field public b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1177970
    new-instance v0, LX/7BB;

    invoke-direct {v0}, LX/7BB;-><init>()V

    sput-object v0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7BC;)V
    .locals 1

    .prologue
    .line 1177971
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1177972
    iget-boolean v0, p1, LX/7BC;->b:Z

    iput-boolean v0, p0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;->b:Z

    .line 1177973
    iget-boolean v0, p1, LX/7BC;->a:Z

    iput-boolean v0, p0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;->a:Z

    .line 1177974
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1177975
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 1177976
    const/4 v0, 0x2

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;->b:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;->a:Z

    aput-boolean v2, v0, v1

    .line 1177977
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 1177978
    return-void
.end method
