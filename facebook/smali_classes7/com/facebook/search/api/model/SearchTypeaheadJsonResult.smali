.class public final Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/api/model/SearchTypeaheadJsonResultDeserializer;
.end annotation


# instance fields
.field public final category:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "category"
    .end annotation
.end field

.field public final fallbackPath:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fallback_path"
    .end annotation
.end field

.field public final friendshipStatus:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "friendship_status"
    .end annotation
.end field

.field public final isVerified:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_verified"
    .end annotation
.end field

.field public final matchedTokens:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "matched_tokens"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final nativeAndroidUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "native_android_url"
    .end annotation
.end field

.field public final path:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "path"
    .end annotation
.end field

.field public final photo:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "photo"
    .end annotation
.end field

.field public final savedState:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "saved_state"
    .end annotation
.end field

.field public final subtext:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "subtext"
    .end annotation
.end field

.field public final text:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text"
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uid"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1178790
    const-class v0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResultDeserializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1178791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178792
    iput-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->category:Ljava/lang/String;

    .line 1178793
    iput-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->friendshipStatus:Ljava/lang/String;

    .line 1178794
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->isVerified:Z

    .line 1178795
    iput-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->nativeAndroidUrl:Ljava/lang/String;

    .line 1178796
    iput-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->path:Ljava/lang/String;

    .line 1178797
    iput-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->fallbackPath:Ljava/lang/String;

    .line 1178798
    iput-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->photo:Ljava/lang/String;

    .line 1178799
    iput-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->subtext:Ljava/lang/String;

    .line 1178800
    iput-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->text:Ljava/lang/String;

    .line 1178801
    iput-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->type:Ljava/lang/String;

    .line 1178802
    iput-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->uid:Ljava/lang/String;

    .line 1178803
    iput-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->matchedTokens:LX/0Px;

    .line 1178804
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->savedState:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1178805
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1178806
    instance-of v1, p1, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;

    if-nez v1, :cond_1

    .line 1178807
    :cond_0
    :goto_0
    return v0

    .line 1178808
    :cond_1
    check-cast p1, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;

    .line 1178809
    iget-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->category:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->category:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->friendshipStatus:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->friendshipStatus:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->isVerified:Z

    iget-boolean v2, p1, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->isVerified:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->nativeAndroidUrl:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->nativeAndroidUrl:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->path:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->path:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->fallbackPath:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->fallbackPath:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->photo:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->photo:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->subtext:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->subtext:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->text:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->text:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->type:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->type:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->uid:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->uid:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->savedState:Lcom/facebook/graphql/enums/GraphQLSavedState;

    iget-object v2, p1, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->savedState:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1178810
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->category:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->friendshipStatus:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->isVerified:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->nativeAndroidUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->path:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->fallbackPath:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->photo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->subtext:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->text:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->uid:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->matchedTokens:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->savedState:Lcom/facebook/graphql/enums/GraphQLSavedState;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1178811
    const-class v0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "category"

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->category:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "friendshipStatus"

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->friendshipStatus:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "isVerified"

    iget-boolean v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->isVerified:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "redirectionUri"

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->nativeAndroidUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "path"

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->path:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "fallbackPath"

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->fallbackPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "photo"

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->photo:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "subtext"

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->subtext:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "text"

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->text:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "type"

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "uid"

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "matchedTokens"

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->matchedTokens:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "savedState"

    iget-object v2, p0, Lcom/facebook/search/api/model/SearchTypeaheadJsonResult;->savedState:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
