.class public Lcom/facebook/search/api/model/GraphSearchQueryFragment;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/api/model/GraphSearchQueryFragmentDeserializer;
.end annotation


# instance fields
.field public final isMatched:Ljava/lang/Boolean;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_matched"
    .end annotation
.end field

.field public final text:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1178617
    const-class v0, Lcom/facebook/search/api/model/GraphSearchQueryFragmentDeserializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1178613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178614
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/search/api/model/GraphSearchQueryFragment;->text:Ljava/lang/String;

    .line 1178615
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/api/model/GraphSearchQueryFragment;->isMatched:Ljava/lang/Boolean;

    .line 1178616
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1178609
    instance-of v1, p1, Lcom/facebook/search/api/model/GraphSearchQueryFragment;

    if-nez v1, :cond_1

    .line 1178610
    :cond_0
    :goto_0
    return v0

    .line 1178611
    :cond_1
    check-cast p1, Lcom/facebook/search/api/model/GraphSearchQueryFragment;

    .line 1178612
    iget-object v1, p0, Lcom/facebook/search/api/model/GraphSearchQueryFragment;->text:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/api/model/GraphSearchQueryFragment;->text:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/api/model/GraphSearchQueryFragment;->isMatched:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/facebook/search/api/model/GraphSearchQueryFragment;->isMatched:Ljava/lang/Boolean;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1178607
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/search/api/model/GraphSearchQueryFragment;->text:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/search/api/model/GraphSearchQueryFragment;->isMatched:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1178608
    const-class v0, Lcom/facebook/search/api/model/GraphSearchQueryFragment;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "text"

    iget-object v2, p0, Lcom/facebook/search/api/model/GraphSearchQueryFragment;->text:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "isMatched"

    iget-object v2, p0, Lcom/facebook/search/api/model/GraphSearchQueryFragment;->isMatched:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
