.class public Lcom/facebook/search/api/model/BatchGraphSearchJsonResponse;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/api/model/BatchGraphSearchJsonResponseDeserializer;
.end annotation


# instance fields
.field public final data:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;",
            ">;"
        }
    .end annotation
.end field

.field public final numTopResultsToShow:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "summary"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1178553
    const-class v0, Lcom/facebook/search/api/model/BatchGraphSearchJsonResponseDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1178554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178555
    iput-object v0, p0, Lcom/facebook/search/api/model/BatchGraphSearchJsonResponse;->data:Ljava/util/List;

    .line 1178556
    iput-object v0, p0, Lcom/facebook/search/api/model/BatchGraphSearchJsonResponse;->numTopResultsToShow:Ljava/util/Map;

    .line 1178557
    return-void
.end method
