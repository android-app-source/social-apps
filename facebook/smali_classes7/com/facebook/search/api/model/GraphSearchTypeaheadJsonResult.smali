.class public Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResultDeserializer;
.end annotation


# instance fields
.field public final allSharedStoriesCount:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "all_shared_stories_count"
    .end annotation
.end field

.field public final boldedSubtext:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "bolded_subtext"
    .end annotation
.end field

.field public final canLike:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "can_like"
    .end annotation
.end field

.field public final category:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "category"
    .end annotation
.end field

.field public final creationTime:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "creation_time"
    .end annotation
.end field

.field public final entityData:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "entity_data"
    .end annotation
.end field

.field public final entityId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "entity_id"
    .end annotation
.end field

.field public final externalUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "external_url"
    .end annotation
.end field

.field public final fallbackPath:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fallback_path"
    .end annotation
.end field

.field public final fragments:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "fragments"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/model/GraphSearchQueryFragment;",
            ">;"
        }
    .end annotation
.end field

.field public final friendshipStatus:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "friendship_status"
    .end annotation
.end field

.field public final grammarResultSetType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "grammar_result_set_type"
    .end annotation
.end field

.field public final grammarType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "grammar_type"
    .end annotation
.end field

.field public final isConnected:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_connected"
    .end annotation
.end field

.field public final isLive:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_live"
    .end annotation
.end field

.field public final isScoped:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_scoped"
    .end annotation
.end field

.field public final isVerified:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_verified"
    .end annotation
.end field

.field public final keywordSource:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "keyword_source"
    .end annotation
.end field

.field public final keywordType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "keyword_type"
    .end annotation
.end field

.field public final link:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field

.field public final logInfo:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "log_info"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final matchedLength:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "matched_length"
    .end annotation
.end field

.field public final matchedPos:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "matched_pos"
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final nativeAndroidUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "native_android_url"
    .end annotation
.end field

.field public final path:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "path"
    .end annotation
.end field

.field public final photoUri:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "photo"
    .end annotation
.end field

.field public final resultType:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "result_type"
    .end annotation
.end field

.field public final semantic:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "semantic"
    .end annotation
.end field

.field public final source:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "source"
    .end annotation
.end field

.field public final subtext:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "subtext"
    .end annotation
.end field

.field public final text:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text"
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uid"
    .end annotation
.end field

.field public final verificationStatus:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "verification_status"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1178698
    const-class v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResultDeserializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1178699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178700
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->grammarResultSetType:Ljava/lang/String;

    .line 1178701
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->resultType:Ljava/lang/String;

    .line 1178702
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1178703
    iput-object v0, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->fragments:Ljava/util/List;

    .line 1178704
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->semantic:Ljava/lang/String;

    .line 1178705
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->grammarType:Ljava/lang/String;

    .line 1178706
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->name:Ljava/lang/String;

    .line 1178707
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->category:Ljava/lang/String;

    .line 1178708
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->subtext:Ljava/lang/String;

    .line 1178709
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->boldedSubtext:Ljava/lang/String;

    .line 1178710
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->keywordType:Ljava/lang/String;

    .line 1178711
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->keywordSource:Ljava/lang/String;

    .line 1178712
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->photoUri:Ljava/lang/String;

    .line 1178713
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->matchedPos:Ljava/lang/String;

    .line 1178714
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->matchedLength:Ljava/lang/String;

    .line 1178715
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->friendshipStatus:Ljava/lang/String;

    .line 1178716
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->isVerified:Ljava/lang/String;

    .line 1178717
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->verificationStatus:Ljava/lang/String;

    .line 1178718
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->canLike:Ljava/lang/String;

    .line 1178719
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->path:Ljava/lang/String;

    .line 1178720
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->fallbackPath:Ljava/lang/String;

    .line 1178721
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->isScoped:Ljava/lang/String;

    .line 1178722
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->uid:Ljava/lang/String;

    .line 1178723
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->creationTime:Ljava/lang/String;

    .line 1178724
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->allSharedStoriesCount:Ljava/lang/String;

    .line 1178725
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->text:Ljava/lang/String;

    .line 1178726
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->source:Ljava/lang/String;

    .line 1178727
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->externalUrl:Ljava/lang/String;

    .line 1178728
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->link:Ljava/lang/String;

    .line 1178729
    iput-boolean v2, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->isLive:Z

    .line 1178730
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->nativeAndroidUrl:Ljava/lang/String;

    .line 1178731
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->entityData:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    .line 1178732
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->logInfo:Ljava/util/Map;

    .line 1178733
    iput-object v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->entityId:Ljava/lang/String;

    .line 1178734
    iput-boolean v2, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadJsonResult;->isConnected:Z

    .line 1178735
    return-void
.end method
