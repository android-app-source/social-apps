.class public Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJsonDeserializer;
.end annotation


# instance fields
.field public final category:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "category"
    .end annotation
.end field

.field public final doesViewerLike:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "does_viewer_like"
    .end annotation
.end field

.field public final friendshipStatus:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "friendship_status"
    .end annotation
.end field

.field public final groupJoinState:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "group_join_state"
    .end annotation
.end field

.field public final isVerified:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_verified"
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final profilePic:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "profile_pic"
    .end annotation
.end field

.field public final subtext:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "subtext"
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "type"
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uid"
    .end annotation
.end field

.field public final verificationStatus:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "verification_status"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1178640
    const-class v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJsonDeserializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1178641
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178642
    iput-object v0, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->uid:Ljava/lang/String;

    .line 1178643
    iput-object v0, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->name:Ljava/lang/String;

    .line 1178644
    iput-object v0, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->type:Ljava/lang/String;

    .line 1178645
    iput-object v0, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->category:Ljava/lang/String;

    .line 1178646
    iput-object v0, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->subtext:Ljava/lang/String;

    .line 1178647
    iput-object v0, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->profilePic:Ljava/lang/String;

    .line 1178648
    iput-boolean v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->isVerified:Z

    .line 1178649
    iput-object v0, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->friendshipStatus:Ljava/lang/String;

    .line 1178650
    iput-object v0, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->verificationStatus:Ljava/lang/String;

    .line 1178651
    iput-object v0, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->groupJoinState:Ljava/lang/String;

    .line 1178652
    iput-boolean v1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->doesViewerLike:Z

    .line 1178653
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1178654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178655
    iput-object p1, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->uid:Ljava/lang/String;

    .line 1178656
    iput-object p2, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->name:Ljava/lang/String;

    .line 1178657
    iput-object p3, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->type:Ljava/lang/String;

    .line 1178658
    iput-object p4, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->category:Ljava/lang/String;

    .line 1178659
    iput-object p5, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->subtext:Ljava/lang/String;

    .line 1178660
    iput-object p6, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->profilePic:Ljava/lang/String;

    .line 1178661
    iput-boolean p7, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->isVerified:Z

    .line 1178662
    iput-object p8, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->friendshipStatus:Ljava/lang/String;

    .line 1178663
    iput-object p9, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->verificationStatus:Ljava/lang/String;

    .line 1178664
    iput-object p10, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->groupJoinState:Ljava/lang/String;

    .line 1178665
    iput-boolean p11, p0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;->doesViewerLike:Z

    .line 1178666
    return-void
.end method
