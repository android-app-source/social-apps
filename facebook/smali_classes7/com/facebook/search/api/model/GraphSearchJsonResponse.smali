.class public Lcom/facebook/search/api/model/GraphSearchJsonResponse;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/api/model/GraphSearchJsonResponseDeserializer;
.end annotation


# instance fields
.field public final numTopResultsToShow:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "num_topresults_to_show"
    .end annotation
.end field

.field public final response:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "response"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1178580
    const-class v0, Lcom/facebook/search/api/model/GraphSearchJsonResponseDeserializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1178581
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1178582
    iput-object v0, p0, Lcom/facebook/search/api/model/GraphSearchJsonResponse;->response:Ljava/lang/String;

    .line 1178583
    iput-object v0, p0, Lcom/facebook/search/api/model/GraphSearchJsonResponse;->numTopResultsToShow:Ljava/lang/String;

    .line 1178584
    return-void
.end method
