.class public Lcom/facebook/search/logging/api/SearchTypeaheadSession;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/logging/api/SearchTypeaheadSession;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/search/logging/api/SearchTypeaheadSession;


# instance fields
.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1374816
    new-instance v0, LX/8cj;

    invoke-direct {v0}, LX/8cj;-><init>()V

    sput-object v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 1374817
    new-instance v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    invoke-direct {v0, v1, v1}, Lcom/facebook/search/logging/api/SearchTypeaheadSession;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->a:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1374818
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1374819
    iput-object p1, p0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    .line 1374820
    iput-object p2, p0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    .line 1374821
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1374822
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1374823
    iget-object v0, p0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1374824
    iget-object v0, p0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1374825
    return-void
.end method
