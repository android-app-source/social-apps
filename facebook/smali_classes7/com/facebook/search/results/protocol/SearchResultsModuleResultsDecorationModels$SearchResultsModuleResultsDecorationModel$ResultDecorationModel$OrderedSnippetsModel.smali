.class public final Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6b262248
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1383877
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1383876
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1383874
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1383875
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1383871
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1383872
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1383873
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;)Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;
    .locals 2

    .prologue
    .line 1383863
    if-nez p0, :cond_0

    .line 1383864
    const/4 p0, 0x0

    .line 1383865
    :goto_0
    return-object p0

    .line 1383866
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;

    if-eqz v0, :cond_1

    .line 1383867
    check-cast p0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;

    goto :goto_0

    .line 1383868
    :cond_1
    new-instance v0, LX/8eq;

    invoke-direct {v0}, LX/8eq;-><init>()V

    .line 1383869
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;->a()Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;->a(Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;)Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object v1

    iput-object v1, v0, LX/8eq;->a:Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    .line 1383870
    invoke-virtual {v0}, LX/8eq;->a()Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;

    move-result-object p0

    goto :goto_0
.end method

.method private j()Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383861
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;->e:Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;->e:Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    .line 1383862
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;->e:Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1383878
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1383879
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;->j()Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1383880
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1383881
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1383882
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1383883
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1383853
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1383854
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;->j()Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1383855
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;->j()Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    .line 1383856
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;->j()Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1383857
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;

    .line 1383858
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;->e:Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    .line 1383859
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1383860
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383852
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;->j()Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1383849
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;-><init>()V

    .line 1383850
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1383851
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1383848
    const v0, 0x31f429b1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1383847
    const v0, -0x252d039

    return v0
.end method
