.class public final Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7d9aa728
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1383633
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1383632
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1383630
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1383631
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1383627
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1383628
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1383629
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1383601
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1383602
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1383603
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1383604
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1383605
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1383606
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1383607
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1383608
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1383609
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1383610
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1383614
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1383615
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1383616
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    .line 1383617
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1383618
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    .line 1383619
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->e:Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    .line 1383620
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1383621
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1383622
    if-eqz v2, :cond_1

    .line 1383623
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    .line 1383624
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 1383625
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1383626
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383634
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->e:Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->e:Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    .line 1383635
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->e:Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1383611
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;-><init>()V

    .line 1383612
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1383613
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1383600
    const v0, -0x14053f3d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1383599
    const v0, -0x1bce060e

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1383597
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->f:Ljava/util/List;

    .line 1383598
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383595
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->g:Ljava/lang/String;

    .line 1383596
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->g:Ljava/lang/String;

    return-object v0
.end method
