.class public final Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x50a312db
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1383839
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1383838
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1383836
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1383837
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1383833
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1383834
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1383835
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;)Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;
    .locals 2

    .prologue
    .line 1383825
    if-nez p0, :cond_0

    .line 1383826
    const/4 p0, 0x0

    .line 1383827
    :goto_0
    return-object p0

    .line 1383828
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    if-eqz v0, :cond_1

    .line 1383829
    check-cast p0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    goto :goto_0

    .line 1383830
    :cond_1
    new-instance v0, LX/8er;

    invoke-direct {v0}, LX/8er;-><init>()V

    .line 1383831
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8er;->a:Ljava/lang/String;

    .line 1383832
    invoke-virtual {v0}, LX/8er;->a()Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1383819
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1383820
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1383821
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1383822
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1383823
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1383824
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1383816
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1383817
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1383818
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383814
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;->e:Ljava/lang/String;

    .line 1383815
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1383809
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;-><init>()V

    .line 1383810
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1383811
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1383813
    const v0, -0x28e77936

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1383812
    const v0, -0x726d476c

    return v0
.end method
