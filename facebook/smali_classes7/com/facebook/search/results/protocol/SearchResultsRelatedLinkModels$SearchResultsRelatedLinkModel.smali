.class public final Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/8dL;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x79bba4b0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$AllShareStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SourceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1385371
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1385412
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1385413
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1385414
    return-void
.end method

.method private j()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$AllShareStoriesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385415
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->e:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$AllShareStoriesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$AllShareStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$AllShareStoriesModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->e:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$AllShareStoriesModel;

    .line 1385416
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->e:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$AllShareStoriesModel;

    return-object v0
.end method

.method private k()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385417
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->h:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->h:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    .line 1385418
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->h:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    return-object v0
.end method

.method private l()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385372
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->j:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->j:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    .line 1385373
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->j:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    return-object v0
.end method

.method private m()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385419
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->k:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->k:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    .line 1385420
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->k:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385421
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->l:Ljava/lang/String;

    .line 1385422
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method private o()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SourceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385423
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->m:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SourceModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SourceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SourceModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->m:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SourceModel;

    .line 1385424
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->m:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SourceModel;

    return-object v0
.end method

.method private p()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385425
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->n:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->n:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;

    .line 1385426
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->n:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;

    return-object v0
.end method

.method private q()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$TitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385427
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->o:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$TitleModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$TitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$TitleModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->o:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$TitleModel;

    .line 1385428
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->o:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$TitleModel;

    return-object v0
.end method


# virtual methods
.method public final O()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385429
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->g:Ljava/lang/String;

    .line 1385430
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final W()J
    .locals 2

    .prologue
    .line 1385431
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1385432
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->f:J

    return-wide v0
.end method

.method public final a(LX/186;)I
    .locals 18

    .prologue
    .line 1385433
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1385434
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->j()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$AllShareStoriesModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1385435
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->O()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1385436
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->k()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1385437
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->dW_()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1385438
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->l()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1385439
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->m()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1385440
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->n()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1385441
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->o()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SourceModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1385442
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->p()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1385443
    invoke-direct/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->q()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$TitleModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1385444
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->aV()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 1385445
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1385446
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 1385447
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->f:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1385448
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1385449
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1385450
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1385451
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1385452
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1385453
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1385454
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1385455
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1385456
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1385457
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1385458
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1385459
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1385374
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1385375
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->j()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$AllShareStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1385376
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->j()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$AllShareStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$AllShareStoriesModel;

    .line 1385377
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->j()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$AllShareStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1385378
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;

    .line 1385379
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->e:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$AllShareStoriesModel;

    .line 1385380
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->k()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1385381
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->k()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    .line 1385382
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->k()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1385383
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;

    .line 1385384
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->h:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    .line 1385385
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->l()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1385386
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->l()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    .line 1385387
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->l()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1385388
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;

    .line 1385389
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->j:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    .line 1385390
    :cond_2
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->m()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1385391
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->m()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    .line 1385392
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->m()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1385393
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;

    .line 1385394
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->k:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    .line 1385395
    :cond_3
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->o()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SourceModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1385396
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->o()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SourceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SourceModel;

    .line 1385397
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->o()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SourceModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1385398
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;

    .line 1385399
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->m:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SourceModel;

    .line 1385400
    :cond_4
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->p()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1385401
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->p()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;

    .line 1385402
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->p()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1385403
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;

    .line 1385404
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->n:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SummaryModel;

    .line 1385405
    :cond_5
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->q()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1385406
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->q()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$TitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$TitleModel;

    .line 1385407
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->q()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$TitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1385408
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;

    .line 1385409
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->o:Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$TitleModel;

    .line 1385410
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1385411
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    :cond_7
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385352
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->dW_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1385353
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1385354
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->f:J

    .line 1385355
    return-void
.end method

.method public final aV()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385356
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->p:Ljava/lang/String;

    .line 1385357
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic ad()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385358
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->k()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic an()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385359
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->l()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1385360
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;-><init>()V

    .line 1385361
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1385362
    return-object v0
.end method

.method public final synthetic bQ()LX/8dJ;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385363
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->q()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$TitleModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic bR()LX/8dI;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385364
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->o()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$SourceModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic bS()LX/A4E;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385365
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->m()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic bT()LX/8d4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385366
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->j()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$AllShareStoriesModel;

    move-result-object v0

    return-object v0
.end method

.method public final dW_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385367
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->i:Ljava/lang/String;

    .line 1385368
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1385369
    const v0, -0x42f8476e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1385370
    const v0, 0x1eaef984

    return v0
.end method
