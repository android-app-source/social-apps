.class public final Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7ea86245
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1375054
    const-class v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1375057
    const-class v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1375055
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1375056
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1375052
    iget-object v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;->e:Ljava/lang/String;

    .line 1375053
    iget-object v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1375044
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1375045
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1375046
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;->j()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1375047
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1375048
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1375049
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1375050
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1375051
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1375058
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1375059
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;->j()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1375060
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;->j()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;

    .line 1375061
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;->j()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1375062
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;

    .line 1375063
    iput-object v0, v1, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;->f:Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;

    .line 1375064
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1375065
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1375043
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1375040
    new-instance v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;-><init>()V

    .line 1375041
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1375042
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1375036
    const v0, 0x279a995e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1375039
    const v0, 0x3c68e4f

    return v0
.end method

.method public final j()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMedia"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1375037
    iget-object v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;->f:Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;->f:Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;

    .line 1375038
    iget-object v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel;->f:Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel;

    return-object v0
.end method
