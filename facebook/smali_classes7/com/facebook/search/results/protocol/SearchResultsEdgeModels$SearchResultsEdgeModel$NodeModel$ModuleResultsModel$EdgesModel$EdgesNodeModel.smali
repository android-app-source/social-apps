.class public final Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/8d6;
.implements LX/8d7;
.implements LX/A24;
.implements LX/A2F;
.implements LX/A2T;
.implements LX/8cw;
.implements LX/8d8;
.implements LX/8d9;
.implements LX/8dA;
.implements LX/8d0;
.implements LX/8d2;
.implements LX/8dB;
.implements LX/8d1;
.implements LX/A3P;
.implements LX/A3Q;
.implements LX/A3R;
.implements LX/A3T;
.implements LX/A3S;
.implements LX/A4u;
.implements LX/A52;
.implements LX/A5I;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x34ed79d5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$Serializer;
.end annotation


# instance fields
.field private A:Z

.field private B:J

.field private C:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Z

.field private F:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:D

.field private J:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private L:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:Z

.field private N:I

.field private O:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private P:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Q:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private R:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private S:I

.field private T:I

.field private U:I

.field private V:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private W:Z

.field private X:Z

.field private Y:Z

.field private Z:Z

.field private aA:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aB:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aC:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aD:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aE:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aF:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aG:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aH:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aI:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aJ:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aK:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aL:D

.field private aM:D

.field private aN:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aO:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aP:I

.field private aQ:J

.field private aR:Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aS:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aT:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aU:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aV:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aW:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aX:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aY:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aZ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aa:Z

.field private ab:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ac:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ad:I

.field private ae:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private af:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ag:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ah:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ai:D

.field private aj:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ak:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private al:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private am:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private an:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ao:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ap:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aq:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ar:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private as:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private at:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private au:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private av:I

.field private aw:I

.field private ax:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ay:D

.field private az:D

.field private ba:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bb:Z

.field private bc:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bd:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private be:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bf:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bg:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bh:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bi:Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bj:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bk:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bl:I

.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:J

.field private w:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:J

.field private y:Z

.field private z:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1378017
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1378018
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1378019
    const/16 v0, 0x70

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1378020
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1378021
    const/16 v0, 0x70

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1378022
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1378023
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1378024
    if-nez p0, :cond_0

    .line 1378025
    const/4 p0, 0x0

    .line 1378026
    :goto_0
    return-object p0

    .line 1378027
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    if-eqz v0, :cond_1

    .line 1378028
    check-cast p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    goto :goto_0

    .line 1378029
    :cond_1
    new-instance v2, LX/8dX;

    invoke-direct {v2}, LX/8dX;-><init>()V

    .line 1378030
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1378031
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->Q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->b:Ljava/lang/String;

    .line 1378032
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->A()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->c:Ljava/lang/String;

    .line 1378033
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->D()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->d:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    .line 1378034
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->R()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->e:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    .line 1378035
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    move v0, v1

    .line 1378036
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->c()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 1378037
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->c()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1378038
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1378039
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->f:LX/0Px;

    .line 1378040
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->S()Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;->a(Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;)Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->g:Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    .line 1378041
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->L()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->h:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    .line 1378042
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->T()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->a(Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;)Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->i:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    .line 1378043
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->U()Z

    move-result v0

    iput-boolean v0, v2, LX/8dX;->j:Z

    .line 1378044
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->X()Z

    move-result v0

    iput-boolean v0, v2, LX/8dX;->k:Z

    .line 1378045
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->u()Z

    move-result v0

    iput-boolean v0, v2, LX/8dX;->l:Z

    .line 1378046
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    move v0, v1

    .line 1378047
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->v()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 1378048
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->v()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1378049
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1378050
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->m:LX/0Px;

    .line 1378051
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->Y()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;->a(Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;)Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->n:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    .line 1378052
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->p()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->o:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1378053
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dV_()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->p:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1378054
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->b()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;->a(Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;)Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->q:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    .line 1378055
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->V()J

    move-result-wide v4

    iput-wide v4, v2, LX/8dX;->r:J

    .line 1378056
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->s:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1378057
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->W()J

    move-result-wide v4

    iput-wide v4, v2, LX/8dX;->t:J

    .line 1378058
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->w()Z

    move-result v0

    iput-boolean v0, v2, LX/8dX;->u:Z

    .line 1378059
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->Z()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;->a(Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;)Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->v:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    .line 1378060
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aa()Z

    move-result v0

    iput-boolean v0, v2, LX/8dX;->w:Z

    .line 1378061
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ab()J

    move-result-wide v4

    iput-wide v4, v2, LX/8dX;->x:J

    .line 1378062
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->j()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->y:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    .line 1378063
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ac()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->z:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    .line 1378064
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ad()Z

    move-result v0

    iput-boolean v0, v2, LX/8dX;->A:Z

    .line 1378065
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->O()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->B:Ljava/lang/String;

    .line 1378066
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ae()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->C:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    .line 1378067
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->af()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->D:Ljava/lang/String;

    .line 1378068
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ag()D

    move-result-wide v4

    iput-wide v4, v2, LX/8dX;->E:D

    .line 1378069
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->F:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1378070
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->q()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->G:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    .line 1378071
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ai()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;->a(Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;)Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->H:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 1378072
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ah()Z

    move-result v0

    iput-boolean v0, v2, LX/8dX;->I:Z

    .line 1378073
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aj()I

    move-result v0

    iput v0, v2, LX/8dX;->J:I

    .line 1378074
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ak()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->K:Ljava/lang/String;

    .line 1378075
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->L:Ljava/lang/String;

    .line 1378076
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->al()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->M:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    .line 1378077
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->C()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->N:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1378078
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->am()I

    move-result v0

    iput v0, v2, LX/8dX;->O:I

    .line 1378079
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->an()I

    move-result v0

    iput v0, v2, LX/8dX;->P:I

    .line 1378080
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ao()I

    move-result v0

    iput v0, v2, LX/8dX;->Q:I

    .line 1378081
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ar()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->R:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    .line 1378082
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->at()Z

    move-result v0

    iput-boolean v0, v2, LX/8dX;->S:Z

    .line 1378083
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ap()Z

    move-result v0

    iput-boolean v0, v2, LX/8dX;->T:Z

    .line 1378084
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aq()Z

    move-result v0

    iput-boolean v0, v2, LX/8dX;->U:Z

    .line 1378085
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aw()Z

    move-result v0

    iput-boolean v0, v2, LX/8dX;->V:Z

    .line 1378086
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->o()Z

    move-result v0

    iput-boolean v0, v2, LX/8dX;->W:Z

    .line 1378087
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->as()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;->a(Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;)Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->X:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    .line 1378088
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ax()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->Y:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    .line 1378089
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->au()I

    move-result v0

    iput v0, v2, LX/8dX;->Z:I

    .line 1378090
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ay()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aa:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    .line 1378091
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->av()LX/175;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a(LX/175;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->ab:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1378092
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->az()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->ac:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    .line 1378093
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->ad:Ljava/lang/String;

    .line 1378094
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aA()D

    move-result-wide v4

    iput-wide v4, v2, LX/8dX;->ae:D

    .line 1378095
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aD()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;->a(Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;)Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->af:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    .line 1378096
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->F()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->ag:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    .line 1378097
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aB()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;->a(Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;)Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->ah:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    .line 1378098
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->x()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->ai:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    .line 1378099
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->y()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aj:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    .line 1378100
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aI()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->ak:Ljava/lang/String;

    .line 1378101
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aC()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;->a(Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;)Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->al:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    .line 1378102
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aK()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->am:Ljava/lang/String;

    .line 1378103
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aM()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->an:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    .line 1378104
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aN()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;->a(Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;)Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->ao:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    .line 1378105
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->G()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->ap:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    .line 1378106
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->H()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aq:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 1378107
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aE()I

    move-result v0

    iput v0, v2, LX/8dX;->ar:I

    .line 1378108
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aF()I

    move-result v0

    iput v0, v2, LX/8dX;->as:I

    .line 1378109
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->I()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->at:Ljava/lang/String;

    .line 1378110
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aG()D

    move-result-wide v4

    iput-wide v4, v2, LX/8dX;->au:D

    .line 1378111
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aH()D

    move-result-wide v4

    iput-wide v4, v2, LX/8dX;->av:D

    .line 1378112
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aw:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1378113
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aO()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->ax:Ljava/lang/String;

    .line 1378114
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aP()LX/8dH;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->a(LX/8dH;)Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->ay:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    .line 1378115
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aQ()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;->a(Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;)Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->az:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    .line 1378116
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aJ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;->a(Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;)Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aA:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    .line 1378117
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aR()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->a(Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;)Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aB:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    .line 1378118
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aL()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->a(Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;)Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aC:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    .line 1378119
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    move v0, v1

    .line 1378120
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->J()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v0, v4, :cond_4

    .line 1378121
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->J()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1378122
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1378123
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aD:LX/0Px;

    .line 1378124
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aS()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aE:Ljava/lang/String;

    .line 1378125
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->r()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aF:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    .line 1378126
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aT()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aG:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    .line 1378127
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aU()D

    move-result-wide v4

    iput-wide v4, v2, LX/8dX;->aH:D

    .line 1378128
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aV()D

    move-result-wide v4

    iput-wide v4, v2, LX/8dX;->aI:D

    .line 1378129
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aW()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aJ:Ljava/lang/String;

    .line 1378130
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aY()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aK:Ljava/lang/String;

    .line 1378131
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aZ()I

    move-result v0

    iput v0, v2, LX/8dX;->aL:I

    .line 1378132
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ba()J

    move-result-wide v4

    iput-wide v4, v2, LX/8dX;->aM:J

    .line 1378133
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bb()Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;->a(Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;)Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aN:Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    .line 1378134
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bc()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aO:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    .line 1378135
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aP:Ljava/lang/String;

    .line 1378136
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bd()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aQ:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    .line 1378137
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->be()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;->a(Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;)Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aR:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    .line 1378138
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bf()LX/1Fb;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aS:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1378139
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bg()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;->a(Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;)Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aT:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    .line 1378140
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bh()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aU:Ljava/lang/String;

    .line 1378141
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bi()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aV:Ljava/lang/String;

    .line 1378142
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->z()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aW:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1378143
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bj()Z

    move-result v0

    iput-boolean v0, v2, LX/8dX;->aX:Z

    .line 1378144
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bk()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aY:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    .line 1378145
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->aZ:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1378146
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->ba:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1378147
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->K()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->bb:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1378148
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->bc:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1378149
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->t()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->bd:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    .line 1378150
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bl()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;->a(Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;)Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->be:Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    .line 1378151
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1378152
    :goto_4
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bm()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1378153
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bm()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;

    invoke-static {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;->a(Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;)Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1378154
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1378155
    :cond_5
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->bf:LX/0Px;

    .line 1378156
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bn()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8dX;->bg:Ljava/lang/String;

    .line 1378157
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bo()I

    move-result v0

    iput v0, v2, LX/8dX;->bh:I

    .line 1378158
    invoke-virtual {v2}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object p0

    goto/16 :goto_0
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378159
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->g:Ljava/lang/String;

    .line 1378160
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic B()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377462
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bC()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic C()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378161
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bD()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic D()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378162
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bp()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic E()LX/8d5;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378163
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bH()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic F()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378164
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bL()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic G()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378165
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bU()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    move-result-object v0

    return-object v0
.end method

.method public final H()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378013
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->au:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    const/16 v1, 0x44

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->au:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 1378014
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->au:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    return-object v0
.end method

.method public final I()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378166
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ax:Ljava/lang/String;

    const/16 v1, 0x47

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ax:Ljava/lang/String;

    .line 1378167
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ax:Ljava/lang/String;

    return-object v0
.end method

.method public final J()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1378168
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aH:Ljava/util/List;

    const/16 v1, 0x51

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aH:Ljava/util/List;

    .line 1378169
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aH:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final K()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378170
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bf:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x69

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bf:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1378171
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bf:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method

.method public final synthetic L()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378172
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bs()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v0

    return-object v0
.end method

.method public final M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378173
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->J:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->J:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1378174
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->J:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final N()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378175
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->w:Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->w:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1378176
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->w:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final O()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378177
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->F:Ljava/lang/String;

    const/16 v1, 0x1b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->F:Ljava/lang/String;

    .line 1378178
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->F:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic P()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378179
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bG()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v0

    return-object v0
.end method

.method public final Q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378180
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->f:Ljava/lang/String;

    .line 1378181
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic R()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378182
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bq()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic S()Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378183
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->br()Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic T()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377994
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bt()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final U()Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1377502
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1377503
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->n:Z

    return v0
.end method

.method public final V()J
    .locals 2

    .prologue
    .line 1377504
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1377505
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->v:J

    return-wide v0
.end method

.method public final W()J
    .locals 2

    .prologue
    .line 1377506
    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1377507
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->x:J

    return-wide v0
.end method

.method public final X()Z
    .locals 2

    .prologue
    .line 1377508
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1377509
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->o:Z

    return v0
.end method

.method public final synthetic Y()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377510
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bu()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic Z()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377511
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bw()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 83

    .prologue
    .line 1377512
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1377513
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1377514
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->Q()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1377515
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->A()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1377516
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bp()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1377517
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bq()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1377518
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->c()LX/0Px;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/util/List;)I

    move-result v7

    .line 1377519
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->br()Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1377520
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bs()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1377521
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bt()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1377522
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->v()LX/0Px;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/util/List;)I

    move-result v11

    .line 1377523
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bu()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1377524
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->p()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    .line 1377525
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dV_()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v14

    .line 1377526
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bv()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1377527
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1377528
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bw()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1377529
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bx()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1377530
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->by()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1377531
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->O()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    .line 1377532
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bz()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1377533
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->af()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 1377534
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v23

    .line 1377535
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bA()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 1377536
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bB()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 1377537
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ak()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    .line 1377538
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    .line 1377539
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bC()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 1377540
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bD()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 1377541
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bE()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 1377542
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bF()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 1377543
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bG()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 1377544
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bH()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 1377545
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bI()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v34

    .line 1377546
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bJ()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 1377547
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->d()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v36

    .line 1377548
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bK()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 1377549
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bL()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v38

    .line 1377550
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bM()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 1377551
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bN()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v40

    .line 1377552
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bO()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 1377553
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aI()Ljava/lang/String;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v42

    .line 1377554
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bQ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 1377555
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aK()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v44

    .line 1377556
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bR()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 1377557
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bT()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v46

    .line 1377558
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bU()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    move-result-object v47

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v47

    .line 1377559
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->H()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v48

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v48

    .line 1377560
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->I()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v49

    .line 1377561
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bV()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v50

    move-object/from16 v0, p1

    move-object/from16 v1, v50

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v50

    .line 1377562
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aO()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v51

    .line 1377563
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bW()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-result-object v52

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v52

    .line 1377564
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bX()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    move-result-object v53

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v53

    .line 1377565
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cc()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v54

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v54

    .line 1377566
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cd()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-result-object v55

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v55

    .line 1377567
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ce()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-result-object v56

    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v56

    .line 1377568
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->J()LX/0Px;

    move-result-object v57

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v57

    .line 1377569
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aS()Ljava/lang/String;

    move-result-object v58

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v58

    .line 1377570
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cf()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v59

    move-object/from16 v0, p1

    move-object/from16 v1, v59

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v59

    .line 1377571
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cg()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v60

    move-object/from16 v0, p1

    move-object/from16 v1, v60

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v60

    .line 1377572
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aW()Ljava/lang/String;

    move-result-object v61

    move-object/from16 v0, p1

    move-object/from16 v1, v61

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v61

    .line 1377573
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aY()Ljava/lang/String;

    move-result-object v62

    move-object/from16 v0, p1

    move-object/from16 v1, v62

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v62

    .line 1377574
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ch()Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    move-result-object v63

    move-object/from16 v0, p1

    move-object/from16 v1, v63

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v63

    .line 1377575
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ci()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    move-result-object v64

    move-object/from16 v0, p1

    move-object/from16 v1, v64

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v64

    .line 1377576
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->k()Ljava/lang/String;

    move-result-object v65

    move-object/from16 v0, p1

    move-object/from16 v1, v65

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v65

    .line 1377577
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cj()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-result-object v66

    move-object/from16 v0, p1

    move-object/from16 v1, v66

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v66

    .line 1377578
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ck()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v67

    move-object/from16 v0, p1

    move-object/from16 v1, v67

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v67

    .line 1377579
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cl()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v68

    move-object/from16 v0, p1

    move-object/from16 v1, v68

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v68

    .line 1377580
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cm()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v69

    move-object/from16 v0, p1

    move-object/from16 v1, v69

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v69

    .line 1377581
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bh()Ljava/lang/String;

    move-result-object v70

    move-object/from16 v0, p1

    move-object/from16 v1, v70

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v70

    .line 1377582
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bi()Ljava/lang/String;

    move-result-object v71

    move-object/from16 v0, p1

    move-object/from16 v1, v71

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v71

    .line 1377583
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->z()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v72

    move-object/from16 v0, p1

    move-object/from16 v1, v72

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v72

    .line 1377584
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cn()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    move-result-object v73

    move-object/from16 v0, p1

    move-object/from16 v1, v73

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v73

    .line 1377585
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v74

    move-object/from16 v0, p1

    move-object/from16 v1, v74

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v74

    .line 1377586
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v75

    move-object/from16 v0, p1

    move-object/from16 v1, v75

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v75

    .line 1377587
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->K()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v76

    move-object/from16 v0, p1

    move-object/from16 v1, v76

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v76

    .line 1377588
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v77

    move-object/from16 v0, p1

    move-object/from16 v1, v77

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v77

    .line 1377589
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->co()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v78

    move-object/from16 v0, p1

    move-object/from16 v1, v78

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v78

    .line 1377590
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cp()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    move-result-object v79

    move-object/from16 v0, p1

    move-object/from16 v1, v79

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v79

    .line 1377591
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bm()LX/0Px;

    move-result-object v80

    move-object/from16 v0, p1

    move-object/from16 v1, v80

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v80

    .line 1377592
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bn()Ljava/lang/String;

    move-result-object v81

    move-object/from16 v0, p1

    move-object/from16 v1, v81

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v81

    .line 1377593
    const/16 v82, 0x70

    move-object/from16 v0, p1

    move/from16 v1, v82

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1377594
    const/16 v82, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v82

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1377595
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1377596
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1377597
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1377598
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1377599
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1377600
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1377601
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1377602
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1377603
    const/16 v2, 0x9

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->n:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1377604
    const/16 v2, 0xa

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->o:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1377605
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1377606
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1377607
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1377608
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1377609
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1377610
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1377611
    const/16 v3, 0x11

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->v:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1377612
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377613
    const/16 v3, 0x13

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->x:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1377614
    const/16 v2, 0x14

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->y:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1377615
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377616
    const/16 v2, 0x16

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->A:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1377617
    const/16 v3, 0x17

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->B:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1377618
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377619
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377620
    const/16 v2, 0x1a

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->E:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1377621
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377622
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377623
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377624
    const/16 v3, 0x1e

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->I:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1377625
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377626
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377627
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377628
    const/16 v2, 0x22

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->M:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1377629
    const/16 v2, 0x23

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->N:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1377630
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377631
    const/16 v2, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377632
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377633
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377634
    const/16 v2, 0x28

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->S:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1377635
    const/16 v2, 0x29

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->T:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1377636
    const/16 v2, 0x2a

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->U:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1377637
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377638
    const/16 v2, 0x2c

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->W:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1377639
    const/16 v2, 0x2d

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->X:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1377640
    const/16 v2, 0x2e

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->Y:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1377641
    const/16 v2, 0x2f

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->Z:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1377642
    const/16 v2, 0x30

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aa:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1377643
    const/16 v2, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377644
    const/16 v2, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377645
    const/16 v2, 0x33

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ad:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1377646
    const/16 v2, 0x34

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377647
    const/16 v2, 0x35

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377648
    const/16 v2, 0x36

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377649
    const/16 v2, 0x37

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377650
    const/16 v3, 0x38

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ai:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1377651
    const/16 v2, 0x39

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377652
    const/16 v2, 0x3a

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377653
    const/16 v2, 0x3b

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377654
    const/16 v2, 0x3c

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377655
    const/16 v2, 0x3d

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377656
    const/16 v2, 0x3e

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377657
    const/16 v2, 0x3f

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377658
    const/16 v2, 0x40

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377659
    const/16 v2, 0x41

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377660
    const/16 v2, 0x42

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377661
    const/16 v2, 0x43

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377662
    const/16 v2, 0x44

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377663
    const/16 v2, 0x45

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->av:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1377664
    const/16 v2, 0x46

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aw:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1377665
    const/16 v2, 0x47

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377666
    const/16 v3, 0x48

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ay:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1377667
    const/16 v3, 0x49

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->az:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1377668
    const/16 v2, 0x4a

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377669
    const/16 v2, 0x4b

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377670
    const/16 v2, 0x4c

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377671
    const/16 v2, 0x4d

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377672
    const/16 v2, 0x4e

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377673
    const/16 v2, 0x4f

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377674
    const/16 v2, 0x50

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377675
    const/16 v2, 0x51

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377676
    const/16 v2, 0x52

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377677
    const/16 v2, 0x53

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377678
    const/16 v2, 0x54

    move-object/from16 v0, p1

    move/from16 v1, v60

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377679
    const/16 v3, 0x55

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aL:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1377680
    const/16 v3, 0x56

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aM:D

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1377681
    const/16 v2, 0x57

    move-object/from16 v0, p1

    move/from16 v1, v61

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377682
    const/16 v2, 0x58

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377683
    const/16 v2, 0x59

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aP:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1377684
    const/16 v3, 0x5a

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aQ:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1377685
    const/16 v2, 0x5b

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377686
    const/16 v2, 0x5c

    move-object/from16 v0, p1

    move/from16 v1, v64

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377687
    const/16 v2, 0x5d

    move-object/from16 v0, p1

    move/from16 v1, v65

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377688
    const/16 v2, 0x5e

    move-object/from16 v0, p1

    move/from16 v1, v66

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377689
    const/16 v2, 0x5f

    move-object/from16 v0, p1

    move/from16 v1, v67

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377690
    const/16 v2, 0x60

    move-object/from16 v0, p1

    move/from16 v1, v68

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377691
    const/16 v2, 0x61

    move-object/from16 v0, p1

    move/from16 v1, v69

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377692
    const/16 v2, 0x62

    move-object/from16 v0, p1

    move/from16 v1, v70

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377693
    const/16 v2, 0x63

    move-object/from16 v0, p1

    move/from16 v1, v71

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377694
    const/16 v2, 0x64

    move-object/from16 v0, p1

    move/from16 v1, v72

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377695
    const/16 v2, 0x65

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bb:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1377696
    const/16 v2, 0x66

    move-object/from16 v0, p1

    move/from16 v1, v73

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377697
    const/16 v2, 0x67

    move-object/from16 v0, p1

    move/from16 v1, v74

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377698
    const/16 v2, 0x68

    move-object/from16 v0, p1

    move/from16 v1, v75

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377699
    const/16 v2, 0x69

    move-object/from16 v0, p1

    move/from16 v1, v76

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377700
    const/16 v2, 0x6a

    move-object/from16 v0, p1

    move/from16 v1, v77

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377701
    const/16 v2, 0x6b

    move-object/from16 v0, p1

    move/from16 v1, v78

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377702
    const/16 v2, 0x6c

    move-object/from16 v0, p1

    move/from16 v1, v79

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377703
    const/16 v2, 0x6d

    move-object/from16 v0, p1

    move/from16 v1, v80

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377704
    const/16 v2, 0x6e

    move-object/from16 v0, p1

    move/from16 v1, v81

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1377705
    const/16 v2, 0x6f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bl:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1377706
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1377707
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1377708
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1377709
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bp()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1377710
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bp()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    .line 1377711
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bp()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1377712
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377713
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->h:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    .line 1377714
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bq()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1377715
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bq()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    .line 1377716
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bq()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1377717
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377718
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->i:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    .line 1377719
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->br()Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1377720
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->br()Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    .line 1377721
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->br()Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1377722
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377723
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->k:Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    .line 1377724
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bs()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1377725
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bs()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    .line 1377726
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bs()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1377727
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377728
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->l:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    .line 1377729
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bt()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1377730
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bt()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    .line 1377731
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bt()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1377732
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377733
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->m:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    .line 1377734
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bu()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1377735
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bu()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    .line 1377736
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bu()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1377737
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377738
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->r:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    .line 1377739
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bv()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1377740
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bv()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    .line 1377741
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bv()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1377742
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377743
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->u:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    .line 1377744
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1377745
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1377746
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1377747
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377748
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->w:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1377749
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bw()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1377750
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bw()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    .line 1377751
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bw()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1377752
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377753
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->z:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    .line 1377754
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bx()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1377755
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bx()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    .line 1377756
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bx()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 1377757
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377758
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->C:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    .line 1377759
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->by()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1377760
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->by()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    .line 1377761
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->by()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 1377762
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377763
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->D:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    .line 1377764
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bz()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1377765
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bz()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    .line 1377766
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bz()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 1377767
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377768
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->G:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    .line 1377769
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bA()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 1377770
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bA()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    .line 1377771
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bA()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 1377772
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377773
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->K:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    .line 1377774
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bB()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 1377775
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bB()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 1377776
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bB()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 1377777
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377778
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->L:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 1377779
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bC()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 1377780
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bC()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    .line 1377781
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bC()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 1377782
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377783
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->Q:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    .line 1377784
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bD()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 1377785
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bD()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1377786
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bD()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 1377787
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377788
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->R:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1377789
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bE()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 1377790
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bE()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    .line 1377791
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bE()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 1377792
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377793
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->V:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    .line 1377794
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bF()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 1377795
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bF()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    .line 1377796
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bF()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 1377797
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377798
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ab:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    .line 1377799
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bG()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 1377800
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bG()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    .line 1377801
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bG()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 1377802
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377803
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ac:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    .line 1377804
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bH()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 1377805
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bH()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    .line 1377806
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bH()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 1377807
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377808
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ae:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    .line 1377809
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bI()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 1377810
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bI()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1377811
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bI()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 1377812
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377813
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->af:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1377814
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bJ()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 1377815
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bJ()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    .line 1377816
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bJ()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 1377817
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377818
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ag:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    .line 1377819
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bK()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 1377820
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bK()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    .line 1377821
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bK()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_16

    .line 1377822
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377823
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aj:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    .line 1377824
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bL()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 1377825
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bL()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    .line 1377826
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bL()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    move-result-object v2

    if-eq v2, v0, :cond_17

    .line 1377827
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377828
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ak:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    .line 1377829
    :cond_17
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bM()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 1377830
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bM()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    .line 1377831
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bM()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_18

    .line 1377832
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377833
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->al:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    .line 1377834
    :cond_18
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bN()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 1377835
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bN()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    .line 1377836
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bN()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 1377837
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377838
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->am:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    .line 1377839
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bO()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 1377840
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bO()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    .line 1377841
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bO()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_1a

    .line 1377842
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377843
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->an:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    .line 1377844
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bQ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 1377845
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bQ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    .line 1377846
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bQ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_1b

    .line 1377847
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377848
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ap:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    .line 1377849
    :cond_1b
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bR()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 1377850
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bR()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    .line 1377851
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bR()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_1c

    .line 1377852
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377853
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ar:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    .line 1377854
    :cond_1c
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bT()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 1377855
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bT()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    .line 1377856
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bT()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    move-result-object v2

    if-eq v2, v0, :cond_1d

    .line 1377857
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377858
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->as:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    .line 1377859
    :cond_1d
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bU()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 1377860
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bU()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    .line 1377861
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bU()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    move-result-object v2

    if-eq v2, v0, :cond_1e

    .line 1377862
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377863
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->at:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    .line 1377864
    :cond_1e
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bV()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 1377865
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bV()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1377866
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bV()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1f

    .line 1377867
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377868
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aA:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1377869
    :cond_1f
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bW()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-result-object v0

    if-eqz v0, :cond_20

    .line 1377870
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bW()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    .line 1377871
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bW()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-result-object v2

    if-eq v2, v0, :cond_20

    .line 1377872
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377873
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aC:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    .line 1377874
    :cond_20
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bX()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 1377875
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bX()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    .line 1377876
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bX()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    move-result-object v2

    if-eq v2, v0, :cond_21

    .line 1377877
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377878
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aD:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    .line 1377879
    :cond_21
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cc()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 1377880
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cc()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    .line 1377881
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cc()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v2

    if-eq v2, v0, :cond_22

    .line 1377882
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377883
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aE:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    .line 1377884
    :cond_22
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cd()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-result-object v0

    if-eqz v0, :cond_23

    .line 1377885
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cd()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    .line 1377886
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cd()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_23

    .line 1377887
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377888
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aF:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    .line 1377889
    :cond_23
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ce()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 1377890
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ce()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    .line 1377891
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ce()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-result-object v2

    if-eq v2, v0, :cond_24

    .line 1377892
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377893
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aG:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    .line 1377894
    :cond_24
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cf()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 1377895
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cf()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    .line 1377896
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cf()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v2

    if-eq v2, v0, :cond_25

    .line 1377897
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377898
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aJ:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    .line 1377899
    :cond_25
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cg()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 1377900
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cg()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    .line 1377901
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cg()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v2

    if-eq v2, v0, :cond_26

    .line 1377902
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377903
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aK:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    .line 1377904
    :cond_26
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ch()Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 1377905
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ch()Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    .line 1377906
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ch()Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_27

    .line 1377907
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377908
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aR:Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    .line 1377909
    :cond_27
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ci()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 1377910
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ci()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    .line 1377911
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ci()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    move-result-object v2

    if-eq v2, v0, :cond_28

    .line 1377912
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377913
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aS:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    .line 1377914
    :cond_28
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cj()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 1377915
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cj()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    .line 1377916
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cj()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_29

    .line 1377917
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377918
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aU:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    .line 1377919
    :cond_29
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ck()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 1377920
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ck()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    .line 1377921
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ck()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_2a

    .line 1377922
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377923
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aV:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    .line 1377924
    :cond_2a
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cl()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 1377925
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cl()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1377926
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cl()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2b

    .line 1377927
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377928
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aW:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1377929
    :cond_2b
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cm()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 1377930
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cm()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    .line 1377931
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cm()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_2c

    .line 1377932
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377933
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aX:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    .line 1377934
    :cond_2c
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cn()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    move-result-object v0

    if-eqz v0, :cond_2d

    .line 1377935
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cn()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    .line 1377936
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cn()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    move-result-object v2

    if-eq v2, v0, :cond_2d

    .line 1377937
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377938
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bc:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    .line 1377939
    :cond_2d
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->co()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 1377940
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->co()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    .line 1377941
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->co()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v2

    if-eq v2, v0, :cond_2e

    .line 1377942
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377943
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bh:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    .line 1377944
    :cond_2e
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cp()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    move-result-object v0

    if-eqz v0, :cond_2f

    .line 1377945
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cp()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    .line 1377946
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cp()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    move-result-object v2

    if-eq v2, v0, :cond_2f

    .line 1377947
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377948
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bi:Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    .line 1377949
    :cond_2f
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bm()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 1377950
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bm()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1377951
    if-eqz v2, :cond_30

    .line 1377952
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 1377953
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bj:Ljava/util/List;

    move-object v1, v0

    .line 1377954
    :cond_30
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1377955
    if-nez v1, :cond_31

    :goto_0
    return-object p0

    :cond_31
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1377956
    new-instance v0, LX/8dj;

    invoke-direct {v0, p1}, LX/8dj;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377957
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 1377958
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1377959
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->n:Z

    .line 1377960
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->o:Z

    .line 1377961
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->p:Z

    .line 1377962
    const/16 v0, 0x11

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->v:J

    .line 1377963
    const/16 v0, 0x13

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->x:J

    .line 1377964
    const/16 v0, 0x14

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->y:Z

    .line 1377965
    const/16 v0, 0x16

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->A:Z

    .line 1377966
    const/16 v0, 0x17

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->B:J

    .line 1377967
    const/16 v0, 0x1a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->E:Z

    .line 1377968
    const/16 v0, 0x1e

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->I:D

    .line 1377969
    const/16 v0, 0x22

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->M:Z

    .line 1377970
    const/16 v0, 0x23

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->N:I

    .line 1377971
    const/16 v0, 0x28

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->S:I

    .line 1377972
    const/16 v0, 0x29

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->T:I

    .line 1377973
    const/16 v0, 0x2a

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->U:I

    .line 1377974
    const/16 v0, 0x2c

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->W:Z

    .line 1377975
    const/16 v0, 0x2d

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->X:Z

    .line 1377976
    const/16 v0, 0x2e

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->Y:Z

    .line 1377977
    const/16 v0, 0x2f

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->Z:Z

    .line 1377978
    const/16 v0, 0x30

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aa:Z

    .line 1377979
    const/16 v0, 0x33

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ad:I

    .line 1377980
    const/16 v0, 0x38

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ai:D

    .line 1377981
    const/16 v0, 0x45

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->av:I

    .line 1377982
    const/16 v0, 0x46

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aw:I

    .line 1377983
    const/16 v0, 0x48

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ay:D

    .line 1377984
    const/16 v0, 0x49

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->az:D

    .line 1377985
    const/16 v0, 0x55

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aL:D

    .line 1377986
    const/16 v0, 0x56

    invoke-virtual {p1, p2, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aM:D

    .line 1377987
    const/16 v0, 0x59

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aP:I

    .line 1377988
    const/16 v0, 0x5a

    invoke-virtual {p1, p2, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aQ:J

    .line 1377989
    const/16 v0, 0x65

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bb:Z

    .line 1377990
    const/16 v0, 0x6f

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bl:I

    .line 1377991
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1377992
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1377993
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1377501
    return-void
.end method

.method public final aA()D
    .locals 2

    .prologue
    .line 1377995
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1377996
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ai:D

    return-wide v0
.end method

.method public final synthetic aB()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377997
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bM()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aC()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377998
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bQ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aD()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377999
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bK()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    move-result-object v0

    return-object v0
.end method

.method public final aE()I
    .locals 2

    .prologue
    .line 1378000
    const/16 v0, 0x8

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378001
    iget v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->av:I

    return v0
.end method

.method public final aF()I
    .locals 2

    .prologue
    .line 1378002
    const/16 v0, 0x8

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378003
    iget v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aw:I

    return v0
.end method

.method public final aG()D
    .locals 2

    .prologue
    .line 1378004
    const/16 v0, 0x9

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378005
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ay:D

    return-wide v0
.end method

.method public final aH()D
    .locals 2

    .prologue
    .line 1378006
    const/16 v0, 0x9

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378007
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->az:D

    return-wide v0
.end method

.method public final aI()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378008
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ao:Ljava/lang/String;

    const/16 v1, 0x3e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ao:Ljava/lang/String;

    .line 1378009
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ao:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic aJ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378010
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cc()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v0

    return-object v0
.end method

.method public final aK()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378011
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aq:Ljava/lang/String;

    const/16 v1, 0x40

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aq:Ljava/lang/String;

    .line 1378012
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aq:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic aL()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378227
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ce()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aM()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378206
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bR()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aN()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378228
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bT()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    move-result-object v0

    return-object v0
.end method

.method public final aO()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378229
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aB:Ljava/lang/String;

    const/16 v1, 0x4b

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aB:Ljava/lang/String;

    .line 1378230
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aB:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic aP()LX/8dH;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378231
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bW()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aQ()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378232
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bX()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aR()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378233
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cd()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-result-object v0

    return-object v0
.end method

.method public final aS()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378234
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aI:Ljava/lang/String;

    const/16 v1, 0x52

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aI:Ljava/lang/String;

    .line 1378235
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aI:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic aT()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378236
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cg()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v0

    return-object v0
.end method

.method public final aU()D
    .locals 2

    .prologue
    .line 1378237
    const/16 v0, 0xa

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378238
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aL:D

    return-wide v0
.end method

.method public final aV()D
    .locals 2

    .prologue
    .line 1378239
    const/16 v0, 0xa

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378240
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aM:D

    return-wide v0
.end method

.method public final aW()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378241
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aN:Ljava/lang/String;

    const/16 v1, 0x57

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aN:Ljava/lang/String;

    .line 1378242
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aN:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic aX()LX/A57;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378263
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cn()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    move-result-object v0

    return-object v0
.end method

.method public final aY()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378245
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aO:Ljava/lang/String;

    const/16 v1, 0x58

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aO:Ljava/lang/String;

    .line 1378246
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aO:Ljava/lang/String;

    return-object v0
.end method

.method public final aZ()I
    .locals 2

    .prologue
    .line 1378247
    const/16 v0, 0xb

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378248
    iget v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aP:I

    return v0
.end method

.method public final aa()Z
    .locals 2

    .prologue
    .line 1378249
    const/4 v0, 0x2

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378250
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->A:Z

    return v0
.end method

.method public final ab()J
    .locals 2

    .prologue
    .line 1378251
    const/4 v0, 0x2

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378252
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->B:J

    return-wide v0
.end method

.method public final synthetic ac()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378253
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->by()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final ad()Z
    .locals 2

    .prologue
    .line 1378254
    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378255
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->E:Z

    return v0
.end method

.method public final synthetic ae()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378256
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bz()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v0

    return-object v0
.end method

.method public final af()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378257
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->H:Ljava/lang/String;

    const/16 v1, 0x1d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->H:Ljava/lang/String;

    .line 1378258
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final ag()D
    .locals 2

    .prologue
    .line 1378259
    const/4 v0, 0x3

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378260
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->I:D

    return-wide v0
.end method

.method public final ah()Z
    .locals 2

    .prologue
    .line 1378261
    const/4 v0, 0x4

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378262
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->M:Z

    return v0
.end method

.method public final synthetic ai()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378226
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bB()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v0

    return-object v0
.end method

.method public final aj()I
    .locals 2

    .prologue
    .line 1378243
    const/4 v0, 0x4

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378244
    iget v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->N:I

    return v0
.end method

.method public final ak()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378186
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->O:Ljava/lang/String;

    const/16 v1, 0x24

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->O:Ljava/lang/String;

    .line 1378187
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->O:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic al()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378188
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bC()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final am()I
    .locals 2

    .prologue
    .line 1378189
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378190
    iget v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->S:I

    return v0
.end method

.method public final an()I
    .locals 2

    .prologue
    .line 1378191
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378192
    iget v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->T:I

    return v0
.end method

.method public final ao()I
    .locals 2

    .prologue
    .line 1378193
    const/4 v0, 0x5

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378194
    iget v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->U:I

    return v0
.end method

.method public final ap()Z
    .locals 1

    .prologue
    const/4 v0, 0x5

    .line 1378195
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378196
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->X:Z

    return v0
.end method

.method public final aq()Z
    .locals 2

    .prologue
    .line 1378197
    const/4 v0, 0x5

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378198
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->Y:Z

    return v0
.end method

.method public final synthetic ar()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378199
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bE()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic as()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378200
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bF()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-result-object v0

    return-object v0
.end method

.method public final at()Z
    .locals 2

    .prologue
    .line 1378201
    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378202
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->W:Z

    return v0
.end method

.method public final au()I
    .locals 2

    .prologue
    .line 1378203
    const/4 v0, 0x6

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378204
    iget v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ad:I

    return v0
.end method

.method public final synthetic av()LX/175;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378205
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bI()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final aw()Z
    .locals 2

    .prologue
    .line 1378184
    const/4 v0, 0x5

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1378185
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->Z:Z

    return v0
.end method

.method public final synthetic ax()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378207
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bG()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ay()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378208
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bH()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic az()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378209
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bJ()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1378210
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;-><init>()V

    .line 1378211
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1378212
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378213
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bv()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v0

    return-object v0
.end method

.method public final bA()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378214
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->K:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    const/16 v1, 0x20

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->K:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    .line 1378215
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->K:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    return-object v0
.end method

.method public final bB()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378216
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->L:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->L:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 1378217
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->L:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    return-object v0
.end method

.method public final bC()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378218
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->Q:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->Q:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    .line 1378219
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->Q:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    return-object v0
.end method

.method public final bD()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378220
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->R:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->R:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1378221
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->R:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final bE()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378222
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->V:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->V:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    .line 1378223
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->V:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    return-object v0
.end method

.method public final bF()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378224
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ab:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    const/16 v1, 0x31

    const-class v2, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ab:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    .line 1378225
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ab:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    return-object v0
.end method

.method public final bG()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1378015
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ac:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    const/16 v1, 0x32

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ac:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    .line 1378016
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ac:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    return-object v0
.end method

.method public final bH()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377396
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ae:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    const/16 v1, 0x34

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ae:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    .line 1377397
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ae:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    return-object v0
.end method

.method public final bI()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377384
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->af:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/16 v1, 0x35

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->af:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1377385
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->af:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method public final bJ()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377386
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ag:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    const/16 v1, 0x36

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ag:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    .line 1377387
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ag:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    return-object v0
.end method

.method public final bK()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377388
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aj:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    const/16 v1, 0x39

    const-class v2, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aj:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    .line 1377389
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aj:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$OpenGraphNodeModel;

    return-object v0
.end method

.method public final bL()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377390
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ak:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    const/16 v1, 0x3a

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ak:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    .line 1377391
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ak:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$OverallStarRatingModel;

    return-object v0
.end method

.method public final bM()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377392
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->al:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    const/16 v1, 0x3b

    const-class v2, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->al:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    .line 1377393
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->al:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    return-object v0
.end method

.method public final bN()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377394
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->am:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    const/16 v1, 0x3c

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->am:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    .line 1377395
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->am:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    return-object v0
.end method

.method public final bO()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377398
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->an:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    const/16 v1, 0x3d

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->an:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    .line 1377399
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->an:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    return-object v0
.end method

.method public final synthetic bP()LX/A2S;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377400
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bC()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final bQ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377401
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ap:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    const/16 v1, 0x3f

    const-class v2, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ap:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    .line 1377402
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ap:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    return-object v0
.end method

.method public final bR()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377403
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ar:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    const/16 v1, 0x41

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ar:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    .line 1377404
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ar:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    return-object v0
.end method

.method public final synthetic bS()LX/A4E;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377405
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bG()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v0

    return-object v0
.end method

.method public final bT()Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377382
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->as:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    const/16 v1, 0x42

    const-class v2, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->as:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    .line 1377383
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->as:Lcom/facebook/search/results/protocol/pulse/PulsePhrasesAnalysisExternalUrlModels$PulsePhrasesAnalysisExternalUrlModel$PhrasesAnalysisModel;

    return-object v0
.end method

.method public final bU()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377407
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->at:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    const/16 v1, 0x43

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->at:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    .line 1377408
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->at:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$PlaceOpenStatusModel;

    return-object v0
.end method

.method public final bV()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377409
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aA:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    const/16 v1, 0x4a

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aA:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 1377410
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aA:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    return-object v0
.end method

.method public final bW()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377411
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aC:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    const/16 v1, 0x4c

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aC:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    .line 1377412
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aC:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    return-object v0
.end method

.method public final bX()Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377413
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aD:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    const/16 v1, 0x4d

    const-class v2, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aD:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    .line 1377414
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aD:Lcom/facebook/search/results/protocol/pulse/PulseQuotesAnalysisExternalUrlModels$PulseQuotesAnalysisExternalUrlModel$QuotesAnalysisModel;

    return-object v0
.end method

.method public final synthetic bY()LX/175;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377415
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cj()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic bZ()LX/175;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377416
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cg()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v0

    return-object v0
.end method

.method public final ba()J
    .locals 2

    .prologue
    .line 1377417
    const/16 v0, 0xb

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1377418
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aQ:J

    return-wide v0
.end method

.method public final synthetic bb()Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377419
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ch()Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic bc()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377420
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ci()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic bd()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377421
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cj()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic be()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377422
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ck()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic bf()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377406
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cl()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic bg()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377342
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cm()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v0

    return-object v0
.end method

.method public final bh()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377357
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aY:Ljava/lang/String;

    const/16 v1, 0x62

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aY:Ljava/lang/String;

    .line 1377358
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aY:Ljava/lang/String;

    return-object v0
.end method

.method public final bi()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377355
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aZ:Ljava/lang/String;

    const/16 v1, 0x63

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aZ:Ljava/lang/String;

    .line 1377356
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aZ:Ljava/lang/String;

    return-object v0
.end method

.method public final bj()Z
    .locals 2

    .prologue
    .line 1377353
    const/16 v0, 0xc

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1377354
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bb:Z

    return v0
.end method

.method public final synthetic bk()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377352
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cn()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic bl()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377351
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cp()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    move-result-object v0

    return-object v0
.end method

.method public final bm()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1377349
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bj:Ljava/util/List;

    const/16 v1, 0x6d

    const-class v2, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bj:Ljava/util/List;

    .line 1377350
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bj:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final bn()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377347
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bk:Ljava/lang/String;

    const/16 v1, 0x6e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bk:Ljava/lang/String;

    .line 1377348
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bk:Ljava/lang/String;

    return-object v0
.end method

.method public final bo()I
    .locals 2

    .prologue
    .line 1377345
    const/16 v0, 0xd

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1377346
    iget v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bl:I

    return v0
.end method

.method public final bp()Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377343
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->h:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->h:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    .line 1377344
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->h:Lcom/facebook/search/results/protocol/entity/SearchResultsPlaceModels$SearchResultsPlaceModel$AddressModel;

    return-object v0
.end method

.method public final bq()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377340
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->i:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->i:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    .line 1377341
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->i:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    return-object v0
.end method

.method public final br()Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377360
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->k:Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->k:Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    .line 1377361
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->k:Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    return-object v0
.end method

.method public final bs()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377362
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->l:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->l:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    .line 1377363
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->l:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    return-object v0
.end method

.method public final bt()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377364
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->m:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->m:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    .line 1377365
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->m:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    return-object v0
.end method

.method public final bu()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377366
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->r:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->r:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    .line 1377367
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->r:Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    return-object v0
.end method

.method public final bv()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377368
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->u:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->u:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    .line 1377369
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->u:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    return-object v0
.end method

.method public final bw()Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377370
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->z:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->z:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    .line 1377371
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->z:Lcom/facebook/search/results/protocol/pulse/PulseEmotionAnalysisExternalUrlModels$PulseEmotionAnalysisExternalUrlModel$EmotionalAnalysisModel;

    return-object v0
.end method

.method public final bx()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377372
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->C:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    const/16 v1, 0x18

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->C:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    .line 1377373
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->C:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    return-object v0
.end method

.method public final by()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377374
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->D:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->D:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    .line 1377375
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->D:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    return-object v0
.end method

.method public final bz()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377376
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->G:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->G:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    .line 1377377
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->G:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1377378
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->j:Ljava/util/List;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->j:Ljava/util/List;

    .line 1377379
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic cA()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377380
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bH()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic cB()LX/A2E;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377381
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bH()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ca()LX/A56;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377359
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bG()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic cb()LX/A55;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377487
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bq()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    move-result-object v0

    return-object v0
.end method

.method public final cc()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377465
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aE:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    const/16 v1, 0x4e

    const-class v2, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aE:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    .line 1377466
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aE:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    return-object v0
.end method

.method public final cd()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377467
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aF:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    const/16 v1, 0x4f

    const-class v2, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aF:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    .line 1377468
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aF:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    return-object v0
.end method

.method public final ce()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377469
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aG:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    const/16 v1, 0x50

    const-class v2, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aG:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    .line 1377470
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aG:Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    return-object v0
.end method

.method public final cf()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377471
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aJ:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    const/16 v1, 0x53

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aJ:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    .line 1377472
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aJ:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    return-object v0
.end method

.method public final cg()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377473
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aK:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    const/16 v1, 0x54

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aK:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    .line 1377474
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aK:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    return-object v0
.end method

.method public final ch()Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377475
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aR:Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    const/16 v1, 0x5b

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aR:Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    .line 1377476
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aR:Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    return-object v0
.end method

.method public final ci()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377477
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aS:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    const/16 v1, 0x5c

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aS:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    .line 1377478
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aS:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    return-object v0
.end method

.method public final cj()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377479
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aU:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    const/16 v1, 0x5e

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aU:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    .line 1377480
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aU:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    return-object v0
.end method

.method public final ck()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377481
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aV:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    const/16 v1, 0x5f

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aV:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    .line 1377482
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aV:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    return-object v0
.end method

.method public final cl()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377483
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aW:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x60

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aW:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1377484
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aW:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method public final cm()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377485
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aX:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    const/16 v1, 0x61

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aX:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    .line 1377486
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aX:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    return-object v0
.end method

.method public final cn()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377463
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bc:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    const/16 v1, 0x66

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bc:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    .line 1377464
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bc:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    return-object v0
.end method

.method public final co()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377488
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bh:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    const/16 v1, 0x6b

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bh:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    .line 1377489
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bh:Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    return-object v0
.end method

.method public final cp()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377490
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bi:Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    const/16 v1, 0x6c

    const-class v2, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bi:Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    .line 1377491
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bi:Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    return-object v0
.end method

.method public final synthetic cq()LX/A4I;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377492
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cj()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic cr()LX/A4H;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377493
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ci()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic cs()LX/A4G;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377494
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cg()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic ct()LX/A4F;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377495
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bG()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic cu()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377496
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cg()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic cv()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377497
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bz()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic cw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377498
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cj()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic cx()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377499
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ci()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic cy()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377500
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bE()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic cz()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377446
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bq()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377425
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ah:Ljava/lang/String;

    const/16 v1, 0x37

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ah:Ljava/lang/String;

    .line 1377426
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ah:Ljava/lang/String;

    return-object v0
.end method

.method public final dV_()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377427
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->t:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->t:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1377428
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->t:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    return-object v0
.end method

.method public final dW_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377429
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->P:Ljava/lang/String;

    const/16 v1, 0x25

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->P:Ljava/lang/String;

    .line 1377430
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->P:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1377431
    const v0, -0x3f55c6d4

    return v0
.end method

.method public final synthetic e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377432
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bV()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1377433
    const v0, 0xe6f9005

    return v0
.end method

.method public final synthetic j()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377434
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bx()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377435
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aT:Ljava/lang/String;

    const/16 v1, 0x5d

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aT:Ljava/lang/String;

    .line 1377436
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aT:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377437
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bd:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const/16 v1, 0x67

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bd:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1377438
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bd:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377439
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bg:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const/16 v1, 0x6a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bg:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1377440
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bg:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377441
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1377442
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1377443
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 1377444
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1377445
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aa:Z

    return v0
.end method

.method public final p()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377423
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->s:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->s:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1377424
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->s:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method public final synthetic q()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377447
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bA()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic r()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377448
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cf()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377449
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->be:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/16 v1, 0x68

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->be:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1377450
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->be:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method public final synthetic t()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377451
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->co()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object v0

    return-object v0
.end method

.method public final u()Z
    .locals 2

    .prologue
    .line 1377452
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1377453
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->p:Z

    return v0
.end method

.method public final v()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1377454
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->q:Ljava/util/List;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->q:Ljava/util/List;

    .line 1377455
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final w()Z
    .locals 2

    .prologue
    .line 1377456
    const/4 v0, 0x2

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1377457
    iget-boolean v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->y:Z

    return v0
.end method

.method public final synthetic x()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377458
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bN()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic y()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377459
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bO()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v0

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1377460
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ba:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    const/16 v1, 0x64

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ba:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1377461
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ba:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    return-object v0
.end method
