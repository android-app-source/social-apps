.class public final Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1385151
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;

    new-instance v1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1385152
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1385153
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1385154
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1385155
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 1385156
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1385157
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1385158
    if-eqz v2, :cond_0

    .line 1385159
    const-string v3, "all_share_stories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1385160
    invoke-static {v1, v2, p1}, LX/8fS;->a(LX/15i;ILX/0nX;)V

    .line 1385161
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1385162
    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 1385163
    const-string v4, "creation_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1385164
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1385165
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1385166
    if-eqz v2, :cond_2

    .line 1385167
    const-string v3, "external_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1385168
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1385169
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1385170
    if-eqz v2, :cond_3

    .line 1385171
    const-string v3, "external_url_owning_profile"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1385172
    invoke-static {v1, v2, p1, p2}, LX/8fU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1385173
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1385174
    if-eqz v2, :cond_4

    .line 1385175
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1385176
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1385177
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1385178
    if-eqz v2, :cond_5

    .line 1385179
    const-string v3, "instant_article"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1385180
    invoke-static {v1, v2, p1, p2}, LX/8fX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1385181
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1385182
    if-eqz v2, :cond_6

    .line 1385183
    const-string v3, "link_media"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1385184
    invoke-static {v1, v2, p1, p2}, LX/A4n;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1385185
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1385186
    if-eqz v2, :cond_7

    .line 1385187
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1385188
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1385189
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1385190
    if-eqz v2, :cond_8

    .line 1385191
    const-string v3, "source"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1385192
    invoke-static {v1, v2, p1}, LX/8fY;->a(LX/15i;ILX/0nX;)V

    .line 1385193
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1385194
    if-eqz v2, :cond_9

    .line 1385195
    const-string v3, "summary"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1385196
    invoke-static {v1, v2, p1}, LX/8fZ;->a(LX/15i;ILX/0nX;)V

    .line 1385197
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1385198
    if-eqz v2, :cond_a

    .line 1385199
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1385200
    invoke-static {v1, v2, p1}, LX/8fa;->a(LX/15i;ILX/0nX;)V

    .line 1385201
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1385202
    if-eqz v2, :cond_b

    .line 1385203
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1385204
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1385205
    :cond_b
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1385206
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1385207
    check-cast p1, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$Serializer;->a(Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel;LX/0nX;LX/0my;)V

    return-void
.end method
