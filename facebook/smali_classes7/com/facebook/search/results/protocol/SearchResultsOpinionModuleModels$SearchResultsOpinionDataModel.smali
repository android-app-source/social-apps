.class public final Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/8d7;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2009527b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1384379
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1384378
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1384376
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1384377
    return-void
.end method

.method private a()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1384374
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->e:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->e:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    .line 1384375
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->e:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    return-object v0
.end method

.method private j()Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1384372
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->f:Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->f:Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    .line 1384373
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->f:Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1384380
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1384381
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1384382
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->j()Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1384383
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1384384
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1384385
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1384386
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1384387
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1384359
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1384360
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1384361
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    .line 1384362
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1384363
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;

    .line 1384364
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->e:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    .line 1384365
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->j()Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1384366
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->j()Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    .line 1384367
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->j()Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1384368
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;

    .line 1384369
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->f:Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    .line 1384370
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1384371
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic aP()LX/8dH;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1384358
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1384355
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataModel;-><init>()V

    .line 1384356
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1384357
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1384354
    const v0, -0x3d42091b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1384353
    const v0, -0x2114909c

    return v0
.end method
