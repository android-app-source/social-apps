.class public final Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2ea5ff83
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:J

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1382811
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1382810
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1382808
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1382809
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1382805
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1382806
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1382807
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;)Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;
    .locals 4

    .prologue
    .line 1382791
    if-nez p0, :cond_0

    .line 1382792
    const/4 p0, 0x0

    .line 1382793
    :goto_0
    return-object p0

    .line 1382794
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    if-eqz v0, :cond_1

    .line 1382795
    check-cast p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    goto :goto_0

    .line 1382796
    :cond_1
    new-instance v0, LX/8eO;

    invoke-direct {v0}, LX/8eO;-><init>()V

    .line 1382797
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8eO;->a:Ljava/lang/String;

    .line 1382798
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->b()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/8eO;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1382799
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->c()J

    move-result-wide v2

    iput-wide v2, v0, LX/8eO;->c:J

    .line 1382800
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8eO;->d:Ljava/lang/String;

    .line 1382801
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8eO;->e:Ljava/lang/String;

    .line 1382802
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->fq_()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;->a(Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;)Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    move-result-object v1

    iput-object v1, v0, LX/8eO;->f:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    .line 1382803
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->fp_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8eO;->g:Ljava/lang/String;

    .line 1382804
    invoke-virtual {v0}, LX/8eO;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object p0

    goto :goto_0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1382789
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1382790
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1382787
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->j:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->j:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    .line 1382788
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->j:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 1382770
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1382771
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1382772
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1382773
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1382774
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1382775
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->k()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1382776
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->fp_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1382777
    const/4 v2, 0x7

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1382778
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1382779
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1382780
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->g:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1382781
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1382782
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1382783
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1382784
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1382785
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1382786
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1382754
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1382755
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1382756
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1382757
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1382758
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    .line 1382759
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1382760
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->k()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1382761
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->k()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    .line 1382762
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->k()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1382763
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    .line 1382764
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->j:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    .line 1382765
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1382766
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1382768
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->e:Ljava/lang/String;

    .line 1382769
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1382812
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1382813
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->g:J

    .line 1382814
    return-void
.end method

.method public final synthetic b()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1382767
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1382751
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;-><init>()V

    .line 1382752
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1382753
    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 1382749
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1382750
    iget-wide v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->g:J

    return-wide v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1382747
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->h:Ljava/lang/String;

    .line 1382748
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1382746
    const v0, 0x40ee9550

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1382744
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->i:Ljava/lang/String;

    .line 1382745
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1382743
    const v0, -0x240c5345

    return v0
.end method

.method public final fp_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1382741
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->k:Ljava/lang/String;

    .line 1382742
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic fq_()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1382740
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->k()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel$MediaModel;

    move-result-object v0

    return-object v0
.end method
