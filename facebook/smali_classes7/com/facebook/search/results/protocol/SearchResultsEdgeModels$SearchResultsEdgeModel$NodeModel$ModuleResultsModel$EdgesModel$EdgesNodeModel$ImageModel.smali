.class public final Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/1Fb;
.implements LX/0jT;
.implements LX/A2S;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2b81edc6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1376581
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1376587
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1376585
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1376586
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1376582
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1376583
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1376584
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;
    .locals 2

    .prologue
    .line 1376548
    if-nez p0, :cond_0

    .line 1376549
    const/4 p0, 0x0

    .line 1376550
    :goto_0
    return-object p0

    .line 1376551
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    if-eqz v0, :cond_1

    .line 1376552
    check-cast p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    goto :goto_0

    .line 1376553
    :cond_1
    new-instance v0, LX/8db;

    invoke-direct {v0}, LX/8db;-><init>()V

    .line 1376554
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;->a()I

    move-result v1

    iput v1, v0, LX/8db;->a:I

    .line 1376555
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8db;->b:Ljava/lang/String;

    .line 1376556
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;->c()I

    move-result v1

    iput v1, v0, LX/8db;->c:I

    .line 1376557
    invoke-virtual {v0}, LX/8db;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1376579
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1376580
    iget v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1376571
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1376572
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1376573
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1376574
    iget v1, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1376575
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1376576
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1376577
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1376578
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1376588
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1376589
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1376590
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1376567
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1376568
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;->e:I

    .line 1376569
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;->g:I

    .line 1376570
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1376564
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;-><init>()V

    .line 1376565
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1376566
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1376562
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;->f:Ljava/lang/String;

    .line 1376563
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1376560
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1376561
    iget v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;->g:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1376559
    const v0, -0x50e3a888

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1376558
    const v0, 0x437b93b

    return v0
.end method
