.class public final Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/8dG;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x50a312db
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1385828
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1385858
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1385856
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1385857
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1385853
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1385854
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1385855
    return-void
.end method

.method public static a(LX/8dG;)Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;
    .locals 2

    .prologue
    .line 1385845
    if-nez p0, :cond_0

    .line 1385846
    const/4 p0, 0x0

    .line 1385847
    :goto_0
    return-object p0

    .line 1385848
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    if-eqz v0, :cond_1

    .line 1385849
    check-cast p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    goto :goto_0

    .line 1385850
    :cond_1
    new-instance v0, LX/8fj;

    invoke-direct {v0}, LX/8fj;-><init>()V

    .line 1385851
    invoke-interface {p0}, LX/8dG;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8fj;->a:Ljava/lang/String;

    .line 1385852
    invoke-virtual {v0}, LX/8fj;->a()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1385839
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1385840
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1385841
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1385842
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1385843
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1385844
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1385836
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1385837
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1385838
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385834
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;->e:Ljava/lang/String;

    .line 1385835
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1385831
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;-><init>()V

    .line 1385832
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1385833
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1385830
    const v0, 0x7e07f57c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1385829
    const v0, -0x6518607a

    return v0
.end method
