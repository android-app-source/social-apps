.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x401672ee
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1387226
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1387319
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1387317
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1387318
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1387314
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1387315
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1387316
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;
    .locals 10

    .prologue
    .line 1387288
    if-nez p0, :cond_0

    .line 1387289
    const/4 p0, 0x0

    .line 1387290
    :goto_0
    return-object p0

    .line 1387291
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    if-eqz v0, :cond_1

    .line 1387292
    check-cast p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    goto :goto_0

    .line 1387293
    :cond_1
    new-instance v0, LX/8gV;

    invoke-direct {v0}, LX/8gV;-><init>()V

    .line 1387294
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/8gV;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1387295
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->b()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    move-result-object v1

    iput-object v1, v0, LX/8gV;->b:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    .line 1387296
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8gV;->c:Ljava/lang/String;

    .line 1387297
    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 1387298
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1387299
    iget-object v3, v0, LX/8gV;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1387300
    iget-object v5, v0, LX/8gV;->b:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1387301
    iget-object v7, v0, LX/8gV;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1387302
    const/4 v8, 0x3

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 1387303
    invoke-virtual {v2, v9, v3}, LX/186;->b(II)V

    .line 1387304
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1387305
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1387306
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1387307
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1387308
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1387309
    invoke-virtual {v3, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1387310
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1387311
    new-instance v3, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;-><init>(LX/15i;)V

    .line 1387312
    move-object p0, v3

    .line 1387313
    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1387282
    iput-object p1, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->g:Ljava/lang/String;

    .line 1387283
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1387284
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1387285
    if-eqz v0, :cond_0

    .line 1387286
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1387287
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387280
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->f:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->f:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    .line 1387281
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->f:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1387270
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1387271
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1387272
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->j()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1387273
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1387274
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1387275
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1387276
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1387277
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1387278
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1387279
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1387262
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1387263
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->j()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1387264
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->j()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    .line 1387265
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->j()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1387266
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    .line 1387267
    iput-object v0, v1, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->f:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    .line 1387268
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1387269
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1387261
    new-instance v0, LX/8gW;

    invoke-direct {v0, p1}, LX/8gW;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387258
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1387259
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1387260
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 1387246
    const-string v0, "address.full_address"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1387247
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->j()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    move-result-object v0

    .line 1387248
    if-eqz v0, :cond_1

    .line 1387249
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1387250
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1387251
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1387252
    :goto_0
    return-void

    .line 1387253
    :cond_0
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1387254
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1387255
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1387256
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1387257
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1387235
    const-string v0, "address.full_address"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1387236
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->j()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    move-result-object v0

    .line 1387237
    if-eqz v0, :cond_0

    .line 1387238
    if-eqz p3, :cond_1

    .line 1387239
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    .line 1387240
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;->a(Ljava/lang/String;)V

    .line 1387241
    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->f:Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    .line 1387242
    :cond_0
    :goto_0
    return-void

    .line 1387243
    :cond_1
    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1387244
    :cond_2
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1387245
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1387232
    new-instance v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;-><init>()V

    .line 1387233
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1387234
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387231
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->j()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387229
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->g:Ljava/lang/String;

    .line 1387230
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1387228
    const v0, 0x20fdd655

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1387227
    const v0, 0x499e8e7

    return v0
.end method
