.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1387399
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;

    new-instance v1, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1387400
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1387401
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;LX/0nX;LX/0my;)V
    .locals 9

    .prologue
    .line 1387402
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1387403
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 v8, 0xe

    const/16 v5, 0xd

    const/4 v4, 0x2

    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    .line 1387404
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1387405
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1387406
    if-eqz v2, :cond_0

    .line 1387407
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387408
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1387409
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1387410
    if-eqz v2, :cond_1

    .line 1387411
    const-string v3, "can_viewer_join"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387412
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1387413
    :cond_1
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1387414
    if-eqz v2, :cond_2

    .line 1387415
    const-string v2, "connection_style"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387416
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1387417
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1387418
    cmp-long v4, v2, v6

    if-eqz v4, :cond_3

    .line 1387419
    const-string v4, "end_timestamp"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387420
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1387421
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1387422
    if-eqz v2, :cond_4

    .line 1387423
    const-string v3, "eventSocialContext"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387424
    invoke-static {v1, v2, p1}, LX/8gc;->a(LX/15i;ILX/0nX;)V

    .line 1387425
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1387426
    if-eqz v2, :cond_5

    .line 1387427
    const-string v3, "event_place"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387428
    invoke-static {v1, v2, p1, p2}, LX/8gb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1387429
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1387430
    if-eqz v2, :cond_6

    .line 1387431
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387432
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1387433
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1387434
    if-eqz v2, :cond_7

    .line 1387435
    const-string v3, "is_all_day"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387436
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1387437
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1387438
    if-eqz v2, :cond_8

    .line 1387439
    const-string v3, "is_verified"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387440
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1387441
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1387442
    if-eqz v2, :cond_9

    .line 1387443
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387444
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1387445
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1387446
    if-eqz v2, :cond_a

    .line 1387447
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387448
    invoke-static {v1, v2, p1}, LX/3lU;->a(LX/15i;ILX/0nX;)V

    .line 1387449
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1387450
    cmp-long v4, v2, v6

    if-eqz v4, :cond_b

    .line 1387451
    const-string v4, "start_timestamp"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387452
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1387453
    :cond_b
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1387454
    if-eqz v2, :cond_c

    .line 1387455
    const-string v3, "time_range_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387456
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1387457
    :cond_c
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1387458
    if-eqz v2, :cond_d

    .line 1387459
    const-string v2, "viewer_guest_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387460
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1387461
    :cond_d
    invoke-virtual {v1, v0, v8}, LX/15i;->g(II)I

    move-result v2

    .line 1387462
    if-eqz v2, :cond_e

    .line 1387463
    const-string v2, "viewer_watch_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1387464
    invoke-virtual {v1, v0, v8}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1387465
    :cond_e
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1387466
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1387467
    check-cast p1, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$Serializer;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel;LX/0nX;LX/0my;)V

    return-void
.end method
