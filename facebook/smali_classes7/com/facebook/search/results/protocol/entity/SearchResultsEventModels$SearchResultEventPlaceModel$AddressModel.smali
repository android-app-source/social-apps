.class public final Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xec47674
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1387202
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1387201
    const-class v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1387199
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1387200
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1387196
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1387197
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1387198
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;
    .locals 8

    .prologue
    .line 1387176
    if-nez p0, :cond_0

    .line 1387177
    const/4 p0, 0x0

    .line 1387178
    :goto_0
    return-object p0

    .line 1387179
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    if-eqz v0, :cond_1

    .line 1387180
    check-cast p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    goto :goto_0

    .line 1387181
    :cond_1
    new-instance v0, LX/8gU;

    invoke-direct {v0}, LX/8gU;-><init>()V

    .line 1387182
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8gU;->a:Ljava/lang/String;

    .line 1387183
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1387184
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1387185
    iget-object v3, v0, LX/8gU;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1387186
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1387187
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1387188
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1387189
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1387190
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1387191
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1387192
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1387193
    new-instance v3, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;-><init>(LX/15i;)V

    .line 1387194
    move-object p0, v3

    .line 1387195
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1387170
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1387171
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1387172
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1387173
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1387174
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1387175
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1387154
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1387155
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1387156
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1387168
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;->e:Ljava/lang/String;

    .line 1387169
    iget-object v0, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1387162
    iput-object p1, p0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;->e:Ljava/lang/String;

    .line 1387163
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1387164
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1387165
    if-eqz v0, :cond_0

    .line 1387166
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1387167
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1387159
    new-instance v0, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultEventPlaceModel$AddressModel;-><init>()V

    .line 1387160
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1387161
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1387158
    const v0, 0x12c271ce

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1387157
    const v0, 0x2fa39a51

    return v0
.end method
