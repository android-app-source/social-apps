.class public final Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/8dH;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xc89c45c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1385946
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1385945
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1385943
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1385944
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1385940
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1385941
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1385942
    return-void
.end method

.method public static a(LX/8dH;)Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1385919
    if-nez p0, :cond_0

    .line 1385920
    const/4 p0, 0x0

    .line 1385921
    :goto_0
    return-object p0

    .line 1385922
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    if-eqz v0, :cond_1

    .line 1385923
    check-cast p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    goto :goto_0

    .line 1385924
    :cond_1
    new-instance v3, LX/8fi;

    invoke-direct {v3}, LX/8fi;-><init>()V

    .line 1385925
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 1385926
    :goto_1
    invoke-interface {p0}, LX/8dH;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1385927
    invoke-interface {p0}, LX/8dH;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    invoke-static {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->a(Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1385928
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1385929
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/8fi;->a:LX/0Px;

    .line 1385930
    invoke-interface {p0}, LX/8dH;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/8fi;->b:Ljava/lang/String;

    .line 1385931
    invoke-interface {p0}, LX/8dH;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/8fi;->c:Ljava/lang/String;

    .line 1385932
    invoke-interface {p0}, LX/8dH;->j()LX/8dG;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;->a(LX/8dG;)Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    move-result-object v0

    iput-object v0, v3, LX/8fi;->d:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    .line 1385933
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1385934
    :goto_2
    invoke-interface {p0}, LX/8dH;->fn_()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 1385935
    invoke-interface {p0}, LX/8dH;->fn_()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1385936
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1385937
    :cond_3
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/8fi;->e:LX/0Px;

    .line 1385938
    invoke-interface {p0}, LX/8dH;->fo_()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;->a(Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;)Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    move-result-object v0

    iput-object v0, v3, LX/8fi;->f:Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    .line 1385939
    invoke-virtual {v3}, LX/8fi;->a()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-result-object p0

    goto :goto_0
.end method

.method private k()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385917
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->h:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->h:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    .line 1385918
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->h:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    return-object v0
.end method

.method private l()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385915
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->j:Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->j:Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    .line 1385916
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->j:Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1385899
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1385900
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1385901
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1385902
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1385903
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->k()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1385904
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->fn_()LX/0Px;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->c(Ljava/util/List;)I

    move-result v4

    .line 1385905
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->l()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1385906
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1385907
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1385908
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1385909
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1385910
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1385911
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1385912
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1385913
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1385914
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1385897
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->e:Ljava/util/List;

    .line 1385898
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1385866
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1385867
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1385868
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1385869
    if-eqz v1, :cond_3

    .line 1385870
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    .line 1385871
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1385872
    :goto_0
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->k()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1385873
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->k()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    .line 1385874
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->k()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1385875
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    .line 1385876
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->h:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    .line 1385877
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->l()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1385878
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->l()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    .line 1385879
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->l()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1385880
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    .line 1385881
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->j:Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    .line 1385882
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1385883
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1385894
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;-><init>()V

    .line 1385895
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1385896
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385892
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->f:Ljava/lang/String;

    .line 1385893
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385890
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->g:Ljava/lang/String;

    .line 1385891
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1385889
    const v0, -0x283d8685

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1385888
    const v0, -0x1bce060e

    return v0
.end method

.method public final fn_()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1385886
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->i:Ljava/util/List;

    .line 1385887
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic fo_()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385885
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->l()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/8dG;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1385884
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;->k()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    move-result-object v0

    return-object v0
.end method
