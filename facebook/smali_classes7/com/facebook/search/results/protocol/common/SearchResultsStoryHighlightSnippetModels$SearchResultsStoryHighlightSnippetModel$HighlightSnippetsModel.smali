.class public final Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4ffcc97a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1386824
    const-class v0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1386823
    const-class v0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1386813
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1386814
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1386845
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1386846
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1386847
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;)Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;
    .locals 8

    .prologue
    .line 1386825
    if-nez p0, :cond_0

    .line 1386826
    const/4 p0, 0x0

    .line 1386827
    :goto_0
    return-object p0

    .line 1386828
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;

    if-eqz v0, :cond_1

    .line 1386829
    check-cast p0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;

    goto :goto_0

    .line 1386830
    :cond_1
    new-instance v0, LX/8gF;

    invoke-direct {v0}, LX/8gF;-><init>()V

    .line 1386831
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;->a()Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;->a(Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;)Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    move-result-object v1

    iput-object v1, v0, LX/8gF;->a:Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    .line 1386832
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1386833
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1386834
    iget-object v3, v0, LX/8gF;->a:Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1386835
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1386836
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1386837
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1386838
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1386839
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1386840
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1386841
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1386842
    new-instance v3, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;-><init>(LX/15i;)V

    .line 1386843
    move-object p0, v3

    .line 1386844
    goto :goto_0
.end method

.method private j()Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1386815
    iget-object v0, p0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;->e:Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;->e:Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    .line 1386816
    iget-object v0, p0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;->e:Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1386817
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1386818
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;->j()Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1386819
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1386820
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1386821
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1386822
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1386799
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1386800
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;->j()Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1386801
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;->j()Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    .line 1386802
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;->j()Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1386803
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;

    .line 1386804
    iput-object v0, v1, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;->e:Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    .line 1386805
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1386806
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1386807
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;->j()Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel$SnippetSentencesModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1386808
    new-instance v0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;-><init>()V

    .line 1386809
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1386810
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1386811
    const v0, 0x28470571

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1386812
    const v0, -0x315555a1

    return v0
.end method
