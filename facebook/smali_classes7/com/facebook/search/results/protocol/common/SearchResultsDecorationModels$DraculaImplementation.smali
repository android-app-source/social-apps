.class public final Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1386115
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1386116
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1386113
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1386114
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 1386089
    if-nez p1, :cond_0

    move v0, v1

    .line 1386090
    :goto_0
    return v0

    .line 1386091
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1386092
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1386093
    :pswitch_0
    const-class v0, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1386094
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1386095
    const-class v0, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    invoke-virtual {p0, p1, v8, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1386096
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 1386097
    const-class v0, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    invoke-virtual {p0, p1, v9, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1386098
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 1386099
    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1386100
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1386101
    const-class v0, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    invoke-virtual {p0, p1, v11, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    .line 1386102
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1386103
    const/4 v0, 0x5

    const-class v7, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    invoke-virtual {p0, p1, v0, v7}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    .line 1386104
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1386105
    const/4 v7, 0x6

    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1386106
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1386107
    invoke-virtual {p3, v8, v3}, LX/186;->b(II)V

    .line 1386108
    invoke-virtual {p3, v9, v4}, LX/186;->b(II)V

    .line 1386109
    invoke-virtual {p3, v10, v5}, LX/186;->b(II)V

    .line 1386110
    invoke-virtual {p3, v11, v6}, LX/186;->b(II)V

    .line 1386111
    const/4 v1, 0x5

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1386112
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x6001c759
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1386088
    new-instance v0, Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1386084
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1386085
    if-eqz v0, :cond_0

    .line 1386086
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1386087
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1386079
    if-eqz p0, :cond_0

    .line 1386080
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1386081
    if-eq v0, p0, :cond_0

    .line 1386082
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1386083
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1386066
    packed-switch p2, :pswitch_data_0

    .line 1386067
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1386068
    :pswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1386069
    invoke-static {v0, p3}, Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1386070
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1386071
    invoke-static {v0, p3}, Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1386072
    const/4 v0, 0x2

    const-class v1, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1386073
    invoke-static {v0, p3}, Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1386074
    const/4 v0, 0x4

    const-class v1, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    .line 1386075
    invoke-static {v0, p3}, Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1386076
    const/4 v0, 0x5

    const-class v1, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    .line 1386077
    invoke-static {v0, p3}, Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1386078
    return-void

    :pswitch_data_0
    .packed-switch -0x6001c759
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1386065
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1386117
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1386118
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1386034
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1386035
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1386036
    :cond_0
    iput-object p1, p0, Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;->a:LX/15i;

    .line 1386037
    iput p2, p0, Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;->b:I

    .line 1386038
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1386048
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1386049
    new-instance v0, Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1386050
    iget v0, p0, LX/1vt;->c:I

    .line 1386051
    move v0, v0

    .line 1386052
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1386053
    iget v0, p0, LX/1vt;->c:I

    .line 1386054
    move v0, v0

    .line 1386055
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1386056
    iget v0, p0, LX/1vt;->b:I

    .line 1386057
    move v0, v0

    .line 1386058
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1386059
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1386060
    move-object v0, v0

    .line 1386061
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1386039
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1386040
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1386041
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1386042
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1386043
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1386044
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1386045
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1386046
    invoke-static {v3, v9, v2}, Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/results/protocol/common/SearchResultsDecorationModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1386047
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1386062
    iget v0, p0, LX/1vt;->c:I

    .line 1386063
    move v0, v0

    .line 1386064
    return v0
.end method
