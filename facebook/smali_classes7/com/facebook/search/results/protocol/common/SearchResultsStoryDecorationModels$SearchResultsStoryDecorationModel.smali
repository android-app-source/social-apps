.class public final Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/8g8;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xc2d92bf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1386592
    const-class v0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1386593
    const-class v0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1386594
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1386595
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1386596
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1386597
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1386598
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;)Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 1386599
    if-nez p0, :cond_0

    .line 1386600
    const/4 p0, 0x0

    .line 1386601
    :goto_0
    return-object p0

    .line 1386602
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;

    if-eqz v0, :cond_1

    .line 1386603
    check-cast p0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;

    goto :goto_0

    .line 1386604
    :cond_1
    new-instance v3, LX/8gA;

    invoke-direct {v3}, LX/8gA;-><init>()V

    .line 1386605
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 1386606
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1386607
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;

    invoke-static {v0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;->a(Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;)Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1386608
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1386609
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/8gA;->a:LX/0Px;

    .line 1386610
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1386611
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 1386612
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1386613
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1386614
    :cond_3
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/8gA;->b:LX/0Px;

    .line 1386615
    const/4 v9, 0x1

    const/4 v11, 0x0

    const/4 v7, 0x0

    .line 1386616
    new-instance v5, LX/186;

    const/16 v6, 0x80

    invoke-direct {v5, v6}, LX/186;-><init>(I)V

    .line 1386617
    iget-object v6, v3, LX/8gA;->a:LX/0Px;

    invoke-static {v5, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 1386618
    iget-object v8, v3, LX/8gA;->b:LX/0Px;

    invoke-virtual {v5, v8}, LX/186;->b(Ljava/util/List;)I

    move-result v8

    .line 1386619
    const/4 v10, 0x2

    invoke-virtual {v5, v10}, LX/186;->c(I)V

    .line 1386620
    invoke-virtual {v5, v11, v6}, LX/186;->b(II)V

    .line 1386621
    invoke-virtual {v5, v9, v8}, LX/186;->b(II)V

    .line 1386622
    invoke-virtual {v5}, LX/186;->d()I

    move-result v6

    .line 1386623
    invoke-virtual {v5, v6}, LX/186;->d(I)V

    .line 1386624
    invoke-virtual {v5}, LX/186;->e()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 1386625
    invoke-virtual {v6, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1386626
    new-instance v5, LX/15i;

    move-object v8, v7

    move-object v10, v7

    invoke-direct/range {v5 .. v10}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1386627
    new-instance v6, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;

    invoke-direct {v6, v5}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;-><init>(LX/15i;)V

    .line 1386628
    move-object p0, v6

    .line 1386629
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1386630
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1386631
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1386632
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1386633
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1386634
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1386635
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1386636
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1386637
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1386638
    iget-object v0, p0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->e:Ljava/util/List;

    .line 1386639
    iget-object v0, p0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1386640
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1386641
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1386642
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1386643
    if-eqz v1, :cond_0

    .line 1386644
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;

    .line 1386645
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->e:Ljava/util/List;

    .line 1386646
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1386647
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1386648
    iget-object v0, p0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->f:Ljava/util/List;

    .line 1386649
    iget-object v0, p0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1386650
    new-instance v0, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;-><init>()V

    .line 1386651
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1386652
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1386653
    const v0, 0x474ff4f9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1386654
    const v0, 0x16973d43

    return v0
.end method
