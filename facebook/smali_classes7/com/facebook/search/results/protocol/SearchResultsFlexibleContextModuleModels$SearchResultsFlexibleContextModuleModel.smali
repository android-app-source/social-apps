.class public final Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/8dK;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6645ace8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1383160
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1383159
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1383157
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1383158
    return-void
.end method

.method private a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383155
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel;->e:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel;->e:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel;

    .line 1383156
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel;->e:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1383161
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1383162
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1383163
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1383164
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1383165
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1383166
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1383147
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1383148
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1383149
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel;

    .line 1383150
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1383151
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel;

    .line 1383152
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel;->e:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel;

    .line 1383153
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1383154
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1383144
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel;-><init>()V

    .line 1383145
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1383146
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1383143
    const v0, 0x127d6282

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1383142
    const v0, -0x1c2adf8

    return v0
.end method
