.class public final Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;
.implements LX/8d6;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x63a646c0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1384208
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1384209
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1384163
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1384164
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1384205
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1384206
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1384207
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private j()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1384203
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->f:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->f:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    .line 1384204
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->f:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1384201
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1384202
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1384199
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->h:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->h:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    .line 1384200
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->h:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1384183
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1384184
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1384185
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->j()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1384186
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1384187
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->l()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1384188
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->bh()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1384189
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->bi()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1384190
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1384191
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1384192
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1384193
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1384194
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1384195
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1384196
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1384197
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1384198
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1384165
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1384166
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->j()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1384167
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->j()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    .line 1384168
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->j()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1384169
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;

    .line 1384170
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->f:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    .line 1384171
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1384172
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1384173
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1384174
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;

    .line 1384175
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1384176
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->l()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1384177
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->l()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    .line 1384178
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->l()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1384179
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;

    .line 1384180
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->h:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    .line 1384181
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1384182
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1384210
    new-instance v0, LX/8f0;

    invoke-direct {v0, p1}, LX/8f0;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1384150
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1384151
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1384152
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1384153
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;-><init>()V

    .line 1384154
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1384155
    return-object v0
.end method

.method public final synthetic bg()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1384156
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->l()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v0

    return-object v0
.end method

.method public final bh()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1384157
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->i:Ljava/lang/String;

    .line 1384158
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final bi()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1384159
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->j:Ljava/lang/String;

    .line 1384160
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1384161
    const v0, -0xbd66cd1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1384162
    const v0, 0x252222

    return v0
.end method
