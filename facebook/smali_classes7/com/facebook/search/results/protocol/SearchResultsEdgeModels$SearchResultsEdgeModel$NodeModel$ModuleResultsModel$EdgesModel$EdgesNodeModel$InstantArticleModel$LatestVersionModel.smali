.class public final Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x9b91c52
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1376752
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1376751
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1376749
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1376750
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1376746
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1376747
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1376748
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;
    .locals 9

    .prologue
    .line 1376723
    if-nez p0, :cond_0

    .line 1376724
    const/4 p0, 0x0

    .line 1376725
    :goto_0
    return-object p0

    .line 1376726
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;

    if-eqz v0, :cond_1

    .line 1376727
    check-cast p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;

    goto :goto_0

    .line 1376728
    :cond_1
    new-instance v0, LX/8dd;

    invoke-direct {v0}, LX/8dd;-><init>()V

    .line 1376729
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/8dd;->a:Ljava/lang/String;

    .line 1376730
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->b()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    move-result-object v1

    iput-object v1, v0, LX/8dd;->b:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    .line 1376731
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1376732
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1376733
    iget-object v3, v0, LX/8dd;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1376734
    iget-object v5, v0, LX/8dd;->b:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1376735
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1376736
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1376737
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1376738
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1376739
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1376740
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1376741
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1376742
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1376743
    new-instance v3, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;-><init>(LX/15i;)V

    .line 1376744
    move-object p0, v3

    .line 1376745
    goto :goto_0
.end method

.method private k()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1376721
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->f:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->f:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    .line 1376722
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->f:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1376753
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1376754
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1376755
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->k()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1376756
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1376757
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1376758
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1376759
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1376760
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1376713
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1376714
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->k()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1376715
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->k()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    .line 1376716
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->k()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1376717
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;

    .line 1376718
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->f:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    .line 1376719
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1376720
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1376711
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->e:Ljava/lang/String;

    .line 1376712
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1376708
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;-><init>()V

    .line 1376709
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1376710
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1376707
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->k()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1376706
    const v0, -0x14f34f47

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1376705
    const v0, 0x60826b23

    return v0
.end method

.method public final synthetic j()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1376704
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->k()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    move-result-object v0

    return-object v0
.end method
