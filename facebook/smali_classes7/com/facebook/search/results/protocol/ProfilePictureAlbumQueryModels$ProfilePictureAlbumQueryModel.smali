.class public final Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x644623aa
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1375150
    const-class v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1375149
    const-class v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1375124
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1375125
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1375143
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1375144
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;->a()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1375145
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1375146
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1375147
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1375148
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1375135
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1375136
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;->a()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1375137
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;->a()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;

    .line 1375138
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;->a()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1375139
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;

    .line 1375140
    iput-object v0, v1, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;->e:Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;

    .line 1375141
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1375142
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1375134
    new-instance v0, LX/8ck;

    invoke-direct {v0, p1}, LX/8ck;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1375151
    iget-object v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;->e:Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;->e:Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;

    .line 1375152
    iget-object v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;->e:Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1375132
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1375133
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1375131
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1375128
    new-instance v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel;-><init>()V

    .line 1375129
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1375130
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1375127
    const v0, -0x7ef98f83

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1375126
    const v0, 0x285feb

    return v0
.end method
