.class public final Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;
.implements LX/A4E;
.implements LX/A56;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5426d2e2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1375861
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1375851
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1375852
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1375853
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1375854
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1375855
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1375856
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;
    .locals 10

    .prologue
    .line 1375862
    if-nez p0, :cond_0

    .line 1375863
    const/4 p0, 0x0

    .line 1375864
    :goto_0
    return-object p0

    .line 1375865
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;

    if-eqz v0, :cond_1

    .line 1375866
    check-cast p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;

    goto :goto_0

    .line 1375867
    :cond_1
    new-instance v0, LX/8dS;

    invoke-direct {v0}, LX/8dS;-><init>()V

    .line 1375868
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/8dS;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1375869
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->b()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;->a(Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;)Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    move-result-object v1

    iput-object v1, v0, LX/8dS;->b:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    .line 1375870
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->c()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/8dS;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1375871
    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 1375872
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1375873
    iget-object v3, v0, LX/8dS;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1375874
    iget-object v5, v0, LX/8dS;->b:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1375875
    iget-object v7, v0, LX/8dS;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1375876
    const/4 v8, 0x3

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 1375877
    invoke-virtual {v2, v9, v3}, LX/186;->b(II)V

    .line 1375878
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1375879
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1375880
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1375881
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1375882
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1375883
    invoke-virtual {v3, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1375884
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1375885
    new-instance v3, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;

    invoke-direct {v3, v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;-><init>(LX/15i;)V

    .line 1375886
    move-object p0, v3

    .line 1375887
    goto :goto_0
.end method

.method private j()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1375857
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->f:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->f:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    .line 1375858
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->f:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1375859
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1375860
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1375828
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1375829
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1375830
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->j()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1375831
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1375832
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1375833
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1375834
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1375835
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1375836
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1375837
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1375838
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1375839
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->j()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1375840
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->j()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    .line 1375841
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->j()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1375842
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;

    .line 1375843
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->f:Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    .line 1375844
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1375845
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1375846
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1375847
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;

    .line 1375848
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1375849
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1375850
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1375814
    new-instance v0, LX/8dT;

    invoke-direct {v0, p1}, LX/8dT;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1375815
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1375816
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1375817
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1375826
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1375827
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1375818
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1375819
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;-><init>()V

    .line 1375820
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1375821
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1375822
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->j()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1375823
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$LinkMediaModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1375824
    const v0, -0x593ac7c7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1375825
    const v0, 0x46c7fc4

    return v0
.end method
