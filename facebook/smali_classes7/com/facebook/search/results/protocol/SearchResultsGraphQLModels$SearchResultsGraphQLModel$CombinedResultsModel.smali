.class public final Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/8ef;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x45f51c5d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1383489
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1383488
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1383537
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1383538
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1383534
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1383535
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1383536
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1383518
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1383519
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1383520
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->j()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1383521
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1383522
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1383523
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1383524
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1383525
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1383526
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1383527
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1383528
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1383529
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1383530
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1383531
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1383532
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1383533
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1383516
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->e:Ljava/util/List;

    .line 1383517
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1383503
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1383504
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1383505
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1383506
    if-eqz v1, :cond_2

    .line 1383507
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    .line 1383508
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1383509
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->j()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1383510
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->j()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    .line 1383511
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->j()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1383512
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    .line 1383513
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->f:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    .line 1383514
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1383515
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final synthetic b()LX/80n;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383502
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->j()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1383539
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;-><init>()V

    .line 1383540
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1383541
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383500
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->h:Ljava/lang/String;

    .line 1383501
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1383499
    const v0, 0x58ee3a50

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1383498
    const v0, 0x21af1a07

    return v0
.end method

.method public final j()Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383496
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->f:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->f:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    .line 1383497
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->f:Lcom/facebook/feed/protocol/FetchNewsFeedGraphQLModels$DefaultPageInfoFieldsStreamingModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383494
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->g:Ljava/lang/String;

    .line 1383495
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383492
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->i:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->i:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    .line 1383493
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->i:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383490
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->j:Ljava/lang/String;

    .line 1383491
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->j:Ljava/lang/String;

    return-object v0
.end method
