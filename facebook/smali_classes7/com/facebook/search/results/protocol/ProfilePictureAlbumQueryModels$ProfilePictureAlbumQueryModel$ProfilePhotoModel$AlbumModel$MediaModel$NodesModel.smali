.class public final Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x44b4971e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1374983
    const-class v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1374986
    const-class v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1374984
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1374985
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1374980
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1374981
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1374982
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1374978
    iget-object v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->f:Ljava/lang/String;

    .line 1374979
    iget-object v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1374976
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1374977
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1374966
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1374967
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1374968
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1374969
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x92ce362

    invoke-static {v3, v2, v4}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1374970
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1374971
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1374972
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1374973
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1374974
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1374975
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1374987
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1374988
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1374989
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x92ce362

    invoke-static {v2, v0, v3}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1374990
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1374991
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;

    .line 1374992
    iput v3, v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->g:I

    .line 1374993
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1374994
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1374995
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1374996
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1374965
    new-instance v0, LX/8cl;

    invoke-direct {v0, p1}, LX/8cl;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1374964
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1374961
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1374962
    const/4 v0, 0x2

    const v1, 0x92ce362

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;->g:I

    .line 1374963
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1374959
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1374960
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1374958
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1374953
    new-instance v0, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/ProfilePictureAlbumQueryModels$ProfilePictureAlbumQueryModel$ProfilePhotoModel$AlbumModel$MediaModel$NodesModel;-><init>()V

    .line 1374954
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1374955
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1374957
    const v0, 0x5ccd28bb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1374956
    const v0, 0x46c7fc4

    return v0
.end method
