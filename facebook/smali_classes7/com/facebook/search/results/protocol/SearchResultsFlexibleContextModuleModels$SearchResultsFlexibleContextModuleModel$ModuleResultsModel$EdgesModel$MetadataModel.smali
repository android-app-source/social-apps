.class public final Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x12fe0c92
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1383054
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1383053
    const-class v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1383051
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1383052
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1383048
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1383049
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1383050
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;)Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;
    .locals 2

    .prologue
    .line 1383039
    if-nez p0, :cond_0

    .line 1383040
    const/4 p0, 0x0

    .line 1383041
    :goto_0
    return-object p0

    .line 1383042
    :cond_0
    instance-of v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    if-eqz v0, :cond_1

    .line 1383043
    check-cast p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    goto :goto_0

    .line 1383044
    :cond_1
    new-instance v0, LX/8eW;

    invoke-direct {v0}, LX/8eW;-><init>()V

    .line 1383045
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->a(Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;)Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v1

    iput-object v1, v0, LX/8eW;->a:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    .line 1383046
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->b()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->a(Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;)Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v1

    iput-object v1, v0, LX/8eW;->b:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    .line 1383047
    invoke-virtual {v0}, LX/8eW;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    move-result-object p0

    goto :goto_0
.end method

.method private j()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383037
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->e:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->e:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    .line 1383038
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->e:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    return-object v0
.end method

.method private k()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383035
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->f:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    iput-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->f:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    .line 1383036
    iget-object v0, p0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->f:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1383027
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1383028
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->j()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1383029
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->k()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1383030
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1383031
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1383032
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1383033
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1383034
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1383014
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1383015
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->j()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1383016
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->j()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    .line 1383017
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->j()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1383018
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    .line 1383019
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->e:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    .line 1383020
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->k()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1383021
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->k()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    .line 1383022
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->k()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1383023
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    .line 1383024
    iput-object v0, v1, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->f:Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    .line 1383025
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1383026
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383007
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->j()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1383011
    new-instance v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    invoke-direct {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;-><init>()V

    .line 1383012
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1383013
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1383010
    invoke-direct {p0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->k()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1383009
    const v0, -0x309eba96

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1383008
    const v0, 0x7c3d9a54

    return v0
.end method
