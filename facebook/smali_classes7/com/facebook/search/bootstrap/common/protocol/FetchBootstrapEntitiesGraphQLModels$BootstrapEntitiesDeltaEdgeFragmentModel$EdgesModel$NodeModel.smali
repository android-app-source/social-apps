.class public final Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/8bp;
.implements LX/8bo;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x48a2e76e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1372648
    const-class v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1372649
    const-class v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1372650
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1372651
    return-void
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372670
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1372671
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1372672
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1372652
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1372653
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1372654
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1372655
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1372656
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->j()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1372657
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->e()LX/2uF;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v4

    .line 1372658
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1372659
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1372660
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1372661
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1372662
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1372663
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1372664
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1372665
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1372666
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1372667
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1372668
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1372669
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1372673
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1372674
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->j()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1372675
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->j()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    .line 1372676
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->j()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1372677
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;

    .line 1372678
    iput-object v0, v1, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->h:Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    .line 1372679
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->e()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1372680
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->e()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 1372681
    if-eqz v2, :cond_1

    .line 1372682
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;

    .line 1372683
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->i:LX/3Sb;

    move-object v1, v0

    .line 1372684
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1372685
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372643
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 1372644
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1372645
    new-instance v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;-><init>()V

    .line 1372646
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1372647
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372633
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    .line 1372634
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372630
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    .line 1372631
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSearchable"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372632
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->j()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1372642
    const v0, -0x4eae5671

    return v0
.end method

.method public final e()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStructuredGrammarCosts"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1372635
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->i:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x4

    const v4, -0x39de6c7c

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->i:LX/3Sb;

    .line 1372636
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->i:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1372637
    const v0, 0x4ccaea6d    # 1.0638628E8f

    return v0
.end method

.method public final j()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSearchable"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372638
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->h:Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->h:Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    .line 1372639
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->h:Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372640
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->k:Ljava/lang/String;

    .line 1372641
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$BootstrapEntitiesDeltaEdgeFragmentModel$EdgesModel$NodeModel;->k:Ljava/lang/String;

    return-object v0
.end method
