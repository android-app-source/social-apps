.class public final Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/7CB;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xa62091c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$ContactModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Z

.field private o:Z

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1372454
    const-class v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1372455
    const-class v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1372456
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1372457
    return-void
.end method

.method private r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372458
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1372459
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method private s()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$ContactModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getContact"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372460
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->h:Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$ContactModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$ContactModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$ContactModel;

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->h:Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$ContactModel;

    .line 1372461
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->h:Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$ContactModel;

    return-object v0
.end method

.method private t()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372462
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->l:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->l:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    .line 1372463
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->l:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    return-object v0
.end method

.method private u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372464
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1372465
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372528
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->u:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->u:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1372529
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->u:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 1372466
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1372467
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1372468
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1372469
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1372470
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->s()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$ContactModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1372471
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->dZ_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1372472
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->t()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1372473
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1372474
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->m()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1372475
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->n()LX/0Px;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/util/List;)I

    move-result v8

    .line 1372476
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1372477
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->p()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 1372478
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->q()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v11

    invoke-virtual {p1, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 1372479
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->v()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v12

    invoke-virtual {p1, v12}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    .line 1372480
    const/16 v13, 0x11

    invoke-virtual {p1, v13}, LX/186;->c(I)V

    .line 1372481
    const/4 v13, 0x0

    invoke-virtual {p1, v13, v0}, LX/186;->b(II)V

    .line 1372482
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1372483
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1372484
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1372485
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1372486
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->j:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1372487
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1372488
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1372489
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1372490
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->n:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1372491
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->o:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1372492
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1372493
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1372494
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1372495
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1372496
    const/16 v0, 0xf

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1372497
    const/16 v0, 0x10

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 1372498
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1372499
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1372500
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1372501
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->s()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$ContactModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1372502
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->s()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$ContactModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$ContactModel;

    .line 1372503
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->s()Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$ContactModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1372504
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    .line 1372505
    iput-object v0, v1, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->h:Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel$ContactModel;

    .line 1372506
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->t()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1372507
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->t()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    .line 1372508
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->t()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1372509
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    .line 1372510
    iput-object v0, v1, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->l:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    .line 1372511
    :cond_1
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1372512
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1372513
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1372514
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    .line 1372515
    iput-object v0, v1, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1372516
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1372517
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372518
    invoke-virtual {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1372519
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1372520
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->i:Z

    .line 1372521
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->j:Z

    .line 1372522
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->n:Z

    .line 1372523
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->o:Z

    .line 1372524
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372525
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1372526
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1372527
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1372449
    new-instance v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;

    invoke-direct {v0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;-><init>()V

    .line 1372450
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1372451
    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1372452
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->f:Ljava/util/List;

    .line 1372453
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1372425
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1372426
    iget-boolean v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->i:Z

    return v0
.end method

.method public final dZ_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372427
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1372428
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1372429
    const v0, 0x16dde695    # 3.5849993E-25f

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1372430
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1372431
    iget-boolean v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->j:Z

    return v0
.end method

.method public final synthetic ea_()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372432
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->t()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1372433
    const v0, 0x4c7ec322    # 6.6784392E7f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372434
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->m:Ljava/lang/String;

    .line 1372435
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1372436
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1372437
    iget-boolean v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->n:Z

    return v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 1372438
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1372439
    iget-boolean v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->o:Z

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372440
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->p:Ljava/lang/String;

    .line 1372441
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final n()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1372442
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->q:Ljava/util/List;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->q:Ljava/util/List;

    .line 1372443
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->q:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic o()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372444
    invoke-direct {p0}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372445
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->s:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->s:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1372446
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->s:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1372447
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->t:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->t:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1372448
    iget-object v0, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$AddEntityFragmentModel$SearchableModel;->t:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method
