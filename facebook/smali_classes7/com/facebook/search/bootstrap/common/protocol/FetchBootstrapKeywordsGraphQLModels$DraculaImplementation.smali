.class public final Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1373690
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1373691
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1373692
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1373693
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    .line 1373694
    if-nez p1, :cond_0

    .line 1373695
    const/4 v0, 0x0

    .line 1373696
    :goto_0
    return v0

    .line 1373697
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1373698
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1373699
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1373700
    const v1, -0x64278fd2

    const/4 v3, 0x0

    .line 1373701
    if-nez v0, :cond_1

    move v2, v3

    .line 1373702
    :goto_1
    move v0, v2

    .line 1373703
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1373704
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1373705
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1373706
    :sswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1373707
    const v1, 0x34eb3da9

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1373708
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1373709
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1373710
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1373711
    :sswitch_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1373712
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1373713
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1373714
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1373715
    const/4 v2, 0x2

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v2, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1373716
    const/4 v4, 0x3

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1373717
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1373718
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1373719
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1373720
    const/4 v4, 0x5

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1373721
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1373722
    const/4 v4, 0x6

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1373723
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1373724
    const/4 v4, 0x7

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1373725
    const/4 v4, 0x0

    invoke-virtual {p3, v4, v0}, LX/186;->b(II)V

    .line 1373726
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1373727
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1373728
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 1373729
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v7}, LX/186;->b(II)V

    .line 1373730
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v8}, LX/186;->b(II)V

    .line 1373731
    const/4 v0, 0x6

    invoke-virtual {p3, v0, v9}, LX/186;->b(II)V

    .line 1373732
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1373733
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    .line 1373734
    if-nez v4, :cond_2

    const/4 v2, 0x0

    .line 1373735
    :goto_2
    if-ge v3, v4, :cond_3

    .line 1373736
    invoke-virtual {p0, v0, v3}, LX/15i;->q(II)I

    move-result v5

    .line 1373737
    invoke-static {p0, v5, v1, p3}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v5

    aput v5, v2, v3

    .line 1373738
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1373739
    :cond_2
    new-array v2, v4, [I

    goto :goto_2

    .line 1373740
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p3, v2, v3}, LX/186;->a([IZ)I

    move-result v2

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7378f90b -> :sswitch_0
        -0x64278fd2 -> :sswitch_1
        0x34eb3da9 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1373760
    new-instance v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1373741
    sparse-switch p2, :sswitch_data_0

    .line 1373742
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1373743
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1373744
    const v1, -0x64278fd2

    .line 1373745
    if-eqz v0, :cond_0

    .line 1373746
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1373747
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1373748
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1373749
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1373750
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1373751
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 1373752
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1373753
    const v1, 0x34eb3da9

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7378f90b -> :sswitch_0
        -0x64278fd2 -> :sswitch_2
        0x34eb3da9 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1373754
    if-eqz p1, :cond_0

    .line 1373755
    invoke-static {p0, p1, p2}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1373756
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;

    .line 1373757
    if-eq v0, v1, :cond_0

    .line 1373758
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1373759
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1373687
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1373688
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1373689
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1373682
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1373683
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1373684
    :cond_0
    iput-object p1, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1373685
    iput p2, p0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;->b:I

    .line 1373686
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1373681
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1373680
    new-instance v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1373677
    iget v0, p0, LX/1vt;->c:I

    .line 1373678
    move v0, v0

    .line 1373679
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1373674
    iget v0, p0, LX/1vt;->c:I

    .line 1373675
    move v0, v0

    .line 1373676
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1373671
    iget v0, p0, LX/1vt;->b:I

    .line 1373672
    move v0, v0

    .line 1373673
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1373668
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1373669
    move-object v0, v0

    .line 1373670
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1373659
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1373660
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1373661
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1373662
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1373663
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1373664
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1373665
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1373666
    invoke-static {v3, v9, v2}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1373667
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1373656
    iget v0, p0, LX/1vt;->c:I

    .line 1373657
    move v0, v0

    .line 1373658
    return v0
.end method
