.class public final Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$FetchBootstrapKeywordsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1373761
    const-class v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$FetchBootstrapKeywordsModel;

    new-instance v1, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$FetchBootstrapKeywordsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$FetchBootstrapKeywordsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1373762
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1373763
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1373764
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1373765
    const/4 v2, 0x0

    .line 1373766
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1373767
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1373768
    :goto_0
    move v1, v2

    .line 1373769
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1373770
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1373771
    new-instance v1, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$FetchBootstrapKeywordsModel;

    invoke-direct {v1}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapKeywordsGraphQLModels$FetchBootstrapKeywordsModel;-><init>()V

    .line 1373772
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1373773
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1373774
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1373775
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1373776
    :cond_0
    return-object v1

    .line 1373777
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1373778
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1373779
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1373780
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1373781
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1373782
    const-string v4, "bootstrap_keywords"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1373783
    const/4 v3, 0x0

    .line 1373784
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1373785
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1373786
    :goto_2
    move v1, v3

    .line 1373787
    goto :goto_1

    .line 1373788
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1373789
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1373790
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1373791
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1373792
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 1373793
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1373794
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1373795
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1373796
    const-string v5, "edges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1373797
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1373798
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 1373799
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1373800
    const/4 v5, 0x0

    .line 1373801
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_d

    .line 1373802
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1373803
    :goto_5
    move v4, v5

    .line 1373804
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1373805
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1373806
    goto :goto_3

    .line 1373807
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1373808
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1373809
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3

    .line 1373810
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1373811
    :cond_b
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_c

    .line 1373812
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1373813
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1373814
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v6, :cond_b

    .line 1373815
    const-string p0, "node"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1373816
    invoke-static {p1, v0}, LX/8c7;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_6

    .line 1373817
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1373818
    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1373819
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_5

    :cond_d
    move v4, v5

    goto :goto_6
.end method
