.class public final Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1372977
    const-class v0, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;

    new-instance v1, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1372978
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1372993
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1372980
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1372981
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1372982
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1372983
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1372984
    if-eqz v2, :cond_0

    .line 1372985
    const-string p0, "bootstrap_entities"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1372986
    invoke-static {v1, v2, p1, p2}, LX/8c0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1372987
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1372988
    if-eqz v2, :cond_1

    .line 1372989
    const-string p0, "bootstrap_entities_delta"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1372990
    invoke-static {v1, v2, p1, p2}, LX/8by;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1372991
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1372992
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1372979
    check-cast p1, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel$Serializer;->a(Lcom/facebook/search/bootstrap/common/protocol/FetchBootstrapEntitiesGraphQLModels$FetchBootstrapEntitiesModel;LX/0nX;LX/0my;)V

    return-void
.end method
