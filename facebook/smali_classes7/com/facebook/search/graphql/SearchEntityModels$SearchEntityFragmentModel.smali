.class public final Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/7CB;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x78cbd8f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Z

.field private j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1180229
    const-class v0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1180230
    const-class v0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1180231
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1180232
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1180233
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1180234
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1180235
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1180242
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->f:Ljava/util/List;

    .line 1180243
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1180236
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1180237
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1180223
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1180224
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method private n()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1180238
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->k:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    iput-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->k:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    .line 1180239
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->k:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1180240
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->l:Ljava/lang/String;

    .line 1180241
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1180225
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->m:Ljava/lang/String;

    .line 1180226
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method private q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1180227
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1180228
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private r()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1180170
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->o:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->o:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1180171
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->o:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method private s()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1180172
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->p:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedState;

    iput-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->p:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1180173
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->p:Lcom/facebook/graphql/enums/GraphQLSavedState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 1180174
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1180175
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1180176
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1180177
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1180178
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->m()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1180179
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->n()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1180180
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1180181
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1180182
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1180183
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1180184
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->s()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 1180185
    const/16 v10, 0xc

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1180186
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 1180187
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1180188
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1180189
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1180190
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1180191
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1180192
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1180193
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1180194
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1180195
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1180196
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1180197
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1180198
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1180199
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1180200
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1180201
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->n()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1180202
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->n()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    .line 1180203
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->n()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1180204
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;

    .line 1180205
    iput-object v0, v1, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->k:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    .line 1180206
    :cond_0
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1180207
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1180208
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1180209
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;

    .line 1180210
    iput-object v0, v1, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1180211
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1180212
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1180213
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1180214
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1180215
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->h:Z

    .line 1180216
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;->i:Z

    .line 1180217
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1180218
    new-instance v0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;

    invoke-direct {v0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel;-><init>()V

    .line 1180219
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1180220
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1180221
    const v0, 0x77fd40bb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1180222
    const v0, 0x223ed590

    return v0
.end method
