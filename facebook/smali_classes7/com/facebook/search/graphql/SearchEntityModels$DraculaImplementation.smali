.class public final Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1179944
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1179945
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1179946
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1179947
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1179948
    if-nez p1, :cond_0

    .line 1179949
    :goto_0
    return v1

    .line 1179950
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1179951
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1179952
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1179953
    const v2, -0x5c2ca6a6

    const/4 v4, 0x0

    .line 1179954
    if-nez v0, :cond_1

    move v3, v4

    .line 1179955
    :goto_1
    move v0, v3

    .line 1179956
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1179957
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1179958
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1179959
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1179960
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 1179961
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1179962
    :sswitch_1
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1179963
    invoke-virtual {p0, p1, v7, v1}, LX/15i;->a(III)I

    move-result v2

    .line 1179964
    invoke-virtual {p0, p1, v8}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v3

    .line 1179965
    invoke-virtual {p3, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1179966
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1179967
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1179968
    invoke-virtual {p3, v7, v2, v1}, LX/186;->a(III)V

    .line 1179969
    invoke-virtual {p3, v8, v3}, LX/186;->b(II)V

    .line 1179970
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1179971
    :sswitch_2
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1179972
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1179973
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1179974
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    move-object v0, p3

    .line 1179975
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1179976
    invoke-virtual {p3, v7, v6}, LX/186;->b(II)V

    .line 1179977
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1179978
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v5

    .line 1179979
    if-nez v5, :cond_2

    const/4 v3, 0x0

    .line 1179980
    :goto_2
    if-ge v4, v5, :cond_3

    .line 1179981
    invoke-virtual {p0, v0, v4}, LX/15i;->q(II)I

    move-result v6

    .line 1179982
    invoke-static {p0, v6, v2, p3}, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v6

    aput v6, v3, v4

    .line 1179983
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1179984
    :cond_2
    new-array v3, v5, [I

    goto :goto_2

    .line 1179985
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x79163e77 -> :sswitch_0
        -0x5c2ca6a6 -> :sswitch_1
        -0x39de6c7c -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1180005
    if-nez p0, :cond_0

    move v0, v1

    .line 1180006
    :goto_0
    return v0

    .line 1180007
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1180008
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1180009
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1180010
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1180011
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1180012
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1180013
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1179986
    const/4 v7, 0x0

    .line 1179987
    const/4 v1, 0x0

    .line 1179988
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1179989
    invoke-static {v2, v3, v0}, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1179990
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1179991
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1179992
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1179993
    new-instance v0, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1179994
    sparse-switch p2, :sswitch_data_0

    .line 1179995
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1179996
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1179997
    const v1, -0x5c2ca6a6

    .line 1179998
    if-eqz v0, :cond_0

    .line 1179999
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1180000
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1180001
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1180002
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1180003
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1180004
    :cond_0
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x79163e77 -> :sswitch_0
        -0x5c2ca6a6 -> :sswitch_1
        -0x39de6c7c -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1179937
    if-eqz p1, :cond_0

    .line 1179938
    invoke-static {p0, p1, p2}, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;

    move-result-object v1

    .line 1179939
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;

    .line 1179940
    if-eq v0, v1, :cond_0

    .line 1179941
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1179942
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1179943
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1179935
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1179936
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1179930
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1179931
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1179932
    :cond_0
    iput-object p1, p0, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;->a:LX/15i;

    .line 1179933
    iput p2, p0, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;->b:I

    .line 1179934
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1179929
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1179928
    new-instance v0, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1179925
    iget v0, p0, LX/1vt;->c:I

    .line 1179926
    move v0, v0

    .line 1179927
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1179922
    iget v0, p0, LX/1vt;->c:I

    .line 1179923
    move v0, v0

    .line 1179924
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1179919
    iget v0, p0, LX/1vt;->b:I

    .line 1179920
    move v0, v0

    .line 1179921
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1179916
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1179917
    move-object v0, v0

    .line 1179918
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1179907
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1179908
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1179909
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1179910
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1179911
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1179912
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1179913
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1179914
    invoke-static {v3, v9, v2}, Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/search/graphql/SearchEntityModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1179915
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1179904
    iget v0, p0, LX/1vt;->c:I

    .line 1179905
    move v0, v0

    .line 1179906
    return v0
.end method
