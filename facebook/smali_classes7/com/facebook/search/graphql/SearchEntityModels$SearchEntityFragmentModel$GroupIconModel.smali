.class public final Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x179291ab
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1180112
    const-class v0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1180111
    const-class v0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1180109
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1180110
    return-void
.end method

.method private j()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1180107
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;->e:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;

    iput-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;->e:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;

    .line 1180108
    iget-object v0, p0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;->e:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1180101
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1180102
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;->j()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1180103
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1180104
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1180105
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1180106
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1180093
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1180094
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;->j()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1180095
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;->j()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;

    .line 1180096
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;->j()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1180097
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    .line 1180098
    iput-object v0, v1, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;->e:Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;

    .line 1180099
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1180100
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1180092
    invoke-direct {p0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;->j()Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel$DarkIconModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1180089
    new-instance v0, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;

    invoke-direct {v0}, Lcom/facebook/search/graphql/SearchEntityModels$SearchEntityFragmentModel$GroupIconModel;-><init>()V

    .line 1180090
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1180091
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1180087
    const v0, -0x6ab4e2d7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1180088
    const v0, -0x1afbefc0

    return v0
.end method
