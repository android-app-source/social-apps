.class public final Lcom/facebook/richdocument/utils/ActionUtils$GetRequestAsyncTask;
.super LX/3nE;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "LX/8bF;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8bG;

.field private final b:Ljava/lang/String;

.field private final c:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/8bG;Ljava/lang/String;LX/0TF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0TF",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1371844
    iput-object p1, p0, Lcom/facebook/richdocument/utils/ActionUtils$GetRequestAsyncTask;->a:LX/8bG;

    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 1371845
    iput-object p2, p0, Lcom/facebook/richdocument/utils/ActionUtils$GetRequestAsyncTask;->b:Ljava/lang/String;

    .line 1371846
    iput-object p3, p0, Lcom/facebook/richdocument/utils/ActionUtils$GetRequestAsyncTask;->c:LX/0TF;

    .line 1371847
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1371848
    iget-object v0, p0, Lcom/facebook/richdocument/utils/ActionUtils$GetRequestAsyncTask;->b:Ljava/lang/String;

    .line 1371849
    :try_start_0
    new-instance v6, Ljava/net/URL;

    invoke-direct {v6, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1371850
    new-instance v1, Ljava/net/URI;

    invoke-virtual {v6}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Ljava/net/URL;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, Ljava/net/URL;->getRef()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1371851
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 1371852
    new-instance v1, LX/3Et;

    iget-object v3, p0, Lcom/facebook/richdocument/utils/ActionUtils$GetRequestAsyncTask;->a:LX/8bG;

    iget-object v3, v3, LX/8bG;->a:LX/11M;

    invoke-direct {v1, v3}, LX/3Et;-><init>(LX/11M;)V

    .line 1371853
    new-instance v3, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v3}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    const-string v4, "http.protocol.handle-redirects"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/params/BasicHttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/http/client/methods/HttpGet;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 1371854
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v3

    const-string v4, "richdocument_async_get"

    .line 1371855
    iput-object v4, v3, LX/15E;->c:Ljava/lang/String;

    .line 1371856
    move-object v3, v3

    .line 1371857
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    .line 1371858
    iput-object v4, v3, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1371859
    move-object v3, v3

    .line 1371860
    iput-object v2, v3, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 1371861
    move-object v2, v3

    .line 1371862
    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1371863
    iput-object v3, v2, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1371864
    move-object v2, v2

    .line 1371865
    iput-object v1, v2, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 1371866
    move-object v1, v2

    .line 1371867
    invoke-virtual {v1}, LX/15E;->a()LX/15D;

    move-result-object v1

    .line 1371868
    new-instance v2, LX/8bF;

    iget-object v3, p0, Lcom/facebook/richdocument/utils/ActionUtils$GetRequestAsyncTask;->a:LX/8bG;

    iget-object v3, v3, LX/8bG;->c:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v3, v1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v2, v1}, LX/8bF;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    .line 1371869
    :goto_0
    move-object v0, v1

    .line 1371870
    return-object v0

    .line 1371871
    :catch_0
    move-exception v1

    move-object v2, v1

    .line 1371872
    iget-object v1, p0, Lcom/facebook/richdocument/utils/ActionUtils$GetRequestAsyncTask;->a:LX/8bG;

    iget-object v1, v1, LX/8bG;->d:LX/03V;

    const-string v3, "RichDocumentGetAction"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Get request to url: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " failed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1371873
    new-instance v1, LX/8bF;

    invoke-direct {v1, v2}, LX/8bF;-><init>(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1371874
    check-cast p1, LX/8bF;

    .line 1371875
    iget-object v0, p0, Lcom/facebook/richdocument/utils/ActionUtils$GetRequestAsyncTask;->c:LX/0TF;

    if-nez v0, :cond_0

    .line 1371876
    :goto_0
    return-void

    .line 1371877
    :cond_0
    iget-object v0, p1, LX/8bF;->b:Ljava/lang/Exception;

    move-object v0, v0

    .line 1371878
    if-eqz v0, :cond_1

    .line 1371879
    iget-object v0, p0, Lcom/facebook/richdocument/utils/ActionUtils$GetRequestAsyncTask;->c:LX/0TF;

    .line 1371880
    iget-object v1, p1, LX/8bF;->b:Ljava/lang/Exception;

    move-object v1, v1

    .line 1371881
    invoke-interface {v0, v1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1371882
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/utils/ActionUtils$GetRequestAsyncTask;->c:LX/0TF;

    .line 1371883
    iget-object v1, p1, LX/8bF;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1371884
    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
