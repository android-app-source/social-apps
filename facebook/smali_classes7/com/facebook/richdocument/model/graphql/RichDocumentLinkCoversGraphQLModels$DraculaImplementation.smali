.class public final Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1369005
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1369006
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1369003
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1369004
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 26

    .prologue
    .line 1368866
    if-nez p1, :cond_0

    .line 1368867
    const/4 v4, 0x0

    .line 1368868
    :goto_0
    return v4

    .line 1368869
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1368870
    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    .line 1368871
    :sswitch_0
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->p(II)I

    move-result v4

    .line 1368872
    const v5, -0x52441ac4

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v4, v5, v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v4

    .line 1368873
    const/4 v5, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1368874
    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1368875
    invoke-virtual/range {p3 .. p3}, LX/186;->d()I

    move-result v4

    goto :goto_0

    .line 1368876
    :sswitch_1
    const/4 v4, 0x0

    const-class v5, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4, v5}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v4

    invoke-static {v4}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v4

    .line 1368877
    move-object/from16 v0, p3

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1368878
    const/4 v4, 0x1

    const-class v6, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4, v6}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368879
    move-object/from16 v0, p3

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1368880
    const/4 v4, 0x2

    const-class v7, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4, v7}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368881
    move-object/from16 v0, p3

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1368882
    const/4 v4, 0x3

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->p(II)I

    move-result v4

    .line 1368883
    const v8, 0x2ddd5715

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v4, v8, v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v8

    .line 1368884
    const/4 v4, 0x4

    const-class v9, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4, v9}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368885
    move-object/from16 v0, p3

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1368886
    const/4 v4, 0x5

    const-class v10, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4, v10}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v4

    invoke-static {v4}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v4

    .line 1368887
    move-object/from16 v0, p3

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v10

    .line 1368888
    const/4 v4, 0x6

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->p(II)I

    move-result v4

    .line 1368889
    const v11, 0x2ddd5715

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v4, v11, v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v11

    .line 1368890
    const/4 v4, 0x7

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1368891
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1368892
    const/16 v4, 0x8

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->p(II)I

    move-result v4

    .line 1368893
    const v13, -0x57707d7b

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v4, v13, v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v13

    .line 1368894
    const/16 v4, 0x9

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->p(II)I

    move-result v4

    .line 1368895
    const v14, 0x2ddd5715

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v4, v14, v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v14

    .line 1368896
    const/16 v4, 0xa

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1368897
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 1368898
    const/16 v4, 0xb

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1368899
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 1368900
    const/16 v4, 0xc

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->b(II)Z

    move-result v17

    .line 1368901
    const/16 v4, 0xd

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->b(II)Z

    move-result v18

    .line 1368902
    const/16 v4, 0xe

    const-class v19, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v4, v2}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368903
    move-object/from16 v0, p3

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1368904
    const/16 v19, 0xf

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1368905
    const/16 v19, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1368906
    const/4 v5, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v6}, LX/186;->b(II)V

    .line 1368907
    const/4 v5, 0x2

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v7}, LX/186;->b(II)V

    .line 1368908
    const/4 v5, 0x3

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v8}, LX/186;->b(II)V

    .line 1368909
    const/4 v5, 0x4

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v9}, LX/186;->b(II)V

    .line 1368910
    const/4 v5, 0x5

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v10}, LX/186;->b(II)V

    .line 1368911
    const/4 v5, 0x6

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v11}, LX/186;->b(II)V

    .line 1368912
    const/4 v5, 0x7

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v12}, LX/186;->b(II)V

    .line 1368913
    const/16 v5, 0x8

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v13}, LX/186;->b(II)V

    .line 1368914
    const/16 v5, 0x9

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v14}, LX/186;->b(II)V

    .line 1368915
    const/16 v5, 0xa

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v15}, LX/186;->b(II)V

    .line 1368916
    const/16 v5, 0xb

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1368917
    const/16 v5, 0xc

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1368918
    const/16 v5, 0xd

    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1368919
    const/16 v5, 0xe

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1368920
    invoke-virtual/range {p3 .. p3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    .line 1368921
    :sswitch_2
    const/4 v4, 0x0

    const-class v5, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4, v5}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v4

    invoke-static {v4}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v4

    .line 1368922
    move-object/from16 v0, p3

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1368923
    const/4 v4, 0x1

    const-class v6, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4, v6}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368924
    move-object/from16 v0, p3

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1368925
    const/4 v4, 0x2

    const-class v7, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4, v7}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368926
    move-object/from16 v0, p3

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1368927
    const/4 v4, 0x3

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->p(II)I

    move-result v4

    .line 1368928
    const v8, 0x2ddd5715

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v4, v8, v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v8

    .line 1368929
    const/4 v4, 0x4

    const-class v9, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4, v9}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368930
    move-object/from16 v0, p3

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1368931
    const/4 v4, 0x5

    const-class v10, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4, v10}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v4

    invoke-static {v4}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v4

    .line 1368932
    move-object/from16 v0, p3

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v10

    .line 1368933
    const/4 v4, 0x6

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->p(II)I

    move-result v4

    .line 1368934
    const v11, 0x2ddd5715

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v4, v11, v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v11

    .line 1368935
    const/4 v4, 0x7

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1368936
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1368937
    const/16 v4, 0x8

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->p(II)I

    move-result v4

    .line 1368938
    const v13, 0x2ddd5715

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v4, v13, v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v13

    .line 1368939
    const/16 v4, 0x9

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1368940
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1368941
    const/16 v4, 0xa

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1368942
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 1368943
    const/16 v4, 0xb

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->b(II)Z

    move-result v16

    .line 1368944
    const/16 v4, 0xc

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->b(II)Z

    move-result v17

    .line 1368945
    const/16 v4, 0xd

    const-class v18, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v4, v2}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368946
    move-object/from16 v0, p3

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1368947
    const/16 v18, 0xe

    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1368948
    const/16 v18, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1368949
    const/4 v5, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v6}, LX/186;->b(II)V

    .line 1368950
    const/4 v5, 0x2

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v7}, LX/186;->b(II)V

    .line 1368951
    const/4 v5, 0x3

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v8}, LX/186;->b(II)V

    .line 1368952
    const/4 v5, 0x4

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v9}, LX/186;->b(II)V

    .line 1368953
    const/4 v5, 0x5

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v10}, LX/186;->b(II)V

    .line 1368954
    const/4 v5, 0x6

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v11}, LX/186;->b(II)V

    .line 1368955
    const/4 v5, 0x7

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v12}, LX/186;->b(II)V

    .line 1368956
    const/16 v5, 0x8

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v13}, LX/186;->b(II)V

    .line 1368957
    const/16 v5, 0x9

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v14}, LX/186;->b(II)V

    .line 1368958
    const/16 v5, 0xa

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v15}, LX/186;->b(II)V

    .line 1368959
    const/16 v5, 0xb

    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1368960
    const/16 v5, 0xc

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1368961
    const/16 v5, 0xd

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1368962
    invoke-virtual/range {p3 .. p3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    .line 1368963
    :sswitch_3
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1368964
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1368965
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1368966
    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1368967
    const/4 v6, 0x2

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 1368968
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1368969
    const/4 v6, 0x3

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 1368970
    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1368971
    const/4 v6, 0x4

    const-wide/16 v10, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1368972
    const/4 v10, 0x5

    const-wide/16 v12, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v10, v12, v13}, LX/15i;->a(IID)D

    move-result-wide v10

    .line 1368973
    const/4 v12, 0x6

    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v12, v14, v15}, LX/15i;->a(IID)D

    move-result-wide v12

    .line 1368974
    const/4 v14, 0x7

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v14}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v14

    .line 1368975
    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1368976
    const/16 v15, 0x8

    const-wide/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-wide/from16 v2, v16

    invoke-virtual {v0, v1, v15, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v16

    .line 1368977
    const/16 v15, 0x9

    const-wide/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-wide/from16 v2, v18

    invoke-virtual {v0, v1, v15, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v18

    .line 1368978
    const/16 v15, 0xa

    const-wide/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-wide/from16 v2, v20

    invoke-virtual {v0, v1, v15, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v20

    .line 1368979
    const/16 v15, 0xb

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v0, v1, v15}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v15

    .line 1368980
    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 1368981
    const/16 v22, 0xc

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/15i;->b(II)Z

    move-result v22

    .line 1368982
    const/16 v23, 0xd

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v23

    .line 1368983
    move-object/from16 v0, p3

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 1368984
    const/16 v24, 0xe

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v24

    .line 1368985
    move-object/from16 v0, p3

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    .line 1368986
    const/16 v25, 0xf

    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1368987
    const/16 v25, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1368988
    const/4 v4, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 1368989
    const/4 v4, 0x2

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 1368990
    const/4 v4, 0x3

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 1368991
    const/4 v5, 0x4

    const-wide/16 v8, 0x0

    move-object/from16 v4, p3

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 1368992
    const/4 v5, 0x5

    const-wide/16 v8, 0x0

    move-object/from16 v4, p3

    move-wide v6, v10

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 1368993
    const/4 v5, 0x6

    const-wide/16 v8, 0x0

    move-object/from16 v4, p3

    move-wide v6, v12

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 1368994
    const/4 v4, 0x7

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 1368995
    const/16 v5, 0x8

    const-wide/16 v8, 0x0

    move-object/from16 v4, p3

    move-wide/from16 v6, v16

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 1368996
    const/16 v5, 0x9

    const-wide/16 v8, 0x0

    move-object/from16 v4, p3

    move-wide/from16 v6, v18

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 1368997
    const/16 v5, 0xa

    const-wide/16 v8, 0x0

    move-object/from16 v4, p3

    move-wide/from16 v6, v20

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IDD)V

    .line 1368998
    const/16 v4, 0xb

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 1368999
    const/16 v4, 0xc

    move-object/from16 v0, p3

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1369000
    const/16 v4, 0xd

    move-object/from16 v0, p3

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1369001
    const/16 v4, 0xe

    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1369002
    invoke-virtual/range {p3 .. p3}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x57707d7b -> :sswitch_2
        -0x52441ac4 -> :sswitch_1
        0x19c6aae3 -> :sswitch_0
        0x2ddd5715 -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1368865
    new-instance v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1368861
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1368862
    if-eqz v0, :cond_0

    .line 1368863
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1368864
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1368856
    if-eqz p0, :cond_0

    .line 1368857
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1368858
    if-eq v0, p0, :cond_0

    .line 1368859
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1368860
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    const v2, 0x2ddd5715

    .line 1368813
    sparse-switch p2, :sswitch_data_0

    .line 1368814
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1368815
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1368816
    const v1, -0x52441ac4

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1368817
    :goto_0
    :sswitch_1
    return-void

    .line 1368818
    :sswitch_2
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1368819
    invoke-static {v0, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1368820
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368821
    invoke-static {v0, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1368822
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368823
    invoke-static {v0, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1368824
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v0

    .line 1368825
    invoke-static {p0, v0, v2, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1368826
    const/4 v0, 0x4

    const-class v1, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368827
    invoke-static {v0, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1368828
    const/4 v0, 0x5

    const-class v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1368829
    invoke-static {v0, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1368830
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1368831
    invoke-static {p0, v0, v2, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1368832
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1368833
    const v1, -0x57707d7b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1368834
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1368835
    invoke-static {p0, v0, v2, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1368836
    const/16 v0, 0xe

    const-class v1, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368837
    invoke-static {v0, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 1368838
    :sswitch_3
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1368839
    invoke-static {v0, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1368840
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368841
    invoke-static {v0, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1368842
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368843
    invoke-static {v0, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1368844
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v0

    .line 1368845
    invoke-static {p0, v0, v2, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1368846
    const/4 v0, 0x4

    const-class v1, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368847
    invoke-static {v0, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1368848
    const/4 v0, 0x5

    const-class v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentFontResourceModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1368849
    invoke-static {v0, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1368850
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1368851
    invoke-static {p0, v0, v2, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1368852
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1368853
    invoke-static {p0, v0, v2, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1368854
    const/16 v0, 0xd

    const-class v1, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$RichDocumentNonTextConfigModel;

    .line 1368855
    invoke-static {v0, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x57707d7b -> :sswitch_3
        -0x52441ac4 -> :sswitch_2
        0x19c6aae3 -> :sswitch_0
        0x2ddd5715 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1368807
    if-eqz p1, :cond_0

    .line 1368808
    invoke-static {p0, p1, p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1368809
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;

    .line 1368810
    if-eq v0, v1, :cond_0

    .line 1368811
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1368812
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1368806
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1369007
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1369008
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1368801
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1368802
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1368803
    :cond_0
    iput-object p1, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1368804
    iput p2, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->b:I

    .line 1368805
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1368800
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1368799
    new-instance v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1368796
    iget v0, p0, LX/1vt;->c:I

    .line 1368797
    move v0, v0

    .line 1368798
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1368793
    iget v0, p0, LX/1vt;->c:I

    .line 1368794
    move v0, v0

    .line 1368795
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1368790
    iget v0, p0, LX/1vt;->b:I

    .line 1368791
    move v0, v0

    .line 1368792
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1368787
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1368788
    move-object v0, v0

    .line 1368789
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1368778
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1368779
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1368780
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1368781
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1368782
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1368783
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1368784
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1368785
    invoke-static {v3, v9, v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1368786
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1368775
    iget v0, p0, LX/1vt;->c:I

    .line 1368776
    move v0, v0

    .line 1368777
    return v0
.end method
