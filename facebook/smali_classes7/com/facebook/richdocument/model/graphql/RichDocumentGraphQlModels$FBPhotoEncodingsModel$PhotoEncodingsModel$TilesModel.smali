.class public final Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1ba8ca18
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1357232
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1357231
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1357229
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1357230
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1357227
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1357228
    iget v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1357217
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1357218
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1357219
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1357220
    iget v1, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->e:I

    invoke-virtual {p1, v3, v1, v3}, LX/186;->a(III)V

    .line 1357221
    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->f:I

    invoke-virtual {p1, v1, v2, v3}, LX/186;->a(III)V

    .line 1357222
    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->g:I

    invoke-virtual {p1, v1, v2, v3}, LX/186;->a(III)V

    .line 1357223
    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->h:I

    invoke-virtual {p1, v1, v2, v3}, LX/186;->a(III)V

    .line 1357224
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1357225
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1357226
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1357214
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1357215
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1357216
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1357208
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1357209
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->e:I

    .line 1357210
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->f:I

    .line 1357211
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->g:I

    .line 1357212
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->h:I

    .line 1357213
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1357206
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1357207
    iget v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1357195
    new-instance v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;

    invoke-direct {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;-><init>()V

    .line 1357196
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1357197
    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1357204
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1357205
    iget v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->g:I

    return v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 1357202
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1357203
    iget v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->h:I

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1357201
    const v0, -0x40db39a1

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1357199
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->i:Ljava/lang/String;

    .line 1357200
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel$TilesModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1357198
    const v0, 0x44e6bbe0

    return v0
.end method
