.class public final Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$InstantArticleLinkCoverConfigFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$InstantArticleLinkCoverConfigFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1369190
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$InstantArticleLinkCoverConfigFragmentModel;

    new-instance v1, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$InstantArticleLinkCoverConfigFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$InstantArticleLinkCoverConfigFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1369191
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1369189
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$InstantArticleLinkCoverConfigFragmentModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1369052
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1369053
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1369054
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1369055
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1369056
    if-eqz v2, :cond_0

    .line 1369057
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369058
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1369059
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1369060
    if-eqz v2, :cond_1f

    .line 1369061
    const-string v3, "latest_version"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369062
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1369063
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1369064
    if-eqz v3, :cond_1e

    .line 1369065
    const-string p0, "feed_cover_config"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369066
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1369067
    const/4 p0, 0x0

    invoke-virtual {v1, v3, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1369068
    if-eqz p0, :cond_1

    .line 1369069
    const-string v0, "bar_configs"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369070
    invoke-static {v1, p0, p1, p2}, LX/8av;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1369071
    :cond_1
    const/4 p0, 0x1

    invoke-virtual {v1, v3, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1369072
    if-eqz p0, :cond_2

    .line 1369073
    const-string v0, "border_config"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369074
    invoke-static {v1, p0, p1}, LX/8av;->a(LX/15i;ILX/0nX;)V

    .line 1369075
    :cond_2
    const/4 p0, 0x2

    invoke-virtual {v1, v3, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1369076
    if-eqz p0, :cond_3

    .line 1369077
    const-string v0, "byline_area_config"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369078
    invoke-static {v1, p0, p1}, LX/8av;->a(LX/15i;ILX/0nX;)V

    .line 1369079
    :cond_3
    const/4 p0, 0x3

    invoke-virtual {v1, v3, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1369080
    if-eqz p0, :cond_4

    .line 1369081
    const-string v0, "byline_config"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369082
    invoke-static {v1, p0, p1}, LX/8aw;->a(LX/15i;ILX/0nX;)V

    .line 1369083
    :cond_4
    const/4 p0, 0x4

    invoke-virtual {v1, v3, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1369084
    if-eqz p0, :cond_5

    .line 1369085
    const-string v0, "cover_image_config"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369086
    invoke-static {v1, p0, p1}, LX/8av;->a(LX/15i;ILX/0nX;)V

    .line 1369087
    :cond_5
    const/4 p0, 0x5

    invoke-virtual {v1, v3, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1369088
    if-eqz p0, :cond_6

    .line 1369089
    const-string v0, "custom_fonts"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369090
    invoke-static {v1, p0, p1, p2}, LX/8a1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1369091
    :cond_6
    const/4 p0, 0x6

    invoke-virtual {v1, v3, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1369092
    if-eqz p0, :cond_7

    .line 1369093
    const-string v0, "description_config"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369094
    invoke-static {v1, p0, p1}, LX/8aw;->a(LX/15i;ILX/0nX;)V

    .line 1369095
    :cond_7
    const/4 p0, 0x7

    invoke-virtual {v1, v3, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1369096
    if-eqz p0, :cond_8

    .line 1369097
    const-string v0, "device_family"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369098
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1369099
    :cond_8
    const/16 p0, 0x8

    invoke-virtual {v1, v3, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1369100
    if-eqz p0, :cond_17

    .line 1369101
    const-string v0, "fallback_feed_style"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369102
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1369103
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1369104
    if-eqz v0, :cond_9

    .line 1369105
    const-string v2, "bar_configs"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369106
    invoke-static {v1, v0, p1, p2}, LX/8av;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1369107
    :cond_9
    const/4 v0, 0x1

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1369108
    if-eqz v0, :cond_a

    .line 1369109
    const-string v2, "border_config"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369110
    invoke-static {v1, v0, p1}, LX/8av;->a(LX/15i;ILX/0nX;)V

    .line 1369111
    :cond_a
    const/4 v0, 0x2

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1369112
    if-eqz v0, :cond_b

    .line 1369113
    const-string v2, "byline_area_config"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369114
    invoke-static {v1, v0, p1}, LX/8av;->a(LX/15i;ILX/0nX;)V

    .line 1369115
    :cond_b
    const/4 v0, 0x3

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1369116
    if-eqz v0, :cond_c

    .line 1369117
    const-string v2, "byline_config"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369118
    invoke-static {v1, v0, p1}, LX/8aw;->a(LX/15i;ILX/0nX;)V

    .line 1369119
    :cond_c
    const/4 v0, 0x4

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1369120
    if-eqz v0, :cond_d

    .line 1369121
    const-string v2, "cover_image_config"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369122
    invoke-static {v1, v0, p1}, LX/8av;->a(LX/15i;ILX/0nX;)V

    .line 1369123
    :cond_d
    const/4 v0, 0x5

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1369124
    if-eqz v0, :cond_e

    .line 1369125
    const-string v2, "custom_fonts"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369126
    invoke-static {v1, v0, p1, p2}, LX/8a1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1369127
    :cond_e
    const/4 v0, 0x6

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1369128
    if-eqz v0, :cond_f

    .line 1369129
    const-string v2, "description_config"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369130
    invoke-static {v1, v0, p1}, LX/8aw;->a(LX/15i;ILX/0nX;)V

    .line 1369131
    :cond_f
    const/4 v0, 0x7

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1369132
    if-eqz v0, :cond_10

    .line 1369133
    const-string v2, "device_family"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369134
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1369135
    :cond_10
    const/16 v0, 0x8

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1369136
    if-eqz v0, :cond_11

    .line 1369137
    const-string v2, "headline_config"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369138
    invoke-static {v1, v0, p1}, LX/8aw;->a(LX/15i;ILX/0nX;)V

    .line 1369139
    :cond_11
    const/16 v0, 0x9

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1369140
    if-eqz v0, :cond_12

    .line 1369141
    const-string v2, "layout_spec_version"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369142
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1369143
    :cond_12
    const/16 v0, 0xa

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1369144
    if-eqz v0, :cond_13

    .line 1369145
    const-string v2, "page_id"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369146
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1369147
    :cond_13
    const/16 v0, 0xb

    invoke-virtual {v1, p0, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1369148
    if-eqz v0, :cond_14

    .line 1369149
    const-string v2, "show_bottom_gradient"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369150
    invoke-virtual {p1, v0}, LX/0nX;->a(Z)V

    .line 1369151
    :cond_14
    const/16 v0, 0xc

    invoke-virtual {v1, p0, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1369152
    if-eqz v0, :cond_15

    .line 1369153
    const-string v2, "show_top_gradient"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369154
    invoke-virtual {p1, v0}, LX/0nX;->a(Z)V

    .line 1369155
    :cond_15
    const/16 v0, 0xd

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1369156
    if-eqz v0, :cond_16

    .line 1369157
    const-string v2, "source_image_config"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369158
    invoke-static {v1, v0, p1}, LX/8av;->a(LX/15i;ILX/0nX;)V

    .line 1369159
    :cond_16
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1369160
    :cond_17
    const/16 p0, 0x9

    invoke-virtual {v1, v3, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1369161
    if-eqz p0, :cond_18

    .line 1369162
    const-string v0, "headline_config"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369163
    invoke-static {v1, p0, p1}, LX/8aw;->a(LX/15i;ILX/0nX;)V

    .line 1369164
    :cond_18
    const/16 p0, 0xa

    invoke-virtual {v1, v3, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1369165
    if-eqz p0, :cond_19

    .line 1369166
    const-string v0, "layout_spec_version"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369167
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1369168
    :cond_19
    const/16 p0, 0xb

    invoke-virtual {v1, v3, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1369169
    if-eqz p0, :cond_1a

    .line 1369170
    const-string v0, "page_id"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369171
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1369172
    :cond_1a
    const/16 p0, 0xc

    invoke-virtual {v1, v3, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1369173
    if-eqz p0, :cond_1b

    .line 1369174
    const-string v0, "show_bottom_gradient"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369175
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1369176
    :cond_1b
    const/16 p0, 0xd

    invoke-virtual {v1, v3, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1369177
    if-eqz p0, :cond_1c

    .line 1369178
    const-string v0, "show_top_gradient"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369179
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1369180
    :cond_1c
    const/16 p0, 0xe

    invoke-virtual {v1, v3, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1369181
    if-eqz p0, :cond_1d

    .line 1369182
    const-string v0, "source_image_config"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1369183
    invoke-static {v1, p0, p1}, LX/8av;->a(LX/15i;ILX/0nX;)V

    .line 1369184
    :cond_1d
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1369185
    :cond_1e
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1369186
    :cond_1f
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1369187
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1369188
    check-cast p1, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$InstantArticleLinkCoverConfigFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$InstantArticleLinkCoverConfigFragmentModel$Serializer;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentLinkCoversGraphQLModels$InstantArticleLinkCoverConfigFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
