.class public final Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1356798
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1356799
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1356796
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1356797
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1356774
    if-nez p1, :cond_0

    move v0, v1

    .line 1356775
    :goto_0
    return v0

    .line 1356776
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1356777
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1356778
    :sswitch_0
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1356779
    invoke-virtual {p0, p1, v4, v1}, LX/15i;->a(III)I

    move-result v2

    .line 1356780
    const-class v3, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBAggregatedEntitiesAtRangeModel$SampleEntitiesModel;

    invoke-virtual {p0, p1, v5, v3}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v3

    invoke-static {v3}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v3

    .line 1356781
    invoke-static {p3, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 1356782
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1356783
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1356784
    invoke-virtual {p3, v4, v2, v1}, LX/186;->a(III)V

    .line 1356785
    invoke-virtual {p3, v5, v3}, LX/186;->b(II)V

    .line 1356786
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1356787
    :sswitch_1
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel$RangesModel$EntityModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel$RangesModel$EntityModel;

    .line 1356788
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1356789
    invoke-virtual {p0, p1, v4, v1}, LX/15i;->a(III)I

    move-result v2

    .line 1356790
    invoke-virtual {p0, p1, v5, v1}, LX/15i;->a(III)I

    move-result v3

    .line 1356791
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1356792
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1356793
    invoke-virtual {p3, v4, v2, v1}, LX/186;->a(III)V

    .line 1356794
    invoke-virtual {p3, v5, v3, v1}, LX/186;->a(III)V

    .line 1356795
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6f6e111 -> :sswitch_1
        0x6f35c8aa -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1356765
    if-nez p0, :cond_0

    move v0, v1

    .line 1356766
    :goto_0
    return v0

    .line 1356767
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1356768
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1356769
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1356770
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1356771
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1356772
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1356773
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1356758
    const/4 v7, 0x0

    .line 1356759
    const/4 v1, 0x0

    .line 1356760
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1356761
    invoke-static {v2, v3, v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1356762
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1356763
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1356764
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1356757
    new-instance v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1356753
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1356754
    if-eqz v0, :cond_0

    .line 1356755
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1356756
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1356734
    if-eqz p0, :cond_0

    .line 1356735
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1356736
    if-eq v0, p0, :cond_0

    .line 1356737
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1356738
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1356746
    sparse-switch p2, :sswitch_data_0

    .line 1356747
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1356748
    :sswitch_0
    const/4 v0, 0x2

    const-class v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBAggregatedEntitiesAtRangeModel$SampleEntitiesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1356749
    invoke-static {v0, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1356750
    :goto_0
    return-void

    .line 1356751
    :sswitch_1
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel$RangesModel$EntityModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel$RangesModel$EntityModel;

    .line 1356752
    invoke-static {v0, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6f6e111 -> :sswitch_1
        0x6f35c8aa -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1356745
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1356800
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1356801
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1356740
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1356741
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1356742
    :cond_0
    iput-object p1, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->a:LX/15i;

    .line 1356743
    iput p2, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->b:I

    .line 1356744
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1356739
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1356733
    new-instance v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1356730
    iget v0, p0, LX/1vt;->c:I

    .line 1356731
    move v0, v0

    .line 1356732
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1356727
    iget v0, p0, LX/1vt;->c:I

    .line 1356728
    move v0, v0

    .line 1356729
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1356724
    iget v0, p0, LX/1vt;->b:I

    .line 1356725
    move v0, v0

    .line 1356726
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1356721
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1356722
    move-object v0, v0

    .line 1356723
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1356712
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1356713
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1356714
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1356715
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1356716
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1356717
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1356718
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1356719
    invoke-static {v3, v9, v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1356720
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1356709
    iget v0, p0, LX/1vt;->c:I

    .line 1356710
    move v0, v0

    .line 1356711
    return v0
.end method
