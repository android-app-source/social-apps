.class public final Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x332c3782
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1358118
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1358119
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1358120
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1358121
    return-void
.end method

.method private a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1358122
    iput-object p1, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    .line 1358123
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1358124
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1358125
    if-eqz v0, :cond_0

    .line 1358126
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1358127
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1358128
    iput-object p1, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    .line 1358129
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1358130
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1358131
    if-eqz v0, :cond_0

    .line 1358132
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1358133
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1358134
    iput-object p1, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->h:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;

    .line 1358135
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1358136
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1358137
    if-eqz v0, :cond_0

    .line 1358138
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1358139
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1358050
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    iput-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    .line 1358051
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1358140
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    iput-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    .line 1358141
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1358142
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->h:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;

    iput-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->h:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;

    .line 1358143
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->h:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1358144
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1358145
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1358146
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1358147
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1358148
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1358149
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1358150
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1358151
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1358152
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1358153
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1358154
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1358155
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1358100
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1358101
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1358102
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    .line 1358103
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1358104
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;

    .line 1358105
    iput-object v0, v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    .line 1358106
    :cond_0
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1358107
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    .line 1358108
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1358109
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;

    .line 1358110
    iput-object v0, v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    .line 1358111
    :cond_1
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1358112
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;

    .line 1358113
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1358114
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;

    .line 1358115
    iput-object v0, v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->h:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;

    .line 1358116
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1358117
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1358156
    new-instance v0, LX/8Z9;

    invoke-direct {v0, p1}, LX/8Z9;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1358098
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->e:Ljava/lang/String;

    .line 1358099
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1358084
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1358085
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    move-result-object v0

    .line 1358086
    if-eqz v0, :cond_1

    .line 1358087
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1358088
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1358089
    iput v2, p2, LX/18L;->c:I

    .line 1358090
    :goto_0
    return-void

    .line 1358091
    :cond_0
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1358092
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    move-result-object v0

    .line 1358093
    if-eqz v0, :cond_1

    .line 1358094
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1358095
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1358096
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1358097
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1358077
    const-string v0, "reactors"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1358078
    check-cast p2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    invoke-direct {p0, p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;)V

    .line 1358079
    :cond_0
    :goto_0
    return-void

    .line 1358080
    :cond_1
    const-string v0, "top_level_comments"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1358081
    check-cast p2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    invoke-direct {p0, p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;)V

    goto :goto_0

    .line 1358082
    :cond_2
    const-string v0, "top_reactions"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1358083
    check-cast p2, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;

    invoke-direct {p0, p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1358060
    const-string v0, "reactors.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1358061
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    move-result-object v0

    .line 1358062
    if-eqz v0, :cond_0

    .line 1358063
    if-eqz p3, :cond_1

    .line 1358064
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    .line 1358065
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;->a(I)V

    .line 1358066
    iput-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->f:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    .line 1358067
    :cond_0
    :goto_0
    return-void

    .line 1358068
    :cond_1
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;->a(I)V

    goto :goto_0

    .line 1358069
    :cond_2
    const-string v0, "top_level_comments.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1358070
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    move-result-object v0

    .line 1358071
    if-eqz v0, :cond_0

    .line 1358072
    if-eqz p3, :cond_3

    .line 1358073
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    .line 1358074
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;->a(I)V

    .line 1358075
    iput-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->g:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    goto :goto_0

    .line 1358076
    :cond_3
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;->a(I)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1358057
    new-instance v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;

    invoke-direct {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;-><init>()V

    .line 1358058
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1358059
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1358056
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$ReactorsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1358055
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopLevelCommentsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1358054
    invoke-direct {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RelatedArticleBlingBarSummaryModel$TopReactionsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1358053
    const v0, -0xdcaf3a6    # -3.58603E30f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1358052
    const v0, -0x78fb05b

    return v0
.end method
