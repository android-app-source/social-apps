.class public final Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/174;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x18911b28
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1357568
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1357569
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1357570
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1357571
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1357616
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1357617
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1357618
    return-void
.end method

.method public static a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;)Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;
    .locals 15
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1357572
    if-nez p0, :cond_0

    .line 1357573
    const/4 p0, 0x0

    .line 1357574
    :goto_0
    return-object p0

    .line 1357575
    :cond_0
    instance-of v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;

    if-eqz v0, :cond_1

    .line 1357576
    check-cast p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;

    goto :goto_0

    .line 1357577
    :cond_1
    new-instance v2, LX/8Z8;

    invoke-direct {v2}, LX/8Z8;-><init>()V

    .line 1357578
    invoke-static {}, LX/2uF;->j()LX/3Si;

    move-result-object v3

    move v0, v1

    .line 1357579
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->b()LX/2uF;

    move-result-object v4

    invoke-virtual {v4}, LX/39O;->c()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 1357580
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->b()LX/2uF;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/2uF;->a(I)LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v6, v4, LX/1vs;->b:I

    const v4, 0x6f35c8aa

    invoke-virtual {v3, v5, v6, v4}, LX/3Si;->c(LX/15i;II)LX/3Si;

    .line 1357581
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1357582
    :cond_2
    invoke-virtual {v3}, LX/3Si;->a()LX/2uF;

    move-result-object v0

    iput-object v0, v2, LX/8Z8;->a:LX/2uF;

    .line 1357583
    invoke-static {}, LX/2uF;->j()LX/3Si;

    move-result-object v0

    .line 1357584
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->c()LX/2uF;

    move-result-object v3

    invoke-virtual {v3}, LX/39O;->c()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 1357585
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->c()LX/2uF;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    const v3, -0x6f6e111

    invoke-virtual {v0, v4, v5, v3}, LX/3Si;->c(LX/15i;II)LX/3Si;

    .line 1357586
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1357587
    :cond_3
    invoke-virtual {v0}, LX/3Si;->a()LX/2uF;

    move-result-object v0

    iput-object v0, v2, LX/8Z8;->b:LX/2uF;

    .line 1357588
    invoke-interface {p0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/8Z8;->c:Ljava/lang/String;

    .line 1357589
    const/4 v11, 0x1

    const/4 v14, 0x0

    const/4 v9, 0x0

    .line 1357590
    new-instance v7, LX/186;

    const/16 v8, 0x80

    invoke-direct {v7, v8}, LX/186;-><init>(I)V

    .line 1357591
    iget-object v8, v2, LX/8Z8;->a:LX/2uF;

    invoke-static {v8, v7}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v8

    .line 1357592
    iget-object v10, v2, LX/8Z8;->b:LX/2uF;

    invoke-static {v10, v7}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v10

    .line 1357593
    iget-object v12, v2, LX/8Z8;->c:Ljava/lang/String;

    invoke-virtual {v7, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1357594
    const/4 v13, 0x3

    invoke-virtual {v7, v13}, LX/186;->c(I)V

    .line 1357595
    invoke-virtual {v7, v14, v8}, LX/186;->b(II)V

    .line 1357596
    invoke-virtual {v7, v11, v10}, LX/186;->b(II)V

    .line 1357597
    const/4 v8, 0x2

    invoke-virtual {v7, v8, v12}, LX/186;->b(II)V

    .line 1357598
    invoke-virtual {v7}, LX/186;->d()I

    move-result v8

    .line 1357599
    invoke-virtual {v7, v8}, LX/186;->d(I)V

    .line 1357600
    invoke-virtual {v7}, LX/186;->e()[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 1357601
    invoke-virtual {v8, v14}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1357602
    new-instance v7, LX/15i;

    move-object v10, v9

    move-object v12, v9

    invoke-direct/range {v7 .. v12}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1357603
    new-instance v8, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;

    invoke-direct {v8, v7}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;-><init>(LX/15i;)V

    .line 1357604
    move-object p0, v8

    .line 1357605
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1357606
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1357607
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->b()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 1357608
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->c()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v1

    .line 1357609
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1357610
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1357611
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1357612
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1357613
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1357614
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1357615
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1357555
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1357556
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->b()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1357557
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->b()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1357558
    if-eqz v1, :cond_0

    .line 1357559
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;

    .line 1357560
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->e:LX/3Sb;

    .line 1357561
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->c()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1357562
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->c()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1357563
    if-eqz v1, :cond_1

    .line 1357564
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;

    .line 1357565
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->f:LX/3Sb;

    .line 1357566
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1357567
    if-nez v0, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1357544
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->g:Ljava/lang/String;

    .line 1357545
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final b()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAggregatedRanges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1357553
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, 0x6f35c8aa

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->e:LX/3Sb;

    .line 1357554
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1357550
    new-instance v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;

    invoke-direct {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;-><init>()V

    .line 1357551
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1357552
    return-object v0
.end method

.method public final c()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRanges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1357548
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->f:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x1

    const v4, -0x6f6e111

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->f:LX/3Sb;

    .line 1357549
    iget-object v0, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBTextWithEntitiesModel;->f:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1357547
    const v0, 0x3c8a0fa4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1357546
    const v0, -0x726d476c

    return v0
.end method
