.class public final Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$RichDocumentSubscriptionActionRejectedModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1371658
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$RichDocumentSubscriptionActionRejectedModel;

    new-instance v1, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$RichDocumentSubscriptionActionRejectedModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$RichDocumentSubscriptionActionRejectedModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1371659
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1371660
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1371661
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1371662
    const/4 v2, 0x0

    .line 1371663
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1371664
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1371665
    :goto_0
    move v1, v2

    .line 1371666
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1371667
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1371668
    new-instance v1, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$RichDocumentSubscriptionActionRejectedModel;

    invoke-direct {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$RichDocumentSubscriptionActionRejectedModel;-><init>()V

    .line 1371669
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1371670
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1371671
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1371672
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1371673
    :cond_0
    return-object v1

    .line 1371674
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1371675
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1371676
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1371677
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1371678
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, p0, :cond_2

    if-eqz v3, :cond_2

    .line 1371679
    const-string v4, "subscription_user_action"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1371680
    const/4 v3, 0x0

    .line 1371681
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1371682
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1371683
    :goto_2
    move v1, v3

    .line 1371684
    goto :goto_1

    .line 1371685
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1371686
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1371687
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1371688
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1371689
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_7

    .line 1371690
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1371691
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1371692
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v4, :cond_6

    .line 1371693
    const-string p0, "user_action"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1371694
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    goto :goto_3

    .line 1371695
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1371696
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1371697
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v1, v3

    goto :goto_3
.end method
