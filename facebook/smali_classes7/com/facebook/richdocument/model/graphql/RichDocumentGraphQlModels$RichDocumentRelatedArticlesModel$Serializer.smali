.class public final Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticlesModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticlesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1362076
    const-class v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticlesModel;

    new-instance v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticlesModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticlesModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1362077
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1362078
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticlesModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1362079
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1362080
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x4

    const/4 v4, 0x1

    .line 1362081
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1362082
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1362083
    if-eqz v2, :cond_0

    .line 1362084
    const-string v3, "block_title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1362085
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1362086
    :cond_0
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1362087
    if-eqz v2, :cond_1

    .line 1362088
    const-string v2, "document_element_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1362089
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1362090
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1362091
    if-eqz v2, :cond_2

    .line 1362092
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1362093
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1362094
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1362095
    if-eqz v2, :cond_3

    .line 1362096
    const-string v3, "related_article_objs"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1362097
    invoke-static {v1, v2, p1, p2}, LX/8aT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1362098
    :cond_3
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1362099
    if-eqz v2, :cond_4

    .line 1362100
    const-string v2, "visual_style"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1362101
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1362102
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1362103
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1362104
    check-cast p1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticlesModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticlesModel$Serializer;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentRelatedArticlesModel;LX/0nX;LX/0my;)V

    return-void
.end method
