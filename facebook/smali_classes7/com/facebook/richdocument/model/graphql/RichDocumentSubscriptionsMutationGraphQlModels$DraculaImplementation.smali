.class public final Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1371567
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1371568
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1371565
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1371566
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1371546
    if-nez p1, :cond_0

    .line 1371547
    :goto_0
    return v0

    .line 1371548
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1371549
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1371550
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    move-result-object v1

    .line 1371551
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1371552
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1371553
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1371554
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1371555
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    move-result-object v1

    .line 1371556
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1371557
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1371558
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1371559
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1371560
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticleUserSubscriptionAction;

    move-result-object v1

    .line 1371561
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1371562
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1371563
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1371564
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7bf3f9ce -> :sswitch_2
        -0x346e20e3 -> :sswitch_1
        0x3f18eecb -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1371545
    new-instance v0, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1371542
    sparse-switch p0, :sswitch_data_0

    .line 1371543
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1371544
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7bf3f9ce -> :sswitch_0
        -0x346e20e3 -> :sswitch_0
        0x3f18eecb -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1371541
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1371539
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$DraculaImplementation;->b(I)V

    .line 1371540
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1371534
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1371535
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1371536
    :cond_0
    iput-object p1, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$DraculaImplementation;->a:LX/15i;

    .line 1371537
    iput p2, p0, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$DraculaImplementation;->b:I

    .line 1371538
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1371508
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1371533
    new-instance v0, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1371530
    iget v0, p0, LX/1vt;->c:I

    .line 1371531
    move v0, v0

    .line 1371532
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1371527
    iget v0, p0, LX/1vt;->c:I

    .line 1371528
    move v0, v0

    .line 1371529
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1371524
    iget v0, p0, LX/1vt;->b:I

    .line 1371525
    move v0, v0

    .line 1371526
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1371521
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1371522
    move-object v0, v0

    .line 1371523
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1371512
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1371513
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1371514
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1371515
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1371516
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1371517
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1371518
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1371519
    invoke-static {v3, v9, v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/richdocument/model/graphql/RichDocumentSubscriptionsMutationGraphQlModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1371520
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1371509
    iget v0, p0, LX/1vt;->c:I

    .line 1371510
    move v0, v0

    .line 1371511
    return v0
.end method
