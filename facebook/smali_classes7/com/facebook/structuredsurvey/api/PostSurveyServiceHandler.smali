.class public Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/7EZ;

.field private final b:LX/7Eb;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/7EZ;LX/7Eb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/7EZ;",
            "LX/7Eb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1184968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184969
    iput-object p1, p0, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;->c:LX/0Or;

    .line 1184970
    iput-object p2, p0, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;->a:LX/7EZ;

    .line 1184971
    iput-object p3, p0, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;->b:LX/7Eb;

    .line 1184972
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;
    .locals 6

    .prologue
    .line 1184973
    const-class v1, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;

    monitor-enter v1

    .line 1184974
    :try_start_0
    sget-object v0, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1184975
    sput-object v2, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1184976
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1184977
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1184978
    new-instance v5, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;

    const/16 v3, 0xb83

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 1184979
    new-instance v3, LX/7EZ;

    invoke-direct {v3}, LX/7EZ;-><init>()V

    .line 1184980
    move-object v3, v3

    .line 1184981
    move-object v3, v3

    .line 1184982
    check-cast v3, LX/7EZ;

    .line 1184983
    new-instance v4, LX/7Eb;

    invoke-direct {v4}, LX/7Eb;-><init>()V

    .line 1184984
    move-object v4, v4

    .line 1184985
    move-object v4, v4

    .line 1184986
    check-cast v4, LX/7Eb;

    invoke-direct {v5, p0, v3, v4}, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;-><init>(LX/0Or;LX/7EZ;LX/7Eb;)V

    .line 1184987
    move-object v0, v5

    .line 1184988
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1184989
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1184990
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1184991
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 1184992
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1184993
    const-string v1, "post_survey_answers"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1184994
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1184995
    const-string v1, "postSurveyAnswersParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;

    .line 1184996
    iget-object v1, p0, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;->a:LX/7EZ;

    const-class v3, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 1184997
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1184998
    :goto_0
    return-object v0

    .line 1184999
    :cond_0
    const-string v1, "post_survey_impressions"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1185000
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1185001
    const-string v1, "postSurveyImpressionsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;

    .line 1185002
    iget-object v1, p0, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;->b:LX/7Eb;

    const-class v3, Lcom/facebook/structuredsurvey/api/PostSurveyServiceHandler;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 1185003
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1185004
    goto :goto_0

    .line 1185005
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
