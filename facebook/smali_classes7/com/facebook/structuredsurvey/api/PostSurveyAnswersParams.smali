.class public Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1184884
    new-instance v0, LX/7Ea;

    invoke-direct {v0}, LX/7Ea;-><init>()V

    sput-object v0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1184885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184886
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->a:Ljava/lang/String;

    .line 1184887
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->b:Ljava/lang/String;

    .line 1184888
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->c:Z

    .line 1184889
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 1184890
    const-class v1, Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1184891
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->d:LX/0P1;

    .line 1184892
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1184893
    sget-object v1, Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1184894
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->e:LX/0Px;

    .line 1184895
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1184896
    const-class v1, Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1184897
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->f:LX/0Px;

    .line 1184898
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1184899
    const-class v1, Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1184900
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->g:LX/0P1;

    .line 1184901
    return-void

    .line 1184902
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLX/0P1;LX/0Px;LX/0Px;LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/structuredsurvey/api/ParcelableStringArrayList;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1184903
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184904
    iput-object p1, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->a:Ljava/lang/String;

    .line 1184905
    iput-object p2, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->b:Ljava/lang/String;

    .line 1184906
    iput-boolean p3, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->c:Z

    .line 1184907
    iput-object p4, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->d:LX/0P1;

    .line 1184908
    iput-object p5, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->e:LX/0Px;

    .line 1184909
    iput-object p6, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->f:LX/0Px;

    .line 1184910
    iput-object p7, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->g:LX/0P1;

    .line 1184911
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1184912
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1184913
    iget-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1184914
    iget-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1184915
    iget-boolean v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1184916
    iget-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->d:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1184917
    iget-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->e:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1184918
    iget-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->f:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1184919
    iget-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyAnswersParams;->g:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1184920
    return-void

    .line 1184921
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
