.class public Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1184967
    new-instance v0, LX/7Ec;

    invoke-direct {v0}, LX/7Ec;-><init>()V

    sput-object v0, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1184947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184948
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->a:Ljava/lang/String;

    .line 1184949
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->b:Ljava/lang/String;

    .line 1184950
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->c:Ljava/lang/String;

    .line 1184951
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1184952
    const-class v1, Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1184953
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->d:LX/0P1;

    .line 1184954
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1184961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184962
    iput-object p1, p0, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->a:Ljava/lang/String;

    .line 1184963
    iput-object p2, p0, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->b:Ljava/lang/String;

    .line 1184964
    iput-object p3, p0, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->c:Ljava/lang/String;

    .line 1184965
    iput-object p4, p0, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->d:LX/0P1;

    .line 1184966
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1184960
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1184955
    iget-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1184956
    iget-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1184957
    iget-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1184958
    iget-object v0, p0, Lcom/facebook/structuredsurvey/api/PostSurveyImpressionsParams;->d:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1184959
    return-void
.end method
