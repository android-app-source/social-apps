.class public final Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1185693
    const-class v0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;

    new-instance v1, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1185694
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1185695
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1185696
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1185697
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1185698
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1185699
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1185700
    if-eqz v2, :cond_0

    .line 1185701
    const-string p0, "survey_config"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185702
    invoke-static {v1, v2, p1}, LX/7Ez;->a(LX/15i;ILX/0nX;)V

    .line 1185703
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1185704
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1185705
    check-cast p1, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$Serializer;->a(Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
