.class public final Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1185097
    const-class v0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;

    new-instance v1, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1185098
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1185096
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1185074
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1185075
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1185076
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1185077
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1185078
    if-eqz v2, :cond_0

    .line 1185079
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185080
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1185081
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1185082
    if-eqz v2, :cond_1

    .line 1185083
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185084
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1185085
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1185086
    if-eqz v2, :cond_2

    .line 1185087
    const-string p0, "survey_inventory"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185088
    invoke-static {v1, v2, p1, p2}, LX/7Fp;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1185089
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1185090
    if-eqz v2, :cond_3

    .line 1185091
    const-string p0, "survey_session"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1185092
    invoke-static {v1, v2, p1, p2}, LX/7Fp;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1185093
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1185094
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1185095
    check-cast p1, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel$Serializer;->a(Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
