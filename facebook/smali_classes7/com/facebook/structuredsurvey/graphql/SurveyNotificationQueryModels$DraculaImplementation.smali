.class public final Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1185282
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1185283
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1185280
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1185281
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1185243
    if-nez p1, :cond_0

    .line 1185244
    :goto_0
    return v0

    .line 1185245
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1185246
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1185247
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1185248
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1185249
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1185250
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1185251
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1185252
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1185253
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1185254
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1185255
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1185256
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1185257
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1185258
    const v2, -0x5f41ddb5

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 1185259
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v2

    .line 1185260
    const v3, -0x63ee10a9

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v2

    .line 1185261
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1185262
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1185263
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1185264
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1185265
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1185266
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 1185267
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1185268
    :sswitch_3
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1185269
    invoke-virtual {p0, p1, v5, v0}, LX/15i;->a(III)I

    move-result v2

    .line 1185270
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1185271
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1185272
    invoke-virtual {p3, v5, v2, v0}, LX/186;->a(III)V

    .line 1185273
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1185274
    :sswitch_4
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1185275
    invoke-virtual {p0, p1, v5, v0}, LX/15i;->a(III)I

    move-result v2

    .line 1185276
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1185277
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1185278
    invoke-virtual {p3, v5, v2, v0}, LX/186;->a(III)V

    .line 1185279
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x63ee10a9 -> :sswitch_4
        -0x61e1ec4e -> :sswitch_1
        -0x5f41ddb5 -> :sswitch_3
        -0x50151f98 -> :sswitch_2
        0x159ad497 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1185242
    new-instance v0, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1185235
    sparse-switch p2, :sswitch_data_0

    .line 1185236
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1185237
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1185238
    const v1, -0x5f41ddb5

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1185239
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1185240
    const v1, -0x63ee10a9

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1185241
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x63ee10a9 -> :sswitch_1
        -0x61e1ec4e -> :sswitch_1
        -0x5f41ddb5 -> :sswitch_1
        -0x50151f98 -> :sswitch_0
        0x159ad497 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1185225
    if-nez p1, :cond_0

    move v0, v1

    .line 1185226
    :goto_0
    return v0

    .line 1185227
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 1185228
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1185229
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1185230
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 1185231
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 1185232
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1185233
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1185234
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1185218
    if-eqz p1, :cond_0

    .line 1185219
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 1185220
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1185221
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 1185222
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1185223
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1185224
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1185178
    if-eqz p1, :cond_0

    .line 1185179
    invoke-static {p0, p1, p2}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1185180
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;

    .line 1185181
    if-eq v0, v1, :cond_0

    .line 1185182
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1185183
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1185217
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1185215
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1185216
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1185210
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1185211
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1185212
    :cond_0
    iput-object p1, p0, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1185213
    iput p2, p0, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;->b:I

    .line 1185214
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1185209
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1185208
    new-instance v0, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1185205
    iget v0, p0, LX/1vt;->c:I

    .line 1185206
    move v0, v0

    .line 1185207
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1185202
    iget v0, p0, LX/1vt;->c:I

    .line 1185203
    move v0, v0

    .line 1185204
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1185199
    iget v0, p0, LX/1vt;->b:I

    .line 1185200
    move v0, v0

    .line 1185201
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1185196
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1185197
    move-object v0, v0

    .line 1185198
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1185187
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1185188
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1185189
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1185190
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1185191
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1185192
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1185193
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1185194
    invoke-static {v3, v9, v2}, Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/structuredsurvey/graphql/SurveyNotificationQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1185195
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1185184
    iget v0, p0, LX/1vt;->c:I

    .line 1185185
    move v0, v0

    .line 1185186
    return v0
.end method
