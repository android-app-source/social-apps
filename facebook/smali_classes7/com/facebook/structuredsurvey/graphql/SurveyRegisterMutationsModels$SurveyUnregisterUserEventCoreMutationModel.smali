.class public final Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x590c2e36
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$SurveyConfigModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1185852
    const-class v0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1185853
    const-class v0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1185854
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1185855
    return-void
.end method

.method private a()Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$SurveyConfigModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1185856
    iget-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel;->e:Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$SurveyConfigModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$SurveyConfigModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$SurveyConfigModel;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel;->e:Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$SurveyConfigModel;

    .line 1185857
    iget-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel;->e:Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$SurveyConfigModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1185858
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1185859
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel;->a()Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$SurveyConfigModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1185860
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1185861
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1185862
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1185863
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1185864
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1185865
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel;->a()Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$SurveyConfigModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1185866
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel;->a()Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$SurveyConfigModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$SurveyConfigModel;

    .line 1185867
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel;->a()Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$SurveyConfigModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1185868
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel;

    .line 1185869
    iput-object v0, v1, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel;->e:Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel$SurveyConfigModel;

    .line 1185870
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1185871
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1185872
    new-instance v0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel;

    invoke-direct {v0}, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyUnregisterUserEventCoreMutationModel;-><init>()V

    .line 1185873
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1185874
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1185875
    const v0, 0x107748fa

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1185876
    const v0, 0x45d789f4

    return v0
.end method
