.class public final Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5baedc1d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$SurveyConfigModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1185765
    const-class v0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1185764
    const-class v0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1185749
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1185750
    return-void
.end method

.method private a()Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$SurveyConfigModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1185762
    iget-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;->e:Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$SurveyConfigModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$SurveyConfigModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$SurveyConfigModel;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;->e:Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$SurveyConfigModel;

    .line 1185763
    iget-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;->e:Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$SurveyConfigModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1185766
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1185767
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;->a()Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$SurveyConfigModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1185768
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1185769
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1185770
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1185771
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1185754
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1185755
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;->a()Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$SurveyConfigModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1185756
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;->a()Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$SurveyConfigModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$SurveyConfigModel;

    .line 1185757
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;->a()Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$SurveyConfigModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1185758
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;

    .line 1185759
    iput-object v0, v1, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;->e:Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel$SurveyConfigModel;

    .line 1185760
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1185761
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1185751
    new-instance v0, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;

    invoke-direct {v0}, Lcom/facebook/structuredsurvey/graphql/SurveyRegisterMutationsModels$SurveyRegisterUserEventCoreMutationModel;-><init>()V

    .line 1185752
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1185753
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1185748
    const v0, 0x6ef45d4a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1185747
    const v0, 0x33ed9ddb

    return v0
.end method
