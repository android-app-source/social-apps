.class public final Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2846ac78
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1185141
    const-class v0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1185140
    const-class v0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1185099
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1185100
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1185138
    iget-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->e:Ljava/lang/String;

    .line 1185139
    iget-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1185136
    iget-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->f:Ljava/lang/String;

    .line 1185137
    iget-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1185134
    iget-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->g:Ljava/util/List;

    .line 1185135
    iget-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private m()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1185132
    iget-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->h:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->h:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    .line 1185133
    iget-object v0, p0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->h:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1185120
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1185121
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1185122
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1185123
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->l()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1185124
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->m()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1185125
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1185126
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1185127
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1185128
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1185129
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1185130
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1185131
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1185107
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1185108
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->l()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1185109
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->l()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1185110
    if-eqz v1, :cond_2

    .line 1185111
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;

    .line 1185112
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->g:Ljava/util/List;

    move-object v1, v0

    .line 1185113
    :goto_0
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->m()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1185114
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->m()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    .line 1185115
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->m()Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1185116
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;

    .line 1185117
    iput-object v0, v1, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->h:Lcom/facebook/survey/graphql/StructuredSurveySessionFragmentsModels$StructuredSurveySessionFragmentModel;

    .line 1185118
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1185119
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1185106
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1185103
    new-instance v0, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;

    invoke-direct {v0}, Lcom/facebook/structuredsurvey/graphql/SurveyMultipleIntegrationPointsQueryModels$SurveyMultipleIntegrationPointsQueryModel;-><init>()V

    .line 1185104
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1185105
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1185102
    const v0, -0x39e55cc9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1185101
    const v0, -0x2e91e84a

    return v0
.end method
