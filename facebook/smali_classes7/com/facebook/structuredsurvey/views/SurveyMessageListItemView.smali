.class public Lcom/facebook/structuredsurvey/views/SurveyMessageListItemView;
.super LX/7FT;
.source ""


# instance fields
.field private b:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1186400
    invoke-direct {p0, p1}, LX/7FT;-><init>(Landroid/content/Context;)V

    .line 1186401
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyMessageListItemView;->a()V

    .line 1186402
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1186397
    invoke-direct {p0, p1, p2}, LX/7FT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1186398
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyMessageListItemView;->a()V

    .line 1186399
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyMessageListItemView;
    .locals 3

    .prologue
    .line 1186386
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031447

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/views/SurveyMessageListItemView;

    .line 1186387
    sget-object v1, LX/7F7;->MESSAGE:LX/7F7;

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/views/SurveyMessageListItemView;->setTag(Ljava/lang/Object;)V

    .line 1186388
    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1186394
    const v0, 0x7f031446

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1186395
    const v0, 0x7f0d2e55

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyMessageListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyMessageListItemView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1186396
    return-void
.end method


# virtual methods
.method public final a(LX/7F3;)V
    .locals 2

    .prologue
    .line 1186389
    check-cast p1, LX/7F8;

    .line 1186390
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyMessageListItemView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1186391
    iget-object v1, p1, LX/7F8;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1186392
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1186393
    return-void
.end method
