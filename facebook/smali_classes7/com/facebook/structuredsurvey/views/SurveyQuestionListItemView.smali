.class public Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;
.super LX/7FT;
.source ""


# instance fields
.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1186536
    invoke-direct {p0, p1}, LX/7FT;-><init>(Landroid/content/Context;)V

    .line 1186537
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->a()V

    .line 1186538
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1186539
    invoke-direct {p0, p1, p2}, LX/7FT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1186540
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->a()V

    .line 1186541
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;
    .locals 3

    .prologue
    .line 1186542
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03144c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;

    .line 1186543
    sget-object v1, LX/7F7;->QUESTION:LX/7F7;

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->setTag(Ljava/lang/Object;)V

    .line 1186544
    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1186545
    const v0, 0x7f03144b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1186546
    const v0, 0x7f0d2e59

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1186547
    const v0, 0x7f0d2e49

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1186548
    const v0, 0x7f0d2e5a

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1186549
    return-void
.end method


# virtual methods
.method public final a(LX/7F3;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1186550
    check-cast p1, LX/7FA;

    .line 1186551
    iget-object v0, p1, LX/7FA;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1186552
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1186553
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1186554
    :goto_0
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1186555
    iget-object v1, p1, LX/7FA;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1186556
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1186557
    iget-object v0, p1, LX/7FA;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1186558
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1186559
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1186560
    :goto_1
    return-void

    .line 1186561
    :cond_0
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1186562
    iget-object v1, p1, LX/7FA;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1186563
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1186564
    :cond_1
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->d:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1186565
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyQuestionListItemView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1186566
    iget-object v1, p1, LX/7FA;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1186567
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
