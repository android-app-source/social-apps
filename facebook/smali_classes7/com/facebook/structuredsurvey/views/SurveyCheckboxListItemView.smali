.class public Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;
.super LX/7FT;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Landroid/widget/CheckBox;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1186311
    invoke-direct {p0, p1}, LX/7FT;-><init>(Landroid/content/Context;)V

    .line 1186312
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->a()V

    .line 1186313
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1186308
    invoke-direct {p0, p1, p2}, LX/7FT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1186309
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->a()V

    .line 1186310
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;
    .locals 3

    .prologue
    .line 1186305
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03143c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;

    .line 1186306
    sget-object v1, LX/7F7;->CHECKBOX:LX/7F7;

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->setTag(Ljava/lang/Object;)V

    .line 1186307
    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1186301
    const v0, 0x7f03143b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1186302
    const v0, 0x7f0d2e44

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1186303
    const v0, 0x7f0d2e45

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->c:Landroid/widget/CheckBox;

    .line 1186304
    return-void
.end method


# virtual methods
.method public final a(LX/7F3;)V
    .locals 2

    .prologue
    .line 1186286
    iput-object p1, p0, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->a:LX/7F3;

    .line 1186287
    check-cast p1, LX/7F4;

    .line 1186288
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, LX/7F4;->a()LX/7EQ;

    move-result-object v1

    .line 1186289
    iget-object p0, v1, LX/7EQ;->c:Ljava/lang/String;

    move-object v1, p0

    .line 1186290
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1186291
    return-void
.end method

.method public final isChecked()Z
    .locals 1

    .prologue
    .line 1186300
    iget-boolean v0, p0, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->d:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 2

    .prologue
    .line 1186295
    iput-boolean p1, p0, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->d:Z

    .line 1186296
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->c:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->d:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1186297
    iget-object v0, p0, LX/7FT;->a:LX/7F3;

    check-cast v0, LX/7F4;

    iget-boolean v1, p0, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->d:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1186298
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    iput-boolean p0, v0, LX/7F4;->d:Z

    .line 1186299
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 1186292
    iget-boolean v0, p0, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyCheckboxListItemView;->setChecked(Z)V

    .line 1186293
    return-void

    .line 1186294
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
