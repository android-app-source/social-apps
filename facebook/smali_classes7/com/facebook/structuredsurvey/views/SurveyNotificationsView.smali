.class public Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public j:LX/11S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final n:LX/1aX;

.field private final o:Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;

.field private final p:Lcom/facebook/widget/text/BetterTextView;

.field private final q:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1186535
    const-class v0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;

    const-string v1, "notifications_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1186533
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1186534
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1186519
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1186520
    const-class v0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;

    invoke-static {v0, p0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1186521
    const v0, 0x7f031448

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1186522
    const v0, 0x7f0d2e56

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->o:Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;

    .line 1186523
    const v0, 0x7f0d2e57

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->p:Lcom/facebook/widget/text/BetterTextView;

    .line 1186524
    invoke-virtual {p0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1151

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->q:I

    .line 1186525
    new-instance v0, LX/1Uo;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const/4 v1, 0x1

    .line 1186526
    iput v1, v0, LX/1Uo;->d:I

    .line 1186527
    move-object v0, v0

    .line 1186528
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 1186529
    invoke-static {v0, p1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->n:LX/1aX;

    .line 1186530
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->n:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1186531
    const v0, 0x7f0a00f2

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 1186532
    return-void
.end method

.method private static a(Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;LX/11S;LX/0Or;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;",
            "LX/11S;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1186518
    iput-object p1, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->j:LX/11S;

    iput-object p2, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->k:LX/0Or;

    iput-object p3, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->l:LX/03V;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;

    invoke-static {v1}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v0

    check-cast v0, LX/11S;

    const/16 v2, 0x509

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0, v0, v2, v1}, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->a(Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;LX/11S;LX/0Or;LX/03V;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/text/Spannable;J)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1186498
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->setBackgroundColor(I)V

    .line 1186499
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e0756

    invoke-virtual {v0, v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1186500
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->o:Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;

    invoke-virtual {v0, p3, v1}, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->a(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 1186501
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->p:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->j:LX/11S;

    sget-object v3, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, p4

    invoke-interface {v2, v3, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1186502
    if-eqz p2, :cond_2

    .line 1186503
    :try_start_0
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v2, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->n:LX/1aX;

    .line 1186504
    iget-object v3, v2, LX/1aX;->f:LX/1aZ;

    move-object v2, v3

    .line 1186505
    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1186506
    iget-object v2, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->n:LX/1aX;

    invoke-virtual {v2, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 1186507
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->n:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1186508
    const/4 v2, 0x0

    const/4 v3, 0x0

    iget v4, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->q:I

    iget v5, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->q:I

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1186509
    iget-object v2, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->p:Lcom/facebook/widget/text/BetterTextView;

    .line 1186510
    iget-boolean v3, v2, Lcom/facebook/widget/text/BetterTextView;->f:Z

    move v3, v3

    .line 1186511
    iget-object v4, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->p:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v3, :cond_0

    move-object v2, v1

    :goto_0
    const/4 v5, 0x0

    if-eqz v3, :cond_1

    :goto_1
    const/4 v3, 0x0

    invoke-virtual {v4, v2, v5, v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1186512
    :goto_2
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1186513
    return-void

    :cond_0
    move-object v2, v0

    .line 1186514
    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1

    .line 1186515
    :catch_0
    move-exception v0

    .line 1186516
    iget-object v2, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->l:LX/03V;

    const-string v3, "SurveyNotificationsView binding error"

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1186517
    :cond_2
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1, v1, v1, v1}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2a5f078c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1186484
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onAttachedToWindow()V

    .line 1186485
    iget-object v1, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->n:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 1186486
    const/16 v1, 0x2d

    const v2, 0x1d72808b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x72f9d72f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1186495
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onDetachedFromWindow()V

    .line 1186496
    iget-object v1, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->n:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 1186497
    const/16 v1, 0x2d

    const v2, 0x45c835

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1186492
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->onFinishTemporaryDetach()V

    .line 1186493
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->n:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 1186494
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1186489
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->onStartTemporaryDetach()V

    .line 1186490
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->n:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 1186491
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1186487
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    .line 1186488
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->n:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
