.class public Lcom/facebook/structuredsurvey/views/SurveyNotificationListItemView;
.super LX/7FT;
.source ""


# instance fields
.field public b:Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1186403
    invoke-direct {p0, p1}, LX/7FT;-><init>(Landroid/content/Context;)V

    .line 1186404
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationListItemView;->a()V

    .line 1186405
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1186406
    invoke-direct {p0, p1, p2}, LX/7FT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1186407
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationListItemView;->a()V

    .line 1186408
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyNotificationListItemView;
    .locals 3

    .prologue
    .line 1186409
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03144a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/views/SurveyNotificationListItemView;

    .line 1186410
    sget-object v1, LX/7F7;->NOTIFICATION:LX/7F7;

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/views/SurveyNotificationListItemView;->setTag(Ljava/lang/Object;)V

    .line 1186411
    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1186412
    const v0, 0x7f031449

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1186413
    const v0, 0x7f0d2e58

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationListItemView;->b:Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;

    .line 1186414
    return-void
.end method


# virtual methods
.method public final a(LX/7F3;)V
    .locals 8

    .prologue
    .line 1186415
    check-cast p1, LX/7F9;

    .line 1186416
    if-eqz p1, :cond_0

    .line 1186417
    iget-object v0, p1, LX/7F9;->c:LX/7FH;

    move-object v0, v0

    .line 1186418
    if-nez v0, :cond_1

    .line 1186419
    :cond_0
    :goto_0
    return-void

    .line 1186420
    :cond_1
    iget-object v0, p1, LX/7F9;->c:LX/7FH;

    move-object v4, v0

    .line 1186421
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationListItemView;->b:Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;

    .line 1186422
    iget-object v1, v4, LX/7FH;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1186423
    iget-object v2, v4, LX/7FH;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1186424
    iget-object v3, v4, LX/7FH;->a:Landroid/text/Spannable;

    move-object v3, v3

    .line 1186425
    iget-wide v6, v4, LX/7FH;->d:J

    move-wide v4, v6

    .line 1186426
    invoke-virtual/range {v0 .. v5}, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;->a(Ljava/lang/String;Ljava/lang/String;Landroid/text/Spannable;J)V

    goto :goto_0
.end method
