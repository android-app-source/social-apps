.class public Lcom/facebook/structuredsurvey/views/SurveyDividerListItemView;
.super LX/7FT;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1186314
    invoke-direct {p0, p1}, LX/7FT;-><init>(Landroid/content/Context;)V

    .line 1186315
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyDividerListItemView;->a()V

    .line 1186316
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1186317
    invoke-direct {p0, p1, p2}, LX/7FT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1186318
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyDividerListItemView;->a()V

    .line 1186319
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyDividerListItemView;
    .locals 3

    .prologue
    .line 1186320
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03143f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/views/SurveyDividerListItemView;

    .line 1186321
    sget-object v1, LX/7F7;->DIVIDER:LX/7F7;

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/views/SurveyDividerListItemView;->setTag(Ljava/lang/Object;)V

    .line 1186322
    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1186323
    const v0, 0x7f03143e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1186324
    return-void
.end method
