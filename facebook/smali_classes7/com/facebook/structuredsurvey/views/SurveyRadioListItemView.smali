.class public Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;
.super LX/7FT;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field public b:Lcom/facebook/widget/text/BetterTextView;

.field public c:Landroid/widget/RadioButton;

.field public d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1186594
    invoke-direct {p0, p1}, LX/7FT;-><init>(Landroid/content/Context;)V

    .line 1186595
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->c()V

    .line 1186596
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1186568
    invoke-direct {p0, p1, p2}, LX/7FT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1186569
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->c()V

    .line 1186570
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;
    .locals 3

    .prologue
    .line 1186591
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03144e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;

    .line 1186592
    sget-object v1, LX/7F7;->RADIO:LX/7F7;

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->setTag(Ljava/lang/Object;)V

    .line 1186593
    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1186589
    const v0, 0x7f03144d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1186590
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 1186585
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->b()V

    .line 1186586
    const v0, 0x7f0d2e5b

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1186587
    const v0, 0x7f0d2e5c

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->c:Landroid/widget/RadioButton;

    .line 1186588
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1186597
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->c:Landroid/widget/RadioButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 1186598
    return-void
.end method

.method public final a(LX/7F3;)V
    .locals 2

    .prologue
    .line 1186579
    iput-object p1, p0, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->a:LX/7F3;

    .line 1186580
    check-cast p1, LX/7FB;

    .line 1186581
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, LX/7FB;->a()LX/7EQ;

    move-result-object v1

    .line 1186582
    iget-object p0, v1, LX/7EQ;->c:Ljava/lang/String;

    move-object v1, p0

    .line 1186583
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1186584
    return-void
.end method

.method public final isChecked()Z
    .locals 1

    .prologue
    .line 1186578
    iget-boolean v0, p0, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->d:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 2

    .prologue
    .line 1186574
    iput-boolean p1, p0, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->d:Z

    .line 1186575
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->c:Landroid/widget/RadioButton;

    iget-boolean v1, p0, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->d:Z

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 1186576
    iget-object v0, p0, LX/7FT;->a:LX/7F3;

    check-cast v0, LX/7FB;

    iget-boolean v1, p0, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->d:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7FB;->a(Ljava/lang/Boolean;)V

    .line 1186577
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 1186571
    iget-boolean v0, p0, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyRadioListItemView;->setChecked(Z)V

    .line 1186572
    return-void

    .line 1186573
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
