.class public Lcom/facebook/structuredsurvey/views/SurveyImageBlockListItemView;
.super LX/7FT;
.source ""


# instance fields
.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/widget/text/BetterButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1186359
    invoke-direct {p0, p1}, LX/7FT;-><init>(Landroid/content/Context;)V

    .line 1186360
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyImageBlockListItemView;->a()V

    .line 1186361
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1186362
    invoke-direct {p0, p1, p2}, LX/7FT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1186363
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyImageBlockListItemView;->a()V

    .line 1186364
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyImageBlockListItemView;
    .locals 3

    .prologue
    .line 1186365
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031444

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/views/SurveyImageBlockListItemView;

    .line 1186366
    sget-object v1, LX/7F7;->IMAGEBLOCK:LX/7F7;

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/views/SurveyImageBlockListItemView;->setTag(Ljava/lang/Object;)V

    .line 1186367
    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1186368
    const v0, 0x7f031443

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1186369
    const v0, 0x7f0d2e4c

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyImageBlockListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyImageBlockListItemView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1186370
    const v0, 0x7f0d2e4d

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyImageBlockListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyImageBlockListItemView;->c:Lcom/facebook/widget/text/BetterButton;

    .line 1186371
    return-void
.end method


# virtual methods
.method public final a(LX/7F3;)V
    .locals 2

    .prologue
    .line 1186372
    check-cast p1, LX/7F6;

    .line 1186373
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyImageBlockListItemView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1186374
    iget-object v1, p1, LX/7F6;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1186375
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1186376
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyImageBlockListItemView;->c:Lcom/facebook/widget/text/BetterButton;

    .line 1186377
    iget-object v1, p1, LX/7F6;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1186378
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setText(Ljava/lang/CharSequence;)V

    .line 1186379
    return-void
.end method
