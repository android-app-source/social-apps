.class public Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;
.super LX/7FT;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private b:Landroid/widget/CompoundButton;

.field private c:Lcom/facebook/widget/text/BetterEditTextView;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:LX/7F7;

.field public f:Landroid/view/View$OnFocusChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1186690
    invoke-direct {p0, p1}, LX/7FT;-><init>(Landroid/content/Context;)V

    .line 1186691
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1186688
    invoke-direct {p0, p1, p2}, LX/7FT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1186689
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;LX/7F7;)Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;
    .locals 3

    .prologue
    .line 1186684
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031452

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;

    .line 1186685
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    invoke-direct {v0, p1}, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->a(LX/7F7;)V

    .line 1186686
    invoke-virtual {v0, p1}, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->setTag(Ljava/lang/Object;)V

    .line 1186687
    return-object v0
.end method

.method private a(LX/7F7;)V
    .locals 4

    .prologue
    .line 1186669
    iput-object p1, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->e:LX/7F7;

    .line 1186670
    sget-object v0, LX/7F7;->CHECKBOXWRITEIN:LX/7F7;

    if-ne p1, v0, :cond_0

    .line 1186671
    const v2, 0x7f03143d

    .line 1186672
    const v1, 0x7f0d2e45

    .line 1186673
    const v0, 0x7f0d2e44

    move v3, v0

    move v0, v1

    move v1, v3

    .line 1186674
    :goto_0
    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1186675
    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->b:Landroid/widget/CompoundButton;

    .line 1186676
    invoke-virtual {p0, v1}, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1186677
    const v0, 0x7f0d2e46

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->c:Lcom/facebook/widget/text/BetterEditTextView;

    .line 1186678
    return-void

    .line 1186679
    :cond_0
    sget-object v0, LX/7F7;->RADIOWRITEIN:LX/7F7;

    if-ne p1, v0, :cond_1

    .line 1186680
    const v2, 0x7f03144f

    .line 1186681
    const v1, 0x7f0d2e5c

    .line 1186682
    const v0, 0x7f0d2e5b

    move v3, v0

    move v0, v1

    move v1, v3

    goto :goto_0

    .line 1186683
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Either CHECKBOXWRITEIN or RADIOWRITEIN type is allowed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1186667
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->c:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->requestFocus()Z

    .line 1186668
    return-void
.end method

.method public final a(LX/7F3;)V
    .locals 2

    .prologue
    .line 1186651
    iput-object p1, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->a:LX/7F3;

    .line 1186652
    check-cast p1, LX/7FD;

    .line 1186653
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->c:Lcom/facebook/widget/text/BetterEditTextView;

    .line 1186654
    iget-object v1, p1, LX/7FD;->e:LX/7F5;

    invoke-virtual {v1}, LX/7F5;->c()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1186655
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1186656
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1186657
    iget-object v1, p1, LX/7FD;->c:LX/7EQ;

    .line 1186658
    iget-object p1, v1, LX/7EQ;->c:Ljava/lang/String;

    move-object v1, p1

    .line 1186659
    move-object v1, v1

    .line 1186660
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1186661
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->e:LX/7F7;

    sget-object v1, LX/7F7;->CHECKBOXWRITEIN:LX/7F7;

    if-ne v0, v1, :cond_1

    .line 1186662
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->c:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v1, LX/7FU;

    invoke-direct {v1, p0}, LX/7FU;-><init>(Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1186663
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->c:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v1, LX/7FW;

    invoke-direct {v1, p0}, LX/7FW;-><init>(Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1186664
    return-void

    .line 1186665
    :cond_1
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->e:LX/7F7;

    sget-object v1, LX/7F7;->RADIOWRITEIN:LX/7F7;

    if-ne v0, v1, :cond_0

    .line 1186666
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->c:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v1, LX/7FV;

    invoke-direct {v1, p0}, LX/7FV;-><init>(Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1186637
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->c:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final isChecked()Z
    .locals 1

    .prologue
    .line 1186650
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->b:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    return v0
.end method

.method public final onStartTemporaryDetach()V
    .locals 2

    .prologue
    .line 1186647
    invoke-super {p0}, LX/7FT;->onStartTemporaryDetach()V

    .line 1186648
    iget-object v0, p0, LX/7FT;->a:LX/7F3;

    check-cast v0, LX/7FD;

    invoke-virtual {p0}, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7FD;->a(Ljava/lang/String;)V

    .line 1186649
    return-void
.end method

.method public setChecked(Z)V
    .locals 2

    .prologue
    .line 1186643
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->b:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 1186644
    iget-object v0, p0, LX/7FT;->a:LX/7F3;

    check-cast v0, LX/7FD;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1186645
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    iput-boolean p0, v0, LX/7FD;->d:Z

    .line 1186646
    return-void
.end method

.method public setItemOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 0

    .prologue
    .line 1186641
    iput-object p1, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->f:Landroid/view/View$OnFocusChangeListener;

    .line 1186642
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 1186638
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->b:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyWriteInListItemView;->setChecked(Z)V

    .line 1186639
    return-void

    .line 1186640
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
