.class public Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;
.super LX/7FT;
.source ""


# instance fields
.field private b:Lcom/facebook/widget/text/BetterEditTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1186348
    invoke-direct {p0, p1}, LX/7FT;-><init>(Landroid/content/Context;)V

    .line 1186349
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->b()V

    .line 1186350
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1186354
    invoke-direct {p0, p1, p2}, LX/7FT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1186355
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->b()V

    .line 1186356
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;
    .locals 3

    .prologue
    .line 1186351
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031441

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;

    .line 1186352
    sget-object v1, LX/7F7;->EDITTEXT:LX/7F7;

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->setTag(Ljava/lang/Object;)V

    .line 1186353
    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1186343
    const v0, 0x7f031440

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1186344
    const v0, 0x7f0d2e47

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    .line 1186345
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setFocusable(Z)V

    .line 1186346
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setFocusableInTouchMode(Z)V

    .line 1186347
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1186357
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->a(Z)V

    .line 1186358
    return-void
.end method

.method public final a(LX/7F3;)V
    .locals 2

    .prologue
    .line 1186336
    iput-object p1, p0, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->a:LX/7F3;

    .line 1186337
    iget-object v0, p0, LX/7FT;->a:LX/7F3;

    check-cast v0, LX/7F5;

    .line 1186338
    iget-boolean v1, v0, LX/7F5;->d:Z

    move v0, v1

    .line 1186339
    if-nez v0, :cond_0

    .line 1186340
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    const v1, 0x7f081a86

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setHint(I)V

    .line 1186341
    :cond_0
    iget-object v1, p0, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v0, p0, LX/7FT;->a:LX/7F3;

    check-cast v0, LX/7F5;

    invoke-virtual {v0}, LX/7F5;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1186342
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1186331
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->requestFocus()Z

    .line 1186332
    if-eqz p1, :cond_0

    .line 1186333
    invoke-virtual {p0}, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1186334
    iget-object v1, p0, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1186335
    :cond_0
    return-void
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1186325
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final onStartTemporaryDetach()V
    .locals 2

    .prologue
    .line 1186328
    iget-object v0, p0, LX/7FT;->a:LX/7F3;

    check-cast v0, LX/7F5;

    invoke-virtual {p0}, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7F5;->a(Ljava/lang/String;)V

    .line 1186329
    invoke-super {p0}, LX/7FT;->onStartTemporaryDetach()V

    .line 1186330
    return-void
.end method

.method public setItemOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1

    .prologue
    .line 1186326
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyEditTextListItemView;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1186327
    return-void
.end method
