.class public Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final a:Ljava/lang/String;

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Landroid/text/style/TabStopSpan;

.field private final e:I

.field private final f:Landroid/graphics/Rect;

.field private final g:I

.field private final h:LX/1aX;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1186461
    const-class v0, Lcom/facebook/structuredsurvey/views/SurveyNotificationsView;

    const-string v1, "notifications_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1186479
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1186480
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1186477
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1186478
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1186462
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1186463
    const-string v0, "\t"

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->a:Ljava/lang/String;

    .line 1186464
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->f:Landroid/graphics/Rect;

    .line 1186465
    const-class v0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;

    invoke-static {v0, p0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1186466
    invoke-virtual {p0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1186467
    new-instance v1, Landroid/text/style/TabStopSpan$Standard;

    const v2, 0x7f0b1150

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/TabStopSpan$Standard;-><init>(I)V

    iput-object v1, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->d:Landroid/text/style/TabStopSpan;

    .line 1186468
    const v1, 0x7f0b1151

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->g:I

    .line 1186469
    const v1, 0x7f0b114e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->e:I

    .line 1186470
    new-instance v1, LX/1Uo;

    invoke-direct {v1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const/4 v0, 0x1

    .line 1186471
    iput v0, v1, LX/1Uo;->d:I

    .line 1186472
    move-object v0, v1

    .line 1186473
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 1186474
    invoke-static {v0, p1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->h:LX/1aX;

    .line 1186475
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->h:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1186476
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;

    const/16 v1, 0x509

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->b:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1186450
    if-nez p2, :cond_0

    .line 1186451
    invoke-virtual {p0, p1}, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1186452
    :goto_0
    return-void

    .line 1186453
    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1186454
    const-string v1, "\t"

    invoke-virtual {v0, v4, v1}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1186455
    iget-object v1, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->d:Landroid/text/style/TabStopSpan;

    const/4 v2, 0x1

    const/16 v3, 0x11

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1186456
    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1186457
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->h:LX/1aX;

    .line 1186458
    iget-object v2, v1, LX/1aX;->f:LX/1aZ;

    move-object v1, v2

    .line 1186459
    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1186460
    iget-object v1, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->h:LX/1aX;

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x71a258dc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1186481
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onAttachedToWindow()V

    .line 1186482
    iget-object v1, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->h:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 1186483
    const/16 v1, 0x2d

    const v2, 0x746d4f86

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x14506c56

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1186447
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onDetachedFromWindow()V

    .line 1186448
    iget-object v1, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->h:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 1186449
    const/16 v1, 0x2d

    const v2, -0x641b0b12

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1186442
    invoke-super {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1186443
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->h:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1186444
    if-eqz v0, :cond_0

    .line 1186445
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1186446
    :cond_0
    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 1186439
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onFinishTemporaryDetach()V

    .line 1186440
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->h:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 1186441
    return-void
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x2

    const/16 v2, 0x2c

    const v3, -0x5524914a

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1186432
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;->onMeasure(II)V

    .line 1186433
    iget-object v2, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->f:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v2}, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 1186434
    iget-boolean v2, p0, Lcom/facebook/widget/text/BetterTextView;->f:Z

    move v2, v2

    .line 1186435
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->getMeasuredWidth()I

    move-result v0

    iget v2, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->g:I

    sub-int/2addr v0, v2

    .line 1186436
    :cond_0
    iget-object v2, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->f:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->e:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->g:I

    sub-int/2addr v2, v3

    .line 1186437
    iget-object v3, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->h:LX/1aX;

    invoke-virtual {v3}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget v4, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->g:I

    add-int/2addr v4, v0

    iget v5, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->g:I

    add-int/2addr v5, v2

    invoke-virtual {v3, v0, v2, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1186438
    const v0, 0x5a93e863

    invoke-static {v0, v1}, LX/02F;->g(II)V

    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 1186429
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onStartTemporaryDetach()V

    .line 1186430
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->h:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 1186431
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1186427
    invoke-super {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    .line 1186428
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveyNotificationTextView;->h:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
