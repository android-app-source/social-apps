.class public Lcom/facebook/structuredsurvey/views/SurveySpaceListItemView;
.super LX/7FT;
.source ""


# instance fields
.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1186599
    invoke-direct {p0, p1}, LX/7FT;-><init>(Landroid/content/Context;)V

    .line 1186600
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveySpaceListItemView;->a()V

    .line 1186601
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1186602
    invoke-direct {p0, p1, p2}, LX/7FT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1186603
    invoke-direct {p0}, Lcom/facebook/structuredsurvey/views/SurveySpaceListItemView;->a()V

    .line 1186604
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)Lcom/facebook/structuredsurvey/views/SurveySpaceListItemView;
    .locals 3

    .prologue
    .line 1186605
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031451

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/structuredsurvey/views/SurveySpaceListItemView;

    .line 1186606
    sget-object v1, LX/7F7;->WHITESPACE:LX/7F7;

    invoke-virtual {v0, v1}, Lcom/facebook/structuredsurvey/views/SurveySpaceListItemView;->setTag(Ljava/lang/Object;)V

    .line 1186607
    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1186608
    const v0, 0x7f031450

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1186609
    const v0, 0x7f0d2e5d

    invoke-virtual {p0, v0}, Lcom/facebook/structuredsurvey/views/SurveySpaceListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveySpaceListItemView;->b:Landroid/view/View;

    .line 1186610
    return-void
.end method


# virtual methods
.method public final a(LX/7F3;)V
    .locals 3

    .prologue
    .line 1186611
    check-cast p1, LX/7FC;

    .line 1186612
    iget-object v0, p0, Lcom/facebook/structuredsurvey/views/SurveySpaceListItemView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1186613
    invoke-virtual {p0}, Lcom/facebook/structuredsurvey/views/SurveySpaceListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 1186614
    iget v2, p1, LX/7FC;->c:I

    move v2, v2

    .line 1186615
    int-to-float v2, v2

    .line 1186616
    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    .line 1186617
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 1186618
    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1186619
    iget-object v1, p0, Lcom/facebook/structuredsurvey/views/SurveySpaceListItemView;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1186620
    return-void
.end method
