.class public Lcom/facebook/payments/paymentmethods/model/CreditCard;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/CreditCard;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field private final c:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field private final f:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Z

.field public final j:Z

.field public final k:Z

.field private final l:Z

.field public final m:Lcom/facebook/payments/paymentmethods/model/BillingAddress;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1161010
    new-instance v0, LX/6zI;

    invoke-direct {v0}, LX/6zI;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1160995
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160996
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a:Ljava/lang/String;

    .line 1160997
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->b:Ljava/lang/String;

    .line 1160998
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->c:Ljava/lang/String;

    .line 1160999
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->d:Ljava/lang/String;

    .line 1161000
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->e:Ljava/lang/String;

    .line 1161001
    const-class v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->f:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    .line 1161002
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->g:Ljava/lang/String;

    .line 1161003
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->h:Ljava/lang/String;

    .line 1161004
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->i:Z

    .line 1161005
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->j:Z

    .line 1161006
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->k:Z

    .line 1161007
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->l:Z

    .line 1161008
    const-class v0, Lcom/facebook/payments/paymentmethods/model/BillingAddress;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/BillingAddress;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->m:Lcom/facebook/payments/paymentmethods/model/BillingAddress;

    .line 1161009
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/paymentmethods/model/CreditCard;)V
    .locals 14

    .prologue
    .line 1160983
    invoke-virtual {p1}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->f()Ljava/lang/String;

    move-result-object v4

    .line 1160984
    iget-object v0, p1, Lcom/facebook/payments/paymentmethods/model/CreditCard;->e:Ljava/lang/String;

    move-object v5, v0

    .line 1160985
    invoke-virtual {p1}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v6

    .line 1160986
    iget-object v0, p1, Lcom/facebook/payments/paymentmethods/model/CreditCard;->g:Ljava/lang/String;

    move-object v7, v0

    .line 1160987
    iget-object v0, p1, Lcom/facebook/payments/paymentmethods/model/CreditCard;->h:Ljava/lang/String;

    move-object v8, v0

    .line 1160988
    iget-boolean v0, p1, Lcom/facebook/payments/paymentmethods/model/CreditCard;->i:Z

    move v9, v0

    .line 1160989
    iget-boolean v0, p1, Lcom/facebook/payments/paymentmethods/model/CreditCard;->j:Z

    move v10, v0

    .line 1160990
    iget-boolean v0, p1, Lcom/facebook/payments/paymentmethods/model/CreditCard;->k:Z

    move v11, v0

    .line 1160991
    invoke-virtual {p1}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->h()Z

    move-result v12

    .line 1160992
    iget-object v0, p1, Lcom/facebook/payments/paymentmethods/model/CreditCard;->m:Lcom/facebook/payments/paymentmethods/model/BillingAddress;

    move-object v13, v0

    .line 1160993
    move-object v0, p0

    invoke-direct/range {v0 .. v13}, Lcom/facebook/payments/paymentmethods/model/CreditCard;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;Ljava/lang/String;Ljava/lang/String;ZZZZLcom/facebook/payments/paymentmethods/model/BillingAddress;)V

    .line 1160994
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;Ljava/lang/String;Ljava/lang/String;ZZZZLcom/facebook/payments/paymentmethods/model/BillingAddress;)V
    .locals 0
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p13    # Lcom/facebook/payments/paymentmethods/model/BillingAddress;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1160968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160969
    iput-object p1, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a:Ljava/lang/String;

    .line 1160970
    iput-object p2, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->b:Ljava/lang/String;

    .line 1160971
    iput-object p3, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->c:Ljava/lang/String;

    .line 1160972
    iput-object p4, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->d:Ljava/lang/String;

    .line 1160973
    iput-object p5, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->e:Ljava/lang/String;

    .line 1160974
    iput-object p6, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->f:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    .line 1160975
    iput-object p7, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->g:Ljava/lang/String;

    .line 1160976
    iput-object p8, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->h:Ljava/lang/String;

    .line 1160977
    iput-boolean p9, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->i:Z

    .line 1160978
    iput-boolean p10, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->j:Z

    .line 1160979
    iput-boolean p11, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->k:Z

    .line 1160980
    iput-boolean p12, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->l:Z

    .line 1160981
    iput-object p13, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->m:Lcom/facebook/payments/paymentmethods/model/BillingAddress;

    .line 1160982
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1160965
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 1160966
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1160967
    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1160964
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1160963
    invoke-static {p0}, LX/6zL;->b(Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;)Z
    .locals 2
    .param p1    # Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1160958
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1160959
    if-eqz p1, :cond_2

    .line 1160960
    iget-object v0, p1, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->c:LX/6xe;

    move-object v0, v0

    .line 1160961
    sget-object v1, LX/6xe;->REQUIRED:LX/6xe;

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->h()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1160962
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/6zU;
    .locals 1

    .prologue
    .line 1160957
    sget-object v0, LX/6zU;->CREDIT_CARD:LX/6zU;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1160929
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1160956
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1160955
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic e()LX/6zP;
    .locals 1

    .prologue
    .line 1160954
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->b()LX/6zU;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1160953
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;
    .locals 1

    .prologue
    .line 1160952
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->f:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1160951
    iget-boolean v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->l:Z

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1160948
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->m:Lcom/facebook/payments/paymentmethods/model/BillingAddress;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->m:Lcom/facebook/payments/paymentmethods/model/BillingAddress;

    .line 1160949
    iget-object p0, v0, Lcom/facebook/payments/paymentmethods/model/BillingAddress;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1160950
    goto :goto_0
.end method

.method public final j()Lcom/facebook/common/locale/Country;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1160945
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->m:Lcom/facebook/payments/paymentmethods/model/BillingAddress;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->m:Lcom/facebook/payments/paymentmethods/model/BillingAddress;

    .line 1160946
    iget-object p0, v0, Lcom/facebook/payments/paymentmethods/model/BillingAddress;->b:Lcom/facebook/common/locale/Country;

    move-object v0, p0

    .line 1160947
    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1160944
    invoke-static {p0}, LX/6zL;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)Z

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1160930
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160931
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160932
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160933
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160934
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160935
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->f:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1160936
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160937
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160938
    iget-boolean v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->i:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1160939
    iget-boolean v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->j:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1160940
    iget-boolean v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->k:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1160941
    iget-boolean v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->l:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1160942
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/CreditCard;->m:Lcom/facebook/payments/paymentmethods/model/BillingAddress;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1160943
    return-void
.end method
