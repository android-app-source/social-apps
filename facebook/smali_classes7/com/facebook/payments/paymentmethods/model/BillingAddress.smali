.class public Lcom/facebook/payments/paymentmethods/model/BillingAddress;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/BillingAddress;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final b:Lcom/facebook/common/locale/Country;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1160925
    new-instance v0, LX/6zH;

    invoke-direct {v0}, LX/6zH;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/model/BillingAddress;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1160913
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160914
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/BillingAddress;->a:Ljava/lang/String;

    .line 1160915
    const-class v0, Lcom/facebook/common/locale/Country;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/BillingAddress;->b:Lcom/facebook/common/locale/Country;

    .line 1160916
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/common/locale/Country;)V
    .locals 0
    .param p2    # Lcom/facebook/common/locale/Country;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1160921
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160922
    iput-object p1, p0, Lcom/facebook/payments/paymentmethods/model/BillingAddress;->a:Ljava/lang/String;

    .line 1160923
    iput-object p2, p0, Lcom/facebook/payments/paymentmethods/model/BillingAddress;->b:Lcom/facebook/common/locale/Country;

    .line 1160924
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1160920
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1160917
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/BillingAddress;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160918
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/BillingAddress;->b:Lcom/facebook/common/locale/Country;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1160919
    return-void
.end method
