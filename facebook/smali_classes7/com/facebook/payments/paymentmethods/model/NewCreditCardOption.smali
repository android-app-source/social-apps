.class public Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;
.super Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/6zJ;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/6xe;

.field public final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1161099
    new-instance v0, LX/6zM;

    invoke-direct {v0}, LX/6zM;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6zN;)V
    .locals 1

    .prologue
    .line 1161080
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;-><init>()V

    .line 1161081
    iget-object v0, p1, LX/6zN;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1161082
    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->a:Ljava/lang/String;

    .line 1161083
    iget-object v0, p1, LX/6zN;->b:LX/0Rf;

    move-object v0, v0

    .line 1161084
    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->b:LX/0Rf;

    .line 1161085
    iget-object v0, p1, LX/6zN;->c:LX/6xe;

    move-object v0, v0

    .line 1161086
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6xe;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->c:LX/6xe;

    .line 1161087
    iget-object v0, p1, LX/6zN;->d:LX/0Rf;

    move-object v0, v0

    .line 1161088
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rf;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->d:LX/0Rf;

    .line 1161089
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1161093
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;-><init>()V

    .line 1161094
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->a:Ljava/lang/String;

    .line 1161095
    const-class v0, LX/6zJ;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/ClassLoader;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->b:LX/0Rf;

    .line 1161096
    const-class v0, LX/6xe;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xe;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->c:LX/6xe;

    .line 1161097
    const-class v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/ClassLoader;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->d:LX/0Rf;

    .line 1161098
    return-void
.end method

.method public static newBuilder()LX/6zN;
    .locals 1

    .prologue
    .line 1161092
    new-instance v0, LX/6zN;

    invoke-direct {v0}, LX/6zN;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final d()LX/6zQ;
    .locals 1

    .prologue
    .line 1161100
    sget-object v0, LX/6zQ;->NEW_CREDIT_CARD:LX/6zQ;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1161091
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic e()LX/6zP;
    .locals 1

    .prologue
    .line 1161090
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;->d()LX/6zQ;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1161075
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1161076
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->b:LX/0Rf;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Set;)V

    .line 1161077
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->c:LX/6xe;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1161078
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;->d:LX/0Rf;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Set;)V

    .line 1161079
    return-void
.end method
