.class public Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/paymentmethods/model/PaymentMethod;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final c:LX/6zT;

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1161160
    new-instance v0, LX/6zR;

    invoke-direct {v0}, LX/6zR;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6zS;)V
    .locals 1

    .prologue
    .line 1161180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161181
    iget-object v0, p1, LX/6zS;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->a:Ljava/lang/String;

    .line 1161182
    iget-object v0, p1, LX/6zS;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->b:Ljava/lang/String;

    .line 1161183
    iget-object v0, p1, LX/6zS;->c:LX/6zT;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->c:LX/6zT;

    .line 1161184
    iget-object v0, p1, LX/6zS;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->d:Ljava/lang/String;

    .line 1161185
    iget-object v0, p1, LX/6zS;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->e:Ljava/lang/String;

    .line 1161186
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1161173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161174
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->a:Ljava/lang/String;

    .line 1161175
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->b:Ljava/lang/String;

    .line 1161176
    const-class v0, LX/6zT;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6zT;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->c:LX/6zT;

    .line 1161177
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->d:Ljava/lang/String;

    .line 1161178
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->e:Ljava/lang/String;

    .line 1161179
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)LX/6zS;
    .locals 1

    .prologue
    .line 1161172
    new-instance v0, LX/6zS;

    invoke-direct {v0, p0, p1}, LX/6zS;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1161171
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1161170
    const v0, 0x7f081d81

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/6zU;
    .locals 1

    .prologue
    .line 1161169
    sget-object v0, LX/6zU;->PAYPAL_BILLING_AGREEMENT:LX/6zU;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1161168
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic e()LX/6zP;
    .locals 1

    .prologue
    .line 1161167
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->b()LX/6zU;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1161161
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1161162
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1161163
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->c:LX/6zT;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1161164
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1161165
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1161166
    return-void
.end method
