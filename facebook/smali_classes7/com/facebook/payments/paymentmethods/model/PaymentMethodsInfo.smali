.class public Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;",
            "LX/6zQ;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Lcom/facebook/common/locale/Country;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end field

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1161245
    new-instance v0, LX/6zV;

    invoke-direct {v0}, LX/6zV;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->a:LX/0QK;

    .line 1161246
    new-instance v0, LX/6zW;

    invoke-direct {v0}, LX/6zW;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1161238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161239
    const-class v0, Lcom/facebook/common/locale/Country;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->b:Lcom/facebook/common/locale/Country;

    .line 1161240
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->c:Ljava/lang/String;

    .line 1161241
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->d:Ljava/lang/String;

    .line 1161242
    const-class v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    .line 1161243
    const-class v0, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f:LX/0Px;

    .line 1161244
    return-void
.end method

.method public constructor <init>(Lcom/facebook/common/locale/Country;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/locale/Country;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethod;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1161231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161232
    iput-object p1, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->b:Lcom/facebook/common/locale/Country;

    .line 1161233
    iput-object p2, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->c:Ljava/lang/String;

    .line 1161234
    iput-object p3, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->d:Ljava/lang/String;

    .line 1161235
    iput-object p4, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    .line 1161236
    iput-object p5, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f:LX/0Px;

    .line 1161237
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/PaymentMethod;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1161226
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 1161227
    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1161228
    :goto_1
    return-object v0

    .line 1161229
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1161230
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(LX/0Px;)Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;",
            ">;)",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;"
        }
    .end annotation

    .prologue
    .line 1161220
    new-instance v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1161221
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->b:Lcom/facebook/common/locale/Country;

    move-object v1, v1

    .line 1161222
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1161223
    iget-object v3, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1161224
    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    move-object v4, v4

    .line 1161225
    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;-><init>(Lcom/facebook/common/locale/Country;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;)V

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1161219
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1161217
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f:LX/0Px;

    move-object v0, v0

    .line 1161218
    const-class v1, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;

    invoke-static {v0, v1}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Iterable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/NewCreditCardOption;

    return-object v0
.end method

.method public final g()Lcom/facebook/payments/paymentmethods/model/PaymentMethod;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1161210
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1161211
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->b:Lcom/facebook/common/locale/Country;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1161212
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1161213
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1161214
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1161215
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1161216
    return-void
.end method
