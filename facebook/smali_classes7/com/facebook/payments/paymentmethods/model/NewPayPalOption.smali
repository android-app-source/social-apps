.class public Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;
.super Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1161122
    new-instance v0, LX/6zO;

    invoke-direct {v0}, LX/6zO;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1161118
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;-><init>()V

    .line 1161119
    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;->a:Ljava/lang/String;

    .line 1161120
    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;->b:Ljava/lang/String;

    .line 1161121
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1161114
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;-><init>()V

    .line 1161115
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;->a:Ljava/lang/String;

    .line 1161116
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;->b:Ljava/lang/String;

    .line 1161117
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1161110
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;-><init>()V

    .line 1161111
    iput-object p1, p0, Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;->a:Ljava/lang/String;

    .line 1161112
    iput-object p2, p0, Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;->b:Ljava/lang/String;

    .line 1161113
    return-void
.end method


# virtual methods
.method public final d()LX/6zQ;
    .locals 1

    .prologue
    .line 1161104
    sget-object v0, LX/6zQ;->NEW_PAYPAL:LX/6zQ;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1161109
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic e()LX/6zP;
    .locals 1

    .prologue
    .line 1161108
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;->d()LX/6zQ;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1161105
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1161106
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/NewPayPalOption;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1161107
    return-void
.end method
