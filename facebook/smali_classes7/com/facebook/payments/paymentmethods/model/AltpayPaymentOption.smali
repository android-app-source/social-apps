.class public Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;
.super Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:Landroid/net/Uri;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1160909
    new-instance v0, LX/6zG;

    invoke-direct {v0}, LX/6zG;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 1160894
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, LX/46R;->p(Landroid/os/Parcel;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    .line 1160895
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1160896
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;-><init>()V

    .line 1160897
    iput-object p1, p0, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;->a:Ljava/lang/String;

    .line 1160898
    iput-object p2, p0, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;->b:Landroid/net/Uri;

    .line 1160899
    iput-object p3, p0, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;->c:Ljava/lang/String;

    .line 1160900
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1160901
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/6zQ;
    .locals 1

    .prologue
    .line 1160902
    sget-object v0, LX/6zQ;->ALTPAY_ADYEN:LX/6zQ;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1160903
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic e()LX/6zP;
    .locals 1

    .prologue
    .line 1160904
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;->d()LX/6zQ;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1160905
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160906
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1160907
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160908
    return-void
.end method
