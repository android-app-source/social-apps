.class public interface abstract Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/payments/paymentmethods/model/PaymentMethod;


# virtual methods
.method public abstract c()Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end method

.method public abstract d()Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end method

.method public abstract f()Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end method

.method public abstract g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end method

.method public abstract h()Z
.end method

.method public abstract i()Ljava/lang/String;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation
.end method

.method public abstract j()Lcom/facebook/common/locale/Country;
    .annotation build Lcom/facebook/infer/annotation/PrivacySource;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()Z
.end method
