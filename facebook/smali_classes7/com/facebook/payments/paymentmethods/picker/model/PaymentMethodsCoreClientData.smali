.class public final Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/picker/model/CoreClientData;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

.field public final b:Lcom/facebook/payments/picker/model/ProductCoreClientData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1161757
    new-instance v0, LX/703;

    invoke-direct {v0}, LX/703;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/704;)V
    .locals 1

    .prologue
    .line 1161758
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161759
    iget-object v0, p1, LX/704;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    move-object v0, v0

    .line 1161760
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1161761
    iget-object v0, p1, LX/704;->b:Lcom/facebook/payments/picker/model/ProductCoreClientData;

    move-object v0, v0

    .line 1161762
    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;->b:Lcom/facebook/payments/picker/model/ProductCoreClientData;

    .line 1161763
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1161764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161765
    const-class v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1161766
    const-class v0, Lcom/facebook/payments/picker/model/ProductCoreClientData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/ProductCoreClientData;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;->b:Lcom/facebook/payments/picker/model/ProductCoreClientData;

    .line 1161767
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1161768
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1161769
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1161770
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsCoreClientData;->b:Lcom/facebook/payments/picker/model/ProductCoreClientData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1161771
    return-void
.end method
