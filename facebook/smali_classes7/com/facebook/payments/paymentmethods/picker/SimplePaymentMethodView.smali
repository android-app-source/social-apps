.class public Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1161717
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1161718
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->a()V

    .line 1161719
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1161678
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1161679
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->a()V

    .line 1161680
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1161714
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1161715
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->a()V

    .line 1161716
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1161709
    const v0, 0x7f031330

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1161710
    const v0, 0x7f0d2c76

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->a:Landroid/widget/ImageView;

    .line 1161711
    const v0, 0x7f0d0a0f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->b:Landroid/widget/TextView;

    .line 1161712
    const v0, 0x7f0d2c77

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->c:Landroid/widget/TextView;

    .line 1161713
    return-void
.end method


# virtual methods
.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1161707
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1161708
    return-void
.end method

.method public setPaymentMethod(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V
    .locals 5

    .prologue
    .line 1161688
    const/4 v0, 0x0

    .line 1161689
    sget-object v1, LX/6zx;->a:[I

    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->b()LX/6zU;

    move-result-object v2

    invoke-virtual {v2}, LX/6zU;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1161690
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 1161691
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->setTitle(Ljava/lang/String;)V

    .line 1161692
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1161693
    sget-object v1, LX/73o;->a:[I

    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->b()LX/6zU;

    move-result-object v2

    invoke-virtual {v2}, LX/6zU;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 1161694
    const-string v1, ""

    :goto_1
    move-object v0, v1

    .line 1161695
    invoke-virtual {p0, v0}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->setSubtitle(Ljava/lang/String;)V

    .line 1161696
    return-void

    :pswitch_0
    move-object v0, p1

    .line 1161697
    check-cast v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1161698
    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getRectangularDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object v0, v2

    .line 1161699
    goto :goto_0

    .line 1161700
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1161701
    const v1, 0x7f02160f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move-object v0, v1

    .line 1161702
    goto :goto_0

    .line 1161703
    :pswitch_2
    check-cast p1, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    .line 1161704
    const v1, 0x7f081e1d

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, LX/73p;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1161705
    :pswitch_3
    check-cast p1, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    .line 1161706
    iget-object v1, p1, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;->b:Ljava/lang/String;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setSubtitle(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1161683
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1161684
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1161685
    :goto_0
    return-void

    .line 1161686
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1161687
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1161681
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1161682
    return-void
.end method
