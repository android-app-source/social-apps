.class public final Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1162233
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1162234
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1162231
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1162232
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1162235
    if-nez p1, :cond_0

    .line 1162236
    :goto_0
    return v0

    .line 1162237
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1162238
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1162239
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1162240
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1162241
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    move-result-object v2

    .line 1162242
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1162243
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1162244
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1162245
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1162246
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2fab46b8
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1162247
    new-instance v0, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1162248
    packed-switch p0, :pswitch_data_0

    .line 1162249
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1162250
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x2fab46b8
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1162251
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1162224
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$DraculaImplementation;->b(I)V

    .line 1162225
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1162226
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1162227
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1162228
    :cond_0
    iput-object p1, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$DraculaImplementation;->a:LX/15i;

    .line 1162229
    iput p2, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$DraculaImplementation;->b:I

    .line 1162230
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1162223
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1162222
    new-instance v0, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1162219
    iget v0, p0, LX/1vt;->c:I

    .line 1162220
    move v0, v0

    .line 1162221
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1162216
    iget v0, p0, LX/1vt;->c:I

    .line 1162217
    move v0, v0

    .line 1162218
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1162213
    iget v0, p0, LX/1vt;->b:I

    .line 1162214
    move v0, v0

    .line 1162215
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1162210
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1162211
    move-object v0, v0

    .line 1162212
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1162198
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1162199
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1162200
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1162201
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1162202
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1162203
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1162204
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1162205
    invoke-static {v3, v9, v2}, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1162206
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1162207
    iget v0, p0, LX/1vt;->c:I

    .line 1162208
    move v0, v0

    .line 1162209
    return v0
.end method
