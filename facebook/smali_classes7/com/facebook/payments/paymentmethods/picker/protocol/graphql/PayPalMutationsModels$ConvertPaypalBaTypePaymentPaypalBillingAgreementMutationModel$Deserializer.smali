.class public final Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$ConvertPaypalBaTypePaymentPaypalBillingAgreementMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1162102
    const-class v0, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$ConvertPaypalBaTypePaymentPaypalBillingAgreementMutationModel;

    new-instance v1, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$ConvertPaypalBaTypePaymentPaypalBillingAgreementMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$ConvertPaypalBaTypePaymentPaypalBillingAgreementMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1162103
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1162104
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1162105
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1162106
    const/4 v2, 0x0

    .line 1162107
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1162108
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1162109
    :goto_0
    move v1, v2

    .line 1162110
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1162111
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1162112
    new-instance v1, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$ConvertPaypalBaTypePaymentPaypalBillingAgreementMutationModel;

    invoke-direct {v1}, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$ConvertPaypalBaTypePaymentPaypalBillingAgreementMutationModel;-><init>()V

    .line 1162113
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1162114
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1162115
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1162116
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1162117
    :cond_0
    return-object v1

    .line 1162118
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1162119
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1162120
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1162121
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1162122
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1162123
    const-string v4, "billing_agreement"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1162124
    const/4 v3, 0x0

    .line 1162125
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1162126
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1162127
    :goto_2
    move v1, v3

    .line 1162128
    goto :goto_1

    .line 1162129
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1162130
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1162131
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1162132
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1162133
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_8

    .line 1162134
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1162135
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1162136
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v5, :cond_6

    .line 1162137
    const-string p0, "billing_agreement"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1162138
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    .line 1162139
    :cond_7
    const-string p0, "billing_agreement_type"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1162140
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPaypalBillingAgreementTypeEnum;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    goto :goto_3

    .line 1162141
    :cond_8
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1162142
    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1162143
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1162144
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    move v4, v3

    goto :goto_3
.end method
