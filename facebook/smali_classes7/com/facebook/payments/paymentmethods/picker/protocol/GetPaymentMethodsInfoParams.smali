.class public Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6xg;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Lorg/json/JSONObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lcom/facebook/common/locale/Country;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1162008
    new-instance v0, LX/70B;

    invoke-direct {v0}, LX/70B;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/70C;)V
    .locals 2

    .prologue
    .line 1162009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1162010
    iget-object v0, p1, LX/70C;->a:LX/6xg;

    sget-object v1, LX/6xg;->INVOICE:LX/6xg;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, LX/70C;->e:Lcom/facebook/common/locale/Country;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1162011
    iget-object v0, p1, LX/70C;->a:LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a:LX/6xg;

    .line 1162012
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a:LX/6xg;

    iget-object v1, p1, LX/70C;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/73n;->a(LX/6xg;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->b:Ljava/lang/String;

    .line 1162013
    iget-object v0, p1, LX/70C;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->c:Ljava/lang/String;

    .line 1162014
    iget-object v0, p1, LX/70C;->d:Lorg/json/JSONObject;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->d:Lorg/json/JSONObject;

    .line 1162015
    iget-object v0, p1, LX/70C;->e:Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->e:Lcom/facebook/common/locale/Country;

    .line 1162016
    return-void

    .line 1162017
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 1162018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1162019
    const-class v0, LX/6xg;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a:LX/6xg;

    .line 1162020
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->b:Ljava/lang/String;

    .line 1162021
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->c:Ljava/lang/String;

    .line 1162022
    const/4 v1, 0x0

    .line 1162023
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 1162024
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1162025
    :goto_0
    move-object v0, v0

    .line 1162026
    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->d:Lorg/json/JSONObject;

    .line 1162027
    const-class v0, Lcom/facebook/common/locale/Country;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->e:Lcom/facebook/common/locale/Country;

    .line 1162028
    return-void

    :cond_0
    move-object v0, v1

    .line 1162029
    goto :goto_0

    .line 1162030
    :catch_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(LX/6xg;)LX/70C;
    .locals 2

    .prologue
    .line 1162031
    new-instance v0, LX/70C;

    invoke-direct {v0, p0}, LX/70C;-><init>(LX/6xg;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 4
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1162032
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 1162033
    :cond_0
    :goto_0
    return v0

    .line 1162034
    :cond_1
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    .line 1162035
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1162036
    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1162037
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1162038
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;

    if-nez v1, :cond_1

    .line 1162039
    :cond_0
    :goto_0
    return v0

    .line 1162040
    :cond_1
    check-cast p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;

    .line 1162041
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a:LX/6xg;

    iget-object v2, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a:LX/6xg;

    if-ne v1, v2, :cond_0

    .line 1162042
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 1162043
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->e:Lcom/facebook/common/locale/Country;

    iget-object v2, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->e:Lcom/facebook/common/locale/Country;

    invoke-static {v1, v2}, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->d:Lorg/json/JSONObject;

    iget-object v2, p1, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->d:Lorg/json/JSONObject;

    invoke-static {v1, v2}, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1162044
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1162045
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a:LX/6xg;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1162046
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1162047
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1162048
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->d:Lorg/json/JSONObject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->d:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1162049
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->e:Lcom/facebook/common/locale/Country;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1162050
    return-void

    .line 1162051
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
