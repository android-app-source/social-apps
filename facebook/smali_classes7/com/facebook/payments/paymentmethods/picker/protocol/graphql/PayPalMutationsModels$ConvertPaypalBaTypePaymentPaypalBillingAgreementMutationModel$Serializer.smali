.class public final Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$ConvertPaypalBaTypePaymentPaypalBillingAgreementMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$ConvertPaypalBaTypePaymentPaypalBillingAgreementMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1162165
    const-class v0, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$ConvertPaypalBaTypePaymentPaypalBillingAgreementMutationModel;

    new-instance v1, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$ConvertPaypalBaTypePaymentPaypalBillingAgreementMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$ConvertPaypalBaTypePaymentPaypalBillingAgreementMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1162166
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1162167
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$ConvertPaypalBaTypePaymentPaypalBillingAgreementMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1162146
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1162147
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1162148
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1162149
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1162150
    if-eqz v2, :cond_2

    .line 1162151
    const-string p0, "billing_agreement"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1162152
    const/4 p2, 0x1

    .line 1162153
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1162154
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1162155
    if-eqz p0, :cond_0

    .line 1162156
    const-string v0, "billing_agreement"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1162157
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1162158
    :cond_0
    invoke-virtual {v1, v2, p2}, LX/15i;->g(II)I

    move-result p0

    .line 1162159
    if-eqz p0, :cond_1

    .line 1162160
    const-string p0, "billing_agreement_type"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1162161
    invoke-virtual {v1, v2, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1162162
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1162163
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1162164
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1162145
    check-cast p1, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$ConvertPaypalBaTypePaymentPaypalBillingAgreementMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$ConvertPaypalBaTypePaymentPaypalBillingAgreementMutationModel$Serializer;->a(Lcom/facebook/payments/paymentmethods/picker/protocol/graphql/PayPalMutationsModels$ConvertPaypalBaTypePaymentPaypalBillingAgreementMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
