.class public final Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/picker/model/PickerScreenConfig;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/6zQ;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Z

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1161329
    new-instance v0, LX/6ze;

    invoke-direct {v0}, LX/6ze;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6zf;)V
    .locals 2

    .prologue
    .line 1161318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161319
    iget-object v0, p1, LX/6zf;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-object v0, v0

    .line 1161320
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 1161321
    iget-object v0, p1, LX/6zf;->b:LX/0Px;

    move-object v0, v0

    .line 1161322
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->b:LX/0Px;

    .line 1161323
    iget-boolean v0, p1, LX/6zf;->c:Z

    move v0, v0

    .line 1161324
    iput-boolean v0, p0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->c:Z

    .line 1161325
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    .line 1161326
    iget-object v1, p1, LX/6zf;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1161327
    invoke-static {v0, v1}, LX/73n;->a(LX/6xg;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->d:Ljava/lang/String;

    .line 1161328
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1161312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1161313
    const-class v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 1161314
    const-class v0, LX/6zQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->b:LX/0Px;

    .line 1161315
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->c:Z

    .line 1161316
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->d:Ljava/lang/String;

    .line 1161317
    return-void
.end method

.method public static newBuilder()LX/6zf;
    .locals 1

    .prologue
    .line 1161311
    new-instance v0, LX/6zf;

    invoke-direct {v0}, LX/6zf;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;
    .locals 1

    .prologue
    .line 1161310
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1161309
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1161304
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1161305
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1161306
    iget-boolean v0, p0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1161307
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1161308
    return-void
.end method
