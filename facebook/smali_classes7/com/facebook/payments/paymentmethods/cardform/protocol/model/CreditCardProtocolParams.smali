.class public abstract Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final d:LX/6xg;

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:I

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/6yz;)V
    .locals 1

    .prologue
    .line 1160643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160644
    iget-object v0, p1, LX/6yz;->a:LX/6xg;

    move-object v0, v0

    .line 1160645
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->d:LX/6xg;

    .line 1160646
    iget-object v0, p1, LX/6yz;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1160647
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->e:Ljava/lang/String;

    .line 1160648
    iget v0, p1, LX/6yz;->c:I

    move v0, v0

    .line 1160649
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->f:I

    .line 1160650
    iget v0, p1, LX/6yz;->d:I

    move v0, v0

    .line 1160651
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->g:I

    .line 1160652
    iget-object v0, p1, LX/6yz;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1160653
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->h:Ljava/lang/String;

    .line 1160654
    iget-object v0, p1, LX/6yz;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1160655
    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->i:Ljava/lang/String;

    .line 1160656
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1160665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160666
    const-class v0, LX/6xg;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->d:LX/6xg;

    .line 1160667
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->e:Ljava/lang/String;

    .line 1160668
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->f:I

    .line 1160669
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->g:I

    .line 1160670
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->h:Ljava/lang/String;

    .line 1160671
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->i:Ljava/lang/String;

    .line 1160672
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1160673
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1160674
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "combined_payment_type"

    iget-object v3, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->d:LX/6xg;

    invoke-virtual {v3}, LX/6xg;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1160675
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "csc"

    iget-object v3, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->e:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1160676
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "expiry_month"

    iget v3, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->f:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1160677
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "expiry_year"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "20"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->g:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1160678
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "zip"

    iget-object v3, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->h:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "country_code"

    iget-object v3, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->i:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1160679
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "billing_address"

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1160680
    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 1160664
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1160657
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->d:LX/6xg;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1160658
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160659
    iget v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1160660
    iget v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1160661
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160662
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160663
    return-void
.end method
