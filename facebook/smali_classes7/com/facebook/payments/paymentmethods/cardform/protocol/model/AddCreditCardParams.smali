.class public Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;
.super Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1160681
    const-class v0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;->a:Ljava/lang/String;

    .line 1160682
    new-instance v0, LX/6yy;

    invoke-direct {v0}, LX/6yy;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6z0;)V
    .locals 1

    .prologue
    .line 1160683
    invoke-direct {p0, p1}, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;-><init>(LX/6yz;)V

    .line 1160684
    iget-object v0, p1, LX/6z0;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1160685
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;->b:Ljava/lang/String;

    .line 1160686
    iget-object v0, p1, LX/6z0;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1160687
    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;->c:Ljava/lang/String;

    .line 1160688
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1160689
    invoke-direct {p0, p1}, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;-><init>(Landroid/os/Parcel;)V

    .line 1160690
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;->b:Ljava/lang/String;

    .line 1160691
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;->c:Ljava/lang/String;

    .line 1160692
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1160693
    invoke-super {p0}, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->a()Ljava/util/List;

    move-result-object v0

    .line 1160694
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "creditCardNumber"

    iget-object v3, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1160695
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->d:LX/6xg;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/73n;->a(LX/6xg;Ljava/lang/String;)Ljava/lang/String;

    .line 1160696
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->d:LX/6xg;

    invoke-static {v1}, LX/73n;->a(LX/6xg;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1160697
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "account_id"

    iget-object v3, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1160698
    :cond_0
    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1160699
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1160700
    invoke-super {p0, p1, p2}, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1160701
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160702
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/AddCreditCardParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160703
    return-void
.end method
