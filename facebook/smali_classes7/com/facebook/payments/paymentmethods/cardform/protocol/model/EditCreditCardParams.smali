.class public Lcom/facebook/payments/paymentmethods/cardform/protocol/model/EditCreditCardParams;
.super Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/cardform/protocol/model/EditCreditCardParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1160744
    const-class v0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/EditCreditCardParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/EditCreditCardParams;->a:Ljava/lang/String;

    .line 1160745
    new-instance v0, LX/6z2;

    invoke-direct {v0}, LX/6z2;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/EditCreditCardParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6z3;)V
    .locals 1

    .prologue
    .line 1160746
    invoke-direct {p0, p1}, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;-><init>(LX/6yz;)V

    .line 1160747
    iget-object v0, p1, LX/6z3;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1160748
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/EditCreditCardParams;->b:Ljava/lang/String;

    .line 1160749
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1160750
    invoke-direct {p0, p1}, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;-><init>(Landroid/os/Parcel;)V

    .line 1160751
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/EditCreditCardParams;->b:Ljava/lang/String;

    .line 1160752
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1160753
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1160754
    invoke-super {p0, p1, p2}, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/CreditCardProtocolParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1160755
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/EditCreditCardParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160756
    return-void
.end method
