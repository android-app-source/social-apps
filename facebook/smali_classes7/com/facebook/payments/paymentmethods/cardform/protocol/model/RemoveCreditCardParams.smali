.class public Lcom/facebook/payments/paymentmethods/cardform/protocol/model/RemoveCreditCardParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/cardform/protocol/model/RemoveCreditCardParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1160769
    const-class v0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/RemoveCreditCardParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/RemoveCreditCardParams;->a:Ljava/lang/String;

    .line 1160770
    new-instance v0, LX/6z4;

    invoke-direct {v0}, LX/6z4;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/RemoveCreditCardParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1160766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160767
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/RemoveCreditCardParams;->b:Ljava/lang/String;

    .line 1160768
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1160763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160764
    iput-object p1, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/RemoveCreditCardParams;->b:Ljava/lang/String;

    .line 1160765
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1160762
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1160760
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/protocol/model/RemoveCreditCardParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1160761
    return-void
.end method
