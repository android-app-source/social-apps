.class public Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/6yZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

.field public g:LX/6y1;

.field public h:LX/6yL;

.field public i:LX/6qh;

.field public j:Lcom/google/common/util/concurrent/ListenableFuture;

.field public k:Lcom/google/common/util/concurrent/ListenableFuture;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1159913
    const-class v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    sput-object v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->e:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1159914
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1159915
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/6yZ;->a(LX/0QB;)LX/6yZ;

    move-result-object p0

    check-cast p0, LX/6yZ;

    iput-object v1, p1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->a:LX/0Zb;

    iput-object v2, p1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->b:Ljava/util/concurrent/Executor;

    iput-object v3, p1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->c:LX/03V;

    iput-object p0, p1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->d:LX/6yZ;

    return-void
.end method

.method public static b(Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)LX/6xu;
    .locals 2

    .prologue
    .line 1159916
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->d:LX/6yZ;

    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v0, v1}, LX/6yZ;->b(LX/6yO;)LX/6xu;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1159917
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1159918
    const-class v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    invoke-static {v0, p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1159919
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1159920
    const-string v1, "card_form_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    .line 1159921
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->d:LX/6yZ;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    .line 1159922
    iget-object v2, v0, LX/6yZ;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/6yZ;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6yP;

    iget-object v2, v2, LX/6yP;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    :goto_0
    check-cast v2, LX/6yL;

    check-cast v2, LX/6yL;

    move-object v0, v2

    .line 1159923
    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->h:LX/6yL;

    .line 1159924
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->h:LX/6yL;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->i:LX/6qh;

    invoke-interface {v0, v1}, LX/6xz;->a(LX/6qh;)V

    .line 1159925
    return-void

    :cond_0
    iget-object v2, v0, LX/6yZ;->a:LX/0P1;

    sget-object p1, LX/6yO;->SIMPLE:LX/6yO;

    invoke-virtual {v2, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6yP;

    iget-object v2, v2, LX/6yP;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/16 v0, 0x2a

    const v1, -0x105d1b1a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1159926
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1159927
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 1159928
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1159929
    iput-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1159930
    :cond_0
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_1

    .line 1159931
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1159932
    iput-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1159933
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x17acdcf6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
