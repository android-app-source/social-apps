.class public Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/6yZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/6xb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Landroid/content/Context;

.field public h:Landroid/widget/LinearLayout;

.field public i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public l:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public m:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

.field private n:Landroid/widget/ProgressBar;

.field public o:LX/0h5;

.field public p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

.field public q:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

.field public r:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

.field public s:LX/6yS;

.field public t:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final u:LX/108;

.field public final v:LX/6y1;

.field public final w:LX/6qh;

.field private final x:LX/6y3;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1159369
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1159370
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const/4 v1, 0x0

    .line 1159371
    iput-boolean v1, v0, LX/108;->d:Z

    .line 1159372
    move-object v0, v0

    .line 1159373
    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->u:LX/108;

    .line 1159374
    new-instance v0, LX/6y1;

    invoke-direct {v0, p0}, LX/6y1;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->v:LX/6y1;

    .line 1159375
    new-instance v0, LX/6y2;

    invoke-direct {v0, p0}, LX/6y2;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->w:LX/6qh;

    .line 1159376
    new-instance v0, LX/6y3;

    invoke-direct {v0, p0}, LX/6y3;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->x:LX/6y3;

    return-void
.end method

.method private l()V
    .locals 7

    .prologue
    .line 1159377
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "card_form_input_controller_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    .line 1159378
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    if-nez v0, :cond_0

    .line 1159379
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    .line 1159380
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1159381
    const-string v2, "card_form_params"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1159382
    new-instance v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-direct {v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;-><init>()V

    .line 1159383
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1159384
    move-object v0, v2

    .line 1159385
    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    .line 1159386
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    const-string v2, "card_form_input_controller_fragment"

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1159387
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->w:LX/6qh;

    .line 1159388
    iput-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->q:LX/6qh;

    .line 1159389
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->x:LX/6y3;

    .line 1159390
    iput-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    .line 1159391
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v3, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->l:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v5, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->m:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    const/4 p0, 0x4

    .line 1159392
    iput-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->r:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1159393
    iget-object v6, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->r:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v6, p0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputType(I)V

    .line 1159394
    iput-object v2, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->s:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1159395
    iget-object v6, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->s:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v6, p0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputType(I)V

    .line 1159396
    iput-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->t:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1159397
    iget-object v6, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->t:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v6, p0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputType(I)V

    .line 1159398
    iput-object v4, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->u:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1159399
    iget-object v6, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->u:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v6, p0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputType(I)V

    .line 1159400
    iput-object v5, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->v:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    .line 1159401
    return-void
.end method

.method public static o(Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;)V
    .locals 4

    .prologue
    .line 1159402
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->b:Ljava/lang/String;

    const v1, 0x7f081e78

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1159403
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->d:LX/23P;

    .line 1159404
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1159405
    invoke-virtual {v1, v0, v2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1159406
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    if-nez v0, :cond_0

    .line 1159407
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->a:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081e79

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1159408
    :goto_0
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->o:LX/0h5;

    invoke-interface {v2, v0}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1159409
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->u:LX/108;

    .line 1159410
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 1159411
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->o:LX/0h5;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->u:LX/108;

    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1159412
    return-void

    .line 1159413
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->a:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081e7a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public static p(Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1159414
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->n:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1159415
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->h:Landroid/widget/LinearLayout;

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1159416
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->a(Z)V

    .line 1159417
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 1159418
    if-eqz v0, :cond_0

    .line 1159419
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1159420
    :cond_0
    return-void
.end method

.method public static q(Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;)V
    .locals 2

    .prologue
    .line 1159421
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->n:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1159422
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->h:Landroid/widget/LinearLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1159423
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->a(Z)V

    .line 1159424
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 1159425
    if-eqz v0, :cond_0

    .line 1159426
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1159427
    :cond_0
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 6

    .prologue
    .line 1159428
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    .line 1159429
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->i:LX/6yZ;

    iget-object v2, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v1, v2}, LX/6yZ;->b(LX/6yO;)LX/6xu;

    move-result-object v1

    .line 1159430
    iget-object v2, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->j:LX/0Zb;

    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v3}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    iget-object v4, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v1, v4}, LX/6xu;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/6xt;

    move-result-object v1

    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b()I

    move-result v3

    .line 1159431
    iget-object v4, v1, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    const-string v5, "card_number_digits"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1159432
    move-object v1, v1

    .line 1159433
    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b()I

    move-result v3

    .line 1159434
    iget-object v4, v1, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    const-string v5, "expiration_date_digits"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1159435
    move-object v1, v1

    .line 1159436
    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b()I

    move-result v3

    .line 1159437
    iget-object v4, v1, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    const-string v5, "csc_digits"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1159438
    move-object v1, v1

    .line 1159439
    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b()I

    move-result v3

    .line 1159440
    iget-object v4, v1, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    const-string v5, "billing_zip_digits"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1159441
    move-object v1, v1

    .line 1159442
    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v3

    .line 1159443
    iget-object v4, v1, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    const-string v5, "is_card_number_valid"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1159444
    move-object v1, v1

    .line 1159445
    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v3

    .line 1159446
    iget-object v4, v1, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    const-string v5, "is_expiration_date_valid"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1159447
    move-object v1, v1

    .line 1159448
    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v3

    .line 1159449
    iget-object v4, v1, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    const-string v5, "is_csc_valid"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1159450
    move-object v1, v1

    .line 1159451
    iget-object v3, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v3}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v3

    .line 1159452
    iget-object v4, v1, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    const-string v5, "is_billing_zip_valid"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1159453
    move-object v1, v1

    .line 1159454
    iget-object v3, v1, LX/6xt;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    move-object v1, v3

    .line 1159455
    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1159456
    const-string v0, "payflows_cancel"

    .line 1159457
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->f:LX/6xb;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v3, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v3}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->c:LX/6xZ;

    invoke-virtual {v1, v2, v3, v0}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/String;)V

    .line 1159458
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1159459
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1159460
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v2, 0x7f0e0326

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->g:Landroid/content/Context;

    .line 1159461
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->g:Landroid/content/Context;

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/6yZ;->a(LX/0QB;)LX/6yZ;

    move-result-object v5

    check-cast v5, LX/6yZ;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v6

    check-cast v6, LX/23P;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/6xb;->a(LX/0QB;)LX/6xb;

    move-result-object v0

    check-cast v0, LX/6xb;

    iput-object v3, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->a:LX/0Zb;

    iput-object v4, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v5, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->c:LX/6yZ;

    iput-object v6, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->d:LX/23P;

    iput-object v7, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->e:Ljava/util/concurrent/Executor;

    iput-object v0, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->f:LX/6xb;

    .line 1159462
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 1159463
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1159464
    const-string v1, "card_form_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    .line 1159465
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->f:LX/6xb;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->c:LX/6xg;

    iget-object v3, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v3}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->c:LX/6xZ;

    invoke-virtual {v0, v1, v2, v3, p1}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xg;LX/6xZ;Landroid/os/Bundle;)V

    .line 1159466
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4dc38788    # 4.10054912E8f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1159467
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->g:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03023c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x4c962745

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x54426f99

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1159468
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1159469
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 1159470
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1159471
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1159472
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x547d25dc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x19625317

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1159473
    iput-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->h:Landroid/widget/LinearLayout;

    .line 1159474
    iput-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1159475
    iput-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1159476
    iput-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1159477
    iput-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->l:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1159478
    iput-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->m:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    .line 1159479
    iput-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->n:Landroid/widget/ProgressBar;

    .line 1159480
    iput-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->o:LX/0h5;

    .line 1159481
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1159482
    const/16 v1, 0x2b

    const v2, 0x3496c824

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1159483
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1159484
    const v0, 0x7f0d08a0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->h:Landroid/widget/LinearLayout;

    .line 1159485
    const v0, 0x7f0d018c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1159486
    const v0, 0x7f0d08a1

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1159487
    const v0, 0x7f0d018d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1159488
    const v0, 0x7f0d08a2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->l:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1159489
    const v0, 0x7f0d08a3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->m:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    .line 1159490
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->n:Landroid/widget/ProgressBar;

    .line 1159491
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v1, "card_form_mutator_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->r:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    .line 1159492
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->r:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    if-nez v0, :cond_0

    .line 1159493
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    .line 1159494
    new-instance v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    invoke-direct {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;-><init>()V

    .line 1159495
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 1159496
    const-string p2, "card_form_params"

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1159497
    invoke-virtual {v1, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1159498
    move-object v0, v1

    .line 1159499
    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->r:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    .line 1159500
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->r:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    const-string p1, "card_form_mutator_fragment"

    invoke-virtual {v0, v1, p1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1159501
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->r:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->v:LX/6y1;

    .line 1159502
    iput-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->g:LX/6y1;

    .line 1159503
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->r:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->w:LX/6qh;

    .line 1159504
    iput-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->i:LX/6qh;

    .line 1159505
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->c:LX/6yZ;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    .line 1159506
    iget-object p1, v0, LX/6yZ;->a:LX/0P1;

    invoke-virtual {p1, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, v0, LX/6yZ;->a:LX/0P1;

    invoke-virtual {p1, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/6yP;

    iget-object p1, p1, LX/6yP;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    :goto_0
    check-cast p1, LX/6yS;

    check-cast p1, LX/6yS;

    move-object v0, p1

    .line 1159507
    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->s:LX/6yS;

    .line 1159508
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->s:LX/6yS;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->w:LX/6qh;

    invoke-interface {v0, v1}, LX/6xz;->a(LX/6qh;)V

    .line 1159509
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->s:LX/6yS;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->h:Landroid/widget/LinearLayout;

    iget-object p1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0, v1, p1}, LX/6yS;->a(Landroid/view/ViewGroup;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)LX/6E6;

    move-result-object v0

    .line 1159510
    if-eqz v0, :cond_1

    .line 1159511
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->h:Landroid/widget/LinearLayout;

    check-cast v0, Landroid/view/View;

    const/4 p1, 0x0

    invoke-virtual {v1, v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1159512
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->s:LX/6yS;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->h:Landroid/widget/LinearLayout;

    iget-object p1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0, v1, p1}, LX/6yS;->b(Landroid/view/ViewGroup;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)LX/6E6;

    move-result-object v0

    .line 1159513
    if-eqz v0, :cond_2

    .line 1159514
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->h:Landroid/widget/LinearLayout;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1159515
    :cond_2
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->l()V

    .line 1159516
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1159517
    const v1, 0x7f0d00bb

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 1159518
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    iget-object p1, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1159519
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1159520
    check-cast v2, Landroid/view/ViewGroup;

    new-instance p2, LX/6y4;

    invoke-direct {p2, p0, v0}, LX/6y4;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;Landroid/app/Activity;)V

    iget-object v0, p1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    iget-object p1, p1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-virtual {p1}, LX/6ws;->getTitleBarNavIconStyle()LX/73h;

    move-result-object p1

    invoke-virtual {v1, v2, p2, v0, p1}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 1159521
    iget-object v0, v1, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    move-object v0, v0

    .line 1159522
    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->o:LX/0h5;

    .line 1159523
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->o:LX/0h5;

    new-instance v1, LX/6y5;

    invoke-direct {v1, p0}, LX/6y5;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1159524
    invoke-static {p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->o(Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;)V

    .line 1159525
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "payments_component_dialog_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/dialogs/FbDialogFragment;

    .line 1159526
    if-eqz v0, :cond_3

    instance-of v1, v0, LX/6xz;

    if-eqz v1, :cond_3

    .line 1159527
    check-cast v0, LX/6xz;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->w:LX/6qh;

    invoke-interface {v0, v1}, LX/6xz;->a(LX/6qh;)V

    .line 1159528
    :cond_3
    return-void

    :cond_4
    iget-object p1, v0, LX/6yZ;->a:LX/0P1;

    sget-object p2, LX/6yO;->SIMPLE:LX/6yO;

    invoke-virtual {p1, p2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/6yP;

    iget-object p1, p1, LX/6yP;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    goto/16 :goto_0
.end method
