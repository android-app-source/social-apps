.class public Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public A:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

.field public final B:LX/6wv;

.field public a:LX/6z7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/6yo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/6zA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/6yp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/6zC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/6yq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/6z5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/6yn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/6yZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/73a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/6xb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

.field public n:Z

.field private o:Lcom/google/common/util/concurrent/ListenableFuture;

.field public p:LX/6y3;

.field public q:LX/6qh;

.field public r:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public s:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public t:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public u:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public v:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

.field public w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

.field public x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

.field public y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

.field public z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1159886
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1159887
    new-instance v0, LX/6yC;

    invoke-direct {v0, p0}, LX/6yC;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->B:LX/6wv;

    return-void
.end method

.method public static b$redex0(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1159884
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->l:LX/6xb;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->c:LX/6xZ;

    invoke-virtual {v0, v1, v2, p1}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/String;)V

    .line 1159885
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1159880
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1159881
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1159882
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1159883
    :cond_0
    return-void
.end method

.method public static x(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)LX/6z9;
    .locals 3

    .prologue
    .line 1159879
    new-instance v0, LX/6z9;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->r:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-direct {v0, v1, v2}, LX/6z9;-><init>(Ljava/lang/String;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)V

    return-object v0
.end method

.method public static y(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)LX/6z8;
    .locals 2

    .prologue
    .line 1159682
    new-instance v0, LX/6zF;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->s:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6zF;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static z(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)LX/6zD;
    .locals 3

    .prologue
    .line 1159876
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;->g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v0

    .line 1159877
    :goto_0
    new-instance v1, LX/6zD;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->t:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/6zD;-><init>(Ljava/lang/String;Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)V

    return-object v1

    .line 1159878
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->r:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6yU;->a(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 1159873
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1159874
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;

    new-instance v4, LX/6z7;

    invoke-static {v0}, LX/6yZ;->a(LX/0QB;)LX/6yZ;

    move-result-object v3

    check-cast v3, LX/6yZ;

    invoke-direct {v4, v3}, LX/6z7;-><init>(LX/6yZ;)V

    move-object v3, v4

    check-cast v3, LX/6z7;

    invoke-static {v0}, LX/6yo;->a(LX/0QB;)LX/6yo;

    move-result-object v4

    check-cast v4, LX/6yo;

    new-instance v7, LX/6zA;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-direct {v7, v5, v6}, LX/6zA;-><init>(LX/0SG;Landroid/content/res/Resources;)V

    move-object v5, v7

    check-cast v5, LX/6zA;

    invoke-static {v0}, LX/6yp;->a(LX/0QB;)LX/6yp;

    move-result-object v6

    check-cast v6, LX/6yp;

    invoke-static {v0}, LX/6zC;->b(LX/0QB;)LX/6zC;

    move-result-object v7

    check-cast v7, LX/6zC;

    invoke-static {v0}, LX/6yq;->a(LX/0QB;)LX/6yq;

    move-result-object v8

    check-cast v8, LX/6yq;

    invoke-static {v0}, LX/6z5;->b(LX/0QB;)LX/6z5;

    move-result-object v9

    check-cast v9, LX/6z5;

    invoke-static {v0}, LX/6yn;->a(LX/0QB;)LX/6yn;

    move-result-object v10

    check-cast v10, LX/6yn;

    invoke-static {v0}, LX/6yZ;->a(LX/0QB;)LX/6yZ;

    move-result-object v11

    check-cast v11, LX/6yZ;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v12

    check-cast v12, LX/0Zb;

    invoke-static {v0}, LX/73a;->b(LX/0QB;)LX/73a;

    move-result-object p1

    check-cast p1, LX/73a;

    invoke-static {v0}, LX/6xb;->a(LX/0QB;)LX/6xb;

    move-result-object v0

    check-cast v0, LX/6xb;

    iput-object v3, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->a:LX/6z7;

    iput-object v4, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->b:LX/6yo;

    iput-object v5, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->c:LX/6zA;

    iput-object v6, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->d:LX/6yp;

    iput-object v7, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->e:LX/6zC;

    iput-object v8, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->f:LX/6yq;

    iput-object v9, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->g:LX/6z5;

    iput-object v10, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->h:LX/6yn;

    iput-object v11, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->i:LX/6yZ;

    iput-object v12, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->j:LX/0Zb;

    iput-object p1, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->k:LX/73a;

    iput-object v0, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->l:LX/6xb;

    .line 1159875
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1159867
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->r:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setEnabled(Z)V

    .line 1159868
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->s:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setEnabled(Z)V

    .line 1159869
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->t:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setEnabled(Z)V

    .line 1159870
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->u:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setEnabled(Z)V

    .line 1159871
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->v:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->setEnabled(Z)V

    .line 1159872
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    .line 1159835
    const-string v0, "payflows_save_click"

    invoke-static {p0, v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->b$redex0(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;Ljava/lang/String;)V

    .line 1159836
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e()V

    .line 1159837
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e()V

    .line 1159838
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e()V

    .line 1159839
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e()V

    .line 1159840
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1159841
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    .line 1159842
    iget-object v1, v0, LX/6y3;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->r:Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;

    .line 1159843
    new-instance v2, LX/6y9;

    invoke-direct {v2}, LX/6y9;-><init>()V

    move-object v2, v2

    .line 1159844
    iget-object v3, v0, LX/6y3;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v3}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v3

    .line 1159845
    iput-object v3, v2, LX/6y9;->a:Ljava/lang/String;

    .line 1159846
    move-object v2, v2

    .line 1159847
    iget-object v3, v0, LX/6y3;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v3}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v3

    .line 1159848
    iput-object v3, v2, LX/6y9;->c:Ljava/lang/String;

    .line 1159849
    move-object v2, v2

    .line 1159850
    iget-object v3, v0, LX/6y3;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v3}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v3

    .line 1159851
    iput-object v3, v2, LX/6y9;->f:Ljava/lang/String;

    .line 1159852
    move-object v2, v2

    .line 1159853
    iget-object v3, v0, LX/6y3;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->l:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v3}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v3

    .line 1159854
    iput-object v3, v2, LX/6y9;->g:Ljava/lang/String;

    .line 1159855
    move-object v2, v2

    .line 1159856
    iget-object v3, v0, LX/6y3;->a:Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;->m:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    invoke-virtual {v3}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->getSelectedCountry()Lcom/facebook/common/locale/Country;

    move-result-object v3

    .line 1159857
    iput-object v3, v2, LX/6y9;->h:Lcom/facebook/common/locale/Country;

    .line 1159858
    move-object v2, v2

    .line 1159859
    invoke-virtual {v2}, LX/6y9;->i()LX/6y8;

    move-result-object v2

    .line 1159860
    iget-object v3, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v3}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1159861
    :goto_0
    const/4 v0, 0x1

    .line 1159862
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1159863
    :cond_1
    iget-object v3, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->g:LX/6y1;

    invoke-virtual {v3}, LX/6y1;->a()V

    .line 1159864
    iget-object v3, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->h:LX/6yL;

    iget-object v4, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v3, v4, v2}, LX/6yL;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/6y8;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    iput-object v3, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1159865
    iget-object v3, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v4, LX/6yN;

    invoke-direct {v4, v1, v2}, LX/6yN;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;LX/6y8;)V

    iget-object p0, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->b:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1159866
    iget-object v3, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->a:LX/0Zb;

    iget-object v4, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v4}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    iget-object p0, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-static {v1, p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->b(Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)LX/6xu;

    move-result-object p0

    iget-object v0, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormMutatorFragment;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {p0, v0}, LX/6xu;->b(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object p0

    invoke-static {v4, p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1159834
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1159823
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->n:Z

    .line 1159824
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->l()V

    .line 1159825
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d()V

    .line 1159826
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d()V

    .line 1159827
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d()V

    .line 1159828
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d()V

    .line 1159829
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->A:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    .line 1159830
    iget-object v1, v0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->c:Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;

    .line 1159831
    iget-object p0, v1, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;->a:Lcom/facebook/common/locale/Country;

    move-object v1, p0

    .line 1159832
    invoke-static {v0, v1}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->a$redex0(Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;Lcom/facebook/common/locale/Country;)V

    .line 1159833
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7353511a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1159697
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1159698
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1159699
    const-string v2, "card_form_params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    .line 1159700
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->i:LX/6yZ;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v0, v2}, LX/6yZ;->d(LX/6yO;)LX/6yT;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->q:LX/6qh;

    invoke-interface {v0, v2}, LX/6xz;->a(LX/6qh;)V

    .line 1159701
    new-instance v0, LX/6yD;

    invoke-direct {v0, p0}, LX/6yD;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V

    .line 1159702
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->r:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1159703
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->s:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1159704
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->t:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1159705
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->u:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1159706
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v2, "card_number_input_controller_fragment_tag"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/controller/CardNumberInputControllerFragment;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1159707
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    if-nez v0, :cond_0

    .line 1159708
    new-instance v0, Lcom/facebook/payments/paymentmethods/cardform/controller/CardNumberInputControllerFragment;

    invoke-direct {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/CardNumberInputControllerFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1159709
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const-string v4, "card_number_input_controller_fragment_tag"

    invoke-virtual {v0, v2, v4}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1159710
    :cond_0
    new-instance v0, LX/6yE;

    invoke-direct {v0, p0}, LX/6yE;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V

    .line 1159711
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->r:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v5, 0x7f0d018e

    invoke-virtual {v2, v4, v5}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;I)V

    .line 1159712
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->b:LX/6yo;

    .line 1159713
    iput-object v4, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b:Landroid/text/TextWatcher;

    .line 1159714
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->a:LX/6z7;

    .line 1159715
    iput-object v4, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->c:LX/6wl;

    .line 1159716
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1159717
    iput-object v0, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d:Landroid/text/TextWatcher;

    .line 1159718
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->w:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    new-instance v2, LX/6yF;

    invoke-direct {v2, p0}, LX/6yF;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V

    .line 1159719
    iput-object v2, v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a:LX/6vE;

    .line 1159720
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v2, "exp_date_input_controller_fragment_tag"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1159721
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    if-nez v0, :cond_1

    .line 1159722
    new-instance v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-direct {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1159723
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const-string v4, "exp_date_input_controller_fragment_tag"

    invoke-virtual {v0, v2, v4}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1159724
    :cond_1
    new-instance v0, LX/6yG;

    invoke-direct {v0, p0}, LX/6yG;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V

    .line 1159725
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->s:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v5, 0x7f0d018f

    invoke-virtual {v2, v4, v5}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;I)V

    .line 1159726
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->d:LX/6yp;

    .line 1159727
    iput-object v4, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b:Landroid/text/TextWatcher;

    .line 1159728
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->c:LX/6zA;

    .line 1159729
    iput-object v4, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->c:LX/6wl;

    .line 1159730
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1159731
    iput-object v0, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d:Landroid/text/TextWatcher;

    .line 1159732
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    new-instance v2, LX/6yH;

    invoke-direct {v2, p0}, LX/6yH;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V

    .line 1159733
    iput-object v2, v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a:LX/6vE;

    .line 1159734
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v2, "security_code_input_controller_fragment_tag"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1159735
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    if-nez v0, :cond_2

    .line 1159736
    new-instance v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-direct {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1159737
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const-string v4, "security_code_input_controller_fragment_tag"

    invoke-virtual {v0, v2, v4}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1159738
    :cond_2
    new-instance v0, LX/6yI;

    invoke-direct {v0, p0}, LX/6yI;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V

    .line 1159739
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->t:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v5, 0x7f0d0190

    invoke-virtual {v2, v4, v5}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;I)V

    .line 1159740
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->f:LX/6yq;

    .line 1159741
    iput-object v4, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b:Landroid/text/TextWatcher;

    .line 1159742
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->e:LX/6zC;

    .line 1159743
    iput-object v4, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->c:LX/6wl;

    .line 1159744
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1159745
    iput-object v0, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d:Landroid/text/TextWatcher;

    .line 1159746
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    new-instance v2, LX/6yJ;

    invoke-direct {v2, p0}, LX/6yJ;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V

    .line 1159747
    iput-object v2, v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a:LX/6vE;

    .line 1159748
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v2, "billing_zip_input_controller_fragment_tag"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1159749
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    if-nez v0, :cond_3

    .line 1159750
    new-instance v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-direct {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1159751
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const-string v4, "billing_zip_input_controller_fragment_tag"

    invoke-virtual {v0, v2, v4}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1159752
    :cond_3
    new-instance v0, LX/6yK;

    invoke-direct {v0, p0}, LX/6yK;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V

    .line 1159753
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->u:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v5, 0x7f0d0191

    invoke-virtual {v2, v4, v5}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;I)V

    .line 1159754
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->h:LX/6yn;

    .line 1159755
    iput-object v4, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b:Landroid/text/TextWatcher;

    .line 1159756
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->g:LX/6z5;

    .line 1159757
    iput-object v4, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->c:LX/6wl;

    .line 1159758
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1159759
    iput-object v0, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d:Landroid/text/TextWatcher;

    .line 1159760
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    new-instance v2, LX/6yA;

    invoke-direct {v2, p0}, LX/6yA;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V

    .line 1159761
    iput-object v2, v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a:LX/6vE;

    .line 1159762
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->f:Z

    if-eqz v0, :cond_c

    .line 1159763
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->v:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->setVisibility(I)V

    .line 1159764
    :goto_0
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    .line 1159765
    if-nez v0, :cond_10

    .line 1159766
    :goto_1
    const/4 v2, 0x1

    .line 1159767
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->i:LX/6yZ;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v4}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v0, v4}, LX/6yZ;->c(LX/6yO;)LX/6y0;

    move-result-object v4

    .line 1159768
    const/4 v0, 0x0

    .line 1159769
    iget-object v5, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v4, v5}, LX/6y0;->d(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1159770
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->y:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    move v0, v2

    .line 1159771
    :cond_4
    iget-object v5, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v4, v5}, LX/6y0;->e(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1159772
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d()V

    .line 1159773
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->x:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    move v0, v2

    .line 1159774
    :cond_5
    iget-object v5, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v4, v5}, LX/6y0;->f(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 1159775
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d()V

    .line 1159776
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->z:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    .line 1159777
    :goto_2
    if-eqz v2, :cond_6

    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    if-eqz v0, :cond_6

    .line 1159778
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->p:LX/6y3;

    invoke-virtual {v0}, LX/6y3;->a()V

    .line 1159779
    :cond_6
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->i:LX/6yZ;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v0, v2}, LX/6yZ;->c(LX/6yO;)LX/6y0;

    move-result-object v0

    .line 1159780
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->s:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0, v4}, LX/6y0;->g(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setEnabled(Z)V

    .line 1159781
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->t:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0, v4}, LX/6y0;->h(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setEnabled(Z)V

    .line 1159782
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->u:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0, v4}, LX/6y0;->i(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setEnabled(Z)V

    .line 1159783
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->v:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v2, v0}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->setEnabled(Z)V

    .line 1159784
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v2, "unsupported_association_dialog"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    .line 1159785
    if-eqz v0, :cond_7

    .line 1159786
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->B:LX/6wv;

    .line 1159787
    iput-object v2, v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 1159788
    :cond_7
    if-eqz p1, :cond_b

    .line 1159789
    const-string v0, "has_made_first_issuer_mistake"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->n:Z

    .line 1159790
    const-string v0, "card_number_edit_text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1159791
    const-string v2, "expiration_date_edit_text"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1159792
    const-string v4, "security_code_edit_text"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1159793
    const-string v5, "billing_zip_edit_text"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1159794
    if-eqz v0, :cond_8

    .line 1159795
    iget-object v6, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->r:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v6, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1159796
    :cond_8
    if-eqz v2, :cond_9

    .line 1159797
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->s:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1159798
    :cond_9
    if-eqz v4, :cond_a

    .line 1159799
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->t:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1159800
    :cond_a
    if-eqz v5, :cond_b

    .line 1159801
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->u:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1159802
    :cond_b
    const/16 v0, 0x2b

    const v2, -0x7e1fd4f5

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1159803
    :cond_c
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->v:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->setVisibility(I)V

    .line 1159804
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v2, "country_selector_component_controller_tag"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->A:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    .line 1159805
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->A:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    if-nez v0, :cond_e

    .line 1159806
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;->j()Lcom/facebook/common/locale/Country;

    move-result-object v0

    if-nez v0, :cond_f

    :cond_d
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->g:Lcom/facebook/common/locale/Country;

    .line 1159807
    :goto_4
    invoke-static {}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;->newBuilder()LX/6uD;

    move-result-object v2

    .line 1159808
    iput-object v0, v2, LX/6uD;->b:Lcom/facebook/common/locale/Country;

    .line 1159809
    move-object v0, v2

    .line 1159810
    invoke-virtual {v0}, LX/6uD;->a()Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;

    move-result-object v0

    .line 1159811
    invoke-static {v0}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->a(Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;)Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->A:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    .line 1159812
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->A:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    const-string v4, "country_selector_component_controller_tag"

    invoke-virtual {v0, v2, v4}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1159813
    :cond_e
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->v:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->A:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->setComponentController(Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;)V

    .line 1159814
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->A:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    new-instance v2, LX/6yB;

    invoke-direct {v2, p0}, LX/6yB;-><init>(Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->a(LX/6u8;)V

    goto/16 :goto_0

    .line 1159815
    :cond_f
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;->j()Lcom/facebook/common/locale/Country;

    move-result-object v0

    goto :goto_4

    .line 1159816
    :cond_10
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->r:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1159817
    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;->g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v4

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;->f()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/6yU;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 1159818
    invoke-virtual {v2, v4}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1159819
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->s:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-static {v0}, LX/73p;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1159820
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->u:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1159821
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->r:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->b()V

    goto/16 :goto_1

    :cond_11
    move v2, v0

    goto/16 :goto_2

    .line 1159822
    :cond_12
    const/4 v0, 0x0

    goto/16 :goto_3
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x69f82456

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1159694
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1159695
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->l()V

    .line 1159696
    const/16 v1, 0x2b

    const v2, 0x77488f6b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1159683
    const-string v0, "has_made_first_issuer_mistake"

    iget-boolean v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1159684
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->r:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1159685
    const-string v0, "card_number_edit_text"

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->r:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159686
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->s:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1159687
    const-string v0, "expiration_date_edit_text"

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->s:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159688
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->t:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1159689
    const-string v0, "security_code_edit_text"

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->t:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159690
    :cond_2
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->u:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1159691
    const-string v0, "billing_zip_edit_text"

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormInputControllerFragment;->u:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159692
    :cond_3
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1159693
    return-void
.end method
