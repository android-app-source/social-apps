.class public Lcom/facebook/payments/paymentmethods/cardform/controller/CardNumberInputControllerFragment;
.super Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1160382
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1160383
    invoke-super {p0, p1}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a(Landroid/os/Bundle;)V

    .line 1160384
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    .line 1160385
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1160386
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    move-object v0, v0

    .line 1160387
    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    .line 1160388
    invoke-static {v0}, LX/6yU;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1160389
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    return v0
.end method
