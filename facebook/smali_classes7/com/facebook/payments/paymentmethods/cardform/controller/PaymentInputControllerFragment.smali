.class public Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:LX/6vE;

.field public b:Landroid/text/TextWatcher;

.field public c:LX/6wl;

.field public d:Landroid/text/TextWatcher;

.field public e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field private f:Z

.field public g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1160379
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1160380
    iput-boolean v0, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->f:Z

    .line 1160381
    iput-boolean v0, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->g:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/ui/PaymentFormEditTextView;I)V
    .locals 0

    .prologue
    .line 1160376
    iput-object p1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1160377
    invoke-virtual {p1, p2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputId(I)V

    .line 1160378
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 1160375
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    return v0
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 1160370
    iput-boolean p1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->f:Z

    .line 1160371
    iget-boolean v0, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a:LX/6vE;

    if-eqz v0, :cond_0

    .line 1160372
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->c:LX/6wl;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a:LX/6vE;

    invoke-interface {v2}, LX/6vE;->a()LX/6z8;

    move-result-object v2

    invoke-interface {v1, v2}, LX/6wl;->b(LX/6z8;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a(Ljava/lang/String;)V

    .line 1160373
    :goto_0
    return-void

    .line 1160374
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->d()V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1160367
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1160368
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->f:Z

    .line 1160369
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1160333
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    .line 1160334
    return-void

    .line 1160335
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1160360
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a:LX/6vE;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1160361
    iget-boolean v2, v1, Lcom/facebook/payments/ui/PaymentFormEditTextView;->b:Z

    move v1, v2

    .line 1160362
    if-nez v1, :cond_0

    .line 1160363
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a:LX/6vE;

    invoke-interface {v1}, LX/6vE;->a()LX/6z8;

    move-result-object v1

    .line 1160364
    iget-boolean v2, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->g:Z

    if-eqz v2, :cond_1

    invoke-interface {v1}, LX/6z8;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1160365
    :cond_0
    :goto_0
    return v0

    .line 1160366
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->c:LX/6wl;

    invoke-interface {v0, v1}, LX/6wl;->a(LX/6z8;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6fe20ca3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1160356
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1160357
    if-eqz p1, :cond_0

    .line 1160358
    const-string v1, "form_input_has_error"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->f:Z

    .line 1160359
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x15ad6920    # 7.004E-26f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x25b35484

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1160352
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1160353
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->b(Landroid/text/TextWatcher;)V

    .line 1160354
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->b(Landroid/text/TextWatcher;)V

    .line 1160355
    const/16 v1, 0x2b

    const v2, 0x6e5ff37a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x584070d3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1160339
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1160340
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1160341
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b:Landroid/text/TextWatcher;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b:Landroid/text/TextWatcher;

    :goto_0
    iput-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b:Landroid/text/TextWatcher;

    .line 1160342
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->c:LX/6wl;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->c:LX/6wl;

    :goto_1
    iput-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->c:LX/6wl;

    .line 1160343
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d:Landroid/text/TextWatcher;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d:Landroid/text/TextWatcher;

    :goto_2
    iput-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d:Landroid/text/TextWatcher;

    .line 1160344
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    new-instance v2, LX/6yj;

    invoke-direct {v2, p0}, LX/6yj;-><init>(Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1160345
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a(Landroid/text/TextWatcher;)V

    .line 1160346
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a(Landroid/text/TextWatcher;)V

    .line 1160347
    iget-boolean v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->f:Z

    invoke-virtual {p0, v1}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b(Z)V

    .line 1160348
    const/16 v1, 0x2b

    const v2, -0x2ce81632

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1160349
    :cond_0
    new-instance v1, LX/6yk;

    invoke-direct {v1, p0}, LX/6yk;-><init>(Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;)V

    goto :goto_0

    .line 1160350
    :cond_1
    new-instance v1, LX/6zE;

    invoke-direct {v1}, LX/6zE;-><init>()V

    goto :goto_1

    .line 1160351
    :cond_2
    new-instance v1, LX/6yl;

    invoke-direct {v1, p0}, LX/6yl;-><init>(Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;)V

    goto :goto_2
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1160336
    const-string v0, "form_input_has_error"

    iget-boolean v1, p0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->f:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1160337
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1160338
    return-void
.end method
