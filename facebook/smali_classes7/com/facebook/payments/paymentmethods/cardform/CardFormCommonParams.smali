.class public final Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6yO;

.field public final b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

.field public final c:LX/6xg;

.field public final d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

.field public final e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Z

.field public final g:Lcom/facebook/common/locale/Country;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1159306
    new-instance v0, LX/6xx;

    invoke-direct {v0}, LX/6xx;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6xy;)V
    .locals 1

    .prologue
    .line 1159297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1159298
    iget-object v0, p1, LX/6xy;->a:LX/6yO;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    .line 1159299
    iget-object v0, p1, LX/6xy;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    .line 1159300
    iget-object v0, p1, LX/6xy;->c:LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->c:LX/6xg;

    .line 1159301
    iget-object v0, p1, LX/6xy;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    .line 1159302
    iget-object v0, p1, LX/6xy;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    .line 1159303
    iget-boolean v0, p1, LX/6xy;->f:Z

    iput-boolean v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->f:Z

    .line 1159304
    iget-object v0, p1, LX/6xy;->g:Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->g:Lcom/facebook/common/locale/Country;

    .line 1159305
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1159288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1159289
    const-class v0, LX/6yO;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6yO;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    .line 1159290
    const-class v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    .line 1159291
    const-class v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    .line 1159292
    const-class v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    .line 1159293
    const-class v0, LX/6xg;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->c:LX/6xg;

    .line 1159294
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->f:Z

    .line 1159295
    const-class v0, Lcom/facebook/common/locale/Country;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->g:Lcom/facebook/common/locale/Country;

    .line 1159296
    return-void
.end method

.method public static a(LX/6yO;Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;LX/6xg;)LX/6xy;
    .locals 1

    .prologue
    .line 1159287
    new-instance v0, LX/6xy;

    invoke-direct {v0, p0, p1, p2}, LX/6xy;-><init>(LX/6yO;Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;LX/6xg;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;
    .locals 0

    .prologue
    .line 1159277
    return-object p0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1159286
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1159278
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1159279
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1159280
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1159281
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1159282
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->c:LX/6xg;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1159283
    iget-boolean v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1159284
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->g:Lcom/facebook/common/locale/Country;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1159285
    return-void
.end method
