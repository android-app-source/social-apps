.class public final Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

.field public final c:LX/6xZ;

.field public final d:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1159246
    new-instance v0, LX/6xv;

    invoke-direct {v0}, LX/6xv;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6xw;)V
    .locals 1

    .prologue
    .line 1159247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1159248
    iget-object v0, p1, LX/6xw;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    .line 1159249
    iget-object v0, p1, LX/6xw;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1159250
    iget-object v0, p1, LX/6xw;->c:LX/6xZ;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->c:LX/6xZ;

    .line 1159251
    iget-object v0, p1, LX/6xw;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->d:Ljava/lang/String;

    .line 1159252
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1159253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1159254
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    .line 1159255
    const-class v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1159256
    const-class v0, LX/6xZ;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xZ;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->c:LX/6xZ;

    .line 1159257
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->d:Ljava/lang/String;

    .line 1159258
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6xw;
    .locals 1

    .prologue
    .line 1159259
    new-instance v0, LX/6xw;

    invoke-direct {v0, p0, p1}, LX/6xw;-><init>(Ljava/lang/String;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1159260
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1159261
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1159262
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1159263
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->c:LX/6xZ;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1159264
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1159265
    return-void
.end method
