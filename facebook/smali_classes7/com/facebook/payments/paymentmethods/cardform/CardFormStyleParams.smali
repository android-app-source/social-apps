.class public Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1159969
    new-instance v0, LX/6yQ;

    invoke-direct {v0}, LX/6yQ;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6yR;)V
    .locals 1

    .prologue
    .line 1159963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1159964
    iget-object v0, p1, LX/6yR;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->a:Ljava/lang/String;

    .line 1159965
    iget-object v0, p1, LX/6yR;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->b:Ljava/lang/String;

    .line 1159966
    iget-object v0, p1, LX/6yR;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1159967
    iget-boolean v0, p1, LX/6yR;->d:Z

    iput-boolean v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->d:Z

    .line 1159968
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1159970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1159971
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->a:Ljava/lang/String;

    .line 1159972
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->b:Ljava/lang/String;

    .line 1159973
    const-class v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1159974
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->d:Z

    .line 1159975
    return-void
.end method

.method public static newBuilder()LX/6yR;
    .locals 1

    .prologue
    .line 1159962
    new-instance v0, LX/6yR;

    invoke-direct {v0}, LX/6yR;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1159961
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1159956
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1159957
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1159958
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1159959
    iget-boolean v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1159960
    return-void
.end method
