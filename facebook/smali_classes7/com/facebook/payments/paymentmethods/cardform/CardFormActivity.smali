.class public Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1159188
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1159189
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1159190
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1159191
    const-string v1, "card_form_params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1159192
    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 1159193
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "card_form_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1159194
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d03c5

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    .line 1159195
    new-instance v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;

    invoke-direct {v3}, Lcom/facebook/payments/paymentmethods/cardform/CardFormFragment;-><init>()V

    .line 1159196
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1159197
    const-string p0, "card_form_params"

    invoke-virtual {v4, p0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1159198
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1159199
    move-object v2, v3

    .line 1159200
    const-string v3, "card_form_fragment"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1159201
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;

    invoke-static {v0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v0

    check-cast v0, LX/6wr;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->p:LX/6wr;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1159202
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1159203
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1159204
    const v0, 0x7f03023b

    invoke-virtual {p0, v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->setContentView(I)V

    .line 1159205
    if-nez p1, :cond_0

    .line 1159206
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->a()V

    .line 1159207
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->a(Landroid/app/Activity;LX/6ws;)V

    .line 1159208
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1159209
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 1159210
    invoke-static {p0, p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1159211
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "card_form_params"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    .line 1159212
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->p:LX/6wr;

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    iget-object v1, v1, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v1, v1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-virtual {v0, p0, v1}, LX/6wr;->b(Landroid/app/Activity;LX/73i;)V

    .line 1159213
    return-void
.end method

.method public final finish()V
    .locals 1

    .prologue
    .line 1159214
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1159215
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->q:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->b(Landroid/app/Activity;LX/6ws;)V

    .line 1159216
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1159217
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "card_form_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1159218
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/0fj;

    if-eqz v1, :cond_0

    .line 1159219
    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    .line 1159220
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1159221
    return-void
.end method
