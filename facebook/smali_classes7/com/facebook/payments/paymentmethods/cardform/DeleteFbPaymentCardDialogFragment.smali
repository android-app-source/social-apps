.class public Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;
.super Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;
.source ""

# interfaces
.implements LX/6xz;


# instance fields
.field public m:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/6yZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:LX/6qh;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1160018
    invoke-direct {p0}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;I)Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1160011
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1160012
    const-string v1, "extra_fb_payment_card"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1160013
    const-string v1, "extra_card_form_style"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1160014
    const-string v1, "extra_message_res_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1160015
    new-instance v1, Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;

    invoke-direct {v1}, Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;-><init>()V

    .line 1160016
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1160017
    return-object v1
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/6yZ;->a(LX/0QB;)LX/6yZ;

    move-result-object p0

    check-cast p0, LX/6yZ;

    iput-object v1, p1, Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;->m:LX/0Zb;

    iput-object p0, p1, Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;->n:LX/6yZ;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 1159976
    new-instance v0, LX/6dy;

    const v1, 0x7f081e7e

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f081e7d

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1159977
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1159978
    const-string v2, "extra_message_res_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1159979
    iput-object v1, v0, LX/6dy;->d:Ljava/lang/String;

    .line 1159980
    move-object v0, v0

    .line 1159981
    const/4 v1, 0x0

    .line 1159982
    iput-boolean v1, v0, LX/6dy;->f:Z

    .line 1159983
    move-object v0, v0

    .line 1159984
    invoke-virtual {v0}, LX/6dy;->a()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v0

    .line 1159985
    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    .line 1159986
    invoke-super {p0, p1}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 1160002
    invoke-super {p0}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->a()V

    .line 1160003
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1160004
    const-string v1, "extra_fb_payment_card"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    .line 1160005
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1160006
    const-string v2, "extra_mutation"

    const-string v3, "action_delete_payment_card"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160007
    const-string v2, "extra_fb_payment_card"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1160008
    new-instance v2, LX/73T;

    sget-object v3, LX/73S;->MUTATION:LX/73S;

    invoke-direct {v2, v3, v1}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    move-object v0, v2

    .line 1160009
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;->o:LX/6qh;

    invoke-virtual {v1, v0}, LX/6qh;->a(LX/73T;)V

    .line 1160010
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 1160000
    iput-object p1, p0, Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;->o:LX/6qh;

    .line 1160001
    return-void
.end method

.method public final k()V
    .locals 3

    .prologue
    .line 1159994
    invoke-super {p0}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->k()V

    .line 1159995
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1159996
    const-string v1, "extra_card_form_style"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    .line 1159997
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;->n:LX/6yZ;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v1, v2}, LX/6yZ;->b(LX/6yO;)LX/6xu;

    move-result-object v1

    invoke-interface {v1, v0}, LX/6xu;->k(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v1

    .line 1159998
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;->m:LX/0Zb;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    move-result-object v0

    invoke-interface {v2, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1159999
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x288601d4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1159987
    invoke-super {p0, p1}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1159988
    const-class v0, Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;

    invoke-static {v0, p0}, Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1159989
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1159990
    const-string v2, "extra_card_form_style"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    .line 1159991
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;->n:LX/6yZ;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a:LX/6yO;

    invoke-virtual {v2, v3}, LX/6yZ;->b(LX/6yO;)LX/6xu;

    move-result-object v2

    invoke-interface {v2, v0}, LX/6xu;->j(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v2

    .line 1159992
    iget-object v3, p0, Lcom/facebook/payments/paymentmethods/cardform/DeleteFbPaymentCardDialogFragment;->m:LX/0Zb;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->b:Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsEvent;

    move-result-object v0

    invoke-interface {v3, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1159993
    const/16 v0, 0x2b

    const v2, -0x571be51d

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
