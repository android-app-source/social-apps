.class public Lcom/facebook/payments/confirmation/ConfirmationCheckMarkRowView;
.super Lcom/facebook/payments/ui/PaymentsComponentViewGroup;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1155732
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;)V

    .line 1155733
    invoke-direct {p0}, Lcom/facebook/payments/confirmation/ConfirmationCheckMarkRowView;->a()V

    .line 1155734
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1155735
    invoke-direct {p0, p1, p2}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1155736
    invoke-direct {p0}, Lcom/facebook/payments/confirmation/ConfirmationCheckMarkRowView;->a()V

    .line 1155737
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1155738
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1155739
    invoke-direct {p0}, Lcom/facebook/payments/confirmation/ConfirmationCheckMarkRowView;->a()V

    .line 1155740
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1155741
    const v0, 0x7f03035a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1155742
    return-void
.end method
