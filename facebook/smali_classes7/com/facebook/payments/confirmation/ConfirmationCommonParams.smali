.class public Lcom/facebook/payments/confirmation/ConfirmationCommonParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/confirmation/ConfirmationParams;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/confirmation/ConfirmationCommonParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6uW;

.field public final b:Z

.field public final c:LX/6xg;

.field public final d:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1155750
    new-instance v0, LX/6uL;

    invoke-direct {v0}, LX/6uL;-><init>()V

    sput-object v0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6uM;)V
    .locals 2

    .prologue
    .line 1155751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1155752
    iget-object v0, p1, LX/6uM;->a:LX/6uW;

    move-object v0, v0

    .line 1155753
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6uW;

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->a:LX/6uW;

    .line 1155754
    iget-boolean v0, p1, LX/6uM;->b:Z

    move v0, v0

    .line 1155755
    iput-boolean v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->b:Z

    .line 1155756
    iget-object v0, p1, LX/6uM;->c:LX/6xg;

    move-object v0, v0

    .line 1155757
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->c:LX/6xg;

    .line 1155758
    iget-object v0, p1, LX/6uM;->d:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-object v0, v0

    .line 1155759
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->d:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1155760
    iget-object v0, p1, LX/6uM;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1155761
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->e:Ljava/lang/String;

    .line 1155762
    iget-object v0, p1, LX/6uM;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1155763
    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->f:Ljava/lang/String;

    .line 1155764
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1155765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1155766
    const-class v0, LX/6uW;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6uW;

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->a:LX/6uW;

    .line 1155767
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->b:Z

    .line 1155768
    const-class v0, LX/6xg;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->c:LX/6xg;

    .line 1155769
    const-class v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->d:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1155770
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->e:Ljava/lang/String;

    .line 1155771
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->f:Ljava/lang/String;

    .line 1155772
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/confirmation/ConfirmationCommonParams;
    .locals 0

    .prologue
    .line 1155773
    return-object p0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1155774
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1155775
    iget-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->a:LX/6uW;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1155776
    iget-boolean v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1155777
    iget-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->c:LX/6xg;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1155778
    iget-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->d:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1155779
    iget-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1155780
    iget-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1155781
    return-void
.end method
