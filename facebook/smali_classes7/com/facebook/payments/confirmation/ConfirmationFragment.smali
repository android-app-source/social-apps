.class public Lcom/facebook/payments/confirmation/ConfirmationFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:LX/6uc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/6uJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Landroid/content/Context;

.field public f:Lcom/facebook/payments/confirmation/ConfirmationData;

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/6uG;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/support/v7/widget/RecyclerView;

.field public i:LX/6ud;

.field public j:LX/6F4;

.field public k:LX/6uN;

.field public l:LX/6uV;

.field public final m:LX/6uO;

.field public final n:LX/6qh;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1155896
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1155897
    new-instance v0, LX/6uO;

    invoke-direct {v0, p0}, LX/6uO;-><init>(Lcom/facebook/payments/confirmation/ConfirmationFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->m:LX/6uO;

    .line 1155898
    new-instance v0, LX/6uP;

    invoke-direct {v0, p0}, LX/6uP;-><init>(Lcom/facebook/payments/confirmation/ConfirmationFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->n:LX/6qh;

    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/payments/confirmation/ConfirmationFragment;

    invoke-static {p0}, LX/6uc;->a(LX/0QB;)LX/6uc;

    move-result-object v0

    check-cast v0, LX/6uc;

    new-instance v2, LX/6uJ;

    invoke-static {p0}, LX/6uc;->a(LX/0QB;)LX/6uc;

    move-result-object v1

    check-cast v1, LX/6uc;

    invoke-direct {v2, v1}, LX/6uJ;-><init>(LX/6uc;)V

    move-object v1, v2

    check-cast v1, LX/6uJ;

    invoke-static {p0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v2

    check-cast v2, LX/6wr;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p1, Lcom/facebook/payments/confirmation/ConfirmationFragment;->a:LX/6uc;

    iput-object v1, p1, Lcom/facebook/payments/confirmation/ConfirmationFragment;->b:LX/6uJ;

    iput-object v2, p1, Lcom/facebook/payments/confirmation/ConfirmationFragment;->c:LX/6wr;

    iput-object p0, p1, Lcom/facebook/payments/confirmation/ConfirmationFragment;->d:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static c(Lcom/facebook/payments/confirmation/ConfirmationFragment;)V
    .locals 2

    .prologue
    .line 1155894
    iget-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->j:LX/6F4;

    iget-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->f:Lcom/facebook/payments/confirmation/ConfirmationData;

    invoke-interface {v0, v1}, LX/6F4;->a(Lcom/facebook/payments/confirmation/ConfirmationData;)V

    .line 1155895
    return-void
.end method

.method public static k(Lcom/facebook/payments/confirmation/ConfirmationFragment;)V
    .locals 2

    .prologue
    .line 1155820
    iget-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->l:LX/6uV;

    iget-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->f:Lcom/facebook/payments/confirmation/ConfirmationData;

    invoke-interface {v0, v1}, LX/6uV;->a(Lcom/facebook/payments/confirmation/ConfirmationData;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->g:LX/0Px;

    .line 1155821
    iget-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->b:LX/6uJ;

    iget-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->g:LX/0Px;

    .line 1155822
    iput-object v1, v0, LX/6uJ;->b:LX/0Px;

    .line 1155823
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1155824
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 1155892
    invoke-static {p0}, Lcom/facebook/payments/confirmation/ConfirmationFragment;->c(Lcom/facebook/payments/confirmation/ConfirmationFragment;)V

    .line 1155893
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1155871
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1155872
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v2, 0x7f0e0326

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->e:Landroid/content/Context;

    .line 1155873
    const-class v0, Lcom/facebook/payments/confirmation/ConfirmationFragment;

    iget-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->e:Landroid/content/Context;

    invoke-static {v0, p0, v1}, Lcom/facebook/payments/confirmation/ConfirmationFragment;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1155874
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1155875
    const-string v1, "confirmation_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/confirmation/ConfirmationParams;

    .line 1155876
    invoke-interface {v0}, Lcom/facebook/payments/confirmation/ConfirmationParams;->a()Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->a:LX/6uW;

    .line 1155877
    iget-object v2, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->a:LX/6uc;

    invoke-virtual {v2, v1}, LX/6uc;->d(LX/6uW;)LX/6ud;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->i:LX/6ud;

    .line 1155878
    iget-object v2, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->a:LX/6uc;

    invoke-virtual {v2, v1}, LX/6uc;->e(LX/6uW;)LX/6F4;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->j:LX/6F4;

    .line 1155879
    iget-object v2, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->j:LX/6F4;

    iget-object v3, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->n:LX/6qh;

    invoke-interface {v2, v3}, LX/6F4;->a(LX/6qh;)V

    .line 1155880
    iget-object v2, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->a:LX/6uc;

    invoke-virtual {v2, v1}, LX/6uc;->b(LX/6uW;)LX/6uV;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->l:LX/6uV;

    .line 1155881
    iget-object v2, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->a:LX/6uc;

    .line 1155882
    iget-object v3, v2, LX/6uc;->a:LX/0P1;

    invoke-virtual {v3, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1155883
    :goto_0
    iget-object v3, v2, LX/6uc;->a:LX/0P1;

    invoke-virtual {v3, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6F1;

    iget-object v3, v3, LX/6F1;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6uN;

    move-object v2, v3

    .line 1155884
    iput-object v2, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->k:LX/6uN;

    .line 1155885
    iget-object v2, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->k:LX/6uN;

    iget-object v3, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->m:LX/6uO;

    invoke-interface {v2, v3}, LX/6uN;->a(LX/6uO;)V

    .line 1155886
    iget-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->f:Lcom/facebook/payments/confirmation/ConfirmationData;

    if-nez v1, :cond_0

    if-eqz p1, :cond_0

    .line 1155887
    const-string v1, "confirmation_data"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/confirmation/ConfirmationData;

    iput-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->f:Lcom/facebook/payments/confirmation/ConfirmationData;

    .line 1155888
    :cond_0
    iget-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->f:Lcom/facebook/payments/confirmation/ConfirmationData;

    if-nez v1, :cond_1

    .line 1155889
    iget-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->k:LX/6uN;

    invoke-interface {v1, v0}, LX/6uN;->a(Lcom/facebook/payments/confirmation/ConfirmationParams;)Lcom/facebook/payments/confirmation/ConfirmationData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->f:Lcom/facebook/payments/confirmation/ConfirmationData;

    .line 1155890
    :cond_1
    return-void

    .line 1155891
    :cond_2
    sget-object v1, LX/6uW;->SIMPLE:LX/6uW;

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 1155852
    packed-switch p1, :pswitch_data_0

    .line 1155853
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1155854
    :goto_0
    return-void

    .line 1155855
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->i:LX/6ud;

    iget-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->f:Lcom/facebook/payments/confirmation/ConfirmationData;

    .line 1155856
    check-cast v1, Lcom/facebook/payments/confirmation/SimpleConfirmationData;

    .line 1155857
    packed-switch p1, :pswitch_data_1

    .line 1155858
    :cond_0
    :goto_1
    goto :goto_0

    .line 1155859
    :pswitch_1
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    .line 1155860
    iget-object v2, v0, LX/6ud;->a:LX/6ub;

    sget-object v3, LX/6uT;->ACTIVATE_SECURITY_PIN:LX/6uT;

    .line 1155861
    iget-object p0, v2, LX/6ub;->a:LX/6uO;

    new-instance v0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;

    .line 1155862
    iget-object p1, v1, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->a:LX/0Rf;

    move-object p1, p1

    .line 1155863
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    .line 1155864
    invoke-interface {p2, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1155865
    invoke-interface {p2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1155866
    invoke-static {p2}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object p2

    move-object p1, p2

    .line 1155867
    invoke-virtual {v1}, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->a()Lcom/facebook/payments/confirmation/ConfirmationParams;

    move-result-object p2

    .line 1155868
    iget-object p3, v1, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->b:Lcom/facebook/payments/confirmation/ProductConfirmationData;

    move-object p3, p3

    .line 1155869
    invoke-direct {v0, p1, p2, p3}, Lcom/facebook/payments/confirmation/SimpleConfirmationData;-><init>(LX/0Rf;Lcom/facebook/payments/confirmation/ConfirmationParams;Lcom/facebook/payments/confirmation/ProductConfirmationData;)V

    invoke-virtual {p0, v0}, LX/6uO;->a(Lcom/facebook/payments/confirmation/ConfirmationData;)V

    .line 1155870
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1ff805fd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1155851
    iget-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->e:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03035b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x50c203b2

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1155848
    const-string v0, "confirmation_data"

    iget-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->f:Lcom/facebook/payments/confirmation/ConfirmationData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1155849
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1155850
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1155825
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1155826
    const v0, 0x7f0d04e7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->h:Landroid/support/v7/widget/RecyclerView;

    .line 1155827
    iget-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->h:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->f:Lcom/facebook/payments/confirmation/ConfirmationData;

    invoke-interface {v1}, Lcom/facebook/payments/confirmation/ConfirmationData;->a()Lcom/facebook/payments/confirmation/ConfirmationParams;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/payments/confirmation/ConfirmationParams;->a()Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->d:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-boolean v1, v1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    invoke-static {v0, v1}, LX/6wr;->a(Landroid/support/v7/widget/RecyclerView;Z)V

    .line 1155828
    iget-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->h:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->b:LX/6uJ;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1155829
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1155830
    const v1, 0x7f0d00bb

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 1155831
    iget-object v2, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->f:Lcom/facebook/payments/confirmation/ConfirmationData;

    invoke-interface {v2}, Lcom/facebook/payments/confirmation/ConfirmationData;->a()Lcom/facebook/payments/confirmation/ConfirmationParams;

    move-result-object v2

    invoke-interface {v2}, Lcom/facebook/payments/confirmation/ConfirmationParams;->a()Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    move-result-object v2

    iget-object v3, v2, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->d:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1155832
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1155833
    check-cast v2, Landroid/view/ViewGroup;

    new-instance v4, LX/6uQ;

    invoke-direct {v4, p0, v0}, LX/6uQ;-><init>(Lcom/facebook/payments/confirmation/ConfirmationFragment;Landroid/app/Activity;)V

    iget-object p1, v3, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    sget-object p2, LX/73h;->NO_NAV_ICON:LX/73h;

    invoke-virtual {v1, v2, v4, p1, p2}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 1155834
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f081e22

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v3, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Ljava/lang/String;LX/73i;)V

    .line 1155835
    iget-object v2, v1, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    move-object v1, v2

    .line 1155836
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081e23

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1155837
    iput-object v3, v2, LX/108;->g:Ljava/lang/String;

    .line 1155838
    move-object v2, v2

    .line 1155839
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 1155840
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1155841
    new-instance v2, LX/6uR;

    invoke-direct {v2, p0, v0}, LX/6uR;-><init>(Lcom/facebook/payments/confirmation/ConfirmationFragment;Landroid/app/Activity;)V

    invoke-interface {v1, v2}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1155842
    iget-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->b:LX/6uJ;

    iget-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->n:LX/6qh;

    .line 1155843
    iput-object v1, v0, LX/6uJ;->d:LX/6qh;

    .line 1155844
    iget-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->b:LX/6uJ;

    iget-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationFragment;->f:Lcom/facebook/payments/confirmation/ConfirmationData;

    invoke-interface {v1}, Lcom/facebook/payments/confirmation/ConfirmationData;->a()Lcom/facebook/payments/confirmation/ConfirmationParams;

    move-result-object v1

    .line 1155845
    iput-object v1, v0, LX/6uJ;->c:Lcom/facebook/payments/confirmation/ConfirmationParams;

    .line 1155846
    invoke-static {p0}, Lcom/facebook/payments/confirmation/ConfirmationFragment;->k(Lcom/facebook/payments/confirmation/ConfirmationFragment;)V

    .line 1155847
    return-void
.end method
