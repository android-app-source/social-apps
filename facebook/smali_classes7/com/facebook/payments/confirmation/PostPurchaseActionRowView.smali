.class public Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;
.super LX/6E7;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/fbui/glyph/GlyphView;

.field private c:LX/6uH;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1155921
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1155922
    invoke-direct {p0}, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->a()V

    .line 1155923
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1155924
    invoke-direct {p0, p1, p2}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1155925
    invoke-direct {p0}, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->a()V

    .line 1155926
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1155927
    invoke-direct {p0, p1, p2, p3}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1155928
    invoke-direct {p0}, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->a()V

    .line 1155929
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1155930
    const v0, 0x7f030ff5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1155931
    invoke-direct {p0}, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->b()V

    .line 1155932
    const v0, 0x7f0d266f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1155933
    const v0, 0x7f0d0b20

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1155934
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1155935
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->setOrientation(I)V

    .line 1155936
    invoke-virtual {p0}, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b13a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1155937
    invoke-virtual {p0}, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b13a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1155938
    invoke-virtual {p0, v0, v1, v0, v1}, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->setPadding(IIII)V

    .line 1155939
    return-void
.end method


# virtual methods
.method public final a(LX/6uH;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1155940
    iput-object p1, p0, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->c:LX/6uH;

    .line 1155941
    iget-object v0, p0, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->a:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p0, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->c:LX/6uH;

    invoke-interface {v2}, LX/6uH;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1155942
    iget-object v0, p0, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->c:LX/6uH;

    invoke-interface {v0}, LX/6uH;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1155943
    iget-object v0, p0, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->a:Lcom/facebook/widget/text/BetterTextView;

    sget-object v2, LX/0xq;->ROBOTO:LX/0xq;

    invoke-virtual {v2}, LX/0xq;->ordinal()I

    move-result v2

    invoke-static {v2}, LX/0xq;->fromIndex(I)LX/0xq;

    move-result-object v2

    sget-object v3, LX/0xr;->MEDIUM:LX/0xr;

    invoke-virtual {v3}, LX/0xr;->ordinal()I

    move-result v3

    invoke-static {v3}, LX/0xr;->fromIndex(I)LX/0xr;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v4}, Lcom/facebook/widget/text/BetterTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    invoke-static {v0, v2, v3, v4}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 1155944
    :cond_0
    invoke-virtual {p0, p0}, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1155945
    iget-object v0, p0, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->c:LX/6uH;

    invoke-interface {v0}, LX/6uH;->b()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->setEnabled(Z)V

    .line 1155946
    iget-object v0, p0, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v2, p0, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->c:LX/6uH;

    invoke-interface {v2}, LX/6uH;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1155947
    return-void

    :cond_1
    move v0, v1

    .line 1155948
    goto :goto_0

    .line 1155949
    :cond_2
    const/16 v1, 0x8

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x3f1681c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1155950
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1155951
    const-string v2, "extra_user_action"

    iget-object v3, p0, Lcom/facebook/payments/confirmation/PostPurchaseActionRowView;->c:LX/6uH;

    invoke-interface {v3}, LX/6uG;->d()LX/6uT;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1155952
    new-instance v2, LX/73T;

    sget-object v3, LX/73S;->USER_ACTION:LX/73S;

    invoke-direct {v2, v3, v1}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    invoke-virtual {p0, v2}, LX/6E7;->a(LX/73T;)V

    .line 1155953
    const v1, 0x555dbd41

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
