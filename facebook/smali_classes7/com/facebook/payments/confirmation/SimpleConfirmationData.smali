.class public Lcom/facebook/payments/confirmation/SimpleConfirmationData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/confirmation/ConfirmationData;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/confirmation/SimpleConfirmationData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/6uT;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/payments/confirmation/ProductConfirmationData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Lcom/facebook/payments/confirmation/ConfirmationParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1155994
    new-instance v0, LX/6ua;

    invoke-direct {v0}, LX/6ua;-><init>()V

    sput-object v0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Rf;Lcom/facebook/payments/confirmation/ConfirmationParams;Lcom/facebook/payments/confirmation/ProductConfirmationData;)V
    .locals 0
    .param p3    # Lcom/facebook/payments/confirmation/ProductConfirmationData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "LX/6uT;",
            ">;",
            "Lcom/facebook/payments/confirmation/ConfirmationParams;",
            "Lcom/facebook/payments/confirmation/ProductConfirmationData;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1156006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156007
    iput-object p1, p0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->a:LX/0Rf;

    .line 1156008
    iput-object p2, p0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->c:Lcom/facebook/payments/confirmation/ConfirmationParams;

    .line 1156009
    iput-object p3, p0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->b:Lcom/facebook/payments/confirmation/ProductConfirmationData;

    .line 1156010
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1156001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156002
    const-class v0, LX/6uT;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/ClassLoader;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->a:LX/0Rf;

    .line 1156003
    const-class v0, Lcom/facebook/payments/confirmation/ProductConfirmationData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/confirmation/ProductConfirmationData;

    iput-object v0, p0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->b:Lcom/facebook/payments/confirmation/ProductConfirmationData;

    .line 1156004
    const-class v0, Lcom/facebook/payments/confirmation/ConfirmationParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/confirmation/ConfirmationParams;

    iput-object v0, p0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->c:Lcom/facebook/payments/confirmation/ConfirmationParams;

    .line 1156005
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/confirmation/ConfirmationParams;Lcom/facebook/payments/confirmation/ProductConfirmationData;)V
    .locals 1
    .param p2    # Lcom/facebook/payments/confirmation/ProductConfirmationData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1156011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156012
    iput-object p1, p0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->c:Lcom/facebook/payments/confirmation/ConfirmationParams;

    .line 1156013
    iput-object p2, p0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->b:Lcom/facebook/payments/confirmation/ProductConfirmationData;

    .line 1156014
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1156015
    iput-object v0, p0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->a:LX/0Rf;

    .line 1156016
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/confirmation/ConfirmationParams;
    .locals 1

    .prologue
    .line 1156000
    iget-object v0, p0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->c:Lcom/facebook/payments/confirmation/ConfirmationParams;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1155999
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1155995
    iget-object v0, p0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->a:LX/0Rf;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Set;)V

    .line 1155996
    iget-object v0, p0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->b:Lcom/facebook/payments/confirmation/ProductConfirmationData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1155997
    iget-object v0, p0, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->c:Lcom/facebook/payments/confirmation/ConfirmationParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1155998
    return-void
.end method
