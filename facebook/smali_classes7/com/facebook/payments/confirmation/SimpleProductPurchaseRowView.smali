.class public Lcom/facebook/payments/confirmation/SimpleProductPurchaseRowView;
.super Lcom/facebook/payments/ui/PaymentsComponentViewGroup;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1156147
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;)V

    .line 1156148
    invoke-direct {p0}, Lcom/facebook/payments/confirmation/SimpleProductPurchaseRowView;->a()V

    .line 1156149
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1156150
    invoke-direct {p0, p1, p2}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1156151
    invoke-direct {p0}, Lcom/facebook/payments/confirmation/SimpleProductPurchaseRowView;->a()V

    .line 1156152
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1156153
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1156154
    invoke-direct {p0}, Lcom/facebook/payments/confirmation/SimpleProductPurchaseRowView;->a()V

    .line 1156155
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1156156
    const v0, 0x7f031334

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1156157
    const v0, 0x7f0d2c86

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/payments/confirmation/SimpleProductPurchaseRowView;->a:Landroid/widget/TextView;

    .line 1156158
    return-void
.end method


# virtual methods
.method public setMessageText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1156159
    iget-object v0, p0, Lcom/facebook/payments/confirmation/SimpleProductPurchaseRowView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1156160
    return-void
.end method
