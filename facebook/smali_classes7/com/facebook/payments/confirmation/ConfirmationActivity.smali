.class public Lcom/facebook/payments/confirmation/ConfirmationActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/payments/confirmation/ConfirmationParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1155682
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/payments/confirmation/ConfirmationParams;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1155683
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1155684
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/payments/confirmation/ConfirmationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1155685
    const-string v1, "confirmation_params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1155686
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/confirmation/ConfirmationActivity;

    invoke-static {v0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v0

    check-cast v0, LX/6wr;

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationActivity;->p:LX/6wr;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1155687
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1155688
    const v0, 0x7f030597

    invoke-virtual {p0, v0}, Lcom/facebook/payments/confirmation/ConfirmationActivity;->setContentView(I)V

    .line 1155689
    iget-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationActivity;->q:Lcom/facebook/payments/confirmation/ConfirmationParams;

    invoke-interface {v0}, Lcom/facebook/payments/confirmation/ConfirmationParams;->a()Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    move-result-object v0

    .line 1155690
    iget-object v1, v0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->d:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-boolean v1, v1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    iget-object v2, v0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->d:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v2, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-static {p0, v1, v2}, LX/6wr;->b(Landroid/app/Activity;ZLX/73i;)V

    .line 1155691
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 1155692
    if-nez p1, :cond_0

    const-string v2, "confirmation_fragment_tag"

    invoke-virtual {v1, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1155693
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    iget-object v3, p0, Lcom/facebook/payments/confirmation/ConfirmationActivity;->q:Lcom/facebook/payments/confirmation/ConfirmationParams;

    .line 1155694
    new-instance v4, Lcom/facebook/payments/confirmation/ConfirmationFragment;

    invoke-direct {v4}, Lcom/facebook/payments/confirmation/ConfirmationFragment;-><init>()V

    .line 1155695
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1155696
    const-string p1, "confirmation_params"

    invoke-virtual {v5, p1, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1155697
    invoke-virtual {v4, v5}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1155698
    move-object v3, v4

    .line 1155699
    const-string v4, "confirmation_fragment_tag"

    invoke-virtual {v1, v2, v3, v4}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1155700
    :cond_0
    iget-object v0, v0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->d:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->a(Landroid/app/Activity;LX/6ws;)V

    .line 1155701
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1155702
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 1155703
    invoke-static {p0, p0}, Lcom/facebook/payments/confirmation/ConfirmationActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1155704
    invoke-virtual {p0}, Lcom/facebook/payments/confirmation/ConfirmationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "confirmation_params"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/confirmation/ConfirmationParams;

    iput-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationActivity;->q:Lcom/facebook/payments/confirmation/ConfirmationParams;

    .line 1155705
    iget-object v0, p0, Lcom/facebook/payments/confirmation/ConfirmationActivity;->q:Lcom/facebook/payments/confirmation/ConfirmationParams;

    invoke-interface {v0}, Lcom/facebook/payments/confirmation/ConfirmationParams;->a()Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    move-result-object v0

    .line 1155706
    iget-object v1, p0, Lcom/facebook/payments/confirmation/ConfirmationActivity;->p:LX/6wr;

    iget-object v2, v0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->d:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-boolean v2, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    iget-object v0, v0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;->d:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-virtual {v1, p0, v2, v0}, LX/6wr;->a(Landroid/app/Activity;ZLX/73i;)V

    .line 1155707
    return-void
.end method

.method public final finish()V
    .locals 1

    .prologue
    .line 1155708
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1155709
    sget-object v0, LX/6ws;->MODAL_BOTTOM:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->b(Landroid/app/Activity;LX/6ws;)V

    .line 1155710
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1155711
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "confirmation_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1155712
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/0fj;

    if-eqz v1, :cond_0

    .line 1155713
    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    .line 1155714
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1155715
    return-void
.end method
