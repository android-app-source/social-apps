.class public Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;
.super Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/payments/picker/model/SimplePickerRunTimeData",
        "<",
        "Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;",
        "Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;",
        "Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;",
        "LX/729;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1163716
    new-instance v0, LX/71r;

    invoke-direct {v0}, LX/71r;-><init>()V

    sput-object v0, Lcom/facebook/payments/shipping/addresspicker/ShippingAddressPickerRunTimeData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 1163717
    invoke-direct {p0, p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;-><init>(Landroid/os/Parcel;)V

    .line 1163718
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;)V
    .locals 0

    .prologue
    .line 1163719
    invoke-direct {p0, p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;-><init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;)V

    .line 1163720
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;",
            "Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;",
            "Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;",
            "LX/0P1",
            "<",
            "LX/729;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1163721
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;-><init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)V

    .line 1163722
    return-void
.end method


# virtual methods
.method public final b()Landroid/content/Intent;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1163723
    iget-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 1163724
    if-nez v0, :cond_0

    .line 1163725
    const/4 v0, 0x0

    .line 1163726
    :goto_0
    return-object v0

    .line 1163727
    :cond_0
    sget-object v1, LX/71s;->a:[I

    invoke-virtual {p0}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;

    iget-object v0, v0, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->b:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->e:LX/72f;

    invoke-virtual {v0}, LX/72f;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1163728
    iget-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 1163729
    check-cast v0, Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;

    iget-object v0, v0, Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;->a:LX/0Px;

    invoke-static {v0}, LX/71q;->a(LX/0Px;)LX/0am;

    move-result-object v0

    .line 1163730
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1163731
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1163732
    const-string v2, "shipping_address"

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1163733
    :cond_1
    move-object v0, v1

    .line 1163734
    goto :goto_0

    .line 1163735
    :pswitch_0
    sget-object v0, LX/729;->SHIPPING_ADDRESSES:LX/729;

    invoke-virtual {p0, v0}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a(LX/6vZ;)Ljava/lang/String;

    move-result-object v2

    .line 1163736
    iget-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 1163737
    check-cast v0, Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;

    iget-object v3, v0, Lcom/facebook/payments/shipping/addresspicker/ShippingCoreClientData;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1163738
    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/MailingAddress;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1163739
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1163740
    const-string v2, "shipping_address"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object v0, v1

    .line 1163741
    :goto_2
    move-object v0, v0

    .line 1163742
    goto :goto_0

    .line 1163743
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1163744
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1163745
    iget-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 1163746
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
