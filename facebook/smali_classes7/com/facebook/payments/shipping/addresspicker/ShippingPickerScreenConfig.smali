.class public Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/picker/model/PickerScreenConfig;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

.field public final b:Lcom/facebook/payments/shipping/model/ShippingParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1163815
    new-instance v0, LX/71x;

    invoke-direct {v0}, LX/71x;-><init>()V

    sput-object v0, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/722;)V
    .locals 2

    .prologue
    .line 1163816
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163817
    iget-object v0, p1, LX/722;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-object v0, v0

    .line 1163818
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    iput-object v0, p0, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 1163819
    iget-object v0, p1, LX/722;->b:Lcom/facebook/payments/shipping/model/ShippingParams;

    move-object v0, v0

    .line 1163820
    if-nez v0, :cond_0

    invoke-static {}, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->newBuilder()LX/72e;

    move-result-object v0

    sget-object v1, LX/72g;->SIMPLE:LX/72g;

    .line 1163821
    iput-object v1, v0, LX/72e;->a:LX/72g;

    .line 1163822
    move-object v0, v0

    .line 1163823
    sget-object v1, LX/72f;->CHECKOUT:LX/72f;

    .line 1163824
    iput-object v1, v0, LX/72e;->e:LX/72f;

    .line 1163825
    move-object v0, v0

    .line 1163826
    iget-object v1, p1, LX/722;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-object v1, v1

    .line 1163827
    iget-object v1, v1, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1163828
    iput-object v1, v0, LX/72e;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1163829
    move-object v0, v0

    .line 1163830
    iget-object v1, p1, LX/722;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-object v1, v1

    .line 1163831
    iget-object v1, v1, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    .line 1163832
    iput-object v1, v0, LX/72e;->i:LX/6xg;

    .line 1163833
    move-object v0, v0

    .line 1163834
    invoke-virtual {v0}, LX/72e;->j()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->b:Lcom/facebook/payments/shipping/model/ShippingParams;

    .line 1163835
    return-void

    .line 1163836
    :cond_0
    iget-object v0, p1, LX/722;->b:Lcom/facebook/payments/shipping/model/ShippingParams;

    move-object v0, v0

    .line 1163837
    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1163838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163839
    const-class v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    iput-object v0, p0, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 1163840
    const-class v0, Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/ShippingParams;

    iput-object v0, p0, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->b:Lcom/facebook/payments/shipping/model/ShippingParams;

    .line 1163841
    return-void
.end method

.method public static newBuilder()LX/722;
    .locals 1

    .prologue
    .line 1163842
    new-instance v0, LX/722;

    invoke-direct {v0}, LX/722;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;
    .locals 1

    .prologue
    .line 1163843
    iget-object v0, p0, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1163844
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1163845
    iget-object v0, p0, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1163846
    iget-object v0, p0, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->b:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1163847
    return-void
.end method
