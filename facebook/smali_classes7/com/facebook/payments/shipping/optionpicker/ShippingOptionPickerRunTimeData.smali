.class public Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerRunTimeData;
.super Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/payments/picker/model/SimplePickerRunTimeData",
        "<",
        "Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;",
        "Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;",
        "Lcom/facebook/payments/picker/model/CoreClientData;",
        "LX/72x;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerRunTimeData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1164927
    new-instance v0, LX/72m;

    invoke-direct {v0}, LX/72m;-><init>()V

    sput-object v0, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerRunTimeData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 1164925
    invoke-direct {p0, p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;-><init>(Landroid/os/Parcel;)V

    .line 1164926
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;)V
    .locals 0

    .prologue
    .line 1164935
    invoke-direct {p0, p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;-><init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;)V

    .line 1164936
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;",
            "Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;",
            "Lcom/facebook/payments/picker/model/CoreClientData;",
            "LX/0P1",
            "<",
            "LX/72x;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1164937
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;-><init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)V

    .line 1164938
    return-void
.end method


# virtual methods
.method public final b()Landroid/content/Intent;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1164929
    sget-object v0, LX/72x;->SHIPPING_OPTIONS:LX/72x;

    invoke-virtual {p0, v0}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a(LX/6vZ;)Ljava/lang/String;

    move-result-object v1

    .line 1164930
    if-nez v1, :cond_0

    .line 1164931
    const/4 v0, 0x0

    .line 1164932
    :goto_0
    return-object v0

    .line 1164933
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1164934
    const-string v2, "extra_shipping_option_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1164928
    const/4 v0, 0x0

    return v0
.end method
