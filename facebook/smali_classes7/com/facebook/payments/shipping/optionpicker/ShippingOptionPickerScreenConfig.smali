.class public Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/picker/model/PickerScreenConfig;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/ShippingOption;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1164979
    new-instance v0, LX/72o;

    invoke-direct {v0}, LX/72o;-><init>()V

    sput-object v0, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/72q;)V
    .locals 2

    .prologue
    .line 1164959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164960
    iget-object v0, p1, LX/72q;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-object v0, v0

    .line 1164961
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    iput-object v0, p0, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 1164962
    iget-object v0, p1, LX/72q;->b:LX/0Px;

    move-object v0, v0

    .line 1164963
    if-eqz v0, :cond_0

    .line 1164964
    iget-object v0, p1, LX/72q;->b:LX/0Px;

    move-object v0, v0

    .line 1164965
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1164966
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No Shipping option passed to show on picker screen"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1164967
    :cond_1
    iget-object v0, p1, LX/72q;->b:LX/0Px;

    move-object v0, v0

    .line 1164968
    iput-object v0, p0, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;->b:LX/0Px;

    .line 1164969
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1164975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164976
    const-class v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    iput-object v0, p0, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 1164977
    const-class v0, Lcom/facebook/payments/shipping/model/ShippingOption;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;->b:LX/0Px;

    .line 1164978
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;
    .locals 1

    .prologue
    .line 1164974
    iget-object v0, p0, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1164973
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1164970
    iget-object v0, p0, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1164971
    iget-object v0, p0, Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1164972
    return-void
.end method
