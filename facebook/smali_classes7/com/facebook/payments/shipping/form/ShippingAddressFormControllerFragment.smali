.class public Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/6yn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/6xb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/73a;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/72M;

.field public e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public f:Landroid/widget/Spinner;

.field public g:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public h:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public l:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

.field public m:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

.field public n:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

.field public o:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

.field public p:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

.field public q:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

.field public r:Lcom/facebook/payments/shipping/model/ShippingParams;

.field public s:LX/73G;

.field public t:LX/73H;

.field public u:LX/6xe;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1164223
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;Ljava/lang/String;Lcom/facebook/payments/ui/PaymentFormEditTextView;IZ)Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;
    .locals 2

    .prologue
    .line 1164211
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1164212
    if-nez v0, :cond_0

    .line 1164213
    new-instance v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-direct {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;-><init>()V

    .line 1164214
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1164215
    :cond_0
    invoke-virtual {v0, p2, p3}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;I)V

    .line 1164216
    new-instance v1, LX/72I;

    invoke-direct {v1, p0, v0}, LX/72I;-><init>(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;)V

    .line 1164217
    move-object v1, v1

    .line 1164218
    iput-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d:Landroid/text/TextWatcher;

    .line 1164219
    iput-boolean p4, v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->g:Z

    .line 1164220
    new-instance v1, LX/72C;

    invoke-direct {v1, p0, p2}, LX/72C;-><init>(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;Lcom/facebook/payments/ui/PaymentFormEditTextView;)V

    .line 1164221
    iput-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a:LX/6vE;

    .line 1164222
    return-object v0
.end method

.method public static b(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;Z)V
    .locals 4

    .prologue
    .line 1164207
    if-eqz p1, :cond_0

    .line 1164208
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->b:LX/6xb;

    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->r:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v0

    iget-object v2, v0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->r:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    if-nez v0, :cond_1

    sget-object v0, LX/6xZ;->ADD_SHIPPING_ADDRESS:LX/6xZ;

    :goto_0
    const-string v3, "payflows_field_focus"

    invoke-virtual {v1, v2, v0, v3}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/String;)V

    .line 1164209
    :cond_0
    return-void

    .line 1164210
    :cond_1
    sget-object v0, LX/6xZ;->UPDATE_SHIPPING_ADDRESS:LX/6xZ;

    goto :goto_0
.end method

.method public static n(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;)LX/6z8;
    .locals 2

    .prologue
    .line 1164206
    new-instance v0, LX/6zF;

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6zF;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1164224
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1164225
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    invoke-static {v0}, LX/6yn;->a(LX/0QB;)LX/6yn;

    move-result-object v2

    check-cast v2, LX/6yn;

    invoke-static {v0}, LX/6xb;->a(LX/0QB;)LX/6xb;

    move-result-object p1

    check-cast p1, LX/6xb;

    invoke-static {v0}, LX/73a;->b(LX/0QB;)LX/73a;

    move-result-object v0

    check-cast v0, LX/73a;

    iput-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->a:LX/6yn;

    iput-object p1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->b:LX/6xb;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->c:LX/73a;

    .line 1164226
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1164199
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setEnabled(Z)V

    .line 1164200
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->g:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setEnabled(Z)V

    .line 1164201
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->h:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setEnabled(Z)V

    .line 1164202
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setEnabled(Z)V

    .line 1164203
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setEnabled(Z)V

    .line 1164204
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setEnabled(Z)V

    .line 1164205
    return-void
.end method

.method public final b()Z
    .locals 7

    .prologue
    .line 1164173
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->l:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e()V

    .line 1164174
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e()V

    .line 1164175
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->n:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e()V

    .line 1164176
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->o:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e()V

    .line 1164177
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e()V

    .line 1164178
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e()V

    .line 1164179
    invoke-virtual {p0}, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1164180
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->d:LX/72M;

    const/4 v5, 0x0

    .line 1164181
    iget-object v1, v0, LX/72M;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    invoke-static {v1}, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->o(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;)V

    .line 1164182
    iget-object v1, v0, LX/72M;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    const-string v2, "payflows_save_click"

    invoke-static {v1, v2}, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->a$redex0(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;Ljava/lang/String;)V

    .line 1164183
    iget-object v1, v0, LX/72M;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    iget-object v1, v1, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->e:LX/72U;

    iget-object v2, v0, LX/72M;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    iget-object v2, v2, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v2}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->a:LX/72g;

    invoke-virtual {v1, v2}, LX/72U;->a(LX/72g;)LX/72T;

    move-result-object v1

    iget-object v2, v0, LX/72M;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    iget-object v2, v2, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->z:LX/6qh;

    .line 1164184
    iput-object v2, v1, LX/72T;->a:LX/6qh;

    .line 1164185
    iget-object v1, v0, LX/72M;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    iget-object v1, v1, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    if-eqz v1, :cond_1

    .line 1164186
    iget-object v1, v0, LX/72M;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    iget-object v1, v1, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->e:LX/72U;

    iget-object v2, v0, LX/72M;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    iget-object v2, v2, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v2}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->a:LX/72g;

    invoke-virtual {v1, v2}, LX/72U;->a(LX/72g;)LX/72T;

    move-result-object v1

    iget-object v2, v0, LX/72M;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    iget-object v2, v2, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v2}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v3, v0, LX/72M;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    invoke-static {v3}, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->q(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;)Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    move-result-object v3

    iget-object v4, v0, LX/72M;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    iget-object v4, v4, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v4}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-interface {v4}, Lcom/facebook/payments/shipping/model/MailingAddress;->a()Ljava/lang/String;

    move-result-object v4

    move v6, v5

    invoke-virtual/range {v1 .. v6}, LX/72T;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;Ljava/lang/String;ZZ)V

    .line 1164187
    :goto_0
    const/4 v0, 0x1

    .line 1164188
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1164189
    :cond_1
    iget-object v1, v0, LX/72M;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    iget-object v1, v1, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->e:LX/72U;

    iget-object v2, v0, LX/72M;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    iget-object v2, v2, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v2}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->a:LX/72g;

    invoke-virtual {v1, v2}, LX/72U;->a(LX/72g;)LX/72T;

    move-result-object v1

    iget-object v2, v0, LX/72M;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    iget-object v2, v2, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v2}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v3, v0, LX/72M;->a:Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    invoke-static {v3}, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->q(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;)Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    move-result-object v3

    .line 1164190
    iget-object v4, v1, LX/72T;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v4}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1164191
    :goto_2
    goto :goto_0

    .line 1164192
    :cond_2
    iget-object v4, v1, LX/72T;->a:LX/6qh;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1164193
    new-instance v4, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;

    invoke-direct {v4, v3}, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;-><init>(Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;)V

    .line 1164194
    iget-object v5, v1, LX/72T;->f:LX/739;

    .line 1164195
    iget-object v6, v5, LX/739;->b:LX/730;

    invoke-virtual {v6, v4}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v4, v6

    .line 1164196
    iput-object v4, v1, LX/72T;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1164197
    iget-object v4, v1, LX/72T;->a:LX/6qh;

    iget-object v5, v1, LX/72T;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, LX/6qh;->a(Lcom/google/common/util/concurrent/ListenableFuture;Z)V

    .line 1164198
    iget-object v4, v1, LX/72T;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v5, LX/72R;

    invoke-direct {v5, v1, v2, v3}, LX/72R;-><init>(LX/72T;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;)V

    iget-object v6, v1, LX/72T;->e:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_2
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1164172
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->l:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->n:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->o:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4f684167

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1164106
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1164107
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1164108
    const-string v2, "extra_shipping_address_params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/ShippingParams;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->r:Lcom/facebook/payments/shipping/model/ShippingParams;

    .line 1164109
    const/4 v6, 0x0

    .line 1164110
    new-instance v0, LX/72B;

    invoke-direct {v0, p0}, LX/72B;-><init>(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;)V

    .line 1164111
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1164112
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->g:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1164113
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->h:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1164114
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1164115
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1164116
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1164117
    const-string v0, "name_input_controller_fragment_tag"

    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v4, 0x7f0d0182

    invoke-static {p0, v0, v2, v4, v6}, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->a(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;Ljava/lang/String;Lcom/facebook/payments/ui/PaymentFormEditTextView;IZ)Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->l:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1164118
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->r:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1164119
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->f:Landroid/widget/Spinner;

    new-instance v4, LX/72D;

    invoke-direct {v4, p0, v0}, LX/72D;-><init>(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;Lcom/facebook/payments/shipping/model/MailingAddress;)V

    invoke-virtual {v2, v4}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1164120
    const-string v0, "address1_input_controller_fragment_tag"

    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->g:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v4, 0x7f0d0183

    invoke-static {p0, v0, v2, v4, v6}, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->a(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;Ljava/lang/String;Lcom/facebook/payments/ui/PaymentFormEditTextView;IZ)Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->m:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1164121
    const-string v0, "address2_input_controller_fragment_tag"

    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->h:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v4, 0x7f0d0184

    const/4 v5, 0x1

    invoke-static {p0, v0, v2, v4, v5}, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->a(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;Ljava/lang/String;Lcom/facebook/payments/ui/PaymentFormEditTextView;IZ)Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->n:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1164122
    const-string v0, "city_input_controller_fragment_tag"

    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v4, 0x7f0d0185

    invoke-static {p0, v0, v2, v4, v6}, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->a(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;Ljava/lang/String;Lcom/facebook/payments/ui/PaymentFormEditTextView;IZ)Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->o:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1164123
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v2, "state_input_controller_fragment_tag"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1164124
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    if-nez v0, :cond_0

    .line 1164125
    new-instance v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-direct {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1164126
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const-string v4, "state_input_controller_fragment_tag"

    invoke-virtual {v0, v2, v4}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1164127
    :cond_0
    new-instance v0, LX/72E;

    invoke-direct {v0, p0}, LX/72E;-><init>(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;)V

    .line 1164128
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v5, 0x7f0d0186

    invoke-virtual {v2, v4, v5}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;I)V

    .line 1164129
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->s:LX/73G;

    .line 1164130
    iput-object v4, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->c:LX/6wl;

    .line 1164131
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1164132
    iput-object v0, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d:Landroid/text/TextWatcher;

    .line 1164133
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->p:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    new-instance v2, LX/72F;

    invoke-direct {v2, p0}, LX/72F;-><init>(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;)V

    .line 1164134
    iput-object v2, v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a:LX/6vE;

    .line 1164135
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v2, "billing_zip_input_controller_fragment_tag"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1164136
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    if-nez v0, :cond_1

    .line 1164137
    new-instance v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-direct {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1164138
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const-string v4, "billing_zip_input_controller_fragment_tag"

    invoke-virtual {v0, v2, v4}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1164139
    :cond_1
    new-instance v0, LX/72G;

    invoke-direct {v0, p0}, LX/72G;-><init>(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;)V

    .line 1164140
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v5, 0x7f0d0181

    invoke-virtual {v2, v4, v5}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;I)V

    .line 1164141
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->a:LX/6yn;

    .line 1164142
    iput-object v4, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b:Landroid/text/TextWatcher;

    .line 1164143
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->t:LX/73H;

    .line 1164144
    iput-object v4, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->c:LX/6wl;

    .line 1164145
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1164146
    iput-object v0, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d:Landroid/text/TextWatcher;

    .line 1164147
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    new-instance v2, LX/72H;

    invoke-direct {v2, p0}, LX/72H;-><init>(Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;)V

    .line 1164148
    iput-object v2, v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a:LX/6vE;

    .line 1164149
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->q:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->u:LX/6xe;

    sget-object v4, LX/6xe;->HIDDEN:LX/6xe;

    if-eq v0, v4, :cond_2

    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->u:LX/6xe;

    sget-object v4, LX/6xe;->OPTIONAL:LX/6xe;

    if-ne v0, v4, :cond_9

    :cond_2
    const/4 v0, 0x1

    .line 1164150
    :goto_0
    iput-boolean v0, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->g:Z

    .line 1164151
    if-eqz p1, :cond_8

    .line 1164152
    const-string v0, "name_edit_text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1164153
    const-string v2, "address1_edit_text"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1164154
    const-string v4, "address2_edit_text"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1164155
    const-string v5, "city_edit_text"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1164156
    const-string v6, "state_edit_text"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1164157
    const-string v7, "billing_zip_edit_text"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1164158
    if-eqz v0, :cond_3

    .line 1164159
    iget-object v8, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v8, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1164160
    :cond_3
    if-eqz v2, :cond_4

    .line 1164161
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->g:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1164162
    :cond_4
    if-eqz v4, :cond_5

    .line 1164163
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->h:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1164164
    :cond_5
    if-eqz v5, :cond_6

    .line 1164165
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1164166
    :cond_6
    if-eqz v6, :cond_7

    .line 1164167
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1164168
    :cond_7
    if-eqz v7, :cond_8

    .line 1164169
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1164170
    :cond_8
    const/16 v0, 0x2b

    const v2, 0x7a2f83ed

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1164171
    :cond_9
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1164092
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1164093
    const-string v0, "name_edit_text"

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164094
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->g:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1164095
    const-string v0, "address1_edit_text"

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->g:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164096
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->h:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1164097
    const-string v0, "address2_edit_text"

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->h:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164098
    :cond_2
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1164099
    const-string v0, "city_edit_text"

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164100
    :cond_3
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1164101
    const-string v0, "state_edit_text"

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164102
    :cond_4
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1164103
    const-string v0, "billing_zip_edit_text"

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1164104
    :cond_5
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1164105
    return-void
.end method
