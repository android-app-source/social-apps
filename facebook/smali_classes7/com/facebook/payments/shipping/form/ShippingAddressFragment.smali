.class public Lcom/facebook/payments/shipping/form/ShippingAddressFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/6xb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/72U;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Landroid/content/Context;

.field public g:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public h:Landroid/widget/Spinner;

.field public i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public l:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public m:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public n:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

.field private o:Landroid/widget/ProgressBar;

.field public p:LX/0h5;

.field public q:Landroid/widget/LinearLayout;

.field public r:LX/6E6;

.field public s:Lcom/facebook/payments/shipping/model/ShippingParams;

.field public t:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

.field public u:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

.field public v:Lcom/google/common/util/concurrent/ListenableFuture;

.field public w:LX/73G;

.field public x:LX/73H;

.field public final y:LX/108;

.field public final z:LX/6qh;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1164454
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1164455
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const/4 v1, 0x0

    .line 1164456
    iput-boolean v1, v0, LX/108;->d:Z

    .line 1164457
    move-object v0, v0

    .line 1164458
    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->y:LX/108;

    .line 1164459
    new-instance v0, LX/72J;

    invoke-direct {v0, p0}, LX/72J;-><init>(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->z:LX/6qh;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1164452
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->d:LX/6xb;

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    sget-object v2, LX/6xZ;->ADD_SHIPPING_ADDRESS:LX/6xZ;

    invoke-virtual {v0, v1, v2, p1}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/String;)V

    .line 1164453
    return-void
.end method

.method public static o(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1164448
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->o:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1164449
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->q:Landroid/widget/LinearLayout;

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1164450
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->t:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->a(Z)V

    .line 1164451
    return-void
.end method

.method public static p(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;)V
    .locals 2

    .prologue
    .line 1164444
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->o:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1164445
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->q:Landroid/widget/LinearLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1164446
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->t:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->a(Z)V

    .line 1164447
    return-void
.end method

.method public static q(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;)Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;
    .locals 3

    .prologue
    .line 1164460
    new-instance v0, LX/72a;

    invoke-direct {v0}, LX/72a;-><init>()V

    move-object v0, v0

    .line 1164461
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->g:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    .line 1164462
    iput-object v1, v0, LX/72a;->a:Ljava/lang/String;

    .line 1164463
    move-object v1, v0

    .line 1164464
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->h:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1164465
    iput-object v0, v1, LX/72a;->b:Ljava/lang/String;

    .line 1164466
    move-object v0, v1

    .line 1164467
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    .line 1164468
    iput-object v1, v0, LX/72a;->c:Ljava/lang/String;

    .line 1164469
    move-object v0, v0

    .line 1164470
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    .line 1164471
    iput-object v1, v0, LX/72a;->d:Ljava/lang/String;

    .line 1164472
    move-object v0, v0

    .line 1164473
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    .line 1164474
    iput-object v1, v0, LX/72a;->e:Ljava/lang/String;

    .line 1164475
    move-object v0, v0

    .line 1164476
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->l:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    .line 1164477
    iput-object v1, v0, LX/72a;->f:Ljava/lang/String;

    .line 1164478
    move-object v0, v0

    .line 1164479
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->m:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    .line 1164480
    iput-object v1, v0, LX/72a;->g:Ljava/lang/String;

    .line 1164481
    move-object v0, v0

    .line 1164482
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->u:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    .line 1164483
    iget-object v2, v1, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->d:Lcom/facebook/common/locale/Country;

    move-object v1, v2

    .line 1164484
    iput-object v1, v0, LX/72a;->h:Lcom/facebook/common/locale/Country;

    .line 1164485
    move-object v0, v0

    .line 1164486
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->r:LX/6E6;

    if-eqz v1, :cond_0

    .line 1164487
    const v1, 0x7f0d24af

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/SwitchCompat;

    .line 1164488
    invoke-virtual {v1}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v1

    .line 1164489
    :goto_0
    move v1, v1

    .line 1164490
    iput-boolean v1, v0, LX/72a;->i:Z

    .line 1164491
    move-object v0, v0

    .line 1164492
    new-instance v1, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    invoke-direct {v1, v0}, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;-><init>(LX/72a;)V

    move-object v0, v1

    .line 1164493
    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 1164442
    const-string v0, "payflows_cancel"

    invoke-static {p0, v0}, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->a$redex0(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;Ljava/lang/String;)V

    .line 1164443
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1164424
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1164425
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v2, 0x7f0e0326

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->f:Landroid/content/Context;

    .line 1164426
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->f:Landroid/content/Context;

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    const/16 v4, 0x12cb

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v5

    check-cast v5, LX/0W9;

    invoke-static {v0}, LX/6xb;->a(LX/0QB;)LX/6xb;

    move-result-object v6

    check-cast v6, LX/6xb;

    invoke-static {v0}, LX/72U;->a(LX/0QB;)LX/72U;

    move-result-object v0

    check-cast v0, LX/72U;

    iput-object v3, v2, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->a:Ljava/util/concurrent/Executor;

    iput-object v4, v2, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->b:LX/0Or;

    iput-object v5, v2, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->c:LX/0W9;

    iput-object v6, v2, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->d:LX/6xb;

    iput-object v0, v2, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->e:LX/72U;

    .line 1164427
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1164428
    const-string v1, "extra_shipping_address_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/ShippingParams;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    .line 1164429
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->d:LX/6xb;

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v2}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->i:LX/6xg;

    sget-object v3, LX/6xZ;->ADD_SHIPPING_ADDRESS:LX/6xZ;

    invoke-virtual {v0, v1, v2, v3, p1}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xg;LX/6xZ;Landroid/os/Bundle;)V

    .line 1164430
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 1164431
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->e:LX/72U;

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->a:LX/72g;

    .line 1164432
    iget-object v2, v0, LX/72U;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1164433
    iget-object v2, v0, LX/72U;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/72Q;

    iget-object v2, v2, LX/72Q;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/73G;

    .line 1164434
    :goto_0
    move-object v0, v2

    .line 1164435
    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->w:LX/73G;

    .line 1164436
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->e:LX/72U;

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->a:LX/72g;

    .line 1164437
    iget-object v2, v0, LX/72U;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1164438
    iget-object v2, v0, LX/72U;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/72Q;

    iget-object v2, v2, LX/72Q;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/73H;

    .line 1164439
    :goto_1
    move-object v0, v2

    .line 1164440
    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->x:LX/73H;

    .line 1164441
    return-void

    :cond_0
    iget-object v2, v0, LX/72U;->a:LX/0P1;

    sget-object v3, LX/72g;->SIMPLE:LX/72g;

    invoke-virtual {v2, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/72Q;

    iget-object v2, v2, LX/72Q;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/73G;

    goto :goto_0

    :cond_1
    iget-object v2, v0, LX/72U;->a:LX/0P1;

    sget-object v3, LX/72g;->SIMPLE:LX/72g;

    invoke-virtual {v2, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/72Q;

    iget-object v2, v2, LX/72Q;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/73H;

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7dfa2d00

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1164423
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->f:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03131f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x6a34c908

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3181f1aa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1164418
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1164419
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 1164420
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1164421
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1164422
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x397cf71e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 1164293
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1164294
    const v0, 0x7f0d051a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->g:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1164295
    const v0, 0x7f0d2c64

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->h:Landroid/widget/Spinner;

    .line 1164296
    const v0, 0x7f0d2c65

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1164297
    const v0, 0x7f0d2c66

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1164298
    const v0, 0x7f0d2c67

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1164299
    const v0, 0x7f0d2c68

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->l:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1164300
    const v0, 0x7f0d08a2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->m:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1164301
    const v0, 0x7f0d08a3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->n:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    .line 1164302
    const v0, 0x7f0d2c63

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->q:Landroid/widget/LinearLayout;

    .line 1164303
    const v0, 0x7f0d2c69

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->o:Landroid/widget/ProgressBar;

    .line 1164304
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->g:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1164305
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->l:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->w:LX/73G;

    invoke-virtual {v1}, LX/73G;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setMaxLength(I)V

    .line 1164306
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->m:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->x:LX/73H;

    invoke-virtual {v1}, LX/73H;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setMaxLength(I)V

    .line 1164307
    if-nez p2, :cond_0

    .line 1164308
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v0

    iget-object v1, v0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1164309
    if-eqz v1, :cond_0

    .line 1164310
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->h:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    .line 1164311
    iget-object p1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->h:Landroid/widget/Spinner;

    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/MailingAddress;->g()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1164312
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/MailingAddress;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1164313
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/MailingAddress;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1164314
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/MailingAddress;->h()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1164315
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->l:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/MailingAddress;->i()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1164316
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->m:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/MailingAddress;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1164317
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1164318
    const v1, 0x7f0d00bb

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 1164319
    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v2}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v2

    iget-object p1, v2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->f:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1164320
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1164321
    check-cast v2, Landroid/view/ViewGroup;

    new-instance p2, LX/72K;

    invoke-direct {p2, p0, v0}, LX/72K;-><init>(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;Landroid/app/Activity;)V

    iget-object v0, p1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    iget-object p1, p1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-virtual {p1}, LX/6ws;->getTitleBarNavIconStyle()LX/73h;

    move-result-object p1

    invoke-virtual {v1, v2, p2, v0, p1}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 1164322
    iget-object v0, v1, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    move-object v0, v0

    .line 1164323
    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->p:LX/0h5;

    .line 1164324
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    if-nez v0, :cond_5

    .line 1164325
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->p:LX/0h5;

    const v1, 0x7f081e49

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1164326
    :goto_0
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->y:LX/108;

    const v1, 0x7f081e4b

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1164327
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 1164328
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->p:LX/0h5;

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->y:LX/108;

    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1164329
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->p:LX/0h5;

    new-instance v1, LX/72L;

    invoke-direct {v1, p0}, LX/72L;-><init>(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1164330
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->e:LX/72U;

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v1}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->a:LX/72g;

    .line 1164331
    iget-object v2, v0, LX/72U;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1164332
    iget-object v2, v0, LX/72U;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/72Q;

    iget-object v2, v2, LX/72Q;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/72Y;

    .line 1164333
    :goto_1
    move-object v0, v2

    .line 1164334
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->z:LX/6qh;

    .line 1164335
    iput-object v1, v0, LX/72Y;->c:LX/6qh;

    .line 1164336
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->q:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    .line 1164337
    new-instance v3, LX/73Y;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v3, p1}, LX/73Y;-><init>(Landroid/content/Context;)V

    iput-object v3, v0, LX/72Y;->a:LX/73Y;

    .line 1164338
    invoke-interface {v2}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v3

    iput-object v3, v0, LX/72Y;->b:Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    .line 1164339
    iget-object v3, v0, LX/72Y;->a:LX/73Y;

    const p1, 0x7f081e56

    invoke-virtual {v3, p1}, LX/73Y;->setSecurityInfo(I)V

    .line 1164340
    iget-object v3, v0, LX/72Y;->b:Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    iget-object v3, v3, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->e:LX/72f;

    sget-object p1, LX/72f;->OTHERS:LX/72f;

    if-ne v3, p1, :cond_2

    .line 1164341
    const/4 v3, 0x0

    .line 1164342
    iget-object p1, v0, LX/72Y;->b:Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    iget-object p1, p1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    if-nez p1, :cond_7

    iget-object p1, v0, LX/72Y;->b:Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    iget p1, p1, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->g:I

    if-lez p1, :cond_7

    .line 1164343
    iget-object p1, v0, LX/72Y;->a:LX/73Y;

    const v1, 0x7f081e57

    .line 1164344
    iget-object v2, p1, LX/73Y;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/SwitchCompat;->setText(I)V

    .line 1164345
    iget-object p1, v0, LX/72Y;->a:LX/73Y;

    invoke-virtual {p1, v3}, LX/73Y;->setVisibilityOfMakeDefaultSwitch(I)V

    .line 1164346
    invoke-static {v0}, LX/72Y;->e(LX/72Y;)V

    .line 1164347
    const/4 v3, 0x1

    .line 1164348
    :goto_2
    move v3, v3

    .line 1164349
    if-nez v3, :cond_1

    const/4 v3, 0x1

    const/4 p1, 0x0

    .line 1164350
    iget-object p2, v0, LX/72Y;->b:Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    iget-object p2, p2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    if-eqz p2, :cond_8

    iget-object p2, v0, LX/72Y;->b:Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    iget-object p2, p2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-interface {p2}, Lcom/facebook/payments/shipping/model/MailingAddress;->j()Z

    move-result p2

    if-nez p2, :cond_8

    iget-object p2, v0, LX/72Y;->b:Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    iget p2, p2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->g:I

    if-le p2, v3, :cond_8

    .line 1164351
    iget-object p2, v0, LX/72Y;->a:LX/73Y;

    const v1, 0x7f081e58

    .line 1164352
    iget-object v2, p2, LX/73Y;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1164353
    iget-object p2, v0, LX/72Y;->a:LX/73Y;

    invoke-virtual {p2, p1}, LX/73Y;->setVisibilityOfMakeDefaultButton(I)V

    .line 1164354
    iget-object p1, v0, LX/72Y;->a:LX/73Y;

    new-instance p2, LX/72W;

    invoke-direct {p2, v0}, LX/72W;-><init>(LX/72Y;)V

    invoke-virtual {p1, p2}, LX/73Y;->setOnClickListenerForMakeDefaultButton(Landroid/view/View$OnClickListener;)V

    .line 1164355
    invoke-static {v0}, LX/72Y;->e(LX/72Y;)V

    .line 1164356
    :goto_3
    move v3, v3

    .line 1164357
    if-nez v3, :cond_1

    const/4 v3, 0x1

    const/4 p1, 0x0

    .line 1164358
    iget-object p2, v0, LX/72Y;->b:Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    iget-object p2, p2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    if-eqz p2, :cond_9

    iget-object p2, v0, LX/72Y;->b:Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    iget-object p2, p2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-interface {p2}, Lcom/facebook/payments/shipping/model/MailingAddress;->j()Z

    move-result p2

    if-eqz p2, :cond_9

    iget-object p2, v0, LX/72Y;->b:Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    iget p2, p2, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->g:I

    if-le p2, v3, :cond_9

    .line 1164359
    iget-object p2, v0, LX/72Y;->a:LX/73Y;

    const v1, 0x7f081e58

    .line 1164360
    iget-object v2, p2, LX/73Y;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1164361
    iget-object p2, v0, LX/72Y;->a:LX/73Y;

    invoke-virtual {p2, p1}, LX/73Y;->setVisibilityOfDefaultInfoView(I)V

    .line 1164362
    invoke-static {v0}, LX/72Y;->e(LX/72Y;)V

    .line 1164363
    :cond_1
    :goto_4
    iget-object v3, v0, LX/72Y;->b:Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    iget-object v3, v3, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    if-eqz v3, :cond_a

    .line 1164364
    iget-object v3, v0, LX/72Y;->a:LX/73Y;

    const p1, 0x7f081e5a

    invoke-virtual {v3, p1}, LX/73Y;->setDeleteButtonText(I)V

    .line 1164365
    iget-object v3, v0, LX/72Y;->a:LX/73Y;

    new-instance p1, LX/72X;

    invoke-direct {p1, v0}, LX/72X;-><init>(LX/72Y;)V

    invoke-virtual {v3, p1}, LX/73Y;->setOnClickListenerForDeleteButton(Landroid/view/View$OnClickListener;)V

    .line 1164366
    iget-object v3, v0, LX/72Y;->a:LX/73Y;

    const/4 p1, 0x0

    invoke-virtual {v3, p1}, LX/73Y;->setVisibilityOfDeleteButton(I)V

    .line 1164367
    :cond_2
    :goto_5
    iget-object v3, v0, LX/72Y;->a:LX/73Y;

    move-object v0, v3

    .line 1164368
    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->r:LX/6E6;

    .line 1164369
    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->q:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->r:LX/6E6;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1164370
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "shipping_address_form_input_controller_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->t:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    .line 1164371
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->t:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    if-nez v0, :cond_3

    .line 1164372
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    .line 1164373
    new-instance v1, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    invoke-direct {v1}, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;-><init>()V

    .line 1164374
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1164375
    const-string v3, "extra_shipping_address_params"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1164376
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1164377
    move-object v0, v1

    .line 1164378
    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->t:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    .line 1164379
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->t:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    const-string v2, "shipping_address_form_input_controller_fragment_tag"

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1164380
    :cond_3
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->t:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->w:LX/73G;

    .line 1164381
    iput-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->s:LX/73G;

    .line 1164382
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->t:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->x:LX/73H;

    .line 1164383
    iput-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->t:LX/73H;

    .line 1164384
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->t:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->g:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v2, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->h:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v4, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v5, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v6, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->l:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v7, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->m:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1164385
    iput-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1164386
    iput-object v2, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->f:Landroid/widget/Spinner;

    .line 1164387
    iput-object v3, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->g:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1164388
    iput-object v4, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->h:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1164389
    iput-object v5, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->i:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1164390
    iput-object v6, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->j:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1164391
    iput-object v7, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1164392
    iget-object p1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->k:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const/4 p2, 0x2

    invoke-virtual {p1, p2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputType(I)V

    .line 1164393
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->t:Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;

    new-instance v1, LX/72M;

    invoke-direct {v1, p0}, LX/72M;-><init>(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;)V

    .line 1164394
    iput-object v1, v0, Lcom/facebook/payments/shipping/form/ShippingAddressFormControllerFragment;->d:LX/72M;

    .line 1164395
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "country_selector_component_controller_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->u:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    .line 1164396
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->u:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    if-nez v0, :cond_4

    .line 1164397
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->b:Lcom/facebook/common/locale/Country;

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->c:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/locale/Country;->a(Ljava/util/Locale;)Lcom/facebook/common/locale/Country;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    .line 1164398
    :goto_6
    invoke-static {}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;->newBuilder()LX/6uD;

    move-result-object v1

    .line 1164399
    iput-object v0, v1, LX/6uD;->b:Lcom/facebook/common/locale/Country;

    .line 1164400
    move-object v0, v1

    .line 1164401
    invoke-virtual {v0}, LX/6uD;->a()Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;

    move-result-object v0

    .line 1164402
    invoke-static {v0}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->a(Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;)Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->u:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    .line 1164403
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->u:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    const-string v2, "country_selector_component_controller_tag"

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1164404
    :cond_4
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->n:Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    iget-object v1, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->u:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->setComponentController(Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;)V

    .line 1164405
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->u:Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    new-instance v1, LX/72N;

    invoke-direct {v1, p0}, LX/72N;-><init>(Lcom/facebook/payments/shipping/form/ShippingAddressFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->a(LX/6u8;)V

    .line 1164406
    return-void

    .line 1164407
    :cond_5
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->p:LX/0h5;

    const v1, 0x7f081e4a

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    iget-object v2, v0, LX/72U;->a:LX/0P1;

    sget-object v3, LX/72g;->SIMPLE:LX/72g;

    invoke-virtual {v2, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/72Q;

    iget-object v2, v2, LX/72Q;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/72Y;

    goto/16 :goto_1

    .line 1164408
    :cond_7
    iget-object p1, v0, LX/72Y;->a:LX/73Y;

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, LX/73Y;->setVisibilityOfMakeDefaultSwitch(I)V

    .line 1164409
    invoke-static {v0}, LX/72Y;->f(LX/72Y;)V

    goto/16 :goto_2

    .line 1164410
    :cond_8
    iget-object v3, v0, LX/72Y;->a:LX/73Y;

    const/16 p2, 0x8

    invoke-virtual {v3, p2}, LX/73Y;->setVisibilityOfMakeDefaultButton(I)V

    .line 1164411
    invoke-static {v0}, LX/72Y;->f(LX/72Y;)V

    move v3, p1

    .line 1164412
    goto/16 :goto_3

    .line 1164413
    :cond_9
    iget-object v3, v0, LX/72Y;->a:LX/73Y;

    const/16 p2, 0x8

    invoke-virtual {v3, p2}, LX/73Y;->setVisibilityOfDefaultInfoView(I)V

    .line 1164414
    invoke-static {v0}, LX/72Y;->f(LX/72Y;)V

    .line 1164415
    goto/16 :goto_4

    .line 1164416
    :cond_a
    iget-object v3, v0, LX/72Y;->a:LX/73Y;

    const/16 p1, 0x8

    invoke-virtual {v3, p1}, LX/73Y;->setVisibilityOfDeleteButton(I)V

    goto/16 :goto_5

    .line 1164417
    :cond_b
    iget-object v0, p0, Lcom/facebook/payments/shipping/form/ShippingAddressFragment;->s:Lcom/facebook/payments/shipping/model/ShippingParams;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/ShippingParams;->a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-interface {v0}, Lcom/facebook/payments/shipping/model/MailingAddress;->f()Lcom/facebook/common/locale/Country;

    move-result-object v0

    goto/16 :goto_6
.end method
