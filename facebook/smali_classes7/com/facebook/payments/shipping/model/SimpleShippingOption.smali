.class public Lcom/facebook/payments/shipping/model/SimpleShippingOption;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/shipping/model/ShippingOption;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/shipping/model/SimpleShippingOption;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1164904
    new-instance v0, LX/72k;

    invoke-direct {v0}, LX/72k;-><init>()V

    sput-object v0, Lcom/facebook/payments/shipping/model/SimpleShippingOption;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/72l;)V
    .locals 1

    .prologue
    .line 1164905
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164906
    iget-object v0, p1, LX/72l;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1164907
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleShippingOption;->a:Ljava/lang/String;

    .line 1164908
    iget-object v0, p1, LX/72l;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1164909
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleShippingOption;->b:Ljava/lang/String;

    .line 1164910
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1164911
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164912
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleShippingOption;->a:Ljava/lang/String;

    .line 1164913
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleShippingOption;->b:Ljava/lang/String;

    .line 1164914
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1164915
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleShippingOption;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1164916
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleShippingOption;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1164917
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1164918
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleShippingOption;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1164919
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleShippingOption;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1164920
    return-void
.end method
