.class public Lcom/facebook/payments/shipping/model/SimpleMailingAddress;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/shipping/model/MailingAddress;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/shipping/model/SimpleMailingAddress;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Lcom/facebook/common/locale/Country;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1164889
    new-instance v0, LX/72h;

    invoke-direct {v0}, LX/72h;-><init>()V

    sput-object v0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/72i;)V
    .locals 1

    .prologue
    .line 1164865
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164866
    iget-object v0, p1, LX/72i;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1164867
    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->a:Ljava/lang/String;

    .line 1164868
    iget-object v0, p1, LX/72i;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1164869
    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->b:Ljava/lang/String;

    .line 1164870
    iget-object v0, p1, LX/72i;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1164871
    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->c:Ljava/lang/String;

    .line 1164872
    iget-object v0, p1, LX/72i;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1164873
    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->d:Ljava/lang/String;

    .line 1164874
    iget-object v0, p1, LX/72i;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1164875
    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->e:Ljava/lang/String;

    .line 1164876
    iget-object v0, p1, LX/72i;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1164877
    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->f:Ljava/lang/String;

    .line 1164878
    iget-object v0, p1, LX/72i;->g:Lcom/facebook/common/locale/Country;

    move-object v0, v0

    .line 1164879
    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->g:Lcom/facebook/common/locale/Country;

    .line 1164880
    iget-object v0, p1, LX/72i;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1164881
    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->h:Ljava/lang/String;

    .line 1164882
    iget-object v0, p1, LX/72i;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1164883
    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->i:Ljava/lang/String;

    .line 1164884
    iget-object v0, p1, LX/72i;->j:Ljava/lang/String;

    move-object v0, v0

    .line 1164885
    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->j:Ljava/lang/String;

    .line 1164886
    iget-boolean v0, p1, LX/72i;->k:Z

    move v0, v0

    .line 1164887
    iput-boolean v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->k:Z

    .line 1164888
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1164852
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164853
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->a:Ljava/lang/String;

    .line 1164854
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->b:Ljava/lang/String;

    .line 1164855
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->c:Ljava/lang/String;

    .line 1164856
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->d:Ljava/lang/String;

    .line 1164857
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->e:Ljava/lang/String;

    .line 1164858
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->f:Ljava/lang/String;

    .line 1164859
    const-class v0, Lcom/facebook/common/locale/Country;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->g:Lcom/facebook/common/locale/Country;

    .line 1164860
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->h:Ljava/lang/String;

    .line 1164861
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->i:Ljava/lang/String;

    .line 1164862
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->j:Ljava/lang/String;

    .line 1164863
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->k:Z

    .line 1164864
    return-void
.end method

.method public static newBuilder()LX/72i;
    .locals 1

    .prologue
    .line 1164851
    new-instance v0, LX/72i;

    invoke-direct {v0}, LX/72i;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1164850
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/payments/shipping/model/SimpleMailingAddressFormatter$FormatType;
        .end annotation
    .end param

    .prologue
    .line 1164849
    invoke-static {p0, p1}, LX/72j;->a(Lcom/facebook/payments/shipping/model/MailingAddress;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1164848
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1164890
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1164847
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1164846
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1164845
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Lcom/facebook/common/locale/Country;
    .locals 1

    .prologue
    .line 1164844
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->g:Lcom/facebook/common/locale/Country;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1164843
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1164842
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1164841
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 1164840
    iget-boolean v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->k:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1164828
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1164829
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1164830
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1164831
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1164832
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1164833
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1164834
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->g:Lcom/facebook/common/locale/Country;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1164835
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1164836
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1164837
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1164838
    iget-boolean v0, p0, Lcom/facebook/payments/shipping/model/SimpleMailingAddress;->k:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1164839
    return-void
.end method
