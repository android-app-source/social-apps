.class public Lcom/facebook/payments/shipping/model/ShippingCommonParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/shipping/model/ShippingParams;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/shipping/model/ShippingCommonParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/72g;

.field public final b:Lcom/facebook/common/locale/Country;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/6xe;

.field public final d:Lcom/facebook/payments/shipping/model/MailingAddress;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:LX/72f;

.field public final f:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

.field public final g:I

.field public final h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

.field public final i:LX/6xg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1164735
    new-instance v0, LX/72d;

    invoke-direct {v0}, LX/72d;-><init>()V

    sput-object v0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/72e;)V
    .locals 2

    .prologue
    .line 1164760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164761
    iget-object v0, p1, LX/72e;->a:LX/72g;

    move-object v0, v0

    .line 1164762
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/72g;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->a:LX/72g;

    .line 1164763
    iget-object v0, p1, LX/72e;->b:Lcom/facebook/common/locale/Country;

    move-object v0, v0

    .line 1164764
    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->b:Lcom/facebook/common/locale/Country;

    .line 1164765
    iget-object v0, p1, LX/72e;->c:LX/6xe;

    move-object v0, v0

    .line 1164766
    if-eqz v0, :cond_0

    .line 1164767
    iget-object v0, p1, LX/72e;->c:LX/6xe;

    move-object v0, v0

    .line 1164768
    :goto_0
    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->c:LX/6xe;

    .line 1164769
    iget-object v0, p1, LX/72e;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    move-object v0, v0

    .line 1164770
    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1164771
    iget-object v0, p1, LX/72e;->e:LX/72f;

    move-object v0, v0

    .line 1164772
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/72f;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->e:LX/72f;

    .line 1164773
    iget-object v0, p1, LX/72e;->f:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-object v0, v0

    .line 1164774
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->f:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1164775
    iget v0, p1, LX/72e;->g:I

    move v0, v0

    .line 1164776
    iput v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->g:I

    .line 1164777
    iget-object v0, p1, LX/72e;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-object v0, v0

    .line 1164778
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1164779
    iget-object v0, p1, LX/72e;->i:LX/6xg;

    move-object v0, v0

    .line 1164780
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->i:LX/6xg;

    .line 1164781
    return-void

    .line 1164782
    :cond_0
    sget-object v0, LX/6xe;->REQUIRED:LX/6xe;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1164748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1164749
    const-class v0, LX/72g;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/72g;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->a:LX/72g;

    .line 1164750
    const-class v0, Lcom/facebook/common/locale/Country;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->b:Lcom/facebook/common/locale/Country;

    .line 1164751
    const-class v0, LX/6xe;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xe;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->c:LX/6xe;

    .line 1164752
    const-class v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 1164753
    const-class v0, LX/72f;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/72f;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->e:LX/72f;

    .line 1164754
    const-class v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->f:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1164755
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->g:I

    .line 1164756
    const-class v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1164757
    const-class v0, LX/6xg;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->i:LX/6xg;

    .line 1164758
    return-void
.end method

.method public static newBuilder()LX/72e;
    .locals 1

    .prologue
    .line 1164759
    new-instance v0, LX/72e;

    invoke-direct {v0}, LX/72e;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/shipping/model/ShippingCommonParams;
    .locals 0

    .prologue
    .line 1164747
    return-object p0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1164746
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1164736
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->a:LX/72g;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1164737
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->b:Lcom/facebook/common/locale/Country;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1164738
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->c:LX/6xe;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1164739
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->d:Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1164740
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->e:LX/72f;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1164741
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->f:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1164742
    iget v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1164743
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1164744
    iget-object v0, p0, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->i:LX/6xg;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1164745
    return-void
.end method
