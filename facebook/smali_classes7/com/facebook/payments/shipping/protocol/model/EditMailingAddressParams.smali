.class public Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

.field public final b:Ljava/lang/String;

.field public final c:Z

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1165406
    new-instance v0, LX/73E;

    invoke-direct {v0}, LX/73E;-><init>()V

    sput-object v0, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1165400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1165401
    const-class v0, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    iput-object v0, p0, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    .line 1165402
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->b:Ljava/lang/String;

    .line 1165403
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->c:Z

    .line 1165404
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->d:Z

    .line 1165405
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;Ljava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 1165392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1165393
    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1165394
    iput-object p1, p0, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    .line 1165395
    iput-object p2, p0, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->b:Ljava/lang/String;

    .line 1165396
    iput-boolean p3, p0, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->c:Z

    .line 1165397
    iput-boolean p4, p0, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->d:Z

    .line 1165398
    return-void

    .line 1165399
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1165386
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1165387
    iget-object v0, p0, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1165388
    iget-object v0, p0, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1165389
    iget-boolean v0, p0, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1165390
    iget-boolean v0, p0, Lcom/facebook/payments/shipping/protocol/model/EditMailingAddressParams;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1165391
    return-void
.end method
