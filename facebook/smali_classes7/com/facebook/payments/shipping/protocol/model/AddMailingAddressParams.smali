.class public Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1165337
    new-instance v0, LX/73C;

    invoke-direct {v0}, LX/73C;-><init>()V

    sput-object v0, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1165338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1165339
    const-class v0, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    iput-object v0, p0, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    .line 1165340
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;)V
    .locals 0

    .prologue
    .line 1165341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1165342
    iput-object p1, p0, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    .line 1165343
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1165344
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1165345
    iget-object v0, p0, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressParams;->a:Lcom/facebook/payments/shipping/model/ShippingAddressFormInput;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1165346
    return-void
.end method
