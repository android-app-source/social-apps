.class public Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressResultDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mMailingAddressId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1165361
    const-class v0, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1165357
    new-instance v0, LX/73D;

    invoke-direct {v0}, LX/73D;-><init>()V

    sput-object v0, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1165353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1165354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressResult;->mMailingAddressId:Ljava/lang/String;

    .line 1165355
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1165358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1165359
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressResult;->mMailingAddressId:Ljava/lang/String;

    .line 1165360
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1165356
    iget-object v0, p0, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressResult;->mMailingAddressId:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1165352
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1165350
    iget-object v0, p0, Lcom/facebook/payments/shipping/protocol/model/AddMailingAddressResult;->mMailingAddressId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1165351
    return-void
.end method
