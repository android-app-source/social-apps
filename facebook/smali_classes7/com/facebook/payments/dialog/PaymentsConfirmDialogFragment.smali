.class public Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;
.super Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;
.source ""


# instance fields
.field public m:LX/6wv;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1158210
    invoke-direct {p0}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;-><init>()V

    .line 1158211
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1158177
    new-instance v0, LX/6dy;

    invoke-direct {v0, p0, p2}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1158178
    iput-object p1, v0, LX/6dy;->d:Ljava/lang/String;

    .line 1158179
    move-object v0, v0

    .line 1158180
    iput-object p3, v0, LX/6dy;->c:Ljava/lang/String;

    .line 1158181
    move-object v0, v0

    .line 1158182
    iput-boolean p4, v0, LX/6dy;->f:Z

    .line 1158183
    move-object v0, v0

    .line 1158184
    invoke-virtual {v0}, LX/6dy;->a()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v0

    .line 1158185
    invoke-static {v0}, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->b(Lcom/facebook/messaging/dialog/ConfirmActionParams;)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/messaging/dialog/ConfirmActionParams;)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;
    .locals 2

    .prologue
    .line 1158205
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1158206
    const-string v1, "confirm_action_params"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1158207
    new-instance v1, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    invoke-direct {v1}, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;-><init>()V

    .line 1158208
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1158209
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1158202
    iget-object v0, p0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    if-eqz v0, :cond_0

    .line 1158203
    iget-object v0, p0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    invoke-interface {v0}, LX/6wv;->a()V

    .line 1158204
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1158199
    iget-object v0, p0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    if-eqz v0, :cond_0

    .line 1158200
    iget-object v0, p0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    invoke-interface {v0}, LX/6wv;->b()V

    .line 1158201
    :cond_0
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 1158195
    invoke-super {p0}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->k()V

    .line 1158196
    iget-object v0, p0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    if-eqz v0, :cond_0

    .line 1158197
    iget-object v0, p0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    invoke-interface {v0}, LX/6wv;->c()V

    .line 1158198
    :cond_0
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 1158191
    invoke-super {p0, p1}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 1158192
    iget-object v0, p0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    if-eqz v0, :cond_0

    .line 1158193
    iget-object v0, p0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    invoke-interface {v0}, LX/6wv;->c()V

    .line 1158194
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x49605052    # 918789.1f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1158186
    invoke-super {p0, p1}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1158187
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1158188
    const-string v2, "confirm_action_params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/dialog/ConfirmActionParams;

    .line 1158189
    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    .line 1158190
    const/16 v0, 0x2b

    const v2, 0xce7b46c

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
