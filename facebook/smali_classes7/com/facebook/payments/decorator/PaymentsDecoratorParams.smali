.class public Lcom/facebook/payments/decorator/PaymentsDecoratorParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/decorator/PaymentsDecoratorParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6ws;

.field public final b:LX/73i;

.field public final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1158153
    new-instance v0, LX/6wt;

    invoke-direct {v0}, LX/6wt;-><init>()V

    sput-object v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6wu;)V
    .locals 2

    .prologue
    .line 1158160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158161
    iget-object v0, p1, LX/6wu;->a:LX/6ws;

    move-object v0, v0

    .line 1158162
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ws;

    iput-object v0, p0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    .line 1158163
    iget-object v0, p1, LX/6wu;->b:LX/73i;

    move-object v0, v0

    .line 1158164
    sget-object v1, LX/73i;->DEFAULT:LX/73i;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/73i;

    iput-object v0, p0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    .line 1158165
    iget-object v0, p1, LX/6wu;->c:LX/0am;

    move-object v0, v0

    .line 1158166
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    iput-object v0, p0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->c:LX/0am;

    .line 1158167
    iget-boolean v0, p1, LX/6wu;->d:Z

    move v0, v0

    .line 1158168
    iput-boolean v0, p0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    .line 1158169
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1158154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158155
    const-class v0, LX/6ws;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6ws;

    iput-object v0, p0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    .line 1158156
    const-class v0, LX/73i;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/73i;

    iput-object v0, p0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    .line 1158157
    invoke-static {p1}, LX/46R;->g(Landroid/os/Parcel;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->c:LX/0am;

    .line 1158158
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    .line 1158159
    return-void
.end method

.method public static a()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;
    .locals 2

    .prologue
    .line 1158118
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v0

    sget-object v1, LX/6ws;->MODAL_BOTTOM:LX/6ws;

    .line 1158119
    iput-object v1, v0, LX/6wu;->a:LX/6ws;

    .line 1158120
    move-object v0, v0

    .line 1158121
    invoke-virtual {v0}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)Lcom/facebook/payments/decorator/PaymentsDecoratorParams;
    .locals 2

    .prologue
    .line 1158149
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/6wu;->a(Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6wu;

    move-result-object v0

    sget-object v1, LX/6ws;->MODAL_BOTTOM:LX/6ws;

    .line 1158150
    iput-object v1, v0, LX/6wu;->a:LX/6ws;

    .line 1158151
    move-object v0, v0

    .line 1158152
    invoke-virtual {v0}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v0

    return-object v0
.end method

.method public static b()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;
    .locals 2

    .prologue
    .line 1158170
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v0

    sget-object v1, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    .line 1158171
    iput-object v1, v0, LX/6wu;->a:LX/6ws;

    .line 1158172
    move-object v0, v0

    .line 1158173
    invoke-virtual {v0}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v0

    return-object v0
.end method

.method public static c()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;
    .locals 2

    .prologue
    .line 1158142
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v0

    sget-object v1, LX/6ws;->MODAL_BOTTOM:LX/6ws;

    .line 1158143
    iput-object v1, v0, LX/6wu;->a:LX/6ws;

    .line 1158144
    move-object v0, v0

    .line 1158145
    sget-object v1, LX/73i;->PAYMENTS_WHITE:LX/73i;

    .line 1158146
    iput-object v1, v0, LX/6wu;->b:LX/73i;

    .line 1158147
    move-object v0, v0

    .line 1158148
    invoke-virtual {v0}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;
    .locals 2

    .prologue
    .line 1158135
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->newBuilder()LX/6wu;

    move-result-object v0

    sget-object v1, LX/6ws;->SLIDE_RIGHT:LX/6ws;

    .line 1158136
    iput-object v1, v0, LX/6wu;->a:LX/6ws;

    .line 1158137
    move-object v0, v0

    .line 1158138
    sget-object v1, LX/73i;->PAYMENTS_WHITE:LX/73i;

    .line 1158139
    iput-object v1, v0, LX/6wu;->b:LX/73i;

    .line 1158140
    move-object v0, v0

    .line 1158141
    invoke-virtual {v0}, LX/6wu;->e()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder()LX/6wu;
    .locals 1

    .prologue
    .line 1158134
    new-instance v0, LX/6wu;

    invoke-direct {v0}, LX/6wu;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1158133
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1158122
    iget-object v0, p0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1158123
    iget-object v0, p0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1158124
    iget-object v0, p0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->c:LX/0am;

    .line 1158125
    if-nez v0, :cond_0

    .line 1158126
    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1158127
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1158128
    return-void

    .line 1158129
    :cond_0
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result p2

    if-nez p2, :cond_1

    .line 1158130
    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 1158131
    :cond_1
    const/4 p2, 0x2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1158132
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-static {p1, p2}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Integer;)V

    goto :goto_0
.end method
