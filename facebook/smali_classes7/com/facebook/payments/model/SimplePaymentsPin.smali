.class public Lcom/facebook/payments/model/SimplePaymentsPin;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/model/PaymentsPin;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/model/SimplePaymentsPin;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/payments/model/SimplePaymentsPin;


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1159169
    new-instance v0, Lcom/facebook/payments/model/SimplePaymentsPin;

    invoke-direct {v0}, Lcom/facebook/payments/model/SimplePaymentsPin;-><init>()V

    sput-object v0, Lcom/facebook/payments/model/SimplePaymentsPin;->a:Lcom/facebook/payments/model/SimplePaymentsPin;

    .line 1159170
    new-instance v0, LX/6xl;

    invoke-direct {v0}, LX/6xl;-><init>()V

    sput-object v0, Lcom/facebook/payments/model/SimplePaymentsPin;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1159171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1159172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/payments/model/SimplePaymentsPin;->b:Ljava/lang/String;

    .line 1159173
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1159163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1159164
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/model/SimplePaymentsPin;->b:Ljava/lang/String;

    .line 1159165
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1159166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1159167
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/model/SimplePaymentsPin;->b:Ljava/lang/String;

    .line 1159168
    return-void
.end method


# virtual methods
.method public final a()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1159162
    iget-object v0, p0, Lcom/facebook/payments/model/SimplePaymentsPin;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1159161
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1159159
    iget-object v0, p0, Lcom/facebook/payments/model/SimplePaymentsPin;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1159160
    return-void
.end method
