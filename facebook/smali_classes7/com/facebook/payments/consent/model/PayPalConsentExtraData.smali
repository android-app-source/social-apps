.class public Lcom/facebook/payments/consent/model/PayPalConsentExtraData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/consent/model/ConsentExtraData;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/consent/model/PayPalConsentExtraData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1156358
    new-instance v0, LX/6uy;

    invoke-direct {v0}, LX/6uy;-><init>()V

    sput-object v0, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1156354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156355
    const-class v0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    iput-object v0, p0, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;->a:Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    .line 1156356
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;->b:Ljava/lang/String;

    .line 1156357
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;)V
    .locals 1

    .prologue
    .line 1156359
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;-><init>(Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;Ljava/lang/String;)V

    .line 1156360
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1156350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156351
    iput-object p1, p0, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;->a:Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    .line 1156352
    iput-object p2, p0, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;->b:Ljava/lang/String;

    .line 1156353
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1156349
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1156346
    iget-object v0, p0, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;->a:Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1156347
    iget-object v0, p0, Lcom/facebook/payments/consent/model/PayPalConsentExtraData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1156348
    return-void
.end method
