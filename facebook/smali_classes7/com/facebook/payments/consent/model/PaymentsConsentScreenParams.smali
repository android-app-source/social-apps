.class public Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

.field public final b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

.field public final c:Lcom/facebook/payments/consent/model/ConsentExtraData;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1156395
    new-instance v0, LX/6uz;

    invoke-direct {v0}, LX/6uz;-><init>()V

    sput-object v0, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6v0;)V
    .locals 1

    .prologue
    .line 1156390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156391
    iget-object v0, p1, LX/6v0;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1156392
    iget-object v0, p1, LX/6v0;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1156393
    iget-object v0, p1, LX/6v0;->a:Lcom/facebook/payments/consent/model/ConsentExtraData;

    iput-object v0, p0, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->c:Lcom/facebook/payments/consent/model/ConsentExtraData;

    .line 1156394
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1156385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156386
    const-class v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1156387
    const-class v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1156388
    const-class v0, Lcom/facebook/payments/consent/model/ConsentExtraData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/consent/model/ConsentExtraData;

    iput-object v0, p0, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->c:Lcom/facebook/payments/consent/model/ConsentExtraData;

    .line 1156389
    return-void
.end method

.method public static a(Lcom/facebook/payments/consent/model/ConsentExtraData;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6v0;
    .locals 2

    .prologue
    .line 1156384
    new-instance v0, LX/6v0;

    invoke-direct {v0, p0, p1}, LX/6v0;-><init>(Lcom/facebook/payments/consent/model/ConsentExtraData;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1156383
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1156379
    iget-object v0, p0, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1156380
    iget-object v0, p0, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1156381
    iget-object v0, p0, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->c:Lcom/facebook/payments/consent/model/ConsentExtraData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1156382
    return-void
.end method
