.class public Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1156271
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1156285
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1156286
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1156287
    const-string v1, "extra_consent_screen_params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1156288
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;

    invoke-static {v0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v0

    check-cast v0, LX/6wr;

    iput-object v0, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;->p:LX/6wr;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1156289
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1156290
    const v0, 0x7f03003a

    invoke-virtual {p0, v0}, Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;->setContentView(I)V

    .line 1156291
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "consent_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1156292
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d03c5

    iget-object v2, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;->q:Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    .line 1156293
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1156294
    const-string p1, "extra_consent_screen_params"

    invoke-virtual {v3, p1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1156295
    new-instance p1, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;

    invoke-direct {p1}, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;-><init>()V

    .line 1156296
    invoke-virtual {p1, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1156297
    move-object v2, p1

    .line 1156298
    const-string v3, "consent_fragment_tag"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1156299
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;->q:Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    iget-object v0, v0, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->a(Landroid/app/Activity;LX/6ws;)V

    .line 1156300
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1156280
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 1156281
    invoke-static {p0, p0}, Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1156282
    invoke-virtual {p0}, Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_consent_screen_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    iput-object v0, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;->q:Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    .line 1156283
    iget-object v0, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;->p:LX/6wr;

    iget-object v1, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;->q:Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    iget-object v1, v1, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v1, v1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-virtual {v0, p0, v1}, LX/6wr;->b(Landroid/app/Activity;LX/73i;)V

    .line 1156284
    return-void
.end method

.method public final finish()V
    .locals 1

    .prologue
    .line 1156277
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1156278
    iget-object v0, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenActivity;->q:Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    iget-object v0, v0, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->b(Landroid/app/Activity;LX/6ws;)V

    .line 1156279
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1156272
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "consent_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1156273
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/0fj;

    if-eqz v1, :cond_0

    .line 1156274
    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    .line 1156275
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1156276
    return-void
.end method
