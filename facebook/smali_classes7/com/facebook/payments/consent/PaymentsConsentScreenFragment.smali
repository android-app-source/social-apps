.class public Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:LX/6uu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:LX/6qh;

.field private d:Landroid/content/Context;

.field public e:Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1156341
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1156342
    new-instance v0, LX/6uv;

    invoke-direct {v0, p0}, LX/6uv;-><init>(Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->c:LX/6qh;

    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;

    new-instance v4, LX/6uu;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {v1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    new-instance p2, LX/70L;

    invoke-static {v1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p0

    check-cast p0, LX/0tX;

    invoke-direct {p2, v3, p0}, LX/70L;-><init>(Ljava/util/concurrent/Executor;LX/0tX;)V

    move-object v3, p2

    check-cast v3, LX/70L;

    invoke-direct {v4, v0, v2, v3}, LX/6uu;-><init>(Landroid/content/res/Resources;LX/1Ck;LX/70L;)V

    move-object v0, v4

    check-cast v0, LX/6uu;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p1, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->a:LX/6uu;

    iput-object v1, p1, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->b:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 1156318
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1156335
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1156336
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v2, 0x7f0e0326

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->d:Landroid/content/Context;

    .line 1156337
    const-class v0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;

    iget-object v1, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->d:Landroid/content/Context;

    invoke-static {v0, p0, v1}, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1156338
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1156339
    const-string v1, "extra_consent_screen_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    iput-object v0, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->e:Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    .line 1156340
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2bcb675e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1156334
    iget-object v1, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->d:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0306ad

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x38e054ab

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x67bdbdee

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1156330
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1156331
    iget-object v1, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->a:LX/6uu;

    .line 1156332
    iget-object v2, v1, LX/6uu;->b:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1156333
    const/16 v1, 0x2b

    const v2, -0x4ecc4d5e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1156319
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1156320
    const v0, 0x7f0d00bb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 1156321
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1156322
    check-cast v1, Landroid/view/ViewGroup;

    new-instance v2, LX/6uw;

    invoke-direct {v2, p0}, LX/6uw;-><init>(Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;)V

    iget-object v3, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->e:Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    iget-object v3, v3, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v3, v3, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    iget-object v4, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->e:Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    iget-object v4, v4, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v4, v4, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-virtual {v4}, LX/6ws;->getTitleBarNavIconStyle()LX/73h;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 1156323
    iget-object v1, v0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    move-object v0, v1

    .line 1156324
    iget-object v1, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->a:LX/6uu;

    iget-object v2, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->e:Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    iget-object v2, v2, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->c:Lcom/facebook/payments/consent/model/ConsentExtraData;

    invoke-virtual {v1, v2}, LX/6uu;->b(Lcom/facebook/payments/consent/model/ConsentExtraData;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1156325
    const v0, 0x7f0d123c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->d:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->e:Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    iget-object v1, v1, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->c:Lcom/facebook/payments/consent/model/ConsentExtraData;

    invoke-static {v1}, LX/6uu;->a(Lcom/facebook/payments/consent/model/ConsentExtraData;)I

    move-result v3

    move-object v1, p1

    check-cast v1, Landroid/view/ViewGroup;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 1156326
    iget-object v0, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->a:LX/6uu;

    iget-object v1, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->c:LX/6qh;

    .line 1156327
    iput-object v1, v0, LX/6uu;->d:LX/6qh;

    .line 1156328
    iget-object v0, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->a:LX/6uu;

    iget-object v1, p0, Lcom/facebook/payments/consent/PaymentsConsentScreenFragment;->e:Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;

    iget-object v1, v1, Lcom/facebook/payments/consent/model/PaymentsConsentScreenParams;->c:Lcom/facebook/payments/consent/model/ConsentExtraData;

    invoke-virtual {v0, p1, v1}, LX/6uu;->a(Landroid/view/View;Lcom/facebook/payments/consent/model/ConsentExtraData;)V

    .line 1156329
    return-void
.end method
