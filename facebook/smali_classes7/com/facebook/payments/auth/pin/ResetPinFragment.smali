.class public Lcom/facebook/payments/auth/pin/ResetPinFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/73q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/content/Context;

.field public c:Lcom/facebook/resources/ui/FbEditText;

.field private d:Landroid/widget/ImageView;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field public f:LX/6oW;

.field private g:Ljava/lang/String;

.field private h:Lcom/facebook/resources/ui/FbEditText;

.field private i:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1148338
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1148334
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setEnabled(Z)V

    .line 1148335
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setFocusableInTouchMode(Z)V

    .line 1148336
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setClickable(Z)V

    .line 1148337
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1148330
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1148331
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v2, 0x7f0e0326

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->b:Landroid/content/Context;

    .line 1148332
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->b:Landroid/content/Context;

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;

    invoke-static {v0}, LX/73q;->b(LX/0QB;)LX/73q;

    move-result-object v0

    check-cast v0, LX/73q;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->a:LX/73q;

    .line 1148333
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1148320
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 1148321
    sget-object v2, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v0, v2, :cond_0

    .line 1148322
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/6up;->a(Landroid/content/Context;Lcom/facebook/fbservice/service/ServiceException;)V

    move v0, v1

    .line 1148323
    :goto_0
    return v0

    .line 1148324
    :cond_0
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1148325
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 1148326
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v2

    const/16 v3, 0x64

    if-eq v2, v3, :cond_1

    .line 1148327
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/payments/auth/pin/EnterPinFragment;->a(Landroid/content/Context;Ljava/lang/String;)LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    move v0, v1

    .line 1148328
    goto :goto_0

    .line 1148329
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1148317
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1148318
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->a:LX/73q;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1, v2}, LX/73q;->a(Landroid/app/Activity;Landroid/view/View;)V

    .line 1148319
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1148311
    const/4 v1, 0x0

    .line 1148312
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setEnabled(Z)V

    .line 1148313
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setFocusable(Z)V

    .line 1148314
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setClickable(Z)V

    .line 1148315
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->i:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1148316
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1148292
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->i:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1148293
    invoke-direct {p0}, Lcom/facebook/payments/auth/pin/ResetPinFragment;->k()V

    .line 1148294
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, 0x5ecced0a

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1148295
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->b:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030f05

    invoke-virtual {v0, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1148296
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v0

    .line 1148297
    const v0, 0x7f0d2498

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1148298
    const-string v0, "savedHeaderText"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->g:Ljava/lang/String;

    .line 1148299
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v3, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1148300
    const v0, 0x7f0d04de

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->i:Landroid/widget/ProgressBar;

    .line 1148301
    const v0, 0x7f0d0cd4

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->h:Lcom/facebook/resources/ui/FbEditText;

    .line 1148302
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->h:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbEditText;->setFocusable(Z)V

    .line 1148303
    const v0, 0x7f0d0cd5

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->d:Landroid/widget/ImageView;

    .line 1148304
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->d:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1148305
    const v0, 0x7f0d249b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    .line 1148306
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbEditText;->setVisibility(I)V

    .line 1148307
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    new-instance v3, LX/6oe;

    invoke-direct {v3, p0}, LX/6oe;-><init>(Lcom/facebook/payments/auth/pin/ResetPinFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1148308
    invoke-direct {p0}, Lcom/facebook/payments/auth/pin/ResetPinFragment;->k()V

    .line 1148309
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->a:LX/73q;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/payments/auth/pin/ResetPinFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v3, v4}, LX/73q;->a(Landroid/app/Activity;Landroid/view/View;)V

    .line 1148310
    const/16 v0, 0x2b

    const v3, -0x4c680d0a

    invoke-static {v5, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
