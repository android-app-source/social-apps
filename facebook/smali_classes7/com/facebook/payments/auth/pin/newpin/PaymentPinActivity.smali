.class public Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1148889
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1148884
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1148885
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1148886
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1148887
    const-string v1, "payment_pin_params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1148888
    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1148890
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "payment_pin_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1148891
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d03c5

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->q:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1148892
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1148893
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1148894
    const-string p0, "payment_pin_params"

    invoke-virtual {v3, p0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1148895
    new-instance p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    invoke-direct {p0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;-><init>()V

    .line 1148896
    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1148897
    move-object v2, p0

    .line 1148898
    const-string v3, "payment_pin_fragment"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1148899
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;

    invoke-static {v0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v0

    check-cast v0, LX/6wr;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->p:LX/6wr;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1148876
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1148877
    const v0, 0x7f030f04

    invoke-virtual {p0, v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->setContentView(I)V

    .line 1148878
    if-nez p1, :cond_0

    .line 1148879
    invoke-direct {p0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->a()V

    .line 1148880
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->q:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1148881
    iget-object p1, v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-object v0, p1

    .line 1148882
    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->a(Landroid/app/Activity;LX/6ws;)V

    .line 1148883
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1148869
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 1148870
    invoke-static {p0, p0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1148871
    invoke-virtual {p0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "payment_pin_params"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->q:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1148872
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->p:LX/6wr;

    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->q:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1148873
    iget-object p1, v1, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-object v1, p1

    .line 1148874
    iget-object v1, v1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-virtual {v0, p0, v1}, LX/6wr;->b(Landroid/app/Activity;LX/73i;)V

    .line 1148875
    return-void
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 1148864
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1148865
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->q:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1148866
    iget-object v1, v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-object v0, v1

    .line 1148867
    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->b(Landroid/app/Activity;LX/6ws;)V

    .line 1148868
    return-void
.end method
