.class public Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/6p6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/6po;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/content/Context;

.field public f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

.field public g:LX/6pY;

.field public h:I

.field public i:[Ljava/lang/String;

.field public j:LX/0h5;

.field public k:Landroid/widget/ProgressBar;

.field public l:Lcom/facebook/widget/CustomViewPager;

.field private m:Lcom/facebook/payments/auth/pin/EnterPinFragment;

.field private n:Lcom/facebook/payments/auth/pin/ResetPinFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1149049
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;LX/6pF;)Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;
    .locals 2

    .prologue
    .line 1149050
    invoke-static {p1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b(LX/6pF;)LX/6pE;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1149051
    iget-object p1, v1, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->c:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    move-object v1, p1

    .line 1149052
    iput-object v1, v0, LX/6pE;->c:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1149053
    move-object v0, v0

    .line 1149054
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1149055
    iget-object p1, v1, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->e:Landroid/content/Intent;

    move-object v1, p1

    .line 1149056
    iput-object v1, v0, LX/6pE;->e:Landroid/content/Intent;

    .line 1149057
    move-object v0, v0

    .line 1149058
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1149059
    iget-object p0, v1, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-object v1, p0

    .line 1149060
    iput-object v1, v0, LX/6pE;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1149061
    move-object v0, v0

    .line 1149062
    invoke-virtual {v0}, LX/6pE;->a()Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1149063
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1149064
    iget-object v1, v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->e:Landroid/content/Intent;

    move-object v0, v1

    .line 1149065
    if-eqz v0, :cond_1

    .line 1149066
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1149067
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1149068
    :cond_0
    :goto_0
    return-void

    .line 1149069
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 1149070
    if-eqz v0, :cond_0

    .line 1149071
    if-eqz p2, :cond_2

    .line 1149072
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1149073
    const-string v2, "user_entered_pin"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1149074
    invoke-virtual {v0, p1, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1149075
    :goto_1
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 1149076
    :cond_2
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setResult(I)V

    goto :goto_1
.end method

.method public static a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;LX/6oc;)V
    .locals 3
    .param p0    # Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1149077
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "payment_pin_sync_controller_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;

    .line 1149078
    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1149079
    new-instance v0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;

    invoke-direct {v0}, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;-><init>()V

    .line 1149080
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const-string v2, "payment_pin_sync_controller_fragment_tag"

    invoke-virtual {v1, v0, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1149081
    :cond_0
    if-eqz v0, :cond_1

    .line 1149082
    iput-object p1, v0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->g:LX/6oc;

    .line 1149083
    :cond_1
    return-void
.end method

.method public static a$redex0(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/model/PaymentPin;)V
    .locals 4

    .prologue
    .line 1149084
    invoke-virtual {p1}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    .line 1149085
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1149086
    iget-object v2, v1, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->a:LX/6pF;

    move-object v1, v2

    .line 1149087
    invoke-virtual {v1, v0}, LX/6pF;->getAction(Z)LX/6pF;

    move-result-object v0

    .line 1149088
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1149089
    iget-object v2, v1, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->a:LX/6pF;

    invoke-static {v2}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b(LX/6pF;)LX/6pE;

    move-result-object v2

    iget-object v3, v1, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1149090
    iput-object v3, v2, LX/6pE;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1149091
    move-object v2, v2

    .line 1149092
    iget-object v3, v1, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->c:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1149093
    iput-object v3, v2, LX/6pE;->c:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1149094
    move-object v2, v2

    .line 1149095
    iget-object v3, v1, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->d:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    .line 1149096
    iput-object v3, v2, LX/6pE;->d:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    .line 1149097
    move-object v2, v2

    .line 1149098
    iget-object v3, v1, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->e:Landroid/content/Intent;

    .line 1149099
    iput-object v3, v2, LX/6pE;->e:Landroid/content/Intent;

    .line 1149100
    move-object v2, v2

    .line 1149101
    iget-object v3, v1, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->f:Ljava/lang/String;

    .line 1149102
    iput-object v3, v2, LX/6pE;->f:Ljava/lang/String;

    .line 1149103
    move-object v2, v2

    .line 1149104
    iget v3, v1, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->g:F

    .line 1149105
    iput v3, v2, LX/6pE;->g:F

    .line 1149106
    move-object v2, v2

    .line 1149107
    move-object v1, v2

    .line 1149108
    iput-object p1, v1, LX/6pE;->c:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1149109
    move-object v1, v1

    .line 1149110
    iput-object v0, v1, LX/6pE;->a:LX/6pF;

    .line 1149111
    move-object v0, v1

    .line 1149112
    invoke-virtual {v0}, LX/6pE;->a()Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1149113
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1149114
    iget-object v1, v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->a:LX/6pF;

    move-object v0, v1

    .line 1149115
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->b:LX/6po;

    invoke-virtual {v1, v0}, LX/6po;->a(LX/6pF;)LX/6pY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->g:LX/6pY;

    .line 1149116
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->i:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1149117
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->g:LX/6pY;

    invoke-virtual {v0}, LX/6pY;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->i:[Ljava/lang/String;

    .line 1149118
    :cond_0
    invoke-static {p0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->o(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    .line 1149119
    invoke-static {p0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->p(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    .line 1149120
    const v0, 0x7f0d249e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomViewPager;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->l:Lcom/facebook/widget/CustomViewPager;

    .line 1149121
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->l:Lcom/facebook/widget/CustomViewPager;

    const/4 v1, 0x0

    .line 1149122
    iput-boolean v1, v0, Lcom/facebook/widget/CustomViewPager;->a:Z

    .line 1149123
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->l:Lcom/facebook/widget/CustomViewPager;

    new-instance v1, LX/6pA;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    invoke-direct {v1, p0, v2}, LX/6pA;-><init>(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;LX/0gc;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1149124
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->l:Lcom/facebook/widget/CustomViewPager;

    new-instance v1, LX/6pB;

    invoke-direct {v1, p0}, LX/6pB;-><init>(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1149125
    invoke-static {p0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->r(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    .line 1149126
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->g:LX/6pY;

    invoke-virtual {v0, p0}, LX/6pY;->a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)LX/6oc;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;LX/6oc;)V

    .line 1149127
    return-void
.end method

.method private b(LX/6pM;)I
    .locals 2

    .prologue
    .line 1149128
    invoke-virtual {p1}, LX/6pM;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->i:[Ljava/lang/String;

    array-length v1, v1

    rem-int/2addr v0, v1

    return v0
.end method

.method public static f(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;I)LX/6pM;
    .locals 1

    .prologue
    .line 1149129
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->g:LX/6pY;

    invoke-virtual {v0}, LX/6pY;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6pM;

    return-object v0
.end method

.method public static o(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V
    .locals 3

    .prologue
    .line 1149130
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->m:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->g:LX/6pY;

    if-nez v0, :cond_1

    .line 1149131
    :cond_0
    :goto_0
    return-void

    .line 1149132
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->m:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    .line 1149133
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1149134
    const-string v2, "savedTag"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    move v0, v1

    .line 1149135
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->g:LX/6pY;

    invoke-virtual {v1}, LX/6pY;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6pM;

    .line 1149136
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->g:LX/6pY;

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->m:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    invoke-virtual {v1, p0, v2, v0}, LX/6pY;->a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/EnterPinFragment;LX/6pM;)LX/6od;

    move-result-object v0

    .line 1149137
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1149138
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->m:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    .line 1149139
    iput-object v0, v1, Lcom/facebook/payments/auth/pin/EnterPinFragment;->g:LX/6od;

    .line 1149140
    iget-object v2, v1, Lcom/facebook/payments/auth/pin/EnterPinFragment;->e:Lcom/facebook/payments/auth/pin/DotsEditTextView;

    if-eqz v2, :cond_2

    .line 1149141
    iget-object v2, v1, Lcom/facebook/payments/auth/pin/EnterPinFragment;->e:Lcom/facebook/payments/auth/pin/DotsEditTextView;

    iget-object p0, v1, Lcom/facebook/payments/auth/pin/EnterPinFragment;->g:LX/6od;

    invoke-virtual {v2, p0}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->setListener(LX/6od;)V

    .line 1149142
    :cond_2
    goto :goto_0
.end method

.method public static p(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V
    .locals 2

    .prologue
    .line 1149143
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->n:Lcom/facebook/payments/auth/pin/ResetPinFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->g:LX/6pY;

    if-nez v0, :cond_1

    .line 1149144
    :cond_0
    :goto_0
    return-void

    .line 1149145
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->g:LX/6pY;

    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->n:Lcom/facebook/payments/auth/pin/ResetPinFragment;

    invoke-virtual {v0, p0, v1}, LX/6pY;->a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/ResetPinFragment;)LX/6oW;

    move-result-object v0

    .line 1149146
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1149147
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->n:Lcom/facebook/payments/auth/pin/ResetPinFragment;

    .line 1149148
    iput-object v0, v1, Lcom/facebook/payments/auth/pin/ResetPinFragment;->f:LX/6oW;

    .line 1149149
    goto :goto_0
.end method

.method public static r(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V
    .locals 2

    .prologue
    .line 1149150
    iget v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->h:I

    invoke-static {p0, v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;I)LX/6pM;

    move-result-object v0

    invoke-virtual {v0}, LX/6pM;->getActionBarTitleResId()I

    move-result v0

    .line 1149151
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->j:LX/0h5;

    invoke-interface {v1, v0}, LX/0h5;->setTitle(I)V

    .line 1149152
    return-void
.end method

.method public static t(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V
    .locals 3

    .prologue
    .line 1148934
    sget-object v0, LX/6pF;->RESET:LX/6pF;

    invoke-static {p0, v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;LX/6pF;)Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    move-result-object v0

    .line 1148935
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->a(Landroid/content/Context;Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;)Landroid/content/Intent;

    move-result-object v0

    .line 1148936
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->c:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1148937
    return-void
.end method

.method public static v(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V
    .locals 2

    .prologue
    .line 1149153
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->k:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1149154
    return-void
.end method


# virtual methods
.method public final a(LX/6pM;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1149155
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->i:[Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->b(LX/6pM;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final a(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1149156
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->d:LX/0kL;

    new-instance v1, LX/27k;

    invoke-direct {v1, p1}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1149157
    return-void
.end method

.method public final a(LX/6pM;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1149047
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->i:[Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->b(LX/6pM;)I

    move-result v1

    aput-object p2, v0, v1

    .line 1149048
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1149158
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1149159
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v2, 0x7f0e0326

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->e:Landroid/content/Context;

    .line 1149160
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->e:Landroid/content/Context;

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;

    invoke-static {v0}, LX/6p6;->a(LX/0QB;)LX/6p6;

    move-result-object v2

    check-cast v2, LX/6p6;

    invoke-static {v0}, LX/6po;->a(LX/0QB;)LX/6po;

    move-result-object v3

    check-cast v3, LX/6po;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p1

    check-cast p1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v0

    check-cast v0, LX/0kL;

    iput-object v2, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a:LX/6p6;

    iput-object v3, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->b:LX/6po;

    iput-object p1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->d:LX/0kL;

    .line 1149161
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;Lcom/facebook/payments/auth/pin/EnterPinFragment;Z)V
    .locals 3

    .prologue
    .line 1149009
    invoke-virtual {p2}, Lcom/facebook/payments/auth/pin/EnterPinFragment;->e()V

    .line 1149010
    invoke-virtual {p2}, Lcom/facebook/payments/auth/pin/EnterPinFragment;->c()V

    .line 1149011
    if-eqz p3, :cond_1

    .line 1149012
    const/4 v1, 0x0

    .line 1149013
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 1149014
    sget-object p3, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v0, p3, :cond_2

    move v0, v1

    .line 1149015
    :goto_0
    move v0, v0

    .line 1149016
    if-eqz v0, :cond_0

    .line 1149017
    invoke-static {p0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->t(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    .line 1149018
    :goto_1
    return-void

    .line 1149019
    :cond_0
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 1149020
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v0, v1, :cond_4

    .line 1149021
    invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/6up;->a(Landroid/content/Context;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 1149022
    :goto_2
    goto :goto_1

    .line 1149023
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->e:Landroid/content/Context;

    invoke-static {v0, p1}, LX/6up;->a(Landroid/content/Context;Lcom/facebook/fbservice/service/ServiceException;)V

    goto :goto_1

    .line 1149024
    :cond_2
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1149025
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 1149026
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    const/16 p3, 0x275b

    if-ne v0, p3, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 1149027
    :cond_4
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1149028
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 1149029
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v1

    const/16 v2, 0x2759

    if-eq v1, v2, :cond_5

    .line 1149030
    invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/payments/auth/pin/EnterPinFragment;->a(Landroid/content/Context;Ljava/lang/String;)LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_2

    .line 1149031
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->d()Ljava/lang/String;

    move-result-object v0

    .line 1149032
    const-string v1, "remain_attempts_count"

    const/4 v2, 0x6

    .line 1149033
    :try_start_0
    iget-object p0, p2, Lcom/facebook/payments/auth/pin/EnterPinFragment;->a:LX/0lC;

    invoke-virtual {p0, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    .line 1149034
    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    invoke-virtual {p0}, LX/0lF;->C()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1149035
    :goto_3
    move v1, v2

    .line 1149036
    packed-switch v1, :pswitch_data_0

    .line 1149037
    iget-object v1, p2, Lcom/facebook/payments/auth/pin/EnterPinFragment;->d:Lcom/facebook/resources/ui/FbTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1149038
    :goto_4
    goto :goto_2

    .line 1149039
    :pswitch_0
    invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081de5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1149040
    invoke-static {p2, v1}, Lcom/facebook/payments/auth/pin/EnterPinFragment;->b(Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V

    goto :goto_4

    .line 1149041
    :pswitch_1
    invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081de6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1149042
    invoke-static {p2, v1}, Lcom/facebook/payments/auth/pin/EnterPinFragment;->b(Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V

    goto :goto_4

    .line 1149043
    :pswitch_2
    invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081de7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1149044
    invoke-static {p2, v1}, Lcom/facebook/payments/auth/pin/EnterPinFragment;->b(Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V

    goto :goto_4

    .line 1149045
    :catch_0
    move-exception p0

    .line 1149046
    sget-object p3, Lcom/facebook/payments/auth/pin/EnterPinFragment;->b:Ljava/lang/Class;

    const-string p1, "Exception when parsing message"

    invoke-static {p3, p1, p0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1149006
    invoke-virtual {p1}, Lcom/facebook/payments/auth/pin/EnterPinFragment;->e()V

    .line 1149007
    const/4 v0, -0x1

    invoke-direct {p0, v0, p2}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(ILjava/lang/String;)V

    .line 1149008
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1148997
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1148998
    iget-object v1, v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->c:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    move-object v0, v1

    .line 1148999
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1149000
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1149001
    iget-object v1, v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->c:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    move-object v0, v1

    .line 1149002
    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v0

    .line 1149003
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1149004
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    .line 1149005
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "pin not set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1148994
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1148995
    iget-object p0, v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->d:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    move-object v0, p0

    .line 1148996
    return-object v0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1148992
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->l:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->l:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 1148993
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 1148990
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(ILjava/lang/String;)V

    .line 1148991
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1148984
    packed-switch p1, :pswitch_data_0

    .line 1148985
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1148986
    :goto_0
    return-void

    .line 1148987
    :pswitch_0
    if-ne p2, v1, :cond_0

    if-nez p3, :cond_1

    .line 1148988
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 1148989
    :cond_1
    const-string v0, "user_entered_pin"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(ILjava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 1148976
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 1148977
    instance-of v0, p1, Lcom/facebook/payments/auth/pin/EnterPinFragment;

    if-eqz v0, :cond_1

    .line 1148978
    check-cast p1, Lcom/facebook/payments/auth/pin/EnterPinFragment;

    iput-object p1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->m:Lcom/facebook/payments/auth/pin/EnterPinFragment;

    .line 1148979
    invoke-static {p0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->o(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    .line 1148980
    :cond_0
    :goto_0
    return-void

    .line 1148981
    :cond_1
    instance-of v0, p1, Lcom/facebook/payments/auth/pin/ResetPinFragment;

    if-eqz v0, :cond_0

    .line 1148982
    check-cast p1, Lcom/facebook/payments/auth/pin/ResetPinFragment;

    iput-object p1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->n:Lcom/facebook/payments/auth/pin/ResetPinFragment;

    .line 1148983
    invoke-static {p0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->p(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x5a36528c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1148975
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->e:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030f07

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x62ae27ea

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x73bef975

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1148972
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->g:LX/6pY;

    invoke-virtual {v1}, LX/6pY;->b()V

    .line 1148973
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1148974
    const/16 v1, 0x2b

    const v2, -0x2cfdfc09

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7ab721a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1148969
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1148970
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;LX/6oc;)V

    .line 1148971
    const/16 v1, 0x2b

    const v2, -0x74d96f0b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x23517872

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1148965
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1148966
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->g:LX/6pY;

    if-eqz v1, :cond_0

    .line 1148967
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->g:LX/6pY;

    invoke-virtual {v1, p0}, LX/6pY;->a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)LX/6oc;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;LX/6oc;)V

    .line 1148968
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x167f9e51

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1148960
    const-string v0, "payment_pin_params"

    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1148961
    const-string v0, "page_index"

    iget v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->h:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1148962
    const-string v0, "pin_storage"

    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->i:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1148963
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1148964
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1148938
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1148939
    if-eqz p2, :cond_0

    .line 1148940
    const-string v0, "payment_pin_params"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1148941
    const-string v0, "page_index"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->h:I

    .line 1148942
    const-string v0, "pin_storage"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->i:[Ljava/lang/String;

    .line 1148943
    :goto_0
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->k:Landroid/widget/ProgressBar;

    .line 1148944
    const v0, 0x7f0d00bb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 1148945
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1148946
    iget-object v2, v1, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-object v2, v2

    .line 1148947
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1148948
    check-cast v1, Landroid/view/ViewGroup;

    new-instance p1, LX/6p7;

    invoke-direct {p1, p0}, LX/6p7;-><init>(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    iget-object p2, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    iget-object v2, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-virtual {v2}, LX/6ws;->getTitleBarNavIconStyle()LX/73h;

    move-result-object v2

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 1148949
    iget-object v1, v0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    move-object v0, v1

    .line 1148950
    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->j:LX/0h5;

    .line 1148951
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    .line 1148952
    iget-object v1, v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->c:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    move-object v0, v1

    .line 1148953
    if-eqz v0, :cond_1

    .line 1148954
    invoke-static {p0, v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a$redex0(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;Lcom/facebook/payments/auth/pin/model/PaymentPin;)V

    .line 1148955
    :goto_1
    return-void

    .line 1148956
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1148957
    const-string v1, "payment_pin_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->f:Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    goto :goto_0

    .line 1148958
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;->a:LX/6p6;

    new-instance v1, LX/6p9;

    invoke-direct {v1, p0}, LX/6p9;-><init>(Lcom/facebook/payments/auth/pin/newpin/PaymentPinFragment;)V

    invoke-virtual {v0, v1}, LX/6p6;->a(LX/6nn;)V

    .line 1148959
    goto :goto_1
.end method
