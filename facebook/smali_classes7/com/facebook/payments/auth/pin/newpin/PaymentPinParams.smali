.class public Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6pF;

.field public final b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

.field public final c:Lcom/facebook/payments/auth/pin/model/PaymentPin;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Landroid/content/Intent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1149185
    new-instance v0, LX/6pD;

    invoke-direct {v0}, LX/6pD;-><init>()V

    sput-object v0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6pE;)V
    .locals 2

    .prologue
    .line 1149206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1149207
    iget-object v0, p1, LX/6pE;->a:LX/6pF;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6pF;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->a:LX/6pF;

    .line 1149208
    iget-object v0, p1, LX/6pE;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1149209
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->a:LX/6pF;

    sget-object v1, LX/6pF;->CREATE:LX/6pF;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, LX/6pE;->c:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    sget-object v1, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    :goto_0
    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->c:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1149210
    iget-object v0, p1, LX/6pE;->d:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->d:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    .line 1149211
    iget-object v0, p1, LX/6pE;->e:Landroid/content/Intent;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->e:Landroid/content/Intent;

    .line 1149212
    iget-object v0, p1, LX/6pE;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->f:Ljava/lang/String;

    .line 1149213
    iget v0, p1, LX/6pE;->g:F

    iput v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->g:F

    .line 1149214
    return-void

    .line 1149215
    :cond_0
    iget-object v0, p1, LX/6pE;->c:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1149197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1149198
    const-class v0, LX/6pF;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6pF;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->a:LX/6pF;

    .line 1149199
    const-class v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1149200
    const-class v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->c:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1149201
    const-class v0, Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->d:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    .line 1149202
    const-class v0, Landroid/content/Intent;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->e:Landroid/content/Intent;

    .line 1149203
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->f:Ljava/lang/String;

    .line 1149204
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->g:F

    .line 1149205
    return-void
.end method

.method public static a(LX/6pF;)Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;
    .locals 1

    .prologue
    .line 1149196
    invoke-static {p0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b(LX/6pF;)LX/6pE;

    move-result-object v0

    invoke-virtual {v0}, LX/6pE;->a()Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/6pF;)LX/6pE;
    .locals 1

    .prologue
    .line 1149216
    new-instance v0, LX/6pE;

    invoke-direct {v0, p0}, LX/6pE;-><init>(LX/6pF;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1149195
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1149194
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "mPinAction"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->a:LX/6pF;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "mPaymentsDecoratorParams"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "mPaymentPin"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->c:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "mPaymentPinProtectionsParams"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->d:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "mOnActivityFinishLaunchIntent"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->e:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "mHeaderText"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "mHeaderTextSizePx"

    iget v2, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->g:F

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;F)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1149186
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->a:LX/6pF;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1149187
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1149188
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->c:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1149189
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->d:Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1149190
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->e:Landroid/content/Intent;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1149191
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1149192
    iget v0, p0, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->g:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1149193
    return-void
.end method
