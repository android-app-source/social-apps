.class public Lcom/facebook/payments/auth/pin/DotsEditTextView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public a:LX/6oT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/73q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/resources/ui/FbEditText;

.field private e:Landroid/widget/ImageView;

.field private f:LX/6od;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1148177
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1148178
    invoke-direct {p0}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->f()V

    .line 1148179
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1148174
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1148175
    invoke-direct {p0}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->f()V

    .line 1148176
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1148171
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1148172
    invoke-direct {p0}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->f()V

    .line 1148173
    return-void
.end method

.method private static a(Lcom/facebook/payments/auth/pin/DotsEditTextView;LX/6oT;LX/73q;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 1148170
    iput-object p1, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->a:LX/6oT;

    iput-object p2, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->b:LX/73q;

    iput-object p3, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->c:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;

    const-class v0, LX/6oT;

    invoke-interface {v2, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/6oT;

    invoke-static {v2}, LX/73q;->b(LX/0QB;)LX/73q;

    move-result-object v1

    check-cast v1, LX/73q;

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->a(Lcom/facebook/payments/auth/pin/DotsEditTextView;LX/6oT;LX/73q;Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method

.method private f()V
    .locals 5

    .prologue
    .line 1148159
    const-class v0, Lcom/facebook/payments/auth/pin/DotsEditTextView;

    invoke-static {v0, p0}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1148160
    const v0, 0x7f03043b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1148161
    const v0, 0x7f0d0cd4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d:Lcom/facebook/resources/ui/FbEditText;

    .line 1148162
    const v0, 0x7f0d0cd5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->e:Landroid/widget/ImageView;

    .line 1148163
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->a:LX/6oT;

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->e:Landroid/widget/ImageView;

    .line 1148164
    new-instance v4, LX/6oS;

    const-class v3, Landroid/content/Context;

    invoke-interface {v1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, v3, v2}, LX/6oS;-><init>(Landroid/content/Context;Landroid/widget/ImageView;)V

    .line 1148165
    move-object v1, v4

    .line 1148166
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1148167
    invoke-virtual {p0}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->e()V

    .line 1148168
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->requestFocus()Z

    .line 1148169
    return-void
.end method

.method public static g(Lcom/facebook/payments/auth/pin/DotsEditTextView;)V
    .locals 2

    .prologue
    .line 1148154
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d:Lcom/facebook/resources/ui/FbEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1148155
    invoke-virtual {p0}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->b()V

    .line 1148156
    invoke-virtual {p0}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021409

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 1148157
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1148158
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1148150
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040063

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1148151
    new-instance v1, LX/6oN;

    invoke-direct {v1, p0}, LX/6oN;-><init>(Lcom/facebook/payments/auth/pin/DotsEditTextView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1148152
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1148153
    return-void
.end method

.method public final b()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedClass"
        }
    .end annotation

    .prologue
    .line 1148128
    invoke-virtual {p0}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1148129
    if-eqz v0, :cond_0

    .line 1148130
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->b:LX/73q;

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1, v0, v2}, LX/73q;->a(Landroid/app/Activity;Landroid/view/View;)V

    .line 1148131
    :goto_0
    return-void

    .line 1148132
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->e:Landroid/widget/ImageView;

    invoke-static {v0, v1}, LX/2Na;->b(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1148146
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1148147
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->length()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->f:LX/6od;

    if-eqz v1, :cond_0

    .line 1148148
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->f:LX/6od;

    invoke-interface {v1, v0}, LX/6od;->a(Ljava/lang/String;)V

    .line 1148149
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1148142
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setEnabled(Z)V

    .line 1148143
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setFocusable(Z)V

    .line 1148144
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setClickable(Z)V

    .line 1148145
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1148138
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setEnabled(Z)V

    .line 1148139
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setFocusableInTouchMode(Z)V

    .line 1148140
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setClickable(Z)V

    .line 1148141
    return-void
.end method

.method public setListener(LX/6od;)V
    .locals 2

    .prologue
    .line 1148133
    iput-object p1, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->f:LX/6od;

    .line 1148134
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/6oP;

    invoke-direct {v1, p0}, LX/6oP;-><init>(Lcom/facebook/payments/auth/pin/DotsEditTextView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1148135
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/6oQ;

    invoke-direct {v1, p0}, LX/6oQ;-><init>(Lcom/facebook/payments/auth/pin/DotsEditTextView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1148136
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/DotsEditTextView;->e:Landroid/widget/ImageView;

    new-instance v1, LX/6oR;

    invoke-direct {v1, p0}, LX/6oR;-><init>(Lcom/facebook/payments/auth/pin/DotsEditTextView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1148137
    return-void
.end method
