.class public Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/0Xj;

.field public c:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

.field public d:Ljava/util/concurrent/Executor;

.field public e:LX/0Yb;

.field public f:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/6oc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1148273
    const-class v0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;

    sput-object v0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1148280
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1148281
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v1

    check-cast v1, LX/0Xj;

    invoke-static {p0}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/Executor;

    iput-object v1, p1, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->b:LX/0Xj;

    iput-object v2, p1, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->c:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    iput-object p0, p1, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->d:Ljava/util/concurrent/Executor;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1148282
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1148283
    const-class v0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;

    invoke-static {v0, p0}, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1148284
    new-instance v0, LX/6oa;

    invoke-direct {v0, p0}, LX/6oa;-><init>(Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;)V

    .line 1148285
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->b:LX/0Xj;

    invoke-virtual {v1}, LX/0Xk;->a()LX/0YX;

    move-result-object v1

    const-string p1, "com.facebook.payments.auth.ACTION_PIN_UPDATED"

    invoke-interface {v1, p1, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->e:LX/0Yb;

    .line 1148286
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1906d671

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1148277
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1148278
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->e:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 1148279
    const/16 v1, 0x2b

    const v2, -0x5e4b3bc9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4e960cd0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1148274
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1148275
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->e:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 1148276
    const/16 v1, 0x2b

    const v2, -0x3fb00b77

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
