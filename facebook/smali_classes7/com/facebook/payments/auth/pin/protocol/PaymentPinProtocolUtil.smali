.class public Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;


# instance fields
.field private final a:LX/0aG;


# direct methods
.method public constructor <init>(LX/0aG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1149897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1149898
    iput-object p1, p0, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a:LX/0aG;

    .line 1149899
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;
    .locals 4

    .prologue
    .line 1149900
    sget-object v0, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->b:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    if-nez v0, :cond_1

    .line 1149901
    const-class v1, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    monitor-enter v1

    .line 1149902
    :try_start_0
    sget-object v0, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->b:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1149903
    if-eqz v2, :cond_0

    .line 1149904
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1149905
    new-instance p0, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-direct {p0, v3}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;-><init>(LX/0aG;)V

    .line 1149906
    move-object v0, p0

    .line 1149907
    sput-object v0, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->b:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1149908
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1149909
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1149910
    :cond_1
    sget-object v0, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->b:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    return-object v0

    .line 1149911
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1149912
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1149913
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a:LX/0aG;

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x1c576c90

    move-object v1, p2

    move-object v2, p1

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1149914
    new-instance v0, LX/6q5;

    invoke-direct {v0, p0}, LX/6q5;-><init>(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1149915
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1149916
    const-string v1, "fetch_payment_pin"

    invoke-static {p0, v0, v1}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1149917
    invoke-static {p0, v0}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLjava/lang/String;LX/03R;Ljava/util/Map;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "LX/03R;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1149918
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1149919
    sget-object v8, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->a:Ljava/lang/String;

    new-instance v1, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;

    const/4 v5, 0x0

    move-wide v2, p1

    move-object v4, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;-><init>(JLjava/lang/String;Ljava/lang/String;LX/03R;Ljava/util/Map;)V

    invoke-virtual {v0, v8, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1149920
    const-string v1, "update_payment_pin_status"

    invoke-static {p0, v0, v1}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1149921
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1149922
    const-string v1, "createFingerprintNonceParams"

    new-instance v2, Lcom/facebook/payments/auth/pin/params/CreateFingerprintNonceParams;

    invoke-direct {v2, p1}, Lcom/facebook/payments/auth/pin/params/CreateFingerprintNonceParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1149923
    const-string v1, "create_fingerprint_nonce"

    invoke-static {p0, v0, v1}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1149924
    new-instance v1, LX/6q4;

    invoke-direct {v1, p0}, LX/6q4;-><init>(Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;)V

    .line 1149925
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 1149926
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
