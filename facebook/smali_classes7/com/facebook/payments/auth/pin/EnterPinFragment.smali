.class public Lcom/facebook/payments/auth/pin/EnterPinFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0lC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/content/Context;

.field public d:Lcom/facebook/resources/ui/FbTextView;

.field public e:Lcom/facebook/payments/auth/pin/DotsEditTextView;

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field public g:LX/6od;

.field private h:Ljava/lang/String;

.field private i:Lcom/facebook/resources/ui/FbTextView;

.field private j:Landroid/widget/ProgressBar;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1148208
    const-class v0, Lcom/facebook/payments/auth/pin/EnterPinFragment;

    sput-object v0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1148209
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)LX/2EJ;
    .locals 3

    .prologue
    .line 1148210
    new-instance v0, LX/31Y;

    invoke-direct {v0, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    new-instance v2, LX/6oV;

    invoke-direct {v2}, LX/6oV;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 1148211
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2EJ;->requestWindowFeature(I)Z

    .line 1148212
    return-object v0
.end method

.method public static b(Lcom/facebook/payments/auth/pin/EnterPinFragment;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1148213
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->d:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1148214
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1148215
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1148216
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1148217
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v2, 0x7f0e0326

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->c:Landroid/content/Context;

    .line 1148218
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->c:Landroid/content/Context;

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v0

    check-cast v0, LX/0lC;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->a:LX/0lC;

    .line 1148219
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1148220
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->e:Lcom/facebook/payments/auth/pin/DotsEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->a()V

    .line 1148221
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1148222
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->e:Lcom/facebook/payments/auth/pin/DotsEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->d()V

    .line 1148223
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->j:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1148224
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1148225
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->j:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1148226
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->e:Lcom/facebook/payments/auth/pin/DotsEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->e()V

    .line 1148227
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x211129d1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1148228
    iget-object v1, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->c:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030f05

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x2f594284

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1148229
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1148230
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 1148231
    const v0, 0x7f0d2498

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1148232
    const-string v0, "savedHeaderText"

    const v2, 0x7f081dd7

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->h:Ljava/lang/String;

    .line 1148233
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->f:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1148234
    const-string v0, "savedHeaderTextSizePx"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    .line 1148235
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-lez v2, :cond_0

    .line 1148236
    iget-object v2, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v3, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    .line 1148237
    :cond_0
    const v0, 0x7f0d2497

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1148238
    const v0, 0x7f0d2499

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->i:Lcom/facebook/resources/ui/FbTextView;

    .line 1148239
    const-string v0, "forgetLink"

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1148240
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->i:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1148241
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->i:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/6oU;

    invoke-direct {v1, p0}, LX/6oU;-><init>(Lcom/facebook/payments/auth/pin/EnterPinFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1148242
    :cond_1
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->j:Landroid/widget/ProgressBar;

    .line 1148243
    const v0, 0x7f0d249a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/DotsEditTextView;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->e:Lcom/facebook/payments/auth/pin/DotsEditTextView;

    .line 1148244
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->g:LX/6od;

    if-eqz v0, :cond_2

    .line 1148245
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->e:Lcom/facebook/payments/auth/pin/DotsEditTextView;

    iget-object v1, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->g:LX/6od;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->setListener(LX/6od;)V

    .line 1148246
    :cond_2
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/EnterPinFragment;->e:Lcom/facebook/payments/auth/pin/DotsEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/DotsEditTextView;->b()V

    .line 1148247
    return-void
.end method
