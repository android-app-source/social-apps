.class public Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;


# instance fields
.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Z

.field private final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1148612
    new-instance v0, LX/6or;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/6or;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/6or;->a()Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;

    move-result-object v0

    sput-object v0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;->a:Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;

    .line 1148613
    new-instance v0, LX/6oq;

    invoke-direct {v0}, LX/6oq;-><init>()V

    sput-object v0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6or;)V
    .locals 1

    .prologue
    .line 1148614
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148615
    iget-object v0, p1, LX/6or;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;->b:Ljava/lang/String;

    .line 1148616
    iget-boolean v0, p1, LX/6or;->b:Z

    iput-boolean v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;->c:Z

    .line 1148617
    iget-object v0, p1, LX/6or;->c:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;->d:LX/0Px;

    .line 1148618
    iget-object v0, p1, LX/6or;->d:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;->e:LX/0Px;

    .line 1148619
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1148620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148621
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;->b:Ljava/lang/String;

    .line 1148622
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;->c:Z

    .line 1148623
    invoke-static {p1}, LX/46R;->j(Landroid/os/Parcel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;->d:LX/0Px;

    .line 1148624
    invoke-static {p1}, LX/46R;->j(Landroid/os/Parcel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;->e:LX/0Px;

    .line 1148625
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1148626
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1148627
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1148628
    iget-boolean v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1148629
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1148630
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPinStatus;->e:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1148631
    return-void
.end method
