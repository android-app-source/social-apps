.class public Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:J

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1148513
    const-string v0, "deletePaymentPinParams"

    sput-object v0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->a:Ljava/lang/String;

    .line 1148514
    new-instance v0, LX/6om;

    invoke-direct {v0}, LX/6om;-><init>()V

    sput-object v0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 1148515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148516
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1148517
    iput-object p3, p0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->b:Ljava/lang/String;

    .line 1148518
    iput-wide p1, p0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->c:J

    .line 1148519
    iput-boolean p4, p0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->d:Z

    .line 1148520
    return-void

    .line 1148521
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1148508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148509
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->b:Ljava/lang/String;

    .line 1148510
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->c:J

    .line 1148511
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->d:Z

    .line 1148512
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1148507
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1148502
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "pinOrPassword"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "pinId"

    iget-wide v2, p0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "isPin"

    iget-boolean v2, p0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->d:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1148503
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1148504
    iget-wide v0, p0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1148505
    iget-boolean v0, p0, Lcom/facebook/payments/auth/pin/model/DeletePaymentPinParams;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1148506
    return-void
.end method
