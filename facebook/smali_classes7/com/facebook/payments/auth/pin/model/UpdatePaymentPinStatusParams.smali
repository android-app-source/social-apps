.class public Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:LX/03R;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1148667
    const-string v0, "updatePaymentPinStatusParams"

    sput-object v0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->a:Ljava/lang/String;

    .line 1148668
    new-instance v0, LX/6ou;

    invoke-direct {v0}, LX/6ou;-><init>()V

    sput-object v0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;LX/03R;Ljava/util/Map;)V
    .locals 5
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/03R;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/03R;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1148669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148670
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1148671
    iput-wide p1, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->b:J

    .line 1148672
    iput-object p3, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->c:Ljava/lang/String;

    .line 1148673
    iput-object p4, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->d:Ljava/lang/String;

    .line 1148674
    iput-object p5, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->e:LX/03R;

    .line 1148675
    if-nez p6, :cond_2

    .line 1148676
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->f:Ljava/util/Map;

    .line 1148677
    :cond_0
    return-void

    .line 1148678
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1148679
    :cond_2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->f:Ljava/util/Map;

    .line 1148680
    invoke-interface {p6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1148681
    iget-object v3, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1148682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148683
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->b:J

    .line 1148684
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->c:Ljava/lang/String;

    .line 1148685
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->d:Ljava/lang/String;

    .line 1148686
    invoke-static {p1}, LX/46R;->f(Landroid/os/Parcel;)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->e:LX/03R;

    .line 1148687
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->f:Ljava/util/Map;

    .line 1148688
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->f:Ljava/util/Map;

    invoke-static {p1, v0}, LX/46R;->b(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 1148689
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1148690
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1148691
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->f:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->f:Ljava/util/Map;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1148692
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "pinId"

    iget-wide v2, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->b:J

    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    move-result-object v0

    const-string v1, "paymentsProtected"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->e:LX/03R;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "threadProfilesProtected"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->f:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1148693
    iget-wide v0, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1148694
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1148695
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1148696
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->e:LX/03R;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/03R;)V

    .line 1148697
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/UpdatePaymentPinStatusParams;->f:Ljava/util/Map;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 1148698
    return-void
.end method
