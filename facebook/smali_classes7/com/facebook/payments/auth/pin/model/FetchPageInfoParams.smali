.class public Lcom/facebook/payments/auth/pin/model/FetchPageInfoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/auth/pin/model/FetchPageInfoParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1148525
    const-string v0, "fetchPageInfoParams"

    sput-object v0, Lcom/facebook/payments/auth/pin/model/FetchPageInfoParams;->a:Ljava/lang/String;

    .line 1148526
    new-instance v0, LX/6on;

    invoke-direct {v0}, LX/6on;-><init>()V

    sput-object v0, Lcom/facebook/payments/auth/pin/model/FetchPageInfoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1148527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148528
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/FetchPageInfoParams;->b:Ljava/lang/String;

    .line 1148529
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1148530
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1148531
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "pageId"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/model/FetchPageInfoParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1148532
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/FetchPageInfoParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1148533
    return-void
.end method
