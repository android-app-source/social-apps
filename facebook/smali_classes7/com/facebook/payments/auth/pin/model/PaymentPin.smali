.class public Lcom/facebook/payments/auth/pin/model/PaymentPin;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/auth/pin/model/PaymentPinDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/payments/auth/pin/model/PaymentPin;


# instance fields
.field private final mPinId:Ljava/lang/Long;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1148578
    const-class v0, Lcom/facebook/payments/auth/pin/model/PaymentPinDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1148576
    new-instance v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-direct {v0}, Lcom/facebook/payments/auth/pin/model/PaymentPin;-><init>()V

    sput-object v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1148577
    new-instance v0, LX/6op;

    invoke-direct {v0}, LX/6op;-><init>()V

    sput-object v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1148573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148574
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPin;->mPinId:Ljava/lang/Long;

    .line 1148575
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 1148570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148571
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPin;->mPinId:Ljava/lang/Long;

    .line 1148572
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1148550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148551
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1148552
    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPin;->mPinId:Ljava/lang/Long;

    .line 1148553
    return-void

    .line 1148554
    :cond_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/payments/model/PaymentsPin;)Lcom/facebook/payments/auth/pin/model/PaymentPin;
    .locals 4
    .param p0    # Lcom/facebook/payments/model/PaymentsPin;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1148567
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/facebook/payments/model/PaymentsPin;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1148568
    :cond_0
    sget-object v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1148569
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-interface {p0}, Lcom/facebook/payments/model/PaymentsPin;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/facebook/payments/auth/pin/model/PaymentPin;-><init>(J)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1148579
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPin;->mPinId:Ljava/lang/Long;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/payments/model/PaymentsPin;
    .locals 2

    .prologue
    .line 1148566
    invoke-virtual {p0}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/facebook/payments/model/SimplePaymentsPin;

    invoke-virtual {p0}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/payments/model/SimplePaymentsPin;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/payments/model/SimplePaymentsPin;->a:Lcom/facebook/payments/model/SimplePaymentsPin;

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1148565
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1148558
    if-ne p0, p1, :cond_1

    .line 1148559
    :cond_0
    :goto_0
    return v0

    .line 1148560
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1148561
    :cond_3
    check-cast p1, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 1148562
    invoke-virtual {p0}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v2

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 1148563
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p1}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1148564
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1148557
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/model/PaymentPin;->mPinId:Ljava/lang/Long;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1148555
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/PaymentPin;->mPinId:Ljava/lang/Long;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1148556
    return-void
.end method
