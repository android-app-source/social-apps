.class public Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:J

.field private final d:LX/03R;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1148661
    const-string v0, "setPaymentPinParams"

    sput-object v0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->a:Ljava/lang/String;

    .line 1148662
    new-instance v0, LX/6os;

    invoke-direct {v0}, LX/6os;-><init>()V

    sput-object v0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6ot;)V
    .locals 4

    .prologue
    .line 1148642
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148643
    iget-object v0, p1, LX/6ot;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1148644
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1148645
    iget-object v0, p1, LX/6ot;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1148646
    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->b:Ljava/lang/String;

    .line 1148647
    iget-wide v2, p1, LX/6ot;->b:J

    move-wide v0, v2

    .line 1148648
    iput-wide v0, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->c:J

    .line 1148649
    iget-object v0, p1, LX/6ot;->c:LX/03R;

    move-object v0, v0

    .line 1148650
    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->d:LX/03R;

    .line 1148651
    iget-object v0, p1, LX/6ot;->d:Ljava/util/Map;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    move-object v0, v0

    .line 1148652
    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->e:Ljava/util/Map;

    .line 1148653
    return-void

    .line 1148654
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p1, LX/6ot;->d:Ljava/util/Map;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    goto :goto_1
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1148655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148656
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->b:Ljava/lang/String;

    .line 1148657
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->c:J

    .line 1148658
    invoke-static {p1}, LX/46R;->f(Landroid/os/Parcel;)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->d:LX/03R;

    .line 1148659
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->e:Ljava/util/Map;

    .line 1148660
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1148635
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1148641
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "pin"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "senderId"

    iget-wide v2, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    move-result-object v0

    const-string v1, "paymentProtected"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->d:LX/03R;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "threadProfileProtected"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->e:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1148636
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1148637
    iget-wide v0, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1148638
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->d:LX/03R;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/03R;)V

    .line 1148639
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/SetPaymentPinParams;->e:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1148640
    return-void
.end method
