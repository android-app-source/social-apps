.class public Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1148482
    const-string v0, "checkPaymentPinParams"

    sput-object v0, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;->a:Ljava/lang/String;

    .line 1148483
    new-instance v0, LX/6ol;

    invoke-direct {v0}, LX/6ol;-><init>()V

    sput-object v0, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 1148493
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148494
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1148495
    iput-object p3, p0, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;->b:Ljava/lang/String;

    .line 1148496
    iput-wide p1, p0, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;->c:J

    .line 1148497
    return-void

    .line 1148498
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1148489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1148490
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;->b:Ljava/lang/String;

    .line 1148491
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;->c:J

    .line 1148492
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1148488
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1148487
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "pin"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "pinId"

    iget-wide v2, p0, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1148484
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1148485
    iget-wide v0, p0, Lcom/facebook/payments/auth/pin/model/CheckPaymentPinParams;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1148486
    return-void
.end method
