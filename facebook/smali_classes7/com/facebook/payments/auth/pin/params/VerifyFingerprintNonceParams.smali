.class public Lcom/facebook/payments/auth/pin/params/VerifyFingerprintNonceParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/auth/pin/params/VerifyFingerprintNonceParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1149803
    new-instance v0, LX/6q1;

    invoke-direct {v0}, LX/6q1;-><init>()V

    sput-object v0, Lcom/facebook/payments/auth/pin/params/VerifyFingerprintNonceParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1149804
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1149805
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/params/VerifyFingerprintNonceParams;->a:Ljava/lang/String;

    .line 1149806
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1149807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1149808
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1149809
    iput-object p1, p0, Lcom/facebook/payments/auth/pin/params/VerifyFingerprintNonceParams;->a:Ljava/lang/String;

    .line 1149810
    return-void

    .line 1149811
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1149812
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1149813
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/params/VerifyFingerprintNonceParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1149814
    return-void
.end method
