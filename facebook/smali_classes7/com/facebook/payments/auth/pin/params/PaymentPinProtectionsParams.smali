.class public Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:LX/03R;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1149786
    const-string v0, "paymentPinProtectionsParams"

    sput-object v0, Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;->a:Ljava/lang/String;

    .line 1149787
    new-instance v0, LX/6pz;

    invoke-direct {v0}, LX/6pz;-><init>()V

    sput-object v0, Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1149788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1149789
    invoke-static {p1}, LX/46R;->f(Landroid/os/Parcel;)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;->b:LX/03R;

    .line 1149790
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;->c:Ljava/util/Map;

    .line 1149791
    return-void
.end method

.method public static newBuilder()LX/6q0;
    .locals 1

    .prologue
    .line 1149792
    new-instance v0, LX/6q0;

    invoke-direct {v0}, LX/6q0;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final b()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1149793
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;->c:Ljava/util/Map;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1149794
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1149795
    invoke-static {p0}, LX/0Qh;->toStringHelper(Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "paymentProtected"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;->b:LX/03R;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "threadProfileProtected"

    iget-object v2, p0, Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;->c:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1149796
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;->b:LX/03R;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/03R;)V

    .line 1149797
    iget-object v0, p0, Lcom/facebook/payments/auth/pin/params/PaymentPinProtectionsParams;->c:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1149798
    return-void
.end method
