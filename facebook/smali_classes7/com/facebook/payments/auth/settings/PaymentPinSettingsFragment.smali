.class public Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;
.super Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;
.source ""


# instance fields
.field public a:LX/6pC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/content/Context;

.field public d:Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;

.field public e:Landroid/preference/PreferenceScreen;

.field public final f:Landroid/preference/Preference$OnPreferenceClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1150562
    invoke-direct {p0}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;-><init>()V

    .line 1150563
    new-instance v0, LX/6qM;

    invoke-direct {v0, p0}, LX/6qM;-><init>(Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->f:Landroid/preference/Preference$OnPreferenceClickListener;

    return-void
.end method

.method public static a(Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;LX/6pF;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1150564
    invoke-static {p1}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->b(LX/6pF;)LX/6pE;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->d:Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;

    .line 1150565
    iget-object p1, v1, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;->b:Landroid/content/Intent;

    move-object v1, p1

    .line 1150566
    iput-object v1, v0, LX/6pE;->e:Landroid/content/Intent;

    .line 1150567
    move-object v0, v0

    .line 1150568
    invoke-virtual {v0}, LX/6pE;->a()Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    move-result-object v0

    .line 1150569
    iget-object v1, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->a(Landroid/content/Context;Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/6oc;)V
    .locals 3
    .param p1    # LX/6oc;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1150570
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "payment_pin_listening_controller_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;

    .line 1150571
    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1150572
    new-instance v0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;

    invoke-direct {v0}, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;-><init>()V

    .line 1150573
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const-string v2, "payment_pin_listening_controller_fragment_tag"

    invoke-virtual {v1, v0, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1150574
    :cond_0
    if-eqz v0, :cond_1

    .line 1150575
    iput-object p1, v0, Lcom/facebook/payments/auth/pin/PaymentPinSyncControllerFragment;->g:LX/6oc;

    .line 1150576
    :cond_1
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;

    invoke-static {p0}, LX/6pC;->a(LX/0QB;)LX/6pC;

    move-result-object v0

    check-cast v0, LX/6pC;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p1, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->a:LX/6pC;

    iput-object p0, p1, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->b:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;I)V
    .locals 3

    .prologue
    .line 1150510
    iget-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->d:Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;

    .line 1150511
    iget-object v1, v0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;->b:Landroid/content/Intent;

    move-object v0, v1

    .line 1150512
    if-eqz v0, :cond_1

    .line 1150513
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1150514
    iget-object v1, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->c:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1150515
    :cond_0
    :goto_0
    return-void

    .line 1150516
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 1150517
    if-eqz v0, :cond_0

    .line 1150518
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setResult(I)V

    .line 1150519
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1150541
    invoke-super {p0, p1}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->a(Landroid/os/Bundle;)V

    .line 1150542
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v2, 0x7f0e0326

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->c:Landroid/content/Context;

    .line 1150543
    const-class v0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;

    iget-object v1, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->c:Landroid/content/Context;

    invoke-static {v0, p0, v1}, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1150544
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1150545
    const-string v1, "payment_pin_settings_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;

    iput-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->d:Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;

    .line 1150546
    iget-object v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->a:Landroid/preference/PreferenceManager;

    move-object v0, v0

    .line 1150547
    iget-object v1, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->e:Landroid/preference/PreferenceScreen;

    .line 1150548
    iget-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->e:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->a(Landroid/preference/PreferenceScreen;)V

    .line 1150549
    new-instance v0, Landroid/preference/Preference;

    iget-object v1, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1150550
    const v1, 0x7f030f0d

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 1150551
    const v1, 0x7f081e00

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 1150552
    sget-object v1, LX/6pF;->CHANGE:LX/6pF;

    invoke-static {p0, v1}, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->a(Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;LX/6pF;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 1150553
    iget-object v1, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->f:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1150554
    iget-object v1, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->e:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1150555
    new-instance v0, Landroid/preference/Preference;

    iget-object v1, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1150556
    const v1, 0x7f030f0d

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 1150557
    const v1, 0x7f081e01

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    .line 1150558
    sget-object v1, LX/6pF;->DELETE:LX/6pF;

    invoke-static {p0, v1}, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->a(Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;LX/6pF;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 1150559
    iget-object v1, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->f:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1150560
    iget-object v1, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->e:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1150561
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1150536
    packed-switch p1, :pswitch_data_0

    .line 1150537
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1150538
    :cond_0
    :goto_0
    return-void

    .line 1150539
    :pswitch_0
    if-ne p2, v0, :cond_0

    .line 1150540
    invoke-static {p0, v0}, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->a$redex0(Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7f90ef4f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1150535
    iget-object v1, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->c:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030f08

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x219a6aca

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x70dcba9f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1150532
    invoke-super {p0}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->onPause()V

    .line 1150533
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->a(LX/6oc;)V

    .line 1150534
    const/16 v1, 0x2b

    const v2, -0x7770f8a4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7da6cb96

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1150529
    invoke-super {p0}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->onResume()V

    .line 1150530
    new-instance v1, LX/6qO;

    invoke-direct {v1, p0}, LX/6qO;-><init>(Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;)V

    invoke-direct {p0, v1}, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->a(LX/6oc;)V

    .line 1150531
    const/16 v1, 0x2b

    const v2, -0x40cac2a7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1150520
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1150521
    const v0, 0x7f0d00bb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 1150522
    iget-object v1, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;->d:Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;

    .line 1150523
    iget-object v2, v1, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-object v2, v2

    .line 1150524
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1150525
    check-cast v1, Landroid/view/ViewGroup;

    new-instance p1, LX/6qN;

    invoke-direct {p1, p0}, LX/6qN;-><init>(Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;)V

    iget-object p2, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    iget-object v2, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-virtual {v2}, LX/6ws;->getTitleBarNavIconStyle()LX/73h;

    move-result-object v2

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 1150526
    iget-object v1, v0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    move-object v0, v1

    .line 1150527
    const v1, 0x7f081dff

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 1150528
    return-void
.end method
