.class public Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

.field public final b:Landroid/content/Intent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1150594
    new-instance v0, LX/6qP;

    invoke-direct {v0}, LX/6qP;-><init>()V

    sput-object v0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6qQ;)V
    .locals 1

    .prologue
    .line 1150590
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150591
    iget-object v0, p1, LX/6qQ;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1150592
    iget-object v0, p1, LX/6qQ;->b:Landroid/content/Intent;

    iput-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;->b:Landroid/content/Intent;

    .line 1150593
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1150586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150587
    const-class v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1150588
    const-class v0, Landroid/content/Intent;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;->b:Landroid/content/Intent;

    .line 1150589
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1150582
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1150583
    iget-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1150584
    iget-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;->b:Landroid/content/Intent;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1150585
    return-void
.end method
