.class public Lcom/facebook/payments/auth/settings/PaymentPinSettingsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1150396
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1150391
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1150392
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1150393
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/payments/auth/settings/PaymentPinSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1150394
    const-string v1, "payment_pin_settings_params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1150395
    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1150381
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "payment_pin_settings_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1150382
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsActivity;->q:Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;

    .line 1150383
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1150384
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1150385
    const-string p0, "payment_pin_settings_params"

    invoke-virtual {v3, p0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1150386
    new-instance p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;

    invoke-direct {p0}, Lcom/facebook/payments/auth/settings/PaymentPinSettingsFragment;-><init>()V

    .line 1150387
    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1150388
    move-object v2, p0

    .line 1150389
    const-string v3, "payment_pin_settings_fragment"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1150390
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsActivity;

    invoke-static {v0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v0

    check-cast v0, LX/6wr;

    iput-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsActivity;->p:LX/6wr;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1150373
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1150374
    const v0, 0x7f030597

    invoke-virtual {p0, v0}, Lcom/facebook/payments/auth/settings/PaymentPinSettingsActivity;->setContentView(I)V

    .line 1150375
    if-nez p1, :cond_0

    .line 1150376
    invoke-direct {p0}, Lcom/facebook/payments/auth/settings/PaymentPinSettingsActivity;->a()V

    .line 1150377
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsActivity;->q:Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;

    .line 1150378
    iget-object p1, v0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-object v0, p1

    .line 1150379
    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->a(Landroid/app/Activity;LX/6ws;)V

    .line 1150380
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1150366
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 1150367
    invoke-static {p0, p0}, Lcom/facebook/payments/auth/settings/PaymentPinSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1150368
    invoke-virtual {p0}, Lcom/facebook/payments/auth/settings/PaymentPinSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "payment_pin_settings_params"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;

    iput-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsActivity;->q:Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;

    .line 1150369
    iget-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsActivity;->p:LX/6wr;

    iget-object v1, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsActivity;->q:Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;

    .line 1150370
    iget-object p1, v1, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-object v1, p1

    .line 1150371
    iget-object v1, v1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-virtual {v0, p0, v1}, LX/6wr;->b(Landroid/app/Activity;LX/73i;)V

    .line 1150372
    return-void
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 1150361
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1150362
    iget-object v0, p0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsActivity;->q:Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;

    .line 1150363
    iget-object v1, v0, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-object v0, v1

    .line 1150364
    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->b(Landroid/app/Activity;LX/6ws;)V

    .line 1150365
    return-void
.end method
