.class public Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/6o9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/6oC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/6o8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/6pC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Z

.field private u:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1148008
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1148005
    invoke-direct {p0}, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->k()V

    .line 1148006
    iget-object v0, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->m:Lcom/facebook/content/SecureContextHelper;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1148007
    return-void
.end method

.method private k()V
    .locals 1

    .prologue
    .line 1147976
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1147977
    if-eqz v0, :cond_0

    .line 1147978
    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 1147979
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->u:Z

    .line 1147980
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 1148004
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ju;->c(Z)LX/0ju;

    move-result-object v0

    const v1, 0x7f081def

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f081df1

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f081df0

    new-instance v2, LX/6oE;

    invoke-direct {v2, p0}, LX/6oE;-><init>(Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080022

    new-instance v2, LX/6oD;

    invoke-direct {v2, p0}, LX/6oD;-><init>(Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1147995
    packed-switch p1, :pswitch_data_0

    .line 1147996
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1147997
    :goto_0
    return-void

    .line 1147998
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1147999
    const-string v0, "user_entered_pin"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1148000
    iget-object p1, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->q:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    invoke-virtual {p1, v0}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    .line 1148001
    new-instance p2, LX/6oF;

    invoke-direct {p2, p0}, LX/6oF;-><init>(Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;)V

    iget-object p3, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->s:Ljava/util/concurrent/Executor;

    invoke-static {p1, p2, p3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1148002
    goto :goto_0

    .line 1148003
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1ee01e57

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1147988
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1147989
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v3, p0

    check-cast v3, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v1}, LX/6o9;->b(LX/0QB;)LX/6o9;

    move-result-object v6

    check-cast v6, LX/6o9;

    invoke-static {v1}, LX/6oC;->b(LX/0QB;)LX/6oC;

    move-result-object v7

    check-cast v7, LX/6oC;

    new-instance v9, LX/6o8;

    const-class v8, Landroid/content/Context;

    invoke-interface {v1, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-direct {v9, v8}, LX/6o8;-><init>(Landroid/content/Context;)V

    move-object v8, v9

    check-cast v8, LX/6o8;

    invoke-static {v1}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    move-result-object v9

    check-cast v9, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    invoke-static {v1}, LX/6pC;->a(LX/0QB;)LX/6pC;

    move-result-object v10

    check-cast v10, LX/6pC;

    invoke-static {v1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v5, v3, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->m:Lcom/facebook/content/SecureContextHelper;

    iput-object v6, v3, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->n:LX/6o9;

    iput-object v7, v3, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->o:LX/6oC;

    iput-object v8, v3, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->p:LX/6o8;

    iput-object v9, v3, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->q:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    iput-object v10, v3, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->r:LX/6pC;

    iput-object v1, v3, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->s:Ljava/util/concurrent/Executor;

    .line 1147990
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1147991
    const-string v2, "is_pin_present"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->t:Z

    .line 1147992
    if-eqz p1, :cond_0

    .line 1147993
    const-string v1, "is_hidden"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->u:Z

    .line 1147994
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x1578bef2

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1147985
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1147986
    const-string v0, "is_hidden"

    iget-boolean v1, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1147987
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x43108d78

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1147981
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 1147982
    iget-boolean v1, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->u:Z

    if-eqz v1, :cond_0

    .line 1147983
    invoke-direct {p0}, Lcom/facebook/payments/auth/fingerprint/FingerprintNuxDialogFragment;->k()V

    .line 1147984
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x49892280    # 1123408.0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
