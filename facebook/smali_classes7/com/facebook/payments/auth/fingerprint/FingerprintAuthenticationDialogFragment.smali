.class public Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x17
.end annotation


# instance fields
.field public m:LX/6oC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/6o6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/6o9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/6ni;

.field public t:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1147818
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1147819
    return-void
.end method

.method public static c(Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1147815
    iget-object v0, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->u:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_0

    .line 1147816
    iget-object v0, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1147817
    :cond_0
    return-void
.end method

.method public static n(Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;)V
    .locals 1

    .prologue
    .line 1147811
    iget-object v0, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->s:LX/6ni;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1147812
    iget-object v0, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->s:LX/6ni;

    invoke-interface {v0}, LX/6ni;->b()V

    .line 1147813
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1147814
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1147800
    new-instance v1, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 1147801
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03066c

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1147802
    const v0, 0x7f0d11a7

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->u:Lcom/facebook/resources/ui/FbTextView;

    .line 1147803
    invoke-virtual {v1, v2}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    .line 1147804
    const v0, 0x7f081dfa

    invoke-virtual {v1, v0}, LX/0ju;->a(I)LX/0ju;

    .line 1147805
    invoke-virtual {v1, v4}, LX/0ju;->c(Z)LX/0ju;

    .line 1147806
    const/high16 v0, 0x1040000

    new-instance v2, LX/6o0;

    invoke-direct {v2, p0}, LX/6o0;-><init>(Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;)V

    invoke-virtual {v1, v0, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1147807
    const v0, 0x7f081dfd

    new-instance v2, LX/6o1;

    invoke-direct {v2, p0}, LX/6o1;-><init>(Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;)V

    invoke-virtual {v1, v0, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1147808
    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 1147809
    invoke-virtual {v0, v4}, LX/2EJ;->setCanceledOnTouchOutside(Z)V

    .line 1147810
    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1147820
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This dialog should only be shown if FingerprintAvailabilityManager.Availability == AVAILABLE"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 1147796
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 1147797
    iget-object v0, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->s:LX/6ni;

    if-eqz v0, :cond_0

    .line 1147798
    iget-object v0, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->s:LX/6ni;

    invoke-interface {v0, p1}, LX/6ni;->onCancel(Landroid/content/DialogInterface;)V

    .line 1147799
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x11993bd3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1147793
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1147794
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v4, p0

    check-cast v4, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;

    invoke-static {v1}, LX/6oC;->b(LX/0QB;)LX/6oC;

    move-result-object v5

    check-cast v5, LX/6oC;

    invoke-static {v1}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    move-result-object v6

    check-cast v6, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    invoke-static {v1}, LX/6o6;->b(LX/0QB;)LX/6o6;

    move-result-object v7

    check-cast v7, LX/6o6;

    invoke-static {v1}, LX/6o9;->b(LX/0QB;)LX/6o9;

    move-result-object v8

    check-cast v8, LX/6o9;

    invoke-static {v1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/Executor;

    invoke-static {v1}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    iput-object v5, v4, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->m:LX/6oC;

    iput-object v6, v4, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->n:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    iput-object v7, v4, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->o:LX/6o6;

    iput-object v8, v4, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->p:LX/6o9;

    iput-object p1, v4, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->q:Ljava/util/concurrent/Executor;

    iput-object v1, v4, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->r:Landroid/os/Handler;

    .line 1147795
    const/16 v1, 0x2b

    const v2, 0x717e53af

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7784b9c5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1147788
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroy()V

    .line 1147789
    iget-object v1, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 1147790
    iget-object v1, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1147791
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->t:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1147792
    :cond_0
    const/16 v1, 0x2b

    const v2, 0xeba796b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x561279ea

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1147783
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onPause()V

    .line 1147784
    iget-object v1, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->m:LX/6oC;

    if-eqz v1, :cond_0

    .line 1147785
    iget-object v1, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->m:LX/6oC;

    .line 1147786
    iget-object p0, v1, LX/6oC;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/6oH;

    invoke-virtual {p0}, LX/6oH;->a()V

    .line 1147787
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x48fc167e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x13432005

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1147769
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 1147770
    iget-object v1, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->o:LX/6o6;

    invoke-virtual {v1}, LX/6o6;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1147771
    iget-object v1, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->m:LX/6oC;

    if-eqz v1, :cond_0

    .line 1147772
    iget-object v1, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->m:LX/6oC;

    .line 1147773
    iget-object v2, v1, LX/6oC;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6oH;

    const-string v3, "nonce_key/"

    .line 1147774
    iget-object v4, v2, LX/6oH;->h:LX/6ny;

    invoke-virtual {v4, v3}, LX/6ny;->a(Ljava/lang/String;)LX/0am;

    move-result-object v4

    .line 1147775
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1147776
    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v2, v4, p0, v1}, LX/6oH;->a$redex0(LX/6oH;Ljava/lang/String;Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;I)V

    .line 1147777
    :cond_0
    :goto_0
    const v1, -0x426cc78b

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 1147778
    :cond_1
    iget-object v1, p0, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->p:LX/6o9;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/6o9;->a(Z)V

    .line 1147779
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1147780
    invoke-virtual {v1}, Landroid/app/Dialog;->cancel()V

    goto :goto_0

    .line 1147781
    :cond_2
    invoke-virtual {v2}, LX/6oH;->a()V

    .line 1147782
    invoke-virtual {p0}, Lcom/facebook/payments/auth/fingerprint/FingerprintAuthenticationDialogFragment;->a()V

    goto :goto_0
.end method
