.class public Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1163434
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1163431
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1163432
    const-string v1, "selector_params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1163433
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;

    invoke-static {v0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v0

    check-cast v0, LX/6wr;

    iput-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;->p:LX/6wr;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1163418
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1163419
    const v0, 0x7f030597

    invoke-virtual {p0, v0}, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;->setContentView(I)V

    .line 1163420
    iget-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;->q:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v0, v0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-boolean v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    iget-object v1, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;->q:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v1, v1, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v1, v1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-static {p0, v0, v1}, LX/6wr;->b(Landroid/app/Activity;ZLX/73i;)V

    .line 1163421
    if-nez p1, :cond_0

    .line 1163422
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;->q:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    .line 1163423
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1163424
    const-string p1, "selector_params"

    invoke-virtual {v3, p1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1163425
    new-instance p1, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;

    invoke-direct {p1}, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;-><init>()V

    .line 1163426
    invoke-virtual {p1, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1163427
    move-object v2, p1

    .line 1163428
    const-string v3, "selector_screen_fragment_tag"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1163429
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;->q:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v0, v0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->a(Landroid/app/Activity;LX/6ws;)V

    .line 1163430
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1163413
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 1163414
    invoke-static {p0, p0}, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1163415
    invoke-virtual {p0}, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "selector_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iput-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;->q:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    .line 1163416
    iget-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;->p:LX/6wr;

    iget-object v1, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;->q:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v1, v1, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-boolean v1, v1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    iget-object v2, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;->q:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v2, v2, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v2, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-virtual {v0, p0, v1, v2}, LX/6wr;->a(Landroid/app/Activity;ZLX/73i;)V

    .line 1163417
    return-void
.end method

.method public final finish()V
    .locals 1

    .prologue
    .line 1163405
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1163406
    iget-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenActivity;->q:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v0, v0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->b(Landroid/app/Activity;LX/6ws;)V

    .line 1163407
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1163408
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "selector_screen_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1163409
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/0fj;

    if-eqz v1, :cond_0

    .line 1163410
    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    .line 1163411
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1163412
    return-void
.end method
