.class public Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;
.super Lcom/facebook/base/fragment/FbListFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public i:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/71Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/71f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final m:LX/6qh;

.field public final n:LX/71a;

.field public o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

.field public p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/payments/selector/model/OptionSelectorRow;",
            ">;"
        }
    .end annotation
.end field

.field private q:Landroid/content/Context;

.field public r:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1163453
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbListFragment;-><init>()V

    .line 1163454
    new-instance v0, LX/71Z;

    invoke-direct {v0, p0}, LX/71Z;-><init>(Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->m:LX/6qh;

    .line 1163455
    new-instance v0, LX/71a;

    invoke-direct {v0, p0}, LX/71a;-><init>(Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->n:LX/71a;

    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;

    invoke-static {p0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v0

    check-cast v0, LX/6wr;

    new-instance p2, LX/71Y;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/71f;->a(LX/0QB;)LX/71f;

    move-result-object v2

    check-cast v2, LX/71f;

    invoke-direct {p2, v1, v2}, LX/71Y;-><init>(Landroid/content/Context;LX/71f;)V

    move-object v1, p2

    check-cast v1, LX/71Y;

    invoke-static {p0}, LX/71f;->a(LX/0QB;)LX/71f;

    move-result-object v2

    check-cast v2, LX/71f;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p1, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->i:LX/6wr;

    iput-object v1, p1, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->j:LX/71Y;

    iput-object v2, p1, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->k:LX/71f;

    iput-object p0, p1, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->l:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static h(Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;)V
    .locals 2

    .prologue
    .line 1163456
    iget-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->j:LX/71Y;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/71Y;->setNotifyOnChange(Z)V

    .line 1163457
    iget-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->j:LX/71Y;

    invoke-virtual {v0}, LX/71Y;->clear()V

    .line 1163458
    iget-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->j:LX/71Y;

    iget-object v1, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v1, v1, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/71Y;->addAll(Ljava/util/Collection;)V

    .line 1163459
    iget-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->j:LX/71Y;

    const v1, -0x29f14b1c

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1163460
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 5

    .prologue
    .line 1163461
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1163462
    if-eqz v0, :cond_0

    .line 1163463
    iget-object v1, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v1, v1, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->b:LX/0Px;

    invoke-static {v1}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v1

    const-class v2, Lcom/facebook/payments/selector/model/OptionSelectorRow;

    invoke-virtual {v1, v2}, LX/0wv;->a(Ljava/lang/Class;)LX/0wv;

    move-result-object v1

    new-instance v2, LX/71b;

    invoke-direct {v2, p0}, LX/71b;-><init>(Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;)V

    invoke-virtual {v1, v2}, LX/0wv;->a(LX/0Rl;)LX/0wv;

    move-result-object v1

    invoke-virtual {v1}, LX/0wv;->b()LX/0Px;

    move-result-object v1

    .line 1163464
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1163465
    const-string v3, "extra_collected_data_key"

    iget-object v4, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v4, v4, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1163466
    const-string v3, "extra_options"

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1163467
    const-string v1, "extra_new_options"

    iget-object v3, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1163468
    const/4 v1, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1163469
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 1163470
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 1163471
    packed-switch p1, :pswitch_data_0

    .line 1163472
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbListFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1163473
    :goto_0
    return-void

    .line 1163474
    :pswitch_0
    const/4 v4, 0x1

    .line 1163475
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1163476
    new-instance v0, Lcom/facebook/payments/selector/model/OptionSelectorRow;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "extra_text"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "extra_currency_amount"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/currency/CurrencyAmount;

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/payments/selector/model/OptionSelectorRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;ZZ)V

    .line 1163477
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1163478
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1163479
    iget-object v2, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v2, v2, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->b:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1163480
    iget-object v2, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->a(LX/0Px;)Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    .line 1163481
    iget-object v1, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1163482
    invoke-static {p0}, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->h(Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;)V

    .line 1163483
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x5eee3f26

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1163484
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1163485
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0103f1

    const v3, 0x7f0e0326

    invoke-static {v0, v2, v3}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->q:Landroid/content/Context;

    .line 1163486
    const-class v0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;

    iget-object v2, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->q:Landroid/content/Context;

    invoke-static {v0, p0, v2}, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1163487
    if-eqz p1, :cond_0

    .line 1163488
    const-string v0, "selector_params"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iput-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    .line 1163489
    const-string v0, "extra_new_option_selector_rows"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->p:Ljava/util/ArrayList;

    .line 1163490
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    if-nez v0, :cond_1

    .line 1163491
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1163492
    const-string v2, "selector_params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iput-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    .line 1163493
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->p:Ljava/util/ArrayList;

    .line 1163494
    :cond_1
    const/16 v0, 0x2b

    const v2, -0x6aaaefb7

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2a87d9b4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1163495
    iget-object v1, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->q:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0306af

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1163496
    iget-object v2, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v2, v2, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v2, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->c:LX/0am;

    iget-object v3, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v3, v3, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-boolean v3, v3, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    invoke-static {v1, v2, v3}, LX/6wr;->a(Landroid/view/View;LX/0am;Z)V

    .line 1163497
    const/16 v2, 0x2b

    const v3, -0x43afa29a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1163498
    const-string v0, "selector_params"

    iget-object v1, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1163499
    const-string v0, "extra_new_option_selector_rows"

    iget-object v1, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->p:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1163500
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1163501
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1163502
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1163503
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->r:Landroid/widget/ListView;

    .line 1163504
    const v0, 0x7f0d00bb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 1163505
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1163506
    check-cast v1, Landroid/view/ViewGroup;

    new-instance v2, LX/71c;

    invoke-direct {v2, p0}, LX/71c;-><init>(Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;)V

    iget-object p1, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object p1, p1, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object p1, p1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    iget-object p2, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object p2, p2, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object p2, p2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-virtual {p2}, LX/6ws;->getTitleBarNavIconStyle()LX/73h;

    move-result-object p2

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 1163507
    iget-object v1, v0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    move-object v0, v1

    .line 1163508
    iget-object v1, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->o:Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v1, v1, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1163509
    iget-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->k:LX/71f;

    iget-object v1, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->n:LX/71a;

    .line 1163510
    iput-object v1, v0, LX/71f;->b:LX/71a;

    .line 1163511
    iget-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->j:LX/71Y;

    iget-object v1, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->m:LX/6qh;

    .line 1163512
    iput-object v1, v0, LX/71Y;->b:LX/6qh;

    .line 1163513
    iget-object v0, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->r:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->j:LX/71Y;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1163514
    invoke-static {p0}, Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;->h(Lcom/facebook/payments/selector/PaymentsSelectorScreenFragment;)V

    .line 1163515
    return-void
.end method
