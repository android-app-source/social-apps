.class public Lcom/facebook/payments/selector/model/OptionSelectorRow;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/selector/model/SelectorRow;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/selector/model/OptionSelectorRow;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/payments/currency/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Z

.field public final e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1163642
    new-instance v0, LX/71j;

    invoke-direct {v0}, LX/71j;-><init>()V

    sput-object v0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1163635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163636
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->a:Ljava/lang/String;

    .line 1163637
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->b:Ljava/lang/String;

    .line 1163638
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1163639
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->d:Z

    .line 1163640
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->e:Z

    .line 1163641
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;ZZ)V
    .locals 0

    .prologue
    .line 1163643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163644
    iput-object p1, p0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->a:Ljava/lang/String;

    .line 1163645
    iput-object p2, p0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->b:Ljava/lang/String;

    .line 1163646
    iput-object p3, p0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1163647
    iput-boolean p4, p0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->d:Z

    .line 1163648
    iput-boolean p5, p0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->e:Z

    .line 1163649
    return-void
.end method


# virtual methods
.method public final a()LX/71l;
    .locals 1

    .prologue
    .line 1163634
    sget-object v0, LX/71l;->CHECKBOX_OPTION_SELECTOR:LX/71l;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1163633
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1163627
    iget-object v0, p0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1163628
    iget-object v0, p0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1163629
    iget-object v0, p0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1163630
    iget-boolean v0, p0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1163631
    iget-boolean v0, p0, Lcom/facebook/payments/selector/model/OptionSelectorRow;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1163632
    return-void
.end method
