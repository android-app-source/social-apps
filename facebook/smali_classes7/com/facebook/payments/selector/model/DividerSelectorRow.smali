.class public Lcom/facebook/payments/selector/model/DividerSelectorRow;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/selector/model/SelectorRow;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/selector/model/DividerSelectorRow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1163599
    new-instance v0, LX/71h;

    invoke-direct {v0}, LX/71h;-><init>()V

    sput-object v0, Lcom/facebook/payments/selector/model/DividerSelectorRow;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1163600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/71l;
    .locals 1

    .prologue
    .line 1163601
    sget-object v0, LX/71l;->DIVIDER_ROW:LX/71l;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1163602
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 1163603
    return-void
.end method
