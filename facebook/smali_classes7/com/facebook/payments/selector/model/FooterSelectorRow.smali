.class public Lcom/facebook/payments/selector/model/FooterSelectorRow;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/selector/model/SelectorRow;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/selector/model/FooterSelectorRow;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1163607
    new-instance v0, LX/71i;

    invoke-direct {v0}, LX/71i;-><init>()V

    sput-object v0, Lcom/facebook/payments/selector/model/FooterSelectorRow;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1163608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163609
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/selector/model/FooterSelectorRow;->a:Ljava/lang/String;

    .line 1163610
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/selector/model/FooterSelectorRow;->b:Ljava/lang/String;

    .line 1163611
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/payments/selector/model/FooterSelectorRow;->c:Landroid/net/Uri;

    .line 1163612
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1163613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163614
    iput-object p1, p0, Lcom/facebook/payments/selector/model/FooterSelectorRow;->a:Ljava/lang/String;

    .line 1163615
    iput-object p2, p0, Lcom/facebook/payments/selector/model/FooterSelectorRow;->b:Ljava/lang/String;

    .line 1163616
    iput-object p3, p0, Lcom/facebook/payments/selector/model/FooterSelectorRow;->c:Landroid/net/Uri;

    .line 1163617
    return-void
.end method


# virtual methods
.method public final a()LX/71l;
    .locals 1

    .prologue
    .line 1163618
    sget-object v0, LX/71l;->FOOTER_VIEW:LX/71l;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1163619
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1163620
    iget-object v0, p0, Lcom/facebook/payments/selector/model/FooterSelectorRow;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1163621
    iget-object v0, p0, Lcom/facebook/payments/selector/model/FooterSelectorRow;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1163622
    iget-object v0, p0, Lcom/facebook/payments/selector/model/FooterSelectorRow;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1163623
    return-void
.end method
