.class public Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/payments/selector/model/SelectorRow;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1163653
    new-instance v0, LX/71k;

    invoke-direct {v0}, LX/71k;-><init>()V

    sput-object v0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1163654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163655
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->a:Ljava/lang/String;

    .line 1163656
    const-class v0, Lcom/facebook/payments/selector/model/SelectorRow;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->b:LX/0Px;

    .line 1163657
    const-class v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1163658
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->d:Ljava/lang/String;

    .line 1163659
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/payments/selector/model/SelectorRow;",
            ">;",
            "Lcom/facebook/payments/decorator/PaymentsDecoratorParams;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1163660
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163661
    iput-object p1, p0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->a:Ljava/lang/String;

    .line 1163662
    iput-object p2, p0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->b:LX/0Px;

    .line 1163663
    iput-object p3, p0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1163664
    iput-object p4, p0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->d:Ljava/lang/String;

    .line 1163665
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/payments/selector/model/SelectorRow;",
            ">;)",
            "Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;"
        }
    .end annotation

    .prologue
    .line 1163666
    new-instance v0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    iget-object v1, p0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v3, p0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->d:Ljava/lang/String;

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;-><init>(Ljava/lang/String;LX/0Px;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;Ljava/lang/String;)V

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1163667
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1163668
    iget-object v0, p0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1163669
    iget-object v0, p0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1163670
    iget-object v0, p0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1163671
    iget-object v0, p0, Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1163672
    return-void
.end method
