.class public Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/selector/model/SelectorRow;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/content/Intent;

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1163590
    new-instance v0, LX/71g;

    invoke-direct {v0}, LX/71g;-><init>()V

    sput-object v0, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1163585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163586
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;->a:Ljava/lang/String;

    .line 1163587
    const-class v0, Landroid/content/Intent;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;->b:Landroid/content/Intent;

    .line 1163588
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;->c:I

    .line 1163589
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 1163591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163592
    iput-object p1, p0, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;->a:Ljava/lang/String;

    .line 1163593
    iput-object p2, p0, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;->b:Landroid/content/Intent;

    .line 1163594
    iput p3, p0, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;->c:I

    .line 1163595
    return-void
.end method


# virtual methods
.method public final a()LX/71l;
    .locals 1

    .prologue
    .line 1163584
    sget-object v0, LX/71l;->ADD_CUSTOM_OPTION_SELECTOR_ROW:LX/71l;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1163583
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1163579
    iget-object v0, p0, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1163580
    iget-object v0, p0, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;->b:Landroid/content/Intent;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1163581
    iget v0, p0, Lcom/facebook/payments/selector/model/AddCustomOptionSelectorRow;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1163582
    return-void
.end method
