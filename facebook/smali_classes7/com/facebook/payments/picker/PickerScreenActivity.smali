.class public Lcom/facebook/payments/picker/PickerScreenActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public p:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/payments/picker/model/PickerScreenConfig;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1162717
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/payments/picker/model/PickerScreenConfig;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1162718
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1162719
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/payments/picker/PickerScreenActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1162720
    invoke-static {v0, p1}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Intent;Lcom/facebook/payments/picker/model/PickerScreenConfig;)V

    .line 1162721
    return-object v0
.end method

.method public static a(Landroid/content/Intent;Lcom/facebook/payments/picker/model/PickerScreenConfig;)V
    .locals 1

    .prologue
    .line 1162722
    const-string v0, "extra_picker_screen_config"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1162723
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/picker/PickerScreenActivity;

    invoke-static {v0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v0

    check-cast v0, LX/6wr;

    iput-object v0, p0, Lcom/facebook/payments/picker/PickerScreenActivity;->p:LX/6wr;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1162724
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1162725
    const v0, 0x7f030597

    invoke-virtual {p0, v0}, Lcom/facebook/payments/picker/PickerScreenActivity;->setContentView(I)V

    .line 1162726
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenActivity;->q:Lcom/facebook/payments/picker/model/PickerScreenConfig;

    invoke-interface {v0}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-boolean v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenActivity;->q:Lcom/facebook/payments/picker/model/PickerScreenConfig;

    invoke-interface {v1}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    iget-object v1, v1, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v1, v1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-static {p0, v0, v1}, LX/6wr;->b(Landroid/app/Activity;ZLX/73i;)V

    .line 1162727
    if-nez p1, :cond_0

    .line 1162728
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/payments/picker/PickerScreenActivity;->q:Lcom/facebook/payments/picker/model/PickerScreenConfig;

    .line 1162729
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1162730
    const-string p1, "extra_picker_screen_config"

    invoke-virtual {v3, p1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1162731
    new-instance p1, Lcom/facebook/payments/picker/PickerScreenFragment;

    invoke-direct {p1}, Lcom/facebook/payments/picker/PickerScreenFragment;-><init>()V

    .line 1162732
    invoke-virtual {p1, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1162733
    move-object v2, p1

    .line 1162734
    const-string v3, "picker_screen_fragment_tag"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1162735
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenActivity;->q:Lcom/facebook/payments/picker/model/PickerScreenConfig;

    invoke-interface {v0}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->a(Landroid/app/Activity;LX/6ws;)V

    .line 1162736
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1162737
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 1162738
    invoke-static {p0, p0}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1162739
    invoke-virtual {p0}, Lcom/facebook/payments/picker/PickerScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1162740
    const-string v1, "extra_picker_screen_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenConfig;

    iput-object v0, p0, Lcom/facebook/payments/picker/PickerScreenActivity;->q:Lcom/facebook/payments/picker/model/PickerScreenConfig;

    .line 1162741
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenActivity;->p:LX/6wr;

    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenActivity;->q:Lcom/facebook/payments/picker/model/PickerScreenConfig;

    invoke-interface {v1}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    iget-object v1, v1, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-boolean v1, v1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    iget-object v2, p0, Lcom/facebook/payments/picker/PickerScreenActivity;->q:Lcom/facebook/payments/picker/model/PickerScreenConfig;

    invoke-interface {v2}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    iget-object v2, v2, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v2, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-virtual {v0, p0, v1, v2}, LX/6wr;->a(Landroid/app/Activity;ZLX/73i;)V

    .line 1162742
    return-void
.end method

.method public final finish()V
    .locals 1

    .prologue
    .line 1162743
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1162744
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenActivity;->q:Lcom/facebook/payments/picker/model/PickerScreenConfig;

    invoke-interface {v0}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->b(Landroid/app/Activity;LX/6ws;)V

    .line 1162745
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1162746
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "picker_screen_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1162747
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/0fj;

    if-eqz v1, :cond_0

    .line 1162748
    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    .line 1162749
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1162750
    return-void
.end method
