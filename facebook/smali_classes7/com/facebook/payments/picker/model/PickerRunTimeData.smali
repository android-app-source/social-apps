.class public interface abstract Lcom/facebook/payments/picker/model/PickerRunTimeData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONFIG::",
        "Lcom/facebook/payments/picker/model/PickerScreenConfig;",
        "FETCHER_PARAMS::",
        "Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;",
        "CORE_C",
        "LIENT_DATA::Lcom/facebook/payments/picker/model/CoreClientData;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# virtual methods
.method public abstract a()Lcom/facebook/payments/picker/model/PickerScreenConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TCONFIG;"
        }
    .end annotation
.end method

.method public abstract b()Landroid/content/Intent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract c()Z
.end method

.method public abstract d()Z
.end method
