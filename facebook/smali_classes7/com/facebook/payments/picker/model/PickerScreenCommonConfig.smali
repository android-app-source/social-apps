.class public final Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

.field public final b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

.field public final c:LX/71C;

.field public final d:LX/6xg;

.field public final e:Ljava/lang/String;

.field public final f:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

.field public final g:Lcom/facebook/payments/picker/model/ProductParcelableConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1163126
    new-instance v0, LX/719;

    invoke-direct {v0}, LX/719;-><init>()V

    sput-object v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/71A;)V
    .locals 1

    .prologue
    .line 1163110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163111
    iget-object v0, p1, LX/71A;->e:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    move-object v0, v0

    .line 1163112
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    .line 1163113
    iget-object v0, p1, LX/71A;->a:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-object v0, v0

    .line 1163114
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    .line 1163115
    iget-object v0, p1, LX/71A;->b:LX/71C;

    move-object v0, v0

    .line 1163116
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/71C;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->c:LX/71C;

    .line 1163117
    iget-object v0, p1, LX/71A;->c:LX/6xg;

    move-object v0, v0

    .line 1163118
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    .line 1163119
    iget-object v0, p1, LX/71A;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1163120
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->e:Ljava/lang/String;

    .line 1163121
    iget-object v0, p1, LX/71A;->f:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    move-object v0, v0

    .line 1163122
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->f:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    .line 1163123
    iget-object v0, p1, LX/71A;->g:Lcom/facebook/payments/picker/model/ProductParcelableConfig;

    move-object v0, v0

    .line 1163124
    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->g:Lcom/facebook/payments/picker/model/ProductParcelableConfig;

    .line 1163125
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1163101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163102
    const-class v0, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    .line 1163103
    const-class v0, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    .line 1163104
    const-class v0, LX/71C;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/71C;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->c:LX/71C;

    .line 1163105
    const-class v0, LX/6xg;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    .line 1163106
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->e:Ljava/lang/String;

    .line 1163107
    const-class v0, Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->f:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    .line 1163108
    const-class v0, Lcom/facebook/payments/picker/model/ProductParcelableConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/ProductParcelableConfig;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->g:Lcom/facebook/payments/picker/model/ProductParcelableConfig;

    .line 1163109
    return-void
.end method

.method public static newBuilder()LX/71A;
    .locals 1

    .prologue
    .line 1163091
    new-instance v0, LX/71A;

    invoke-direct {v0}, LX/71A;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1163100
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1163092
    iget-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1163093
    iget-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1163094
    iget-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->c:LX/71C;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1163095
    iget-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1163096
    iget-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1163097
    iget-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->f:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1163098
    iget-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->g:Lcom/facebook/payments/picker/model/ProductParcelableConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1163099
    return-void
.end method
