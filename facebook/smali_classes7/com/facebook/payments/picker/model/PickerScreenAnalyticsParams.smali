.class public Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6xZ;

.field public final b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1163071
    new-instance v0, LX/717;

    invoke-direct {v0}, LX/717;-><init>()V

    sput-object v0, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/718;)V
    .locals 1

    .prologue
    .line 1163072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163073
    iget-object v0, p1, LX/718;->a:LX/6xZ;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->a:LX/6xZ;

    .line 1163074
    iget-object v0, p1, LX/718;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1163075
    iget-object v0, p1, LX/718;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->c:Ljava/lang/String;

    .line 1163076
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1163077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163078
    const-class v0, LX/6xZ;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xZ;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->a:LX/6xZ;

    .line 1163079
    const-class v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1163080
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->c:Ljava/lang/String;

    .line 1163081
    return-void
.end method

.method public static a(LX/6xZ;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/718;
    .locals 1

    .prologue
    .line 1163082
    new-instance v0, LX/718;

    invoke-direct {v0, p0, p1}, LX/718;-><init>(LX/6xZ;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1163083
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1163084
    iget-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->a:LX/6xZ;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1163085
    iget-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1163086
    iget-object v0, p0, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1163087
    return-void
.end method
