.class public Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1163256
    new-instance v0, LX/71K;

    invoke-direct {v0}, LX/71K;-><init>()V

    sput-object v0, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1163253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163254
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;->a:Z

    .line 1163255
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 1163250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163251
    iput-boolean p1, p0, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;->a:Z

    .line 1163252
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1163249
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1163247
    iget-boolean v0, p0, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1163248
    return-void
.end method
