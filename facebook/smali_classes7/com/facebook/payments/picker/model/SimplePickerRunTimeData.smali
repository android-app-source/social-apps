.class public abstract Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/picker/model/PickerRunTimeData;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONFIG::",
        "Lcom/facebook/payments/picker/model/PickerScreenConfig;",
        "FETCHER_PARAMS::",
        "Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;",
        "CORE_C",
        "LIENT_DATA::Lcom/facebook/payments/picker/model/CoreClientData;",
        "SECTION_TYPE::",
        "LX/6vZ;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/payments/picker/model/PickerRunTimeData",
        "<TCONFIG;TFETCHER_PARAMS;TCORE_C",
        "LIENT_DATA;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/payments/picker/model/PickerScreenConfig;

.field public final b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

.field public final c:Lcom/facebook/payments/picker/model/CoreClientData;

.field public final d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<+",
            "LX/6vZ;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1157272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157273
    const-class v0, Lcom/facebook/payments/picker/model/PickerScreenConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenConfig;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a:Lcom/facebook/payments/picker/model/PickerScreenConfig;

    .line 1157274
    const-class v0, Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    .line 1157275
    const-class v0, Lcom/facebook/payments/picker/model/CoreClientData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/CoreClientData;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    .line 1157276
    invoke-static {p1}, LX/46R;->h(Landroid/os/Parcel;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->d:LX/0P1;

    .line 1157277
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;)V
    .locals 1

    .prologue
    .line 1157249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157250
    iput-object p1, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a:Lcom/facebook/payments/picker/model/PickerScreenConfig;

    .line 1157251
    invoke-interface {p1}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->f:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    .line 1157252
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    .line 1157253
    invoke-interface {p1}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->b:LX/0P1;

    iput-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->d:LX/0P1;

    .line 1157254
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/picker/model/PickerScreenConfig;",
            "Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;",
            "Lcom/facebook/payments/picker/model/CoreClientData;",
            "LX/0P1",
            "<TSECTION_TYPE;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1157266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157267
    iput-object p1, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a:Lcom/facebook/payments/picker/model/PickerScreenConfig;

    .line 1157268
    iput-object p2, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    .line 1157269
    iput-object p3, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    .line 1157270
    iput-object p4, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->d:LX/0P1;

    .line 1157271
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/picker/model/PickerScreenConfig;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TCONFIG;"
        }
    .end annotation

    .prologue
    .line 1157265
    iget-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a:Lcom/facebook/payments/picker/model/PickerScreenConfig;

    return-object v0
.end method

.method public final a(LX/6vZ;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1157264
    iget-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->d:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 1157263
    const/4 v0, 0x0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1157262
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;
    .locals 1

    .prologue
    .line 1157261
    invoke-virtual {p0}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    return-object v0
.end method

.method public final g()Lcom/facebook/payments/picker/model/CoreClientData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TCORE_C",
            "LIENT_DATA;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1157260
    iget-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1157255
    iget-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a:Lcom/facebook/payments/picker/model/PickerScreenConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1157256
    iget-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1157257
    iget-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1157258
    iget-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->d:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1157259
    return-void
.end method
