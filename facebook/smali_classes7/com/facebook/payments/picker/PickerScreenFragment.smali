.class public Lcom/facebook/payments/picker/PickerScreenFragment;
.super Lcom/facebook/base/fragment/FbListFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public final A:LX/6qh;

.field private final B:LX/70q;

.field public i:LX/6xb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/70l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/70y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:Landroid/content/Context;

.field private o:Landroid/widget/ListView;

.field private p:LX/70k;

.field public q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

.field public r:LX/6w1;

.field public s:LX/6wK;

.field public t:LX/6wI;

.field public u:LX/6vv;

.field public v:LX/6u4;

.field public w:LX/6w3;

.field public final x:LX/70m;

.field public final y:LX/6zj;

.field private final z:Landroid/widget/AbsListView$OnScrollListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1162889
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbListFragment;-><init>()V

    .line 1162890
    new-instance v0, LX/70m;

    invoke-direct {v0, p0}, LX/70m;-><init>(Lcom/facebook/payments/picker/PickerScreenFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->x:LX/70m;

    .line 1162891
    new-instance v0, LX/70n;

    invoke-direct {v0, p0}, LX/70n;-><init>(Lcom/facebook/payments/picker/PickerScreenFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->y:LX/6zj;

    .line 1162892
    new-instance v0, LX/70o;

    invoke-direct {v0, p0}, LX/70o;-><init>(Lcom/facebook/payments/picker/PickerScreenFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->z:Landroid/widget/AbsListView$OnScrollListener;

    .line 1162893
    new-instance v0, LX/70p;

    invoke-direct {v0, p0}, LX/70p;-><init>(Lcom/facebook/payments/picker/PickerScreenFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->A:LX/6qh;

    .line 1162894
    new-instance v0, LX/70q;

    invoke-direct {v0, p0}, LX/70q;-><init>(Lcom/facebook/payments/picker/PickerScreenFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->B:LX/70q;

    return-void
.end method

.method public static a(Lcom/facebook/payments/picker/PickerScreenFragment;Landroid/content/Intent;)V
    .locals 2
    .param p0    # Lcom/facebook/payments/picker/PickerScreenFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1162883
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1162884
    if-nez v0, :cond_0

    .line 1162885
    :goto_0
    return-void

    .line 1162886
    :cond_0
    if-eqz p1, :cond_1

    .line 1162887
    const/4 v1, -0x1

    invoke-virtual {v0, v1, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 1162888
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/payments/picker/PickerScreenFragment;)V
    .locals 4

    .prologue
    .line 1162877
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->t:LX/6wI;

    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    iget-object v2, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->s:LX/6wK;

    iget-object v3, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    invoke-interface {v2, v3}, LX/6wK;->a(Lcom/facebook/payments/picker/model/PickerRunTimeData;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/6wI;->a(Lcom/facebook/payments/picker/model/PickerRunTimeData;LX/0Px;)LX/0Px;

    move-result-object v0

    .line 1162878
    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->j:LX/70l;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/70l;->setNotifyOnChange(Z)V

    .line 1162879
    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->j:LX/70l;

    invoke-virtual {v1}, LX/70l;->clear()V

    .line 1162880
    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->j:LX/70l;

    invoke-virtual {v1, v0}, LX/70l;->addAll(Ljava/util/Collection;)V

    .line 1162881
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->j:LX/70l;

    const v1, 0x467806c9

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1162882
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 5

    .prologue
    .line 1162873
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    invoke-interface {v0}, Lcom/facebook/payments/picker/model/PickerRunTimeData;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/payments/picker/PickerScreenFragment;->a(Lcom/facebook/payments/picker/PickerScreenFragment;Landroid/content/Intent;)V

    .line 1162874
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    invoke-interface {v0}, Lcom/facebook/payments/picker/model/PickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    .line 1162875
    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->i:LX/6xb;

    invoke-interface {v0}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-interface {v0}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->a:LX/6xZ;

    const-string v4, "payflows_back_click"

    invoke-virtual {v1, v2, v3, v4}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/String;)V

    .line 1162876
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 1162871
    check-cast p1, LX/6vq;

    invoke-interface {p1}, LX/6vq;->onClick()V

    .line 1162872
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1162895
    sparse-switch p1, :sswitch_data_0

    .line 1162896
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbListFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1162897
    :goto_0
    return-void

    .line 1162898
    :sswitch_0
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->w:LX/6w3;

    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    invoke-interface {v0, v1, p1, p2, p3}, LX/6w3;->a(Lcom/facebook/payments/picker/model/PickerRunTimeData;IILandroid/content/Intent;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x65 -> :sswitch_0
        0x66 -> :sswitch_0
        0xc9 -> :sswitch_0
        0x12d -> :sswitch_0
        0x192 -> :sswitch_0
        0x193 -> :sswitch_0
        0x194 -> :sswitch_0
        0x1f5 -> :sswitch_0
        0x1f6 -> :sswitch_0
    .end sparse-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x63c3a68a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1162848
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1162849
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v3, 0x7f0e0326

    invoke-static {v0, v1, v3}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->n:Landroid/content/Context;

    .line 1162850
    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->n:Landroid/content/Context;

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, Lcom/facebook/payments/picker/PickerScreenFragment;

    invoke-static {v0}, LX/6xb;->a(LX/0QB;)LX/6xb;

    move-result-object v5

    check-cast v5, LX/6xb;

    new-instance v7, LX/70l;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-direct {v7, v6}, LX/70l;-><init>(Landroid/content/Context;)V

    move-object v6, v7

    check-cast v6, LX/70l;

    invoke-static {v0}, LX/70y;->a(LX/0QB;)LX/70y;

    move-result-object v7

    check-cast v7, LX/70y;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v0

    check-cast v0, LX/6wr;

    iput-object v5, v3, Lcom/facebook/payments/picker/PickerScreenFragment;->i:LX/6xb;

    iput-object v6, v3, Lcom/facebook/payments/picker/PickerScreenFragment;->j:LX/70l;

    iput-object v7, v3, Lcom/facebook/payments/picker/PickerScreenFragment;->k:LX/70y;

    iput-object v8, v3, Lcom/facebook/payments/picker/PickerScreenFragment;->l:Lcom/facebook/content/SecureContextHelper;

    iput-object v0, v3, Lcom/facebook/payments/picker/PickerScreenFragment;->m:LX/6wr;

    .line 1162851
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1162852
    const-string v1, "extra_picker_screen_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenConfig;

    .line 1162853
    invoke-interface {v0}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->c:LX/71C;

    .line 1162854
    iget-object v3, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->k:LX/70y;

    invoke-virtual {v3, v1}, LX/70y;->e(LX/71C;)LX/6vv;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->u:LX/6vv;

    .line 1162855
    iget-object v3, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->u:LX/6vv;

    iget-object v5, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->x:LX/70m;

    .line 1162856
    iput-object v5, v3, LX/6vv;->a:LX/70m;

    .line 1162857
    iget-object v3, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->k:LX/70y;

    invoke-virtual {v3, v1}, LX/70y;->a(LX/71C;)LX/6w1;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->r:LX/6w1;

    .line 1162858
    iget-object v3, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->k:LX/70y;

    invoke-virtual {v3, v1}, LX/70y;->c(LX/71C;)LX/6wI;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->t:LX/6wI;

    .line 1162859
    iget-object v3, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->k:LX/70y;

    invoke-virtual {v3, v1}, LX/70y;->b(LX/71C;)LX/6wK;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->s:LX/6wK;

    .line 1162860
    iget-object v3, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->j:LX/70l;

    iget-object v5, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->k:LX/70y;

    invoke-virtual {v5, v1}, LX/70y;->g(LX/71C;)LX/6wC;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->A:LX/6qh;

    .line 1162861
    iput-object v6, v3, LX/70l;->a:LX/6qh;

    .line 1162862
    iput-object v5, v3, LX/70l;->b:LX/6wC;

    .line 1162863
    iget-object v3, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->k:LX/70y;

    invoke-virtual {v3, v1}, LX/70y;->d(LX/71C;)LX/6w3;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->w:LX/6w3;

    .line 1162864
    iget-object v3, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->k:LX/70y;

    invoke-virtual {v3, v1}, LX/70y;->f(LX/71C;)LX/6u4;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->v:LX/6u4;

    .line 1162865
    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->i:LX/6xb;

    invoke-interface {v0}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-interface {v0}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v5

    iget-object v5, v5, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    invoke-interface {v0}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v6

    iget-object v6, v6, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iget-object v6, v6, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->a:LX/6xZ;

    invoke-virtual {v1, v3, v5, v6, p1}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xg;LX/6xZ;Landroid/os/Bundle;)V

    .line 1162866
    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    if-nez v1, :cond_0

    if-eqz p1, :cond_0

    .line 1162867
    const-string v1, "picker_run_time_data"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/picker/model/PickerRunTimeData;

    iput-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    .line 1162868
    :cond_0
    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    if-nez v1, :cond_1

    .line 1162869
    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->u:LX/6vv;

    invoke-virtual {v1, v0}, LX/6vv;->a(Lcom/facebook/payments/picker/model/PickerScreenConfig;)Lcom/facebook/payments/picker/model/PickerRunTimeData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    .line 1162870
    :cond_1
    const/16 v0, 0x2b

    const v1, 0x1f673212

    invoke-static {v4, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x52eff92

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1162844
    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->n:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0306b4

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1162845
    iget-object v2, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    invoke-interface {v2}, Lcom/facebook/payments/picker/model/PickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v2

    .line 1162846
    iget-object v3, v2, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    iget-object v3, v3, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v3, v3, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->c:LX/0am;

    iget-object v2, v2, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    iget-object v2, v2, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-boolean v2, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    invoke-static {v1, v3, v2}, LX/6wr;->a(Landroid/view/View;LX/0am;Z)V

    .line 1162847
    const/16 v2, 0x2b

    const v3, 0x4ba55a51    # 2.1673122E7f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x335720d6    # -8.8537424E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1162840
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onDestroy()V

    .line 1162841
    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->r:LX/6w1;

    if-eqz v1, :cond_0

    .line 1162842
    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->r:LX/6w1;

    invoke-interface {v1}, LX/6w1;->a()V

    .line 1162843
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x3c9ccb45

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x157f2c2f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1162838
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onResume()V

    .line 1162839
    const/16 v1, 0x2b

    const v2, -0x1cc82644

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1162834
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    invoke-interface {v0}, Lcom/facebook/payments/picker/model/PickerRunTimeData;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1162835
    const-string v0, "picker_run_time_data"

    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1162836
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1162837
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1162816
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1162817
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1162818
    const v1, 0x7f0d00bb

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 1162819
    iget-object v2, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    invoke-interface {v2}, Lcom/facebook/payments/picker/model/PickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    iget-object p1, v2, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->a:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1162820
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1162821
    check-cast v2, Landroid/view/ViewGroup;

    new-instance p2, LX/70r;

    invoke-direct {p2, p0, v0}, LX/70r;-><init>(Lcom/facebook/payments/picker/PickerScreenFragment;Landroid/app/Activity;)V

    iget-object v0, p1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    iget-object p1, p1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-virtual {p1}, LX/6ws;->getTitleBarNavIconStyle()LX/73h;

    move-result-object p1

    invoke-virtual {v1, v2, p2, v0, p1}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 1162822
    iget-object v0, v1, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    move-object v0, v0

    .line 1162823
    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    invoke-interface {v1}, Lcom/facebook/payments/picker/model/PickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/payments/picker/model/PickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1162824
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->o:Landroid/widget/ListView;

    .line 1162825
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->o:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->j:LX/70l;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1162826
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->o:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->z:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1162827
    new-instance v1, LX/70k;

    const v0, 0x7f0d0a95

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbListFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v2, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->o:Landroid/widget/ListView;

    invoke-direct {v1, v0, v2}, LX/70k;-><init>(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;Landroid/view/View;)V

    iput-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->p:LX/70k;

    .line 1162828
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->r:LX/6w1;

    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->p:LX/70k;

    invoke-interface {v0, v1}, LX/6w1;->a(LX/70k;)V

    .line 1162829
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->w:LX/6w3;

    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->A:LX/6qh;

    iget-object v2, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->p:LX/70k;

    invoke-interface {v0, v1, v2}, LX/6w3;->a(LX/6qh;LX/70k;)V

    .line 1162830
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    invoke-interface {v0}, Lcom/facebook/payments/picker/model/PickerRunTimeData;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1162831
    iget-object v0, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->r:LX/6w1;

    iget-object v1, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->y:LX/6zj;

    iget-object v2, p0, Lcom/facebook/payments/picker/PickerScreenFragment;->q:Lcom/facebook/payments/picker/model/PickerRunTimeData;

    invoke-interface {v0, v1, v2}, LX/6w1;->a(LX/6zj;Lcom/facebook/payments/picker/model/PickerRunTimeData;)V

    .line 1162832
    :goto_0
    return-void

    .line 1162833
    :cond_0
    invoke-static {p0}, Lcom/facebook/payments/picker/PickerScreenFragment;->b(Lcom/facebook/payments/picker/PickerScreenFragment;)V

    goto :goto_0
.end method
