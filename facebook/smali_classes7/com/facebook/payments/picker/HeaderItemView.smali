.class public Lcom/facebook/payments/picker/HeaderItemView;
.super Lcom/facebook/payments/ui/PaymentsComponentViewGroup;
.source ""

# interfaces
.implements LX/6vq;


# instance fields
.field private a:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1162679
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;)V

    .line 1162680
    invoke-direct {p0}, Lcom/facebook/payments/picker/HeaderItemView;->a()V

    .line 1162681
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1162682
    invoke-direct {p0, p1, p2}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1162683
    invoke-direct {p0}, Lcom/facebook/payments/picker/HeaderItemView;->a()V

    .line 1162684
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1162685
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1162686
    invoke-direct {p0}, Lcom/facebook/payments/picker/HeaderItemView;->a()V

    .line 1162687
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1162688
    const v0, 0x7f030875

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1162689
    const v0, 0x7f0d160c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/payments/picker/HeaderItemView;->a:Landroid/widget/TextView;

    .line 1162690
    return-void
.end method


# virtual methods
.method public final a(LX/6vm;)V
    .locals 2

    .prologue
    .line 1162691
    check-cast p1, LX/716;

    .line 1162692
    iget-object v0, p1, LX/716;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1162693
    iget-object v0, p0, Lcom/facebook/payments/picker/HeaderItemView;->a:Landroid/widget/TextView;

    iget-object v1, p1, LX/716;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1162694
    :cond_0
    return-void
.end method

.method public onClick()V
    .locals 0

    .prologue
    .line 1162695
    return-void
.end method
