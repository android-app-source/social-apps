.class public Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerRunTimeData;
.super Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/payments/picker/model/SimplePickerRunTimeData",
        "<",
        "Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;",
        "Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;",
        "Lcom/facebook/payments/picker/model/CoreClientData;",
        "LX/71X;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerRunTimeData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1163289
    new-instance v0, LX/71M;

    invoke-direct {v0}, LX/71M;-><init>()V

    sput-object v0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerRunTimeData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 1163294
    invoke-direct {p0, p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;-><init>(Landroid/os/Parcel;)V

    .line 1163295
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;)V
    .locals 0

    .prologue
    .line 1163290
    invoke-direct {p0, p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;-><init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;)V

    .line 1163291
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/picker/model/PickerScreenConfig;",
            "Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;",
            "Lcom/facebook/payments/picker/model/CoreClientData;",
            "LX/0P1",
            "<",
            "LX/71X;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1163292
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;-><init>(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)V

    .line 1163293
    return-void
.end method


# virtual methods
.method public final b()Landroid/content/Intent;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1163280
    sget-object v0, LX/71X;->PAYMENTS_PICKER_OPTION:LX/71X;

    invoke-virtual {p0, v0}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a(LX/6vZ;)Ljava/lang/String;

    move-result-object v0

    .line 1163281
    if-nez v0, :cond_0

    .line 1163282
    const/4 v0, 0x0

    .line 1163283
    :goto_0
    return-object v0

    .line 1163284
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1163285
    const-string v2, "payments_picker_option_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1163286
    const-string v2, "collected_data_key"

    invoke-virtual {p0}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;

    iget-object v0, v0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object v0, v1

    .line 1163287
    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1163288
    const/4 v0, 0x0

    return v0
.end method
