.class public Lcom/facebook/payments/picker/option/PaymentsPickerOption;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/picker/option/PaymentsPickerOption;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1163260
    new-instance v0, LX/71L;

    invoke-direct {v0}, LX/71L;-><init>()V

    sput-object v0, Lcom/facebook/payments/picker/option/PaymentsPickerOption;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1163261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163262
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOption;->a:Ljava/lang/String;

    .line 1163263
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOption;->b:Ljava/lang/String;

    .line 1163264
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1163265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163266
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Null or empty id provided."

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1163267
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Null or empty title provided."

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1163268
    iput-object p1, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOption;->a:Ljava/lang/String;

    .line 1163269
    iput-object p2, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOption;->b:Ljava/lang/String;

    .line 1163270
    return-void

    :cond_0
    move v0, v2

    .line 1163271
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1163272
    goto :goto_1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1163273
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1163274
    iget-object v0, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOption;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1163275
    iget-object v0, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOption;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1163276
    return-void
.end method
