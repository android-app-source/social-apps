.class public Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/picker/model/PickerScreenConfig;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/picker/option/PaymentsPickerOption;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1163316
    new-instance v0, LX/71O;

    invoke-direct {v0}, LX/71O;-><init>()V

    sput-object v0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1163317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163318
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;->a:Ljava/lang/String;

    .line 1163319
    const-class v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    iput-object v0, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 1163320
    const-class v0, Lcom/facebook/payments/picker/option/PaymentsPickerOption;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;->c:LX/0Px;

    .line 1163321
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/picker/option/PaymentsPickerOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1163322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1163323
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;->a:Ljava/lang/String;

    .line 1163324
    const-string v0, "PickerScreenCommonConfig is not provided."

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    iput-object v0, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 1163325
    invoke-virtual {p3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "No PaymentsPickerOption passed from client."

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1163326
    iput-object p3, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;->c:LX/0Px;

    .line 1163327
    return-void

    .line 1163328
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;
    .locals 1

    .prologue
    .line 1163329
    iget-object v0, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1163330
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1163331
    iget-object v0, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1163332
    iget-object v0, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1163333
    iget-object v0, p0, Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1163334
    return-void
.end method
