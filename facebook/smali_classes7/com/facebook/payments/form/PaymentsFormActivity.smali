.class public Lcom/facebook/payments/form/PaymentsFormActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/payments/form/model/PaymentsFormParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1158415
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/payments/form/model/PaymentsFormParams;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1158416
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1158417
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/payments/form/PaymentsFormActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1158418
    const-string v1, "extra_payments_form_params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1158419
    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1158420
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "payments_form_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1158421
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d03c5

    iget-object v2, p0, Lcom/facebook/payments/form/PaymentsFormActivity;->q:Lcom/facebook/payments/form/model/PaymentsFormParams;

    .line 1158422
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1158423
    const-string p0, "extra_payments_form_params"

    invoke-virtual {v3, p0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1158424
    new-instance p0, Lcom/facebook/payments/form/PaymentsFormFragment;

    invoke-direct {p0}, Lcom/facebook/payments/form/PaymentsFormFragment;-><init>()V

    .line 1158425
    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1158426
    move-object v2, p0

    .line 1158427
    const-string v3, "payments_form_fragment_tag"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1158428
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/form/PaymentsFormActivity;

    invoke-static {v0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v0

    check-cast v0, LX/6wr;

    iput-object v0, p0, Lcom/facebook/payments/form/PaymentsFormActivity;->p:LX/6wr;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1158429
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1158430
    const v0, 0x7f030f19

    invoke-virtual {p0, v0}, Lcom/facebook/payments/form/PaymentsFormActivity;->setContentView(I)V

    .line 1158431
    if-nez p1, :cond_0

    .line 1158432
    invoke-direct {p0}, Lcom/facebook/payments/form/PaymentsFormActivity;->a()V

    .line 1158433
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/form/PaymentsFormActivity;->q:Lcom/facebook/payments/form/model/PaymentsFormParams;

    iget-object v0, v0, Lcom/facebook/payments/form/model/PaymentsFormParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->a(Landroid/app/Activity;LX/6ws;)V

    .line 1158434
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1158435
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 1158436
    invoke-static {p0, p0}, Lcom/facebook/payments/form/PaymentsFormActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1158437
    invoke-virtual {p0}, Lcom/facebook/payments/form/PaymentsFormActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_payments_form_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/PaymentsFormParams;

    iput-object v0, p0, Lcom/facebook/payments/form/PaymentsFormActivity;->q:Lcom/facebook/payments/form/model/PaymentsFormParams;

    .line 1158438
    iget-object v0, p0, Lcom/facebook/payments/form/PaymentsFormActivity;->p:LX/6wr;

    iget-object v1, p0, Lcom/facebook/payments/form/PaymentsFormActivity;->q:Lcom/facebook/payments/form/model/PaymentsFormParams;

    iget-object v1, v1, Lcom/facebook/payments/form/model/PaymentsFormParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v1, v1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-virtual {v0, p0, v1}, LX/6wr;->b(Landroid/app/Activity;LX/73i;)V

    .line 1158439
    return-void
.end method

.method public final finish()V
    .locals 1

    .prologue
    .line 1158440
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1158441
    iget-object v0, p0, Lcom/facebook/payments/form/PaymentsFormActivity;->q:Lcom/facebook/payments/form/model/PaymentsFormParams;

    iget-object v0, v0, Lcom/facebook/payments/form/model/PaymentsFormParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->b(Landroid/app/Activity;LX/6ws;)V

    .line 1158442
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1158443
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "payments_form_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1158444
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/0fj;

    if-eqz v1, :cond_0

    .line 1158445
    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    .line 1158446
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1158447
    return-void
.end method
