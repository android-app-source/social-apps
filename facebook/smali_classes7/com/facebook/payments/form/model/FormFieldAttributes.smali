.class public Lcom/facebook/payments/form/model/FormFieldAttributes;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/form/model/FormFieldAttributes;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/payments/form/model/FormFieldAttributes;


# instance fields
.field public final b:LX/6xN;

.field public final c:Ljava/lang/String;

.field public final d:LX/6xe;

.field public final e:LX/6xO;

.field public final f:I

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1158703
    sget-object v0, LX/6xN;->UNKNOWN:LX/6xN;

    const-string v1, ""

    sget-object v2, LX/6xe;->OPTIONAL:LX/6xe;

    sget-object v3, LX/6xO;->UNKNOWN:LX/6xO;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/payments/form/model/FormFieldAttributes;->a(LX/6xN;Ljava/lang/String;LX/6xe;LX/6xO;)LX/6xM;

    move-result-object v0

    invoke-virtual {v0}, LX/6xM;->a()Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-result-object v0

    sput-object v0, Lcom/facebook/payments/form/model/FormFieldAttributes;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 1158704
    new-instance v0, LX/6xL;

    invoke-direct {v0}, LX/6xL;-><init>()V

    sput-object v0, Lcom/facebook/payments/form/model/FormFieldAttributes;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6xM;)V
    .locals 1

    .prologue
    .line 1158741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158742
    iget-object v0, p1, LX/6xM;->a:LX/6xN;

    iput-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->b:LX/6xN;

    .line 1158743
    iget-object v0, p1, LX/6xM;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->c:Ljava/lang/String;

    .line 1158744
    iget-object v0, p1, LX/6xM;->c:LX/6xe;

    iput-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->d:LX/6xe;

    .line 1158745
    iget-object v0, p1, LX/6xM;->d:LX/6xO;

    iput-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->e:LX/6xO;

    .line 1158746
    iget v0, p1, LX/6xM;->f:I

    iput v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->f:I

    .line 1158747
    iget-object v0, p1, LX/6xM;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->g:Ljava/lang/String;

    .line 1158748
    iget-object v0, p1, LX/6xM;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->h:Ljava/lang/String;

    .line 1158749
    iget-object v0, p1, LX/6xM;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->i:Ljava/lang/String;

    .line 1158750
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1158731
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158732
    const-class v0, LX/6xN;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xN;

    iput-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->b:LX/6xN;

    .line 1158733
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->c:Ljava/lang/String;

    .line 1158734
    const-class v0, LX/6xe;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xe;

    iput-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->d:LX/6xe;

    .line 1158735
    const-class v0, LX/6xO;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xO;

    iput-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->e:LX/6xO;

    .line 1158736
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->f:I

    .line 1158737
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->g:Ljava/lang/String;

    .line 1158738
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->h:Ljava/lang/String;

    .line 1158739
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->i:Ljava/lang/String;

    .line 1158740
    return-void
.end method

.method public static a(LX/6xN;Ljava/lang/String;LX/6xe;LX/6xO;)LX/6xM;
    .locals 1

    .prologue
    .line 1158751
    new-instance v0, LX/6xM;

    invoke-direct {v0, p0, p1, p2, p3}, LX/6xM;-><init>(LX/6xN;Ljava/lang/String;LX/6xe;LX/6xO;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/payments/form/model/FormFieldAttributes;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1158715
    new-instance v0, LX/6xM;

    iget-object v1, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->b:LX/6xN;

    iget-object v2, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->d:LX/6xe;

    iget-object v4, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->e:LX/6xO;

    invoke-direct {v0, v1, v2, v3, v4}, LX/6xM;-><init>(LX/6xN;Ljava/lang/String;LX/6xe;LX/6xO;)V

    iget-object v1, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->i:Ljava/lang/String;

    .line 1158716
    iput-object v1, v0, LX/6xM;->e:Ljava/lang/String;

    .line 1158717
    move-object v0, v0

    .line 1158718
    iget-object v1, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->g:Ljava/lang/String;

    .line 1158719
    iput-object v1, v0, LX/6xM;->g:Ljava/lang/String;

    .line 1158720
    move-object v0, v0

    .line 1158721
    iget v1, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->f:I

    .line 1158722
    iput v1, v0, LX/6xM;->f:I

    .line 1158723
    move-object v0, v0

    .line 1158724
    iget-object v1, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->h:Ljava/lang/String;

    .line 1158725
    iput-object v1, v0, LX/6xM;->h:Ljava/lang/String;

    .line 1158726
    move-object v0, v0

    .line 1158727
    move-object v0, v0

    .line 1158728
    iput-object p1, v0, LX/6xM;->e:Ljava/lang/String;

    .line 1158729
    move-object v0, v0

    .line 1158730
    invoke-virtual {v0}, LX/6xM;->a()Lcom/facebook/payments/form/model/FormFieldAttributes;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1158714
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1158705
    iget-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->b:LX/6xN;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1158706
    iget-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1158707
    iget-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->d:LX/6xe;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1158708
    iget-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->e:LX/6xO;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1158709
    iget v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1158710
    iget-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1158711
    iget-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1158712
    iget-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1158713
    return-void
.end method
