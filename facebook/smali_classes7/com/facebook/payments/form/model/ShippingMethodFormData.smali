.class public Lcom/facebook/payments/form/model/ShippingMethodFormData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/form/model/PaymentsFormData;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/form/model/ShippingMethodFormData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/Currency;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1158901
    new-instance v0, LX/6xX;

    invoke-direct {v0}, LX/6xX;-><init>()V

    sput-object v0, Lcom/facebook/payments/form/model/ShippingMethodFormData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1158902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158903
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Currency;

    iput-object v0, p0, Lcom/facebook/payments/form/model/ShippingMethodFormData;->a:Ljava/util/Currency;

    .line 1158904
    return-void
.end method

.method public constructor <init>(Ljava/util/Currency;)V
    .locals 1

    .prologue
    .line 1158905
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158906
    const-string v0, "Shipping Method Form cannot be initiated without the currency for price."

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1158907
    iput-object p1, p0, Lcom/facebook/payments/form/model/ShippingMethodFormData;->a:Ljava/util/Currency;

    .line 1158908
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1158909
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1158910
    iget-object v0, p0, Lcom/facebook/payments/form/model/ShippingMethodFormData;->a:Ljava/util/Currency;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1158911
    return-void
.end method
