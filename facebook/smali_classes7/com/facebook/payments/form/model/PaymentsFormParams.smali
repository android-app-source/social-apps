.class public Lcom/facebook/payments/form/model/PaymentsFormParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/form/model/PaymentsFormParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6xK;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

.field public final d:Z

.field public final e:Lcom/facebook/payments/form/model/PaymentsFormData;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1158897
    new-instance v0, LX/6xV;

    invoke-direct {v0}, LX/6xV;-><init>()V

    sput-object v0, Lcom/facebook/payments/form/model/PaymentsFormParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6xW;)V
    .locals 1

    .prologue
    .line 1158889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158890
    iget-object v0, p1, LX/6xW;->a:LX/6xK;

    iput-object v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->a:LX/6xK;

    .line 1158891
    iget-object v0, p1, LX/6xW;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->b:Ljava/lang/String;

    .line 1158892
    iget-object v0, p1, LX/6xW;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1158893
    iget-boolean v0, p1, LX/6xW;->d:Z

    iput-boolean v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->d:Z

    .line 1158894
    iget-object v0, p1, LX/6xW;->e:Lcom/facebook/payments/form/model/PaymentsFormData;

    iput-object v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->e:Lcom/facebook/payments/form/model/PaymentsFormData;

    .line 1158895
    iget-object v0, p1, LX/6xW;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->f:Ljava/lang/String;

    .line 1158896
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1158881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158882
    const-class v0, LX/6xK;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xK;

    iput-object v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->a:LX/6xK;

    .line 1158883
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->b:Ljava/lang/String;

    .line 1158884
    const-class v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1158885
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->d:Z

    .line 1158886
    const-class v0, Lcom/facebook/payments/form/model/PaymentsFormData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/PaymentsFormData;

    iput-object v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->e:Lcom/facebook/payments/form/model/PaymentsFormData;

    .line 1158887
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->f:Ljava/lang/String;

    .line 1158888
    return-void
.end method

.method public static a(LX/6xK;Ljava/lang/String;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)LX/6xW;
    .locals 2

    .prologue
    .line 1158872
    new-instance v0, LX/6xW;

    invoke-direct {v0, p0, p1, p2}, LX/6xW;-><init>(LX/6xK;Ljava/lang/String;Lcom/facebook/payments/decorator/PaymentsDecoratorParams;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1158880
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1158873
    iget-object v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->a:LX/6xK;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1158874
    iget-object v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1158875
    iget-object v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1158876
    iget-boolean v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1158877
    iget-object v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->e:Lcom/facebook/payments/form/model/PaymentsFormData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1158878
    iget-object v0, p0, Lcom/facebook/payments/form/model/PaymentsFormParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1158879
    return-void
.end method
