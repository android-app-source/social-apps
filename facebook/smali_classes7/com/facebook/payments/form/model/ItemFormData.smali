.class public Lcom/facebook/payments/form/model/ItemFormData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/form/model/PaymentsFormData;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/form/model/ItemFormData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:Lcom/facebook/payments/ui/MediaGridTextLayoutParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Landroid/os/Parcelable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/6xN;",
            "Lcom/facebook/payments/form/model/FormFieldAttributes;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1158833
    new-instance v0, LX/6xQ;

    invoke-direct {v0}, LX/6xQ;-><init>()V

    sput-object v0, Lcom/facebook/payments/form/model/ItemFormData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6xR;)V
    .locals 1

    .prologue
    .line 1158806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158807
    iget v0, p1, LX/6xR;->a:I

    iput v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->a:I

    .line 1158808
    iget-object v0, p1, LX/6xR;->d:Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    iput-object v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->b:Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    .line 1158809
    iget-object v0, p1, LX/6xR;->e:Landroid/os/Parcelable;

    iput-object v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->c:Landroid/os/Parcelable;

    .line 1158810
    iget-object v0, p1, LX/6xR;->f:LX/0P1;

    iput-object v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->d:LX/0P1;

    .line 1158811
    iget-object v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->d:LX/0P1;

    sget-object p1, LX/6xN;->TITLE:LX/6xN;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->b:Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1158812
    iget-object v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->d:LX/0P1;

    sget-object p1, LX/6xN;->PRICE:LX/6xN;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/FormFieldAttributes;

    invoke-static {v0}, Lcom/facebook/payments/form/model/ItemFormData;->a(Lcom/facebook/payments/form/model/FormFieldAttributes;)V

    .line 1158813
    iget-object v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->d:LX/0P1;

    sget-object p1, LX/6xN;->SUBTITLE:LX/6xN;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/FormFieldAttributes;

    invoke-static {v0}, Lcom/facebook/payments/form/model/ItemFormData;->a(Lcom/facebook/payments/form/model/FormFieldAttributes;)V

    .line 1158814
    return-void

    .line 1158815
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1158827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158828
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->a:I

    .line 1158829
    const-class v0, Lcom/facebook/payments/ui/MediaGridTextLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    iput-object v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->b:Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    .line 1158830
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->c:Landroid/os/Parcelable;

    .line 1158831
    invoke-static {p1}, LX/46R;->h(Landroid/os/Parcel;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->d:LX/0P1;

    .line 1158832
    return-void
.end method

.method public static a(Lcom/facebook/payments/form/model/FormFieldAttributes;)V
    .locals 2
    .param p0    # Lcom/facebook/payments/form/model/FormFieldAttributes;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1158823
    if-eqz p0, :cond_0

    .line 1158824
    iget-object v0, p0, Lcom/facebook/payments/form/model/FormFieldAttributes;->d:LX/6xe;

    sget-object v1, LX/6xe;->HIDDEN:LX/6xe;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1158825
    :cond_0
    return-void

    .line 1158826
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newBuilder()LX/6xR;
    .locals 2

    .prologue
    .line 1158822
    new-instance v0, LX/6xR;

    invoke-direct {v0}, LX/6xR;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1158821
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1158816
    iget v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1158817
    iget-object v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->b:Lcom/facebook/payments/ui/MediaGridTextLayoutParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1158818
    iget-object v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->c:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1158819
    iget-object v0, p0, Lcom/facebook/payments/form/model/ItemFormData;->d:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1158820
    return-void
.end method
