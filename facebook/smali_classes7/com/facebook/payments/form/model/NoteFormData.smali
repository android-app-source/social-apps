.class public Lcom/facebook/payments/form/model/NoteFormData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/payments/form/model/PaymentsFormData;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/form/model/NoteFormData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/form/model/FormFieldAttributes;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1158859
    new-instance v0, LX/6xS;

    invoke-direct {v0}, LX/6xS;-><init>()V

    sput-object v0, Lcom/facebook/payments/form/model/NoteFormData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6xT;)V
    .locals 1

    .prologue
    .line 1158856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158857
    iget-object v0, p1, LX/6xT;->b:Lcom/facebook/payments/form/model/FormFieldAttributes;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/FormFieldAttributes;

    iput-object v0, p0, Lcom/facebook/payments/form/model/NoteFormData;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 1158858
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1158853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158854
    const-class v0, Lcom/facebook/payments/form/model/FormFieldAttributes;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/FormFieldAttributes;

    iput-object v0, p0, Lcom/facebook/payments/form/model/NoteFormData;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 1158855
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1158860
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1158846
    if-ne p0, p1, :cond_1

    .line 1158847
    :cond_0
    :goto_0
    return v0

    .line 1158848
    :cond_1
    instance-of v2, p1, Lcom/facebook/payments/form/model/NoteFormData;

    if-nez v2, :cond_2

    move v0, v1

    .line 1158849
    goto :goto_0

    .line 1158850
    :cond_2
    check-cast p1, Lcom/facebook/payments/form/model/NoteFormData;

    .line 1158851
    iget-object v2, p0, Lcom/facebook/payments/form/model/NoteFormData;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    iget-object v3, p1, Lcom/facebook/payments/form/model/NoteFormData;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1158852
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1158845
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/payments/form/model/NoteFormData;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1158843
    iget-object v0, p0, Lcom/facebook/payments/form/model/NoteFormData;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1158844
    return-void
.end method
