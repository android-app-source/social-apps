.class public Lcom/facebook/payments/form/model/AmountFormData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/payments/form/model/PaymentsFormData;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/form/model/AmountFormData;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:LX/6xJ;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/payments/form/model/FormFieldAttributes;

.field public final d:Lcom/facebook/payments/currency/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lcom/facebook/payments/currency/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1158681
    new-instance v0, LX/6xG;

    invoke-direct {v0}, LX/6xG;-><init>()V

    sput-object v0, Lcom/facebook/payments/form/model/AmountFormData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 1158682
    new-instance v0, LX/6xJ;

    invoke-direct {v0}, LX/6xJ;-><init>()V

    sput-object v0, Lcom/facebook/payments/form/model/AmountFormData;->a:LX/6xJ;

    return-void
.end method

.method public constructor <init>(LX/6xH;)V
    .locals 2

    .prologue
    .line 1158665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158666
    iget-object v0, p1, LX/6xH;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->b:Ljava/lang/String;

    .line 1158667
    iget-object v0, p1, LX/6xH;->c:Lcom/facebook/payments/form/model/FormFieldAttributes;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/FormFieldAttributes;

    iput-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->c:Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 1158668
    iget-object v0, p1, LX/6xH;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1158669
    iget-object v0, p1, LX/6xH;->e:Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->e:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1158670
    iget-object v0, p1, LX/6xH;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->f:Ljava/lang/String;

    .line 1158671
    iget-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1158672
    iget-object v1, p0, Lcom/facebook/payments/form/model/AmountFormData;->e:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v1, v1

    .line 1158673
    iget-object p1, p0, Lcom/facebook/payments/form/model/AmountFormData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object p1, p1

    .line 1158674
    if-eqz v1, :cond_0

    .line 1158675
    iget-object p0, v1, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v1, p0

    .line 1158676
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1158677
    :cond_0
    if-eqz p1, :cond_1

    .line 1158678
    iget-object v1, p1, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1158679
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1158680
    :cond_1
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1158652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158653
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->b:Ljava/lang/String;

    .line 1158654
    const-class v0, Lcom/facebook/payments/form/model/FormFieldAttributes;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/FormFieldAttributes;

    iput-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->c:Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 1158655
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1158656
    iput-object v1, p0, Lcom/facebook/payments/form/model/AmountFormData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1158657
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1158658
    iput-object v1, p0, Lcom/facebook/payments/form/model/AmountFormData;->e:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1158659
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1158660
    iput-object v1, p0, Lcom/facebook/payments/form/model/AmountFormData;->f:Ljava/lang/String;

    .line 1158661
    :goto_2
    return-void

    .line 1158662
    :cond_0
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    goto :goto_0

    .line 1158663
    :cond_1
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->e:Lcom/facebook/payments/currency/CurrencyAmount;

    goto :goto_1

    .line 1158664
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->f:Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1158651
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1158620
    if-ne p0, p1, :cond_1

    .line 1158621
    :cond_0
    :goto_0
    return v0

    .line 1158622
    :cond_1
    instance-of v2, p1, Lcom/facebook/payments/form/model/AmountFormData;

    if-nez v2, :cond_2

    move v0, v1

    .line 1158623
    goto :goto_0

    .line 1158624
    :cond_2
    check-cast p1, Lcom/facebook/payments/form/model/AmountFormData;

    .line 1158625
    iget-object v2, p0, Lcom/facebook/payments/form/model/AmountFormData;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/payments/form/model/AmountFormData;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1158626
    goto :goto_0

    .line 1158627
    :cond_3
    iget-object v2, p0, Lcom/facebook/payments/form/model/AmountFormData;->c:Lcom/facebook/payments/form/model/FormFieldAttributes;

    iget-object v3, p1, Lcom/facebook/payments/form/model/AmountFormData;->c:Lcom/facebook/payments/form/model/FormFieldAttributes;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1158628
    goto :goto_0

    .line 1158629
    :cond_4
    iget-object v2, p0, Lcom/facebook/payments/form/model/AmountFormData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v3, p1, Lcom/facebook/payments/form/model/AmountFormData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1158630
    goto :goto_0

    .line 1158631
    :cond_5
    iget-object v2, p0, Lcom/facebook/payments/form/model/AmountFormData;->e:Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v3, p1, Lcom/facebook/payments/form/model/AmountFormData;->e:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1158632
    goto :goto_0

    .line 1158633
    :cond_6
    iget-object v2, p0, Lcom/facebook/payments/form/model/AmountFormData;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/payments/form/model/AmountFormData;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1158634
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1158650
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/payments/form/model/AmountFormData;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/payments/form/model/AmountFormData;->c:Lcom/facebook/payments/form/model/FormFieldAttributes;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/payments/form/model/AmountFormData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/payments/form/model/AmountFormData;->e:Lcom/facebook/payments/currency/CurrencyAmount;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/payments/form/model/AmountFormData;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1158635
    iget-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1158636
    iget-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->c:Lcom/facebook/payments/form/model/FormFieldAttributes;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1158637
    iget-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    if-nez v0, :cond_0

    .line 1158638
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1158639
    :goto_0
    iget-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->e:Lcom/facebook/payments/currency/CurrencyAmount;

    if-nez v0, :cond_1

    .line 1158640
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1158641
    :goto_1
    iget-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1158642
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1158643
    :goto_2
    return-void

    .line 1158644
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1158645
    iget-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    .line 1158646
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1158647
    iget-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->e:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_1

    .line 1158648
    :cond_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1158649
    iget-object v0, p0, Lcom/facebook/payments/form/model/AmountFormData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2
.end method
