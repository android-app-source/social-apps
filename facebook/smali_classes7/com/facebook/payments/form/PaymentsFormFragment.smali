.class public Lcom/facebook/payments/form/PaymentsFormFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:LX/6x6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:LX/6qh;

.field private c:Landroid/content/Context;

.field public d:LX/0h5;

.field public e:Lcom/facebook/payments/form/model/PaymentsFormParams;

.field public f:LX/6wy;

.field private final g:LX/6x9;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1158542
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1158543
    new-instance v0, LX/6x8;

    invoke-direct {v0, p0}, LX/6x8;-><init>(Lcom/facebook/payments/form/PaymentsFormFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->b:LX/6qh;

    .line 1158544
    new-instance v0, LX/6x9;

    invoke-direct {v0, p0}, LX/6x9;-><init>(Lcom/facebook/payments/form/PaymentsFormFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->g:LX/6x9;

    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p1, Lcom/facebook/payments/form/PaymentsFormFragment;

    new-instance p2, LX/6x6;

    invoke-static {v0}, LX/6wz;->a(LX/0QB;)LX/6wz;

    move-result-object v1

    check-cast v1, LX/6wz;

    invoke-static {v0}, LX/6x2;->a(LX/0QB;)LX/6x2;

    move-result-object v2

    check-cast v2, LX/6x2;

    invoke-static {v0}, LX/6x4;->a(LX/0QB;)LX/6x4;

    move-result-object v3

    check-cast v3, LX/6x4;

    invoke-static {v0}, LX/6xF;->a(LX/0QB;)LX/6xF;

    move-result-object p0

    check-cast p0, LX/6xF;

    invoke-direct {p2, v1, v2, v3, p0}, LX/6x6;-><init>(LX/6wz;LX/6x2;LX/6x4;LX/6xF;)V

    move-object v0, p2

    check-cast v0, LX/6x6;

    iput-object v0, p1, Lcom/facebook/payments/form/PaymentsFormFragment;->a:LX/6x6;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/payments/form/PaymentsFormFragment;LX/73T;)V
    .locals 3

    .prologue
    .line 1158532
    sget-object v0, LX/6xC;->a:[I

    .line 1158533
    iget-object v1, p1, LX/73T;->a:LX/73S;

    move-object v1, v1

    .line 1158534
    invoke-virtual {v1}, LX/73S;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1158535
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1158536
    :pswitch_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1158537
    if-eqz v0, :cond_0

    .line 1158538
    const-string v1, "extra_activity_result_data"

    invoke-virtual {p1, v1}, LX/73T;->a(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 1158539
    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1158540
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1158541
    :cond_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/payments/form/PaymentsFormFragment;Z)V
    .locals 2

    .prologue
    .line 1158522
    iget-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->e:Lcom/facebook/payments/form/model/PaymentsFormParams;

    iget-boolean v0, v0, Lcom/facebook/payments/form/model/PaymentsFormParams;->d:Z

    if-eqz v0, :cond_0

    .line 1158523
    iget-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->e:Lcom/facebook/payments/form/model/PaymentsFormParams;

    iget-object v0, v0, Lcom/facebook/payments/form/model/PaymentsFormParams;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f081e43

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1158524
    :goto_0
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    .line 1158525
    iput-object v0, v1, LX/108;->g:Ljava/lang/String;

    .line 1158526
    move-object v0, v1

    .line 1158527
    iput-boolean p1, v0, LX/108;->d:Z

    .line 1158528
    move-object v0, v0

    .line 1158529
    iget-object v1, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->d:LX/0h5;

    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1158530
    :cond_0
    return-void

    .line 1158531
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->e:Lcom/facebook/payments/form/model/PaymentsFormParams;

    iget-object v0, v0, Lcom/facebook/payments/form/model/PaymentsFormParams;->f:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 1158521
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1158497
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1158498
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v2, 0x7f0e0326

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->c:Landroid/content/Context;

    .line 1158499
    const-class v0, Lcom/facebook/payments/form/PaymentsFormFragment;

    iget-object v1, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->c:Landroid/content/Context;

    invoke-static {v0, p0, v1}, Lcom/facebook/payments/form/PaymentsFormFragment;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1158500
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1158501
    const-string v1, "extra_payments_form_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/PaymentsFormParams;

    iput-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->e:Lcom/facebook/payments/form/model/PaymentsFormParams;

    .line 1158502
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5748e76a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1158520
    iget-object v1, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->c:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030f1b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x139e7b2f    # 4.0006276E-27f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1158503
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1158504
    const v0, 0x7f0d123b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    .line 1158505
    new-instance v1, LX/6xD;

    invoke-direct {v1, v0}, LX/6xD;-><init>(Lcom/facebook/widget/CustomLinearLayout;)V

    .line 1158506
    const v0, 0x7f0d00bb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 1158507
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1158508
    check-cast v2, Landroid/view/ViewGroup;

    new-instance v3, LX/6xA;

    invoke-direct {v3, p0}, LX/6xA;-><init>(Lcom/facebook/payments/form/PaymentsFormFragment;)V

    iget-object p1, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->e:Lcom/facebook/payments/form/model/PaymentsFormParams;

    iget-object p1, p1, Lcom/facebook/payments/form/model/PaymentsFormParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object p1, p1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    iget-object p2, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->e:Lcom/facebook/payments/form/model/PaymentsFormParams;

    iget-object p2, p2, Lcom/facebook/payments/form/model/PaymentsFormParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object p2, p2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-virtual {p2}, LX/6ws;->getTitleBarNavIconStyle()LX/73h;

    move-result-object p2

    invoke-virtual {v0, v2, v3, p1, p2}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 1158509
    iget-object v2, v0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    move-object v0, v2

    .line 1158510
    iput-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->d:LX/0h5;

    .line 1158511
    iget-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->d:LX/0h5;

    iget-object v2, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->e:Lcom/facebook/payments/form/model/PaymentsFormParams;

    iget-object v2, v2, Lcom/facebook/payments/form/model/PaymentsFormParams;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1158512
    iget-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->d:LX/0h5;

    new-instance v2, LX/6xB;

    invoke-direct {v2, p0}, LX/6xB;-><init>(Lcom/facebook/payments/form/PaymentsFormFragment;)V

    invoke-interface {v0, v2}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1158513
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/payments/form/PaymentsFormFragment;->a$redex0(Lcom/facebook/payments/form/PaymentsFormFragment;Z)V

    .line 1158514
    iget-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->a:LX/6x6;

    iget-object v2, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->e:Lcom/facebook/payments/form/model/PaymentsFormParams;

    iget-object v2, v2, Lcom/facebook/payments/form/model/PaymentsFormParams;->a:LX/6xK;

    invoke-virtual {v0, v2}, LX/6x6;->a(LX/6xK;)LX/6wy;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->f:LX/6wy;

    .line 1158515
    iget-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->f:LX/6wy;

    iget-object v2, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->g:LX/6x9;

    invoke-interface {v0, v2}, LX/6wy;->a(LX/6x9;)V

    .line 1158516
    iget-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->f:LX/6wy;

    iget-object v2, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->b:LX/6qh;

    invoke-interface {v0, v2}, LX/6wy;->a(LX/6qh;)V

    .line 1158517
    iget-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->f:LX/6wy;

    iget-object v2, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->e:Lcom/facebook/payments/form/model/PaymentsFormParams;

    iget-object v2, v2, Lcom/facebook/payments/form/model/PaymentsFormParams;->e:Lcom/facebook/payments/form/model/PaymentsFormData;

    invoke-interface {v0, v1, v2}, LX/6wy;->a(LX/6xD;Lcom/facebook/payments/form/model/PaymentsFormData;)V

    .line 1158518
    iget-object v0, p0, Lcom/facebook/payments/form/PaymentsFormFragment;->f:LX/6wy;

    invoke-interface {v0}, LX/6wy;->b()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/payments/form/PaymentsFormFragment;->a$redex0(Lcom/facebook/payments/form/PaymentsFormFragment;Z)V

    .line 1158519
    return-void
.end method
