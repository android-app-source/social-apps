.class public Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1156440
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1156441
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1156442
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1156443
    const-string v1, "extra_contact_info_form_params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1156444
    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 1156445
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "contact_info_form_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1156446
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d03c5

    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;->q:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156447
    new-instance v3, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    invoke-direct {v3}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;-><init>()V

    .line 1156448
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1156449
    const-string p0, "extra_contact_info_form_params"

    invoke-virtual {v4, p0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1156450
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1156451
    move-object v2, v3

    .line 1156452
    const-string v3, "contact_info_form_fragment_tag"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1156453
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;

    invoke-static {v0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v0

    check-cast v0, LX/6wr;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;->p:LX/6wr;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1156454
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1156455
    const v0, 0x7f030361

    invoke-virtual {p0, v0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;->setContentView(I)V

    .line 1156456
    if-nez p1, :cond_0

    .line 1156457
    invoke-direct {p0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;->a()V

    .line 1156458
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;->q:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156459
    move-object v0, v0

    .line 1156460
    iget-object v0, v0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->a(Landroid/app/Activity;LX/6ws;)V

    .line 1156461
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1156462
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 1156463
    invoke-static {p0, p0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1156464
    invoke-virtual {p0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_contact_info_form_params"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;->q:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156465
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;->p:LX/6wr;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;->q:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156466
    move-object v1, v1

    .line 1156467
    iget-object v1, v1, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v1, v1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-virtual {v0, p0, v1}, LX/6wr;->b(Landroid/app/Activity;LX/73i;)V

    .line 1156468
    return-void
.end method

.method public final finish()V
    .locals 1

    .prologue
    .line 1156469
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1156470
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormActivity;->q:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156471
    move-object v0, v0

    .line 1156472
    iget-object v0, v0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->b(Landroid/app/Activity;LX/6ws;)V

    .line 1156473
    return-void
.end method
