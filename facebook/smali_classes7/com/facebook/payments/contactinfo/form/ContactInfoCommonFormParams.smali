.class public Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6vY;

.field public final b:Lcom/facebook/payments/contactinfo/model/ContactInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

.field public final d:I

.field public final e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1156399
    new-instance v0, LX/6v1;

    invoke-direct {v0}, LX/6v1;-><init>()V

    sput-object v0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6v2;)V
    .locals 2

    .prologue
    .line 1156400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156401
    iget-object v0, p1, LX/6v2;->a:LX/6vY;

    move-object v0, v0

    .line 1156402
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6vY;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->a:LX/6vY;

    .line 1156403
    iget-object v0, p1, LX/6v2;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    move-object v0, v0

    .line 1156404
    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1156405
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    if-eqz v0, :cond_0

    .line 1156406
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v0}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->d()LX/6vb;

    move-result-object v0

    invoke-virtual {v0}, LX/6vb;->getContactInfoFormStyle()LX/6vY;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->a:LX/6vY;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1156407
    :cond_0
    iget-object v0, p1, LX/6v2;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-object v0, v0

    .line 1156408
    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1156409
    iget v0, p1, LX/6v2;->d:I

    move v0, v0

    .line 1156410
    iput v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->d:I

    .line 1156411
    iget-boolean v0, p1, LX/6v2;->e:Z

    move v0, v0

    .line 1156412
    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->e:Z

    .line 1156413
    return-void

    .line 1156414
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1156415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156416
    const-class v0, LX/6vY;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6vY;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->a:LX/6vY;

    .line 1156417
    const-class v0, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1156418
    const-class v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1156419
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->d:I

    .line 1156420
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->e:Z

    .line 1156421
    return-void
.end method

.method public static newBuilder()LX/6v2;
    .locals 1

    .prologue
    .line 1156422
    new-instance v0, LX/6v2;

    invoke-direct {v0}, LX/6v2;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1156423
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1156424
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->a:LX/6vY;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1156425
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1156426
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1156427
    iget v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1156428
    iget-boolean v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1156429
    return-void
.end method
