.class public Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/6vT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

.field public c:LX/6v4;

.field public d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public e:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

.field public f:LX/6wm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1156799
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1156800
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1156742
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1156743
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    invoke-static {v0}, LX/6vT;->a(LX/0QB;)LX/6vT;

    move-result-object v0

    check-cast v0, LX/6vT;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->a:LX/6vT;

    .line 1156744
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1156797
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setEnabled(Z)V

    .line 1156798
    return-void
.end method

.method public final b()Z
    .locals 7

    .prologue
    .line 1156801
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->e:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->e()V

    .line 1156802
    invoke-virtual {p0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1156803
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->c:LX/6v4;

    .line 1156804
    iget-object v1, v0, LX/6v4;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    iget-object v1, v1, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v1}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1156805
    :goto_0
    const/4 v0, 0x1

    .line 1156806
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1156807
    :cond_1
    iget-object v1, v0, LX/6v4;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    invoke-static {v1}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->v(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V

    .line 1156808
    iget-object v1, v0, LX/6v4;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    iget-object v2, v0, LX/6v4;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    iget-object v2, v2, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->l:LX/6vJ;

    iget-object v3, v0, LX/6v4;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    iget-object v3, v3, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    iget-object v4, v0, LX/6v4;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    invoke-static {v4}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->l(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    move-result-object v4

    const/4 v6, 0x0

    .line 1156809
    move-object v5, v3

    .line 1156810
    iget-object v5, v5, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    if-nez v5, :cond_2

    .line 1156811
    new-instance v5, LX/6wd;

    invoke-direct {v5}, LX/6wd;-><init>()V

    move-object v5, v5

    .line 1156812
    iput-object v4, v5, LX/6wd;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    .line 1156813
    move-object v5, v5

    .line 1156814
    invoke-interface {v4}, Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;->a()Z

    move-result v6

    .line 1156815
    iput-boolean v6, v5, LX/6wd;->a:Z

    .line 1156816
    move-object v5, v5

    .line 1156817
    new-instance v6, Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;

    invoke-direct {v6, v5}, Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;-><init>(LX/6wd;)V

    move-object v5, v6

    .line 1156818
    invoke-virtual {v2, v5}, LX/6vJ;->a(Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 1156819
    new-instance v6, LX/6vU;

    invoke-direct {v6, v2, v3, v4}, LX/6vU;-><init>(LX/6vJ;Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;)V

    iget-object p0, v2, LX/6vJ;->b:Ljava/util/concurrent/Executor;

    invoke-static {v5, v6, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1156820
    move-object v5, v5

    .line 1156821
    :goto_2
    move-object v2, v5

    .line 1156822
    iput-object v2, v1, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1156823
    iget-object v1, v0, LX/6v4;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    iget-object v2, v0, LX/6v4;->a:Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    iget-object v2, v2, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v1, v2}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->a(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V

    goto :goto_0

    :cond_2
    invoke-static {v2, v3, v4, v6, v6}, LX/6vJ;->a(LX/6vJ;Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    goto :goto_2
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1156796
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->e:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->k()Z

    move-result v0

    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x33537fa4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1156745
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1156746
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1156747
    const-string v2, "extra_contact_info_form_params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->b:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156748
    new-instance v0, LX/6vC;

    invoke-direct {v0, p0}, LX/6vC;-><init>(Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;)V

    .line 1156749
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1156750
    sget-object v0, LX/6vG;->a:[I

    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->b:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156751
    move-object v2, v2

    .line 1156752
    iget-object v2, v2, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->a:LX/6vY;

    invoke-virtual {v2}, LX/6vY;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1156753
    :goto_0
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->a:LX/6vT;

    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->b:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156754
    move-object v2, v2

    .line 1156755
    iget-object v2, v2, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->a:LX/6vY;

    .line 1156756
    iget-object v4, v0, LX/6vT;->a:LX/0P1;

    invoke-virtual {v4, v2}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1156757
    :goto_1
    iget-object v4, v0, LX/6vT;->a:LX/0P1;

    invoke-virtual {v4, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/6vH;

    iget-object v4, v4, LX/6vH;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/6wm;

    move-object v0, v4

    .line 1156758
    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->f:LX/6wm;

    .line 1156759
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v2, "contact_info_input_controller_fragment_tag"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->e:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1156760
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->e:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    if-nez v0, :cond_0

    .line 1156761
    new-instance v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-direct {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->e:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1156762
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->e:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const-string v4, "contact_info_input_controller_fragment_tag"

    invoke-virtual {v0, v2, v4}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1156763
    :cond_0
    new-instance v0, LX/6vD;

    invoke-direct {v0, p0}, LX/6vD;-><init>(Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;)V

    .line 1156764
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->e:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-static {}, LX/473;->a()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;I)V

    .line 1156765
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->e:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v4, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->f:LX/6wm;

    .line 1156766
    iput-object v4, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->c:LX/6wl;

    .line 1156767
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->e:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 1156768
    iput-object v0, v2, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d:Landroid/text/TextWatcher;

    .line 1156769
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->e:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    new-instance v2, LX/6vF;

    invoke-direct {v2, p0}, LX/6vF;-><init>(Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;)V

    .line 1156770
    iput-object v2, v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a:LX/6vE;

    .line 1156771
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->b:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156772
    move-object v0, v0

    .line 1156773
    iget-object v0, v0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1156774
    if-nez v0, :cond_3

    .line 1156775
    :goto_2
    if-eqz p1, :cond_1

    .line 1156776
    const-string v0, "extra_contact_info_edit_text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1156777
    if-eqz v0, :cond_1

    .line 1156778
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    .line 1156779
    :cond_1
    const/16 v0, 0x2b

    const v2, -0x1286511a

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1156780
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputType(I)V

    goto/16 :goto_0

    .line 1156781
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputType(I)V

    goto/16 :goto_0

    .line 1156782
    :cond_2
    sget-object v2, LX/6vY;->SIMPLE:LX/6vY;

    goto/16 :goto_1

    .line 1156783
    :cond_3
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->b:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156784
    move-object v2, v2

    .line 1156785
    iget-object v2, v2, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->a:LX/6vY;

    .line 1156786
    sget-object v4, LX/6vG;->a:[I

    invoke-virtual {v2}, LX/6vY;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_1

    goto :goto_2

    .line 1156787
    :pswitch_2
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    check-cast v0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;

    .line 1156788
    iget-object v4, v0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->b:Ljava/lang/String;

    move-object v0, v4

    .line 1156789
    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1156790
    :pswitch_3
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    check-cast v0, Lcom/facebook/payments/contactinfo/model/NameContactInfo;

    .line 1156791
    iget-object v4, v0, Lcom/facebook/payments/contactinfo/model/NameContactInfo;->a:Ljava/lang/String;

    move-object v0, v4

    .line 1156792
    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1156793
    :pswitch_4
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    check-cast v0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;

    .line 1156794
    iget-object v4, v0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->b:Ljava/lang/String;

    move-object v0, v4

    .line 1156795
    invoke-virtual {v2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputText(Ljava/lang/CharSequence;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1156738
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1156739
    const-string v0, "extra_contact_info_edit_text"

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156740
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1156741
    return-void
.end method
