.class public Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/6vT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/content/Context;

.field public d:Landroid/widget/LinearLayout;

.field public e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public f:LX/73Y;

.field private g:Landroid/widget/ProgressBar;

.field public h:LX/0h5;

.field public i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

.field public j:Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

.field public k:LX/6v3;

.field public l:LX/6vJ;

.field public m:Lcom/google/common/util/concurrent/ListenableFuture;

.field private n:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final o:LX/108;

.field public final p:LX/6v4;

.field public final q:LX/6qh;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1156713
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1156714
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const/4 v1, 0x0

    .line 1156715
    iput-boolean v1, v0, LX/108;->d:Z

    .line 1156716
    move-object v0, v0

    .line 1156717
    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->o:LX/108;

    .line 1156718
    new-instance v0, LX/6v4;

    invoke-direct {v0, p0}, LX/6v4;-><init>(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->p:LX/6v4;

    .line 1156719
    new-instance v0, LX/6v5;

    invoke-direct {v0, p0}, LX/6v5;-><init>(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->q:LX/6qh;

    return-void
.end method

.method public static a(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 2

    .prologue
    .line 1156509
    new-instance v0, LX/6vA;

    invoke-direct {v0, p0}, LX/6vA;-><init>(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->b:Ljava/util/concurrent/Executor;

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1156510
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    invoke-static {p0}, LX/6vT;->a(LX/0QB;)LX/6vT;

    move-result-object v0

    check-cast v0, LX/6vT;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/Executor;

    iput-object v0, p1, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->a:LX/6vT;

    iput-object p0, p1, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->b:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1156511
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1156512
    :goto_0
    return-void

    .line 1156513
    :cond_0
    invoke-static {p0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->v(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V

    .line 1156514
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156515
    move-object v0, v0

    .line 1156516
    iget-object v0, v0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1156517
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1156518
    const-string v1, "extra_mutation"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156519
    new-instance v1, LX/73T;

    sget-object v2, LX/73S;->MUTATION:LX/73S;

    invoke-direct {v1, v2, v0}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    .line 1156520
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->l:LX/6vJ;

    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    const/4 p1, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1156521
    const-string v3, "extra_mutation"

    invoke-virtual {v1, v3, v6}, LX/73T;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1156522
    const-string v4, "make_default_mutation"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1156523
    invoke-static {v0, v2, v6, v5, p1}, LX/6vJ;->a(LX/6vJ;Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 1156524
    :goto_1
    move-object v0, v3

    .line 1156525
    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1156526
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {p0, v0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->a(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V

    goto :goto_0

    .line 1156527
    :cond_1
    const-string v4, "delete_mutation"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1156528
    invoke-static {v0, v2, v6, p1, v5}, LX/6vJ;->a(LX/6vJ;Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_1

    .line 1156529
    :cond_2
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v3}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_1
.end method

.method public static l(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;
    .locals 3

    .prologue
    .line 1156530
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156531
    move-object v0, v0

    .line 1156532
    iget-object v0, v0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->a:LX/6vY;

    .line 1156533
    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156534
    move-object v1, v1

    .line 1156535
    iget-object v1, v1, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1156536
    sget-object v2, LX/6vB;->a:[I

    invoke-virtual {v0}, LX/6vY;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 1156537
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Not supported this style yet!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1156538
    :pswitch_0
    new-instance v0, LX/6vf;

    invoke-direct {v0}, LX/6vf;-><init>()V

    move-object v0, v0

    .line 1156539
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    .line 1156540
    iput-object v2, v0, LX/6vf;->a:Ljava/lang/String;

    .line 1156541
    move-object v2, v0

    .line 1156542
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->b()Z

    move-result v0

    .line 1156543
    :goto_0
    iput-boolean v0, v2, LX/6vf;->b:Z

    .line 1156544
    move-object v0, v2

    .line 1156545
    new-instance v1, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;

    invoke-direct {v1, v0}, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;-><init>(LX/6vf;)V

    move-object v0, v1

    .line 1156546
    :goto_1
    return-object v0

    .line 1156547
    :cond_0
    invoke-direct {p0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->t()Z

    move-result v0

    goto :goto_0

    .line 1156548
    :pswitch_1
    new-instance v0, Lcom/facebook/payments/contactinfo/model/NameContactInfoFormInput;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/payments/contactinfo/model/NameContactInfoFormInput;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 1156549
    :pswitch_2
    new-instance v0, LX/6vl;

    invoke-direct {v0}, LX/6vl;-><init>()V

    move-object v0, v0

    .line 1156550
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    .line 1156551
    iput-object v2, v0, LX/6vl;->a:Ljava/lang/String;

    .line 1156552
    move-object v2, v0

    .line 1156553
    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->b()Z

    move-result v0

    .line 1156554
    :goto_2
    iput-boolean v0, v2, LX/6vl;->b:Z

    .line 1156555
    move-object v0, v2

    .line 1156556
    new-instance v1, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;

    invoke-direct {v1, v0}, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;-><init>(LX/6vl;)V

    move-object v0, v1

    .line 1156557
    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->t()Z

    move-result v0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static r(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V
    .locals 3

    .prologue
    .line 1156558
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->k:LX/6v3;

    invoke-interface {v1}, LX/6v3;->h()Ljava/lang/String;

    move-result-object v1

    .line 1156559
    iget-object v2, v0, LX/73Y;->e:Lcom/facebook/payments/ui/CallToActionSummaryView;

    invoke-virtual {v2, v1}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->setText(Ljava/lang/String;)V

    .line 1156560
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/73Y;->setVisibilityOfDefaultActionSummary(I)V

    .line 1156561
    return-void
.end method

.method public static s(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V
    .locals 2

    .prologue
    .line 1156562
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/73Y;->setVisibilityOfDefaultActionSummary(I)V

    .line 1156563
    return-void
.end method

.method private t()Z
    .locals 1

    .prologue
    .line 1156564
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    if-eqz v0, :cond_0

    .line 1156565
    const v0, 0x7f0d24af

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    .line 1156566
    invoke-virtual {v0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    .line 1156567
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static v(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1156568
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1156569
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->d:Landroid/widget/LinearLayout;

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1156570
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->j:Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->a(Z)V

    .line 1156571
    return-void
.end method

.method public static w(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V
    .locals 2

    .prologue
    .line 1156572
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->g:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1156573
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->d:Landroid/widget/LinearLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1156574
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->j:Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->a(Z)V

    .line 1156575
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1156576
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1156577
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v2, 0x7f0e0326

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->c:Landroid/content/Context;

    .line 1156578
    const-class v0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->c:Landroid/content/Context;

    invoke-static {v0, p0, v1}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1156579
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1156580
    const-string v1, "extra_contact_info_form_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156581
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1156582
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->a:LX/6vT;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156583
    move-object v1, v1

    .line 1156584
    iget-object v1, v1, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->a:LX/6vY;

    .line 1156585
    iget-object v2, v0, LX/6vT;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1156586
    :goto_0
    iget-object v2, v0, LX/6vT;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6vH;

    iget-object v2, v2, LX/6vH;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6v3;

    move-object v0, v2

    .line 1156587
    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->k:LX/6v3;

    .line 1156588
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->a:LX/6vT;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156589
    move-object v1, v1

    .line 1156590
    iget-object v1, v1, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->a:LX/6vY;

    .line 1156591
    iget-object v2, v0, LX/6vT;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1156592
    :goto_1
    iget-object v2, v0, LX/6vT;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6vH;

    iget-object v2, v2, LX/6vH;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6vJ;

    move-object v0, v2

    .line 1156593
    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->l:LX/6vJ;

    .line 1156594
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->l:LX/6vJ;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->q:LX/6qh;

    .line 1156595
    iput-object v1, v0, LX/6vJ;->c:LX/6qh;

    .line 1156596
    return-void

    .line 1156597
    :cond_0
    sget-object v1, LX/6vY;->SIMPLE:LX/6vY;

    goto :goto_0

    .line 1156598
    :cond_1
    sget-object v1, LX/6vY;->SIMPLE:LX/6vY;

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0xf638244

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1156599
    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->c:Landroid/content/Context;

    invoke-virtual {p1, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030362

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x47b93730

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/16 v0, 0x2a

    const v1, 0x459f53a0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1156600
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1156601
    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 1156602
    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1156603
    iput-object v4, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1156604
    :cond_0
    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_1

    .line 1156605
    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1156606
    iput-object v4, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->n:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1156607
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x66f80bf2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1156608
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1156609
    const v0, 0x7f0d08a0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->d:Landroid/widget/LinearLayout;

    .line 1156610
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->g:Landroid/widget/ProgressBar;

    .line 1156611
    const v0, 0x7f0d0b1e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1156612
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-static {}, LX/473;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setId(I)V

    .line 1156613
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->k:LX/6v3;

    invoke-interface {v1}, LX/6v3;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1156614
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "contact_info_form_input_controller_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->j:Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    .line 1156615
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->j:Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    if-nez v0, :cond_0

    .line 1156616
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156617
    new-instance v1, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    invoke-direct {v1}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;-><init>()V

    .line 1156618
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 1156619
    const-string p2, "extra_contact_info_form_params"

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1156620
    invoke-virtual {v1, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1156621
    move-object v0, v1

    .line 1156622
    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->j:Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    .line 1156623
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->j:Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    const-string p1, "contact_info_form_input_controller_fragment_tag"

    invoke-virtual {v0, v1, p1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1156624
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->j:Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->p:LX/6v4;

    .line 1156625
    iput-object v1, v0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->c:LX/6v4;

    .line 1156626
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->j:Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1156627
    iput-object v1, v0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormInputControllerFragment;->d:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 1156628
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1156629
    const v1, 0x7f0d00bb

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 1156630
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156631
    move-object v2, v2

    .line 1156632
    iget-object p1, v2, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1156633
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1156634
    check-cast v2, Landroid/view/ViewGroup;

    new-instance p2, LX/6v6;

    invoke-direct {p2, p0, v0}, LX/6v6;-><init>(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;Landroid/app/Activity;)V

    iget-object v0, p1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    iget-object p1, p1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-virtual {p1}, LX/6ws;->getTitleBarNavIconStyle()LX/73h;

    move-result-object p1

    invoke-virtual {v1, v2, p2, v0, p1}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 1156635
    iget-object v0, v1, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    move-object v0, v0

    .line 1156636
    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->h:LX/0h5;

    .line 1156637
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156638
    move-object v0, v0

    .line 1156639
    iget-object v0, v0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    if-nez v0, :cond_3

    .line 1156640
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->h:LX/0h5;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->k:LX/6v3;

    invoke-interface {v1}, LX/6v3;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1156641
    :goto_0
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->o:LX/108;

    const v1, 0x7f081e2d

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1156642
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 1156643
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->h:LX/0h5;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->o:LX/108;

    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1156644
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->h:LX/0h5;

    new-instance v1, LX/6v7;

    invoke-direct {v1, p0}, LX/6v7;-><init>(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1156645
    new-instance v0, LX/73Y;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/73Y;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    .line 1156646
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->k:LX/6v3;

    invoke-interface {v1}, LX/6v3;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/73Y;->setSecurityInfo(Ljava/lang/String;)V

    .line 1156647
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b13aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/73Y;->setLeftAndRightPaddingForChildViews(I)V

    .line 1156648
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156649
    move-object v0, v0

    .line 1156650
    iget-boolean v0, v0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->e:Z

    if-eqz v0, :cond_2

    .line 1156651
    const/4 v0, 0x0

    .line 1156652
    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156653
    move-object v1, v1

    .line 1156654
    iget-object v1, v1, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156655
    move-object v1, v1

    .line 1156656
    iget v1, v1, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->d:I

    if-lez v1, :cond_4

    .line 1156657
    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->k:LX/6v3;

    invoke-interface {v2}, LX/6v3;->e()Ljava/lang/String;

    move-result-object v2

    .line 1156658
    iget-object p1, v1, LX/73Y;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {p1, v2}, Lcom/facebook/widget/SwitchCompat;->setText(Ljava/lang/CharSequence;)V

    .line 1156659
    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    invoke-virtual {v1, v0}, LX/73Y;->setVisibilityOfMakeDefaultSwitch(I)V

    .line 1156660
    invoke-static {p0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->r(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V

    .line 1156661
    const/4 v0, 0x1

    .line 1156662
    :goto_1
    move v0, v0

    .line 1156663
    if-nez v0, :cond_1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1156664
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156665
    move-object v2, v2

    .line 1156666
    iget-object v2, v2, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156667
    move-object v2, v2

    .line 1156668
    iget-object v2, v2, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v2}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->b()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156669
    move-object v2, v2

    .line 1156670
    iget v2, v2, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->d:I

    if-le v2, v0, :cond_5

    .line 1156671
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    iget-object p1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->k:LX/6v3;

    invoke-interface {p1}, LX/6v3;->f()Ljava/lang/String;

    move-result-object p1

    .line 1156672
    iget-object p2, v2, LX/73Y;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1156673
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    new-instance p1, LX/6v8;

    invoke-direct {p1, p0}, LX/6v8;-><init>(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V

    invoke-virtual {v2, p1}, LX/73Y;->setOnClickListenerForMakeDefaultButton(Landroid/view/View$OnClickListener;)V

    .line 1156674
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    invoke-virtual {v2, v1}, LX/73Y;->setVisibilityOfMakeDefaultButton(I)V

    .line 1156675
    invoke-static {p0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->r(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V

    .line 1156676
    :goto_2
    move v0, v0

    .line 1156677
    if-nez v0, :cond_1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1156678
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156679
    move-object v2, v2

    .line 1156680
    iget-object v2, v2, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156681
    move-object v2, v2

    .line 1156682
    iget-object v2, v2, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v2}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->b()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156683
    move-object v2, v2

    .line 1156684
    iget v2, v2, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->d:I

    if-le v2, v0, :cond_6

    .line 1156685
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    iget-object p1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->k:LX/6v3;

    invoke-interface {p1}, LX/6v3;->g()Ljava/lang/String;

    move-result-object p1

    .line 1156686
    iget-object p2, v2, LX/73Y;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1156687
    iget-object v2, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    invoke-virtual {v2, v1}, LX/73Y;->setVisibilityOfDefaultInfoView(I)V

    .line 1156688
    invoke-static {p0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->r(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V

    .line 1156689
    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156690
    move-object v0, v0

    .line 1156691
    iget-object v0, v0, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    .line 1156692
    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->i:Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    .line 1156693
    move-object v1, v1

    .line 1156694
    iget-object v2, v1, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-interface {v2}, Lcom/facebook/payments/contactinfo/model/ContactInfo;->d()LX/6vb;

    move-result-object v2

    sget-object p1, LX/6vb;->EMAIL:LX/6vb;

    if-ne v2, p1, :cond_8

    iget v1, v1, Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;->d:I

    if-ne v1, v0, :cond_8

    .line 1156695
    :goto_4
    move v0, v0

    .line 1156696
    if-nez v0, :cond_7

    .line 1156697
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->k:LX/6v3;

    invoke-interface {v1}, LX/6v3;->i()Ljava/lang/String;

    move-result-object v1

    .line 1156698
    iget-object v2, v0, LX/73Y;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1156699
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    new-instance v1, LX/6v9;

    invoke-direct {v1, p0}, LX/6v9;-><init>(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V

    invoke-virtual {v0, v1}, LX/73Y;->setOnClickListenerForDeleteButton(Landroid/view/View$OnClickListener;)V

    .line 1156700
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/73Y;->setVisibilityOfDeleteButton(I)V

    .line 1156701
    :cond_2
    :goto_5
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->d:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1156702
    return-void

    .line 1156703
    :cond_3
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->h:LX/0h5;

    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->k:LX/6v3;

    invoke-interface {v1}, LX/6v3;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1156704
    :cond_4
    iget-object v1, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, LX/73Y;->setVisibilityOfMakeDefaultSwitch(I)V

    .line 1156705
    invoke-static {p0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->s(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V

    goto/16 :goto_1

    .line 1156706
    :cond_5
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, LX/73Y;->setVisibilityOfMakeDefaultButton(I)V

    .line 1156707
    invoke-static {p0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->s(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V

    move v0, v1

    .line 1156708
    goto/16 :goto_2

    .line 1156709
    :cond_6
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, LX/73Y;->setVisibilityOfDefaultInfoView(I)V

    .line 1156710
    invoke-static {p0}, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->s(Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;)V

    .line 1156711
    goto :goto_3

    .line 1156712
    :cond_7
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/form/ContactInfoFormFragment;->f:LX/73Y;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/73Y;->setVisibilityOfDeleteButton(I)V

    goto :goto_5

    :cond_8
    const/4 v0, 0x0

    goto :goto_4
.end method
