.class public Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResultDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mContactInfoId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1157923
    const-class v0, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1157937
    new-instance v0, LX/6we;

    invoke-direct {v0}, LX/6we;-><init>()V

    sput-object v0, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1157934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157935
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;->mContactInfoId:Ljava/lang/String;

    .line 1157936
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1157931
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157932
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;->mContactInfoId:Ljava/lang/String;

    .line 1157933
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1157928
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157929
    iput-object p1, p0, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;->mContactInfoId:Ljava/lang/String;

    .line 1157930
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157927
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;->mContactInfoId:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1157926
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1157924
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/ContactInfoProtocolResult;->mContactInfoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1157925
    return-void
.end method
