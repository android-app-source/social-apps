.class public Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1157919
    new-instance v0, LX/6wc;

    invoke-direct {v0}, LX/6wc;-><init>()V

    sput-object v0, Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6wd;)V
    .locals 1

    .prologue
    .line 1157913
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157914
    iget-boolean v0, p1, LX/6wd;->a:Z

    move v0, v0

    .line 1157915
    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;->a:Z

    .line 1157916
    iget-object v0, p1, LX/6wd;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    move-object v0, v0

    .line 1157917
    iput-object v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    .line 1157918
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1157909
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157910
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;->a:Z

    .line 1157911
    const-class v0, Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    .line 1157912
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1157905
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1157906
    iget-boolean v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1157907
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/AddContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1157908
    return-void
.end method
