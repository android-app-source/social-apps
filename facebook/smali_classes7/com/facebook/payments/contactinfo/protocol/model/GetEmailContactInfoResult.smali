.class public Lcom/facebook/payments/contactinfo/protocol/model/GetEmailContactInfoResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/contactinfo/protocol/model/GetEmailContactInfoResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/contactinfo/model/EmailContactInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1158003
    new-instance v0, LX/6wh;

    invoke-direct {v0}, LX/6wh;-><init>()V

    sput-object v0, Lcom/facebook/payments/contactinfo/protocol/model/GetEmailContactInfoResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/contactinfo/model/EmailContactInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1158000
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1158001
    iput-object p1, p0, Lcom/facebook/payments/contactinfo/protocol/model/GetEmailContactInfoResult;->a:LX/0Px;

    .line 1158002
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1157997
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157998
    const-class v0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/GetEmailContactInfoResult;->a:LX/0Px;

    .line 1157999
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1157994
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1157995
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/GetEmailContactInfoResult;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1157996
    return-void
.end method
