.class public Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

.field public final c:Z

.field public final d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1157963
    new-instance v0, LX/6wf;

    invoke-direct {v0}, LX/6wf;-><init>()V

    sput-object v0, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6wg;)V
    .locals 1

    .prologue
    .line 1157964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157965
    iget-boolean v0, p1, LX/6wg;->c:Z

    move v0, v0

    .line 1157966
    if-eqz v0, :cond_0

    .line 1157967
    iget-boolean v0, p1, LX/6wg;->d:Z

    move v0, v0

    .line 1157968
    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1157969
    iget-object v0, p1, LX/6wg;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1157970
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->a:Ljava/lang/String;

    .line 1157971
    iget-object v0, p1, LX/6wg;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    move-object v0, v0

    .line 1157972
    iput-object v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    .line 1157973
    iget-boolean v0, p1, LX/6wg;->c:Z

    move v0, v0

    .line 1157974
    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->c:Z

    .line 1157975
    iget-boolean v0, p1, LX/6wg;->d:Z

    move v0, v0

    .line 1157976
    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->d:Z

    .line 1157977
    return-void

    .line 1157978
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1157979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157980
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->a:Ljava/lang/String;

    .line 1157981
    const-class v0, Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    .line 1157982
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->c:Z

    .line 1157983
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->d:Z

    .line 1157984
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1157985
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1157986
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1157987
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->b:Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1157988
    iget-boolean v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1157989
    iget-boolean v0, p0, Lcom/facebook/payments/contactinfo/protocol/model/EditContactInfoParams;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1157990
    return-void
.end method
