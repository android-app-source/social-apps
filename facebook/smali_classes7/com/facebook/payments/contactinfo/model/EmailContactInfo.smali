.class public Lcom/facebook/payments/contactinfo/model/EmailContactInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/contactinfo/model/ContactInfo;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/contactinfo/model/EmailContactInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1157090
    new-instance v0, LX/6vc;

    invoke-direct {v0}, LX/6vc;-><init>()V

    sput-object v0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6vd;)V
    .locals 1

    .prologue
    .line 1157082
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157083
    iget-object v0, p1, LX/6vd;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1157084
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->a:Ljava/lang/String;

    .line 1157085
    iget-object v0, p1, LX/6vd;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1157086
    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->b:Ljava/lang/String;

    .line 1157087
    iget-boolean v0, p1, LX/6vd;->c:Z

    move v0, v0

    .line 1157088
    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->c:Z

    .line 1157089
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1157077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157078
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->a:Ljava/lang/String;

    .line 1157079
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->b:Ljava/lang/String;

    .line 1157080
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->c:Z

    .line 1157081
    return-void
.end method

.method public static newBuilder()LX/6vd;
    .locals 1

    .prologue
    .line 1157076
    new-instance v0, LX/6vd;

    invoke-direct {v0}, LX/6vd;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157066
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1157075
    iget-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->c:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157074
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/6vb;
    .locals 1

    .prologue
    .line 1157073
    sget-object v0, LX/6vb;->EMAIL:LX/6vb;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1157072
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157071
    invoke-virtual {p0}, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1157067
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1157068
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1157069
    iget-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfo;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1157070
    return-void
.end method
