.class public Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1157197
    new-instance v0, LX/6vk;

    invoke-direct {v0}, LX/6vk;-><init>()V

    sput-object v0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6vl;)V
    .locals 1

    .prologue
    .line 1157191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157192
    iget-object v0, p1, LX/6vl;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1157193
    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;->a:Ljava/lang/String;

    .line 1157194
    iget-boolean v0, p1, LX/6vl;->b:Z

    move v0, v0

    .line 1157195
    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;->b:Z

    .line 1157196
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1157187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157188
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;->a:Ljava/lang/String;

    .line 1157189
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;->b:Z

    .line 1157190
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1157182
    iget-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;->b:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1157186
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1157183
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1157184
    iget-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfoFormInput;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1157185
    return-void
.end method
