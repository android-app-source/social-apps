.class public Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1157111
    new-instance v0, LX/6ve;

    invoke-direct {v0}, LX/6ve;-><init>()V

    sput-object v0, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6vf;)V
    .locals 1

    .prologue
    .line 1157096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157097
    iget-object v0, p1, LX/6vf;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1157098
    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;->a:Ljava/lang/String;

    .line 1157099
    iget-boolean v0, p1, LX/6vf;->b:Z

    move v0, v0

    .line 1157100
    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;->b:Z

    .line 1157101
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1157107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157108
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;->a:Ljava/lang/String;

    .line 1157109
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;->b:Z

    .line 1157110
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1157106
    iget-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;->b:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1157105
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1157102
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1157103
    iget-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/EmailContactInfoFormInput;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1157104
    return-void
.end method
