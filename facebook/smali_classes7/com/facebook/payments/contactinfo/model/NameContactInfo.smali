.class public Lcom/facebook/payments/contactinfo/model/NameContactInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/contactinfo/model/ContactInfo;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/contactinfo/model/NameContactInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1157129
    new-instance v0, LX/6vg;

    invoke-direct {v0}, LX/6vg;-><init>()V

    sput-object v0, Lcom/facebook/payments/contactinfo/model/NameContactInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1157126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157127
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/NameContactInfo;->a:Ljava/lang/String;

    .line 1157128
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1157123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157124
    iput-object p1, p0, Lcom/facebook/payments/contactinfo/model/NameContactInfo;->a:Ljava/lang/String;

    .line 1157125
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157121
    const-string v0, "0"

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1157122
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157130
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/model/NameContactInfo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/6vb;
    .locals 1

    .prologue
    .line 1157116
    sget-object v0, LX/6vb;->NAME:LX/6vb;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1157117
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157118
    invoke-virtual {p0}, Lcom/facebook/payments/contactinfo/model/NameContactInfo;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1157119
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/model/NameContactInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1157120
    return-void
.end method
