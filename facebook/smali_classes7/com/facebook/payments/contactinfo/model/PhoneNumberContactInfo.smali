.class public Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/contactinfo/model/ContactInfo;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1157175
    new-instance v0, LX/6vi;

    invoke-direct {v0}, LX/6vi;-><init>()V

    sput-object v0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6vj;)V
    .locals 1

    .prologue
    .line 1157165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157166
    iget-object v0, p1, LX/6vj;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1157167
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->a:Ljava/lang/String;

    .line 1157168
    iget-object v0, p1, LX/6vj;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1157169
    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->b:Ljava/lang/String;

    .line 1157170
    iget-object v0, p1, LX/6vj;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1157171
    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->c:Ljava/lang/String;

    .line 1157172
    iget-boolean v0, p1, LX/6vj;->d:Z

    move v0, v0

    .line 1157173
    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->d:Z

    .line 1157174
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1157159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157160
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->a:Ljava/lang/String;

    .line 1157161
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->b:Ljava/lang/String;

    .line 1157162
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->c:Ljava/lang/String;

    .line 1157163
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->d:Z

    .line 1157164
    return-void
.end method

.method public static newBuilder()LX/6vj;
    .locals 1

    .prologue
    .line 1157158
    new-instance v0, LX/6vj;

    invoke-direct {v0}, LX/6vj;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157157
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1157176
    iget-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->d:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157156
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/6vb;
    .locals 1

    .prologue
    .line 1157155
    sget-object v0, LX/6vb;->PHONE_NUMBER:LX/6vb;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1157148
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157154
    invoke-virtual {p0}, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1157149
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1157150
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1157151
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1157152
    iget-boolean v0, p0, Lcom/facebook/payments/contactinfo/model/PhoneNumberContactInfo;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1157153
    return-void
.end method
