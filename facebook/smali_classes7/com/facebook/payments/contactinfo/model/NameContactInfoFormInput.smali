.class public Lcom/facebook/payments/contactinfo/model/NameContactInfoFormInput;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/contactinfo/model/ContactInfoFormInput;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/contactinfo/model/NameContactInfoFormInput;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1157144
    new-instance v0, LX/6vh;

    invoke-direct {v0}, LX/6vh;-><init>()V

    sput-object v0, Lcom/facebook/payments/contactinfo/model/NameContactInfoFormInput;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1157141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157142
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/model/NameContactInfoFormInput;->a:Ljava/lang/String;

    .line 1157143
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1157138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157139
    iput-object p1, p0, Lcom/facebook/payments/contactinfo/model/NameContactInfoFormInput;->a:Ljava/lang/String;

    .line 1157140
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1157134
    const/4 v0, 0x0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1157137
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1157135
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/model/NameContactInfoFormInput;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1157136
    return-void
.end method
