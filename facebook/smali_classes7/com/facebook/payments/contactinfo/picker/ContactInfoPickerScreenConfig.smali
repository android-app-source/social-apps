.class public Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/picker/model/PickerScreenConfig;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

.field public final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/6vb;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/71H;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1157342
    new-instance v0, LX/6vy;

    invoke-direct {v0}, LX/6vy;-><init>()V

    sput-object v0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6w5;)V
    .locals 1

    .prologue
    .line 1157343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157344
    iget-object v0, p1, LX/6w5;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-object v0, v0

    .line 1157345
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 1157346
    iget-object v0, p1, LX/6w5;->b:LX/0Rf;

    move-object v0, v0

    .line 1157347
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rf;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->b:LX/0Rf;

    .line 1157348
    iget-object v0, p1, LX/6w5;->c:LX/71H;

    move-object v0, v0

    .line 1157349
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/71H;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->c:LX/71H;

    .line 1157350
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1157351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1157352
    const-class v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 1157353
    const-class v0, LX/6vb;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/ClassLoader;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->b:LX/0Rf;

    .line 1157354
    const-class v0, LX/71H;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/71H;

    iput-object v0, p0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->c:LX/71H;

    .line 1157355
    return-void
.end method

.method public static newBuilder()LX/6w5;
    .locals 1

    .prologue
    .line 1157356
    new-instance v0, LX/6w5;

    invoke-direct {v0}, LX/6w5;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;
    .locals 1

    .prologue
    .line 1157357
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1157358
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1157359
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1157360
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->b:LX/0Rf;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Set;)V

    .line 1157361
    iget-object v0, p0, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->c:LX/71H;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1157362
    return-void
.end method
