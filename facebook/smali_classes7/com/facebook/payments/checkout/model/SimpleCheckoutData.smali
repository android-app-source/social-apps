.class public Lcom/facebook/payments/checkout/model/SimpleCheckoutData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/checkout/model/CheckoutData;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/payments/checkout/CheckoutParams;

.field private final b:Z

.field private final c:Lcom/facebook/payments/model/PaymentsPin;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/shipping/model/ShippingOption;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/ShippingOption;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lcom/facebook/payments/contactinfo/model/ContactInfo;

.field private final n:Landroid/os/Parcelable;

.field private final o:Lcom/facebook/flatbuffers/Flattenable;

.field private final p:LX/6tr;

.field private final q:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

.field private final s:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            ">;>;"
        }
    .end annotation
.end field

.field private final t:Ljava/lang/String;

.field private final u:I

.field private final v:Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;

.field private final w:Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

.field private final x:Ljava/lang/Integer;

.field private final y:Lcom/facebook/payments/currency/CurrencyAmount;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1153295
    new-instance v0, LX/6sP;

    invoke-direct {v0}, LX/6sP;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6sR;)V
    .locals 2

    .prologue
    .line 1153164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153165
    iget-object v0, p1, LX/6sR;->a:Lcom/facebook/payments/checkout/CheckoutParams;

    move-object v0, v0

    .line 1153166
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a:Lcom/facebook/payments/checkout/CheckoutParams;

    .line 1153167
    iget-boolean v0, p1, LX/6sR;->b:Z

    move v0, v0

    .line 1153168
    iput-boolean v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->b:Z

    .line 1153169
    iget-object v0, p1, LX/6sR;->c:Lcom/facebook/payments/model/PaymentsPin;

    move-object v0, v0

    .line 1153170
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->c:Lcom/facebook/payments/model/PaymentsPin;

    .line 1153171
    iget-object v0, p1, LX/6sR;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1153172
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->d:Ljava/lang/String;

    .line 1153173
    iget-object v0, p1, LX/6sR;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1153174
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->e:Ljava/lang/String;

    .line 1153175
    iget-object v0, p1, LX/6sR;->f:LX/0am;

    move-object v0, v0

    .line 1153176
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->f:LX/0am;

    .line 1153177
    iget-object v0, p1, LX/6sR;->g:LX/0Px;

    move-object v0, v0

    .line 1153178
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->g:LX/0Px;

    .line 1153179
    iget-object v0, p1, LX/6sR;->h:LX/0am;

    move-object v0, v0

    .line 1153180
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->h:LX/0am;

    .line 1153181
    iget-object v0, p1, LX/6sR;->i:LX/0Px;

    move-object v0, v0

    .line 1153182
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->i:LX/0Px;

    .line 1153183
    iget-object v0, p1, LX/6sR;->j:LX/0am;

    move-object v0, v0

    .line 1153184
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->j:LX/0am;

    .line 1153185
    iget-object v0, p1, LX/6sR;->k:LX/0am;

    move-object v0, v0

    .line 1153186
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->k:LX/0am;

    .line 1153187
    iget-object v0, p1, LX/6sR;->l:LX/0Px;

    move-object v0, v0

    .line 1153188
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->l:LX/0Px;

    .line 1153189
    iget-object v0, p1, LX/6sR;->m:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    move-object v0, v0

    .line 1153190
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->m:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1153191
    iget-object v0, p1, LX/6sR;->n:Landroid/os/Parcelable;

    move-object v0, v0

    .line 1153192
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->n:Landroid/os/Parcelable;

    .line 1153193
    iget-object v0, p1, LX/6sR;->o:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1153194
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->o:Lcom/facebook/flatbuffers/Flattenable;

    .line 1153195
    iget-object v0, p1, LX/6sR;->p:LX/6tr;

    move-object v0, v0

    .line 1153196
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6tr;

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->p:LX/6tr;

    .line 1153197
    iget-object v0, p1, LX/6sR;->q:LX/0am;

    move-object v0, v0

    .line 1153198
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->q:LX/0am;

    .line 1153199
    iget-object v0, p1, LX/6sR;->r:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    move-object v0, v0

    .line 1153200
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->r:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1153201
    iget-object v0, p1, LX/6sR;->s:LX/0P1;

    move-object v0, v0

    .line 1153202
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 1153203
    invoke-static {v0, v1}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->s:LX/0P1;

    .line 1153204
    iget-object v0, p1, LX/6sR;->t:Ljava/lang/String;

    move-object v0, v0

    .line 1153205
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->t:Ljava/lang/String;

    .line 1153206
    iget v0, p1, LX/6sR;->u:I

    move v0, v0

    .line 1153207
    iput v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->u:I

    .line 1153208
    iget-object v0, p1, LX/6sR;->v:Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;

    move-object v0, v0

    .line 1153209
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->v:Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;

    .line 1153210
    iget-object v0, p1, LX/6sR;->w:Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    move-object v0, v0

    .line 1153211
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->w:Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    .line 1153212
    iget-object v0, p1, LX/6sR;->x:Ljava/lang/Integer;

    move-object v0, v0

    .line 1153213
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->x:Ljava/lang/Integer;

    .line 1153214
    iget-object v0, p1, LX/6sR;->y:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v0, v0

    .line 1153215
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->y:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1153216
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 1153217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153218
    const-class v0, Lcom/facebook/payments/checkout/CheckoutParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/CheckoutParams;

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a:Lcom/facebook/payments/checkout/CheckoutParams;

    .line 1153219
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->b:Z

    .line 1153220
    const-class v0, Lcom/facebook/payments/model/PaymentsPin;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/model/PaymentsPin;

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->c:Lcom/facebook/payments/model/PaymentsPin;

    .line 1153221
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->d:Ljava/lang/String;

    .line 1153222
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->e:Ljava/lang/String;

    .line 1153223
    const-class v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Class;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->f:LX/0am;

    .line 1153224
    const-class v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->g:LX/0Px;

    .line 1153225
    const-class v0, Lcom/facebook/payments/shipping/model/ShippingOption;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Class;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->h:LX/0am;

    .line 1153226
    const-class v0, Lcom/facebook/payments/shipping/model/ShippingOption;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->i:LX/0Px;

    .line 1153227
    const-class v0, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Class;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->j:LX/0am;

    .line 1153228
    const-class v0, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Class;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->k:LX/0am;

    .line 1153229
    const-class v0, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->l:LX/0Px;

    .line 1153230
    const-class v0, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/contactinfo/model/ContactInfo;

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->m:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1153231
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->n:Landroid/os/Parcelable;

    .line 1153232
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->o:Lcom/facebook/flatbuffers/Flattenable;

    .line 1153233
    const-class v0, LX/6tr;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6tr;

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->p:LX/6tr;

    .line 1153234
    const-class v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Class;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->q:LX/0am;

    .line 1153235
    const-class v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->r:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1153236
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1153237
    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 1153238
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1153239
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1153240
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1153241
    :cond_0
    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    move-object v0, v0

    .line 1153242
    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->s:LX/0P1;

    .line 1153243
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->t:Ljava/lang/String;

    .line 1153244
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->u:I

    .line 1153245
    const-class v0, Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->v:Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;

    .line 1153246
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->w:Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    .line 1153247
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->x:Ljava/lang/Integer;

    .line 1153248
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->y:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1153249
    return-void
.end method

.method public static newBuilder()LX/6sR;
    .locals 1

    .prologue
    .line 1153250
    new-instance v0, LX/6sR;

    invoke-direct {v0}, LX/6sR;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final A()Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153251
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->y:Lcom/facebook/payments/currency/CurrencyAmount;

    return-object v0
.end method

.method public final a()Lcom/facebook/payments/checkout/CheckoutCommonParams;
    .locals 1

    .prologue
    .line 1153252
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/6rp;)Lcom/facebook/payments/checkout/model/SimpleCheckoutData;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1153253
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    .line 1153254
    sget-object v1, LX/6sQ;->a:[I

    invoke-virtual {p1}, LX/6rp;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1153255
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid PurchaseInfo provided: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1153256
    :pswitch_0
    iput-object v3, v0, LX/6sR;->j:LX/0am;

    .line 1153257
    move-object v1, v0

    .line 1153258
    iput-object v3, v1, LX/6sR;->k:LX/0am;

    .line 1153259
    move-object v1, v1

    .line 1153260
    iput-object v3, v1, LX/6sR;->l:LX/0Px;

    .line 1153261
    :goto_0
    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    return-object v0

    .line 1153262
    :pswitch_1
    iput-object v3, v0, LX/6sR;->m:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    .line 1153263
    goto :goto_0

    .line 1153264
    :pswitch_2
    iput-object v3, v0, LX/6sR;->s:LX/0P1;

    .line 1153265
    goto :goto_0

    .line 1153266
    :pswitch_3
    iput-object v3, v0, LX/6sR;->f:LX/0am;

    .line 1153267
    move-object v1, v0

    .line 1153268
    invoke-virtual {v1, v3}, LX/6sR;->a(Ljava/util/List;)LX/6sR;

    goto :goto_0

    .line 1153269
    :pswitch_4
    iput-object v3, v0, LX/6sR;->r:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1153270
    move-object v1, v0

    .line 1153271
    iput-object v3, v1, LX/6sR;->t:Ljava/lang/String;

    .line 1153272
    move-object v1, v1

    .line 1153273
    iput-object v3, v1, LX/6sR;->q:LX/0am;

    .line 1153274
    goto :goto_0

    .line 1153275
    :pswitch_5
    iput-object v3, v0, LX/6sR;->c:Lcom/facebook/payments/model/PaymentsPin;

    .line 1153276
    move-object v1, v0

    .line 1153277
    iput-object v3, v1, LX/6sR;->e:Ljava/lang/String;

    .line 1153278
    goto :goto_0

    .line 1153279
    :pswitch_6
    iput-object v3, v0, LX/6sR;->h:LX/0am;

    .line 1153280
    move-object v1, v0

    .line 1153281
    iput-object v3, v1, LX/6sR;->i:LX/0Px;

    .line 1153282
    goto :goto_0

    .line 1153283
    :pswitch_7
    iput-object v3, v0, LX/6sR;->w:Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    .line 1153284
    iput-object v3, v0, LX/6sR;->x:Ljava/lang/Integer;

    .line 1153285
    iput-object v3, v0, LX/6sR;->y:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1153286
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final b()Lcom/facebook/payments/checkout/CheckoutParams;
    .locals 1

    .prologue
    .line 1153287
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a:Lcom/facebook/payments/checkout/CheckoutParams;

    return-object v0
.end method

.method public final c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;
    .locals 1

    .prologue
    .line 1153122
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v0

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->D:Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1153288
    iget-boolean v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->b:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1153289
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/payments/model/PaymentsPin;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153290
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->c:Lcom/facebook/payments/model/PaymentsPin;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153291
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153292
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final h()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153293
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->f:LX/0am;

    return-object v0
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153294
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->g:LX/0Px;

    return-object v0
.end method

.method public final j()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/shipping/model/ShippingOption;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153162
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->h:LX/0am;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/ShippingOption;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153163
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->i:LX/0Px;

    return-object v0
.end method

.method public final l()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153114
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->j:LX/0am;

    return-object v0
.end method

.method public final m()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153115
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->k:LX/0am;

    return-object v0
.end method

.method public final n()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153116
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->l:LX/0Px;

    return-object v0
.end method

.method public final o()Lcom/facebook/payments/contactinfo/model/ContactInfo;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153117
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->m:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    return-object v0
.end method

.method public final p()Landroid/os/Parcelable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153118
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->n:Landroid/os/Parcelable;

    return-object v0
.end method

.method public final q()Lcom/facebook/flatbuffers/Flattenable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">()TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153119
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->o:Lcom/facebook/flatbuffers/Flattenable;

    return-object v0
.end method

.method public final r()LX/6tr;
    .locals 1

    .prologue
    .line 1153120
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->p:LX/6tr;

    return-object v0
.end method

.method public final s()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethod;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153121
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->q:LX/0am;

    return-object v0
.end method

.method public final t()Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153113
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->r:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    return-object v0
.end method

.method public final u()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1153123
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->s:LX/0P1;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153124
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 1153125
    iget v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->u:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    .line 1153126
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a:Lcom/facebook/payments/checkout/CheckoutParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1153127
    iget-boolean v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1153128
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->c:Lcom/facebook/payments/model/PaymentsPin;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1153129
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153130
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153131
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->f:LX/0am;

    invoke-static {p1, v0, p2}, LX/46R;->a(Landroid/os/Parcel;LX/0am;I)V

    .line 1153132
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->g:LX/0Px;

    .line 1153133
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1153134
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->h:LX/0am;

    invoke-static {p1, v0, p2}, LX/46R;->a(Landroid/os/Parcel;LX/0am;I)V

    .line 1153135
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->i:LX/0Px;

    .line 1153136
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1153137
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->j:LX/0am;

    invoke-static {p1, v0, p2}, LX/46R;->a(Landroid/os/Parcel;LX/0am;I)V

    .line 1153138
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->k:LX/0am;

    invoke-static {p1, v0, p2}, LX/46R;->a(Landroid/os/Parcel;LX/0am;I)V

    .line 1153139
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->l:LX/0Px;

    .line 1153140
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1153141
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->m:Lcom/facebook/payments/contactinfo/model/ContactInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1153142
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->n:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1153143
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->o:Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1153144
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->p:LX/6tr;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1153145
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->q:LX/0am;

    invoke-static {p1, v0, p2}, LX/46R;->a(Landroid/os/Parcel;LX/0am;I)V

    .line 1153146
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->r:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1153147
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->s:LX/0P1;

    .line 1153148
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1153149
    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1153150
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1153151
    :cond_0
    invoke-static {p1, v2}, LX/46R;->d(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 1153152
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153153
    iget v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->u:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1153154
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->v:Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1153155
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->w:Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1153156
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->x:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1153157
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->y:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1153158
    return-void
.end method

.method public final x()Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153159
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->v:Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;

    return-object v0
.end method

.method public final y()Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153160
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->w:Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    return-object v0
.end method

.method public final z()Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1153161
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->x:Ljava/lang/Integer;

    return-object v0
.end method
