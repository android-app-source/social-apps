.class public Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:LX/0lF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1153389
    new-instance v0, LX/6sS;

    invoke-direct {v0}, LX/6sS;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1153385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153386
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;->a:Ljava/lang/String;

    .line 1153387
    invoke-static {p1}, LX/46R;->k(Landroid/os/Parcel;)LX/0lF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;->b:LX/0lF;

    .line 1153388
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0lF;)V
    .locals 0
    .param p2    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1153376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153377
    iput-object p1, p0, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;->a:Ljava/lang/String;

    .line 1153378
    iput-object p2, p0, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;->b:LX/0lF;

    .line 1153379
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1153384
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1153383
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1153380
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153381
    iget-object v0, p0, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;->b:LX/0lF;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/0lF;)V

    .line 1153382
    return-void
.end method
