.class public interface abstract Lcom/facebook/payments/checkout/model/CheckoutData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# virtual methods
.method public abstract A()Lcom/facebook/payments/currency/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract a()Lcom/facebook/payments/checkout/CheckoutCommonParams;
.end method

.method public abstract b()Lcom/facebook/payments/checkout/CheckoutParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/payments/checkout/CheckoutParams;",
            ">()TT;"
        }
    .end annotation
.end method

.method public abstract c()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;
.end method

.method public abstract d()Z
.end method

.method public abstract e()Lcom/facebook/payments/model/PaymentsPin;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract f()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract g()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract h()LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract i()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract j()LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/shipping/model/ShippingOption;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/ShippingOption;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract l()LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract m()LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract n()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract o()Lcom/facebook/payments/contactinfo/model/ContactInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract p()Landroid/os/Parcelable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract q()Lcom/facebook/flatbuffers/Flattenable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">()TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract r()LX/6tr;
.end method

.method public abstract s()LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethod;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract t()Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract u()LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract v()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract w()I
.end method

.method public abstract x()Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract y()Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract z()Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
