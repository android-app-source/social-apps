.class public Lcom/facebook/payments/checkout/CheckoutFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/6sm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/6r9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/6xb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:Landroid/content/Context;

.field public i:Landroid/view/ViewGroup;

.field public j:Landroid/widget/ProgressBar;

.field public k:LX/6sO;

.field private l:Landroid/support/v7/widget/RecyclerView;

.field public m:Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

.field public n:Lcom/facebook/payments/checkout/model/CheckoutData;

.field public o:LX/6qd;

.field public p:LX/6Ev;

.field public q:LX/6Dk;

.field public r:LX/6tv;

.field public s:LX/6Du;

.field public final t:LX/6qh;

.field private final u:LX/0Vd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1151302
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1151303
    new-instance v0, LX/6qi;

    invoke-direct {v0, p0}, LX/6qi;-><init>(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->t:LX/6qh;

    .line 1151304
    new-instance v0, LX/6qj;

    invoke-direct {v0, p0}, LX/6qj;-><init>(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->u:LX/0Vd;

    return-void
.end method

.method public static D(Lcom/facebook/payments/checkout/CheckoutFragment;)V
    .locals 2

    .prologue
    .line 1151299
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->j:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1151300
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->i:Landroid/view/ViewGroup;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 1151301
    return-void
.end method

.method public static a$redex0(Lcom/facebook/payments/checkout/CheckoutFragment;LX/6qt;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 2

    .prologue
    .line 1151291
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->g:LX/1Ck;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->u:LX/0Vd;

    invoke-virtual {v0, p1, p2, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1151292
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 1151293
    if-eqz v0, :cond_0

    .line 1151294
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1151295
    :cond_0
    invoke-static {p0}, Lcom/facebook/payments/checkout/CheckoutFragment;->z(Lcom/facebook/payments/checkout/CheckoutFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1151296
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->i:Landroid/view/ViewGroup;

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 1151297
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->j:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1151298
    :cond_1
    return-void
.end method

.method public static o(Lcom/facebook/payments/checkout/CheckoutFragment;)V
    .locals 3

    .prologue
    .line 1151284
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->g:LX/1Ck;

    sget-object v1, LX/6qt;->CHECKOUT_LOADER:LX/6qt;

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151285
    :goto_0
    return-void

    .line 1151286
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->c:LX/6r9;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->a(LX/6qw;)LX/6qa;

    move-result-object v0

    .line 1151287
    new-instance v1, LX/6qr;

    invoke-direct {v1, p0}, LX/6qr;-><init>(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    invoke-interface {v0, v1}, LX/6qa;->a(LX/6qb;)V

    .line 1151288
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1}, LX/6qa;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1151289
    new-instance v1, LX/6qs;

    invoke-direct {v1, p0}, LX/6qs;-><init>(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    iget-object v2, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1151290
    sget-object v1, LX/6qt;->CHECKOUT_LOADER:LX/6qt;

    invoke-static {p0, v1, v0}, Lcom/facebook/payments/checkout/CheckoutFragment;->a$redex0(Lcom/facebook/payments/checkout/CheckoutFragment;LX/6qt;Lcom/google/common/util/concurrent/ListenableFuture;)V

    goto :goto_0
.end method

.method public static p(Lcom/facebook/payments/checkout/CheckoutFragment;)V
    .locals 3

    .prologue
    .line 1151266
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->c:LX/6r9;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->f(LX/6qw;)LX/6Ds;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1}, LX/6Ds;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;

    move-result-object v0

    .line 1151267
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->b:LX/6sm;

    .line 1151268
    iput-object v0, v1, LX/6sm;->c:LX/0Px;

    .line 1151269
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 1151270
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->r:LX/6tv;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-virtual {v0, v1}, LX/6tv;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151271
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->r:LX/6tv;

    invoke-static {p0}, Lcom/facebook/payments/checkout/CheckoutFragment;->t(Lcom/facebook/payments/checkout/CheckoutFragment;)LX/6ng;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6tv;->a(LX/6ng;)V

    .line 1151272
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->r:LX/6tv;

    invoke-virtual {v0}, LX/6tv;->a()V

    .line 1151273
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->p:LX/6Ev;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1}, LX/6Ev;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151274
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-boolean v1, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->n:Z

    .line 1151275
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->r()LX/6tr;

    move-result-object v0

    sget-object v2, LX/6tr;->PROCESSING_SEND_PAYMENT:LX/6tr;

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    .line 1151276
    :goto_0
    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    .line 1151277
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->k:LX/6sO;

    if-nez v0, :cond_0

    .line 1151278
    new-instance v0, LX/6sO;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6sO;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->k:LX/6sO;

    .line 1151279
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->k:LX/6sO;

    invoke-virtual {v0}, LX/6sO;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1151280
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->k:LX/6sO;

    invoke-virtual {v0}, LX/6sO;->show()V

    .line 1151281
    :cond_1
    :goto_1
    return-void

    .line 1151282
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1151283
    :cond_3
    invoke-static {p0}, Lcom/facebook/payments/checkout/CheckoutFragment;->y(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    goto :goto_1
.end method

.method public static t(Lcom/facebook/payments/checkout/CheckoutFragment;)LX/6ng;
    .locals 1

    .prologue
    .line 1151155
    invoke-static {}, LX/6nh;->newBuilder()LX/6ng;

    move-result-object v0

    .line 1151156
    iput-object p0, v0, LX/6ng;->a:Lcom/facebook/base/fragment/FbFragment;

    .line 1151157
    move-object v0, v0

    .line 1151158
    return-object v0
.end method

.method public static w(Lcom/facebook/payments/checkout/CheckoutFragment;)V
    .locals 4

    .prologue
    .line 1151263
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    .line 1151264
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->e:LX/6xb;

    iget-object v2, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->D:Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->D:Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->b:LX/6xZ;

    const-string v3, "payflows_cancel"

    invoke-virtual {v1, v2, v0, v3}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/String;)V

    .line 1151265
    return-void
.end method

.method public static y(Lcom/facebook/payments/checkout/CheckoutFragment;)V
    .locals 1

    .prologue
    .line 1151259
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->k:LX/6sO;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->k:LX/6sO;

    invoke-virtual {v0}, LX/6sO;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1151260
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->k:LX/6sO;

    invoke-virtual {v0}, LX/6sO;->dismiss()V

    .line 1151261
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->k:LX/6sO;

    .line 1151262
    return-void
.end method

.method public static z(Lcom/facebook/payments/checkout/CheckoutFragment;)Z
    .locals 2

    .prologue
    .line 1151258
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->g:LX/1Ck;

    sget-object v1, LX/6qt;->CHECKOUT_LOADER:LX/6qt;

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->g:LX/1Ck;

    sget-object v1, LX/6qt;->PAYMENTS_COMPONENT_WITH_UI_PROGRESS:LX/6qt;

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    .line 1151251
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->C:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->r()LX/6tr;

    move-result-object v0

    sget-object v1, LX/6tr;->PROCESSING_SEND_PAYMENT:LX/6tr;

    if-ne v0, v1, :cond_0

    .line 1151252
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v1, 0x7f081d53

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f081d54

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080021

    new-instance v2, LX/6qf;

    invoke-direct {v2, p0}, LX/6qf;-><init>(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080020

    new-instance v2, LX/6qe;

    invoke-direct {v2, p0}, LX/6qe;-><init>(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 1151253
    const/4 v0, 0x0

    .line 1151254
    :goto_0
    return v0

    .line 1151255
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->q:LX/6Dk;

    invoke-interface {v0}, LX/6Dk;->a()V

    .line 1151256
    invoke-static {p0}, Lcom/facebook/payments/checkout/CheckoutFragment;->w(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    .line 1151257
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1151244
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1151245
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0103f1

    const v2, 0x7f0e0326

    invoke-static {v0, v1, v2}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->h:Landroid/content/Context;

    .line 1151246
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->h:Landroid/content/Context;

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/payments/checkout/CheckoutFragment;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    new-instance v5, LX/6sm;

    invoke-static {v0}, LX/6r9;->a(LX/0QB;)LX/6r9;

    move-result-object v4

    check-cast v4, LX/6r9;

    invoke-direct {v5, v4}, LX/6sm;-><init>(LX/6r9;)V

    move-object v4, v5

    check-cast v4, LX/6sm;

    invoke-static {v0}, LX/6r9;->a(LX/0QB;)LX/6r9;

    move-result-object v5

    check-cast v5, LX/6r9;

    invoke-static {v0}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v6

    check-cast v6, LX/6wr;

    invoke-static {v0}, LX/6xb;->a(LX/0QB;)LX/6xb;

    move-result-object v7

    check-cast v7, LX/6xb;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    iput-object v3, v2, Lcom/facebook/payments/checkout/CheckoutFragment;->a:Ljava/util/concurrent/Executor;

    iput-object v4, v2, Lcom/facebook/payments/checkout/CheckoutFragment;->b:LX/6sm;

    iput-object v5, v2, Lcom/facebook/payments/checkout/CheckoutFragment;->c:LX/6r9;

    iput-object v6, v2, Lcom/facebook/payments/checkout/CheckoutFragment;->d:LX/6wr;

    iput-object v7, v2, Lcom/facebook/payments/checkout/CheckoutFragment;->e:LX/6xb;

    iput-object v8, v2, Lcom/facebook/payments/checkout/CheckoutFragment;->f:Lcom/facebook/content/SecureContextHelper;

    iput-object v0, v2, Lcom/facebook/payments/checkout/CheckoutFragment;->g:LX/1Ck;

    .line 1151247
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1151248
    const-string v1, "checkout_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/CheckoutParams;

    .line 1151249
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->e:LX/6xb;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->D:Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->D:Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->b:LX/6xZ;

    invoke-virtual {v1, v2, v3, v0, p1}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xg;LX/6xZ;Landroid/os/Bundle;)V

    .line 1151250
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1151239
    packed-switch p1, :pswitch_data_0

    .line 1151240
    :pswitch_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1151241
    :goto_0
    return-void

    .line 1151242
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->q:LX/6Dk;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1, p2, p3}, LX/6Dk;->a(Lcom/facebook/payments/checkout/model/CheckoutData;IILandroid/content/Intent;)V

    goto :goto_0

    .line 1151243
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->r:LX/6tv;

    invoke-virtual {v0, p1, p2, p3}, LX/6tv;->a(IILandroid/content/Intent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x3af386b4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1151234
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->h:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03028f

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1151235
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1151236
    const-string v3, "checkout_params"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/CheckoutParams;

    .line 1151237
    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v3, v3, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->c:LX/0am;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-boolean v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    invoke-static {v2, v3, v0}, LX/6wr;->a(Landroid/view/View;LX/0am;Z)V

    .line 1151238
    const/16 v0, 0x2b

    const v3, 0x7f9d1de6

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x50aa700d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1151229
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1151230
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->g:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1151231
    invoke-static {p0}, Lcom/facebook/payments/checkout/CheckoutFragment;->y(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    .line 1151232
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->s:LX/6Du;

    invoke-interface {v1}, LX/6Du;->a()V

    .line 1151233
    const/16 v1, 0x2b

    const v2, 0x60019dc2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1151225
    const-string v0, "checkout_data"

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1151226
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->p:LX/6Ev;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, p1, v1}, LX/6Ev;->a(Landroid/os/Bundle;Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151227
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1151228
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1151159
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1151160
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1151161
    const-string v1, "checkout_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/CheckoutParams;

    .line 1151162
    const v1, 0x7f0d04e7

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    iput-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->l:Landroid/support/v7/widget/RecyclerView;

    .line 1151163
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->l:Landroid/support/v7/widget/RecyclerView;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-boolean v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    invoke-static {v1, v0}, LX/6wr;->a(Landroid/support/v7/widget/RecyclerView;Z)V

    .line 1151164
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->l:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->b:LX/6sm;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1151165
    const v0, 0x7f0d093f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->i:Landroid/view/ViewGroup;

    .line 1151166
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->j:Landroid/widget/ProgressBar;

    .line 1151167
    if-eqz p2, :cond_3

    .line 1151168
    const-string v0, "checkout_data"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/model/CheckoutData;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    .line 1151169
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1151170
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1151171
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1151172
    new-instance v1, LX/6qk;

    invoke-direct {v1, p0}, LX/6qk;-><init>(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1151173
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1151174
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v2, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1151175
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->m:Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    if-nez v1, :cond_1

    .line 1151176
    const v1, 0x7f0d00bb

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    iput-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->m:Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 1151177
    iget-object v3, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->m:Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    .line 1151178
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1151179
    check-cast v1, Landroid/view/ViewGroup;

    new-instance v4, LX/6ql;

    invoke-direct {v4, p0, v0}, LX/6ql;-><init>(Lcom/facebook/payments/checkout/CheckoutFragment;Landroid/app/Activity;)V

    iget-object v0, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    iget-object p1, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-virtual {p1}, LX/6ws;->getTitleBarNavIconStyle()LX/73h;

    move-result-object p1

    invoke-virtual {v3, v1, v4, v0, p1}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V

    .line 1151180
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->m:Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->l:I

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Ljava/lang/String;LX/73i;)V

    .line 1151181
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->m:Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->m:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->setAppIconVisibility(I)V

    .line 1151182
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->b:LX/6sm;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->t:LX/6qh;

    .line 1151183
    iput-object v1, v0, LX/6sm;->d:LX/6qh;

    .line 1151184
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->b:LX/6sm;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    .line 1151185
    iput-object v1, v0, LX/6sm;->b:Lcom/facebook/payments/checkout/model/CheckoutData;

    .line 1151186
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->c:LX/6r9;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->b(LX/6qw;)LX/6qd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    .line 1151187
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->t:LX/6qh;

    invoke-interface {v0, v1}, LX/6qd;->a(LX/6qh;)V

    .line 1151188
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->o:LX/6qd;

    new-instance v1, LX/6qm;

    invoke-direct {v1, p0}, LX/6qm;-><init>(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    invoke-interface {v0, v1}, LX/6qd;->a(LX/6qc;)V

    .line 1151189
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->c:LX/6r9;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    .line 1151190
    iget-object v2, v0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1151191
    iget-object v2, v0, LX/6r9;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Dy;

    iget-object v2, v2, LX/6Dy;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Ev;

    .line 1151192
    :goto_2
    move-object v0, v2

    .line 1151193
    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->p:LX/6Ev;

    .line 1151194
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->p:LX/6Ev;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->t:LX/6qh;

    invoke-interface {v0, v1}, LX/6Ev;->a(LX/6qh;)V

    .line 1151195
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->p:LX/6Ev;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, p2, v1}, LX/6Ev;->b(Landroid/os/Bundle;Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151196
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->c:LX/6r9;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->d(LX/6qw;)LX/6Dk;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->q:LX/6Dk;

    .line 1151197
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->q:LX/6Dk;

    new-instance v1, LX/6qo;

    invoke-direct {v1, p0}, LX/6qo;-><init>(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    invoke-interface {v0, v1}, LX/6Dk;->a(LX/6qn;)V

    .line 1151198
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->c:LX/6r9;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->i(LX/6qw;)LX/6tv;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->r:LX/6tv;

    .line 1151199
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->r:LX/6tv;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->t:LX/6qh;

    .line 1151200
    iput-object v1, v0, LX/6tv;->g:LX/6qh;

    .line 1151201
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->r:LX/6tv;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-virtual {v0, v1}, LX/6tv;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    .line 1151202
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->r:LX/6tv;

    new-instance v1, LX/6qq;

    invoke-direct {v1, p0}, LX/6qq;-><init>(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    invoke-virtual {v0, v1}, LX/6tv;->a(LX/6qp;)V

    .line 1151203
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->r:LX/6tv;

    invoke-static {p0}, Lcom/facebook/payments/checkout/CheckoutFragment;->t(Lcom/facebook/payments/checkout/CheckoutFragment;)LX/6ng;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6tv;->a(LX/6ng;)V

    .line 1151204
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->c:LX/6r9;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-virtual {v0, v1}, LX/6r9;->g(LX/6qw;)LX/6Du;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->s:LX/6Du;

    .line 1151205
    invoke-static {p0}, Lcom/facebook/payments/checkout/CheckoutFragment;->o(Lcom/facebook/payments/checkout/CheckoutFragment;)V

    .line 1151206
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->s:LX/6Du;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1}, LX/6Du;->b(Lcom/facebook/payments/checkout/model/CheckoutData;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1151207
    :cond_2
    :goto_3
    return-void

    .line 1151208
    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1151209
    const-string v1, "checkout_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/CheckoutParams;

    .line 1151210
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v1

    .line 1151211
    iput-object v0, v1, LX/6sR;->a:Lcom/facebook/payments/checkout/CheckoutParams;

    .line 1151212
    move-object v1, v1

    .line 1151213
    sget-object v2, LX/6tr;->PREPARE_CHECKOUT:LX/6tr;

    .line 1151214
    iput-object v2, v1, LX/6sR;->p:LX/6tr;

    .line 1151215
    move-object v1, v1

    .line 1151216
    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    sget-object v3, LX/6rp;->CHECKOUT_OPTIONS:LX/6rp;

    invoke-virtual {v2, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1151217
    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->v:LX/0Px;

    .line 1151218
    invoke-static {v0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;->a(LX/0Px;)LX/0P1;

    move-result-object v0

    .line 1151219
    iput-object v0, v1, LX/6sR;->s:LX/0P1;

    .line 1151220
    :cond_4
    invoke-virtual {v1}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    goto/16 :goto_0

    .line 1151221
    :cond_5
    const/16 v0, 0x8

    goto/16 :goto_1

    :cond_6
    iget-object v2, v0, LX/6r9;->a:LX/0P1;

    sget-object v3, LX/6qw;->SIMPLE:LX/6qw;

    invoke-virtual {v2, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Dy;

    iget-object v2, v2, LX/6Dy;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Ev;

    goto/16 :goto_2

    .line 1151222
    :cond_7
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->s:LX/6Du;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1}, LX/6Du;->c(Lcom/facebook/payments/checkout/model/CheckoutData;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1151223
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_3

    .line 1151224
    :cond_8
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->s:LX/6Du;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutFragment;->n:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1}, LX/6Du;->d(Lcom/facebook/payments/checkout/model/CheckoutData;)V

    goto :goto_3
.end method
