.class public Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesCheckoutView;
.super Lcom/facebook/payments/ui/PaymentsComponentViewGroup;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1154987
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;)V

    .line 1154988
    invoke-direct {p0}, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesCheckoutView;->a()V

    .line 1154989
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1154984
    invoke-direct {p0, p1, p2}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1154985
    invoke-direct {p0}, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesCheckoutView;->a()V

    .line 1154986
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1154990
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1154991
    invoke-direct {p0}, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesCheckoutView;->a()V

    .line 1154992
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1154974
    const v0, 0x7f03148f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1154975
    const v0, 0x7f0d02a7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesCheckoutView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1154976
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 1154977
    new-instance v0, LX/6tO;

    invoke-direct {v0, p0, p2}, LX/6tO;-><init>(Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesCheckoutView;Landroid/net/Uri;)V

    .line 1154978
    new-instance v1, LX/47x;

    invoke-virtual {p0}, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesCheckoutView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1154979
    invoke-virtual {v1, p1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1154980
    const-string v2, "[[terms_and_policies]]"

    invoke-virtual {p0}, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesCheckoutView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081d48

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x21

    invoke-virtual {v1, v2, v3, v0, v4}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 1154981
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesCheckoutView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1154982
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesCheckoutView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1154983
    return-void
.end method
