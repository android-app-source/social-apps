.class public Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;
.super Lcom/facebook/payments/ui/PaymentsComponentViewGroup;
.source ""


# instance fields
.field public a:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1153736
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;)V

    .line 1153737
    invoke-direct {p0}, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;->a()V

    .line 1153738
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1153739
    invoke-direct {p0, p1, p2}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1153740
    invoke-direct {p0}, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;->a()V

    .line 1153741
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1153742
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1153743
    invoke-direct {p0}, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;->a()V

    .line 1153744
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1153745
    const-class v0, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;

    invoke-static {v0, p0}, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1153746
    const v0, 0x7f03028b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1153747
    const v0, 0x7f0d02a7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;->b:Landroid/widget/TextView;

    .line 1153748
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;->a:LX/23P;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1153749
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    iput-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;->a:LX/23P;

    return-void
.end method


# virtual methods
.method public setText(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1153750
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 1153751
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1153752
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/CheckoutActionTextRowView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1153753
    return-void
.end method
