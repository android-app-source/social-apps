.class public Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;
.super LX/6E7;
.source ""


# instance fields
.field private a:Landroid/support/v7/widget/RecyclerView;

.field private b:LX/6t3;

.field private c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1154000
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1154001
    invoke-direct {p0}, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->a()V

    .line 1154002
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1154036
    invoke-direct {p0, p1, p2}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1154037
    invoke-direct {p0}, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->a()V

    .line 1154038
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1154033
    invoke-direct {p0, p1, p2, p3}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1154034
    invoke-direct {p0}, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->a()V

    .line 1154035
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1154024
    const v0, 0x7f03100e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1154025
    const v0, 0x7f0d268c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->a:Landroid/support/v7/widget/RecyclerView;

    .line 1154026
    const v0, 0x7f0d268b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->c:Landroid/widget/TextView;

    .line 1154027
    new-instance v0, LX/6t3;

    invoke-direct {v0}, LX/6t3;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->b:LX/6t3;

    .line 1154028
    new-instance v0, LX/25T;

    invoke-virtual {p0}, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/25U;

    invoke-direct {v2}, LX/25U;-><init>()V

    new-instance v3, LX/25V;

    invoke-direct {v3}, LX/25V;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/25T;-><init>(Landroid/content/Context;LX/25U;LX/25V;)V

    .line 1154029
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 1154030
    iget-object v1, p0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->b:LX/6t3;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1154031
    iget-object v1, p0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1154032
    return-void
.end method


# virtual methods
.method public setCustomAmountButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1154021
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->b:LX/6t3;

    .line 1154022
    iput-object p1, v0, LX/6t3;->c:Landroid/view/View$OnClickListener;

    .line 1154023
    return-void
.end method

.method public setPaymentsComponentCallback(LX/6qh;)V
    .locals 1

    .prologue
    .line 1154017
    invoke-super {p0, p1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 1154018
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->b:LX/6t3;

    .line 1154019
    iput-object p1, v0, LX/6t3;->d:LX/6qh;

    .line 1154020
    return-void
.end method

.method public setPrices(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/6t0;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1154012
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->b:LX/6t3;

    .line 1154013
    iput-object p1, v0, LX/6t3;->a:LX/0Px;

    .line 1154014
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1154015
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->b:LX/6t3;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1154016
    return-void
.end method

.method public setSelectedPriceIndex(Ljava/lang/Integer;)V
    .locals 1
    .param p1    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1154008
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->b:LX/6t3;

    .line 1154009
    iput-object p1, v0, LX/6t3;->b:Ljava/lang/Integer;

    .line 1154010
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1154011
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1154003
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1154004
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1154005
    :goto_0
    return-void

    .line 1154006
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1154007
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/PriceSelectorView;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
