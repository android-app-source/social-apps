.class public Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

.field public static final b:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;


# instance fields
.field public c:Z

.field public d:Landroid/net/Uri;

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1155008
    invoke-static {}, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->newBuilder()LX/6tR;

    move-result-object v0

    const-string v1, "https://m.facebook.com/payments_terms"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1155009
    iput-object v1, v0, LX/6tR;->b:Landroid/net/Uri;

    .line 1155010
    move-object v0, v0

    .line 1155011
    invoke-virtual {v0}, LX/6tR;->a()Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->a:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    .line 1155012
    invoke-static {}, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->newBuilder()LX/6tR;

    move-result-object v0

    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 1155013
    iput-object v1, v0, LX/6tR;->b:Landroid/net/Uri;

    .line 1155014
    move-object v0, v0

    .line 1155015
    const/4 v1, 0x1

    .line 1155016
    iput-boolean v1, v0, LX/6tR;->a:Z

    .line 1155017
    move-object v0, v0

    .line 1155018
    invoke-virtual {v0}, LX/6tR;->a()Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->b:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    .line 1155019
    new-instance v0, LX/6tQ;

    invoke-direct {v0}, LX/6tQ;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6tR;)V
    .locals 1

    .prologue
    .line 1155020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1155021
    iget-boolean v0, p1, LX/6tR;->a:Z

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->c:Z

    .line 1155022
    iget-object v0, p1, LX/6tR;->b:Landroid/net/Uri;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->d:Landroid/net/Uri;

    .line 1155023
    iget-object v0, p1, LX/6tR;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->e:Ljava/lang/String;

    .line 1155024
    iget-object v0, p1, LX/6tR;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->f:Ljava/lang/String;

    .line 1155025
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1155026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1155027
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->c:Z

    .line 1155028
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->d:Landroid/net/Uri;

    .line 1155029
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->e:Ljava/lang/String;

    .line 1155030
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->f:Ljava/lang/String;

    .line 1155031
    return-void
.end method

.method public static newBuilder()LX/6tR;
    .locals 1

    .prologue
    .line 1155032
    new-instance v0, LX/6tR;

    invoke-direct {v0}, LX/6tR;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1155033
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1155034
    iget-boolean v0, p0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1155035
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->d:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1155036
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1155037
    iget-object v0, p0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1155038
    return-void
.end method
