.class public Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0lF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1153657
    const-class v0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;

    sput-object v0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;->c:Ljava/lang/Class;

    .line 1153658
    new-instance v0, LX/6se;

    invoke-direct {v0}, LX/6se;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 1153641
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153642
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;->a:Ljava/lang/String;

    .line 1153643
    :try_start_0
    invoke-static {p1}, LX/46R;->l(Landroid/os/Parcel;)LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1153644
    :goto_0
    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;->b:LX/0lF;

    .line 1153645
    return-void

    .line 1153646
    :catch_0
    move-exception v0

    .line 1153647
    sget-object v1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;->c:Ljava/lang/Class;

    const-string v2, "Could not read JSON from parcel"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1153648
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;LX/0lF;)V
    .locals 0
    .param p2    # LX/0lF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1153653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153654
    iput-object p1, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;->a:Ljava/lang/String;

    .line 1153655
    iput-object p2, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;->b:LX/0lF;

    .line 1153656
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1153652
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1153649
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153650
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;->b:LX/0lF;

    invoke-static {p1, v0}, LX/46R;->b(Landroid/os/Parcel;LX/0lF;)V

    .line 1153651
    return-void
.end method
