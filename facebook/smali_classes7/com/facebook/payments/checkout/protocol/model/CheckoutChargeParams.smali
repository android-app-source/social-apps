.class public Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

.field public final c:LX/6xg;

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Lcom/facebook/payments/currency/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:LX/0m9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Lcom/facebook/payments/currency/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1153575
    const-class v0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->a:Ljava/lang/String;

    .line 1153576
    new-instance v0, LX/6sc;

    invoke-direct {v0}, LX/6sc;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6sd;)V
    .locals 1

    .prologue
    .line 1153577
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153578
    iget-object v0, p1, LX/6sd;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1153579
    iget-object v0, p1, LX/6sd;->b:LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->c:LX/6xg;

    .line 1153580
    iget-object v0, p1, LX/6sd;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->d:Ljava/lang/String;

    .line 1153581
    iget-object v0, p1, LX/6sd;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->e:Ljava/lang/String;

    .line 1153582
    iget-object v0, p1, LX/6sd;->e:Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->f:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1153583
    iget-object v0, p1, LX/6sd;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->g:Ljava/lang/String;

    .line 1153584
    iget-object v0, p1, LX/6sd;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->h:Ljava/lang/String;

    .line 1153585
    iget-object v0, p1, LX/6sd;->h:LX/0m9;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->i:LX/0m9;

    .line 1153586
    iget-object v0, p1, LX/6sd;->i:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->j:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 1153587
    iget-object v0, p1, LX/6sd;->j:Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->k:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1153588
    iget-object v0, p1, LX/6sd;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->l:Ljava/lang/String;

    .line 1153589
    iget-object v0, p1, LX/6sd;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->m:Ljava/lang/String;

    .line 1153590
    iget-object v0, p1, LX/6sd;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->n:Ljava/lang/String;

    .line 1153591
    iget-object v0, p1, LX/6sd;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->o:Ljava/lang/String;

    .line 1153592
    iget-object v0, p1, LX/6sd;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->p:Ljava/lang/String;

    .line 1153593
    iget-object v0, p1, LX/6sd;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->q:Ljava/lang/String;

    .line 1153594
    iget-object v0, p1, LX/6sd;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->r:Ljava/lang/String;

    .line 1153595
    iget-object v0, p1, LX/6sd;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->s:Ljava/lang/String;

    .line 1153596
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1153597
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153598
    const-class v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1153599
    const-class v0, LX/6xg;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->c:LX/6xg;

    .line 1153600
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->d:Ljava/lang/String;

    .line 1153601
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->e:Ljava/lang/String;

    .line 1153602
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->f:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1153603
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->g:Ljava/lang/String;

    .line 1153604
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->h:Ljava/lang/String;

    .line 1153605
    invoke-static {p1}, LX/46R;->k(Landroid/os/Parcel;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/0m9;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->i:LX/0m9;

    .line 1153606
    const-class v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->j:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 1153607
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->k:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1153608
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->l:Ljava/lang/String;

    .line 1153609
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->m:Ljava/lang/String;

    .line 1153610
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->n:Ljava/lang/String;

    .line 1153611
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->o:Ljava/lang/String;

    .line 1153612
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->p:Ljava/lang/String;

    .line 1153613
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->q:Ljava/lang/String;

    .line 1153614
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->r:Ljava/lang/String;

    .line 1153615
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->s:Ljava/lang/String;

    .line 1153616
    return-void
.end method

.method public static a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xg;)LX/6sd;
    .locals 1

    .prologue
    .line 1153617
    new-instance v0, LX/6sd;

    invoke-direct {v0, p0, p1}, LX/6sd;-><init>(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xg;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1153618
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1153619
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1153620
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->c:LX/6xg;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1153621
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153622
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153623
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->f:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1153624
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153625
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153626
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->i:LX/0m9;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/0lF;)V

    .line 1153627
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->j:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1153628
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->k:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1153629
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153630
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153631
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153632
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153633
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153634
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153635
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153636
    iget-object v0, p0, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeParams;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1153637
    return-void
.end method
