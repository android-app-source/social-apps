.class public Lcom/facebook/payments/checkout/CheckoutActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public p:LX/6rt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/6wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:Lcom/facebook/payments/checkout/CheckoutParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1150644
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1150645
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/payments/checkout/CheckoutParams;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1150646
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/payments/checkout/CheckoutActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1150647
    const-string v1, "checkout_launch_mode"

    sget-object v2, LX/6qS;->POJO_CONFIG:LX/6qS;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1150648
    const-string v1, "checkout_params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1150649
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1150627
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/payments/checkout/CheckoutActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1150628
    const-string v1, "checkout_launch_mode"

    sget-object v2, LX/6qS;->JSON_ENCODED_CONFIG:LX/6qS;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1150629
    const-string v1, "checkout_config"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1150630
    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 1150651
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "checkout_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1150652
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/payments/checkout/CheckoutActivity;->r:Lcom/facebook/payments/checkout/CheckoutParams;

    .line 1150653
    new-instance v3, Lcom/facebook/payments/checkout/CheckoutFragment;

    invoke-direct {v3}, Lcom/facebook/payments/checkout/CheckoutFragment;-><init>()V

    .line 1150654
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1150655
    const-string p0, "checkout_params"

    invoke-virtual {v4, p0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1150656
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1150657
    move-object v2, v3

    .line 1150658
    const-string v3, "checkout_fragment"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1150659
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/payments/checkout/CheckoutActivity;LX/6rt;LX/6wr;)V
    .locals 0

    .prologue
    .line 1150650
    iput-object p1, p0, Lcom/facebook/payments/checkout/CheckoutActivity;->p:LX/6rt;

    iput-object p2, p0, Lcom/facebook/payments/checkout/CheckoutActivity;->q:LX/6wr;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/payments/checkout/CheckoutActivity;

    invoke-static {v1}, LX/6rt;->b(LX/0QB;)LX/6rt;

    move-result-object v0

    check-cast v0, LX/6rt;

    invoke-static {v1}, LX/6wr;->b(LX/0QB;)LX/6wr;

    move-result-object v1

    check-cast v1, LX/6wr;

    invoke-static {p0, v0, v1}, Lcom/facebook/payments/checkout/CheckoutActivity;->a(Lcom/facebook/payments/checkout/CheckoutActivity;LX/6rt;LX/6wr;)V

    return-void
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1150631
    if-eqz p1, :cond_0

    .line 1150632
    const-string v0, "checkout_params"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/CheckoutParams;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutActivity;->r:Lcom/facebook/payments/checkout/CheckoutParams;

    .line 1150633
    :goto_0
    return-void

    .line 1150634
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/CheckoutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "checkout_launch_mode"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/6qS;

    .line 1150635
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1150636
    sget-object v1, LX/6qR;->a:[I

    invoke-virtual {v0}, LX/6qS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1150637
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown LaunchMode found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1150638
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/CheckoutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "checkout_config"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1150639
    :try_start_0
    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutActivity;->p:LX/6rt;

    .line 1150640
    sget-object v2, LX/6qw;->SIMPLE:LX/6qw;

    invoke-static {v1, v0, v2}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(LX/6rt;Ljava/lang/String;LX/6qw;)Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    move-object v1, v2

    .line 1150641
    iput-object v1, p0, Lcom/facebook/payments/checkout/CheckoutActivity;->r:Lcom/facebook/payments/checkout/CheckoutParams;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1150642
    :catch_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to parse json config: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1150643
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/CheckoutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "checkout_params"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/CheckoutParams;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutActivity;->r:Lcom/facebook/payments/checkout/CheckoutParams;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1150610
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1150611
    const v0, 0x7f030597

    invoke-virtual {p0, v0}, Lcom/facebook/payments/checkout/CheckoutActivity;->setContentView(I)V

    .line 1150612
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutActivity;->r:Lcom/facebook/payments/checkout/CheckoutParams;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-boolean v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutActivity;->r:Lcom/facebook/payments/checkout/CheckoutParams;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v1, v1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-static {p0, v0, v1}, LX/6wr;->b(Landroid/app/Activity;ZLX/73i;)V

    .line 1150613
    if-nez p1, :cond_0

    .line 1150614
    invoke-direct {p0}, Lcom/facebook/payments/checkout/CheckoutActivity;->a()V

    .line 1150615
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutActivity;->r:Lcom/facebook/payments/checkout/CheckoutParams;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->a(Landroid/app/Activity;LX/6ws;)V

    .line 1150616
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1150602
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 1150603
    invoke-static {p0, p0}, Lcom/facebook/payments/checkout/CheckoutActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1150604
    invoke-direct {p0, p1}, Lcom/facebook/payments/checkout/CheckoutActivity;->d(Landroid/os/Bundle;)V

    .line 1150605
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutActivity;->q:LX/6wr;

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutActivity;->r:Lcom/facebook/payments/checkout/CheckoutParams;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-boolean v1, v1, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->d:Z

    iget-object v2, p0, Lcom/facebook/payments/checkout/CheckoutActivity;->r:Lcom/facebook/payments/checkout/CheckoutParams;

    invoke-interface {v2}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v2, v2, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    invoke-virtual {v0, p0, v1, v2}, LX/6wr;->a(Landroid/app/Activity;ZLX/73i;)V

    .line 1150606
    return-void
.end method

.method public final finish()V
    .locals 1

    .prologue
    .line 1150607
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1150608
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutActivity;->r:Lcom/facebook/payments/checkout/CheckoutParams;

    invoke-interface {v0}, Lcom/facebook/payments/checkout/CheckoutParams;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->a:LX/6ws;

    invoke-static {p0, v0}, LX/6wr;->b(Landroid/app/Activity;LX/6ws;)V

    .line 1150609
    return-void
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 1150617
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "checkout_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1150618
    const/4 v1, 0x1

    .line 1150619
    if-eqz v0, :cond_1

    instance-of v2, v0, LX/0fj;

    if-eqz v2, :cond_1

    .line 1150620
    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    move-result v0

    .line 1150621
    :goto_0
    if-eqz v0, :cond_0

    .line 1150622
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1150623
    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1150624
    const-string v0, "checkout_params"

    iget-object v1, p0, Lcom/facebook/payments/checkout/CheckoutActivity;->r:Lcom/facebook/payments/checkout/CheckoutParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1150625
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1150626
    return-void
.end method
