.class public Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152417
    new-instance v0, LX/6rZ;

    invoke-direct {v0}, LX/6rZ;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1152418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152419
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->a:Ljava/lang/String;

    .line 1152420
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->b:Ljava/lang/String;

    .line 1152421
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->c:Ljava/lang/String;

    .line 1152422
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->d:Ljava/lang/String;

    .line 1152423
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->e:Ljava/lang/String;

    .line 1152424
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1152425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152426
    iput-object p1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->a:Ljava/lang/String;

    .line 1152427
    iput-object p2, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->b:Ljava/lang/String;

    .line 1152428
    iput-object p3, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->c:Ljava/lang/String;

    .line 1152429
    iput-object p4, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->d:Ljava/lang/String;

    .line 1152430
    iput-object p5, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->e:Ljava/lang/String;

    .line 1152431
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152432
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1152433
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152434
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152435
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152436
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152437
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152438
    return-void
.end method
