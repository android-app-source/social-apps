.class public Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Z

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152318
    new-instance v0, LX/6rV;

    invoke-direct {v0}, LX/6rV;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1152319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152320
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    .line 1152321
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->b:Ljava/lang/String;

    .line 1152322
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->c:Z

    .line 1152323
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->d:LX/0Px;

    .line 1152324
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1152325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152326
    iput-object p1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    .line 1152327
    iput-object p2, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->b:Ljava/lang/String;

    .line 1152328
    iput-boolean p3, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->c:Z

    .line 1152329
    iput-object p4, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->d:LX/0Px;

    .line 1152330
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152331
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1152332
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152333
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152334
    iget-boolean v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1152335
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;->d:LX/0Px;

    .line 1152336
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1152337
    return-void
.end method
