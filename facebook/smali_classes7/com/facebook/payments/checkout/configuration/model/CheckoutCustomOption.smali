.class public Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/form/model/FormRowDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152250
    new-instance v0, LX/6rR;

    invoke-direct {v0}, LX/6rR;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1152251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152252
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;->a:Ljava/lang/String;

    .line 1152253
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;->b:Ljava/lang/String;

    .line 1152254
    const-class v0, Lcom/facebook/payments/form/model/FormRowDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;->c:LX/0Px;

    .line 1152255
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;->d:Ljava/lang/String;

    .line 1152256
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/form/model/FormRowDefinition;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1152257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152258
    iput-object p1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;->a:Ljava/lang/String;

    .line 1152259
    iput-object p2, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;->b:Ljava/lang/String;

    .line 1152260
    iput-object p3, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;->c:LX/0Px;

    .line 1152261
    iput-object p4, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;->d:Ljava/lang/String;

    .line 1152262
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152263
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1152264
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152265
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152266
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1152267
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutCustomOption;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152268
    return-void
.end method
