.class public Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/payments/currency/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152182
    new-instance v0, LX/6rN;

    invoke-direct {v0}, LX/6rN;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1152176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152177
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    .line 1152178
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b:LX/0Px;

    .line 1152179
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1152180
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->d:Ljava/lang/String;

    .line 1152181
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;Lcom/facebook/payments/currency/CurrencyAmount;Ljava/lang/String;)V
    .locals 0
    .param p2    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/payments/currency/CurrencyAmount;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1152170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152171
    iput-object p1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    .line 1152172
    iput-object p2, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b:LX/0Px;

    .line 1152173
    iput-object p3, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1152174
    iput-object p4, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->d:Ljava/lang/String;

    .line 1152175
    return-void
.end method

.method public static a(LX/0Px;LX/0Px;)LX/0Px;
    .locals 5
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1152159
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1152160
    :goto_0
    return-object p0

    .line 1152161
    :cond_0
    new-instance v3, Ljava/util/LinkedHashMap;

    new-instance v0, LX/6rM;

    invoke-direct {v0}, LX/6rM;-><init>()V

    invoke-static {p0, v0}, LX/0PM;->a(Ljava/lang/Iterable;LX/0QK;)LX/0P1;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 1152162
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    .line 1152163
    iget-object v1, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    .line 1152164
    if-nez v1, :cond_1

    .line 1152165
    iget-object v1, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1152166
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1152167
    :cond_1
    invoke-static {v1, v0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object v1

    .line 1152168
    iget-object v0, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1152169
    :cond_2
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;
    .locals 3

    .prologue
    .line 1152144
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object p0, p1

    .line 1152145
    :cond_0
    :goto_0
    return-object p0

    .line 1152146
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1152147
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1152148
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1152149
    invoke-static {p0, p1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a$redex0(Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object p0

    goto :goto_0

    .line 1152150
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    .line 1152151
    if-eqz v0, :cond_0

    invoke-static {p0, v0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object p0

    goto :goto_0

    .line 1152152
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1152153
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1152154
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1152155
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    .line 1152156
    if-eqz v0, :cond_4

    invoke-static {p1, v0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object p0

    goto :goto_0

    :cond_4
    move-object p0, p1

    goto :goto_0

    .line 1152157
    :cond_5
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b:LX/0Px;

    iget-object v2, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b:LX/0Px;

    invoke-static {v1, v2}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(LX/0Px;LX/0Px;)LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Ljava/lang/String;LX/0Px;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object p0

    goto :goto_0

    .line 1152158
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to merge "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1152142
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152143
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v1, p1}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;LX/0Px;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;)",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1152141
    new-instance v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    invoke-direct {v0, p0, p1, v1, v1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;-><init>(Ljava/lang/String;LX/0Px;Lcom/facebook/payments/currency/CurrencyAmount;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1152140
    new-instance v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    invoke-direct {v0, p0, v1, p1, v1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;-><init>(Ljava/lang/String;LX/0Px;Lcom/facebook/payments/currency/CurrencyAmount;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/0Px;)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;)",
            "Lcom/facebook/payments/currency/CurrencyAmount;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1152106
    const/4 v1, 0x0

    .line 1152107
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    .line 1152108
    invoke-virtual {v0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    .line 1152109
    if-eqz v0, :cond_2

    .line 1152110
    if-nez v1, :cond_0

    .line 1152111
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 1152112
    :cond_0
    invoke-virtual {v1, v0}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    goto :goto_1

    .line 1152113
    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private static a$redex0(Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1152136
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152137
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152138
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1152139
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v2, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v1, v2}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1152130
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1152131
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1152132
    :goto_0
    return-object v0

    .line 1152133
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1152134
    const/4 v0, 0x0

    goto :goto_0

    .line 1152135
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b:LX/0Px;

    invoke-static {v0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a(LX/0Px;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1152129
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1152128
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b:LX/0Px;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1152127
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152126
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1152120
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1152121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1152122
    :goto_0
    return-object v0

    .line 1152123
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1152124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b:LX/0Px;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1152125
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1152114
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152115
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->b:LX/0Px;

    .line 1152116
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1152117
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1152118
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152119
    return-void
.end method
