.class public Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/payments/currency/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152658
    new-instance v0, LX/6rk;

    invoke-direct {v0}, LX/6rk;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1152653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152654
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1152655
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1152656
    :goto_0
    return-void

    .line 1152657
    :cond_0
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    goto :goto_0
.end method

.method public static newBuilder()LX/6rl;
    .locals 2

    .prologue
    .line 1152652
    new-instance v0, LX/6rl;

    invoke-direct {v0}, LX/6rl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152638
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1152645
    if-ne p0, p1, :cond_1

    .line 1152646
    :cond_0
    :goto_0
    return v0

    .line 1152647
    :cond_1
    instance-of v2, p1, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;

    if-nez v2, :cond_2

    move v0, v1

    .line 1152648
    goto :goto_0

    .line 1152649
    :cond_2
    check-cast p1, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;

    .line 1152650
    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v3, p1, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1152651
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1152644
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1152639
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    if-nez v0, :cond_0

    .line 1152640
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1152641
    :goto_0
    return-void

    .line 1152642
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1152643
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0
.end method
