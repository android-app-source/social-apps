.class public Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152272
    new-instance v0, LX/6rS;

    invoke-direct {v0}, LX/6rS;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1152277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152278
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;->a:Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;

    .line 1152279
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;->b:Ljava/lang/String;

    .line 1152280
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1152281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152282
    iput-object p1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;->a:Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;

    .line 1152283
    iput-object p2, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;->b:Ljava/lang/String;

    .line 1152284
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152276
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1152273
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;->a:Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1152274
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152275
    return-void
.end method
