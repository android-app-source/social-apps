.class public Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152292
    new-instance v0, LX/6rT;

    invoke-direct {v0}, LX/6rT;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6rU;)V
    .locals 1

    .prologue
    .line 1152293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152294
    iget-object v0, p1, LX/6rU;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->a:Ljava/lang/String;

    .line 1152295
    iget-object v0, p1, LX/6rU;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->b:Ljava/lang/String;

    .line 1152296
    iget-object v0, p1, LX/6rU;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->c:Ljava/lang/String;

    .line 1152297
    iget-object v0, p1, LX/6rU;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->d:Ljava/lang/String;

    .line 1152298
    iget-object v0, p1, LX/6rU;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->e:Ljava/lang/String;

    .line 1152299
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1152300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152301
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->a:Ljava/lang/String;

    .line 1152302
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->b:Ljava/lang/String;

    .line 1152303
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->c:Ljava/lang/String;

    .line 1152304
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->d:Ljava/lang/String;

    .line 1152305
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->e:Ljava/lang/String;

    .line 1152306
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/6rU;
    .locals 2

    .prologue
    .line 1152307
    new-instance v0, LX/6rU;

    invoke-direct {v0, p0}, LX/6rU;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152308
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1152309
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152310
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152311
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152312
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152313
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152314
    return-void
.end method
