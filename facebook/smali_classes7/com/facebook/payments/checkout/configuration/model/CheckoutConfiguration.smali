.class public Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;

.field public final c:LX/6re;

.field public final d:Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152204
    new-instance v0, LX/6rO;

    invoke-direct {v0}, LX/6rO;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1152198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152199
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->a:Ljava/lang/String;

    .line 1152200
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->b:Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;

    .line 1152201
    const-class v0, LX/6re;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6re;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->c:LX/6re;

    .line 1152202
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->d:Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    .line 1152203
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;LX/6re;Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;)V
    .locals 0
    .param p4    # Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1152186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152187
    iput-object p1, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->a:Ljava/lang/String;

    .line 1152188
    iput-object p2, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->b:Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;

    .line 1152189
    iput-object p3, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->c:LX/6re;

    .line 1152190
    iput-object p4, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->d:Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    .line 1152191
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152197
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1152192
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152193
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->b:Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1152194
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->c:LX/6re;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1152195
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->d:Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1152196
    return-void
.end method
