.class public Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152529
    new-instance v0, LX/6rg;

    invoke-direct {v0}, LX/6rg;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1152530
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152531
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;->a:Ljava/lang/String;

    .line 1152532
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;->b:Ljava/lang/String;

    .line 1152533
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;->c:Ljava/lang/String;

    .line 1152534
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1152535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152536
    iput-object p1, p0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;->a:Ljava/lang/String;

    .line 1152537
    iput-object p2, p0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;->b:Ljava/lang/String;

    .line 1152538
    iput-object p3, p0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;->c:Ljava/lang/String;

    .line 1152539
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152540
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1152541
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152542
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152543
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152544
    return-void
.end method
