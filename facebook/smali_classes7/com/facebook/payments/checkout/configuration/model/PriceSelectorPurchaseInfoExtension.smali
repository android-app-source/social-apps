.class public Lcom/facebook/payments/checkout/configuration/model/PriceSelectorPurchaseInfoExtension;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/PriceSelectorPurchaseInfoExtension;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152684
    new-instance v0, LX/6ro;

    invoke-direct {v0}, LX/6ro;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorPurchaseInfoExtension;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1152685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152686
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorPurchaseInfoExtension;->a:Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    .line 1152687
    return-void
.end method


# virtual methods
.method public final a()LX/6rc;
    .locals 1

    .prologue
    .line 1152688
    sget-object v0, LX/6rc;->PRICE_SELECTOR:LX/6rc;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152689
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1152690
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorPurchaseInfoExtension;->a:Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1152691
    return-void
.end method
