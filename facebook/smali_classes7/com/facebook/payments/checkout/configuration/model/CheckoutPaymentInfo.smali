.class public Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6xg;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/0m9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152464
    new-instance v0, LX/6ra;

    invoke-direct {v0}, LX/6ra;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6rb;)V
    .locals 1

    .prologue
    .line 1152446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152447
    iget-object v0, p1, LX/6rb;->a:LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->a:LX/6xg;

    .line 1152448
    iget-object v0, p1, LX/6rb;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->b:Ljava/lang/String;

    .line 1152449
    iget-object v0, p1, LX/6rb;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->c:Ljava/lang/String;

    .line 1152450
    iget-object v0, p1, LX/6rb;->d:LX/0m9;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->d:LX/0m9;

    .line 1152451
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1152452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152453
    const-class v0, LX/6xg;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->a:LX/6xg;

    .line 1152454
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->b:Ljava/lang/String;

    .line 1152455
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->c:Ljava/lang/String;

    .line 1152456
    invoke-static {p1}, LX/46R;->k(Landroid/os/Parcel;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/0m9;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->d:LX/0m9;

    .line 1152457
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152458
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1152459
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->a:LX/6xg;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1152460
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152461
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152462
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->d:LX/0m9;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/0lF;)V

    .line 1152463
    return-void
.end method
