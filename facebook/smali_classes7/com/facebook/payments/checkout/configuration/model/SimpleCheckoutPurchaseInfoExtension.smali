.class public final Lcom/facebook/payments/checkout/configuration/model/SimpleCheckoutPurchaseInfoExtension;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/SimpleCheckoutPurchaseInfoExtension;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/6rc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152708
    new-instance v0, LX/6rq;

    invoke-direct {v0}, LX/6rq;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/SimpleCheckoutPurchaseInfoExtension;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6rc;)V
    .locals 0

    .prologue
    .line 1152709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152710
    iput-object p1, p0, Lcom/facebook/payments/checkout/configuration/model/SimpleCheckoutPurchaseInfoExtension;->a:LX/6rc;

    .line 1152711
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1152712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152713
    const-class v0, LX/6rc;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6rc;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/SimpleCheckoutPurchaseInfoExtension;->a:LX/6rc;

    .line 1152714
    return-void
.end method


# virtual methods
.method public final a()LX/6rc;
    .locals 1

    .prologue
    .line 1152715
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/SimpleCheckoutPurchaseInfoExtension;->a:LX/6rc;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152716
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1152717
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/SimpleCheckoutPurchaseInfoExtension;->a:LX/6rc;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1152718
    return-void
.end method
