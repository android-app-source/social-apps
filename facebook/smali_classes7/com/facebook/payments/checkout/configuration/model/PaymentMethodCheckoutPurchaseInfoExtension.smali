.class public Lcom/facebook/payments/checkout/configuration/model/PaymentMethodCheckoutPurchaseInfoExtension;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/PaymentMethodCheckoutPurchaseInfoExtension;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152515
    new-instance v0, LX/6rf;

    invoke-direct {v0}, LX/6rf;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/PaymentMethodCheckoutPurchaseInfoExtension;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1152516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152517
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/configuration/model/PaymentMethodCheckoutPurchaseInfoExtension;->a:Z

    .line 1152518
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 1152519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152520
    iput-boolean p1, p0, Lcom/facebook/payments/checkout/configuration/model/PaymentMethodCheckoutPurchaseInfoExtension;->a:Z

    .line 1152521
    return-void
.end method


# virtual methods
.method public final a()LX/6rc;
    .locals 1

    .prologue
    .line 1152522
    sget-object v0, LX/6rc;->PAYMENT_METHOD:LX/6rc;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152523
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1152524
    iget-boolean v0, p0, Lcom/facebook/payments/checkout/configuration/model/PaymentMethodCheckoutPurchaseInfoExtension;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1152525
    return-void
.end method
