.class public Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152222
    new-instance v0, LX/6rP;

    invoke-direct {v0}, LX/6rP;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6rQ;)V
    .locals 1

    .prologue
    .line 1152223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152224
    iget-object v0, p1, LX/6rQ;->a:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->a:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1152225
    iget-object v0, p1, LX/6rQ;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->b:LX/0Px;

    .line 1152226
    iget-object v0, p1, LX/6rQ;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->c:LX/0Px;

    .line 1152227
    iget-object v0, p1, LX/6rQ;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->d:LX/0Px;

    .line 1152228
    iget-object v0, p1, LX/6rQ;->e:Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->e:Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;

    .line 1152229
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1152230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152231
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->a:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1152232
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->b:LX/0Px;

    .line 1152233
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->c:LX/0Px;

    .line 1152234
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->d:LX/0Px;

    .line 1152235
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->e:Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;

    .line 1152236
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152237
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1152238
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->a:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1152239
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->b:LX/0Px;

    .line 1152240
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1152241
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->c:LX/0Px;

    .line 1152242
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1152243
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->d:LX/0Px;

    .line 1152244
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1152245
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->e:Lcom/facebook/payments/checkout/configuration/model/CheckoutPayActionContent;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1152246
    return-void
.end method
