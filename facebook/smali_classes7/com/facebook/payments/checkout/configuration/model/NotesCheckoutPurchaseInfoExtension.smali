.class public Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/form/model/FormFieldAttributes;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152488
    new-instance v0, LX/6rd;

    invoke-direct {v0}, LX/6rd;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1152489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152490
    const-class v0, LX/6xN;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/FormFieldAttributes;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 1152491
    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/form/model/FormFieldAttributes;)V
    .locals 0

    .prologue
    .line 1152492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152493
    iput-object p1, p0, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    .line 1152494
    return-void
.end method


# virtual methods
.method public final a()LX/6rc;
    .locals 1

    .prologue
    .line 1152495
    sget-object v0, LX/6rc;->NOTES:LX/6rc;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1152496
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    iget-object v0, v0, Lcom/facebook/payments/form/model/FormFieldAttributes;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152497
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1152498
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;->a:Lcom/facebook/payments/form/model/FormFieldAttributes;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1152499
    return-void
.end method
