.class public Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:LX/6rj;


# instance fields
.field private final b:Lcom/facebook/payments/currency/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Lcom/facebook/payments/form/model/AmountFormData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/PriceSelectorPercentageAmountModel;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1152629
    new-instance v0, LX/6rh;

    invoke-direct {v0}, LX/6rh;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 1152630
    new-instance v0, LX/6rj;

    invoke-direct {v0}, LX/6rj;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->a:LX/6rj;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 1152601
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1152602
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1152603
    iput-object v4, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1152604
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 1152605
    iput-object v4, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->c:Lcom/facebook/payments/form/model/AmountFormData;

    .line 1152606
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 1152607
    iput-object v4, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->d:Ljava/lang/Integer;

    .line 1152608
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v3, v0, [Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;

    move v1, v2

    .line 1152609
    :goto_3
    array-length v0, v3

    if-ge v1, v0, :cond_3

    .line 1152610
    sget-object v0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;

    .line 1152611
    aput-object v0, v3, v1

    .line 1152612
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1152613
    :cond_0
    const-class v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    goto :goto_0

    .line 1152614
    :cond_1
    sget-object v0, Lcom/facebook/payments/form/model/AmountFormData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/form/model/AmountFormData;

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->c:Lcom/facebook/payments/form/model/AmountFormData;

    goto :goto_1

    .line 1152615
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->d:Ljava/lang/Integer;

    goto :goto_2

    .line 1152616
    :cond_3
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->e:LX/0Px;

    .line 1152617
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v1, v0, [Lcom/facebook/payments/checkout/configuration/model/PriceSelectorPercentageAmountModel;

    .line 1152618
    :goto_4
    array-length v0, v1

    if-ge v2, v0, :cond_4

    .line 1152619
    sget-object v0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorPercentageAmountModel;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorPercentageAmountModel;

    .line 1152620
    aput-object v0, v1, v2

    .line 1152621
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1152622
    :cond_4
    invoke-static {v1}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->f:LX/0Px;

    .line 1152623
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->g:Ljava/lang/String;

    .line 1152624
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    .line 1152625
    iput-object v4, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->h:Ljava/lang/String;

    .line 1152626
    :goto_5
    return-void

    .line 1152627
    :cond_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->h:Ljava/lang/String;

    goto :goto_5
.end method

.method public static newBuilder()LX/6ri;
    .locals 2

    .prologue
    .line 1152628
    new-instance v0, LX/6ri;

    invoke-direct {v0}, LX/6ri;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1152631
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1152556
    if-ne p0, p1, :cond_1

    .line 1152557
    :cond_0
    :goto_0
    return v0

    .line 1152558
    :cond_1
    instance-of v2, p1, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    if-nez v2, :cond_2

    move v0, v1

    .line 1152559
    goto :goto_0

    .line 1152560
    :cond_2
    check-cast p1, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;

    .line 1152561
    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v3, p1, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1152562
    goto :goto_0

    .line 1152563
    :cond_3
    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->c:Lcom/facebook/payments/form/model/AmountFormData;

    iget-object v3, p1, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->c:Lcom/facebook/payments/form/model/AmountFormData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1152564
    goto :goto_0

    .line 1152565
    :cond_4
    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->d:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->d:Ljava/lang/Integer;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1152566
    goto :goto_0

    .line 1152567
    :cond_5
    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->e:LX/0Px;

    iget-object v3, p1, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->e:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1152568
    goto :goto_0

    .line 1152569
    :cond_6
    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->f:LX/0Px;

    iget-object v3, p1, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->f:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1152570
    goto :goto_0

    .line 1152571
    :cond_7
    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->g:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1152572
    goto :goto_0

    .line 1152573
    :cond_8
    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->h:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1152574
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1152555
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->c:Lcom/facebook/payments/form/model/AmountFormData;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->d:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->e:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->f:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1152575
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    if-nez v0, :cond_0

    .line 1152576
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1152577
    :goto_0
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->c:Lcom/facebook/payments/form/model/AmountFormData;

    if-nez v0, :cond_1

    .line 1152578
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1152579
    :goto_1
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->d:Ljava/lang/Integer;

    if-nez v0, :cond_2

    .line 1152580
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1152581
    :goto_2
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1152582
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_3
    if-ge v1, v3, :cond_3

    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->e:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;

    .line 1152583
    invoke-virtual {v0, p1, p2}, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorFixedAmountModel;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1152584
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1152585
    :cond_0
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1152586
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    .line 1152587
    :cond_1
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1152588
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->c:Lcom/facebook/payments/form/model/AmountFormData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/payments/form/model/AmountFormData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_1

    .line 1152589
    :cond_2
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1152590
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    .line 1152591
    :cond_3
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1152592
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_4
    if-ge v1, v3, :cond_4

    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->f:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorPercentageAmountModel;

    .line 1152593
    invoke-virtual {v0, p1, p2}, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorPercentageAmountModel;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1152594
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1152595
    :cond_4
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1152596
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->h:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 1152597
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1152598
    :goto_5
    return-void

    .line 1152599
    :cond_5
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1152600
    iget-object v0, p0, Lcom/facebook/payments/checkout/configuration/model/PriceSelectorConfig;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_5
.end method
