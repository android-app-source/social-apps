.class public Lcom/facebook/payments/checkout/CheckoutCommonParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/checkout/CheckoutParams;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/CheckoutCommonParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:LX/0m9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final B:Landroid/os/Parcelable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final C:Z

.field public final D:Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

.field public final E:Z

.field public final a:LX/6qw;

.field public final b:LX/6xg;

.field public final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/6rp;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/6re;

.field public final e:Ljava/util/Currency;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Lorg/json/JSONObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

.field public final k:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

.field public final l:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field public final m:Z

.field public final n:Z

.field public final o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final s:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/6vb;",
            ">;"
        }
    .end annotation
.end field

.field public final t:Z

.field public final u:Z

.field public final v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;",
            ">;"
        }
    .end annotation
.end field

.field public final w:Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

.field public final x:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/6so;",
            ">;"
        }
    .end annotation
.end field

.field public final y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1151011
    new-instance v0, LX/6qX;

    invoke-direct {v0}, LX/6qX;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6qZ;)V
    .locals 3

    .prologue
    .line 1150970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150971
    iget-object v0, p1, LX/6qZ;->a:LX/6qw;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    .line 1150972
    iget-object v0, p1, LX/6qZ;->b:LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    .line 1150973
    iget-object v0, p1, LX/6qZ;->d:LX/0Rf;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    .line 1150974
    iget-object v0, p1, LX/6qZ;->e:LX/6re;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->d:LX/6re;

    .line 1150975
    iget-object v0, p1, LX/6qZ;->f:Ljava/util/Currency;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->e:Ljava/util/Currency;

    .line 1150976
    iget-object v0, p1, LX/6qZ;->g:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->f:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1150977
    iget-object v0, p1, LX/6qZ;->h:LX/0Px;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->g:LX/0Px;

    .line 1150978
    iget-object v0, p1, LX/6qZ;->i:LX/0Px;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->h:LX/0Px;

    .line 1150979
    iget-object v0, p1, LX/6qZ;->j:Lorg/json/JSONObject;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->i:Lorg/json/JSONObject;

    .line 1150980
    iget-object v0, p1, LX/6qZ;->k:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1150981
    iget-object v0, p1, LX/6qZ;->l:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->k:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    .line 1150982
    iget v0, p1, LX/6qZ;->m:I

    iput v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->l:I

    .line 1150983
    iget-boolean v0, p1, LX/6qZ;->n:Z

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->m:Z

    .line 1150984
    iget-boolean v0, p1, LX/6qZ;->o:Z

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->n:Z

    .line 1150985
    iget-object v0, p1, LX/6qZ;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->o:Ljava/lang/String;

    .line 1150986
    iget-object v0, p1, LX/6qZ;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->p:Ljava/lang/String;

    .line 1150987
    iget-object v0, p1, LX/6qZ;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->q:Ljava/lang/String;

    .line 1150988
    iget-object v0, p1, LX/6qZ;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->r:Ljava/lang/String;

    .line 1150989
    iget-object v0, p1, LX/6qZ;->t:LX/0Rf;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    .line 1150990
    iget-boolean v0, p1, LX/6qZ;->u:Z

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->t:Z

    .line 1150991
    iget-boolean v0, p1, LX/6qZ;->v:Z

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->u:Z

    .line 1150992
    iget-object v0, p1, LX/6qZ;->w:LX/0Px;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->v:LX/0Px;

    .line 1150993
    iget-object v0, p1, LX/6qZ;->x:Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->w:Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    .line 1150994
    iget-object v0, p1, LX/6qZ;->y:LX/0Px;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->x:LX/0Px;

    .line 1150995
    iget-object v0, p1, LX/6qZ;->z:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->y:Ljava/lang/String;

    .line 1150996
    iget-boolean v0, p1, LX/6qZ;->A:Z

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->z:Z

    .line 1150997
    iget-object v0, p1, LX/6qZ;->B:LX/0m9;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->A:LX/0m9;

    .line 1150998
    iget-object v0, p1, LX/6qZ;->C:Landroid/os/Parcelable;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->B:Landroid/os/Parcelable;

    .line 1150999
    iget-object v0, p1, LX/6qZ;->c:Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->D:Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    .line 1151000
    iget-boolean v0, p1, LX/6qZ;->D:Z

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->E:Z

    .line 1151001
    iget-boolean v0, p1, LX/6qZ;->E:Z

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->C:Z

    .line 1151002
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1151003
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iget-object v0, v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b:LX/73i;

    sget-object p1, LX/73i;->PAYMENTS_WHITE:LX/73i;

    if-ne v0, p1, :cond_4

    move v0, v1

    :goto_0
    const-string p1, "Checkout screen should always open with White titleBarStyle."

    invoke-static {v0, p1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1151004
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    sget-object p1, LX/6rp;->CONTACT_INFO:LX/6rp;

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_0
    move v0, v1

    :goto_1
    const-string p1, "Missing ContactInfoType to show when ContactInfo purchase info needs to be collected."

    invoke-static {v0, p1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1151005
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    sget-object p1, LX/6rp;->CHECKOUT_OPTIONS:LX/6rp;

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->v:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_1
    move v0, v1

    :goto_2
    const-string p1, "Missing CheckoutOptionsPurchaseInfoExtensions to show when CheckoutOptions purchase info needs to be collected."

    invoke-static {v0, p1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1151006
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    sget-object p1, LX/6rp;->NOTE:LX/6rp;

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->w:Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    if-eqz v0, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    const-string v0, "Missing notesCheckoutPurchaseInfoExtension to show when NOTE purchase info needs to be collected."

    invoke-static {v2, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1151007
    return-void

    :cond_4
    move v0, v2

    .line 1151008
    goto :goto_0

    :cond_5
    move v0, v2

    .line 1151009
    goto :goto_1

    :cond_6
    move v0, v2

    .line 1151010
    goto :goto_2
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 1150924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150925
    const-class v0, LX/6qw;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6qw;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    .line 1150926
    const-class v0, LX/6xg;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    .line 1150927
    const-class v0, LX/6rp;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/ClassLoader;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    .line 1150928
    const-class v0, LX/6re;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6re;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->d:LX/6re;

    .line 1150929
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Currency;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->e:Ljava/util/Currency;

    .line 1150930
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->f:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1150931
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfigPrice;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->g:LX/0Px;

    .line 1150932
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutItem;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->h:LX/0Px;

    .line 1150933
    const/4 v0, 0x0

    .line 1150934
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 1150935
    if-nez v2, :cond_1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1150936
    :goto_0
    move-object v0, v0

    .line 1150937
    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->i:Lorg/json/JSONObject;

    .line 1150938
    const-class v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 1150939
    const-class v0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->k:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    .line 1150940
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->l:I

    .line 1150941
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->m:Z

    .line 1150942
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->n:Z

    .line 1150943
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->o:Ljava/lang/String;

    .line 1150944
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->p:Ljava/lang/String;

    .line 1150945
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->q:Ljava/lang/String;

    .line 1150946
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->r:Ljava/lang/String;

    .line 1150947
    const-class v0, LX/6vb;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/ClassLoader;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    .line 1150948
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->t:Z

    .line 1150949
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->u:Z

    .line 1150950
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->v:LX/0Px;

    .line 1150951
    const-class v0, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->w:Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    .line 1150952
    const-class v0, LX/6so;

    .line 1150953
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1150954
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1150955
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1150956
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1150957
    invoke-static {v0, v1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1150958
    :cond_0
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 1150959
    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->x:LX/0Px;

    .line 1150960
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->y:Ljava/lang/String;

    .line 1150961
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->z:Z

    .line 1150962
    invoke-static {p1}, LX/46R;->k(Landroid/os/Parcel;)LX/0lF;

    move-result-object v0

    check-cast v0, LX/0m9;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->A:LX/0m9;

    .line 1150963
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->B:Landroid/os/Parcelable;

    .line 1150964
    const-class v0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->D:Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    .line 1150965
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->E:Z

    .line 1150966
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->C:Z

    .line 1150967
    return-void

    .line 1150968
    :cond_1
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v0, v1

    goto/16 :goto_0

    .line 1150969
    :catch_0
    goto/16 :goto_0
.end method

.method public static a(LX/0Px;)LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutPurchaseInfoExtension;",
            ">;)",
            "LX/0Rf",
            "<",
            "LX/6rp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1150923
    invoke-static {p0}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v0

    new-instance v1, LX/6qW;

    invoke-direct {v1}, LX/6qW;-><init>()V

    invoke-virtual {v0, v1}, LX/0wv;->a(LX/0QK;)LX/0wv;

    move-result-object v0

    invoke-virtual {v0}, LX/0wv;->c()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/6qw;LX/6xg;LX/0Rf;Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;)LX/6qZ;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6qw;",
            "LX/6xg;",
            "LX/0Rf",
            "<",
            "LX/6rp;",
            ">;",
            "Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;",
            ")",
            "LX/6qZ;"
        }
    .end annotation

    .prologue
    .line 1150922
    new-instance v0, LX/6qZ;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v4}, LX/6qZ;-><init>(LX/6qw;LX/6xg;LX/0Rf;Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;)V

    return-object v0
.end method

.method public static a(LX/6rt;Ljava/lang/String;LX/6qw;)Lcom/facebook/payments/checkout/CheckoutCommonParams;
    .locals 3

    .prologue
    .line 1150893
    iget-object v0, p0, LX/6rt;->a:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1150894
    const-string v1, "checkout_configuration"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1150895
    const-string v1, "checkout_configuration"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 1150896
    const-string v2, "version"

    invoke-virtual {v1, v2}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1150897
    const-string v2, "version"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v2

    .line 1150898
    iget-object p1, p0, LX/6rt;->b:LX/6rs;

    .line 1150899
    iget-object p0, p1, LX/6rs;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/6rr;

    move-object p1, p0

    .line 1150900
    invoke-interface {p1, v2, v1}, LX/6rr;->a(Ljava/lang/String;LX/0lF;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;

    move-object v0, v1

    .line 1150901
    move-object v0, v0

    .line 1150902
    sget-object v1, LX/6xY;->CHECKOUT:LX/6xY;

    invoke-static {v1}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object v1

    invoke-virtual {v1}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6qU;

    move-result-object v1

    invoke-virtual {v1}, LX/6qU;->a()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v2

    .line 1150903
    iget-object v1, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->d:Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->d:Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    iget-object v1, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->d:LX/0Px;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->d:Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    iget-object v1, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;->d:LX/0Px;

    invoke-static {v1}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(LX/0Px;)LX/0Rf;

    move-result-object v1

    .line 1150904
    :goto_0
    new-instance p0, LX/6qZ;

    iget-object p1, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->b:Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;

    iget-object p1, p1, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->a:LX/6xg;

    invoke-direct {p0, p2, p1, v1, v2}, LX/6qZ;-><init>(LX/6qw;LX/6xg;LX/0Rf;Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;)V

    iget-object v1, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->b:Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;

    iget-object v1, v1, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->b:Ljava/lang/String;

    .line 1150905
    iput-object v1, p0, LX/6qZ;->r:Ljava/lang/String;

    .line 1150906
    move-object v1, p0

    .line 1150907
    iget-object v2, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->b:Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;

    iget-object v2, v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->c:Ljava/lang/String;

    .line 1150908
    iput-object v2, v1, LX/6qZ;->q:Ljava/lang/String;

    .line 1150909
    move-object v1, v1

    .line 1150910
    iget-object v2, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->b:Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;

    iget-object v2, v2, Lcom/facebook/payments/checkout/configuration/model/CheckoutPaymentInfo;->d:LX/0m9;

    .line 1150911
    iput-object v2, v1, LX/6qZ;->B:LX/0m9;

    .line 1150912
    move-object v1, v1

    .line 1150913
    iget-object v2, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->c:LX/6re;

    .line 1150914
    iput-object v2, v1, LX/6qZ;->e:LX/6re;

    .line 1150915
    move-object v1, v1

    .line 1150916
    iget-object v2, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->d:Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    if-eqz v2, :cond_0

    .line 1150917
    iget-object v2, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutConfiguration;->d:Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;

    invoke-static {v1, v2}, LX/6qZ;->a$redex0(LX/6qZ;Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;)LX/6qZ;

    .line 1150918
    :cond_0
    move-object v0, v1

    .line 1150919
    invoke-virtual {v0}, LX/6qZ;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    return-object v0

    .line 1150920
    :cond_1
    sget-object v1, LX/0Re;->a:LX/0Re;

    move-object v1, v1

    .line 1150921
    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/checkout/CheckoutCommonParams;
    .locals 0

    .prologue
    .line 1150892
    return-object p0
.end method

.method public final a(Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;)Lcom/facebook/payments/checkout/CheckoutCommonParams;
    .locals 1

    .prologue
    .line 1150847
    invoke-static {p0}, LX/6qZ;->a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)LX/6qZ;

    move-result-object v0

    invoke-static {v0, p1}, LX/6qZ;->a$redex0(LX/6qZ;Lcom/facebook/payments/checkout/configuration/model/CheckoutContentConfiguration;)LX/6qZ;

    move-result-object v0

    invoke-virtual {v0}, LX/6qZ;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)Lcom/facebook/payments/checkout/CheckoutParams;
    .locals 0

    .prologue
    .line 1150891
    return-object p1
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;
    .locals 2

    .prologue
    .line 1150890
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->v:LX/0Px;

    invoke-static {v0}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v0

    new-instance v1, LX/6qV;

    invoke-direct {v1, p0, p1}, LX/6qV;-><init>(Lcom/facebook/payments/checkout/CheckoutCommonParams;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0wv;->a(LX/0Rl;)LX/0wv;

    move-result-object v0

    invoke-virtual {v0}, LX/0wv;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1150889
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    .line 1150848
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a:LX/6qw;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1150849
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->b:LX/6xg;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1150850
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->c:LX/0Rf;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Set;)V

    .line 1150851
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->d:LX/6re;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1150852
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->e:Ljava/util/Currency;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1150853
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->f:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1150854
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->g:LX/0Px;

    .line 1150855
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1150856
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->h:LX/0Px;

    .line 1150857
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1150858
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->i:Lorg/json/JSONObject;

    .line 1150859
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1150860
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->j:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1150861
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->k:Lcom/facebook/payments/checkout/recyclerview/TermsAndPoliciesParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1150862
    iget v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->l:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1150863
    iget-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->m:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1150864
    iget-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->n:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1150865
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1150866
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1150867
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1150868
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->r:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1150869
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->s:LX/0Rf;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Set;)V

    .line 1150870
    iget-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->t:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1150871
    iget-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->u:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1150872
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->v:LX/0Px;

    .line 1150873
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1150874
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->w:Lcom/facebook/payments/checkout/configuration/model/NotesCheckoutPurchaseInfoExtension;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1150875
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->x:LX/0Px;

    .line 1150876
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1150877
    invoke-virtual {v0}, LX/0Px;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Enum;

    .line 1150878
    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1150879
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1150880
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1150881
    iget-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->z:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1150882
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->A:LX/0m9;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/0lF;)V

    .line 1150883
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->B:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1150884
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->D:Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1150885
    iget-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->E:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1150886
    iget-boolean v0, p0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->C:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1150887
    return-void

    .line 1150888
    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_0
.end method
