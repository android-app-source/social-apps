.class public Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

.field public final b:LX/6xZ;

.field public final c:LX/6xZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1150685
    new-instance v0, LX/6qT;

    invoke-direct {v0}, LX/6qT;-><init>()V

    sput-object v0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6qU;)V
    .locals 1

    .prologue
    .line 1150680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150681
    iget-object v0, p1, LX/6qU;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1150682
    iget-object v0, p1, LX/6qU;->b:LX/6xZ;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->b:LX/6xZ;

    .line 1150683
    iget-object v0, p1, LX/6qU;->c:LX/6xZ;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->c:LX/6xZ;

    .line 1150684
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1150675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1150676
    const-class v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 1150677
    const-class v0, LX/6xZ;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xZ;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->b:LX/6xZ;

    .line 1150678
    const-class v0, LX/6xZ;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xZ;

    iput-object v0, p0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->c:LX/6xZ;

    .line 1150679
    return-void
.end method

.method public static a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6qU;
    .locals 2

    .prologue
    .line 1150669
    new-instance v0, LX/6qU;

    invoke-direct {v0, p0}, LX/6qU;-><init>(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1150674
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1150670
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1150671
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->b:LX/6xZ;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1150672
    iget-object v0, p0, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->c:LX/6xZ;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1150673
    return-void
.end method
