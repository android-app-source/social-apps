.class public Lcom/facebook/payments/paymentsflow/uicomponents/LoadingDialog;
.super Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1162624
    invoke-direct {p0}, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x21ebeb1d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1162613
    invoke-super {p0, p1}, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->onCreate(Landroid/os/Bundle;)V

    .line 1162614
    const/16 v1, 0x2b

    const v2, -0x3e2cc685

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x69cbcdbb

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1162615
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 1162616
    iget-object v2, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v2, v2

    .line 1162617
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1162618
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a004f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1162619
    const/4 v2, 0x1

    .line 1162620
    iget-object v3, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->r:Landroid/widget/ProgressBar;

    invoke-static {v3, v2}, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->a(Landroid/view/View;Z)V

    .line 1162621
    iget-object v4, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->m:Landroid/view/View;

    goto :goto_1

    :goto_0
    invoke-static {v4, v3}, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->a(Landroid/view/View;Z)V

    .line 1162622
    const/16 v2, 0x2b

    const v3, -0x19232cbb

    invoke-static {v5, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1

    .line 1162623
    :goto_1
    const/4 v3, 0x0

    goto :goto_0
.end method
