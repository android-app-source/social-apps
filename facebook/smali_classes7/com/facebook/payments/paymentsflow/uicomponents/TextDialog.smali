.class public Lcom/facebook/payments/paymentsflow/uicomponents/TextDialog;
.super Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1162652
    invoke-direct {p0}, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;-><init>()V

    .line 1162653
    return-void
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/16 v0, 0x2a

    const v1, -0x110832f2

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1162654
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 1162655
    const v0, 0x7f0306c3

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 1162656
    const v0, 0x7f0d124e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1162657
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 1162658
    const-string v5, "header_title"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1162659
    const v0, 0x7f0d124d

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1162660
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 1162661
    const-string v5, "header_icon_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 1162662
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1162663
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->n:Landroid/widget/FrameLayout;

    invoke-static {v0, v3}, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->a(Landroid/widget/FrameLayout;Landroid/view/View;)V

    .line 1162664
    const v0, 0x7f0306c2

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1162665
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 1162666
    const-string v4, "body_text"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1162667
    iget-object v3, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->o:Landroid/widget/FrameLayout;

    invoke-static {v3, v0}, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->a(Landroid/widget/FrameLayout;Landroid/view/View;)V

    .line 1162668
    const/4 v0, 0x1

    .line 1162669
    iget-object v3, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->p:Lcom/facebook/resources/ui/FbButton;

    invoke-static {v3, v0}, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->a(Landroid/view/View;Z)V

    .line 1162670
    const v0, 0x7f081e9a

    .line 1162671
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1162672
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->p:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1162673
    new-instance v0, LX/70j;

    invoke-direct {v0, p0}, LX/70j;-><init>(Lcom/facebook/payments/paymentsflow/uicomponents/TextDialog;)V

    .line 1162674
    iget-object v3, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->p:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v3, v0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1162675
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->q:Lcom/facebook/resources/ui/FbButton;

    invoke-static {v0, v6}, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->a(Landroid/view/View;Z)V

    .line 1162676
    const/16 v0, 0x2b

    const v3, 0x34de6916

    invoke-static {v7, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 1162677
    invoke-super {p0, p1}, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1162678
    return-void
.end method
