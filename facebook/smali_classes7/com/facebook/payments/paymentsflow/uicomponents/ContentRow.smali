.class public Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/ProgressBar;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1162572
    const-class v0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1162573
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1162574
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1162575
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1162576
    invoke-direct {p0}, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->a()V

    .line 1162577
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1162593
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1162594
    invoke-direct {p0}, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->a()V

    .line 1162595
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1162578
    const v0, 0x7f030dfb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1162579
    const v0, 0x7f0d2227

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1162580
    const v0, 0x7f0d222a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1162581
    const v0, 0x7f0d222b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1162582
    const v0, 0x7f0d2229

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1162583
    const v0, 0x7f0d2228

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->f:Landroid/widget/ImageView;

    .line 1162584
    const v0, 0x7f0d222c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->g:Landroid/widget/ProgressBar;

    .line 1162585
    sget-object v0, LX/70h;->INACTIVE:LX/70h;

    invoke-virtual {p0, v0}, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->setAccessoryTextViewMode(LX/70h;)V

    .line 1162586
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->setAccessoryImageViewVisibility(I)V

    .line 1162587
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 1162588
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;->onMeasure(II)V

    .line 1162589
    invoke-virtual {p0}, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b13e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->setMeasuredDimension(II)V

    .line 1162590
    return-void
.end method

.method public setAccessoryImageOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1162591
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1162592
    return-void
.end method

.method public setAccessoryImageViewVisibility(I)V
    .locals 1

    .prologue
    .line 1162559
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1162560
    return-void
.end method

.method public setAccessoryText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1162565
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1162566
    return-void
.end method

.method public setAccessoryTextViewMode(LX/70h;)V
    .locals 3

    .prologue
    .line 1162567
    const v0, 0x7f0a068a

    .line 1162568
    sget-object v1, LX/70h;->INACTIVE:LX/70h;

    if-ne p1, v1, :cond_0

    .line 1162569
    const v0, 0x7f0a0689

    .line 1162570
    :cond_0
    iget-object v1, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1162571
    return-void
.end method

.method public setAccessoryTextViewOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1162563
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1162564
    return-void
.end method

.method public setAccessoryTextVisibility(I)V
    .locals 1

    .prologue
    .line 1162561
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1162562
    return-void
.end method

.method public setImageResource(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1162557
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1162558
    return-void
.end method

.method public setImageUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1162555
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1162556
    return-void
.end method

.method public setProgress(I)V
    .locals 2

    .prologue
    .line 1162552
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->g:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 1162553
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1162554
    return-void
.end method

.method public setProgressBarIndeterminate(Z)V
    .locals 1

    .prologue
    .line 1162550
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 1162551
    return-void
.end method

.method public setProgressBarVisibility(I)V
    .locals 1

    .prologue
    .line 1162548
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1162549
    return-void
.end method

.method public setSubtitleText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1162546
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1162547
    return-void
.end method

.method public setTitleText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1162544
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/ContentRow;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1162545
    return-void
.end method
