.class public Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Landroid/view/View;

.field public n:Landroid/widget/FrameLayout;

.field public o:Landroid/widget/FrameLayout;

.field public p:Lcom/facebook/resources/ui/FbButton;

.field public q:Lcom/facebook/resources/ui/FbButton;

.field public r:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1162596
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 1162597
    if-eqz p1, :cond_0

    .line 1162598
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1162599
    :goto_0
    return-void

    .line 1162600
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(Landroid/widget/FrameLayout;Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1162601
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1162602
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3ee22a56

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1162603
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1162604
    const/16 v1, 0x2b

    const v2, 0x4c19945d    # 4.0259956E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2915c2fc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1162605
    const v0, 0x7f03069e

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1162606
    const v0, 0x7f0d0cbc

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->m:Landroid/view/View;

    .line 1162607
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->m:Landroid/view/View;

    const v3, 0x7f0d04f9

    invoke-static {v0, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->n:Landroid/widget/FrameLayout;

    .line 1162608
    iget-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->m:Landroid/view/View;

    const v3, 0x7f0d0626

    invoke-static {v0, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->o:Landroid/widget/FrameLayout;

    .line 1162609
    const v0, 0x7f0d0526

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->q:Lcom/facebook/resources/ui/FbButton;

    .line 1162610
    const v0, 0x7f0d0525

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->p:Lcom/facebook/resources/ui/FbButton;

    .line 1162611
    const v0, 0x7f0d05b0

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/payments/paymentsflow/uicomponents/DialogBase;->r:Landroid/widget/ProgressBar;

    .line 1162612
    const/16 v0, 0x2b

    const v3, 0x65431be5

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
