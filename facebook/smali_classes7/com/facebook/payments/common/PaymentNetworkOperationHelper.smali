.class public Lcom/facebook/payments/common/PaymentNetworkOperationHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0TD;

.field public final c:LX/0Sh;

.field public final e:LX/0aG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1155437
    new-instance v0, LX/6ty;

    invoke-direct {v0}, LX/6ty;-><init>()V

    sput-object v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->d:LX/0QK;

    .line 1155438
    const/4 v0, 0x0

    invoke-static {v0}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v0

    sput-object v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->a:LX/0QK;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/0TD;LX/0Sh;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1155439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1155440
    iput-object p1, p0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->e:LX/0aG;

    .line 1155441
    iput-object p2, p0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b:LX/0TD;

    .line 1155442
    iput-object p3, p0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->c:LX/0Sh;

    .line 1155443
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;
    .locals 4

    .prologue
    .line 1155444
    new-instance v3, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, LX/0TD;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;-><init>(LX/0aG;LX/0TD;LX/0Sh;)V

    .line 1155445
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/os/Parcelable;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1155446
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1155447
    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1155448
    move-object v0, v0

    .line 1155449
    iget-object v1, p0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->e:LX/0aG;

    sget-object v4, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    const v6, 0x2a659e43

    move-object v2, p3

    move-object v3, v0

    invoke-static/range {v1 .. v6}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    move-object v0, v1

    .line 1155450
    return-object v0
.end method
