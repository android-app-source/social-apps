.class public Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/7Tk;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/7Tj;

.field public c:Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;

.field public d:Lcom/facebook/common/locale/Country;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6u8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1155518
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1155519
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->e:Ljava/util/List;

    return-void
.end method

.method public static a(Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;)Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;
    .locals 2

    .prologue
    .line 1155513
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1155514
    const-string v1, "view_params"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1155515
    new-instance v1, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    invoke-direct {v1}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;-><init>()V

    .line 1155516
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1155517
    return-object v1
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    const-class p0, LX/7Tk;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/7Tk;

    iput-object v1, p1, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->a:LX/7Tk;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;Lcom/facebook/common/locale/Country;)V
    .locals 3

    .prologue
    .line 1155508
    iget-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->d:Lcom/facebook/common/locale/Country;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->d:Lcom/facebook/common/locale/Country;

    invoke-virtual {v0}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1155509
    :cond_0
    return-void

    .line 1155510
    :cond_1
    iput-object p1, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->d:Lcom/facebook/common/locale/Country;

    .line 1155511
    iget-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6u8;

    .line 1155512
    iget-object v2, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->d:Lcom/facebook/common/locale/Country;

    invoke-interface {v0, v2}, LX/6u8;->a(Lcom/facebook/common/locale/Country;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/6u8;)V
    .locals 1

    .prologue
    .line 1155520
    iget-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1155521
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1155504
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1155505
    const-class v0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;

    invoke-static {v0, p0}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->a(Ljava/lang/Class;LX/02k;)V

    .line 1155506
    iget-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->a:LX/7Tk;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/7Tk;->a(Landroid/content/Context;Z)LX/7Tj;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->b:LX/7Tj;

    .line 1155507
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x4dab0654

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1155495
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1155496
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1155497
    const-string v2, "view_params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;

    iput-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->c:Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;

    .line 1155498
    if-eqz p1, :cond_0

    const-string v0, "selected_country"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    .line 1155499
    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->a$redex0(Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;Lcom/facebook/common/locale/Country;)V

    .line 1155500
    const v0, 0x1afb9854

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 1155501
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->c:Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;

    .line 1155502
    iget-object v2, v0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;->a:Lcom/facebook/common/locale/Country;

    move-object v0, v2

    .line 1155503
    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1155492
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1155493
    const-string v0, "selected_country"

    iget-object v1, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;->d:Lcom/facebook/common/locale/Country;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1155494
    return-void
.end method
