.class public Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;
.super Lcom/facebook/payments/ui/PaymentFormEditTextView;
.source ""


# instance fields
.field public a:LX/6uB;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1155606
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;-><init>(Landroid/content/Context;)V

    .line 1155607
    invoke-direct {p0}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->e()V

    .line 1155608
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1155609
    invoke-direct {p0, p1, p2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1155610
    invoke-direct {p0}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->e()V

    .line 1155611
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1155596
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/payments/ui/PaymentFormEditTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1155597
    invoke-direct {p0}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->e()V

    .line 1155598
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    invoke-static {v0}, LX/6uB;->a(LX/0QB;)LX/6uB;

    move-result-object v0

    check-cast v0, LX/6uB;

    iput-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->a:LX/6uB;

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1155602
    const-class v0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;

    invoke-static {v0, p0}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1155603
    invoke-virtual {p0}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081e81

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1155604
    iget-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->a:LX/6uB;

    invoke-virtual {v0, p0}, LX/6uB;->a(Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;)V

    .line 1155605
    return-void
.end method


# virtual methods
.method public getSelectedCountry()Lcom/facebook/common/locale/Country;
    .locals 1

    .prologue
    .line 1155601
    iget-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->a:LX/6uB;

    invoke-virtual {v0}, LX/6uB;->a()Lcom/facebook/common/locale/Country;

    move-result-object v0

    return-object v0
.end method

.method public setComponentController(Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;)V
    .locals 1

    .prologue
    .line 1155599
    iget-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorView;->a:LX/6uB;

    invoke-virtual {v0, p1}, LX/6uB;->a(Lcom/facebook/payments/common/country/PaymentsCountrySelectorComponentController;)V

    .line 1155600
    return-void
.end method
