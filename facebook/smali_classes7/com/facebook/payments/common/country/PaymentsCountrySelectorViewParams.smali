.class public Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/6uE;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/common/locale/Country;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1155651
    new-instance v0, LX/6uC;

    invoke-direct {v0}, LX/6uC;-><init>()V

    sput-object v0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6uD;)V
    .locals 1

    .prologue
    .line 1155668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1155669
    iget-object v0, p1, LX/6uD;->b:Lcom/facebook/common/locale/Country;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;->a:Lcom/facebook/common/locale/Country;

    .line 1155670
    return-void
.end method

.method public synthetic constructor <init>(LX/6uD;B)V
    .locals 0

    .prologue
    .line 1155667
    invoke-direct {p0, p1}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;-><init>(LX/6uD;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1155664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1155665
    const-class v0, Lcom/facebook/common/locale/Country;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;->a:Lcom/facebook/common/locale/Country;

    .line 1155666
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 1155663
    invoke-direct {p0, p1}, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static newBuilder()LX/6uD;
    .locals 2

    .prologue
    .line 1155671
    new-instance v0, LX/6uD;

    invoke-direct {v0}, LX/6uD;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1155662
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1155655
    if-ne p0, p1, :cond_1

    .line 1155656
    :cond_0
    :goto_0
    return v0

    .line 1155657
    :cond_1
    instance-of v2, p1, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 1155658
    goto :goto_0

    .line 1155659
    :cond_2
    check-cast p1, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;

    .line 1155660
    iget-object v2, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;->a:Lcom/facebook/common/locale/Country;

    iget-object v3, p1, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;->a:Lcom/facebook/common/locale/Country;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1155661
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1155654
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;->a:Lcom/facebook/common/locale/Country;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1155652
    iget-object v0, p0, Lcom/facebook/payments/common/country/PaymentsCountrySelectorViewParams;->a:Lcom/facebook/common/locale/Country;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1155653
    return-void
.end method
