.class public Lcom/facebook/payments/ui/ProgressLayout;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field private a:F

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private final j:LX/4AU;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1166136
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/payments/ui/ProgressLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1166137
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1166134
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/payments/ui/ProgressLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1166135
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1166130
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1166131
    new-instance v0, LX/4AU;

    invoke-direct {v0}, LX/4AU;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/ui/ProgressLayout;->j:LX/4AU;

    .line 1166132
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/payments/ui/ProgressLayout;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1166133
    return-void
.end method

.method private a(II)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 1166117
    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getChildCount()I

    move-result v9

    move v8, v3

    move v6, v3

    move v7, v3

    .line 1166118
    :goto_0
    if-ge v8, v9, :cond_0

    .line 1166119
    invoke-virtual {p0, v8}, Lcom/facebook/payments/ui/ProgressLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1166120
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_1

    .line 1166121
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1166122
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int v10, v2, v4

    .line 1166123
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int v11, v2, v0

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    .line 1166124
    invoke-virtual/range {v0 .. v5}, Lcom/facebook/payments/ui/ProgressLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 1166125
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v10

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1166126
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v11

    add-int/2addr v0, v6

    move v1, v2

    .line 1166127
    :goto_1
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v6, v0

    move v7, v1

    goto :goto_0

    .line 1166128
    :cond_0
    invoke-direct {p0, v7, v6}, Lcom/facebook/payments/ui/ProgressLayout;->b(II)V

    .line 1166129
    return-void

    :cond_1
    move v0, v6

    move v1, v7

    goto :goto_1
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x41800000    # 16.0f

    .line 1166030
    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/facebook/payments/ui/ProgressLayout;->a:F

    .line 1166031
    sget-object v0, LX/03r;->ProgressLayout:[I

    invoke-virtual {p1, p2, v0, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1166032
    const/16 v1, 0x1

    iget v2, p0, Lcom/facebook/payments/ui/ProgressLayout;->a:F

    mul-float/2addr v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/facebook/payments/ui/ProgressLayout;->d:I

    .line 1166033
    const/16 v1, 0x2

    const/high16 v2, 0x40800000    # 4.0f

    iget v3, p0, Lcom/facebook/payments/ui/ProgressLayout;->a:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/facebook/payments/ui/ProgressLayout;->e:I

    .line 1166034
    const/16 v1, 0x0

    iget v2, p0, Lcom/facebook/payments/ui/ProgressLayout;->a:F

    mul-float/2addr v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/facebook/payments/ui/ProgressLayout;->b:I

    .line 1166035
    const/16 v1, 0x3

    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a03a0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/payments/ui/ProgressLayout;->f:I

    .line 1166036
    const/16 v1, 0x4

    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00fd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/payments/ui/ProgressLayout;->g:I

    .line 1166037
    iget-object v1, p0, Lcom/facebook/payments/ui/ProgressLayout;->j:LX/4AU;

    iget v2, p0, Lcom/facebook/payments/ui/ProgressLayout;->b:I

    .line 1166038
    iget v3, v1, LX/4AU;->g:I

    if-eq v3, v2, :cond_0

    .line 1166039
    iput v2, v1, LX/4AU;->g:I

    .line 1166040
    invoke-virtual {v1}, LX/4AU;->invalidateSelf()V

    .line 1166041
    :cond_0
    iget-object v1, p0, Lcom/facebook/payments/ui/ProgressLayout;->j:LX/4AU;

    const/4 v2, 0x1

    .line 1166042
    iget-boolean v3, v1, LX/4AU;->k:Z

    if-eq v3, v2, :cond_1

    .line 1166043
    iput-boolean v2, v1, LX/4AU;->k:Z

    .line 1166044
    invoke-virtual {v1}, LX/4AU;->invalidateSelf()V

    .line 1166045
    :cond_1
    iget-object v1, p0, Lcom/facebook/payments/ui/ProgressLayout;->j:LX/4AU;

    iget v2, p0, Lcom/facebook/payments/ui/ProgressLayout;->b:I

    div-int/lit8 v2, v2, 0x2

    .line 1166046
    iget v3, v1, LX/4AU;->i:I

    if-eq v3, v2, :cond_2

    .line 1166047
    iput v2, v1, LX/4AU;->i:I

    .line 1166048
    invoke-virtual {v1}, LX/4AU;->invalidateSelf()V

    .line 1166049
    :cond_2
    iget-object v1, p0, Lcom/facebook/payments/ui/ProgressLayout;->j:LX/4AU;

    iget v2, p0, Lcom/facebook/payments/ui/ProgressLayout;->f:I

    .line 1166050
    iget v3, v1, LX/4AU;->e:I

    if-eq v3, v2, :cond_3

    .line 1166051
    iput v2, v1, LX/4AU;->e:I

    .line 1166052
    invoke-virtual {v1}, LX/4AU;->invalidateSelf()V

    .line 1166053
    :cond_3
    iget-object v1, p0, Lcom/facebook/payments/ui/ProgressLayout;->j:LX/4AU;

    iget v2, p0, Lcom/facebook/payments/ui/ProgressLayout;->g:I

    .line 1166054
    iget v3, v1, LX/4AU;->d:I

    if-eq v3, v2, :cond_4

    .line 1166055
    iput v2, v1, LX/4AU;->d:I

    .line 1166056
    invoke-virtual {v1}, LX/4AU;->invalidateSelf()V

    .line 1166057
    :cond_4
    iget-object v1, p0, Lcom/facebook/payments/ui/ProgressLayout;->j:LX/4AU;

    .line 1166058
    iget v2, v1, LX/4AU;->f:I

    if-eq v2, v5, :cond_5

    .line 1166059
    iput v5, v1, LX/4AU;->f:I

    .line 1166060
    invoke-virtual {v1}, LX/4AU;->invalidateSelf()V

    .line 1166061
    :cond_5
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1166062
    return-void
.end method

.method private b(II)V
    .locals 0

    .prologue
    .line 1166114
    iput p1, p0, Lcom/facebook/payments/ui/ProgressLayout;->h:I

    .line 1166115
    iput p2, p0, Lcom/facebook/payments/ui/ProgressLayout;->i:I

    .line 1166116
    return-void
.end method

.method private c(II)V
    .locals 9

    .prologue
    .line 1166100
    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getChildCount()I

    move-result v3

    .line 1166101
    const/4 v0, 0x0

    move v2, v0

    move v1, p2

    :goto_0
    if-ge v2, v3, :cond_0

    .line 1166102
    invoke-virtual {p0, v2}, Lcom/facebook/payments/ui/ProgressLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1166103
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v5, 0x8

    if-eq v0, v5, :cond_1

    .line 1166104
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1166105
    invoke-static {v0}, LX/1ck;->a(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v5

    .line 1166106
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 1166107
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    .line 1166108
    add-int/2addr v5, p1

    .line 1166109
    iget v8, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v8

    .line 1166110
    add-int/2addr v6, v5

    add-int v8, v1, v7

    invoke-virtual {v4, v5, v1, v6, v8}, Landroid/view/View;->layout(IIII)V

    .line 1166111
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v7

    add-int/2addr v0, v1

    .line 1166112
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1166113
    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1166097
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1166098
    iget-object v0, p0, Lcom/facebook/payments/ui/ProgressLayout;->j:LX/4AU;

    invoke-virtual {v0, p1}, LX/4AU;->draw(Landroid/graphics/Canvas;)V

    .line 1166099
    return-void
.end method

.method public final measureChildWithMargins(Landroid/view/View;IIII)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2c

    const v1, 0x6d6a567f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1166092
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1166093
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v2, p3

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v2, v3

    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p2, v2, v3}, Lcom/facebook/payments/ui/ProgressLayout;->getChildMeasureSpec(III)I

    move-result v2

    .line 1166094
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v3, p5

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v3, v4

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p4, v3, v0}, Lcom/facebook/payments/ui/ProgressLayout;->getChildMeasureSpec(III)I

    move-result v0

    .line 1166095
    invoke-virtual {p1, v2, v0}, Landroid/view/View;->measure(II)V

    .line 1166096
    const/16 v0, 0x2d

    const v2, -0x2a548f7a

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 1166084
    iget v0, p0, Lcom/facebook/payments/ui/ProgressLayout;->b:I

    iget v1, p0, Lcom/facebook/payments/ui/ProgressLayout;->d:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 1166085
    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getPaddingTop()I

    move-result v1

    iget v2, p0, Lcom/facebook/payments/ui/ProgressLayout;->e:I

    add-int/2addr v1, v2

    .line 1166086
    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getPaddingLeft()I

    move-result v2

    iget v3, p0, Lcom/facebook/payments/ui/ProgressLayout;->d:I

    add-int/2addr v2, v3

    .line 1166087
    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getPaddingLeft()I

    move-result v3

    add-int/2addr v0, v3

    .line 1166088
    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getPaddingTop()I

    move-result v3

    .line 1166089
    iget-object v4, p0, Lcom/facebook/payments/ui/ProgressLayout;->j:LX/4AU;

    iget v5, p0, Lcom/facebook/payments/ui/ProgressLayout;->b:I

    add-int/2addr v5, v2

    iget v6, p0, Lcom/facebook/payments/ui/ProgressLayout;->c:I

    add-int/2addr v6, v1

    invoke-virtual {v4, v2, v1, v5, v6}, LX/4AU;->setBounds(IIII)V

    .line 1166090
    invoke-direct {p0, v0, v3}, Lcom/facebook/payments/ui/ProgressLayout;->c(II)V

    .line 1166091
    return-void
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1166067
    iput v5, p0, Lcom/facebook/payments/ui/ProgressLayout;->h:I

    .line 1166068
    iput v5, p0, Lcom/facebook/payments/ui/ProgressLayout;->i:I

    .line 1166069
    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 1166070
    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    .line 1166071
    iget v2, p0, Lcom/facebook/payments/ui/ProgressLayout;->b:I

    iget v3, p0, Lcom/facebook/payments/ui/ProgressLayout;->d:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 1166072
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    sub-int/2addr v2, v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 1166073
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    sub-int/2addr v3, v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 1166074
    invoke-direct {p0, v2, v3}, Lcom/facebook/payments/ui/ProgressLayout;->a(II)V

    .line 1166075
    iget v2, p0, Lcom/facebook/payments/ui/ProgressLayout;->h:I

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1166076
    iget v3, p0, Lcom/facebook/payments/ui/ProgressLayout;->i:I

    invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1166077
    add-int/2addr v0, v2

    .line 1166078
    add-int/2addr v1, v3

    .line 1166079
    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getSuggestedMinimumWidth()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1166080
    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1166081
    iget v2, p0, Lcom/facebook/payments/ui/ProgressLayout;->e:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, v1, v2

    iput v2, p0, Lcom/facebook/payments/ui/ProgressLayout;->c:I

    .line 1166082
    invoke-static {v0, p1}, Lcom/facebook/payments/ui/ProgressLayout;->resolveSize(II)I

    move-result v0

    invoke-static {v1, p2}, Lcom/facebook/payments/ui/ProgressLayout;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/payments/ui/ProgressLayout;->setMeasuredDimension(II)V

    .line 1166083
    return-void
.end method

.method public setProgress(I)V
    .locals 3

    .prologue
    .line 1166063
    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getChildCount()I

    move-result v0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1166064
    iget-object v1, p0, Lcom/facebook/payments/ui/ProgressLayout;->j:LX/4AU;

    mul-int/lit16 v0, v0, 0x2710

    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->getChildCount()I

    move-result v2

    div-int/2addr v0, v2

    invoke-virtual {v1, v0}, LX/4AU;->setLevel(I)Z

    .line 1166065
    invoke-virtual {p0}, Lcom/facebook/payments/ui/ProgressLayout;->invalidate()V

    .line 1166066
    return-void
.end method
