.class public Lcom/facebook/payments/ui/PaymentFormEditTextView;
.super Landroid/support/design/widget/TextInputLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:Lcom/facebook/resources/ui/FbEditText;

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1155594
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1155595
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1155592
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1155593
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1155587
    invoke-direct {p0, p1, p2, p3}, Landroid/support/design/widget/TextInputLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1155588
    invoke-direct {p0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->e()V

    .line 1155589
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a(Landroid/content/Context;)V

    .line 1155590
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1155591
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1155579
    new-instance v0, Lcom/facebook/resources/ui/FbEditText;

    invoke-direct {v0, p1}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    .line 1155580
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    const v1, 0x10000006

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setImeOptions(I)V

    .line 1155581
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setSingleLine(Z)V

    .line 1155582
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b055a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setTextSize(IF)V

    .line 1155583
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {p0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0a40

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1155584
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->addView(Landroid/view/View;)V

    .line 1155585
    invoke-direct {p0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->f()V

    .line 1155586
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1155567
    sget-object v0, LX/03r;->PaymentFormEditText:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1155568
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 1155569
    invoke-virtual {p0, v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setMaxLength(I)V

    .line 1155570
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1155571
    if-lez v1, :cond_0

    .line 1155572
    invoke-virtual {p0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1155573
    :cond_0
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1155574
    if-lez v1, :cond_1

    .line 1155575
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 1155576
    invoke-virtual {p0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 1155577
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1155578
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 1155561
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1155562
    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1155563
    const v0, 0x7f0e013e

    invoke-virtual {p0, v0}, Landroid/support/design/widget/TextInputLayout;->setHintTextAppearance(I)V

    .line 1155564
    const/4 v0, 0x1

    .line 1155565
    iput-boolean v0, p0, Landroid/support/design/widget/TextInputLayout;->r:Z

    .line 1155566
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 1155559
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1155560
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 1155557
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1155558
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1155553
    invoke-virtual {p0, p1}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 1155554
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 1155555
    return-void

    .line 1155556
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1155550
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->b:Z

    .line 1155551
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setEnabled(Z)V

    .line 1155552
    return-void
.end method

.method public final b(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 1155548
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1155549
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1155522
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setSelection(I)V

    .line 1155523
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1155545
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    .line 1155546
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/design/widget/TextInputLayout;->setErrorEnabled(Z)V

    .line 1155547
    return-void
.end method

.method public getInputText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1155544
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 1155541
    iget-boolean v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->b:Z

    if-nez v0, :cond_0

    .line 1155542
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setEnabled(Z)V

    .line 1155543
    :cond_0
    return-void
.end method

.method public setInputId(I)V
    .locals 1

    .prologue
    .line 1155539
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setId(I)V

    .line 1155540
    return-void
.end method

.method public setInputText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1155537
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1155538
    return-void
.end method

.method public setInputType(I)V
    .locals 1

    .prologue
    .line 1155535
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setInputType(I)V

    .line 1155536
    return-void
.end method

.method public setMaxLength(I)V
    .locals 4

    .prologue
    .line 1155532
    if-lez p1, :cond_0

    .line 1155533
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v3, p1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1155534
    :cond_0
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1155528
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 1155529
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setFocusable(Z)V

    .line 1155530
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1155531
    return-void
.end method

.method public setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V
    .locals 1

    .prologue
    .line 1155526
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1155527
    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1

    .prologue
    .line 1155524
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentFormEditTextView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1155525
    return-void
.end method
