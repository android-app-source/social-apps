.class public Lcom/facebook/payments/ui/FloatingLabelTextView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1165676
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1165677
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1165678
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165673
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1165674
    invoke-direct {p0, p1, p2}, Lcom/facebook/payments/ui/FloatingLabelTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1165675
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165670
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1165671
    invoke-direct {p0, p1, p2}, Lcom/facebook/payments/ui/FloatingLabelTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1165672
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165663
    const v0, 0x7f030673

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1165664
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setOrientation(I)V

    .line 1165665
    const v0, 0x7f0d11c7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1165666
    const v0, 0x7f0d02a7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1165667
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->c:Landroid/widget/ProgressBar;

    .line 1165668
    invoke-direct {p0, p1, p2}, Lcom/facebook/payments/ui/FloatingLabelTextView;->b(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1165669
    return-void
.end method

.method private b(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1165654
    if-nez p2, :cond_0

    .line 1165655
    :goto_0
    return-void

    .line 1165656
    :cond_0
    sget-object v0, LX/03r;->FloatingLabelTextView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1165657
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 1165658
    iget-object v2, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setSingleLine(Z)V

    .line 1165659
    invoke-virtual {p0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x1

    invoke-static {v1, v0, v2}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 1165660
    const/16 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    .line 1165661
    iget-object v2, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v4, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(IF)V

    .line 1165662
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 1165652
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->a:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1165653
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1165650
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1165651
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1165644
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->removeView(Landroid/view/View;)V

    .line 1165645
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 1165646
    iget-object v1, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->a:Lcom/facebook/widget/text/BetterTextView;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->addView(Landroid/view/View;I)V

    .line 1165647
    invoke-virtual {p0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0052

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setTextSize(F)V

    .line 1165648
    invoke-virtual {p0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setHintSize(F)V

    .line 1165649
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1165640
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->a:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1165641
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1165642
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1165643
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1165619
    invoke-virtual {p0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->c()V

    .line 1165620
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->c:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1165621
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1165622
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->c:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1165623
    invoke-direct {p0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->g()V

    .line 1165624
    return-void
.end method

.method public getTextSize()F
    .locals 1

    .prologue
    .line 1165625
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getTextSize()F

    move-result v0

    return v0
.end method

.method public setHint(I)V
    .locals 1

    .prologue
    .line 1165626
    invoke-direct {p0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->f()V

    .line 1165627
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1165628
    return-void
.end method

.method public setHint(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1165629
    invoke-direct {p0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->f()V

    .line 1165630
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1165631
    return-void
.end method

.method public setHintSize(F)V
    .locals 1
    .param p1    # F
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 1165632
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1165633
    return-void
.end method

.method public setText(I)V
    .locals 1

    .prologue
    .line 1165634
    invoke-direct {p0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->g()V

    .line 1165635
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1165636
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1165637
    invoke-direct {p0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->g()V

    .line 1165638
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1165639
    return-void
.end method

.method public setTextSize(F)V
    .locals 1
    .param p1    # F
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 1165617
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1165618
    return-void
.end method
