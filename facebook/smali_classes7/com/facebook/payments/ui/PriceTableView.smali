.class public Lcom/facebook/payments/ui/PriceTableView;
.super LX/6E7;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1165972
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1165973
    invoke-direct {p0}, Lcom/facebook/payments/ui/PriceTableView;->a()V

    .line 1165974
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165975
    invoke-direct {p0, p1, p2}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1165976
    invoke-direct {p0}, Lcom/facebook/payments/ui/PriceTableView;->a()V

    .line 1165977
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165978
    invoke-direct {p0, p1, p2, p3}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1165979
    invoke-direct {p0}, Lcom/facebook/payments/ui/PriceTableView;->a()V

    .line 1165980
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1165981
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/PriceTableView;->setOrientation(I)V

    .line 1165982
    return-void
.end method


# virtual methods
.method public setRowDatas(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/73b;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1165983
    invoke-virtual {p0}, Lcom/facebook/payments/ui/PriceTableView;->removeAllViews()V

    .line 1165984
    invoke-virtual {p0}, Lcom/facebook/payments/ui/PriceTableView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 1165985
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/73b;

    .line 1165986
    const v1, 0x7f03165f

    invoke-virtual {v4, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/ui/PriceTableRowView;

    .line 1165987
    invoke-virtual {v1, v0}, Lcom/facebook/payments/ui/PriceTableRowView;->setRowData(LX/73b;)V

    .line 1165988
    invoke-virtual {p0, v1}, Lcom/facebook/payments/ui/PriceTableView;->addView(Landroid/view/View;)V

    .line 1165989
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1165990
    :cond_0
    return-void
.end method
