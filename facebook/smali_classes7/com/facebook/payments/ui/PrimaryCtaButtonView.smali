.class public Lcom/facebook/payments/ui/PrimaryCtaButtonView;
.super LX/73U;
.source ""


# instance fields
.field public a:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/widget/text/BetterButton;

.field private c:Landroid/widget/ProgressBar;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1166024
    invoke-direct {p0, p1}, LX/73U;-><init>(Landroid/content/Context;)V

    .line 1166025
    invoke-direct {p0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->g()V

    .line 1166026
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1166021
    invoke-direct {p0, p1, p2}, LX/73U;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1166022
    invoke-direct {p0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->g()V

    .line 1166023
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1166018
    invoke-direct {p0, p1, p2, p3}, LX/73U;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1166019
    invoke-direct {p0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->g()V

    .line 1166020
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    iput-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->a:LX/23P;

    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 1166010
    const-class v0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;

    invoke-static {v0, p0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1166011
    const v0, 0x7f031011

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1166012
    const v0, 0x7f0d268f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b:Lcom/facebook/widget/text/BetterButton;

    .line 1166013
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->c:Landroid/widget/ProgressBar;

    .line 1166014
    const v0, 0x7f0d0b20

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->d:Landroid/view/View;

    .line 1166015
    iget-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->c:Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b056a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0vv;->f(Landroid/view/View;F)V

    .line 1166016
    iget-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->d:Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b056a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0vv;->f(Landroid/view/View;F)V

    .line 1166017
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1166007
    iget-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b:Lcom/facebook/widget/text/BetterButton;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setAlpha(F)V

    .line 1166008
    iget-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->c:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1166009
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1166027
    iget-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b:Lcom/facebook/widget/text/BetterButton;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setAlpha(F)V

    .line 1166028
    iget-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->c:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1166029
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1166004
    iget-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b:Lcom/facebook/widget/text/BetterButton;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setAlpha(F)V

    .line 1166005
    iget-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1166006
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1166001
    iget-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b:Lcom/facebook/widget/text/BetterButton;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setAlpha(F)V

    .line 1166002
    iget-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1166003
    return-void
.end method

.method public setCtaButtonText(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1165999
    invoke-virtual {p0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->setCtaButtonText(Ljava/lang/CharSequence;)V

    .line 1166000
    return-void
.end method

.method public setCtaButtonText(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 1165997
    iget-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b:Lcom/facebook/widget/text/BetterButton;

    iget-object v1, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->a:LX/23P;

    iget-object v2, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v1, p1, v2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setText(Ljava/lang/CharSequence;)V

    .line 1165998
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 1165994
    invoke-super {p0, p1}, LX/73U;->setEnabled(Z)V

    .line 1165995
    iget-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterButton;->setEnabled(Z)V

    .line 1165996
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1165991
    invoke-super {p0, p1}, LX/73U;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1165992
    iget-object v0, p0, Lcom/facebook/payments/ui/PrimaryCtaButtonView;->b:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1165993
    return-void
.end method
