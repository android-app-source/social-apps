.class public Lcom/facebook/payments/ui/PriceTableRowView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1165969
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1165970
    invoke-direct {p0}, Lcom/facebook/payments/ui/PriceTableRowView;->a()V

    .line 1165971
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1165966
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1165967
    invoke-direct {p0}, Lcom/facebook/payments/ui/PriceTableRowView;->a()V

    .line 1165968
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165953
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1165954
    invoke-direct {p0}, Lcom/facebook/payments/ui/PriceTableRowView;->a()V

    .line 1165955
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1165962
    const v0, 0x7f031010

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1165963
    const v0, 0x7f0d268d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/payments/ui/PriceTableRowView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1165964
    const v0, 0x7f0d268e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/payments/ui/PriceTableRowView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1165965
    return-void
.end method


# virtual methods
.method public setRowData(LX/73b;)V
    .locals 3

    .prologue
    .line 1165956
    iget-object v0, p0, Lcom/facebook/payments/ui/PriceTableRowView;->a:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p1, LX/73b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1165957
    iget-object v0, p0, Lcom/facebook/payments/ui/PriceTableRowView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p1, LX/73b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1165958
    iget-boolean v0, p1, LX/73b;->c:Z

    if-eqz v0, :cond_0

    .line 1165959
    iget-object v0, p0, Lcom/facebook/payments/ui/PriceTableRowView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/payments/ui/PriceTableRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e039d

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1165960
    iget-object v0, p0, Lcom/facebook/payments/ui/PriceTableRowView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/payments/ui/PriceTableRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e039d

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1165961
    :cond_0
    return-void
.end method
