.class public Lcom/facebook/payments/ui/PaymentsComponentViewGroup;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements LX/6E6;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:LX/6qh;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1153734
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1153735
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1153732
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1153733
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1153730
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1153731
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1153728
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a:LX/6qh;

    invoke-virtual {v0, p1}, LX/6qh;->b(Landroid/content/Intent;)V

    .line 1153729
    return-void
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 1153726
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a:LX/6qh;

    invoke-virtual {v0, p1, p2}, LX/6qh;->a(Landroid/content/Intent;I)V

    .line 1153727
    return-void
.end method

.method public final b(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 1153722
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a:LX/6qh;

    invoke-virtual {v0, p1, p2}, LX/6qh;->b(Landroid/content/Intent;I)V

    .line 1153723
    return-void
.end method

.method public setPaymentsComponentCallback(LX/6qh;)V
    .locals 0

    .prologue
    .line 1153724
    iput-object p1, p0, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a:LX/6qh;

    .line 1153725
    return-void
.end method
