.class public Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;
.super LX/6E7;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field public b:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field private c:LX/73M;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1165587
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1165588
    invoke-direct {p0}, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->a()V

    .line 1165589
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1165590
    invoke-direct {p0, p1, p2}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1165591
    invoke-direct {p0}, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->a()V

    .line 1165592
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1165601
    invoke-direct {p0, p1, p2, p3}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1165602
    invoke-direct {p0}, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->a()V

    .line 1165603
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1165593
    const v0, 0x7f030672

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1165594
    invoke-direct {p0}, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->b()V

    .line 1165595
    const v0, 0x7f0d11c5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1165596
    const v0, 0x7f0d11c6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 1165597
    return-void
.end method

.method private a(LX/73O;)V
    .locals 2

    .prologue
    .line 1165583
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->a:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p1, LX/73O;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1165584
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->a:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/73J;

    invoke-direct {v1, p0}, LX/73J;-><init>(Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1165585
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->a:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1165586
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1165598
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->setGravity(I)V

    .line 1165599
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->setOrientation(I)V

    .line 1165600
    return-void
.end method

.method private b(LX/73O;)V
    .locals 2

    .prologue
    .line 1165574
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    iget-object v1, p1, LX/73O;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 1165575
    iget-object v0, p1, LX/73O;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1165576
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->d()V

    .line 1165577
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1165578
    :goto_0
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setVisibility(I)V

    .line 1165579
    return-void

    .line 1165580
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->e()V

    .line 1165581
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    iget-object v1, p1, LX/73O;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1165582
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    new-instance v1, LX/73K;

    invoke-direct {v1, p0}, LX/73K;-><init>(Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1165571
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1165572
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setVisibility(I)V

    .line 1165573
    return-void
.end method


# virtual methods
.method public getSelectedItemView()Lcom/facebook/payments/ui/FloatingLabelTextView;
    .locals 1

    .prologue
    .line 1165570
    iget-object v0, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    return-object v0
.end method

.method public setListener(LX/73M;)V
    .locals 0

    .prologue
    .line 1165562
    iput-object p1, p0, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->c:LX/73M;

    .line 1165563
    return-void
.end method

.method public setViewParams(LX/73O;)V
    .locals 3

    .prologue
    .line 1165564
    invoke-direct {p0}, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->c()V

    .line 1165565
    sget-object v0, LX/73L;->a:[I

    iget-object v1, p1, LX/73O;->a:LX/73N;

    invoke-virtual {v1}, LX/73N;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1165566
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown mode seen: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, LX/73O;->a:LX/73N;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1165567
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->a(LX/73O;)V

    .line 1165568
    :goto_0
    return-void

    .line 1165569
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/FloatingLabelMultiOptionsView;->b(LX/73O;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
