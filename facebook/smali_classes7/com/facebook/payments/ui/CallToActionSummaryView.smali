.class public Lcom/facebook/payments/ui/CallToActionSummaryView;
.super Lcom/facebook/payments/ui/PaymentsSecurityInfoView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1165521
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;-><init>(Landroid/content/Context;)V

    .line 1165522
    invoke-direct {p0}, Lcom/facebook/payments/ui/CallToActionSummaryView;->a()V

    .line 1165523
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165518
    invoke-direct {p0, p1, p2}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1165519
    invoke-direct {p0}, Lcom/facebook/payments/ui/CallToActionSummaryView;->a()V

    .line 1165520
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165515
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1165516
    invoke-direct {p0}, Lcom/facebook/payments/ui/CallToActionSummaryView;->a()V

    .line 1165517
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1165513
    invoke-virtual {p0}, Lcom/facebook/payments/ui/CallToActionSummaryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-static {v0, v1}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v0

    int-to-float v0, v0

    invoke-super {p0, v0}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->setTextSize(F)V

    .line 1165514
    return-void
.end method
