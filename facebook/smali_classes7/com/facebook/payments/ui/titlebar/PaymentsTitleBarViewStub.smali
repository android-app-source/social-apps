.class public final Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:LX/63V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0h5;

.field private c:Landroid/support/v7/widget/SearchView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Landroid/support/v7/widget/Toolbar;

.field public e:LX/63J;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1166293
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1166294
    invoke-direct {p0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->c()V

    .line 1166295
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1166290
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1166291
    invoke-direct {p0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->c()V

    .line 1166292
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1166264
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1166265
    invoke-direct {p0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->c()V

    .line 1166266
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 1166287
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 1166288
    invoke-direct {p0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->c()V

    .line 1166289
    return-void
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/support/v7/widget/Toolbar;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1166280
    invoke-direct {p0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->d()V

    .line 1166281
    const v0, 0x7f030f20

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->d:Landroid/support/v7/widget/Toolbar;

    .line 1166282
    new-instance v0, LX/63h;

    iget-object v1, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->d:Landroid/support/v7/widget/Toolbar;

    invoke-direct {v0, v1}, LX/63h;-><init>(Landroid/support/v7/widget/Toolbar;)V

    iput-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    .line 1166283
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->d:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0d24b8

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SearchView;

    iput-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->c:Landroid/support/v7/widget/SearchView;

    .line 1166284
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->c:Landroid/support/v7/widget/SearchView;

    const v1, 0x7f0d0352

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1166285
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1166286
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->d:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method private a(Landroid/view/ViewGroup;LX/73i;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1166269
    invoke-virtual {p0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1166270
    sget-object v1, LX/73i;->PAYMENTS_WHITE:LX/73i;

    if-ne p2, v1, :cond_0

    .line 1166271
    invoke-direct {p0, v0, p1}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/support/v7/widget/Toolbar;

    move-result-object v0

    .line 1166272
    :goto_0
    invoke-static {p1, p0, v0}, LX/478;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    .line 1166273
    return-void

    .line 1166274
    :cond_0
    iget-object v1, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a:LX/63V;

    invoke-virtual {v1}, LX/63V;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1166275
    const v1, 0x7f030023

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 1166276
    new-instance v1, LX/63h;

    invoke-direct {v1, v0}, LX/63h;-><init>(Landroid/support/v7/widget/Toolbar;)V

    iput-object v1, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    goto :goto_0

    .line 1166277
    :cond_1
    const v1, 0x7f031500

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1166278
    invoke-static {v0}, LX/63Z;->a(Landroid/view/View;)Z

    .line 1166279
    const v1, 0x7f0d00bc

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/0h5;

    iput-object v1, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    invoke-static {v0}, LX/63V;->a(LX/0QB;)LX/63V;

    move-result-object v0

    check-cast v0, LX/63V;

    iput-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a:LX/63V;

    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 1166267
    const-class v0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;

    invoke-static {v0, p0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1166268
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 1166296
    invoke-virtual {p0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1166297
    invoke-virtual {p0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b13a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1166298
    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1166299
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1166231
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    new-instance v1, LX/73j;

    invoke-direct {v1, p0}, LX/73j;-><init>(Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;)V

    invoke-interface {v0, v1}, LX/0h5;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    .line 1166232
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 1166233
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    instance-of v0, v0, LX/63h;

    if-eqz v0, :cond_0

    .line 1166234
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 1166235
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    iget-object v1, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->e:LX/63J;

    invoke-interface {v0, v1}, LX/0h5;->setOnBackPressedListener(LX/63J;)V

    .line 1166236
    :goto_0
    return-void

    .line 1166237
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    new-instance v1, LX/73k;

    invoke-direct {v1, p0}, LX/73k;-><init>(Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1166238
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 1166239
    return-void
.end method


# virtual methods
.method public final a()Landroid/support/v7/widget/SearchView;
    .locals 2

    .prologue
    .line 1166240
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->c:Landroid/support/v7/widget/SearchView;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166241
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->d:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0d143d

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1166242
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->c:Landroid/support/v7/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setVisibility(I)V

    .line 1166243
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->c:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 1166244
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->c:Landroid/support/v7/widget/SearchView;

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;LX/63J;LX/73i;LX/73h;)V
    .locals 2

    .prologue
    .line 1166245
    iput-object p2, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->e:LX/63J;

    .line 1166246
    invoke-direct {p0, p1, p3}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->a(Landroid/view/ViewGroup;LX/73i;)V

    .line 1166247
    sget-object v0, LX/73l;->b:[I

    invoke-virtual {p4}, LX/73h;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1166248
    :goto_0
    return-void

    .line 1166249
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->e()V

    goto :goto_0

    .line 1166250
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->f()V

    goto :goto_0

    .line 1166251
    :pswitch_2
    invoke-direct {p0}, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->g()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;LX/73i;)V
    .locals 3

    .prologue
    .line 1166252
    sget-object v0, LX/73l;->a:[I

    invoke-virtual {p2}, LX/73i;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1166253
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid titleBarStyle provided: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1166254
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1166255
    :goto_0
    return-void

    .line 1166256
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->d:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0d143d

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 1166257
    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1166258
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->c:Landroid/support/v7/widget/SearchView;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1166259
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->d:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0d143d

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1166260
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->c:Landroid/support/v7/widget/SearchView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setVisibility(I)V

    .line 1166261
    return-void
.end method

.method public final getFbTitleBar()LX/0h5;
    .locals 1

    .prologue
    .line 1166230
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->b:LX/0h5;

    return-object v0
.end method

.method public final setAppIconVisibility(I)V
    .locals 2

    .prologue
    .line 1166262
    iget-object v0, p0, Lcom/facebook/payments/ui/titlebar/PaymentsTitleBarViewStub;->d:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0d17e0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1166263
    return-void
.end method
