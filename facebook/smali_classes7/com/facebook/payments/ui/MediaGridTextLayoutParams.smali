.class public Lcom/facebook/payments/ui/MediaGridTextLayoutParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/ui/MediaGridTextLayoutParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1165818
    new-instance v0, LX/73Q;

    invoke-direct {v0}, LX/73Q;-><init>()V

    sput-object v0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/73R;)V
    .locals 1

    .prologue
    .line 1165819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1165820
    iget-object v0, p1, LX/73R;->c:LX/0Px;

    if-nez v0, :cond_0

    .line 1165821
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1165822
    :goto_0
    iput-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->a:LX/0Px;

    .line 1165823
    iget-object v0, p1, LX/73R;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->b:Ljava/lang/String;

    .line 1165824
    iget-object v0, p1, LX/73R;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->c:Ljava/lang/String;

    .line 1165825
    iget-object v0, p1, LX/73R;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->d:Ljava/lang/String;

    .line 1165826
    iget-object v0, p1, LX/73R;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->e:Ljava/lang/String;

    .line 1165827
    iget-object v0, p1, LX/73R;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->f:Ljava/lang/String;

    .line 1165828
    return-void

    .line 1165829
    :cond_0
    iget-object v0, p1, LX/73R;->c:LX/0Px;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1165830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1165831
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1165832
    const/4 v0, 0x0

    .line 1165833
    :goto_0
    move-object v0, v0

    .line 1165834
    iput-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->a:LX/0Px;

    .line 1165835
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->b:Ljava/lang/String;

    .line 1165836
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->c:Ljava/lang/String;

    .line 1165837
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->d:Ljava/lang/String;

    .line 1165838
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->e:Ljava/lang/String;

    .line 1165839
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->f:Ljava/lang/String;

    .line 1165840
    return-void

    :cond_0
    invoke-static {p1}, LX/46R;->j(Landroid/os/Parcel;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)LX/73R;
    .locals 2

    .prologue
    .line 1165841
    new-instance v0, LX/73R;

    invoke-direct {v0, p0}, LX/73R;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1165842
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1165843
    iget-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->a:LX/0Px;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 1165844
    iget-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1165845
    iget-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1165846
    iget-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1165847
    iget-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1165848
    iget-object v0, p0, Lcom/facebook/payments/ui/MediaGridTextLayoutParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1165849
    return-void
.end method
