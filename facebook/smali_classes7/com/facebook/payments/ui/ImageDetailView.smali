.class public Lcom/facebook/payments/ui/ImageDetailView;
.super Lcom/facebook/payments/ui/PaymentsComponentViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1165717
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;)V

    .line 1165718
    invoke-direct {p0}, Lcom/facebook/payments/ui/ImageDetailView;->a()V

    .line 1165719
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165714
    invoke-direct {p0, p1, p2}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1165715
    invoke-direct {p0}, Lcom/facebook/payments/ui/ImageDetailView;->a()V

    .line 1165716
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165711
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1165712
    invoke-direct {p0}, Lcom/facebook/payments/ui/ImageDetailView;->a()V

    .line 1165713
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1165679
    const v0, 0x7f0308d0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1165680
    const v0, 0x7f0d0340

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1165681
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1165682
    const v0, 0x7f0d02c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1165683
    const v0, 0x7f0d16e3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1165684
    return-void
.end method


# virtual methods
.method public final a(LX/0xq;LX/0xr;)V
    .locals 4

    .prologue
    .line 1165709
    iget-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, LX/0xq;->ordinal()I

    move-result v1

    invoke-static {v1}, LX/0xq;->fromIndex(I)LX/0xq;

    move-result-object v1

    invoke-virtual {p2}, LX/0xr;->ordinal()I

    move-result v2

    invoke-static {v2}, LX/0xr;->fromIndex(I)LX/0xr;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/payments/ui/ImageDetailView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3}, Lcom/facebook/widget/text/BetterTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 1165710
    return-void
.end method

.method public setAuxTextView(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165704
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1165705
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->d:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1165706
    :goto_0
    return-void

    .line 1165707
    :cond_1
    iget-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1165708
    iget-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->d:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setImageUrl(Landroid/net/Uri;)V
    .locals 2
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165699
    if-nez p1, :cond_0

    .line 1165700
    iget-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1165701
    :goto_0
    return-void

    .line 1165702
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-class v1, Lcom/facebook/payments/ui/ImageDetailView;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1165703
    iget-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSubtitle(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1165696
    iget-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1165697
    iget-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->c:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1165698
    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1165692
    iget-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1165693
    iget-object v1, p0, Lcom/facebook/payments/ui/ImageDetailView;->c:Lcom/facebook/widget/text/BetterTextView;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1165694
    return-void

    .line 1165695
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setTitle(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1165689
    iget-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1165690
    iget-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1165691
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1165685
    iget-object v0, p0, Lcom/facebook/payments/ui/ImageDetailView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1165686
    iget-object v1, p0, Lcom/facebook/payments/ui/ImageDetailView;->b:Lcom/facebook/widget/text/BetterTextView;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1165687
    return-void

    .line 1165688
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
