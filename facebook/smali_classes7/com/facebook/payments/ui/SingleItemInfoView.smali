.class public Lcom/facebook/payments/ui/SingleItemInfoView;
.super LX/6E7;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;

.field private final d:Lcom/facebook/widget/text/BetterTextView;

.field private e:LX/73f;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1166164
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/payments/ui/SingleItemInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1166165
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1166166
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/payments/ui/SingleItemInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1166167
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1166168
    invoke-direct {p0, p1, p2, p3}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1166169
    const v0, 0x7f03133d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1166170
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/SingleItemInfoView;->setOrientation(I)V

    .line 1166171
    const v0, 0x7f0d1590

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1166172
    const v0, 0x7f0d0a0a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1166173
    const v0, 0x7f0d0738

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1166174
    const v0, 0x7f0d2c99

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1166175
    return-void
.end method

.method private static a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1166176
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1166177
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1166178
    :goto_0
    return-void

    .line 1166179
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1166180
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public setViewParams(LX/73f;)V
    .locals 3

    .prologue
    .line 1166181
    iput-object p1, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->e:LX/73f;

    .line 1166182
    iget-object v0, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->e:LX/73f;

    iget-object v0, v0, LX/73f;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1166183
    iget-object v0, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1166184
    :goto_0
    iget-object v0, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->e:LX/73f;

    iget-object v1, v1, LX/73f;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/payments/ui/SingleItemInfoView;->a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V

    .line 1166185
    iget-object v0, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->e:LX/73f;

    iget-object v1, v1, LX/73f;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/payments/ui/SingleItemInfoView;->a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V

    .line 1166186
    iget-object v0, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->d:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->e:LX/73f;

    iget-object v1, v1, LX/73f;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/payments/ui/SingleItemInfoView;->a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V

    .line 1166187
    return-void

    .line 1166188
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->e:LX/73f;

    iget-object v1, v1, LX/73f;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-class v2, Lcom/facebook/payments/ui/SingleItemInfoView;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1166189
    iget-object v0, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1166190
    iget-object v0, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1166191
    iget-object v1, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->e:LX/73f;

    iget v1, v1, LX/73f;->b:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1166192
    iget-object v1, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->e:LX/73f;

    iget v1, v1, LX/73f;->b:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1166193
    iget-object v1, p0, Lcom/facebook/payments/ui/SingleItemInfoView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method
