.class public Lcom/facebook/payments/ui/PaymentsSecurityInfoView;
.super LX/6E7;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1165506
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1165507
    invoke-direct {p0}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->a()V

    .line 1165508
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165503
    invoke-direct {p0, p1, p2}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1165504
    invoke-direct {p0}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->a()V

    .line 1165505
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1165500
    invoke-direct {p0, p1, p2, p3}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1165501
    invoke-direct {p0}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->a()V

    .line 1165502
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1165493
    const v0, 0x7f030f1f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1165494
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->setOrientation(I)V

    .line 1165495
    const v0, 0x7f0d02a7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1165496
    const v0, 0x7f0d247b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1165497
    const v0, 0x7f0d24b6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->c:Landroid/view/View;

    .line 1165498
    const v0, 0x7f0d24b7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->d:Landroid/view/View;

    .line 1165499
    return-void
.end method

.method private a(Landroid/view/View;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1165489
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1165490
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1165491
    new-instance v0, LX/73Z;

    invoke-direct {v0, p0, p2}, LX/73Z;-><init>(Lcom/facebook/payments/ui/PaymentsSecurityInfoView;Landroid/net/Uri;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1165492
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1165509
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-direct {p0, v0, p1}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->a(Landroid/view/View;Landroid/net/Uri;)V

    .line 1165510
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1165511
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->d:Landroid/view/View;

    invoke-direct {p0, v0, p2}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->a(Landroid/view/View;Landroid/net/Uri;)V

    .line 1165512
    return-void
.end method

.method public setLearnMoreText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1165487
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1165488
    return-void
.end method

.method public setLearnMoreUri(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1165485
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-direct {p0, v0, p1}, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->a(Landroid/view/View;Landroid/net/Uri;)V

    .line 1165486
    return-void
.end method

.method public setText(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1165483
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1165484
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1165481
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1165482
    return-void
.end method

.method public setTextSize(F)V
    .locals 1

    .prologue
    .line 1165479
    iget-object v0, p0, Lcom/facebook/payments/ui/PaymentsSecurityInfoView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(F)V

    .line 1165480
    return-void
.end method
