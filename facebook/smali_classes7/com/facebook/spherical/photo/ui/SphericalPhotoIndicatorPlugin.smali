.class public Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements LX/7Co;


# instance fields
.field public a:LX/1xG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;

.field private final c:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

.field private final d:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

.field private final e:Ljava/lang/Runnable;

.field public f:Z

.field private g:Z

.field public h:Z

.field private i:Landroid/os/Handler;

.field public j:LX/7Dj;

.field public k:Ljava/lang/String;

.field private l:F

.field private m:F

.field private n:F

.field private o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1183920
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1183921
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1183922
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1183923
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1183924
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1183925
    new-instance v0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin$1;

    invoke-direct {v0, p0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin$1;-><init>(Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;)V

    iput-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->e:Ljava/lang/Runnable;

    .line 1183926
    iput-boolean v3, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f:Z

    .line 1183927
    iput-boolean v2, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->g:Z

    .line 1183928
    iput-boolean v2, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->h:Z

    .line 1183929
    iput v1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->l:F

    .line 1183930
    iput v1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->m:F

    .line 1183931
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->n:F

    .line 1183932
    const-class v0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-static {v0, p0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1183933
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1183934
    const v1, 0x7f031396

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1183935
    const v0, 0x7f0d2d41

    invoke-virtual {p0, v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;

    iput-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->b:Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;

    .line 1183936
    const v0, 0x7f0d2d42

    invoke-virtual {p0, v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    iput-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->c:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    .line 1183937
    new-instance v0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-direct {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->d:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    .line 1183938
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->i:Landroid/os/Handler;

    .line 1183939
    invoke-virtual {p0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->b()V

    .line 1183940
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-static {v0}, LX/1xG;->b(LX/0QB;)LX/1xG;

    move-result-object v0

    check-cast v0, LX/1xG;

    iput-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->a:LX/1xG;

    return-void
.end method

.method private a(FF)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1183941
    iget v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->n:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    cmpl-float v0, p1, v1

    if-eqz v0, :cond_0

    cmpl-float v0, p2, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1183911
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->d:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    if-eqz v0, :cond_0

    .line 1183912
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->d:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    .line 1183913
    iget-object v1, v0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1183914
    iget-object v1, v0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->end()V

    .line 1183915
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f:Z

    .line 1183916
    return-void
.end method

.method public final a(FFF)V
    .locals 3

    .prologue
    .line 1183942
    iget-boolean v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->o:Z

    if-nez v0, :cond_1

    .line 1183943
    :cond_0
    :goto_0
    return-void

    .line 1183944
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1183945
    iput p1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->l:F

    .line 1183946
    iput p2, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->m:F

    .line 1183947
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->n:F

    .line 1183948
    :goto_1
    iget v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->n:F

    const/high16 v1, 0x41f00000    # 30.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1183949
    iget-boolean v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->h:Z

    if-nez v0, :cond_2

    .line 1183950
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->a:LX/1xG;

    iget-object v1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->j:LX/7Dj;

    .line 1183951
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "spherical_photo_significant_movement"

    invoke-direct {p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1183952
    invoke-static {v0, p1, v1, v2}, LX/1xG;->a(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V

    .line 1183953
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->h:Z

    .line 1183954
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->h()V

    .line 1183955
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f:Z

    goto :goto_0

    .line 1183956
    :cond_3
    iget v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->l:F

    sub-float v0, p1, v0

    .line 1183957
    iget v1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->m:F

    sub-float v1, p2, v1

    .line 1183958
    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->n:F

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;LX/7Dj;)V
    .locals 0

    .prologue
    .line 1183959
    iput-object p1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->k:Ljava/lang/String;

    .line 1183960
    iput-object p2, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->j:LX/7Dj;

    .line 1183961
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1183962
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->c:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->setVisibility(I)V

    .line 1183963
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1183964
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->c:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->setVisibility(I)V

    .line 1183965
    return-void
.end method

.method public final d()V
    .locals 9

    .prologue
    .line 1183966
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->d:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    iget-object v1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->b:Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x12c

    const-wide/16 v6, 0x7d0

    const/4 v8, -0x1

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->a(Lcom/facebook/spherical/ui/Spherical360GyroAnimationView;JJJI)V

    .line 1183967
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->d:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->g()V

    .line 1183968
    return-void
.end method

.method public final e()V
    .locals 10

    .prologue
    const-wide/16 v2, 0x12c

    .line 1183917
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->d:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    iget-object v1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->c:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    const-wide/16 v6, 0x7d0

    const-wide/16 v8, 0x0

    move-wide v4, v2

    invoke-virtual/range {v0 .. v9}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->a(Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;JJJJ)V

    .line 1183918
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->d:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->h()V

    .line 1183919
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1183969
    invoke-virtual {p0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->j()V

    .line 1183970
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->d:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->c()V

    .line 1183971
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->d:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->b()V

    .line 1183972
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->o:Z

    .line 1183973
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->e:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1183974
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1183871
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->d:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    .line 1183872
    iget-object p0, v0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->e:Landroid/animation/ObjectAnimator;

    if-eqz p0, :cond_0

    .line 1183873
    iget-object p0, v0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->e:Landroid/animation/ObjectAnimator;

    invoke-virtual {p0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1183874
    :cond_0
    return-void
.end method

.method public getShouldShowPhoneAnimationInFullScreen()Z
    .locals 1

    .prologue
    .line 1183875
    iget-boolean v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f:Z

    return v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1183876
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->d:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    .line 1183877
    iget-object p0, v0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->f:Landroid/animation/ObjectAnimator;

    if-eqz p0, :cond_0

    .line 1183878
    iget-object p0, v0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->f:Landroid/animation/ObjectAnimator;

    invoke-virtual {p0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1183879
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 5

    .prologue
    .line 1183880
    invoke-virtual {p0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->l()V

    .line 1183881
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa0

    const v4, 0x5321dee0

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1183882
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 1183883
    iget-boolean v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->g:Z

    if-nez v0, :cond_0

    .line 1183884
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f:Z

    .line 1183885
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->k()V

    .line 1183886
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->o:Z

    .line 1183887
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->e:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1183888
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1183889
    iput v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->l:F

    .line 1183890
    iput v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->m:F

    .line 1183891
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->n:F

    .line 1183892
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 1183893
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->o:Z

    .line 1183894
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 1183895
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->o:Z

    .line 1183896
    return-void
.end method

.method public setBasePhoneAndNuxOffset(I)V
    .locals 1

    .prologue
    .line 1183897
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->c:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    if-eqz v0, :cond_0

    .line 1183898
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->c:Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;

    .line 1183899
    iput p1, v0, Lcom/facebook/spherical/ui/Spherical360PhoneAnimationView;->j:I

    .line 1183900
    :cond_0
    return-void
.end method

.method public setHasAlreadyLoggedSignificantMovement(Z)V
    .locals 0

    .prologue
    .line 1183901
    iput-boolean p1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->h:Z

    .line 1183902
    return-void
.end method

.method public setListener(LX/7Nl;)V
    .locals 1

    .prologue
    .line 1183903
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->d:Lcom/facebook/spherical/ui/SphericalNuxAnimationController;

    .line 1183904
    iput-object p1, v0, Lcom/facebook/spherical/ui/SphericalNuxAnimationController;->b:LX/7Nl;

    .line 1183905
    return-void
.end method

.method public setShouldShowPhoneAnimation(Z)V
    .locals 0

    .prologue
    .line 1183906
    iput-boolean p1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f:Z

    .line 1183907
    return-void
.end method

.method public setShouldShowPhoneAnimationInFullScreen(Z)V
    .locals 1

    .prologue
    .line 1183908
    iput-boolean p1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f:Z

    .line 1183909
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->g:Z

    .line 1183910
    return-void
.end method
