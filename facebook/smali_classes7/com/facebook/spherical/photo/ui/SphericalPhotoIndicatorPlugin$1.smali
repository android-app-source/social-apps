.class public final Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;)V
    .locals 0

    .prologue
    .line 1183861
    iput-object p1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin$1;->a:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1183862
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin$1;->a:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    iget-boolean v0, v0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f:Z

    if-eqz v0, :cond_0

    .line 1183863
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin$1;->a:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    iget-object v0, v0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->a:LX/1xG;

    iget-object v1, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin$1;->a:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    iget-object v1, v1, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin$1;->a:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    iget-object v2, v2, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->j:LX/7Dj;

    .line 1183864
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "spherical_photo_phone_pan_animation"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1183865
    invoke-static {v0, v3, v1, v2}, LX/1xG;->a(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V

    .line 1183866
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin$1;->a:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->c()V

    .line 1183867
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin$1;->a:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->e()V

    .line 1183868
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin$1;->a:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    const/4 v1, 0x0

    .line 1183869
    iput-boolean v1, v0, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->f:Z

    .line 1183870
    :cond_0
    return-void
.end method
