.class public Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;
.super Landroid/widget/FrameLayout;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/7Dh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1xG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/resources/ui/FbButton;

.field private f:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

.field private g:LX/03R;

.field private h:LX/7Dj;

.field private i:Ljava/lang/String;

.field private final j:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1183860
    const-class v0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1183858
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1183859
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1183856
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1183857
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1183847
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1183848
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->g:LX/03R;

    .line 1183849
    new-instance v0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin$1;

    invoke-direct {v0, p0}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin$1;-><init>(Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;)V

    iput-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->j:Ljava/lang/Runnable;

    .line 1183850
    const-class v0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;

    invoke-static {v0, p0}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1183851
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1183852
    const v1, 0x7f0315f8

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1183853
    const v0, 0x7f0d3173

    invoke-virtual {p0, v0}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->e:Lcom/facebook/resources/ui/FbButton;

    .line 1183854
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080d52

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1183855
    return-void
.end method

.method private static a(Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;Lcom/facebook/content/SecureContextHelper;LX/7Dh;LX/1xG;)V
    .locals 0

    .prologue
    .line 1183846
    iput-object p1, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->b:LX/7Dh;

    iput-object p3, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->c:LX/1xG;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v2}, LX/7Dh;->a(LX/0QB;)LX/7Dh;

    move-result-object v1

    check-cast v1, LX/7Dh;

    invoke-static {v2}, LX/1xG;->b(LX/0QB;)LX/1xG;

    move-result-object v2

    check-cast v2, LX/1xG;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->a(Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;Lcom/facebook/content/SecureContextHelper;LX/7Dh;LX/1xG;)V

    return-void
.end method

.method private static a(Landroid/content/pm/PackageManager;)Z
    .locals 2

    .prologue
    .line 1183842
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1183843
    const-string v1, "com.oculus.oculus360photos.action.CAST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1183844
    const/high16 v1, 0x10000

    invoke-virtual {p0, v0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 1183845
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 1183838
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->g:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_0

    .line 1183839
    invoke-virtual {p0}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->a(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/03R;->YES:LX/03R;

    :goto_0
    iput-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->g:LX/03R;

    .line 1183840
    :cond_0
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->g:LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0

    .line 1183841
    :cond_1
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 1183829
    invoke-direct {p0}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1183830
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->c:LX/1xG;

    iget-object v1, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->h:LX/7Dj;

    .line 1183831
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "spherical_photo_show_view_in_vr"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1183832
    invoke-static {v0, v3, v1, v2}, LX/1xG;->a(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V

    .line 1183833
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->e:Lcom/facebook/resources/ui/FbButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1183834
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->e:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->j:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/resources/ui/FbButton;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1183835
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->e:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/7Dr;

    invoke-direct {v1, p0}, LX/7Dr;-><init>(Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1183836
    :goto_0
    return-void

    .line 1183837
    :cond_0
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->e:Lcom/facebook/resources/ui/FbButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public static d(Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;)V
    .locals 5

    .prologue
    .line 1183823
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->c:LX/1xG;

    iget-object v1, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->h:LX/7Dj;

    .line 1183824
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "spherical_photo_tap_view_in_vr"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1183825
    invoke-static {v0, v3, v1, v2}, LX/1xG;->a(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V

    .line 1183826
    invoke-direct {p0}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->e()Landroid/content/Intent;

    move-result-object v0

    .line 1183827
    iget-object v1, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1183828
    return-void
.end method

.method private e()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 1183805
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.oculus.oculus360photos.action.CAST"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1183806
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 1183807
    :try_start_0
    const-string v0, "photo_uri"

    iget-object v3, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->f:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    .line 1183808
    iget-object v4, v3, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1183809
    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1183810
    const-string v0, "author"

    iget-object v3, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->f:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    .line 1183811
    iget-object v4, v3, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->b:Ljava/lang/String;

    move-object v3, v4

    .line 1183812
    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1183813
    const-string v0, "title"

    iget-object v3, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->f:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    .line 1183814
    iget-object v4, v3, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->c:Ljava/lang/String;

    move-object v3, v4

    .line 1183815
    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1183816
    const-string v0, "photo_fbid"

    iget-object v3, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->f:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    .line 1183817
    iget-object v4, v3, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1183818
    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1183819
    :goto_0
    const-string v0, "intent_cmd"

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1183820
    return-object v1

    .line 1183821
    :catch_0
    move-exception v0

    .line 1183822
    sget-object v3, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->d:Ljava/lang/String;

    const-string v4, "Exception when applying json encoding"

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1183796
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->e:Lcom/facebook/resources/ui/FbButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1183797
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->e:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1183798
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->e:Lcom/facebook/resources/ui/FbButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1183799
    return-void
.end method

.method public final a(Lcom/facebook/spherical/photo/model/PhotoVRCastParams;Ljava/lang/String;LX/7Dj;)V
    .locals 0

    .prologue
    .line 1183800
    iput-object p1, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->f:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    .line 1183801
    iput-object p2, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->i:Ljava/lang/String;

    .line 1183802
    iput-object p3, p0, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->h:LX/7Dj;

    .line 1183803
    invoke-direct {p0}, Lcom/facebook/spherical/photo/ui/PhotoVRCastPlugin;->c()V

    .line 1183804
    return-void
.end method
