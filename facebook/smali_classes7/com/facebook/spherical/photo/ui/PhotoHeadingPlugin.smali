.class public Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/3II;
.implements LX/7Co;


# instance fields
.field public a:LX/1xG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

.field public c:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

.field public d:LX/3IP;

.field private e:LX/7Dq;

.field public f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

.field private g:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

.field public h:LX/7Dj;

.field public i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1183773
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1183774
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1183775
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1183776
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1183777
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1183778
    const-class v0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    invoke-static {v0, p0}, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1183779
    const v0, 0x7f030f37

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1183780
    const v0, 0x7f0d24df

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iput-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->g:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    .line 1183781
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->g:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->b()V

    .line 1183782
    new-instance v0, LX/3IP;

    invoke-direct {v0}, LX/3IP;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->d:LX/3IP;

    .line 1183783
    new-instance v0, LX/7Dq;

    invoke-direct {v0, p0}, LX/7Dq;-><init>(Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;)V

    iput-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->e:LX/7Dq;

    .line 1183784
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;

    invoke-static {v0}, LX/1xG;->b(LX/0QB;)LX/1xG;

    move-result-object v0

    check-cast v0, LX/1xG;

    iput-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->a:LX/1xG;

    return-void
.end method


# virtual methods
.method public final a(FFF)V
    .locals 1

    .prologue
    .line 1183785
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->g:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-virtual {v0, p2, p3}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->a(FF)V

    .line 1183786
    return-void
.end method

.method public final a(Ljava/lang/String;LX/7Dj;)V
    .locals 0

    .prologue
    .line 1183787
    iput-object p1, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->i:Ljava/lang/String;

    .line 1183788
    iput-object p2, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->h:LX/7Dj;

    .line 1183789
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1183772
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1183753
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->g:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->c()V

    .line 1183754
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->g:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    iget-object v1, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->c:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->e:LX/7Dq;

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->a(LX/7DE;ZZLX/3Ia;)V

    .line 1183755
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->g:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-virtual {v0, v4}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->setClickable(Z)V

    .line 1183756
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1183770
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->g:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->b()V

    .line 1183771
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1183767
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->g:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    if-eqz v0, :cond_0

    .line 1183768
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->g:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->a()V

    .line 1183769
    :cond_0
    return-void
.end method

.method public get360TextureView()LX/2qW;
    .locals 1

    .prologue
    .line 1183766
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1183763
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1183764
    iget-object v0, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->g:Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/ui/SphericalHeadingIndicatorPlugin;->c()V

    .line 1183765
    return-void
.end method

.method public setFeedbackListener(Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;)V
    .locals 0

    .prologue
    .line 1183761
    iput-object p1, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->b:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    .line 1183762
    return-void
.end method

.method public setSphericalPhotoParams(Lcom/facebook/spherical/photo/model/SphericalPhotoParams;)V
    .locals 0

    .prologue
    .line 1183759
    iput-object p1, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->c:Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 1183760
    return-void
.end method

.method public setTextureView(Lcom/facebook/spherical/photo/SphericalPhotoTextureView;)V
    .locals 0

    .prologue
    .line 1183757
    iput-object p1, p0, Lcom/facebook/spherical/photo/ui/PhotoHeadingPlugin;->f:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    .line 1183758
    return-void
.end method
