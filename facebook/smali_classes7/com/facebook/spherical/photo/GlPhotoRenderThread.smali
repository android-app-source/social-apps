.class public Lcom/facebook/spherical/photo/GlPhotoRenderThread;
.super Lcom/facebook/spherical/GlMediaRenderThread;
.source ""

# interfaces
.implements LX/7DK;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public B:LX/7DM;

.field private C:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

.field public D:Z

.field public E:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/graphics/SurfaceTexture;Ljava/lang/Runnable;Ljava/lang/Runnable;IILX/7D0;LX/0Ot;LX/0SG;LX/0wW;LX/7DU;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/graphics/SurfaceTexture;",
            "Ljava/lang/Runnable;",
            "Ljava/lang/Runnable;",
            "II",
            "LX/7D0;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0SG;",
            "LX/0wW;",
            "LX/7DU;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1182713
    invoke-direct/range {p0 .. p11}, Lcom/facebook/spherical/GlMediaRenderThread;-><init>(Landroid/content/Context;Landroid/graphics/SurfaceTexture;Ljava/lang/Runnable;Ljava/lang/Runnable;IILX/7D0;LX/0Ot;LX/0SG;LX/0wW;LX/7Cz;)V

    .line 1182714
    iput-boolean v1, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->D:Z

    .line 1182715
    iput-boolean v1, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->E:Z

    .line 1182716
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->p:F

    .line 1182717
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->m:F

    .line 1182718
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->n:F

    .line 1182719
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->e:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1182720
    iget-object v0, p11, LX/7DU;->k:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    if-eqz v0, :cond_0

    .line 1182721
    iget-object v0, p11, LX/7DU;->k:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    invoke-virtual {p0, v0}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->a(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)V

    .line 1182722
    :cond_0
    iget-object v0, p11, LX/7DU;->l:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    if-eqz v0, :cond_1

    .line 1182723
    iget-object v0, p11, LX/7DU;->l:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    invoke-virtual {p0, v0}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->a(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;)V

    .line 1182724
    :cond_1
    iget-object v0, p11, LX/7DU;->m:LX/7DM;

    if-eqz v0, :cond_2

    .line 1182725
    iget-object v0, p11, LX/7DU;->m:LX/7DM;

    .line 1182726
    iput-object v0, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->B:LX/7DM;

    .line 1182727
    :cond_2
    iget-object v0, p11, LX/7DU;->n:LX/7Da;

    if-eqz v0, :cond_3

    .line 1182728
    iget-object v0, p11, LX/7DU;->n:LX/7Da;

    invoke-virtual {p0, v0}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->a(LX/7Da;)V

    .line 1182729
    :cond_3
    return-void
.end method

.method private static a(FFF)F
    .locals 1

    .prologue
    .line 1182765
    const/high16 v0, 0x42af0000    # 87.5f

    cmpl-float v0, p0, v0

    if-ltz v0, :cond_0

    :goto_0
    return p2

    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, p1, v0

    sub-float p2, p0, v0

    goto :goto_0
.end method

.method private a(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 1182747
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    check-cast v0, LX/7Cf;

    .line 1182748
    iget-object v1, v0, LX/7Cf;->g:LX/7DG;

    move-object v0, v1

    .line 1182749
    iget-boolean v1, v0, LX/7DG;->e:Z

    move v0, v1

    .line 1182750
    if-nez v0, :cond_1

    .line 1182751
    :cond_0
    :goto_0
    return-void

    .line 1182752
    :cond_1
    int-to-float v0, p2

    int-to-float v1, p3

    div-float/2addr v0, v1

    .line 1182753
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_2

    .line 1182754
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    check-cast v0, LX/7Cf;

    .line 1182755
    iget v1, p1, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->b:F

    move v1, v1

    .line 1182756
    iget v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    div-float/2addr v2, v4

    sub-float/2addr v1, v2

    .line 1182757
    iget v2, p1, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->a:F

    move v2, v2

    .line 1182758
    neg-float v2, v2

    iget v3, p0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, LX/7Cf;->a_(FF)V

    goto :goto_0

    .line 1182759
    :cond_2
    iget v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    mul-float/2addr v1, v0

    .line 1182760
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    check-cast v0, LX/7Cf;

    .line 1182761
    iget v2, p1, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->b:F

    move v2, v2

    .line 1182762
    div-float v3, v1, v4

    sub-float/2addr v2, v3

    .line 1182763
    iget v3, p1, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->a:F

    move v3, v3

    .line 1182764
    neg-float v3, v3

    div-float/2addr v1, v4

    add-float/2addr v1, v3

    invoke-virtual {v0, v2, v1}, LX/7Cf;->a_(FF)V

    goto :goto_0
.end method

.method private b(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;II)V
    .locals 4

    .prologue
    const/high16 v3, 0x42b40000    # 90.0f

    .line 1182731
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    check-cast v0, LX/7Cf;

    .line 1182732
    iget-object v1, v0, LX/7Cf;->g:LX/7DG;

    move-object v0, v1

    .line 1182733
    iget-boolean v1, v0, LX/7DG;->f:Z

    move v0, v1

    .line 1182734
    if-nez v0, :cond_1

    .line 1182735
    :cond_0
    :goto_0
    return-void

    .line 1182736
    :cond_1
    int-to-float v0, p2

    int-to-float v1, p3

    div-float/2addr v0, v1

    .line 1182737
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_2

    iget v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    div-float v0, v1, v0

    .line 1182738
    :goto_1
    iget v1, p1, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->d:F

    move v1, v1

    .line 1182739
    invoke-static {v1, v0, v3}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->a(FFF)F

    move-result v1

    .line 1182740
    iget v2, p1, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->c:F

    move v2, v2

    .line 1182741
    invoke-static {v2, v0, v3}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->a(FFF)F

    move-result v0

    neg-float v2, v0

    .line 1182742
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    check-cast v0, LX/7Cf;

    .line 1182743
    iput v1, v0, LX/7Cf;->f:F

    .line 1182744
    iput v2, v0, LX/7Cf;->e:F

    .line 1182745
    goto :goto_0

    .line 1182746
    :cond_2
    iget v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    goto :goto_1
.end method

.method private static d(F)Z
    .locals 1

    .prologue
    .line 1182730
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(Lcom/facebook/spherical/photo/GlPhotoRenderThread;)V
    .locals 1

    .prologue
    .line 1182706
    iget-object v0, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->B:LX/7DM;

    if-eqz v0, :cond_1

    .line 1182707
    iget-boolean v0, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->E:Z

    move v0, v0

    .line 1182708
    if-eqz v0, :cond_1

    .line 1182709
    iget-boolean v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->q:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->D:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1182710
    if-eqz v0, :cond_1

    .line 1182711
    iget-object v0, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->B:LX/7DM;

    invoke-interface {v0}, LX/7DM;->c()V

    .line 1182712
    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(FF)V
    .locals 1

    .prologue
    .line 1182698
    iget v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->v:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->w:F

    cmpl-float v0, p2, v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 1182699
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/facebook/spherical/GlMediaRenderThread;->a(FF)V

    .line 1182700
    if-eqz v0, :cond_1

    .line 1182701
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v0, v0, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1182702
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    invoke-virtual {v0, p1, p2}, LX/7Ce;->a(FF)V

    .line 1182703
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v0, v0, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1182704
    :cond_1
    return-void

    .line 1182705
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(IIZ)V
    .locals 6

    .prologue
    .line 1182766
    if-lez p1, :cond_0

    .line 1182767
    iput p1, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->x:I

    .line 1182768
    :cond_0
    if-lez p2, :cond_1

    .line 1182769
    iput p2, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->y:I

    .line 1182770
    :cond_1
    iget v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    .line 1182771
    iget v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->x:I

    int-to-float v0, v0

    iget v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->y:I

    int-to-float v1, v1

    div-float v3, v0, v1

    .line 1182772
    if-nez p3, :cond_2

    .line 1182773
    iget-object v0, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->C:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    iget v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->x:I

    iget v4, p0, Lcom/facebook/spherical/GlMediaRenderThread;->y:I

    invoke-direct {p0, v0, v1, v4}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->a(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;II)V

    .line 1182774
    iget-object v0, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->C:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    iget v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->x:I

    iget v4, p0, Lcom/facebook/spherical/GlMediaRenderThread;->y:I

    invoke-direct {p0, v0, v1, v4}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->b(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;II)V

    .line 1182775
    invoke-static {v3}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->d(F)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1182776
    div-float/2addr v2, v3

    .line 1182777
    :cond_2
    if-eqz p3, :cond_3

    invoke-static {v3}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->d(F)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1182778
    div-float/2addr v2, v3

    .line 1182779
    :cond_3
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->d:[F

    const/4 v1, 0x0

    const v4, 0x3dcccccd    # 0.1f

    const/high16 v5, 0x42c80000    # 100.0f

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->perspectiveM([FIFFFF)V

    .line 1182780
    return-void
.end method

.method public final a(LX/1FJ;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 1182688
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->h:Landroid/os/Handler;

    move-object v0, v0

    .line 1182689
    if-eqz v0, :cond_0

    .line 1182690
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1182691
    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1182692
    invoke-virtual {p1}, LX/1FJ;->b()LX/1FJ;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1182693
    iput p3, v0, Landroid/os/Message;->arg1:I

    .line 1182694
    iput p2, v0, Landroid/os/Message;->arg2:I

    .line 1182695
    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->h:Landroid/os/Handler;

    move-object v1, v1

    .line 1182696
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1182697
    :cond_0
    return-void
.end method

.method public final a(LX/7Da;)V
    .locals 2

    .prologue
    .line 1182681
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    instance-of v0, v0, LX/7Df;

    if-eqz v0, :cond_0

    .line 1182682
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    check-cast v0, LX/7Df;

    .line 1182683
    iput-object p1, v0, LX/7Df;->n:LX/7Da;

    .line 1182684
    iget-object v1, v0, LX/7Df;->q:LX/7DD;

    iget-object p0, v0, LX/7Df;->n:LX/7Da;

    .line 1182685
    iget v0, p0, LX/7Da;->g:I

    move p0, v0

    .line 1182686
    iput p0, v1, LX/7DD;->e:I

    .line 1182687
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)V
    .locals 1

    .prologue
    .line 1182604
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    invoke-interface {v0, p1}, LX/7D0;->a(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)V

    .line 1182605
    return-void
.end method

.method public final a(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;)V
    .locals 3

    .prologue
    .line 1182675
    iput-object p1, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->C:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    .line 1182676
    iget-object v0, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->C:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    iget v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->x:I

    iget v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->y:I

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->b(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;II)V

    .line 1182677
    iget-object v0, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->C:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    iget v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->x:I

    iget v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->y:I

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->a(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;II)V

    .line 1182678
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    check-cast v0, LX/7Cu;

    invoke-virtual {v0}, LX/7Cu;->e()V

    .line 1182679
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    invoke-interface {v0, p1}, LX/7D0;->a(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;)V

    .line 1182680
    return-void
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 11

    .prologue
    const/4 v2, 0x1

    .line 1182612
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 1182613
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/1FJ;

    .line 1182614
    invoke-static {v0}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1182615
    :try_start_0
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ln;

    .line 1182616
    instance-of v3, v1, LX/1ll;

    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1182617
    check-cast v1, LX/1ll;

    invoke-virtual {v1}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1182618
    iget-object v3, p0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    invoke-interface {v3, v1}, LX/7D0;->a(Landroid/graphics/Bitmap;)V

    .line 1182619
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-nez v1, :cond_0

    .line 1182620
    const/4 v1, 0x1

    .line 1182621
    iput-boolean v1, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->E:Z

    .line 1182622
    invoke-static {p0}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->s(Lcom/facebook/spherical/photo/GlPhotoRenderThread;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1182623
    :cond_0
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    move v0, v2

    .line 1182624
    :goto_0
    return v0

    .line 1182625
    :catch_0
    :try_start_1
    iget-object v1, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->B:LX/7DM;

    invoke-interface {v1}, LX/7DM;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1182626
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 1182627
    :cond_1
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1182628
    :catchall_0
    move-exception v1

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    throw v1

    .line 1182629
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 1182630
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/7Dk;

    .line 1182631
    :try_start_2
    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    instance-of v1, v1, LX/7Df;

    if-eqz v1, :cond_5

    .line 1182632
    iget-object v1, v0, LX/7Dk;->b:LX/1FJ;

    move-object v1, v1

    .line 1182633
    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ln;

    .line 1182634
    instance-of v2, v1, LX/1ll;

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1182635
    check-cast v1, LX/1ll;

    invoke-virtual {v1}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1182636
    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    check-cast v1, LX/7Df;

    .line 1182637
    iget-object v3, v0, LX/7Dk;->a:LX/7D7;

    move-object v3, v3

    .line 1182638
    iget-object v4, v1, LX/7Df;->c:LX/7Dc;

    invoke-static {v4, v3}, LX/7Dc;->a(LX/7Dc;LX/7D7;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1182639
    const/16 v6, 0xde1

    const/4 v5, 0x0

    .line 1182640
    const p1, 0x812f

    const/4 v7, 0x1

    const v10, 0x46180400    # 9729.0f

    const/4 v9, 0x0

    const/16 v8, 0xde1

    .line 1182641
    new-array v4, v7, [I

    .line 1182642
    invoke-static {v7, v4, v9}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 1182643
    aget v4, v4, v9

    .line 1182644
    invoke-static {v8, v4}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1182645
    const/16 v7, 0x2801

    invoke-static {v8, v7, v10}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1182646
    const/16 v7, 0x2800

    invoke-static {v8, v7, v10}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1182647
    const/16 v7, 0x2802

    invoke-static {v8, v7, p1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 1182648
    const/16 v7, 0x2803

    invoke-static {v8, v7, p1}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 1182649
    const/16 v7, 0xb44

    invoke-static {v7}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 1182650
    const/16 v7, 0x404

    invoke-static {v7}, Landroid/opengl/GLES20;->glCullFace(I)V

    .line 1182651
    invoke-static {v8, v9}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1182652
    move v4, v4

    .line 1182653
    invoke-static {v6, v4}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1182654
    invoke-static {v6, v5, v2, v5}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 1182655
    move v4, v4

    .line 1182656
    const/16 v5, 0xde1

    invoke-static {v5, v4}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1182657
    iget-object v5, v1, LX/7Df;->c:LX/7Dc;

    .line 1182658
    iget v6, v3, LX/7D7;->a:I

    move v6, v6

    .line 1182659
    if-nez v6, :cond_6

    .line 1182660
    iget-object v6, v5, LX/7Dc;->a:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1182661
    new-instance v6, LX/7Dd;

    invoke-direct {v6, v3, v4}, LX/7Dd;-><init>(LX/7D7;I)V

    .line 1182662
    iget-object v7, v5, LX/7Dc;->a:Ljava/util/Map;

    invoke-interface {v7, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1182663
    :cond_3
    :goto_2
    iget-object v4, v1, LX/7Df;->p:LX/7DR;

    if-eqz v4, :cond_4

    iget-boolean v4, v1, LX/7Df;->s:Z

    if-nez v4, :cond_4

    iget-object v4, v1, LX/7Df;->c:LX/7Dc;

    invoke-static {v4}, LX/7Dc;->a(LX/7Dc;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1182664
    const/4 v4, 0x1

    iput-boolean v4, v1, LX/7Df;->s:Z

    .line 1182665
    iget-object v4, v1, LX/7Df;->p:LX/7DR;

    invoke-virtual {v4}, LX/7DR;->a()V

    .line 1182666
    :cond_4
    iget v4, v1, LX/7Df;->z:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, LX/7Df;->z:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1182667
    :cond_5
    iget-object v1, v0, LX/7Dk;->b:LX/1FJ;

    move-object v0, v1

    .line 1182668
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto/16 :goto_1

    .line 1182669
    :catch_1
    :try_start_3
    iget-object v1, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->B:LX/7DM;

    invoke-interface {v1}, LX/7DM;->d()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1182670
    iget-object v1, v0, LX/7Dk;->b:LX/1FJ;

    move-object v0, v1

    .line 1182671
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto/16 :goto_1

    :catchall_1
    move-exception v1

    .line 1182672
    iget-object v2, v0, LX/7Dk;->b:LX/1FJ;

    move-object v0, v2

    .line 1182673
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    throw v1

    .line 1182674
    :cond_6
    invoke-static {v5, v3, v4}, LX/7Dc;->c(LX/7Dc;LX/7D7;I)V

    goto :goto_2
.end method

.method public final b([I)V
    .locals 1

    .prologue
    .line 1182609
    iget-boolean v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->s:Z

    if-eqz v0, :cond_0

    .line 1182610
    :goto_0
    return-void

    .line 1182611
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/spherical/GlMediaRenderThread;->b([I)V

    goto :goto_0
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 1182606
    iput-boolean p1, p0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->D:Z

    .line 1182607
    invoke-static {p0}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->s(Lcom/facebook/spherical/photo/GlPhotoRenderThread;)V

    .line 1182608
    return-void
.end method
