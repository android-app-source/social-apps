.class public Lcom/facebook/spherical/photo/SphericalPhotoTextureView;
.super LX/2qW;
.source ""

# interfaces
.implements LX/1a7;


# instance fields
.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

.field public g:LX/7DU;

.field public h:LX/7D0;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1183006
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1183007
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1182987
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1182988
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1182989
    invoke-direct {p0, p1, p2, p3}, LX/2qW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1182990
    const-class v0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-static {v0, p0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1182991
    new-instance v0, LX/7DU;

    invoke-direct {v0}, LX/7DU;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->g:LX/7DU;

    .line 1182992
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->g:LX/7DU;

    sget-object v1, LX/19o;->CUBEMAP:LX/19o;

    iput-object v1, v0, LX/7DU;->b:LX/19o;

    .line 1182993
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->g:LX/7DU;

    sget-object v1, LX/7Cl;->PHOTO:LX/7Cl;

    iput-object v1, v0, LX/7DU;->c:LX/7Cl;

    .line 1182994
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/2qW;->setZoomInteractionEnabled(Z)V

    .line 1182995
    new-instance v0, LX/7DN;

    invoke-virtual {p0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->d:LX/0SG;

    invoke-direct {v0, v1, v2}, LX/7DN;-><init>(Landroid/content/res/Resources;LX/0SG;)V

    iput-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->h:LX/7D0;

    .line 1182996
    return-void
.end method

.method private a(LX/19o;)LX/7D0;
    .locals 3

    .prologue
    .line 1182997
    if-nez p1, :cond_0

    .line 1182998
    new-instance v0, LX/7DN;

    invoke-virtual {p0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->d:LX/0SG;

    invoke-direct {v0, v1, v2}, LX/7DN;-><init>(Landroid/content/res/Resources;LX/0SG;)V

    .line 1182999
    :goto_0
    return-object v0

    .line 1183000
    :cond_0
    sget-object v0, LX/7DS;->a:[I

    invoke-virtual {p1}, LX/19o;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1183001
    new-instance v0, LX/7DN;

    invoke-virtual {p0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->d:LX/0SG;

    invoke-direct {v0, v1, v2}, LX/7DN;-><init>(Landroid/content/res/Resources;LX/0SG;)V

    goto :goto_0

    .line 1183002
    :pswitch_0
    new-instance v0, LX/7DP;

    invoke-virtual {p0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7DP;-><init>(Landroid/content/res/Resources;)V

    .line 1183003
    invoke-interface {v0, p1}, LX/7D0;->a(LX/19o;)V

    goto :goto_0

    .line 1183004
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->h()LX/7Df;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static synthetic a(Lcom/facebook/spherical/photo/SphericalPhotoTextureView;LX/19o;)LX/7D0;
    .locals 1

    .prologue
    .line 1183005
    invoke-direct {p0, p1}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->a(LX/19o;)LX/7D0;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/spherical/photo/SphericalPhotoTextureView;LX/0Ot;LX/0SG;LX/0wW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/spherical/photo/SphericalPhotoTextureView;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0SG;",
            "LX/0wW;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1183008
    iput-object p1, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->c:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->d:LX/0SG;

    iput-object p3, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->e:LX/0wW;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    const/16 v0, 0x259

    invoke-static {v1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {v1}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v1

    check-cast v1, LX/0wW;

    invoke-static {p0, v2, v0, v1}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->a(Lcom/facebook/spherical/photo/SphericalPhotoTextureView;LX/0Ot;LX/0SG;LX/0wW;)V

    return-void
.end method

.method private h()LX/7Df;
    .locals 4

    .prologue
    .line 1182982
    new-instance v0, LX/7Df;

    invoke-virtual {p0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->d:LX/0SG;

    invoke-virtual {p0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LX/7Df;-><init>(Landroid/content/res/Resources;LX/0SG;I)V

    .line 1182983
    new-instance v1, LX/7DR;

    invoke-direct {v1, p0}, LX/7DR;-><init>(Lcom/facebook/spherical/photo/SphericalPhotoTextureView;)V

    .line 1182984
    iput-object v1, v0, LX/7Df;->p:LX/7DR;

    .line 1182985
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/TextureView$SurfaceTextureListener;)LX/7Cy;
    .locals 1

    .prologue
    .line 1182986
    new-instance v0, LX/7DT;

    invoke-direct {v0, p0, p1}, LX/7DT;-><init>(Lcom/facebook/spherical/photo/SphericalPhotoTextureView;Landroid/view/TextureView$SurfaceTextureListener;)V

    return-object v0
.end method

.method public final a(LX/1FJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1182980
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->a(LX/1FJ;I)V

    .line 1182981
    return-void
.end method

.method public final a(LX/1FJ;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1182976
    invoke-virtual {p0}, LX/2qW;->a()V

    .line 1182977
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    if-eqz v0, :cond_0

    .line 1182978
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->a(LX/1FJ;II)V

    .line 1182979
    :cond_0
    return-void
.end method

.method public final b(LX/1FJ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1182972
    invoke-virtual {p0}, LX/2qW;->a()V

    .line 1182973
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    if-eqz v0, :cond_0

    .line 1182974
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    invoke-virtual {v0, p1, v1, v1}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->a(LX/1FJ;II)V

    .line 1182975
    :cond_0
    return-void
.end method

.method public final cr_()Z
    .locals 1

    .prologue
    .line 1182971
    const/4 v0, 0x1

    return v0
.end method

.method public getRenderThreadParams()LX/7Cz;
    .locals 1

    .prologue
    .line 1182970
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->g:LX/7DU;

    return-object v0
.end method

.method public setHasSphericalPhoto(LX/7DM;)V
    .locals 1

    .prologue
    .line 1182948
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->g:LX/7DU;

    iput-object p1, v0, LX/7DU;->m:LX/7DM;

    .line 1182949
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    if-eqz v0, :cond_0

    .line 1182950
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    .line 1182951
    iput-object p1, v0, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->B:LX/7DM;

    .line 1182952
    :cond_0
    return-void
.end method

.method public setPanoBounds(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;)V
    .locals 1

    .prologue
    .line 1182966
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->g:LX/7DU;

    iput-object p1, v0, LX/7DU;->l:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    .line 1182967
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    if-eqz v0, :cond_0

    .line 1182968
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    invoke-virtual {v0, p1}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->a(Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;)V

    .line 1182969
    :cond_0
    return-void
.end method

.method public setSphericalPhotoMetadata(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)V
    .locals 1

    .prologue
    .line 1182962
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    if-nez v0, :cond_0

    .line 1182963
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->g:LX/7DU;

    iput-object p1, v0, LX/7DU;->k:Lcom/facebook/bitmaps/SphericalPhotoMetadata;

    .line 1182964
    :goto_0
    return-void

    .line 1182965
    :cond_0
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    invoke-virtual {v0, p1}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->a(Lcom/facebook/bitmaps/SphericalPhotoMetadata;)V

    goto :goto_0
.end method

.method public setSphericalPhotoParams(Lcom/facebook/spherical/photo/model/SphericalPhotoParams;)V
    .locals 2

    .prologue
    .line 1182959
    invoke-virtual {p1}, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->b()F

    move-result v0

    invoke-virtual {p0, v0}, LX/2qW;->setPreferredVerticalFOV(F)V

    .line 1182960
    invoke-virtual {p1}, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->e()F

    move-result v0

    neg-float v0, v0

    invoke-virtual {p1}, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->c()F

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/2qW;->a(FF)V

    .line 1182961
    return-void
.end method

.method public setTileProvider(LX/7Da;)V
    .locals 1

    .prologue
    .line 1182953
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->g:LX/7DU;

    iput-object p1, v0, LX/7DU;->n:LX/7Da;

    .line 1182954
    new-instance v0, LX/7DQ;

    invoke-direct {v0, p0}, LX/7DQ;-><init>(Lcom/facebook/spherical/photo/SphericalPhotoTextureView;)V

    .line 1182955
    iput-object v0, p1, LX/7Da;->h:LX/7DQ;

    .line 1182956
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    if-eqz v0, :cond_0

    .line 1182957
    iget-object v0, p0, Lcom/facebook/spherical/photo/SphericalPhotoTextureView;->f:Lcom/facebook/spherical/photo/GlPhotoRenderThread;

    invoke-virtual {v0, p1}, Lcom/facebook/spherical/photo/GlPhotoRenderThread;->a(LX/7Da;)V

    .line 1182958
    :cond_0
    return-void
.end method
