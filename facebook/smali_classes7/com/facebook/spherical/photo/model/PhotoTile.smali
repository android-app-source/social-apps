.class public Lcom/facebook/spherical/photo/model/PhotoTile;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/spherical/photo/model/PhotoTile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1183600
    new-instance v0, LX/7Dl;

    invoke-direct {v0}, LX/7Dl;-><init>()V

    sput-object v0, Lcom/facebook/spherical/photo/model/PhotoTile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7Dm;)V
    .locals 1

    .prologue
    .line 1183593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1183594
    iget v0, p1, LX/7Dm;->a:I

    iput v0, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->a:I

    .line 1183595
    iget v0, p1, LX/7Dm;->b:I

    iput v0, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->b:I

    .line 1183596
    iget v0, p1, LX/7Dm;->c:I

    iput v0, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->c:I

    .line 1183597
    iget v0, p1, LX/7Dm;->d:I

    iput v0, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->d:I

    .line 1183598
    iget-object v0, p1, LX/7Dm;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->e:Ljava/lang/String;

    .line 1183599
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1183586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1183587
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->a:I

    .line 1183588
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->b:I

    .line 1183589
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->c:I

    .line 1183590
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->d:I

    .line 1183591
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->e:Ljava/lang/String;

    .line 1183592
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1183601
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1183580
    if-ne p1, p0, :cond_1

    .line 1183581
    :cond_0
    :goto_0
    return v0

    .line 1183582
    :cond_1
    instance-of v2, p1, Lcom/facebook/spherical/photo/model/PhotoTile;

    if-nez v2, :cond_2

    move v0, v1

    .line 1183583
    goto :goto_0

    .line 1183584
    :cond_2
    check-cast p1, Lcom/facebook/spherical/photo/model/PhotoTile;

    .line 1183585
    iget v2, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->a:I

    iget v3, p1, Lcom/facebook/spherical/photo/model/PhotoTile;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->b:I

    iget v3, p1, Lcom/facebook/spherical/photo/model/PhotoTile;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->c:I

    iget v3, p1, Lcom/facebook/spherical/photo/model/PhotoTile;->c:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->d:I

    iget v3, p1, Lcom/facebook/spherical/photo/model/PhotoTile;->d:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/spherical/photo/model/PhotoTile;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1183579
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1183573
    iget v0, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1183574
    iget v0, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1183575
    iget v0, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1183576
    iget v0, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1183577
    iget-object v0, p0, Lcom/facebook/spherical/photo/model/PhotoTile;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1183578
    return-void
.end method
