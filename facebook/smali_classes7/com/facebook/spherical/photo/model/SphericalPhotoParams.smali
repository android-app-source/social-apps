.class public Lcom/facebook/spherical/photo/model/SphericalPhotoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/7DE;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/spherical/photo/model/SphericalPhotoParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:D

.field public final g:D

.field public final h:D

.field public final i:D

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:LX/19o;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;

.field public final m:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

.field public final o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/spherical/photo/model/PhotoTile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1183741
    new-instance v0, LX/7Do;

    invoke-direct {v0}, LX/7Do;-><init>()V

    sput-object v0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7Dp;)V
    .locals 2

    .prologue
    .line 1183724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1183725
    iget v0, p1, LX/7Dp;->a:I

    iput v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->a:I

    .line 1183726
    iget v0, p1, LX/7Dp;->b:I

    iput v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->b:I

    .line 1183727
    iget v0, p1, LX/7Dp;->c:I

    iput v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->c:I

    .line 1183728
    iget v0, p1, LX/7Dp;->d:I

    iput v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->d:I

    .line 1183729
    iget v0, p1, LX/7Dp;->e:I

    iput v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->e:I

    .line 1183730
    iget-wide v0, p1, LX/7Dp;->f:D

    iput-wide v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->f:D

    .line 1183731
    iget-wide v0, p1, LX/7Dp;->g:D

    iput-wide v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->g:D

    .line 1183732
    iget-wide v0, p1, LX/7Dp;->h:D

    iput-wide v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->h:D

    .line 1183733
    iget-wide v0, p1, LX/7Dp;->i:D

    iput-wide v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->i:D

    .line 1183734
    iget-object v0, p1, LX/7Dp;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->j:Ljava/lang/String;

    .line 1183735
    iget-object v0, p1, LX/7Dp;->k:LX/19o;

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->k:LX/19o;

    .line 1183736
    iget-object v0, p1, LX/7Dp;->l:Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->l:Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;

    .line 1183737
    iget-object v0, p1, LX/7Dp;->m:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->m:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    .line 1183738
    iget-object v0, p1, LX/7Dp;->n:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->n:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    .line 1183739
    iget-object v0, p1, LX/7Dp;->o:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->o:LX/0Px;

    .line 1183740
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 1183703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1183704
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->a:I

    .line 1183705
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->b:I

    .line 1183706
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->c:I

    .line 1183707
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->d:I

    .line 1183708
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->e:I

    .line 1183709
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->f:D

    .line 1183710
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->g:D

    .line 1183711
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->h:D

    .line 1183712
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->i:D

    .line 1183713
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->j:Ljava/lang/String;

    .line 1183714
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/19o;

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->k:LX/19o;

    .line 1183715
    const-class v0, Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->l:Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;

    .line 1183716
    const-class v0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->m:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    .line 1183717
    const-class v0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->n:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    .line 1183718
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1183719
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1183720
    sget-object v2, Lcom/facebook/spherical/photo/model/PhotoTile;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1183721
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1183722
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->o:LX/0Px;

    .line 1183723
    return-void
.end method


# virtual methods
.method public final a()LX/03z;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1183702
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()F
    .locals 2

    .prologue
    .line 1183701
    iget-wide v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->i:D

    double-to-float v0, v0

    return v0
.end method

.method public final c()F
    .locals 2

    .prologue
    .line 1183742
    iget-wide v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->f:D

    double-to-float v0, v0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1183676
    const/4 v0, 0x0

    return v0
.end method

.method public final e()F
    .locals 2

    .prologue
    .line 1183700
    iget-wide v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->g:D

    double-to-float v0, v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1183694
    if-ne p1, p0, :cond_1

    .line 1183695
    :cond_0
    :goto_0
    return v0

    .line 1183696
    :cond_1
    instance-of v2, p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 1183697
    goto :goto_0

    .line 1183698
    :cond_2
    check-cast p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;

    .line 1183699
    iget v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->a:I

    iget v3, p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->b:I

    iget v3, p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->c:I

    iget v3, p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->c:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->d:I

    iget v3, p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->d:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->e:I

    iget v3, p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->e:I

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->f:D

    iget-wide v4, p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->f:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->g:D

    iget-wide v4, p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->g:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->h:D

    iget-wide v4, p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->h:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->i:D

    iget-wide v4, p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->i:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->j:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->k:LX/19o;

    iget-object v3, p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->k:LX/19o;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->l:Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;

    iget-object v3, p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->l:Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->m:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    iget-object v3, p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->m:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->n:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    iget-object v3, p1, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->n:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1183693
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->f:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->g:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->h:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->i:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->l:Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->m:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->n:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1183677
    iget v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1183678
    iget v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1183679
    iget v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1183680
    iget v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1183681
    iget v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1183682
    iget-wide v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->f:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1183683
    iget-wide v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->g:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1183684
    iget-wide v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->h:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1183685
    iget-wide v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->i:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1183686
    iget-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1183687
    iget-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->k:LX/19o;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1183688
    iget-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->l:Lcom/facebook/spherical/photo/utils/CubemapsUtil$CubemapUris;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1183689
    iget-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->m:Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1183690
    iget-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->n:Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1183691
    iget-object v0, p0, Lcom/facebook/spherical/photo/model/SphericalPhotoParams;->o:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1183692
    return-void
.end method
