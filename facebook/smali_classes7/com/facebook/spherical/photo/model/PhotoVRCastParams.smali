.class public Lcom/facebook/spherical/photo/model/PhotoVRCastParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/spherical/photo/model/PhotoVRCastParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1183630
    new-instance v0, LX/7Dn;

    invoke-direct {v0}, LX/7Dn;-><init>()V

    sput-object v0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1183624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1183625
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->a:Ljava/lang/String;

    .line 1183626
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->b:Ljava/lang/String;

    .line 1183627
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->c:Ljava/lang/String;

    .line 1183628
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->d:Ljava/lang/String;

    .line 1183629
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1183618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1183619
    iput-object p1, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->a:Ljava/lang/String;

    .line 1183620
    iput-object p2, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->b:Ljava/lang/String;

    .line 1183621
    iput-object p3, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->c:Ljava/lang/String;

    .line 1183622
    iput-object p4, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->d:Ljava/lang/String;

    .line 1183623
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1183617
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1183611
    if-ne p1, p0, :cond_1

    .line 1183612
    :cond_0
    :goto_0
    return v0

    .line 1183613
    :cond_1
    instance-of v2, p1, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 1183614
    goto :goto_0

    .line 1183615
    :cond_2
    check-cast p1, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;

    .line 1183616
    iget-object v2, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1183605
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1183606
    iget-object v0, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1183607
    iget-object v0, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1183608
    iget-object v0, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1183609
    iget-object v0, p0, Lcom/facebook/spherical/photo/model/PhotoVRCastParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1183610
    return-void
.end method
