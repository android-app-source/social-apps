.class public final Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:F

.field public final b:F

.field public final c:F

.field public final d:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1184067
    new-instance v0, LX/7Du;

    invoke-direct {v0}, LX/7Du;-><init>()V

    sput-object v0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0

    .prologue
    .line 1184042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184043
    iput p1, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->a:F

    .line 1184044
    iput p2, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->b:F

    .line 1184045
    iput p3, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->c:F

    .line 1184046
    iput p4, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->d:F

    .line 1184047
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1184061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1184062
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->a:F

    .line 1184063
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->b:F

    .line 1184064
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->c:F

    .line 1184065
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->d:F

    .line 1184066
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1184060
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1184054
    if-ne p1, p0, :cond_1

    .line 1184055
    :cond_0
    :goto_0
    return v0

    .line 1184056
    :cond_1
    instance-of v2, p1, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    if-nez v2, :cond_2

    move v0, v1

    .line 1184057
    goto :goto_0

    .line 1184058
    :cond_2
    check-cast p1, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;

    .line 1184059
    iget v2, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->a:F

    iget v3, p1, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->a:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_3

    iget v2, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->b:F

    iget v3, p1, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->b:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_3

    iget v2, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->c:F

    iget v3, p1, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->c:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_3

    iget v2, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->d:F

    iget v3, p1, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->d:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1184053
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->a:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->b:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->c:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->d:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1184048
    iget v0, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1184049
    iget v0, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->b:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1184050
    iget v0, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->c:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1184051
    iget v0, p0, Lcom/facebook/spherical/photo/utils/PartialPanoUtil$PanoBounds;->d:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1184052
    return-void
.end method
