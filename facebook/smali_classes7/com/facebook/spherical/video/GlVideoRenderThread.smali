.class public Lcom/facebook/spherical/video/GlVideoRenderThread;
.super Lcom/facebook/spherical/GlMediaRenderThread;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final B:Ljava/lang/String;

.field private final C:LX/7E9;

.field public D:Landroid/graphics/SurfaceTexture;

.field private E:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/graphics/SurfaceTexture;Ljava/lang/Runnable;Ljava/lang/Runnable;IILX/7D0;LX/7E9;LX/0Ot;LX/0SG;LX/0wW;LX/7Cz;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/graphics/SurfaceTexture;",
            "Ljava/lang/Runnable;",
            "Ljava/lang/Runnable;",
            "II",
            "LX/7D0;",
            "Lcom/facebook/spherical/video/GlVideoRenderThread$VideoSurfaceCreatedListener;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0SG;",
            "LX/0wW;",
            "LX/7Cz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1184357
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-direct/range {v1 .. v12}, Lcom/facebook/spherical/GlMediaRenderThread;-><init>(Landroid/content/Context;Landroid/graphics/SurfaceTexture;Ljava/lang/Runnable;Ljava/lang/Runnable;IILX/7D0;LX/0Ot;LX/0SG;LX/0wW;LX/7Cz;)V

    .line 1184358
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->B:Ljava/lang/String;

    .line 1184359
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->C:LX/7E9;

    .line 1184360
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->k:Z

    .line 1184361
    const v1, 0x3f4ccccd    # 0.8f

    iput v1, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->n:F

    .line 1184362
    return-void
.end method


# virtual methods
.method public final a(IIZ)V
    .locals 6

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1184340
    if-lez p1, :cond_0

    .line 1184341
    iput p1, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->x:I

    .line 1184342
    :cond_0
    if-lez p2, :cond_1

    .line 1184343
    iput p2, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->y:I

    .line 1184344
    :cond_1
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 1184345
    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->a:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1184346
    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    div-int v0, v1, v0

    int-to-float v1, v0

    .line 1184347
    iget v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    .line 1184348
    if-nez p3, :cond_2

    .line 1184349
    int-to-float v2, p1

    int-to-float v3, p2

    div-float/2addr v2, v3

    .line 1184350
    cmpl-float v3, v2, v4

    if-lez v3, :cond_2

    cmpl-float v3, v1, v4

    if-lez v3, :cond_2

    .line 1184351
    div-float/2addr v0, v2

    .line 1184352
    :cond_2
    iget v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->x:I

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/spherical/GlMediaRenderThread;->y:I

    int-to-float v3, v3

    div-float v3, v2, v3

    .line 1184353
    if-eqz p3, :cond_3

    cmpl-float v2, v3, v4

    if-lez v2, :cond_3

    cmpl-float v1, v1, v4

    if-lez v1, :cond_3

    .line 1184354
    div-float/2addr v0, v3

    move v2, v0

    .line 1184355
    :goto_0
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->d:[F

    const/4 v1, 0x0

    const v4, 0x3dcccccd    # 0.1f

    const/high16 v5, 0x42c80000    # 100.0f

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->perspectiveM([FIFFFF)V

    .line 1184356
    return-void

    :cond_3
    move v2, v0

    goto :goto_0
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1184337
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 1184338
    iput-boolean v0, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->E:Z

    .line 1184339
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b([I)V
    .locals 4

    .prologue
    .line 1184322
    iget-boolean v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->s:Z

    if-eqz v0, :cond_1

    .line 1184323
    :cond_0
    :goto_0
    return-void

    .line 1184324
    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/spherical/GlMediaRenderThread;->b([I)V

    .line 1184325
    iget-boolean v0, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->E:Z

    if-eqz v0, :cond_0

    .line 1184326
    :try_start_0
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->g:LX/5PW;

    invoke-virtual {v0}, LX/5PV;->a()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1184327
    :goto_1
    iget-object v0, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->D:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 1184328
    iget-object v0, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->D:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->e:[F

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 1184329
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->E:Z

    goto :goto_0

    .line 1184330
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1184331
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iget-object v2, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->B:Ljava/lang/String;

    const-string v3, "makeCurrent failed in onVSync"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1184332
    :try_start_1
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->g:LX/5PW;

    invoke-virtual {v0}, LX/5PV;->c()V

    .line 1184333
    invoke-virtual {p0}, Lcom/facebook/spherical/GlMediaRenderThread;->l()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 1184334
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 1184335
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iget-object v2, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->B:Ljava/lang/String;

    const-string v3, "Failed to recreate OutputSurface in onVSync"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1184336
    invoke-virtual {p0}, Lcom/facebook/spherical/GlMediaRenderThread;->c()V

    goto :goto_0
.end method

.method public final c([I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1184312
    invoke-super {p0, p1}, Lcom/facebook/spherical/GlMediaRenderThread;->c([I)V

    .line 1184313
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    invoke-interface {v1}, LX/7D0;->a()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->D:Landroid/graphics/SurfaceTexture;

    .line 1184314
    iget-object v0, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->D:Landroid/graphics/SurfaceTexture;

    new-instance v1, LX/7E8;

    invoke-direct {v1, p0}, LX/7E8;-><init>(Lcom/facebook/spherical/video/GlVideoRenderThread;)V

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 1184315
    iget-object v0, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->C:LX/7E9;

    iget-object v1, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->D:Landroid/graphics/SurfaceTexture;

    aget v2, p1, v2

    const/4 v3, 0x1

    aget v3, p1, v3

    .line 1184316
    iget-object v4, v0, LX/7E9;->a:LX/7EA;

    iget-object v4, v4, LX/7EA;->n:LX/7EB;

    iget-object v4, v4, LX/2qW;->a:Landroid/os/Handler;

    new-instance p0, Lcom/facebook/spherical/video/SphericalVideoTextureView$GlVideoThreadController$1$1;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/spherical/video/SphericalVideoTextureView$GlVideoThreadController$1$1;-><init>(LX/7E9;Landroid/graphics/SurfaceTexture;II)V

    const p1, 0x69d68482

    invoke-static {v4, p0, p1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1184317
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 1184318
    invoke-super {p0}, Lcom/facebook/spherical/GlMediaRenderThread;->m()V

    .line 1184319
    iget-object v0, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->D:Landroid/graphics/SurfaceTexture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 1184320
    iget-object v0, p0, Lcom/facebook/spherical/video/GlVideoRenderThread;->D:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 1184321
    return-void
.end method
