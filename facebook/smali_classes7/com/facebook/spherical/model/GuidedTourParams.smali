.class public Lcom/facebook/spherical/model/GuidedTourParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/spherical/model/GuidedTourParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/spherical/model/KeyframeParams;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1182438
    new-instance v0, LX/7D8;

    invoke-direct {v0}, LX/7D8;-><init>()V

    sput-object v0, Lcom/facebook/spherical/model/GuidedTourParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7D9;)V
    .locals 1

    .prologue
    .line 1182435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1182436
    iget-object v0, p1, LX/7D9;->a:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/model/GuidedTourParams;->a:LX/0Px;

    .line 1182437
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 1182427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1182428
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1182429
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1182430
    sget-object v2, Lcom/facebook/spherical/model/KeyframeParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1182431
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1182432
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/model/GuidedTourParams;->a:LX/0Px;

    .line 1182433
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1182434
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1182420
    if-ne p1, p0, :cond_0

    .line 1182421
    const/4 v0, 0x1

    .line 1182422
    :goto_0
    return v0

    .line 1182423
    :cond_0
    instance-of v0, p1, Lcom/facebook/spherical/model/GuidedTourParams;

    if-nez v0, :cond_1

    .line 1182424
    const/4 v0, 0x0

    goto :goto_0

    .line 1182425
    :cond_1
    check-cast p1, Lcom/facebook/spherical/model/GuidedTourParams;

    .line 1182426
    iget-object v0, p0, Lcom/facebook/spherical/model/GuidedTourParams;->a:LX/0Px;

    iget-object v1, p1, Lcom/facebook/spherical/model/GuidedTourParams;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1182417
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/spherical/model/GuidedTourParams;->a:LX/0Px;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1182418
    iget-object v0, p0, Lcom/facebook/spherical/model/GuidedTourParams;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1182419
    return-void
.end method
