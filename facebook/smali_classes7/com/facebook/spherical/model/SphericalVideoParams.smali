.class public Lcom/facebook/spherical/model/SphericalVideoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/7DE;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/spherical/model/SphericalVideoParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/19o;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/03z;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:D

.field public final h:D

.field public final i:Lcom/facebook/spherical/model/GuidedTourParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Z

.field public final k:D

.field public final l:D


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1182568
    new-instance v0, LX/7DH;

    invoke-direct {v0}, LX/7DH;-><init>()V

    sput-object v0, Lcom/facebook/spherical/model/SphericalVideoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7DI;)V
    .locals 2

    .prologue
    .line 1182569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1182570
    iget-object v0, p1, LX/7DI;->a:LX/19o;

    iput-object v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->a:LX/19o;

    .line 1182571
    iget-object v0, p1, LX/7DI;->b:LX/03z;

    iput-object v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->b:LX/03z;

    .line 1182572
    iget v0, p1, LX/7DI;->c:I

    iput v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->c:I

    .line 1182573
    iget v0, p1, LX/7DI;->d:I

    iput v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->d:I

    .line 1182574
    iget v0, p1, LX/7DI;->e:I

    iput v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->e:I

    .line 1182575
    iget v0, p1, LX/7DI;->f:I

    iput v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->f:I

    .line 1182576
    iget-wide v0, p1, LX/7DI;->g:D

    iput-wide v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->g:D

    .line 1182577
    iget-wide v0, p1, LX/7DI;->h:D

    iput-wide v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->h:D

    .line 1182578
    iget-object v0, p1, LX/7DI;->i:Lcom/facebook/spherical/model/GuidedTourParams;

    iput-object v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->i:Lcom/facebook/spherical/model/GuidedTourParams;

    .line 1182579
    iget-boolean v0, p1, LX/7DI;->j:Z

    iput-boolean v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->j:Z

    .line 1182580
    iget-wide v0, p1, LX/7DI;->k:D

    iput-wide v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->k:D

    .line 1182581
    iget-wide v0, p1, LX/7DI;->l:D

    iput-wide v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->l:D

    .line 1182582
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1182584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1182585
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/19o;

    iput-object v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->a:LX/19o;

    .line 1182586
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/03z;

    iput-object v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->b:LX/03z;

    .line 1182587
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->c:I

    .line 1182588
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->d:I

    .line 1182589
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->e:I

    .line 1182590
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->f:I

    .line 1182591
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->g:D

    .line 1182592
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->h:D

    .line 1182593
    const-class v0, Lcom/facebook/spherical/model/GuidedTourParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/spherical/model/GuidedTourParams;

    iput-object v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->i:Lcom/facebook/spherical/model/GuidedTourParams;

    .line 1182594
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->j:Z

    .line 1182595
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->k:D

    .line 1182596
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->l:D

    .line 1182597
    return-void

    .line 1182598
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/03z;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1182583
    iget-object v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->b:LX/03z;

    return-object v0
.end method

.method public final b()F
    .locals 1

    .prologue
    .line 1182567
    iget v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->f:I

    int-to-float v0, v0

    return v0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 1182566
    iget v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->c:I

    int-to-float v0, v0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1182543
    const/4 v0, 0x0

    return v0
.end method

.method public final e()F
    .locals 1

    .prologue
    .line 1182544
    iget v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->d:I

    int-to-float v0, v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1182545
    if-ne p1, p0, :cond_1

    .line 1182546
    :cond_0
    :goto_0
    return v0

    .line 1182547
    :cond_1
    instance-of v2, p1, Lcom/facebook/spherical/model/SphericalVideoParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 1182548
    goto :goto_0

    .line 1182549
    :cond_2
    check-cast p1, Lcom/facebook/spherical/model/SphericalVideoParams;

    .line 1182550
    iget-object v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->a:LX/19o;

    iget-object v3, p1, Lcom/facebook/spherical/model/SphericalVideoParams;->a:LX/19o;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->b:LX/03z;

    iget-object v3, p1, Lcom/facebook/spherical/model/SphericalVideoParams;->b:LX/03z;

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->c:I

    iget v3, p1, Lcom/facebook/spherical/model/SphericalVideoParams;->c:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->d:I

    iget v3, p1, Lcom/facebook/spherical/model/SphericalVideoParams;->d:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->e:I

    iget v3, p1, Lcom/facebook/spherical/model/SphericalVideoParams;->e:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->f:I

    iget v3, p1, Lcom/facebook/spherical/model/SphericalVideoParams;->f:I

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->g:D

    iget-wide v4, p1, Lcom/facebook/spherical/model/SphericalVideoParams;->g:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->h:D

    iget-wide v4, p1, Lcom/facebook/spherical/model/SphericalVideoParams;->h:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->i:Lcom/facebook/spherical/model/GuidedTourParams;

    iget-object v3, p1, Lcom/facebook/spherical/model/SphericalVideoParams;->i:Lcom/facebook/spherical/model/GuidedTourParams;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->j:Z

    iget-boolean v3, p1, Lcom/facebook/spherical/model/SphericalVideoParams;->j:Z

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->k:D

    iget-wide v4, p1, Lcom/facebook/spherical/model/SphericalVideoParams;->k:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->l:D

    iget-wide v4, p1, Lcom/facebook/spherical/model/SphericalVideoParams;->l:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1182551
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->a:LX/19o;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->b:LX/03z;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->g:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->h:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->i:Lcom/facebook/spherical/model/GuidedTourParams;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->k:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-wide v2, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->l:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1182552
    iget-object v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->a:LX/19o;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1182553
    iget-object v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->b:LX/03z;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1182554
    iget v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1182555
    iget v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1182556
    iget v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1182557
    iget v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1182558
    iget-wide v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->g:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1182559
    iget-wide v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->h:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1182560
    iget-object v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->i:Lcom/facebook/spherical/model/GuidedTourParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1182561
    iget-boolean v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1182562
    iget-wide v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->k:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1182563
    iget-wide v0, p0, Lcom/facebook/spherical/model/SphericalVideoParams;->l:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1182564
    return-void

    .line 1182565
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
