.class public Lcom/facebook/spherical/model/KeyframeParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/spherical/model/KeyframeParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:I

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1182472
    new-instance v0, LX/7DA;

    invoke-direct {v0}, LX/7DA;-><init>()V

    sput-object v0, Lcom/facebook/spherical/model/KeyframeParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/7DB;)V
    .locals 2

    .prologue
    .line 1182467
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1182468
    iget-wide v0, p1, LX/7DB;->a:J

    iput-wide v0, p0, Lcom/facebook/spherical/model/KeyframeParams;->a:J

    .line 1182469
    iget v0, p1, LX/7DB;->b:I

    iput v0, p0, Lcom/facebook/spherical/model/KeyframeParams;->b:I

    .line 1182470
    iget v0, p1, LX/7DB;->c:I

    iput v0, p0, Lcom/facebook/spherical/model/KeyframeParams;->c:I

    .line 1182471
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1182450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1182451
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/spherical/model/KeyframeParams;->a:J

    .line 1182452
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/model/KeyframeParams;->b:I

    .line 1182453
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/spherical/model/KeyframeParams;->c:I

    .line 1182454
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1182466
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1182460
    if-ne p1, p0, :cond_1

    .line 1182461
    :cond_0
    :goto_0
    return v0

    .line 1182462
    :cond_1
    instance-of v2, p1, Lcom/facebook/spherical/model/KeyframeParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 1182463
    goto :goto_0

    .line 1182464
    :cond_2
    check-cast p1, Lcom/facebook/spherical/model/KeyframeParams;

    .line 1182465
    iget-wide v2, p0, Lcom/facebook/spherical/model/KeyframeParams;->a:J

    iget-wide v4, p1, Lcom/facebook/spherical/model/KeyframeParams;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget v2, p0, Lcom/facebook/spherical/model/KeyframeParams;->b:I

    iget v3, p1, Lcom/facebook/spherical/model/KeyframeParams;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/facebook/spherical/model/KeyframeParams;->c:I

    iget v3, p1, Lcom/facebook/spherical/model/KeyframeParams;->c:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1182459
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/spherical/model/KeyframeParams;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/spherical/model/KeyframeParams;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/facebook/spherical/model/KeyframeParams;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1182455
    iget-wide v0, p0, Lcom/facebook/spherical/model/KeyframeParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1182456
    iget v0, p0, Lcom/facebook/spherical/model/KeyframeParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1182457
    iget v0, p0, Lcom/facebook/spherical/model/KeyframeParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1182458
    return-void
.end method
