.class public Lcom/facebook/spherical/GlMediaRenderThread;
.super Landroid/os/HandlerThread;
.source ""

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field public static C:LX/0SG;

.field private static D:I


# instance fields
.field public A:I

.field private final B:Ljava/lang/String;

.field private final E:Landroid/graphics/SurfaceTexture;

.field private final F:Landroid/hardware/SensorEventListener;

.field public final G:Landroid/view/Choreographer$FrameCallback;

.field public final H:Landroid/widget/Scroller;

.field public final I:Landroid/view/Choreographer;

.field private final J:Landroid/hardware/SensorManager;

.field public final K:[F

.field public final L:LX/3IO;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ViewportOrientationTracker.lock"
    .end annotation
.end field

.field public M:Z

.field public N:Z

.field public O:Z

.field private P:I

.field public Q:I

.field private R:Ljava/lang/Throwable;

.field public final a:Landroid/view/WindowManager;

.field public final b:LX/7D0;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final d:[F

.field public final e:[F

.field public final f:LX/7Ce;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "RenderThreadController.lock"
    .end annotation
.end field

.field public g:LX/5PW;

.field public h:Landroid/os/Handler;

.field public i:Ljava/lang/Runnable;

.field public j:Ljava/lang/Runnable;

.field public k:Z

.field public l:F

.field public m:F

.field public n:F

.field public o:F

.field public p:F

.field public q:Z

.field public r:LX/7Cm;

.field public volatile s:Z

.field public volatile t:Z

.field public u:F

.field public v:F

.field public w:F

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1181746
    const/16 v0, 0xf

    sput v0, Lcom/facebook/spherical/GlMediaRenderThread;->D:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/graphics/SurfaceTexture;Ljava/lang/Runnable;Ljava/lang/Runnable;IILX/7D0;LX/0Ot;LX/0SG;LX/0wW;LX/7Cz;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/graphics/SurfaceTexture;",
            "Ljava/lang/Runnable;",
            "Ljava/lang/Runnable;",
            "II",
            "LX/7D0;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0SG;",
            "LX/0wW;",
            "LX/7Cz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1181644
    const-string v0, "GlMediaRenderThread"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 1181645
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->B:Ljava/lang/String;

    .line 1181646
    new-instance v0, LX/7Cn;

    invoke-direct {v0, p0}, LX/7Cn;-><init>(Lcom/facebook/spherical/GlMediaRenderThread;)V

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->F:Landroid/hardware/SensorEventListener;

    .line 1181647
    new-instance v0, LX/7Ck;

    invoke-direct {v0, p0}, LX/7Ck;-><init>(Lcom/facebook/spherical/GlMediaRenderThread;)V

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->G:Landroid/view/Choreographer$FrameCallback;

    .line 1181648
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->d:[F

    .line 1181649
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->K:[F

    .line 1181650
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->e:[F

    .line 1181651
    new-instance v0, LX/3IO;

    invoke-direct {v0}, LX/3IO;-><init>()V

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    .line 1181652
    const/high16 v0, 0x42b40000    # 90.0f

    iput v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->l:F

    .line 1181653
    const/high16 v0, 0x42b40000    # 90.0f

    iput v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->m:F

    .line 1181654
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->n:F

    .line 1181655
    const/high16 v0, 0x42b40000    # 90.0f

    iput v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->o:F

    .line 1181656
    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->p:F

    .line 1181657
    sget-object v0, LX/7Cm;->RENDER_AXIS_DEFAULT:LX/7Cm;

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->r:LX/7Cm;

    .line 1181658
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->N:Z

    .line 1181659
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->O:Z

    .line 1181660
    const/high16 v0, 0x428c0000    # 70.0f

    iput v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    .line 1181661
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->v:F

    .line 1181662
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->w:F

    .line 1181663
    iput-object p2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->E:Landroid/graphics/SurfaceTexture;

    .line 1181664
    iput-object p3, p0, Lcom/facebook/spherical/GlMediaRenderThread;->i:Ljava/lang/Runnable;

    .line 1181665
    iput-object p4, p0, Lcom/facebook/spherical/GlMediaRenderThread;->j:Ljava/lang/Runnable;

    .line 1181666
    iput-object p7, p0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    .line 1181667
    iput-object p8, p0, Lcom/facebook/spherical/GlMediaRenderThread;->c:LX/0Ot;

    .line 1181668
    sput-object p9, Lcom/facebook/spherical/GlMediaRenderThread;->C:LX/0SG;

    .line 1181669
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->I:Landroid/view/Choreographer;

    .line 1181670
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->J:Landroid/hardware/SensorManager;

    .line 1181671
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->a:Landroid/view/WindowManager;

    .line 1181672
    new-instance v0, Landroid/widget/Scroller;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->H:Landroid/widget/Scroller;

    .line 1181673
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->H:Landroid/widget/Scroller;

    const v1, 0x3bb851ec    # 0.005625f

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->setFriction(F)V

    .line 1181674
    iget-boolean v0, p11, LX/7Cz;->e:Z

    .line 1181675
    iput-boolean v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->M:Z

    .line 1181676
    iget-boolean v0, p11, LX/7Cz;->f:Z

    .line 1181677
    iput-boolean v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->q:Z

    .line 1181678
    iget-object v0, p11, LX/7Cz;->b:LX/19o;

    if-eqz v0, :cond_0

    .line 1181679
    iget-object v0, p11, LX/7Cz;->b:LX/19o;

    invoke-virtual {p0, v0}, Lcom/facebook/spherical/GlMediaRenderThread;->a(LX/19o;)V

    .line 1181680
    :cond_0
    sget-object v0, LX/7Cj;->a:[I

    iget-object v1, p11, LX/7Cz;->c:LX/7Cl;

    invoke-virtual {v1}, LX/7Cl;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1181681
    new-instance v0, LX/7Cf;

    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->a:Landroid/view/WindowManager;

    iget-object v2, p11, LX/7Cz;->a:LX/7DG;

    invoke-direct {v0, v1, v2}, LX/7Cf;-><init>(Landroid/view/WindowManager;LX/7DG;)V

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    .line 1181682
    :goto_0
    iget-object v0, p11, LX/7Cz;->g:Ljava/lang/Float;

    if-eqz v0, :cond_1

    iget-object v0, p11, LX/7Cz;->g:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->isNaN()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1181683
    iget-object v0, p11, LX/7Cz;->g:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/spherical/GlMediaRenderThread;->a(FZ)V

    .line 1181684
    :cond_1
    iget-object v0, p11, LX/7Cz;->h:Ljava/lang/Float;

    if-eqz v0, :cond_2

    iget-object v0, p11, LX/7Cz;->h:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->isNaN()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1181685
    iget-object v0, p11, LX/7Cz;->h:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/spherical/GlMediaRenderThread;->a(F)V

    .line 1181686
    :cond_2
    iget-object v0, p11, LX/7Cz;->j:Ljava/lang/Float;

    if-eqz v0, :cond_3

    iget-object v0, p11, LX/7Cz;->j:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->isNaN()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p11, LX/7Cz;->i:Ljava/lang/Float;

    if-eqz v0, :cond_3

    iget-object v0, p11, LX/7Cz;->i:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->isNaN()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1181687
    iget-object v0, p11, LX/7Cz;->j:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v1, p11, LX/7Cz;->i:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/spherical/GlMediaRenderThread;->a(FF)V

    .line 1181688
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, p5, p6, v0}, Lcom/facebook/spherical/GlMediaRenderThread;->a(IIZ)V

    .line 1181689
    return-void

    .line 1181690
    :pswitch_0
    new-instance v0, LX/7Cu;

    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->a:Landroid/view/WindowManager;

    iget-object v2, p11, LX/7Cz;->a:LX/7DG;

    invoke-direct {v0, v1, v2, p10}, LX/7Cu;-><init>(Landroid/view/WindowManager;LX/7DG;LX/0wW;)V

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    goto :goto_0

    .line 1181691
    :pswitch_1
    new-instance v0, LX/7Cg;

    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->a:Landroid/view/WindowManager;

    invoke-direct {v0, v1}, LX/7Cg;-><init>(Landroid/view/WindowManager;)V

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    goto :goto_0

    .line 1181692
    :pswitch_2
    new-instance v0, LX/7Ch;

    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->a:Landroid/view/WindowManager;

    invoke-direct {v0, v1}, LX/7Ch;-><init>(Landroid/view/WindowManager;)V

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private d([I)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    .line 1181693
    const/4 v0, 0x0

    iget v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->x:I

    aput v1, p1, v0

    .line 1181694
    iget v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->y:I

    aput v0, p1, v2

    .line 1181695
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->H:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1181696
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    instance-of v0, v0, LX/7Cu;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    check-cast v0, LX/7Cu;

    .line 1181697
    iget-boolean v4, v0, LX/7Cu;->t:Z

    if-eqz v4, :cond_0

    .line 1181698
    iget-boolean v5, v0, LX/7Cu;->A:Z

    if-eqz v5, :cond_4

    .line 1181699
    :cond_0
    :goto_0
    iget-boolean v4, v0, LX/7Cu;->u:Z

    if-eqz v4, :cond_1

    .line 1181700
    iget-boolean v5, v0, LX/7Cu;->B:Z

    if-eqz v5, :cond_5

    .line 1181701
    :cond_1
    :goto_1
    iget-boolean v3, v0, LX/7Cu;->u:Z

    move v0, v3

    .line 1181702
    if-eqz v0, :cond_3

    .line 1181703
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->H:Landroid/widget/Scroller;

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 1181704
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v0, v0, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1181705
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->K:[F

    iget-object v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    invoke-virtual {v0, v1, v2}, LX/7Ce;->a([FLX/3IO;)V

    .line 1181706
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v0, v0, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1181707
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    iget-object v0, v0, LX/3IO;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1181708
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    iget v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->x:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->y:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, v0, LX/3IO;->c:F

    .line 1181709
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    iget v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    iput v1, v0, LX/3IO;->b:F

    .line 1181710
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->L:LX/3IO;

    iget-object v0, v0, LX/3IO;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1181711
    return-void

    .line 1181712
    :cond_3
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->H:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    .line 1181713
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->H:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iget v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->z:I

    sub-int/2addr v0, v1

    .line 1181714
    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->H:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    iget v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->A:I

    sub-int/2addr v1, v2

    .line 1181715
    iget-object v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->H:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    iput v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->z:I

    .line 1181716
    iget-object v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->H:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    iput v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->A:I

    .line 1181717
    iget-object v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v2, v2, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1181718
    iget-object v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {v2, v0, v1}, LX/7Ce;->b(FF)V

    .line 1181719
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v0, v0, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_2

    .line 1181720
    :cond_4
    iget-object v5, v0, LX/7Cu;->y:LX/0wd;

    const-wide/16 v7, 0x0

    invoke-virtual {v5, v7, v8}, LX/0wd;->a(D)LX/0wd;

    .line 1181721
    iget-object v5, v0, LX/7Cu;->y:LX/0wd;

    invoke-virtual {v5}, LX/0wd;->k()LX/0wd;

    .line 1181722
    iget-object v5, v0, LX/7Cu;->y:LX/0wd;

    new-instance v6, LX/7Cr;

    invoke-direct {v6, v0}, LX/7Cr;-><init>(LX/7Cu;)V

    invoke-virtual {v5, v6}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1181723
    iget-object v5, v0, LX/7Cu;->y:LX/0wd;

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v5, v7, v8}, LX/0wd;->b(D)LX/0wd;

    .line 1181724
    const/4 v5, 0x1

    iput-boolean v5, v0, LX/7Cu;->A:Z

    goto/16 :goto_0

    .line 1181725
    :cond_5
    iget-object v5, v0, LX/7Cu;->z:LX/0wd;

    const-wide/16 v7, 0x0

    invoke-virtual {v5, v7, v8}, LX/0wd;->a(D)LX/0wd;

    .line 1181726
    iget-object v5, v0, LX/7Cu;->z:LX/0wd;

    invoke-virtual {v5}, LX/0wd;->k()LX/0wd;

    .line 1181727
    iget-object v5, v0, LX/7Cu;->z:LX/0wd;

    new-instance v6, LX/7Cs;

    invoke-direct {v6, v0}, LX/7Cs;-><init>(LX/7Cu;)V

    invoke-virtual {v5, v6}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1181728
    iget-object v5, v0, LX/7Cu;->z:LX/0wd;

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v5, v7, v8}, LX/0wd;->b(D)LX/0wd;

    .line 1181729
    const/4 v5, 0x1

    iput-boolean v5, v0, LX/7Cu;->B:Z

    goto/16 :goto_1
.end method

.method public static p(Lcom/facebook/spherical/GlMediaRenderThread;)V
    .locals 0

    .prologue
    .line 1181730
    invoke-virtual {p0}, Lcom/facebook/spherical/GlMediaRenderThread;->quit()Z

    .line 1181731
    invoke-virtual {p0}, Lcom/facebook/spherical/GlMediaRenderThread;->m()V

    .line 1181732
    return-void
.end method

.method public static r(Lcom/facebook/spherical/GlMediaRenderThread;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 1181733
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->Q:I

    .line 1181734
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->J:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->J:Landroid/hardware/SensorManager;

    sget v2, Lcom/facebook/spherical/GlMediaRenderThread;->D:I

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    invoke-virtual {v0, p0, v1, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    .line 1181735
    if-nez v0, :cond_0

    .line 1181736
    const/16 v0, 0xb

    sput v0, Lcom/facebook/spherical/GlMediaRenderThread;->D:I

    .line 1181737
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->J:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->J:Landroid/hardware/SensorManager;

    sget v2, Lcom/facebook/spherical/GlMediaRenderThread;->D:I

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    invoke-virtual {v0, p0, v1, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    .line 1181738
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/facebook/spherical/GlMediaRenderThread;->B:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".registerSensorListener"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Device failed to register listener for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/facebook/spherical/GlMediaRenderThread;->s()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1181739
    :cond_0
    if-nez v0, :cond_1

    .line 1181740
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->q:Z

    .line 1181741
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".registerSensorListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Device failed to register listener for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/facebook/spherical/GlMediaRenderThread;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1181742
    :cond_1
    return-void
.end method

.method private static s()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1181759
    sget v0, Lcom/facebook/spherical/GlMediaRenderThread;->D:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_0

    const-string v0, "TYPE_GAME_ROTATION_VECTOR"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "TYPE_ROTATION_VECTOR"

    goto :goto_0
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    .line 1181743
    iput p1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->o:F

    .line 1181744
    iget v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/spherical/GlMediaRenderThread;->a(FZ)V

    .line 1181745
    return-void
.end method

.method public a(FF)V
    .locals 1

    .prologue
    .line 1181747
    iput p1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->v:F

    .line 1181748
    iput p2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->w:F

    .line 1181749
    iget-boolean v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->N:Z

    if-nez v0, :cond_0

    .line 1181750
    :goto_0
    return-void

    .line 1181751
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->N:Z

    .line 1181752
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v0, v0, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1181753
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    invoke-virtual {v0, p1, p2}, LX/7Ce;->a(FF)V

    .line 1181754
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v0, v0, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0
.end method

.method public final a(FZ)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1181755
    iput p1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    .line 1181756
    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v1, v1, v0}, Lcom/facebook/spherical/GlMediaRenderThread;->a(IIZ)V

    .line 1181757
    return-void

    .line 1181758
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(IIZ)V
    .locals 6

    .prologue
    .line 1181635
    if-lez p1, :cond_0

    .line 1181636
    iput p1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->x:I

    .line 1181637
    :cond_0
    if-lez p2, :cond_1

    .line 1181638
    iput p2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->y:I

    .line 1181639
    :cond_1
    iget v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->x:I

    int-to-float v0, v0

    iget v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->y:I

    int-to-float v1, v1

    div-float v3, v0, v1

    .line 1181640
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->d:[F

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    const v4, 0x3dcccccd    # 0.1f

    const/high16 v5, 0x42c80000    # 100.0f

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->perspectiveM([FIFFFF)V

    .line 1181641
    return-void
.end method

.method public final a(LX/19o;)V
    .locals 1

    .prologue
    .line 1181642
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    invoke-interface {v0, p1}, LX/7D0;->a(LX/19o;)V

    .line 1181643
    return-void
.end method

.method public a(Landroid/os/Message;)Z
    .locals 1

    .prologue
    .line 1181634
    const/4 v0, 0x0

    return v0
.end method

.method public b([I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1181624
    iget-boolean v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->s:Z

    if-eqz v0, :cond_1

    .line 1181625
    :cond_0
    :goto_0
    return-void

    .line 1181626
    :cond_1
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->I:Landroid/view/Choreographer;

    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->G:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1181627
    invoke-direct {p0, p1}, Lcom/facebook/spherical/GlMediaRenderThread;->d([I)V

    .line 1181628
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    iget v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->u:F

    invoke-interface {v0, v1}, LX/7D0;->a(F)V

    .line 1181629
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->K:[F

    iget-object v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->d:[F

    iget-object v3, p0, Lcom/facebook/spherical/GlMediaRenderThread;->e:[F

    aget v4, p1, v6

    aget v5, p1, v7

    invoke-interface/range {v0 .. v5}, LX/7D0;->a([F[F[FII)V

    .line 1181630
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->g:LX/5PW;

    invoke-virtual {v0}, LX/5PV;->b()V

    .line 1181631
    iget-boolean v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->O:Z

    if-eqz v0, :cond_0

    instance-of v0, p0, LX/7DK;

    if-eqz v0, :cond_0

    .line 1181632
    iput-boolean v6, p0, Lcom/facebook/spherical/GlMediaRenderThread;->O:Z

    .line 1181633
    check-cast p0, LX/7DK;

    invoke-interface {p0, v7}, LX/7DK;->e(Z)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1181621
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->s:Z

    .line 1181622
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->J:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 1181623
    return-void
.end method

.method public c([I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1181565
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/spherical/GlMediaRenderThread;->l()V

    .line 1181566
    iget v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->P:I

    if-eqz v0, :cond_0

    .line 1181567
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GlMediaRenderThread-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->P:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Succeeded creating an OutputSurface after "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/facebook/spherical/GlMediaRenderThread;->P:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " retries!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/spherical/GlMediaRenderThread;->R:Ljava/lang/Throwable;

    invoke-virtual {v0, v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1181568
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->R:Ljava/lang/Throwable;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1181569
    :cond_0
    invoke-static {p0}, Lcom/facebook/spherical/GlMediaRenderThread;->r(Lcom/facebook/spherical/GlMediaRenderThread;)V

    .line 1181570
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->I:Landroid/view/Choreographer;

    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->G:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1181571
    :goto_0
    return-void

    .line 1181572
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1181573
    iget v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->P:I

    if-nez v0, :cond_1

    .line 1181574
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iget-object v2, p0, Lcom/facebook/spherical/GlMediaRenderThread;->B:Ljava/lang/String;

    const-string v3, "Failed to create OutputSurface"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1181575
    :cond_1
    iput-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->R:Ljava/lang/Throwable;

    .line 1181576
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->g:LX/5PW;

    if-eqz v0, :cond_2

    .line 1181577
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->g:LX/5PW;

    invoke-virtual {v0}, LX/5PV;->c()V

    .line 1181578
    iput-object v5, p0, Lcom/facebook/spherical/GlMediaRenderThread;->g:LX/5PW;

    .line 1181579
    :cond_2
    iget v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->P:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->P:I

    const/4 v2, 0x2

    if-le v0, v2, :cond_3

    .line 1181580
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GlMediaRenderThread-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/facebook/spherical/GlMediaRenderThread;->P:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to create OutputSurface after "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/facebook/spherical/GlMediaRenderThread;->P:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " retries! Aborting!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1181581
    iput-object v5, p0, Lcom/facebook/spherical/GlMediaRenderThread;->R:Ljava/lang/Throwable;

    .line 1181582
    throw v1

    .line 1181583
    :cond_3
    const-wide/16 v0, 0x64

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1181584
    :goto_1
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->h:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :catch_1
    goto :goto_1
.end method

.method public final e(FF)V
    .locals 1

    .prologue
    .line 1181617
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v0, v0, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1181618
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    invoke-virtual {v0, p1, p2}, LX/7Ce;->c(FF)V

    .line 1181619
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->f:LX/7Ce;

    iget-object v0, v0, LX/7Ce;->h:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1181620
    return-void
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 1181613
    new-instance v0, LX/5PW;

    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->E:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, LX/5PW;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->g:LX/5PW;

    .line 1181614
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->g:LX/5PW;

    invoke-virtual {v0}, LX/5PV;->a()V

    .line 1181615
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    invoke-interface {v0}, LX/7D0;->b()V

    .line 1181616
    return-void
.end method

.method public m()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1181594
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->I:Landroid/view/Choreographer;

    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->G:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1181595
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->J:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 1181596
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->b:LX/7D0;

    invoke-interface {v0}, LX/7D0;->c()V

    .line 1181597
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->g:LX/5PW;

    if-eqz v0, :cond_1

    .line 1181598
    const/4 v0, 0x0

    .line 1181599
    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->i:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 1181600
    :try_start_0
    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->g:LX/5PW;

    invoke-virtual {v1}, LX/5PV;->a()V

    .line 1181601
    const/16 v1, 0x4000

    invoke-static {v1}, Landroid/opengl/GLES20;->glClear(I)V

    .line 1181602
    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->g:LX/5PW;

    invoke-virtual {v1}, LX/5PV;->b()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1181603
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->g:LX/5PW;

    invoke-virtual {v1}, LX/5PV;->c()V

    .line 1181604
    iput-object v4, p0, Lcom/facebook/spherical/GlMediaRenderThread;->g:LX/5PW;

    .line 1181605
    iget-object v1, p0, Lcom/facebook/spherical/GlMediaRenderThread;->i:Ljava/lang/Runnable;

    if-eqz v1, :cond_1

    .line 1181606
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->j:Ljava/lang/Runnable;

    :goto_1
    const v2, 0x5ecb0e53

    invoke-static {v1, v0, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1181607
    :cond_1
    iput-object v4, p0, Lcom/facebook/spherical/GlMediaRenderThread;->h:Landroid/os/Handler;

    .line 1181608
    return-void

    .line 1181609
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1181610
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/facebook/spherical/GlMediaRenderThread;->B:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".releaseResources"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error encountered in clearing the SurfaceTexture"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1181611
    const/4 v0, 0x1

    goto :goto_0

    .line 1181612
    :cond_2
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->i:Ljava/lang/Runnable;

    goto :goto_1
.end method

.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 1181593
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 1

    .prologue
    .line 1181590
    iget-boolean v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->q:Z

    if-eqz v0, :cond_0

    .line 1181591
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->F:Landroid/hardware/SensorEventListener;

    invoke-interface {v0, p1}, Landroid/hardware/SensorEventListener;->onSensorChanged(Landroid/hardware/SensorEvent;)V

    .line 1181592
    :cond_0
    return-void
.end method

.method public final declared-synchronized start()V
    .locals 2

    .prologue
    .line 1181585
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/os/HandlerThread;->start()V

    .line 1181586
    new-instance v0, LX/7Ci;

    invoke-virtual {p0}, Lcom/facebook/spherical/GlMediaRenderThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/7Ci;-><init>(Lcom/facebook/spherical/GlMediaRenderThread;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->h:Landroid/os/Handler;

    .line 1181587
    iget-object v0, p0, Lcom/facebook/spherical/GlMediaRenderThread;->h:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1181588
    monitor-exit p0

    return-void

    .line 1181589
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
