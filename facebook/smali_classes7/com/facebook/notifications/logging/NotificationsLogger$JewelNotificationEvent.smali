.class public final Lcom/facebook/notifications/logging/NotificationsLogger$JewelNotificationEvent;
.super Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLoggerEvent;
.source ""


# instance fields
.field public final synthetic c:LX/2Yg;


# direct methods
.method public constructor <init>(LX/2Yg;LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
    .locals 1

    .prologue
    .line 1312174
    iput-object p1, p0, Lcom/facebook/notifications/logging/NotificationsLogger$JewelNotificationEvent;->c:LX/2Yg;

    .line 1312175
    const-string v0, "notifications_jewel_module"

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLoggerEvent;-><init>(LX/2Yg;LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Ljava/lang/String;)V

    .line 1312176
    return-void
.end method

.method public constructor <init>(LX/2Yg;LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1312177
    iput-object p1, p0, Lcom/facebook/notifications/logging/NotificationsLogger$JewelNotificationEvent;->c:LX/2Yg;

    .line 1312178
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLoggerEvent;-><init>(LX/2Yg;LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Ljava/lang/String;)V

    .line 1312179
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
    .locals 2

    .prologue
    .line 1312180
    const-string v0, "hn"

    .line 1312181
    iget-boolean v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->j:Z

    move v1, v1

    .line 1312182
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1312183
    const-string v0, "iu"

    .line 1312184
    iget-boolean v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->k:Z

    move v1, v1

    .line 1312185
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1312186
    const-string v0, "pos"

    .line 1312187
    iget v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->u:I

    move v1, v1

    .line 1312188
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1312189
    const-string v0, "seen_state"

    .line 1312190
    iget-object v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->v:Ljava/lang/String;

    move-object v1, v1

    .line 1312191
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1312192
    return-void
.end method
