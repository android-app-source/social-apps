.class public final Lcom/facebook/notifications/logging/NotificationsLogger$NextNotificationEvent;
.super Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLoggerEvent;
.source ""


# instance fields
.field public final synthetic c:LX/2Yg;


# direct methods
.method public constructor <init>(LX/2Yg;LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
    .locals 1

    .prologue
    .line 1312193
    iput-object p1, p0, Lcom/facebook/notifications/logging/NotificationsLogger$NextNotificationEvent;->c:LX/2Yg;

    .line 1312194
    const-string v0, "next_notifications_module"

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLoggerEvent;-><init>(LX/2Yg;LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;Ljava/lang/String;)V

    .line 1312195
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V
    .locals 2

    .prologue
    .line 1312196
    const-string v0, "hn"

    .line 1312197
    iget-boolean v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->j:Z

    move v1, v1

    .line 1312198
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1312199
    const-string v0, "iu"

    .line 1312200
    iget-boolean v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->k:Z

    move v1, v1

    .line 1312201
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1312202
    const-string v0, "pos"

    .line 1312203
    iget v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->u:I

    move v1, v1

    .line 1312204
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1312205
    const-string v0, "seen_state"

    .line 1312206
    iget-object v1, p1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->v:Ljava/lang/String;

    move-object v1, v1

    .line 1312207
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1312208
    return-void
.end method
