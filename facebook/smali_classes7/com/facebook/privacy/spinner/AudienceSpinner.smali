.class public Lcom/facebook/privacy/spinner/AudienceSpinner;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/8SQ;

.field public b:Lcom/facebook/privacy/ui/PrivacyOptionView;

.field public c:LX/5ON;

.field public d:LX/8SP;

.field private e:F

.field private f:I

.field private g:Z

.field private h:LX/8SM;

.field private final i:Landroid/widget/AdapterView$OnItemClickListener;

.field private final j:Landroid/view/View$OnClickListener;

.field private final k:LX/2dD;

.field public l:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/8SN;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/8SO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1346575
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1346576
    new-instance v0, LX/8SJ;

    invoke-direct {v0, p0}, LX/8SJ;-><init>(Lcom/facebook/privacy/spinner/AudienceSpinner;)V

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->i:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1346577
    new-instance v0, LX/8SK;

    invoke-direct {v0, p0}, LX/8SK;-><init>(Lcom/facebook/privacy/spinner/AudienceSpinner;)V

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->j:Landroid/view/View$OnClickListener;

    .line 1346578
    new-instance v0, LX/8SL;

    invoke-direct {v0, p0}, LX/8SL;-><init>(Lcom/facebook/privacy/spinner/AudienceSpinner;)V

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->k:LX/2dD;

    .line 1346579
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/privacy/spinner/AudienceSpinner;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1346580
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1346543
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1346544
    new-instance v0, LX/8SJ;

    invoke-direct {v0, p0}, LX/8SJ;-><init>(Lcom/facebook/privacy/spinner/AudienceSpinner;)V

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->i:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1346545
    new-instance v0, LX/8SK;

    invoke-direct {v0, p0}, LX/8SK;-><init>(Lcom/facebook/privacy/spinner/AudienceSpinner;)V

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->j:Landroid/view/View$OnClickListener;

    .line 1346546
    new-instance v0, LX/8SL;

    invoke-direct {v0, p0}, LX/8SL;-><init>(Lcom/facebook/privacy/spinner/AudienceSpinner;)V

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->k:LX/2dD;

    .line 1346547
    invoke-direct {p0, p1, p2}, Lcom/facebook/privacy/spinner/AudienceSpinner;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1346548
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1346549
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1346550
    new-instance v0, LX/8SJ;

    invoke-direct {v0, p0}, LX/8SJ;-><init>(Lcom/facebook/privacy/spinner/AudienceSpinner;)V

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->i:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1346551
    new-instance v0, LX/8SK;

    invoke-direct {v0, p0}, LX/8SK;-><init>(Lcom/facebook/privacy/spinner/AudienceSpinner;)V

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->j:Landroid/view/View$OnClickListener;

    .line 1346552
    new-instance v0, LX/8SL;

    invoke-direct {v0, p0}, LX/8SL;-><init>(Lcom/facebook/privacy/spinner/AudienceSpinner;)V

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->k:LX/2dD;

    .line 1346553
    invoke-direct {p0, p1, p2}, Lcom/facebook/privacy/spinner/AudienceSpinner;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1346554
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v2, 0x41000000    # 8.0f

    .line 1346555
    const-class v0, Lcom/facebook/privacy/spinner/AudienceSpinner;

    invoke-static {v0, p0}, Lcom/facebook/privacy/spinner/AudienceSpinner;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1346556
    invoke-virtual {p0}, Lcom/facebook/privacy/spinner/AudienceSpinner;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030140

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1346557
    const v0, 0x7f0d060b

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/spinner/AudienceSpinner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/ui/PrivacyOptionView;

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->b:Lcom/facebook/privacy/ui/PrivacyOptionView;

    .line 1346558
    new-instance v0, LX/5ON;

    invoke-direct {v0, p1}, LX/5ON;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    .line 1346559
    if-eqz p2, :cond_1

    .line 1346560
    sget-object v0, LX/03r;->AudienceSpinner:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1346561
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->e:F

    .line 1346562
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->f:I

    .line 1346563
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->g:Z

    .line 1346564
    invoke-static {}, LX/8SM;->values()[LX/8SM;

    move-result-object v1

    const/16 v2, 0x3

    sget-object v3, LX/8SM;->AUDIENCE_SHOW_NONE:LX/8SM;

    invoke-virtual {v3}, LX/8SM;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->h:LX/8SM;

    .line 1346565
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1346566
    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->b:Lcom/facebook/privacy/ui/PrivacyOptionView;

    iget-boolean v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->g:Z

    .line 1346567
    iput-boolean v1, v0, Lcom/facebook/privacy/ui/PrivacyOptionView;->c:Z

    .line 1346568
    iget v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->f:I

    if-lez v0, :cond_0

    .line 1346569
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->b:Lcom/facebook/privacy/ui/PrivacyOptionView;

    iget v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->f:I

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setMaxWidth(I)V

    .line 1346570
    :cond_0
    return-void

    .line 1346571
    :cond_1
    iput v2, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->e:F

    .line 1346572
    iput v3, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->f:I

    .line 1346573
    iput-boolean v4, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->g:Z

    .line 1346574
    sget-object v0, LX/8SM;->AUDIENCE_SHOW_NONE:LX/8SM;

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->h:LX/8SM;

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/privacy/spinner/AudienceSpinner;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/privacy/spinner/AudienceSpinner;

    const-class v1, LX/8SQ;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/8SQ;

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->a:LX/8SQ;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/privacy/spinner/AudienceSpinner;LX/1oT;)V
    .locals 1

    .prologue
    .line 1346481
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->d:LX/8SP;

    .line 1346482
    iput-object p1, v0, LX/8SP;->f:LX/1oT;

    .line 1346483
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->b:Lcom/facebook/privacy/ui/PrivacyOptionView;

    invoke-virtual {v0, p1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setPrivacyOption(LX/1oT;)V

    .line 1346484
    return-void
.end method

.method private setupViewAndPopover(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1346510
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    iget-object v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->d:LX/8SP;

    .line 1346511
    iput-object v1, v0, LX/5ON;->m:Landroid/widget/ListAdapter;

    .line 1346512
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    iget-object v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->d:LX/8SP;

    invoke-virtual {v1}, LX/8SP;->b()I

    move-result v1

    .line 1346513
    iput v1, v0, LX/5ON;->n:I

    .line 1346514
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    iget v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->e:F

    .line 1346515
    iput v1, v0, LX/5ON;->o:F

    .line 1346516
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, LX/0ht;->b(F)V

    .line 1346517
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    invoke-virtual {v0, v2}, LX/5OM;->a(Z)V

    .line 1346518
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    .line 1346519
    iput-boolean v2, v0, LX/0ht;->e:Z

    .line 1346520
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    iget-object v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->i:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1346521
    iput-object v1, v0, LX/5ON;->p:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1346522
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    iget-object v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->k:LX/2dD;

    .line 1346523
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 1346524
    if-eqz p1, :cond_0

    .line 1346525
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->b:Lcom/facebook/privacy/ui/PrivacyOptionView;

    iget-object v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1346526
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->b:Lcom/facebook/privacy/ui/PrivacyOptionView;

    iget-boolean v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->g:Z

    .line 1346527
    iput-boolean v1, v0, Lcom/facebook/privacy/ui/PrivacyOptionView;->c:Z

    .line 1346528
    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->b:Lcom/facebook/privacy/ui/PrivacyOptionView;

    iget-object v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->d:LX/8SP;

    .line 1346529
    iget-object v2, v1, LX/8SP;->f:LX/1oT;

    move-object v1, v2

    .line 1346530
    invoke-virtual {v0, v1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setPrivacyOption(LX/1oT;)V

    .line 1346531
    return-void

    .line 1346532
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->b:Lcom/facebook/privacy/ui/PrivacyOptionView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1346533
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->b:Lcom/facebook/privacy/ui/PrivacyOptionView;

    const/4 v1, 0x0

    .line 1346534
    iput-boolean v1, v0, Lcom/facebook/privacy/ui/PrivacyOptionView;->c:Z

    .line 1346535
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1346536
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    .line 1346537
    iput-object v1, v0, LX/5ON;->m:Landroid/widget/ListAdapter;

    .line 1346538
    iput-object v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->d:LX/8SP;

    .line 1346539
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->b:Lcom/facebook/privacy/ui/PrivacyOptionView;

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1346540
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    .line 1346541
    iput-object v1, v0, LX/5ON;->p:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1346542
    return-void
.end method

.method public final a(LX/8SP;Z)V
    .locals 0

    .prologue
    .line 1346507
    iput-object p1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->d:LX/8SP;

    .line 1346508
    invoke-direct {p0, p2}, Lcom/facebook/privacy/spinner/AudienceSpinner;->setupViewAndPopover(Z)V

    .line 1346509
    return-void
.end method

.method public final a(LX/8SR;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 1346501
    invoke-virtual {p1}, LX/8SR;->b()LX/1oT;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/8SR;->a(LX/1oT;)I

    move-result v0

    .line 1346502
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Selected privacy option must be in the list of options."

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1346503
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->a:LX/8SQ;

    iget-object v2, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->h:LX/8SM;

    invoke-virtual {v0, p1, v2, p2}, LX/8SQ;->a(LX/8SR;LX/8SM;Ljava/lang/String;)LX/8SP;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->d:LX/8SP;

    .line 1346504
    invoke-direct {p0, v1}, Lcom/facebook/privacy/spinner/AudienceSpinner;->setupViewAndPopover(Z)V

    .line 1346505
    return-void

    .line 1346506
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x313cb44c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1346495
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1346496
    iget-object v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    .line 1346497
    iget-boolean v2, v1, LX/0ht;->r:Z

    move v1, v2

    .line 1346498
    if-eqz v1, :cond_0

    .line 1346499
    iget-object v1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    invoke-virtual {v1}, LX/0ht;->l()V

    .line 1346500
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x1b6411b2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAudienceExplanationDisplayMode(LX/8SM;)V
    .locals 0

    .prologue
    .line 1346493
    iput-object p1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->h:LX/8SM;

    .line 1346494
    return-void
.end method

.method public setMaxRows(F)V
    .locals 1

    .prologue
    .line 1346489
    iput p1, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->e:F

    .line 1346490
    iget-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->c:LX/5ON;

    .line 1346491
    iput p1, v0, LX/5ON;->o:F

    .line 1346492
    return-void
.end method

.method public setPrivacyChangeListener(LX/8SN;)V
    .locals 1

    .prologue
    .line 1346487
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->l:Ljava/lang/ref/WeakReference;

    .line 1346488
    return-void
.end method

.method public setSelectorOpenedListener(LX/8SO;)V
    .locals 1

    .prologue
    .line 1346485
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/privacy/spinner/AudienceSpinner;->m:Ljava/lang/ref/WeakReference;

    .line 1346486
    return-void
.end method
