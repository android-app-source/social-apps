.class public final Lcom/facebook/privacy/edit/EditStoryPrivacyActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1342446
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1342447
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1342448
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1342449
    const v0, 0x7f030460

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/edit/EditStoryPrivacyActivity;->setContentView(I)V

    .line 1342450
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1342451
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1342452
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setShowDividers(Z)V

    .line 1342453
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 1342454
    new-instance v1, LX/8Q4;

    invoke-direct {v1, p0}, LX/8Q4;-><init>(Lcom/facebook/privacy/edit/EditStoryPrivacyActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1342455
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/privacy/edit/EditStoryPrivacyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1342456
    new-instance v1, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    invoke-direct {v1}, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;-><init>()V

    .line 1342457
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1342458
    move-object v0, v1

    .line 1342459
    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyActivity;->p:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    .line 1342460
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d0d2e

    iget-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyActivity;->p:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    .line 1342461
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1342462
    return-void
.end method

.method public final onBackPressed()V
    .locals 7

    .prologue
    .line 1342463
    iget-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyActivity;->p:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    if-eqz v0, :cond_1

    .line 1342464
    iget-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyActivity;->p:Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    .line 1342465
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->i:LX/1Ck;

    sget-object v2, LX/8QA;->SET_STORY_PRIVACY_FROM_ATF:LX/8QA;

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1342466
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->i:LX/1Ck;

    sget-object v2, LX/8QA;->FETCH_PRIVACY_FROM_ATF:LX/8QA;

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1342467
    invoke-static {v0}, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->l(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1342468
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    if-nez v1, :cond_2

    .line 1342469
    invoke-static {v0}, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->s(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)V

    .line 1342470
    :cond_0
    :goto_0
    return-void

    .line 1342471
    :cond_1
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0

    .line 1342472
    :cond_2
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-virtual {v1}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1342473
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-virtual {v1}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->c()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    .line 1342474
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v2, v2

    .line 1342475
    iput-object v2, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1342476
    iget-boolean v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v1, v2

    .line 1342477
    iput-boolean v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->t:Z

    .line 1342478
    :goto_1
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->e:LX/2c9;

    sget-object v2, LX/2Zv;->EDIT_STORY_PRIVACY_CANCEL:LX/2Zv;

    invoke-virtual {v1, v2}, LX/2c9;->a(LX/2Zv;)V

    .line 1342479
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1342480
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->k:Landroid/view/inputmethod/InputMethodManager;

    .line 1342481
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1342482
    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1342483
    invoke-static {v0}, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->r(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1342484
    invoke-static {v0}, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->s(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)V

    .line 1342485
    :goto_2
    invoke-static {v0}, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->s(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)V

    goto :goto_0

    .line 1342486
    :cond_3
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->q:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    if-nez v1, :cond_4

    .line 1342487
    invoke-static {v0}, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->s(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)V

    goto :goto_0

    .line 1342488
    :cond_4
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->q:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-virtual {v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->S_()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1342489
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->q:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-virtual {v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    .line 1342490
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v2, v2

    .line 1342491
    iput-object v2, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1342492
    iget-boolean v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v1, v2

    .line 1342493
    iput-boolean v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->t:Z

    .line 1342494
    goto :goto_1

    .line 1342495
    :cond_5
    const v1, 0x7f081304

    invoke-static {v1, v4, v4, v3}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->s:Landroid/support/v4/app/DialogFragment;

    .line 1342496
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->s:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1342497
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->f:LX/8Pp;

    .line 1342498
    iget-object v2, v1, LX/8Pp;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x160006

    invoke-interface {v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1342499
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->l:Lcom/facebook/privacy/edit/EditStoryPrivacyParams;

    iget-object v1, v1, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1342500
    iget-object v1, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->i:LX/1Ck;

    sget-object v2, LX/8QA;->SET_STORY_PRIVACY_FROM_ATF:LX/8QA;

    iget-object v3, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->g:Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v4, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->l:Lcom/facebook/privacy/edit/EditStoryPrivacyParams;

    iget-object v4, v4, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->d:Ljava/lang/String;

    iget-object v5, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->l:Lcom/facebook/privacy/edit/EditStoryPrivacyParams;

    iget-object v5, v5, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->c:Ljava/lang/String;

    iget-object v6, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->l:Lcom/facebook/privacy/edit/EditStoryPrivacyParams;

    iget-object v6, v6, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->e:Ljava/lang/String;

    iget-object p0, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v3, v4, v5, v6, p0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v4, LX/8Q9;

    invoke-direct {v4, v0}, LX/8Q9;-><init>(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_2

    .line 1342501
    :cond_6
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1342502
    const-string v2, "privacy_option"

    iget-object v3, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v1, v2, v3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1342503
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1342504
    invoke-static {v0}, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->s(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)V

    goto/16 :goto_2
.end method
