.class public final Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Sh;

.field public b:LX/8RJ;

.field public c:LX/03V;

.field public d:LX/0Uh;

.field public e:LX/2c9;

.field public f:LX/8Pp;

.field public g:Lcom/facebook/privacy/PrivacyOperationsClient;

.field public h:LX/0ad;

.field public i:LX/1Ck;

.field public j:LX/0kL;

.field public k:Landroid/view/inputmethod/InputMethodManager;

.field public l:Lcom/facebook/privacy/edit/EditStoryPrivacyParams;

.field public m:LX/8QW;

.field public n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public o:Lcom/facebook/privacy/model/SelectablePrivacyData;

.field public p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

.field public q:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

.field public r:Landroid/support/v4/app/DialogFragment;

.field public s:Landroid/support/v4/app/DialogFragment;

.field public t:Z

.field public u:Landroid/view/View;

.field public v:Landroid/view/View;

.field public w:Landroid/view/View;

.field public final x:LX/8Q5;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1342603
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1342604
    new-instance v0, LX/8Q6;

    invoke-direct {v0, p0}, LX/8Q6;-><init>(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->x:LX/8Q5;

    return-void
.end method

.method public static e(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)V
    .locals 5

    .prologue
    .line 1342605
    iget-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->v:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1342606
    iget-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->r:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1342607
    iget-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->l:Lcom/facebook/privacy/edit/EditStoryPrivacyParams;

    iget-object v0, v0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->a:LX/8QD;

    sget-object v1, LX/8QD;->ALBUM:LX/8QD;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->g:Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->l:Lcom/facebook/privacy/edit/EditStoryPrivacyParams;

    iget-object v1, v1, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->d:Ljava/lang/String;

    .line 1342608
    new-instance v2, LX/5nC;

    invoke-direct {v2}, LX/5nC;-><init>()V

    move-object v2, v2

    .line 1342609
    const-string v3, "album_id"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1342610
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 1342611
    iget-object v2, v0, Lcom/facebook/privacy/PrivacyOperationsClient;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-virtual {v2, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1342612
    new-instance v3, LX/8Pc;

    invoke-direct {v3, v0}, LX/8Pc;-><init>(Lcom/facebook/privacy/PrivacyOperationsClient;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v2, v2

    .line 1342613
    invoke-static {v0, v2}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 1342614
    :goto_0
    new-instance v1, LX/8Q8;

    invoke-direct {v1, p0}, LX/8Q8;-><init>(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)V

    .line 1342615
    iget-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->i:LX/1Ck;

    sget-object v3, LX/8QA;->FETCH_PRIVACY_FROM_ATF:LX/8QA;

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1342616
    return-void

    .line 1342617
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->g:Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->l:Lcom/facebook/privacy/edit/EditStoryPrivacyParams;

    iget-object v1, v1, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->d:Ljava/lang/String;

    .line 1342618
    new-instance v2, LX/5nD;

    invoke-direct {v2}, LX/5nD;-><init>()V

    move-object v3, v2

    .line 1342619
    const-string v2, "story_id"

    invoke-virtual {v3, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1342620
    iget-object v2, v0, Lcom/facebook/privacy/PrivacyOperationsClient;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1342621
    new-instance v3, LX/8Pd;

    invoke-direct {v3, v0}, LX/8Pd;-><init>(Lcom/facebook/privacy/PrivacyOperationsClient;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v2, v2

    .line 1342622
    invoke-static {v0, v2}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 1342623
    goto :goto_0
.end method

.method public static l(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1342624
    iget-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->d:LX/0Uh;

    const/16 v2, 0x440

    invoke-virtual {v1, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    .line 1342625
    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1342626
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->h:LX/0ad;

    sget-short v2, LX/8Po;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method

.method public static r(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1342627
    iget-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->o:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1342628
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1342629
    if-nez v1, :cond_1

    .line 1342630
    :cond_0
    :goto_0
    return v0

    .line 1342631
    :cond_1
    iget-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->o:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1342632
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1342633
    iget-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v1, v2}, LX/2cA;->a(LX/1oU;LX/1oU;)Z

    move-result v1

    .line 1342634
    if-eqz v1, :cond_0

    .line 1342635
    iget-boolean v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->t:Z

    iget-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->o:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1342636
    iget-boolean p0, v2, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v2, p0

    .line 1342637
    if-ne v1, v2, :cond_0

    .line 1342638
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)V
    .locals 1

    .prologue
    .line 1342639
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1342640
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 1342641
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1342642
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/8RJ;->a(LX/0QB;)LX/8RJ;

    move-result-object v4

    check-cast v4, LX/8RJ;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v0}, LX/2c9;->b(LX/0QB;)LX/2c9;

    move-result-object v7

    check-cast v7, LX/2c9;

    invoke-static {v0}, LX/8Pp;->a(LX/0QB;)LX/8Pp;

    move-result-object v8

    check-cast v8, LX/8Pp;

    invoke-static {v0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;

    move-result-object v9

    check-cast v9, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v11

    check-cast v11, LX/1Ck;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object p1

    check-cast p1, LX/0kL;

    invoke-static {v0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v3, v2, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->a:LX/0Sh;

    iput-object v4, v2, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->b:LX/8RJ;

    iput-object v5, v2, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->c:LX/03V;

    iput-object v6, v2, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->d:LX/0Uh;

    iput-object v7, v2, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->e:LX/2c9;

    iput-object v8, v2, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->f:LX/8Pp;

    iput-object v9, v2, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->g:Lcom/facebook/privacy/PrivacyOperationsClient;

    iput-object v10, v2, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->h:LX/0ad;

    iput-object v11, v2, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->i:LX/1Ck;

    iput-object p1, v2, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->j:LX/0kL;

    iput-object v0, v2, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->k:Landroid/view/inputmethod/InputMethodManager;

    .line 1342643
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1342644
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1342645
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1342646
    const-string v1, "params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;

    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->l:Lcom/facebook/privacy/edit/EditStoryPrivacyParams;

    .line 1342647
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1342648
    const-string v1, "initial_privacy"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1342649
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1342650
    const-string v1, "initial_privacy"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1342651
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x573ddbcf

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1342652
    iget-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->b:LX/8RJ;

    sget-object v2, LX/8RI;->EDIT_STORY_PRIVACY_FRAGMENT:LX/8RI;

    invoke-virtual {v1, v2}, LX/8RJ;->a(LX/8RI;)V

    .line 1342653
    iget-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->e:LX/2c9;

    sget-object v2, LX/2Zv;->EDIT_STORY_PRIVACY_OPEN:LX/2Zv;

    invoke-virtual {v1, v2}, LX/2c9;->a(LX/2Zv;)V

    .line 1342654
    const v1, 0x7f030461

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->u:Landroid/view/View;

    .line 1342655
    iget-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->u:Landroid/view/View;

    const v2, 0x7f0d0d2f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->w:Landroid/view/View;

    .line 1342656
    iget-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->u:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, 0x7727988b

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x6aef14d7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1342657
    iget-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->i:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1342658
    iput-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->a:LX/0Sh;

    .line 1342659
    iput-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->c:LX/03V;

    .line 1342660
    iput-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->e:LX/2c9;

    .line 1342661
    iput-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->f:LX/8Pp;

    .line 1342662
    iput-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->g:Lcom/facebook/privacy/PrivacyOperationsClient;

    .line 1342663
    iput-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->i:LX/1Ck;

    .line 1342664
    iput-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->j:LX/0kL;

    .line 1342665
    iput-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->m:LX/8QW;

    .line 1342666
    iput-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->n:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1342667
    iput-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->o:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1342668
    iput-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->q:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    .line 1342669
    iput-object v2, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->p:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    .line 1342670
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1342671
    const/16 v1, 0x2b

    const v2, -0x3cd0df9b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5e4ab6a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1342672
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1342673
    const/4 v4, 0x1

    .line 1342674
    const v1, 0x7f081303

    const/4 v2, 0x0

    invoke-static {v1, v4, v4, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->r:Landroid/support/v4/app/DialogFragment;

    .line 1342675
    iget-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->u:Landroid/view/View;

    const v2, 0x7f0d0d30

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->v:Landroid/view/View;

    .line 1342676
    iget-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->v:Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setClickable(Z)V

    .line 1342677
    iget-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->v:Landroid/view/View;

    new-instance v2, LX/8Q7;

    invoke-direct {v2, p0}, LX/8Q7;-><init>(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1342678
    invoke-static {p0}, Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;->e(Lcom/facebook/privacy/edit/EditStoryPrivacyFragment;)V

    .line 1342679
    const/16 v1, 0x2b

    const v2, 0x48e5ad52

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
