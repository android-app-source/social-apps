.class public Lcom/facebook/privacy/edit/EditStoryPrivacyParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/edit/EditStoryPrivacyParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/8QD;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1342694
    new-instance v0, LX/8QB;

    invoke-direct {v0}, LX/8QB;-><init>()V

    sput-object v0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/8QC;)V
    .locals 1

    .prologue
    .line 1342695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1342696
    sget-object v0, LX/8QD;->STORY:LX/8QD;

    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->a:LX/8QD;

    .line 1342697
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->b:Ljava/lang/Boolean;

    .line 1342698
    iget-object v0, p1, LX/8QC;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->b:Ljava/lang/Boolean;

    .line 1342699
    iget-object v0, p1, LX/8QC;->b:LX/8QD;

    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->a:LX/8QD;

    .line 1342700
    iget-object v0, p1, LX/8QC;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->c:Ljava/lang/String;

    .line 1342701
    iget-object v0, p1, LX/8QC;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->d:Ljava/lang/String;

    .line 1342702
    iget-object v0, p1, LX/8QC;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->e:Ljava/lang/String;

    .line 1342703
    iget-boolean v0, p1, LX/8QC;->f:Z

    iput-boolean v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->f:Z

    .line 1342704
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1342705
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1342706
    sget-object v1, LX/8QD;->STORY:LX/8QD;

    iput-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->a:LX/8QD;

    .line 1342707
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->b:Ljava/lang/Boolean;

    .line 1342708
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->b:Ljava/lang/Boolean;

    .line 1342709
    invoke-static {}, LX/8QD;->values()[LX/8QD;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->a:LX/8QD;

    .line 1342710
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->c:Ljava/lang/String;

    .line 1342711
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->d:Ljava/lang/String;

    .line 1342712
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->e:Ljava/lang/String;

    .line 1342713
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->f:Z

    .line 1342714
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1342715
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1342716
    iget-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1342717
    iget-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->a:LX/8QD;

    invoke-virtual {v0}, LX/8QD;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1342718
    iget-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1342719
    iget-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1342720
    iget-object v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1342721
    iget-boolean v0, p0, Lcom/facebook/privacy/edit/EditStoryPrivacyParams;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1342722
    return-void

    .line 1342723
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
