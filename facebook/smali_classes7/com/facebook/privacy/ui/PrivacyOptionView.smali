.class public Lcom/facebook/privacy/ui/PrivacyOptionView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field private a:LX/8Sa;

.field private b:LX/0wM;

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1346874
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 1346875
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/privacy/ui/PrivacyOptionView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1346876
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1346871
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1346872
    invoke-direct {p0, p1, p2}, Lcom/facebook/privacy/ui/PrivacyOptionView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1346873
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1346868
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1346869
    invoke-direct {p0, p1, p2}, Lcom/facebook/privacy/ui/PrivacyOptionView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1346870
    return-void
.end method

.method private a(II)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1346864
    iget-object v0, p0, Lcom/facebook/privacy/ui/PrivacyOptionView;->b:LX/0wM;

    invoke-virtual {p0}, Lcom/facebook/privacy/ui/PrivacyOptionView;->getCurrentTextColor()I

    move-result v1

    invoke-virtual {v0, p1, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1346865
    invoke-virtual {p0}, Lcom/facebook/privacy/ui/PrivacyOptionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1346866
    invoke-virtual {v0, v2, v2, v1, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1346867
    return-object v0
.end method

.method private a(LX/8Sa;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1346861
    iput-object p1, p0, Lcom/facebook/privacy/ui/PrivacyOptionView;->a:LX/8Sa;

    .line 1346862
    iput-object p2, p0, Lcom/facebook/privacy/ui/PrivacyOptionView;->b:LX/0wM;

    .line 1346863
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1346854
    const-class v0, Lcom/facebook/privacy/ui/PrivacyOptionView;

    invoke-static {v0, p0}, Lcom/facebook/privacy/ui/PrivacyOptionView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1346855
    if-eqz p2, :cond_0

    .line 1346856
    sget-object v0, LX/03r;->PrivacyOptionView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1346857
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/privacy/ui/PrivacyOptionView;->c:Z

    .line 1346858
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1346859
    :goto_0
    return-void

    .line 1346860
    :cond_0
    iput-boolean v2, p0, Lcom/facebook/privacy/ui/PrivacyOptionView;->c:Z

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/privacy/ui/PrivacyOptionView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/privacy/ui/PrivacyOptionView;

    invoke-static {v1}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v0

    check-cast v0, LX/8Sa;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-direct {p0, v0, v1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->a(LX/8Sa;LX/0wM;)V

    return-void
.end method

.method private setViewDrawables(I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1346848
    if-lez p1, :cond_1

    .line 1346849
    const v0, 0x7f0b0ba0

    invoke-direct {p0, p1, v0}, Lcom/facebook/privacy/ui/PrivacyOptionView;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1346850
    :goto_0
    iget-boolean v2, p0, Lcom/facebook/privacy/ui/PrivacyOptionView;->c:Z

    if-eqz v2, :cond_0

    .line 1346851
    const v2, 0x7f020a12

    const v3, 0x7f0b0ba1

    invoke-direct {p0, v2, v3}, Lcom/facebook/privacy/ui/PrivacyOptionView;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1346852
    :goto_1
    invoke-virtual {p0, v0, v1, v2, v1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1346853
    return-void

    :cond_0
    move-object v2, v1

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1346845
    invoke-virtual {p0, p1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setText(Ljava/lang/CharSequence;)V

    .line 1346846
    invoke-direct {p0, p2}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setViewDrawables(I)V

    .line 1346847
    return-void
.end method

.method public final a(Ljava/lang/String;LX/1Fd;)V
    .locals 2

    .prologue
    .line 1346840
    invoke-virtual {p0, p1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setText(Ljava/lang/CharSequence;)V

    .line 1346841
    if-eqz p2, :cond_0

    .line 1346842
    iget-object v0, p0, Lcom/facebook/privacy/ui/PrivacyOptionView;->a:LX/8Sa;

    sget-object v1, LX/8SZ;->TOKEN:LX/8SZ;

    invoke-virtual {v0, p2, v1}, LX/8Sa;->a(LX/1Fd;LX/8SZ;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setViewDrawables(I)V

    .line 1346843
    :goto_0
    return-void

    .line 1346844
    :cond_0
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setViewDrawables(I)V

    goto :goto_0
.end method

.method public setPrivacyOption(LX/1oT;)V
    .locals 3

    .prologue
    .line 1346833
    if-nez p1, :cond_0

    .line 1346834
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setText(Ljava/lang/CharSequence;)V

    .line 1346835
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setViewDrawables(I)V

    .line 1346836
    :goto_0
    return-void

    .line 1346837
    :cond_0
    invoke-interface {p1}, LX/1oT;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setText(Ljava/lang/CharSequence;)V

    .line 1346838
    invoke-static {p1}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v1

    sget-object v2, LX/8SZ;->TOKEN:LX/8SZ;

    invoke-static {v1, v2}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v0

    .line 1346839
    invoke-direct {p0, v0}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setViewDrawables(I)V

    goto :goto_0
.end method

.method public setShowChevron(Z)V
    .locals 0

    .prologue
    .line 1346831
    iput-boolean p1, p0, Lcom/facebook/privacy/ui/PrivacyOptionView;->c:Z

    .line 1346832
    return-void
.end method
