.class public Lcom/facebook/privacy/selector/AudiencePickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field private a:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

.field private b:LX/8Qv;

.field public c:LX/8Qu;

.field private d:LX/2c9;

.field private e:Landroid/view/View;

.field public f:Landroid/widget/FrameLayout;

.field public g:Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;

.field public h:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

.field public i:Lcom/facebook/privacy/model/AudiencePickerInput;

.field public j:Lcom/facebook/privacy/model/AudiencePickerModel;

.field public k:LX/8Qi;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/privacy/model/AudiencePickerModel;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/8Qz;

.field private final n:LX/8R4;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1344256
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1344257
    new-instance v0, LX/8Qy;

    invoke-direct {v0, p0}, LX/8Qy;-><init>(Lcom/facebook/privacy/selector/AudiencePickerFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->l:LX/0Or;

    .line 1344258
    new-instance v0, LX/8R0;

    invoke-direct {v0, p0}, LX/8R0;-><init>(Lcom/facebook/privacy/selector/AudiencePickerFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->m:LX/8Qz;

    .line 1344259
    new-instance v0, LX/8R5;

    invoke-direct {v0, p0}, LX/8R5;-><init>(Lcom/facebook/privacy/selector/AudiencePickerFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->n:LX/8R4;

    .line 1344260
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;)I
    .locals 3

    .prologue
    .line 1344243
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344244
    iget-object v2, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    move-object v0, v2

    .line 1344245
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1344246
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344247
    iget-object v2, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->d:LX/0Px;

    move-object v0, v2

    .line 1344248
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1344249
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344250
    iget-object v2, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    move-object v0, v2

    .line 1344251
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1344252
    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 1344253
    :goto_1
    return v1

    .line 1344254
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1344255
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private static a(Lcom/facebook/privacy/model/PrivacyOptionsResult;Z)Lcom/facebook/privacy/model/AudiencePickerModel;
    .locals 5

    .prologue
    .line 1344214
    new-instance v2, LX/8QH;

    invoke-direct {v2}, LX/8QH;-><init>()V

    .line 1344215
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1344216
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    .line 1344217
    iput-object v0, v2, LX/8QH;->a:LX/0Px;

    .line 1344218
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    .line 1344219
    iput-object v0, v2, LX/8QH;->b:LX/0Px;

    .line 1344220
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->primaryOptionIndices:LX/0Px;

    .line 1344221
    iput-object v0, v2, LX/8QH;->c:LX/0Px;

    .line 1344222
    iget-object v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    .line 1344223
    iput-object v0, v2, LX/8QH;->d:LX/0Px;

    .line 1344224
    iget v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOptionIndex:I

    .line 1344225
    iput v0, v2, LX/8QH;->e:I

    .line 1344226
    iget v0, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOptionIndex:I

    .line 1344227
    iput v0, v2, LX/8QH;->f:I

    .line 1344228
    iput-boolean p1, v2, LX/8QH;->g:Z

    .line 1344229
    iget-object v3, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    .line 1344230
    invoke-static {v3}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1344231
    iput-object v3, v2, LX/8QH;->d:LX/0Px;

    .line 1344232
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1344233
    iget-object v4, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1344234
    invoke-static {v0}, LX/2cA;->d(LX/1oS;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1344235
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    .line 1344236
    iput-object v0, v2, LX/8QH;->i:LX/0Px;

    .line 1344237
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1344238
    :cond_1
    invoke-static {v0}, LX/2cA;->b(LX/1oS;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1344239
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v0

    .line 1344240
    iput-object v0, v2, LX/8QH;->j:LX/0Px;

    .line 1344241
    goto :goto_1

    .line 1344242
    :cond_2
    invoke-virtual {v2}, LX/8QH;->a()Lcom/facebook/privacy/model/AudiencePickerModel;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/8Qv;LX/2c9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1344210
    iput-object p1, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    .line 1344211
    iput-object p2, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->b:LX/8Qv;

    .line 1344212
    iput-object p3, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->d:LX/2c9;

    .line 1344213
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-static {v2}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    const-class v1, LX/8Qv;

    invoke-interface {v2, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/8Qv;

    invoke-static {v2}, LX/2c9;->b(LX/0QB;)LX/2c9;

    move-result-object v2

    check-cast v2, LX/2c9;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/8Qv;LX/2c9;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/privacy/selector/AudiencePickerFragment;I)V
    .locals 2

    .prologue
    .line 1344207
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->k:LX/8Qi;

    if-nez v0, :cond_0

    .line 1344208
    :goto_0
    return-void

    .line 1344209
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->k:LX/8Qi;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/8Qi;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static b(Lcom/facebook/privacy/model/AudiencePickerInput;)Lcom/facebook/privacy/model/AudiencePickerModel;
    .locals 13

    .prologue
    const/4 v5, 0x0

    .line 1344128
    iget-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerInput;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v0, v0

    .line 1344129
    iget-object v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1344130
    iget-boolean v1, p0, Lcom/facebook/privacy/model/AudiencePickerInput;->b:Z

    move v1, v1

    .line 1344131
    invoke-static {v0, v1}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a(Lcom/facebook/privacy/model/PrivacyOptionsResult;Z)Lcom/facebook/privacy/model/AudiencePickerModel;

    move-result-object v0

    .line 1344132
    iget-object v1, p0, Lcom/facebook/privacy/model/AudiencePickerInput;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v1, v1

    .line 1344133
    iget-boolean v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v1, v2

    .line 1344134
    if-eqz v1, :cond_a

    .line 1344135
    invoke-virtual {v0}, Lcom/facebook/privacy/model/AudiencePickerModel;->l()LX/8QH;

    move-result-object v0

    const/4 v1, 0x1

    .line 1344136
    iput-boolean v1, v0, LX/8QH;->h:Z

    .line 1344137
    move-object v0, v0

    .line 1344138
    invoke-virtual {v0}, LX/8QH;->a()Lcom/facebook/privacy/model/AudiencePickerModel;

    move-result-object v0

    move-object v2, v0

    .line 1344139
    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerInput;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v0, v0

    .line 1344140
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v7, v1

    .line 1344141
    iget-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerInput;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v0, v0

    .line 1344142
    iget-object v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v4, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    .line 1344143
    invoke-static {v7}, LX/2cA;->c(LX/1oS;)Z

    move-result v8

    .line 1344144
    invoke-static {v7}, LX/2cA;->e(LX/1oS;)Z

    move-result v9

    .line 1344145
    if-nez v8, :cond_0

    if-eqz v9, :cond_5

    .line 1344146
    :cond_0
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1344147
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1344148
    invoke-static {v0}, LX/2cA;->f(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1344149
    invoke-static {v1}, LX/2cA;->f(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1344150
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {v4, v5, v0}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    move-object v3, v0

    .line 1344151
    :goto_1
    iget-object v0, v2, Lcom/facebook/privacy/model/AudiencePickerModel;->d:LX/0Px;

    move-object v10, v0

    .line 1344152
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v6, v5

    :goto_2
    if-ge v6, v11, :cond_5

    invoke-virtual {v10, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1344153
    iget-object v1, v2, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    move-object v1, v1

    .line 1344154
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {v1, v12}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1344155
    if-eqz v8, :cond_3

    invoke-static {v1}, LX/2cA;->b(LX/1oS;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1344156
    invoke-virtual {v2}, Lcom/facebook/privacy/model/AudiencePickerModel;->l()LX/8QH;

    move-result-object v1

    .line 1344157
    iput-object v3, v1, LX/8QH;->a:LX/0Px;

    .line 1344158
    move-object v1, v1

    .line 1344159
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1344160
    iput v0, v1, LX/8QH;->e:I

    .line 1344161
    move-object v0, v1

    .line 1344162
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v1

    .line 1344163
    iput-object v1, v0, LX/8QH;->j:LX/0Px;

    .line 1344164
    move-object v0, v0

    .line 1344165
    invoke-virtual {v0}, LX/8QH;->a()Lcom/facebook/privacy/model/AudiencePickerModel;

    move-result-object v2

    .line 1344166
    :cond_1
    :goto_3
    return-object v2

    .line 1344167
    :cond_2
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v5, v0}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    move-object v3, v0

    goto :goto_1

    .line 1344168
    :cond_3
    if-eqz v9, :cond_4

    invoke-static {v1}, LX/2cA;->d(LX/1oS;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1344169
    invoke-virtual {v2}, Lcom/facebook/privacy/model/AudiencePickerModel;->l()LX/8QH;

    move-result-object v1

    .line 1344170
    iput-object v3, v1, LX/8QH;->a:LX/0Px;

    .line 1344171
    move-object v1, v1

    .line 1344172
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1344173
    iput v0, v1, LX/8QH;->e:I

    .line 1344174
    move-object v0, v1

    .line 1344175
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v1

    .line 1344176
    iput-object v1, v0, LX/8QH;->i:LX/0Px;

    .line 1344177
    move-object v0, v0

    .line 1344178
    invoke-virtual {v0}, LX/8QH;->a()Lcom/facebook/privacy/model/AudiencePickerModel;

    move-result-object v2

    goto :goto_3

    .line 1344179
    :cond_4
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_2

    :cond_5
    move v1, v5

    .line 1344180
    :goto_4
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 1344181
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1344182
    iget-object v3, v2, Lcom/facebook/privacy/model/AudiencePickerModel;->d:LX/0Px;

    move-object v3, v3

    .line 1344183
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {v0, v7}, LX/2cA;->a(LX/1oU;LX/1oU;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1344184
    invoke-virtual {v2}, Lcom/facebook/privacy/model/AudiencePickerModel;->l()LX/8QH;

    move-result-object v0

    .line 1344185
    iput v1, v0, LX/8QH;->e:I

    .line 1344186
    move-object v0, v0

    .line 1344187
    invoke-virtual {v0}, LX/8QH;->a()Lcom/facebook/privacy/model/AudiencePickerModel;

    move-result-object v2

    goto :goto_3

    .line 1344188
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1344189
    :cond_7
    add-int/lit8 v5, v5, 0x1

    .line 1344190
    :cond_8
    iget-object v0, v2, Lcom/facebook/privacy/model/AudiencePickerModel;->b:LX/0Px;

    move-object v0, v0

    .line 1344191
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v5, v0, :cond_1

    .line 1344192
    iget-object v0, v2, Lcom/facebook/privacy/model/AudiencePickerModel;->b:LX/0Px;

    move-object v0, v0

    .line 1344193
    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1344194
    invoke-static {v0, v7}, LX/2cA;->a(LX/1oU;LX/1oU;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1344195
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    invoke-virtual {v0, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1344196
    invoke-virtual {v2}, Lcom/facebook/privacy/model/AudiencePickerModel;->l()LX/8QH;

    move-result-object v1

    .line 1344197
    iput-object v0, v1, LX/8QH;->a:LX/0Px;

    .line 1344198
    move-object v0, v1

    .line 1344199
    iget-object v1, v2, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    move-object v1, v1

    .line 1344200
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 1344201
    iput v1, v0, LX/8QH;->e:I

    .line 1344202
    move-object v0, v0

    .line 1344203
    invoke-virtual {v0}, LX/8QH;->a()Lcom/facebook/privacy/model/AudiencePickerModel;

    move-result-object v2

    goto/16 :goto_3

    :cond_9
    move-object v3, v4

    goto/16 :goto_1

    :cond_a
    move-object v2, v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1344261
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1344262
    const-class v0, Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-static {v0, p0}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1344263
    if-eqz p1, :cond_0

    .line 1344264
    const-string v0, "audience_picker_input"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/AudiencePickerInput;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->i:Lcom/facebook/privacy/model/AudiencePickerInput;

    .line 1344265
    const-string v0, "audience_picker_model_internal"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/AudiencePickerModel;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344266
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-virtual {v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a()V

    .line 1344267
    return-void
.end method

.method public final a(Lcom/facebook/privacy/model/AudiencePickerInput;)V
    .locals 1

    .prologue
    .line 1344204
    iput-object p1, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->i:Lcom/facebook/privacy/model/AudiencePickerInput;

    .line 1344205
    invoke-static {p1}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->b(Lcom/facebook/privacy/model/AudiencePickerInput;)Lcom/facebook/privacy/model/AudiencePickerModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344206
    return-void
.end method

.method public final b()Z
    .locals 15

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x8

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1344047
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->g:Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;

    if-eqz v0, :cond_0

    .line 1344048
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->g:Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 1344049
    iget-object v3, v0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->c:Lcom/facebook/privacy/model/AudiencePickerModel;

    invoke-virtual {v3}, Lcom/facebook/privacy/model/AudiencePickerModel;->l()LX/8QH;

    move-result-object v3

    .line 1344050
    iput-boolean v8, v3, LX/8QH;->k:Z

    .line 1344051
    move-object v9, v3

    .line 1344052
    iget-object v3, v0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->f:LX/8Qx;

    .line 1344053
    iget v4, v3, LX/8Qx;->e:I

    move v10, v4

    .line 1344054
    if-gez v10, :cond_6

    .line 1344055
    invoke-virtual {v9}, LX/8QH;->a()Lcom/facebook/privacy/model/AudiencePickerModel;

    move-result-object v3

    .line 1344056
    :goto_0
    move-object v0, v3

    .line 1344057
    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344058
    iput-object v7, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->g:Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;

    .line 1344059
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1344060
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->c:LX/8Qu;

    const v3, -0x717ee884

    invoke-static {v0, v3}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1344061
    const v0, 0x7f0812fb

    invoke-static {p0, v0}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a$redex0(Lcom/facebook/privacy/selector/AudiencePickerFragment;I)V

    .line 1344062
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    const-string v4, "optionType"

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344063
    iget-object v5, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    move-object v0, v5

    .line 1344064
    iget-object v5, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344065
    iget v6, v5, Lcom/facebook/privacy/model/AudiencePickerModel;->e:I

    move v5, v6

    .line 1344066
    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oV;

    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 1344067
    iget-object v3, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->d:LX/2c9;

    const-string v4, "selected_from_all_lists"

    invoke-virtual {v3, v4, v2, v0}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    move v0, v1

    .line 1344068
    :goto_1
    return v0

    .line 1344069
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->h:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    if-eqz v0, :cond_5

    .line 1344070
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->h:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    invoke-virtual {v0}, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->c()V

    .line 1344071
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->h:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    .line 1344072
    iget-object v3, v0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->k:LX/8RD;

    move-object v3, v3

    .line 1344073
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v4

    const-string v5, "numSelected"

    sget-object v0, LX/8RD;->FRIENDS_EXCEPT:LX/8RD;

    if-ne v3, v0, :cond_2

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344074
    iget-object v8, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->j:LX/0Px;

    move-object v0, v8

    .line 1344075
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    :goto_2
    invoke-virtual {v4, v5, v0}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v4

    .line 1344076
    iget-object v5, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->d:LX/2c9;

    sget-object v0, LX/8RD;->FRIENDS_EXCEPT:LX/8RD;

    if-ne v3, v0, :cond_3

    const-string v0, "friends_except_selected"

    :goto_3
    invoke-virtual {v5, v0, v2, v4}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    .line 1344077
    iput-object v7, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->h:Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    .line 1344078
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1344079
    const v0, 0x7f0812fb

    invoke-static {p0, v0}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a$redex0(Lcom/facebook/privacy/selector/AudiencePickerFragment;I)V

    .line 1344080
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344081
    iget-object v2, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    move-object v0, v2

    .line 1344082
    iget-object v2, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344083
    iget v3, v2, Lcom/facebook/privacy/model/AudiencePickerModel;->e:I

    move v2, v3

    .line 1344084
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1344085
    invoke-static {v0}, LX/2cA;->b(LX/1oS;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344086
    iget-object v3, v2, Lcom/facebook/privacy/model/AudiencePickerModel;->j:LX/0Px;

    move-object v2, v3

    .line 1344087
    invoke-static {v2}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1344088
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    invoke-virtual {v0}, Lcom/facebook/privacy/model/AudiencePickerModel;->l()LX/8QH;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-direct {p0, v2}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;)I

    move-result v2

    .line 1344089
    iput v2, v0, LX/8QH;->e:I

    .line 1344090
    move-object v0, v0

    .line 1344091
    invoke-virtual {v0}, LX/8QH;->a()Lcom/facebook/privacy/model/AudiencePickerModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344092
    :cond_1
    :goto_4
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->c:LX/8Qu;

    const v2, -0x734bcd0d

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    move v0, v1

    .line 1344093
    goto :goto_1

    .line 1344094
    :cond_2
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344095
    iget-object v8, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->i:LX/0Px;

    move-object v0, v8

    .line 1344096
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_2

    .line 1344097
    :cond_3
    const-string v0, "specific_friends_selected"

    goto :goto_3

    .line 1344098
    :cond_4
    invoke-static {v0}, LX/2cA;->d(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344099
    iget-object v2, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->i:LX/0Px;

    move-object v0, v2

    .line 1344100
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1344101
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    invoke-virtual {v0}, Lcom/facebook/privacy/model/AudiencePickerModel;->l()LX/8QH;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->ONLY_ME:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-direct {p0, v2}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;)I

    move-result v2

    .line 1344102
    iput v2, v0, LX/8QH;->e:I

    .line 1344103
    move-object v0, v0

    .line 1344104
    invoke-virtual {v0}, LX/8QH;->a()Lcom/facebook/privacy/model/AudiencePickerModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    goto :goto_4

    :cond_5
    move v0, v2

    .line 1344105
    goto/16 :goto_1

    .line 1344106
    :cond_6
    iget-object v3, v0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->c:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344107
    iget-object v4, v3, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    move-object v11, v4

    .line 1344108
    iget-object v3, v0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->c:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344109
    iget-object v4, v3, Lcom/facebook/privacy/model/AudiencePickerModel;->b:LX/0Px;

    move-object v12, v4

    .line 1344110
    new-instance v13, LX/0Pz;

    invoke-direct {v13}, LX/0Pz;-><init>()V

    move v3, v5

    .line 1344111
    :goto_5
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_7

    .line 1344112
    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v13, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1344113
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1344114
    :cond_7
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v11, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1344115
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_8
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1344116
    invoke-static {v3, v4}, LX/2cA;->a(LX/1oU;LX/1oU;)Z

    move-result v4

    if-eqz v4, :cond_8

    move v5, v8

    .line 1344117
    :cond_9
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 1344118
    if-nez v5, :cond_a

    .line 1344119
    add-int/lit8 v3, v3, 0x1

    .line 1344120
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v11, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v13, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1344121
    :cond_a
    invoke-interface {v12, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v13, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1344122
    invoke-virtual {v13}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 1344123
    iput-object v4, v9, LX/8QH;->a:LX/0Px;

    .line 1344124
    move-object v4, v9

    .line 1344125
    iput v3, v4, LX/8QH;->e:I

    .line 1344126
    move-object v3, v4

    .line 1344127
    invoke-virtual {v3}, LX/8QH;->a()Lcom/facebook/privacy/model/AudiencePickerModel;

    move-result-object v3

    goto/16 :goto_0
.end method

.method public final c()Lcom/facebook/privacy/model/SelectablePrivacyData;
    .locals 4

    .prologue
    .line 1344018
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344019
    iget v1, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->e:I

    move v1, v1

    .line 1344020
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344021
    iget-object v2, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    move-object v0, v2

    .line 1344022
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1344023
    iget-object v2, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344024
    iget-object v3, v2, Lcom/facebook/privacy/model/AudiencePickerModel;->d:LX/0Px;

    move-object v2, v3

    .line 1344025
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1344026
    invoke-static {v0}, LX/2cA;->d(LX/1oS;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1344027
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344028
    iget-object v2, v1, Lcom/facebook/privacy/model/AudiencePickerModel;->i:LX/0Px;

    move-object v1, v2

    .line 1344029
    iget-object v2, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344030
    iget-boolean v3, v2, Lcom/facebook/privacy/model/AudiencePickerModel;->h:Z

    move v2, v3

    .line 1344031
    invoke-static {v0, v1, v2}, LX/8Rp;->b(Landroid/content/res/Resources;Ljava/util/List;Z)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    .line 1344032
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->d:LX/2c9;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/2c9;->b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;Z)V

    .line 1344033
    new-instance v1, LX/8QV;

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->i:Lcom/facebook/privacy/model/AudiencePickerInput;

    .line 1344034
    iget-object v3, v2, Lcom/facebook/privacy/model/AudiencePickerInput;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v2, v3

    .line 1344035
    invoke-direct {v1, v2}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    invoke-virtual {v1, v0}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344036
    iget-boolean v2, v1, Lcom/facebook/privacy/model/AudiencePickerModel;->h:Z

    move v1, v2

    .line 1344037
    iput-boolean v1, v0, LX/8QV;->c:Z

    .line 1344038
    move-object v0, v0

    .line 1344039
    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    .line 1344040
    return-object v0

    .line 1344041
    :cond_1
    invoke-static {v0}, LX/2cA;->b(LX/1oS;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1344042
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344043
    iget-object v2, v1, Lcom/facebook/privacy/model/AudiencePickerModel;->j:LX/0Px;

    move-object v1, v2

    .line 1344044
    iget-object v2, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344045
    iget-boolean v3, v2, Lcom/facebook/privacy/model/AudiencePickerModel;->h:Z

    move v2, v3

    .line 1344046
    invoke-static {v0, v1, v2}, LX/8Rp;->a(Landroid/content/res/Resources;Ljava/util/List;Z)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x769ed607

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1344016
    const v1, 0x7f03013e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->e:Landroid/view/View;

    .line 1344017
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->e:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x3f6066db

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x54a5c29b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1344013
    const v1, 0x7f0812fb

    invoke-static {p0, v1}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a$redex0(Lcom/facebook/privacy/selector/AudiencePickerFragment;I)V

    .line 1344014
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1344015
    const/16 v1, 0x2b

    const v2, -0x31bdfa7d    # -8.1378528E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1344009
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1344010
    const-string v0, "audience_picker_input"

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->i:Lcom/facebook/privacy/model/AudiencePickerInput;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1344011
    const-string v0, "audience_picker_model_internal"

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1344012
    return-void
.end method

.method public final onStart()V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x15012b0e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1344001
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->c:LX/8Qu;

    const v2, 0x135e0fbe

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1344002
    iget-object v2, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->d:LX/2c9;

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344003
    iget-object v3, v0, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    move-object v0, v3

    .line 1344004
    iget-object v3, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->j:Lcom/facebook/privacy/model/AudiencePickerModel;

    .line 1344005
    iget v5, v3, Lcom/facebook/privacy/model/AudiencePickerModel;->e:I

    move v3, v5

    .line 1344006
    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/2c9;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;Z)V

    .line 1344007
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1344008
    const/16 v0, 0x2b

    const v2, 0x6c3668a2

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 1343989
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1343990
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->b:LX/8Qv;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->l:LX/0Or;

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->m:LX/8Qz;

    iget-object v3, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->n:LX/8R4;

    .line 1343991
    new-instance v4, LX/8Qu;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/2c9;->b(LX/0QB;)LX/2c9;

    move-result-object v6

    check-cast v6, LX/2c9;

    invoke-static {v0}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v7

    check-cast v7, LX/8Sa;

    move-object v8, v1

    move-object v9, v2

    move-object v10, v3

    invoke-direct/range {v4 .. v10}, LX/8Qu;-><init>(Landroid/content/Context;LX/2c9;LX/8Sa;LX/0Or;LX/8Qz;LX/8R4;)V

    .line 1343992
    move-object v0, v4

    .line 1343993
    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->c:LX/8Qu;

    .line 1343994
    const v0, 0x7f0d0606

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 1343995
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->c:LX/8Qu;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1343996
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->c:LX/8Qu;

    .line 1343997
    new-instance v2, LX/8Qn;

    invoke-direct {v2, v1}, LX/8Qn;-><init>(LX/8Qu;)V

    move-object v1, v2

    .line 1343998
    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1343999
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->e:Landroid/view/View;

    const v1, 0x7f0d0605

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->f:Landroid/widget/FrameLayout;

    .line 1344000
    return-void
.end method
