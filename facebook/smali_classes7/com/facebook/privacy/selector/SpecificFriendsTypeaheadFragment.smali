.class public Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;
.super Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;
.source ""


# static fields
.field public static final v:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final w:Landroid/text/TextWatcher;

.field public final x:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1346282
    const-class v0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    sput-object v0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;->v:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1346283
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;-><init>()V

    .line 1346284
    new-instance v0, LX/8SG;

    invoke-direct {v0, p0}, LX/8SG;-><init>(Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;->w:Landroid/text/TextWatcher;

    .line 1346285
    new-instance v0, LX/8SH;

    invoke-direct {v0, p0}, LX/8SH;-><init>(Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;->x:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1346286
    invoke-super {p0, p1}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a(Landroid/os/Bundle;)V

    .line 1346287
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    .line 1346288
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6fbbda04

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1346289
    const v1, 0x7f031030

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;->o:Landroid/view/View;

    .line 1346290
    iget-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->o:Landroid/view/View;

    .line 1346291
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    iget-object v3, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->j:LX/8RM;

    invoke-virtual {v2, v3}, LX/8tB;->a(LX/8RK;)V

    .line 1346292
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    new-instance v3, LX/623;

    invoke-direct {v3}, LX/623;-><init>()V

    new-instance p1, LX/623;

    invoke-direct {p1}, LX/623;-><init>()V

    invoke-static {v3, p1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8tB;->a(Ljava/util/List;)V

    .line 1346293
    const v2, 0x7f0d26d6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/listview/BetterListView;

    iput-object v2, p0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    .line 1346294
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1346295
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->u:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1346296
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;->x:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1346297
    const v2, 0x7f0d26d7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;->r:Landroid/view/View;

    .line 1346298
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->r:Landroid/view/View;

    new-instance v3, LX/8SE;

    invoke-direct {v3, p0}, LX/8SE;-><init>(Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1346299
    iget-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->o:Landroid/view/View;

    .line 1346300
    const v2, 0x7f0d26d4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iput-object v2, p0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1346301
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v3, p0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;->w:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1346302
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    sget-object v3, LX/8ut;->NO_DROPDOWN:LX/8ut;

    .line 1346303
    iput-object v3, v2, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->f:LX/8ut;

    .line 1346304
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    sget-object v3, LX/8uy;->PLAIN_TEXT:LX/8uy;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setTextMode(LX/8uy;)V

    .line 1346305
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1346306
    iget-object v3, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1346307
    iput v2, v3, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->p:I

    .line 1346308
    iget-object v3, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setTokenIconColor(I)V

    .line 1346309
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setLongClickable(Z)V

    .line 1346310
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v3, LX/8SF;

    invoke-direct {v3, p0}, LX/8SF;-><init>(Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1346311
    iget-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->o:Landroid/view/View;

    const v2, 0x7f0d26d5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;->n:Landroid/view/View;

    .line 1346312
    iget-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->o:Landroid/view/View;

    const v2, 0x7f0d26d3

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;->s:Landroid/view/View;

    .line 1346313
    iget-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->s:Landroid/view/View;

    new-instance v2, LX/8SC;

    invoke-direct {v2, p0}, LX/8SC;-><init>(Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1346314
    invoke-virtual {p0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->b()V

    .line 1346315
    iget-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, LX/8SD;

    invoke-direct {v2, p0}, LX/8SD;-><init>(Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalFocusChangeListener(Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;)V

    .line 1346316
    iget-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->s:Landroid/view/View;

    .line 1346317
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f010289

    invoke-static {v2, v3}, LX/0WH;->b(Landroid/content/Context;I)LX/0am;

    move-result-object v2

    .line 1346318
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1346319
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v2}, LX/1r0;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1346320
    :cond_0
    iget-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->o:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, 0x4daf9a7c

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method
