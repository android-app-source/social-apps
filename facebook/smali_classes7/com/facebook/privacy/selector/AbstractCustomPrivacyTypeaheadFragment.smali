.class public Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Sh;

.field public b:LX/0TD;

.field public c:LX/3LP;

.field public d:LX/8Rp;

.field public e:Landroid/view/inputmethod/InputMethodManager;

.field public f:LX/0Sy;

.field public g:LX/8tB;

.field public h:LX/8uo;

.field public i:LX/03V;

.field public j:LX/8RM;

.field public k:LX/2RQ;

.field public l:LX/8Qe;

.field public m:LX/8Qf;

.field public n:Landroid/view/View;

.field public o:Landroid/view/View;

.field public p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

.field public q:Lcom/facebook/widget/listview/BetterListView;

.field public r:Landroid/view/View;

.field public s:Landroid/view/View;

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field

.field public final u:Landroid/widget/AbsListView$OnScrollListener;

.field public v:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;>;"
        }
    .end annotation
.end field

.field public w:LX/0RV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0RV",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;>;"
        }
    .end annotation
.end field

.field public x:LX/0RV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0RV",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1343591
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1343592
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->t:Ljava/util/List;

    .line 1343593
    new-instance v0, LX/8Qb;

    invoke-direct {v0, p0}, LX/8Qb;-><init>(Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->u:Landroid/widget/AbsListView$OnScrollListener;

    return-void
.end method

.method public static a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1343584
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v0

    check-cast v0, [LX/8ul;

    .line 1343585
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1343586
    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 1343587
    iget-object p0, v4, LX/8uk;->f:LX/8QK;

    move-object v4, p0

    .line 1343588
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1343589
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1343590
    :cond_0
    return-object v2
.end method

.method public static b(Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;)LX/8QL;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1343564
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 1343565
    :goto_0
    return-object v0

    .line 1343566
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x285feb

    if-ne v0, v2, :cond_2

    .line 1343567
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    sget v2, LX/8Qg;->b:I

    invoke-virtual {v0, v2}, LX/8tB;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8vN;

    .line 1343568
    if-nez v0, :cond_1

    move-object v0, v1

    .line 1343569
    goto :goto_0

    .line 1343570
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v1

    .line 1343571
    iget-object v2, v0, LX/8vN;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    move-object v0, v2

    .line 1343572
    goto :goto_0

    .line 1343573
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0xe198c7c

    if-ne v0, v2, :cond_5

    .line 1343574
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    sget v2, LX/8Qg;->a:I

    invoke-virtual {v0, v2}, LX/8tB;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8ST;

    .line 1343575
    if-nez v0, :cond_3

    move-object v0, v1

    .line 1343576
    goto :goto_0

    .line 1343577
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v1

    .line 1343578
    iget-object v2, v0, LX/8ST;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_4
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    .line 1343579
    iget-object p1, v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->e:Ljava/lang/String;

    move-object p1, p1

    .line 1343580
    invoke-static {p1, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 1343581
    :goto_1
    move-object v0, v2

    .line 1343582
    goto :goto_0

    :cond_5
    move-object v0, v1

    .line 1343583
    goto :goto_0

    :cond_6
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/8QL;)V
    .locals 2

    .prologue
    .line 1343562
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;Z)V

    .line 1343563
    return-void
.end method

.method public final a(LX/8Qe;)V
    .locals 0

    .prologue
    .line 1343559
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1343560
    iput-object p1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->l:LX/8Qe;

    .line 1343561
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 1343556
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1343557
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v0}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v5

    check-cast v5, LX/3LP;

    invoke-static {v0}, LX/8RM;->c(LX/0QB;)LX/8RM;

    move-result-object v6

    check-cast v6, LX/8RM;

    invoke-static {v0}, LX/8Rp;->b(LX/0QB;)LX/8Rp;

    move-result-object v7

    check-cast v7, LX/8Rp;

    invoke-static {v0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v8

    check-cast v8, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v9

    check-cast v9, LX/0Sy;

    invoke-static {v0}, LX/8tB;->b(LX/0QB;)LX/8tB;

    move-result-object v10

    check-cast v10, LX/8tB;

    invoke-static {v0}, LX/8uo;->a(LX/0QB;)LX/8uo;

    move-result-object v11

    check-cast v11, LX/8uo;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p1

    check-cast p1, LX/03V;

    invoke-static {v0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v0

    check-cast v0, LX/2RQ;

    iput-object v3, v2, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a:LX/0Sh;

    iput-object v4, v2, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->b:LX/0TD;

    iput-object v5, v2, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->c:LX/3LP;

    iput-object v7, v2, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->d:LX/8Rp;

    iput-object v8, v2, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->e:Landroid/view/inputmethod/InputMethodManager;

    iput-object v9, v2, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->f:LX/0Sy;

    iput-object v10, v2, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    iput-object v11, v2, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->h:LX/8uo;

    iput-object p1, v2, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->i:LX/03V;

    iput-object v6, v2, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->j:LX/8RM;

    iput-object v0, v2, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->k:LX/2RQ;

    .line 1343558
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1343477
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->b()V

    .line 1343478
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v1, LX/8QQ;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/8QQ;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 1343479
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setEnabled(Z)V

    .line 1343480
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setVisibility(I)V

    .line 1343481
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setAlpha(F)V

    .line 1343482
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->n:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1343483
    return-void
.end method

.method public final c()V
    .locals 11

    .prologue
    .line 1343522
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->d:LX/8Rp;

    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->x:LX/0RV;

    invoke-virtual {v0}, LX/0RV;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->w:LX/0RV;

    invoke-virtual {v1}, LX/0RV;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 1343523
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v6

    .line 1343524
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2cp;

    .line 1343525
    invoke-interface {v3}, LX/2cp;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1343526
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1343527
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1343528
    const/4 v3, 0x3

    .line 1343529
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v4, v3

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1oS;

    .line 1343530
    invoke-virtual {v2, v3}, LX/8Rp;->a(LX/1oS;)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    move-result-object v3

    .line 1343531
    if-eqz v3, :cond_6

    .line 1343532
    invoke-virtual {v5, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1343533
    iget-object v9, v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->e:Ljava/lang/String;

    move-object v9, v9

    .line 1343534
    invoke-virtual {v6, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1343535
    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1343536
    add-int/lit8 v4, v4, -0x1

    move v3, v4

    :goto_2
    move v4, v3

    .line 1343537
    goto :goto_1

    .line 1343538
    :cond_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 1343539
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v3, 0x0

    move v5, v3

    :goto_3
    if-ge v5, v9, :cond_2

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    .line 1343540
    if-lez v4, :cond_2

    .line 1343541
    iget-object v10, v3, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->e:Ljava/lang/String;

    move-object v10, v10

    .line 1343542
    invoke-virtual {v6, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 1343543
    invoke-virtual {v7, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1343544
    add-int/lit8 v3, v4, -0x1

    .line 1343545
    :goto_4
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    goto :goto_3

    .line 1343546
    :cond_2
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1343547
    new-instance v4, LX/8ST;

    invoke-direct {v4, v8, v3}, LX/8ST;-><init>(Ljava/util/List;Ljava/util/List;)V

    move-object v0, v4

    .line 1343548
    iget-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    sget v2, LX/8Qg;->a:I

    invoke-virtual {v1, v2, v0}, LX/8tB;->a(ILX/621;)V

    .line 1343549
    invoke-virtual {p0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->k()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1343550
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->b:LX/0TD;

    new-instance v1, LX/8Qc;

    invoke-direct {v1, p0}, LX/8Qc;-><init>(Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1343551
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a:LX/0Sh;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/8Qd;

    invoke-direct {v2, p0}, LX/8Qd;-><init>(Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1343552
    :cond_3
    iget-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->requestFocus()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1343553
    new-instance v1, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment$4;

    invoke-direct {v1, p0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment$4;-><init>(Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;)V

    .line 1343554
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const-wide/16 v3, 0x64

    invoke-virtual {v2, v1, v3, v4}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1343555
    :cond_4
    return-void

    :cond_5
    move v3, v4

    goto :goto_4

    :cond_6
    move v3, v4

    goto :goto_2
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1343510
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->b()V

    .line 1343511
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setEnabled(Z)V

    .line 1343512
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setAlpha(F)V

    .line 1343513
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->w:LX/0RV;

    if-eqz v0, :cond_1

    .line 1343514
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->w:LX/0RV;

    invoke-virtual {v0}, LX/0RV;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1343515
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1343516
    invoke-static {p0, v0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->b(Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;)LX/8QL;

    move-result-object v0

    .line 1343517
    if-eqz v0, :cond_0

    .line 1343518
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    goto :goto_0

    .line 1343519
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->n:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1343520
    invoke-virtual {p0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->e()V

    .line 1343521
    return-void
.end method

.method public e()V
    .locals 7

    .prologue
    .line 1343492
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v2

    .line 1343493
    invoke-virtual {p0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1343494
    :cond_0
    :goto_0
    return-void

    .line 1343495
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1343496
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1343497
    const/4 v1, 0x0

    .line 1343498
    iget-object v5, v0, LX/8QL;->a:LX/8vA;

    sget-object v6, LX/8vA;->USER:LX/8vA;

    if-ne v5, v6, :cond_3

    .line 1343499
    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1343500
    invoke-static {v0}, LX/8Rp;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    move-result-object v0

    .line 1343501
    :goto_2
    if-eqz v0, :cond_2

    .line 1343502
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1343503
    :cond_3
    iget-object v5, v0, LX/8QL;->a:LX/8vA;

    sget-object v6, LX/8vA;->FRIENDLIST:LX/8vA;

    if-ne v5, v6, :cond_6

    .line 1343504
    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    .line 1343505
    invoke-static {v0}, LX/8Rp;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;)Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    move-result-object v0

    goto :goto_2

    .line 1343506
    :cond_4
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->l:LX/8Qe;

    if-eqz v0, :cond_5

    .line 1343507
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->l:LX/8Qe;

    invoke-interface {v0, v3}, LX/8Qe;->a(Ljava/util/List;)V

    .line 1343508
    :cond_5
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->m:LX/8Qf;

    if-eqz v0, :cond_0

    .line 1343509
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->m:LX/8Qf;

    invoke-interface {v0, v2}, LX/8Qf;->a(Ljava/util/List;)V

    goto :goto_0

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 1343491
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->n:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 1343489
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->e:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1343490
    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4cfacc0e    # 1.31489904E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1343484
    iget-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 1343485
    iget-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1343486
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->v:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1343487
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1343488
    const/16 v1, 0x2b

    const v2, -0x7f1aa5c6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
