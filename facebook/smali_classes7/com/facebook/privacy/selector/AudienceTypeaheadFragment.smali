.class public Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public C:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field

.field private D:I

.field private E:Lcom/facebook/privacy/model/SelectablePrivacyData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final F:LX/8SB;

.field private final G:Landroid/widget/AbsListView$OnScrollListener;

.field private final H:Landroid/text/TextWatcher;

.field private final I:Landroid/widget/AdapterView$OnItemClickListener;

.field private b:LX/8Rp;

.field private c:LX/8S9;

.field public d:LX/8RJ;

.field private e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/8Q5;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/8Rm;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/privacy/model/SelectablePrivacyData;

.field private i:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

.field public j:LX/0Sy;

.field private k:Landroid/view/inputmethod/InputMethodManager;

.field private l:LX/03V;

.field private m:LX/2c9;

.field private n:LX/0ad;

.field private o:Landroid/view/View;

.field public p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

.field public q:LX/8tB;

.field private r:Landroid/view/View;

.field private s:Landroid/view/View;

.field private t:Landroid/view/View;

.field public u:Lcom/facebook/widget/listview/BetterListView;

.field private v:Landroid/view/View;

.field private w:Z

.field private x:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

.field private y:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

.field private z:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1345320
    const-class v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    sput-object v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1345321
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1345322
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    .line 1345323
    new-instance v0, LX/8SB;

    invoke-direct {v0}, LX/8SB;-><init>()V

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->F:LX/8SB;

    .line 1345324
    new-instance v0, LX/8RX;

    invoke-direct {v0, p0}, LX/8RX;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->G:Landroid/widget/AbsListView$OnScrollListener;

    .line 1345325
    new-instance v0, LX/8Ra;

    invoke-direct {v0, p0}, LX/8Ra;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->H:Landroid/text/TextWatcher;

    .line 1345326
    new-instance v0, LX/8Rb;

    invoke-direct {v0, p0}, LX/8Rb;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->I:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1345327
    return-void
.end method

.method public static A(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)LX/8QN;
    .locals 2

    .prologue
    .line 1345318
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    sget v1, LX/8Rk;->a:I

    invoke-virtual {v0, v1}, LX/8tB;->g(I)LX/621;

    move-result-object v0

    check-cast v0, LX/8Rn;

    .line 1345319
    invoke-virtual {v0}, LX/8Rn;->g()LX/8QN;

    move-result-object v0

    return-object v0
.end method

.method public static B(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)LX/8QO;
    .locals 2

    .prologue
    .line 1345316
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    sget v1, LX/8Rk;->a:I

    invoke-virtual {v0, v1}, LX/8tB;->g(I)LX/621;

    move-result-object v0

    check-cast v0, LX/8Rn;

    .line 1345317
    invoke-virtual {v0}, LX/8Rn;->h()LX/8QO;

    move-result-object v0

    return-object v0
.end method

.method public static C(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)LX/8QX;
    .locals 2

    .prologue
    .line 1345314
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    sget v1, LX/8Rk;->a:I

    invoke-virtual {v0, v1}, LX/8tB;->g(I)LX/621;

    move-result-object v0

    check-cast v0, LX/8Rn;

    .line 1345315
    invoke-virtual {v0}, LX/8Rn;->i()LX/8QX;

    move-result-object v0

    return-object v0
.end method

.method public static D(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V
    .locals 3

    .prologue
    .line 1345312
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->k:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1345313
    return-void
.end method

.method private E()LX/8Rm;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1345216
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->f:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    .line 1345217
    const/4 v0, 0x0

    .line 1345218
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Rm;

    goto :goto_0
.end method

.method private F()LX/8Q5;
    .locals 1

    .prologue
    .line 1345305
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->e:Ljava/lang/ref/WeakReference;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1345306
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Q5;

    return-object v0
.end method

.method private a(LX/8QM;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 3

    .prologue
    .line 1345297
    invoke-virtual {p1}, LX/8QM;->n()Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1345298
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-virtual {p1}, LX/8QM;->n()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->a(I)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    .line 1345299
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1345300
    invoke-static {v0}, LX/2cA;->h(LX/1oS;)Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v1

    invoke-static {v1}, LX/4YK;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/4YK;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->t()Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    move-result-object v2

    .line 1345301
    iput-object v2, v1, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1345302
    move-object v1, v1

    .line 1345303
    invoke-virtual {v1}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v1

    .line 1345304
    invoke-static {v0}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;"
        }
    .end annotation

    .prologue
    .line 1345257
    new-instance v1, LX/8QP;

    invoke-direct {v1}, LX/8QP;-><init>()V

    .line 1345258
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    if-ne p1, v0, :cond_0

    .line 1345259
    const-string v0, "{\"value\":\"SELF\"}"

    invoke-virtual {v1, v0}, LX/8QP;->c(Ljava/lang/String;)LX/8QP;

    .line 1345260
    :goto_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1345261
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1345262
    if-eqz p2, :cond_1

    .line 1345263
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1345264
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345265
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345266
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8QP;->a(Ljava/lang/String;)LX/8QP;

    goto :goto_1

    .line 1345267
    :cond_0
    const-string v0, "{\"value\":\"ALL_FRIENDS\"}"

    invoke-virtual {v1, v0}, LX/8QP;->c(Ljava/lang/String;)LX/8QP;

    goto :goto_0

    .line 1345268
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1345269
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1345270
    if-eqz p3, :cond_2

    .line 1345271
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1345272
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345273
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345274
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8QP;->b(Ljava/lang/String;)LX/8QP;

    goto :goto_2

    .line 1345275
    :cond_2
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    const-string v6, "custom"

    .line 1345276
    iput-object v6, v0, LX/2dc;->f:Ljava/lang/String;

    .line 1345277
    move-object v0, v0

    .line 1345278
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1345279
    new-instance v6, LX/4YK;

    invoke-direct {v6}, LX/4YK;-><init>()V

    .line 1345280
    iput-object p1, v6, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 1345281
    move-object v6, v6

    .line 1345282
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1345283
    iput-object v3, v6, LX/4YK;->b:LX/0Px;

    .line 1345284
    move-object v3, v6

    .line 1345285
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 1345286
    iput-object v5, v3, LX/4YK;->d:LX/0Px;

    .line 1345287
    move-object v3, v3

    .line 1345288
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->t()Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    move-result-object v5

    .line 1345289
    iput-object v5, v3, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1345290
    move-object v3, v3

    .line 1345291
    invoke-virtual {v3}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v3

    .line 1345292
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5}, LX/8Rp;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, LX/8QP;->d(Ljava/lang/String;)LX/8QP;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/8QP;

    move-result-object v0

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/8QP;->a(LX/0Px;)LX/8QP;

    move-result-object v0

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/8QP;->b(LX/0Px;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/8QP;

    .line 1345293
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    if-ne p1, v0, :cond_3

    .line 1345294
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8QP;->c(LX/0Px;)LX/8QP;

    .line 1345295
    :goto_3
    invoke-virtual {v1}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    return-object v0

    .line 1345296
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->FRIENDS_OF_TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-static {v0, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8QP;->c(LX/0Px;)LX/8QP;

    goto :goto_3
.end method

.method public static synthetic a(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;
    .locals 1

    .prologue
    .line 1345256
    invoke-static {p1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1345249
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v0

    check-cast v0, [LX/8ul;

    .line 1345250
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1345251
    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 1345252
    iget-object p0, v4, LX/8uk;->f:LX/8QK;

    move-object v4, p0

    .line 1345253
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1345254
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1345255
    :cond_0
    return-object v2
.end method

.method private a(LX/8QL;)V
    .locals 3

    .prologue
    .line 1345234
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1345235
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1345236
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1345237
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1345238
    invoke-static {v0}, LX/2cA;->h(LX/1oS;)Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v1

    invoke-static {v1}, LX/4YK;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/4YK;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->UNSPECIFIED:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1345239
    iput-object v2, v1, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1345240
    move-object v1, v1

    .line 1345241
    invoke-virtual {v1}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v1

    .line 1345242
    invoke-static {v0}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    .line 1345243
    new-instance v1, LX/8QV;

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v1, v2}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    const/4 v2, 0x0

    .line 1345244
    iput-boolean v2, v1, LX/8QV;->c:Z

    .line 1345245
    move-object v1, v1

    .line 1345246
    invoke-virtual {v1, v0}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1345247
    invoke-direct {p0, p1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b(LX/8QL;)V

    .line 1345248
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1345225
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->c:LX/8S9;

    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->c()LX/8RE;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/8tB;->a(LX/8RK;LX/8RE;)V

    .line 1345226
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    new-instance v1, LX/623;

    invoke-direct {v1}, LX/623;-><init>()V

    new-instance v2, LX/623;

    invoke-direct {v2}, LX/623;-><init>()V

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8tB;->a(Ljava/util/List;)V

    .line 1345227
    const v0, 0x7f0d04af

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    .line 1345228
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1345229
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->G:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1345230
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->I:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1345231
    const v0, 0x7f0d25f1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->v:Landroid/view/View;

    .line 1345232
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->v:Landroid/view/View;

    new-instance v1, LX/8Rc;

    invoke-direct {v1, p0}, LX/8Rc;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1345233
    return-void
.end method

.method private a(Landroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 1345219
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 1345220
    if-eqz v0, :cond_0

    .line 1345221
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a$redex0(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Landroid/text/Layout;Ljava/lang/CharSequence;)V

    .line 1345222
    :goto_0
    return-void

    .line 1345223
    :cond_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1345224
    new-instance v1, LX/8RY;

    invoke-direct {v1, p0, p1, v0}, LX/8RY;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Landroid/widget/TextView;Landroid/view/ViewTreeObserver;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/8RJ;LX/8Rp;LX/8S9;LX/8tB;Landroid/view/inputmethod/InputMethodManager;LX/0Sy;LX/03V;LX/0Or;LX/2c9;LX/0ad;)V
    .locals 0
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/privacy/gating/IsFullCustomPrivacyEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/contacts/background/AddressBookPeriodicRunner;",
            "LX/8RJ;",
            "LX/8Rp;",
            "LX/8S9;",
            "LX/8tB;",
            "Landroid/view/inputmethod/InputMethodManager;",
            "Lcom/facebook/common/userinteraction/UserInteractionController;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/2c9;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1345460
    iput-object p1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->i:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    .line 1345461
    iput-object p2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->d:LX/8RJ;

    .line 1345462
    iput-object p3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    .line 1345463
    iput-object p4, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->c:LX/8S9;

    .line 1345464
    iput-object p5, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    .line 1345465
    iput-object p6, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->k:Landroid/view/inputmethod/InputMethodManager;

    .line 1345466
    iput-object p7, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->j:LX/0Sy;

    .line 1345467
    iput-object p8, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->l:LX/03V;

    .line 1345468
    iput-object p9, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->g:LX/0Or;

    .line 1345469
    iput-object p10, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m:LX/2c9;

    .line 1345470
    iput-object p11, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->n:LX/0ad;

    .line 1345471
    return-void
.end method

.method public static a(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1345347
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-nez v0, :cond_1

    .line 1345348
    :cond_0
    :goto_0
    return-void

    .line 1345349
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1345350
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto :goto_0

    .line 1345351
    :cond_2
    invoke-static {p1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b(Ljava/util/List;)LX/8QL;

    move-result-object v0

    .line 1345352
    sget-object v1, LX/8RZ;->a:[I

    iget-object v2, v0, LX/8QL;->a:LX/8vA;

    invoke-virtual {v2}, LX/8vA;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1345353
    sget-object v1, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unexpected selected option token of type %s"

    iget-object v0, v0, LX/8QL;->a:LX/8vA;

    invoke-virtual {v0}, LX/8vA;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 1345354
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->l:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_0

    .line 1345355
    :pswitch_0
    new-instance v1, LX/8QV;

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v1, v2}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    check-cast v0, LX/8QM;

    invoke-direct {p0, v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(LX/8QM;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto :goto_0

    .line 1345356
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1345357
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    invoke-direct {p0, v1, v2, v3}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto/16 :goto_0

    .line 1345358
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1345359
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1345360
    iget-boolean p1, v3, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v3, p1

    .line 1345361
    invoke-static {v1, v2, v3}, LX/8Rp;->a(Landroid/content/res/Resources;Ljava/util/List;Z)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto/16 :goto_0

    .line 1345362
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1345363
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1345364
    iget-boolean p1, v3, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v3, p1

    .line 1345365
    invoke-static {v1, v2, v3}, LX/8Rp;->b(Landroid/content/res/Resources;Ljava/util/List;Z)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1345456
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1345457
    const/16 v1, 0x30

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 1345458
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1345459
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 12

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v11

    move-object v0, p0

    check-cast v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-static {v11}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-static {v11}, LX/8RJ;->a(LX/0QB;)LX/8RJ;

    move-result-object v2

    check-cast v2, LX/8RJ;

    invoke-static {v11}, LX/8Rp;->b(LX/0QB;)LX/8Rp;

    move-result-object v3

    check-cast v3, LX/8Rp;

    invoke-static {v11}, LX/8S9;->c(LX/0QB;)LX/8S9;

    move-result-object v4

    check-cast v4, LX/8S9;

    invoke-static {v11}, LX/8tB;->b(LX/0QB;)LX/8tB;

    move-result-object v5

    check-cast v5, LX/8tB;

    invoke-static {v11}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v6

    check-cast v6, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v11}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v7

    check-cast v7, LX/0Sy;

    invoke-static {v11}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    const/16 v9, 0x34e

    invoke-static {v11, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v11}, LX/2c9;->b(LX/0QB;)LX/2c9;

    move-result-object v10

    check-cast v10, LX/2c9;

    invoke-static {v11}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-direct/range {v0 .. v11}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/8RJ;LX/8Rp;LX/8S9;LX/8tB;Landroid/view/inputmethod/InputMethodManager;LX/0Sy;LX/03V;LX/0Or;LX/2c9;LX/0ad;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;LX/8QL;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1345437
    if-nez p1, :cond_1

    .line 1345438
    :cond_0
    :goto_0
    return-void

    .line 1345439
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {p0, p1, v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(LX/8QL;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    .line 1345440
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d()V

    .line 1345441
    instance-of v0, p1, LX/8QO;

    if-nez v0, :cond_0

    instance-of v0, p1, LX/8QX;

    if-nez v0, :cond_0

    instance-of v0, p1, LX/8QN;

    if-nez v0, :cond_0

    .line 1345442
    const/4 v0, -0x1

    .line 1345443
    instance-of v1, p2, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;

    if-eqz v1, :cond_4

    .line 1345444
    const v0, 0x7f0d0beb

    .line 1345445
    :cond_2
    :goto_1
    if-lez v0, :cond_3

    .line 1345446
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1345447
    if-eqz v0, :cond_3

    .line 1345448
    invoke-direct {p0, v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Landroid/widget/TextView;)V

    .line 1345449
    :cond_3
    iget-object v0, p1, LX/8QL;->a:LX/8vA;

    sget-object v1, LX/8vA;->PRIVACY:LX/8vA;

    if-ne v0, v1, :cond_0

    .line 1345450
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "option_type"

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1345451
    iget-object v3, v2, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v2, v3

    .line 1345452
    invoke-static {v2}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 1345453
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m:LX/2c9;

    const-string v2, "basic_option"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    goto :goto_0

    .line 1345454
    :cond_4
    instance-of v1, p2, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;

    if-eqz v1, :cond_2

    .line 1345455
    const v0, 0x7f0d0e19

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Landroid/text/Layout;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1345432
    if-eqz p1, :cond_0

    .line 1345433
    invoke-virtual {p1}, Landroid/text/Layout;->getLineCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1345434
    if-ltz v0, :cond_0

    invoke-virtual {p1, v0}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v0

    if-lez v0, :cond_0

    .line 1345435
    invoke-direct {p0, p2}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Ljava/lang/CharSequence;)V

    .line 1345436
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1345428
    iput-object p1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    .line 1345429
    iput-object p2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    .line 1345430
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    invoke-direct {p0, v1, v2, v3}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1345431
    return-void
.end method

.method private static b(Ljava/util/List;)LX/8QL;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;)",
            "LX/8QL;"
        }
    .end annotation

    .prologue
    .line 1345424
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1345425
    iget-object v1, v0, LX/8QL;->a:LX/8vA;

    sget-object v2, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-ne v1, v2, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1345426
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1345427
    :cond_0
    return-object v0
.end method

.method private b(LX/8QL;)V
    .locals 2

    .prologue
    .line 1345417
    iget-object v0, p1, LX/8QL;->a:LX/8vA;

    sget-object v1, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Treating non tag expansion token as a tag expansion token"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1345418
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setSelection(I)V

    .line 1345419
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1345420
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 1345421
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c()V

    .line 1345422
    return-void

    .line 1345423
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1345400
    const v0, 0x7f0d260c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1345401
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->H:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1345402
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    sget-object v1, LX/8ut;->NO_DROPDOWN:LX/8ut;

    .line 1345403
    iput-object v1, v0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->f:LX/8ut;

    .line 1345404
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    sget-object v1, LX/8uy;->PLAIN_TEXT:LX/8uy;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setTextMode(LX/8uy;)V

    .line 1345405
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1345406
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1345407
    iput v0, v1, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->p:I

    .line 1345408
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setTokenIconColor(I)V

    .line 1345409
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setLongClickable(Z)V

    .line 1345410
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v1, LX/8Rd;

    invoke-direct {v1, p0}, LX/8Rd;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1345411
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->F:LX/8SB;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/8SB;->a(Landroid/view/View;Landroid/content/Context;)V

    .line 1345412
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->n:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-short v2, LX/8Po;->c:S

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    .line 1345413
    if-eqz v0, :cond_0

    .line 1345414
    const v0, 0x7f0d260b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1345415
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1345416
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 7

    .prologue
    .line 1345388
    iget-object v1, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1345389
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v2, v0

    .line 1345390
    new-instance v3, LX/8Rh;

    invoke-direct {v3, p0, p1}, LX/8Rh;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1345391
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1345392
    invoke-direct {p0, p1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->e(Lcom/facebook/privacy/model/SelectablePrivacyData;)Ljava/util/List;

    move-result-object v6

    .line 1345393
    invoke-direct {p0, p1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->f(Lcom/facebook/privacy/model/SelectablePrivacyData;)Ljava/util/List;

    move-result-object v5

    .line 1345394
    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual/range {v0 .. v6}, LX/8Rp;->a(Lcom/facebook/privacy/model/PrivacyOptionsResult;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/0Or;Landroid/content/res/Resources;Ljava/util/List;Ljava/util/List;)LX/622;

    move-result-object v0

    .line 1345395
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    sget v2, LX/8Rk;->a:I

    invoke-virtual {v1, v2, v0}, LX/8tB;->a(ILX/621;)V

    .line 1345396
    invoke-virtual {v0}, LX/622;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->D:I

    .line 1345397
    return-void

    .line 1345398
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->c(Lcom/facebook/privacy/model/SelectablePrivacyData;)Ljava/util/List;

    move-result-object v5

    .line 1345399
    invoke-direct {p0, p1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->d(Lcom/facebook/privacy/model/SelectablePrivacyData;)Ljava/util/List;

    move-result-object v6

    goto :goto_0
.end method

.method private c()LX/8RE;
    .locals 2

    .prologue
    .line 1345384
    new-instance v0, LX/8Rl;

    invoke-direct {v0}, LX/8Rl;-><init>()V

    .line 1345385
    new-instance v1, LX/8Rg;

    invoke-direct {v1, p0}, LX/8Rg;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1345386
    iput-object v1, v0, LX/8Rl;->a:LX/8Rf;

    .line 1345387
    return-object v0
.end method

.method private c(Lcom/facebook/privacy/model/SelectablePrivacyData;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/privacy/model/SelectablePrivacyData;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1345367
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1345368
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    .line 1345369
    :goto_0
    return-object v0

    .line 1345370
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_1

    .line 1345371
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1345372
    if-nez v0, :cond_2

    .line 1345373
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1345374
    goto :goto_0

    .line 1345375
    :cond_2
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1345376
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {v1, v0}, LX/8Rp;->c(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1345377
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1345378
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1345379
    :cond_3
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1345380
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {v1, v0}, LX/8Rp;->c(LX/1oS;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1345381
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1345382
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1345383
    goto :goto_0
.end method

.method private static c(LX/8QL;)Z
    .locals 2

    .prologue
    .line 1345366
    iget-object v0, p0, LX/8QL;->a:LX/8vA;

    sget-object v1, LX/8vA;->PRIVACY:LX/8vA;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/8QL;->a:LX/8vA;

    sget-object v1, LX/8vA;->FULL_CUSTOM:LX/8vA;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/8QL;->a:LX/8vA;

    sget-object v1, LX/8vA;->FRIENDS_EXCEPT:LX/8vA;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/8QL;->a:LX/8vA;

    sget-object v1, LX/8vA;->SPECIFIC_FRIENDS:LX/8vA;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c$redex0(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1345307
    iput-object p1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    .line 1345308
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1345309
    iget-boolean p1, v3, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v3, p1

    .line 1345310
    invoke-static {v1, v2, v3}, LX/8Rp;->a(Landroid/content/res/Resources;Ljava/util/List;Z)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1345311
    return-void
.end method

.method private d(Lcom/facebook/privacy/model/SelectablePrivacyData;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/privacy/model/SelectablePrivacyData;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1345328
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1345329
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    .line 1345330
    :goto_0
    return-object v0

    .line 1345331
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_1

    .line 1345332
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1345333
    if-nez v0, :cond_2

    .line 1345334
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1345335
    goto :goto_0

    .line 1345336
    :cond_2
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1345337
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {v1, v0}, LX/8Rp;->d(LX/1oS;)Z

    move-result v0

    .line 1345338
    if-eqz v0, :cond_3

    .line 1345339
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1345340
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1345341
    :cond_3
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1345342
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {v1, v0}, LX/8Rp;->d(LX/1oS;)Z

    move-result v1

    .line 1345343
    if-eqz v1, :cond_4

    .line 1345344
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1345345
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1345346
    goto :goto_0
.end method

.method public static d(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V
    .locals 3

    .prologue
    .line 1344813
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1344814
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344815
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1344816
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1344817
    invoke-static {v0}, LX/2cA;->h(LX/1oS;)Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v1

    invoke-static {v1}, LX/4YK;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/4YK;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1344818
    iput-object v2, v1, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1344819
    move-object v1, v1

    .line 1344820
    invoke-virtual {v1}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v1

    .line 1344821
    invoke-static {v0}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    .line 1344822
    new-instance v1, LX/8QV;

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v1, v2}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    const/4 v2, 0x1

    .line 1344823
    iput-boolean v2, v1, LX/8QV;->c:Z

    .line 1344824
    move-object v1, v1

    .line 1344825
    invoke-virtual {v1, v0}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344826
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->E()LX/8Rm;

    .line 1344827
    return-void
.end method

.method public static d$redex0(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1345025
    iput-object p1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    .line 1345026
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1345027
    iget-boolean p1, v3, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v3, p1

    .line 1345028
    invoke-static {v1, v2, v3}, LX/8Rp;->b(Landroid/content/res/Resources;Ljava/util/List;Z)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1345029
    return-void
.end method

.method private e(Lcom/facebook/privacy/model/SelectablePrivacyData;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/privacy/model/SelectablePrivacyData;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1345006
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1345007
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    .line 1345008
    :goto_0
    return-object v0

    .line 1345009
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_1

    .line 1345010
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1345011
    if-nez v0, :cond_2

    .line 1345012
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1345013
    goto :goto_0

    .line 1345014
    :cond_2
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1345015
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {v1, v0}, LX/8Rp;->b(LX/1oS;)Z

    move-result v0

    .line 1345016
    if-eqz v0, :cond_3

    .line 1345017
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1345018
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1345019
    :cond_3
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1345020
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {v1, v0}, LX/8Rp;->b(LX/1oS;)Z

    move-result v1

    .line 1345021
    if-eqz v1, :cond_4

    .line 1345022
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1345023
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1345024
    goto :goto_0
.end method

.method private f(Lcom/facebook/privacy/model/SelectablePrivacyData;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/privacy/model/SelectablePrivacyData;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1344987
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1344988
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    .line 1344989
    :goto_0
    return-object v0

    .line 1344990
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_1

    .line 1344991
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1344992
    if-nez v0, :cond_2

    .line 1344993
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1344994
    goto :goto_0

    .line 1344995
    :cond_2
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1344996
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {v1, v0}, LX/8Rp;->b(LX/1oS;)Z

    move-result v0

    .line 1344997
    if-eqz v0, :cond_3

    .line 1344998
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1344999
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1345000
    :cond_3
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1345001
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {v1, v0}, LX/8Rp;->b(LX/1oS;)Z

    move-result v1

    .line 1345002
    if-eqz v1, :cond_4

    .line 1345003
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1345004
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1345005
    goto :goto_0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 1344956
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->F()LX/8Q5;

    move-result-object v0

    invoke-interface {v0}, LX/8Q5;->a()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    .line 1344957
    if-nez v0, :cond_0

    .line 1344958
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->E:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344959
    :cond_0
    invoke-static {p0, v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1344960
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v1, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-boolean v1, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    iget-boolean v2, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344961
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1344962
    iget-object v2, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v2, v2

    .line 1344963
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v1, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v1, v1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v2, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v2, v2, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1344964
    :goto_0
    return-void

    .line 1344965
    :cond_1
    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344966
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344967
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1344968
    invoke-virtual {v0, v1}, LX/8Rp;->c(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1344969
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344970
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1344971
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->c$redex0(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Ljava/util/List;)V

    .line 1344972
    :cond_2
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344973
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1344974
    invoke-virtual {v0, v1}, LX/8Rp;->d(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1344975
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344976
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1344977
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->d$redex0(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Ljava/util/List;)V

    .line 1344978
    :cond_3
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344979
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1344980
    invoke-virtual {v0, v1}, LX/8Rp;->b(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1344981
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344982
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1344983
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344984
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1344985
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a$redex0(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Ljava/util/List;Ljava/util/List;)V

    .line 1344986
    :cond_4
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->l()V

    goto :goto_0
.end method

.method private l()V
    .locals 4

    .prologue
    .line 1344925
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-nez v0, :cond_1

    .line 1344926
    :cond_0
    :goto_0
    return-void

    .line 1344927
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344928
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v1

    .line 1344929
    if-eqz v1, :cond_0

    .line 1344930
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0}, Lcom/facebook/privacy/model/SelectablePrivacyData;->c()I

    move-result v2

    .line 1344931
    const/4 v0, -0x1

    if-ne v2, v0, :cond_2

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344932
    iget-object v3, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v3

    .line 1344933
    invoke-static {v0}, LX/2cA;->c(LX/1oS;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344934
    iget-object v3, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v3

    .line 1344935
    invoke-static {v0}, LX/2cA;->e(LX/1oS;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344936
    iget-object v3, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v3

    .line 1344937
    invoke-static {v0}, LX/2cA;->a(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1344938
    :cond_2
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->b()V

    .line 1344939
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {v0, v1}, LX/8Rp;->b(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1344940
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)LX/8QN;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 1344941
    :goto_1
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    .line 1344942
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1344943
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1344944
    invoke-static {v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->c(LX/8QL;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1344945
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->n()V

    .line 1344946
    :cond_3
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->s(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    goto :goto_0

    .line 1344947
    :cond_4
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {v0, v1}, LX/8Rp;->c(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1344948
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)LX/8QO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    goto :goto_1

    .line 1344949
    :cond_5
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {v0, v1}, LX/8Rp;->d(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1344950
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)LX/8QX;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    goto :goto_1

    .line 1344951
    :cond_6
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    sget v3, LX/8Rk;->a:I

    invoke-virtual {v0, v3}, LX/8tB;->g(I)LX/621;

    move-result-object v0

    check-cast v0, LX/8Rn;

    .line 1344952
    invoke-virtual {v0, v2}, LX/8Rn;->a(I)LX/8QM;

    move-result-object v0

    .line 1344953
    if-nez v0, :cond_7

    .line 1344954
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {v0, v1, v2}, LX/8Rp;->a(LX/1oT;I)LX/8QM;

    move-result-object v0

    .line 1344955
    :cond_7
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    goto :goto_1
.end method

.method public static m(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V
    .locals 3

    .prologue
    .line 1344920
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->o(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)LX/8QY;

    move-result-object v0

    .line 1344921
    if-eqz v0, :cond_0

    .line 1344922
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1344923
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;Z)V

    .line 1344924
    :cond_0
    return-void
.end method

.method private n()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1344888
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-nez v0, :cond_1

    .line 1344889
    :cond_0
    :goto_0
    return-void

    .line 1344890
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 1344891
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    .line 1344892
    :cond_2
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1344893
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1344894
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1344895
    invoke-static {v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->c(LX/8QL;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1344896
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    sget v4, LX/8Rk;->a:I

    invoke-virtual {v1, v4}, LX/8tB;->g(I)LX/621;

    move-result-object v1

    check-cast v1, LX/8Rn;

    .line 1344897
    invoke-virtual {v1}, LX/8Rn;->j()V

    .line 1344898
    iget-object v4, v0, LX/8QL;->a:LX/8vA;

    sget-object v5, LX/8vA;->FULL_CUSTOM:LX/8vA;

    if-ne v4, v5, :cond_5

    .line 1344899
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)LX/8QN;

    move-result-object v0

    .line 1344900
    :goto_1
    iget-boolean v4, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->w:Z

    if-eqz v4, :cond_0

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v4}, Lcom/facebook/privacy/model/SelectablePrivacyData;->f()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1344901
    iget-object v4, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344902
    iget-object v5, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v4, v5

    .line 1344903
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l()LX/0Px;

    move-result-object v4

    .line 1344904
    if-eqz v4, :cond_8

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v2, :cond_8

    .line 1344905
    :goto_2
    iget-object v4, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    iget-object v5, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v5}, Lcom/facebook/privacy/model/SelectablePrivacyData;->g()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v5

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v4, v5, v6, v3, v2}, LX/8Rp;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;Landroid/content/res/Resources;ZZ)LX/8QY;

    move-result-object v3

    .line 1344906
    iget-object v4, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v4}, Lcom/facebook/widget/listview/BetterListView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_3

    .line 1344907
    invoke-virtual {v1}, LX/622;->e()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1344908
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v3, v0}, LX/8Rn;->a(LX/8QY;I)V

    .line 1344909
    :cond_3
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    const v1, 0x590fff0d

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1344910
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344911
    iget-boolean v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v0, v1

    .line 1344912
    if-eqz v0, :cond_4

    if-nez v2, :cond_0

    .line 1344913
    :cond_4
    invoke-direct {p0, v3}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b(LX/8QL;)V

    goto/16 :goto_0

    .line 1344914
    :cond_5
    iget-object v4, v0, LX/8QL;->a:LX/8vA;

    sget-object v5, LX/8vA;->FRIENDS_EXCEPT:LX/8vA;

    if-ne v4, v5, :cond_6

    .line 1344915
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)LX/8QO;

    move-result-object v0

    goto :goto_1

    .line 1344916
    :cond_6
    iget-object v0, v0, LX/8QL;->a:LX/8vA;

    sget-object v4, LX/8vA;->SPECIFIC_FRIENDS:LX/8vA;

    if-ne v0, v4, :cond_7

    .line 1344917
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)LX/8QX;

    move-result-object v0

    goto :goto_1

    .line 1344918
    :cond_7
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0}, Lcom/facebook/privacy/model/SelectablePrivacyData;->c()I

    move-result v0

    invoke-virtual {v1, v0}, LX/8Rn;->a(I)LX/8QM;

    move-result-object v0

    goto :goto_1

    :cond_8
    move v2, v3

    .line 1344919
    goto :goto_2
.end method

.method public static o(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)LX/8QY;
    .locals 4

    .prologue
    .line 1344884
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1344885
    iget-object v2, v0, LX/8QL;->a:LX/8vA;

    sget-object v3, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-ne v2, v3, :cond_0

    .line 1344886
    check-cast v0, LX/8QY;

    .line 1344887
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()Z
    .locals 1

    .prologue
    .line 1344881
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344882
    iget-object p0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, p0

    .line 1344883
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1344861
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    .line 1344862
    :cond_0
    :goto_0
    return-void

    .line 1344863
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1344864
    :cond_2
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1344865
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0xe198c7c

    if-ne v2, v3, :cond_0

    .line 1344866
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/8QR;->SOME_FRIENDS:LX/8QR;

    invoke-virtual {v3}, LX/8QR;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1344867
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v3, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1344868
    iget-object v4, v3, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    :goto_1
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1344869
    invoke-static {v0}, LX/2cA;->a(LX/1oR;)Lcom/facebook/privacy/model/PrivacyParameter;

    move-result-object v6

    .line 1344870
    if-eqz v6, :cond_3

    iget-object v6, v6, Lcom/facebook/privacy/model/PrivacyParameter;->allow:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1344871
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {v3, v0}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, LX/8Rp;->a(LX/1oT;I)LX/8QM;

    move-result-object v0

    .line 1344872
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->b()V

    .line 1344873
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 1344874
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1344875
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1344876
    invoke-static {p0, v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Ljava/util/List;)V

    .line 1344877
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->n()V

    .line 1344878
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->e()V

    .line 1344879
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-static {p0, v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    goto/16 :goto_0

    .line 1344880
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method private r()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1344844
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 1344845
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1344846
    :cond_0
    :goto_0
    return-void

    .line 1344847
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->o:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1344848
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/8Ri;

    invoke-direct {v1, p0}, LX/8Ri;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1344849
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->k()V

    .line 1344850
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1344851
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->n()V

    .line 1344852
    :cond_2
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->l()V

    .line 1344853
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setSelection(I)V

    .line 1344854
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->E()LX/8Rm;

    move-result-object v0

    .line 1344855
    if-eqz v0, :cond_3

    .line 1344856
    invoke-interface {v0}, LX/8Rm;->a()V

    .line 1344857
    :cond_3
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->D(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1344858
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m:LX/2c9;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344859
    iget-object p0, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, p0

    .line 1344860
    invoke-virtual {v0, v1, v2}, LX/2c9;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;Z)V

    goto :goto_0
.end method

.method public static s(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V
    .locals 2

    .prologue
    .line 1344831
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    if-eqz v0, :cond_0

    .line 1344832
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    .line 1344833
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1344834
    :cond_1
    :goto_0
    return-void

    .line 1344835
    :cond_2
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->F()LX/8Q5;

    move-result-object v0

    invoke-interface {v0}, LX/8Q5;->b()Z

    move-result v0

    .line 1344836
    iget-boolean v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->w:Z

    if-eq v0, v1, :cond_1

    .line 1344837
    iput-boolean v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->w:Z

    .line 1344838
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->n()V

    .line 1344839
    iget-boolean v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->w:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_1

    .line 1344840
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    const/4 v1, 0x1

    .line 1344841
    iput-boolean v1, v0, LX/8QV;->c:Z

    .line 1344842
    move-object v0, v0

    .line 1344843
    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto :goto_0
.end method

.method private t()Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;
    .locals 1

    .prologue
    .line 1344828
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1344829
    iget-boolean p0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v0, p0

    .line 1344830
    invoke-static {v0}, LX/8Rp;->a(Z)Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    move-result-object v0

    return-object v0
.end method

.method private u()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1344802
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    if-nez v0, :cond_1

    .line 1344803
    :cond_0
    :goto_0
    return-void

    .line 1344804
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    sget v1, LX/8Rk;->a:I

    invoke-virtual {v0, v1}, LX/8tB;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    .line 1344805
    instance-of v1, v0, LX/622;

    if-eqz v1, :cond_2

    invoke-interface {v0}, LX/621;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    .line 1344806
    :goto_1
    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v4, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->D:I

    if-ne v0, v4, :cond_3

    .line 1344807
    :goto_2
    if-nez v1, :cond_0

    if-nez v2, :cond_0

    .line 1344808
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->E()LX/8Rm;

    move-result-object v0

    .line 1344809
    if-eqz v0, :cond_0

    .line 1344810
    invoke-interface {v0}, LX/8Rm;->b()V

    goto :goto_0

    :cond_2
    move v1, v3

    .line 1344811
    goto :goto_1

    :cond_3
    move v2, v3

    .line 1344812
    goto :goto_2
.end method

.method private v()V
    .locals 4

    .prologue
    .line 1345030
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->F:LX/8SB;

    invoke-virtual {v0}, LX/8SB;->b()V

    .line 1345031
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    .line 1345032
    const v1, 0x7f0d2608

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1345033
    new-instance v1, Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    invoke-direct {v1}, Lcom/facebook/privacy/selector/CustomPrivacyFragment;-><init>()V

    iput-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->x:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    .line 1345034
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1345035
    const v2, 0x7f0d2608

    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->x:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    invoke-virtual {v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1345036
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1345037
    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1345038
    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->x:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    new-instance v1, LX/8Rj;

    invoke-direct {v1, p0}, LX/8Rj;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1345039
    iput-object v1, v0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->t:LX/8Qe;

    .line 1345040
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->x:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    new-instance v1, LX/8RN;

    invoke-direct {v1, p0}, LX/8RN;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1345041
    iput-object v1, v0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->u:LX/0RV;

    .line 1345042
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->x:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    new-instance v1, LX/8RO;

    invoke-direct {v1, p0}, LX/8RO;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1345043
    iput-object v1, v0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->w:LX/8Qe;

    .line 1345044
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->x:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    new-instance v1, LX/8RP;

    invoke-direct {v1, p0}, LX/8RP;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1345045
    iput-object v1, v0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->x:LX/0RV;

    .line 1345046
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->x:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    new-instance v1, LX/8RQ;

    invoke-direct {v1, p0}, LX/8RQ;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1345047
    iput-object v1, v0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->y:LX/0RV;

    .line 1345048
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->x:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    invoke-virtual {v0}, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->b()V

    .line 1345049
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->o:Landroid/view/View;

    const v1, 0x7f0d2608

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->r:Landroid/view/View;

    .line 1345050
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->r:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1345051
    return-void

    .line 1345052
    :cond_0
    const v1, 0x7f0d2608

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->x:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    goto :goto_0
.end method

.method private w()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1345053
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m:LX/2c9;

    const-string v1, "friends_except"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v4, v2}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    .line 1345054
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->F:LX/8SB;

    invoke-virtual {v0}, LX/8SB;->b()V

    .line 1345055
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    .line 1345056
    const v1, 0x7f0d2609

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1345057
    invoke-static {v4}, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->a(Z)Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->y:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    .line 1345058
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1345059
    const v2, 0x7f0d2609

    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->y:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    invoke-virtual {v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1345060
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1345061
    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1345062
    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->y:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    new-instance v1, LX/8RR;

    invoke-direct {v1, p0}, LX/8RR;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a(LX/8Qe;)V

    .line 1345063
    new-instance v0, LX/8RS;

    invoke-direct {v0, p0}, LX/8RS;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1345064
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->y:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    .line 1345065
    iput-object v0, v1, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->w:LX/0RV;

    .line 1345066
    new-instance v0, LX/8RT;

    invoke-direct {v0, p0}, LX/8RT;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1345067
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->y:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    .line 1345068
    iput-object v0, v1, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->x:LX/0RV;

    .line 1345069
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->y:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    invoke-virtual {v0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->c()V

    .line 1345070
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->o:Landroid/view/View;

    const v1, 0x7f0d2609

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->s:Landroid/view/View;

    .line 1345071
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1345072
    return-void

    .line 1345073
    :cond_0
    const v1, 0x7f0d2609

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->y:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    goto :goto_0
.end method

.method private x()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1345074
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m:LX/2c9;

    const-string v1, "specific_friends"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v4, v2}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    .line 1345075
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->F:LX/8SB;

    invoke-virtual {v0}, LX/8SB;->b()V

    .line 1345076
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    .line 1345077
    const v1, 0x7f0d260a

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1345078
    new-instance v1, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    invoke-direct {v1}, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;-><init>()V

    iput-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->z:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    .line 1345079
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1345080
    const v2, 0x7f0d260a

    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->z:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    invoke-virtual {v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1345081
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1345082
    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1345083
    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->z:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    new-instance v1, LX/8RU;

    invoke-direct {v1, p0}, LX/8RU;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a(LX/8Qe;)V

    .line 1345084
    new-instance v0, LX/8RV;

    invoke-direct {v0, p0}, LX/8RV;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1345085
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->z:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    .line 1345086
    iput-object v0, v1, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->w:LX/0RV;

    .line 1345087
    new-instance v0, LX/8RW;

    invoke-direct {v0, p0}, LX/8RW;-><init>(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1345088
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->z:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    .line 1345089
    iput-object v0, v1, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->x:LX/0RV;

    .line 1345090
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->z:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    invoke-virtual {v0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->c()V

    .line 1345091
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->o:Landroid/view/View;

    const v1, 0x7f0d260a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->t:Landroid/view/View;

    .line 1345092
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->t:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1345093
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m:LX/2c9;

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_1
    invoke-virtual {v1, v0}, LX/2c9;->a(I)V

    .line 1345094
    return-void

    .line 1345095
    :cond_0
    const v1, 0x7f0d260a

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->z:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    goto :goto_0

    .line 1345096
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private y()LX/8QM;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1345097
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1345098
    :goto_0
    return-object v0

    .line 1345099
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v3, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1345100
    iget-object v4, v3, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_3

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1345101
    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v6, v7, :cond_2

    .line 1345102
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {v3, v0}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, LX/8Rp;->a(LX/1oT;I)LX/8QM;

    move-result-object v0

    goto :goto_0

    .line 1345103
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 1345104
    goto :goto_0
.end method

.method private z()LX/8QM;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1345105
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1345106
    :goto_0
    return-object v0

    .line 1345107
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v3, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1345108
    iget-object v4, v3, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_3

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1345109
    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->ONLY_ME:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v6, v7, :cond_2

    .line 1345110
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b:LX/8Rp;

    invoke-virtual {v3, v0}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, LX/8Rp;->a(LX/1oT;I)LX/8QM;

    move-result-object v0

    goto :goto_0

    .line 1345111
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 1345112
    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1345113
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->r:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->r:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1345114
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->x:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    invoke-virtual {v0}, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1345115
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q()V

    .line 1345116
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->r:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1345117
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->D(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1345118
    :cond_0
    :goto_0
    return v1

    .line 1345119
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->s:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->s:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 1345120
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1345121
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->D(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1345122
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "numSelected"

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v2, v3, v0}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v0

    .line 1345123
    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m:LX/2c9;

    const-string v3, "friends_except_selected"

    invoke-virtual {v2, v3, v1, v0}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    goto :goto_0

    .line 1345124
    :cond_2
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_1

    .line 1345125
    :cond_3
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->t:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    .line 1345126
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q()V

    .line 1345127
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1345128
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->D(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1345129
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m:LX/2c9;

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, LX/2c9;->b(I)V

    .line 1345130
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "numSelected"

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {v2, v3, v0}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v0

    .line 1345131
    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m:LX/2c9;

    const-string v3, "specific_friends_selected"

    invoke-virtual {v2, v3, v1, v0}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    goto :goto_0

    .line 1345132
    :cond_4
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_2

    .line 1345133
    :cond_5
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->F:LX/8SB;

    invoke-virtual {v0}, LX/8SB;->c()V

    .line 1345134
    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public final a(LX/8Q5;)V
    .locals 2

    .prologue
    .line 1345135
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->e:Ljava/lang/ref/WeakReference;

    .line 1345136
    return-void
.end method

.method public final a(LX/8QL;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x1

    const/16 v5, 0x8

    const/4 v1, 0x0

    .line 1345137
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->u()V

    .line 1345138
    invoke-static {p2}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v2

    .line 1345139
    iget-object v3, p1, LX/8QL;->a:LX/8vA;

    sget-object v4, LX/8vA;->FULL_CUSTOM:LX/8vA;

    if-ne v3, v4, :cond_6

    .line 1345140
    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    invoke-static {v3}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->y()LX/8QM;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1345141
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->y()LX/8QM;

    move-result-object p1

    .line 1345142
    :cond_0
    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->r:Landroid/view/View;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->r:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-ne v3, v5, :cond_2

    .line 1345143
    :cond_1
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->v()V

    .line 1345144
    :cond_2
    :goto_0
    iget-object v3, p1, LX/8QL;->a:LX/8vA;

    sget-object v4, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-eq v3, v4, :cond_3

    iget-object v3, p1, LX/8QL;->a:LX/8vA;

    sget-object v4, LX/8vA;->PRIVACY:LX/8vA;

    if-eq v3, v4, :cond_4

    :cond_3
    iget-object v3, p1, LX/8QL;->a:LX/8vA;

    sget-object v4, LX/8vA;->FULL_CUSTOM:LX/8vA;

    if-eq v3, v4, :cond_4

    iget-object v3, p1, LX/8QL;->a:LX/8vA;

    sget-object v4, LX/8vA;->FRIENDS_EXCEPT:LX/8vA;

    if-eq v3, v4, :cond_4

    iget-object v3, p1, LX/8QL;->a:LX/8vA;

    sget-object v4, LX/8vA;->SPECIFIC_FRIENDS:LX/8vA;

    if-ne v3, v4, :cond_5

    .line 1345145
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->b()V

    .line 1345146
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1345147
    :cond_5
    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 1345148
    invoke-virtual {p2, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 1345149
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 1345150
    :goto_1
    invoke-static {p0, v2}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;Ljava/util/List;)V

    .line 1345151
    iget-object v2, p1, LX/8QL;->a:LX/8vA;

    sget-object v3, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-ne v2, v3, :cond_e

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1345152
    iget-object v3, v2, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v2, v3

    .line 1345153
    if-eqz v2, :cond_e

    .line 1345154
    if-eqz v0, :cond_d

    .line 1345155
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m:LX/2c9;

    const-string v2, "disable_tag_expansion"

    invoke-virtual {v0, v2, v1, v6}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    .line 1345156
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->d(Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;)V

    .line 1345157
    :goto_2
    invoke-virtual {p2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->e()V

    .line 1345158
    return-void

    .line 1345159
    :cond_6
    iget-object v3, p1, LX/8QL;->a:LX/8vA;

    sget-object v4, LX/8vA;->FRIENDS_EXCEPT:LX/8vA;

    if-ne v3, v4, :cond_9

    .line 1345160
    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->A:Ljava/util/List;

    invoke-static {v3}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->y()LX/8QM;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 1345161
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->y()LX/8QM;

    move-result-object p1

    .line 1345162
    :cond_7
    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->s:Landroid/view/View;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->s:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-ne v3, v5, :cond_2

    .line 1345163
    :cond_8
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->w()V

    goto :goto_0

    .line 1345164
    :cond_9
    iget-object v3, p1, LX/8QL;->a:LX/8vA;

    sget-object v4, LX/8vA;->SPECIFIC_FRIENDS:LX/8vA;

    if-ne v3, v4, :cond_2

    .line 1345165
    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->B:Ljava/util/List;

    invoke-static {v3}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->z()LX/8QM;

    move-result-object v3

    if-eqz v3, :cond_a

    .line 1345166
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->z()LX/8QM;

    move-result-object p1

    .line 1345167
    :cond_a
    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->t:Landroid/view/View;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->t:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-ne v3, v5, :cond_2

    .line 1345168
    :cond_b
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->x()V

    goto/16 :goto_0

    .line 1345169
    :cond_c
    invoke-virtual {p2, p1, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;Z)V

    .line 1345170
    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1345171
    :cond_d
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m:LX/2c9;

    const-string v2, "enable_tag_expansion"

    invoke-virtual {v0, v2, v1, v6}, LX/2c9;->a(Ljava/lang/String;ZLX/1rQ;)V

    .line 1345172
    invoke-direct {p0, p1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(LX/8QL;)V

    goto :goto_2

    .line 1345173
    :cond_e
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->n()V

    goto :goto_2
.end method

.method public final a(LX/8Rm;)V
    .locals 2

    .prologue
    .line 1345174
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->f:Ljava/lang/ref/WeakReference;

    .line 1345175
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1345176
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1345177
    const-class v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-static {v0, p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1345178
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->i:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-virtual {v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a()V

    .line 1345179
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1345180
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->F:LX/8SB;

    invoke-virtual {v0, p1}, LX/8SB;->a(Landroid/view/View$OnClickListener;)V

    .line 1345181
    return-void
.end method

.method public final a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 2

    .prologue
    .line 1345182
    new-instance v0, LX/8QV;

    invoke-direct {v0, p1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1345183
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    .line 1345184
    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->E:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1345185
    return-void
.end method

.method public final b()Lcom/facebook/privacy/model/SelectablePrivacyData;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1345186
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1345187
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->k()V

    :cond_0
    move v1, v2

    .line 1345188
    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    invoke-virtual {v0}, LX/8tB;->c()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1345189
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    invoke-virtual {v0, v1}, LX/8tB;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    .line 1345190
    invoke-interface {v0, v2}, LX/621;->a(Z)V

    .line 1345191
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1345192
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    const v1, -0x6fa20034

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1345193
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a()V

    .line 1345194
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->clearComposingText()V

    .line 1345195
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->e()V

    .line 1345196
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->C:Ljava/util/List;

    .line 1345197
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->m:LX/2c9;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1345198
    iget-object v3, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v3

    .line 1345199
    invoke-virtual {v0, v1, v2}, LX/2c9;->b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;Z)V

    .line 1345200
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->h:Lcom/facebook/privacy/model/SelectablePrivacyData;

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6e4ce06d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1345201
    const v1, 0x7f031012

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->o:Landroid/view/View;

    .line 1345202
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->o:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(Landroid/view/View;)V

    .line 1345203
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->o:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b(Landroid/view/View;)V

    .line 1345204
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->o:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x2a35bbfe

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x3d90a62a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1345208
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1345209
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1345210
    iput-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->u:Lcom/facebook/widget/listview/BetterListView;

    .line 1345211
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->H:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1345212
    iput-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1345213
    iput-object v3, p0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->q:LX/8tB;

    .line 1345214
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1345215
    const/16 v1, 0x2b

    const v2, 0x70cb4967

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x29dff909

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1345205
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1345206
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->r()V

    .line 1345207
    const/16 v1, 0x2b

    const v2, -0x5d0cf127

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
