.class public Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:LX/8Sa;

.field public c:Lcom/facebook/privacy/model/AudiencePickerModel;

.field private d:Landroid/view/View;

.field private e:Lcom/facebook/widget/listview/BetterListView;

.field public f:LX/8Qx;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1343884
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1343885
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1343894
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1343895
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;

    const-class p1, Landroid/content/Context;

    invoke-interface {v0, p1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-static {v0}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v0

    check-cast v0, LX/8Sa;

    iput-object p1, p0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->a:Landroid/content/Context;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->b:LX/8Sa;

    .line 1343896
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x3003045e

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1343886
    const v0, 0x7f03013d

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->d:Landroid/view/View;

    .line 1343887
    new-instance v0, LX/8Qx;

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->b:LX/8Sa;

    iget-object v4, p0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->c:Lcom/facebook/privacy/model/AudiencePickerModel;

    invoke-direct {v0, v2, v3, v4}, LX/8Qx;-><init>(Landroid/content/Context;LX/8Sa;Lcom/facebook/privacy/model/AudiencePickerModel;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->f:LX/8Qx;

    .line 1343888
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->d:Landroid/view/View;

    const v2, 0x7f0d0604

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    .line 1343889
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->f:LX/8Qx;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1343890
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->e:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->f:LX/8Qx;

    .line 1343891
    new-instance v3, LX/8Qw;

    invoke-direct {v3, v2}, LX/8Qw;-><init>(LX/8Qx;)V

    move-object v2, v3

    .line 1343892
    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1343893
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerAllListsFragment;->d:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, 0x47691a39

    invoke-static {v5, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method
