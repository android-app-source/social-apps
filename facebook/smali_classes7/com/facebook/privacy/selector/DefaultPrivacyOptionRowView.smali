.class public Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field private a:LX/8QM;

.field private b:Z

.field private c:Landroid/widget/ToggleButton;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1346052
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1346053
    invoke-direct {p0}, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->a()V

    .line 1346054
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1346055
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1346056
    invoke-direct {p0}, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->a()V

    .line 1346057
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1346058
    const v0, 0x7f0303fc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1346059
    const v0, 0x7f0d0c48

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->c:Landroid/widget/ToggleButton;

    .line 1346060
    const v0, 0x7f0d0c49

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->d:Landroid/widget/ImageView;

    .line 1346061
    const v0, 0x7f0d0c4b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->e:Landroid/widget/TextView;

    .line 1346062
    const v0, 0x7f0d0c4c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->f:Landroid/widget/TextView;

    .line 1346063
    invoke-direct {p0}, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->b()V

    .line 1346064
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1346065
    iget-object v0, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->c:Landroid/widget/ToggleButton;

    iget-boolean v1, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->b:Z

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 1346066
    iget-object v0, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->a:LX/8QM;

    if-eqz v0, :cond_0

    .line 1346067
    iget-object v0, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->d:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->a:LX/8QM;

    invoke-virtual {v1}, LX/8QL;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1346068
    iget-object v0, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->a:LX/8QM;

    invoke-virtual {v1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1346069
    iget-object v0, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->a:LX/8QM;

    invoke-virtual {v0}, LX/8QL;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1346070
    iget-object v0, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1346071
    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->a:LX/8QM;

    invoke-virtual {v1}, LX/8QL;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1346072
    :cond_0
    return-void

    .line 1346073
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public getIsSelected()Z
    .locals 1

    .prologue
    .line 1346074
    iget-boolean v0, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->b:Z

    return v0
.end method

.method public getPrivacyToken()LX/8QM;
    .locals 1

    .prologue
    .line 1346075
    iget-object v0, p0, Lcom/facebook/privacy/selector/DefaultPrivacyOptionRowView;->a:LX/8QM;

    return-object v0
.end method
