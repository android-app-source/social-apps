.class public Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;
.super Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;
.source ""


# static fields
.field public static final v:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private w:Lcom/facebook/resources/ui/FbTextView;

.field private x:Landroid/view/View;

.field public final y:Landroid/text/TextWatcher;

.field public final z:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1346198
    const-class v0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    sput-object v0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->v:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1346124
    invoke-direct {p0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;-><init>()V

    .line 1346125
    new-instance v0, LX/8S6;

    invoke-direct {v0, p0}, LX/8S6;-><init>(Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->y:Landroid/text/TextWatcher;

    .line 1346126
    new-instance v0, LX/8S7;

    invoke-direct {v0, p0}, LX/8S7;-><init>(Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->z:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1346127
    return-void
.end method

.method public static a(Z)Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;
    .locals 3

    .prologue
    .line 1346190
    new-instance v0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    invoke-direct {v0}, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;-><init>()V

    .line 1346191
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1346192
    const-string v2, "shouldHideToBar"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1346193
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1346194
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1346195
    invoke-super {p0, p1}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a(Landroid/os/Bundle;)V

    .line 1346196
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    .line 1346197
    return-void
.end method

.method public final e()V
    .locals 7

    .prologue
    .line 1346171
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v2

    .line 1346172
    invoke-virtual {p0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1346173
    :goto_0
    return-void

    .line 1346174
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1346175
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1346176
    const/4 v1, 0x0

    .line 1346177
    iget-object v5, v0, LX/8QL;->a:LX/8vA;

    sget-object v6, LX/8vA;->USER:LX/8vA;

    if-ne v5, v6, :cond_2

    .line 1346178
    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1346179
    invoke-static {v0}, LX/8Rp;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    move-result-object v0

    .line 1346180
    :goto_2
    if-eqz v0, :cond_1

    .line 1346181
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1346182
    :cond_2
    iget-object v5, v0, LX/8QL;->a:LX/8vA;

    sget-object v6, LX/8vA;->FRIENDLIST:LX/8vA;

    if-ne v5, v6, :cond_6

    .line 1346183
    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    .line 1346184
    invoke-static {v0}, LX/8Rp;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;)Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    move-result-object v0

    goto :goto_2

    .line 1346185
    :cond_3
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->l:LX/8Qe;

    if-eqz v0, :cond_4

    .line 1346186
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->l:LX/8Qe;

    invoke-interface {v0, v3}, LX/8Qe;->a(Ljava/util/List;)V

    .line 1346187
    :cond_4
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->m:LX/8Qf;

    if-eqz v0, :cond_5

    .line 1346188
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->m:LX/8Qf;

    invoke-interface {v0, v2}, LX/8Qf;->a(Ljava/util/List;)V

    .line 1346189
    :cond_5
    iget-object v0, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->w:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v3}, LX/8Rp;->a(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x479f71c4

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1346128
    const v0, 0x7f031023

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->o:Landroid/view/View;

    .line 1346129
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->o:Landroid/view/View;

    .line 1346130
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    iget-object v3, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->j:LX/8RM;

    new-instance p1, LX/8S8;

    invoke-direct {p1}, LX/8S8;-><init>()V

    invoke-virtual {v2, v3, p1}, LX/8tB;->a(LX/8RK;LX/8RE;)V

    .line 1346131
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    new-instance v3, LX/623;

    invoke-direct {v3}, LX/623;-><init>()V

    new-instance p1, LX/623;

    invoke-direct {p1}, LX/623;-><init>()V

    invoke-static {v3, p1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8tB;->a(Ljava/util/List;)V

    .line 1346132
    const v2, 0x7f0d26c5

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/listview/BetterListView;

    iput-object v2, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    .line 1346133
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->g:LX/8tB;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1346134
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->u:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1346135
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->z:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1346136
    const v2, 0x7f0d26c6

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->r:Landroid/view/View;

    .line 1346137
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->r:Landroid/view/View;

    new-instance v3, LX/8S4;

    invoke-direct {v3, p0}, LX/8S4;-><init>(Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1346138
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->o:Landroid/view/View;

    .line 1346139
    const v2, 0x7f0d26c3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iput-object v2, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1346140
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v3, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->y:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1346141
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    sget-object v3, LX/8ut;->NO_DROPDOWN:LX/8ut;

    .line 1346142
    iput-object v3, v2, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->f:LX/8ut;

    .line 1346143
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    sget-object v3, LX/8uy;->PLAIN_TEXT:LX/8uy;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setTextMode(LX/8uy;)V

    .line 1346144
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f0a00d2

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 1346145
    iput v3, v2, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->p:I

    .line 1346146
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setLongClickable(Z)V

    .line 1346147
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v3, LX/8S5;

    invoke-direct {v3, p0}, LX/8S5;-><init>(Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1346148
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->o:Landroid/view/View;

    const v2, 0x7f0d26c1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->w:Lcom/facebook/resources/ui/FbTextView;

    .line 1346149
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1346150
    iput v2, v0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->p:I

    .line 1346151
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setTokenIconColor(I)V

    .line 1346152
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    sget-object v2, LX/8ux;->RED:LX/8ux;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setSelectedTokenHighlightColor(LX/8ux;)V

    .line 1346153
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->o:Landroid/view/View;

    const v2, 0x7f0d26c4

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->n:Landroid/view/View;

    .line 1346154
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->o:Landroid/view/View;

    const v2, 0x7f0d26c0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->x:Landroid/view/View;

    .line 1346155
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->o:Landroid/view/View;

    const v2, 0x7f0d26c2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->s:Landroid/view/View;

    .line 1346156
    invoke-virtual {p0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->b()V

    .line 1346157
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, LX/8S2;

    invoke-direct {v2, p0}, LX/8S2;-><init>(Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalFocusChangeListener(Landroid/view/ViewTreeObserver$OnGlobalFocusChangeListener;)V

    .line 1346158
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->s:Landroid/view/View;

    iget-object v2, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->x:Landroid/view/View;

    .line 1346159
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const p1, 0x7f010289

    invoke-static {v3, p1}, LX/0WH;->b(Landroid/content/Context;I)LX/0am;

    move-result-object p1

    .line 1346160
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1346161
    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v3}, LX/1r0;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1346162
    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/Drawable;

    invoke-static {v2, v3}, LX/1r0;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1346163
    :cond_0
    new-instance v0, LX/8S3;

    invoke-direct {v0, p0}, LX/8S3;-><init>(Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;)V

    .line 1346164
    iget-object v2, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->x:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1346165
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 1346166
    const-string v3, "shouldHideToBar"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1346167
    iget-object v2, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->x:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1346168
    :goto_0
    iget-object v2, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->s:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1346169
    iget-object v0, p0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->o:Landroid/view/View;

    const v2, -0x3da54dc

    invoke-static {v2, v1}, LX/02F;->f(II)V

    return-object v0

    .line 1346170
    :cond_1
    iget-object v2, p0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->x:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
