.class public Lcom/facebook/privacy/selector/CustomPrivacyFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Sh;

.field public b:LX/0TD;

.field public c:LX/3LP;

.field public d:LX/8uo;

.field public e:LX/8Rp;

.field public f:Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

.field public g:Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

.field public h:LX/2RQ;

.field private i:Landroid/view/View;

.field public j:Landroid/view/View;

.field public k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;

.field public n:Lcom/facebook/widget/listview/BetterListView;

.field public o:Lcom/facebook/widget/listview/BetterListView;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;

.field private r:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "LX/8QL;",
            ">;>;"
        }
    .end annotation
.end field

.field public s:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

.field public t:LX/8Qe;

.field public u:LX/0RV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0RV",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;>;"
        }
    .end annotation
.end field

.field public v:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

.field public w:LX/8Qe;

.field public x:LX/0RV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0RV",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;>;"
        }
    .end annotation
.end field

.field public y:LX/0RV;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0RV",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1346029
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 1346011
    invoke-virtual {p2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1346012
    const v0, 0x7f031020

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->i:Landroid/view/View;

    .line 1346013
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->i:Landroid/view/View;

    const v1, 0x7f0d26b2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->l:Landroid/view/View;

    .line 1346014
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->i:Landroid/view/View;

    const v1, 0x7f0d26b8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->m:Landroid/view/View;

    .line 1346015
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->i:Landroid/view/View;

    const v1, 0x7f0d26b4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->j:Landroid/view/View;

    .line 1346016
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->i:Landroid/view/View;

    const v1, 0x7f0d26ba

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->k:Landroid/view/View;

    .line 1346017
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->i:Landroid/view/View;

    .line 1346018
    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->f:Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

    new-instance p1, LX/8Rt;

    invoke-direct {p1, p0}, LX/8Rt;-><init>(Lcom/facebook/privacy/selector/CustomPrivacyFragment;)V

    .line 1346019
    iput-object p1, v1, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->d:LX/8Rr;

    .line 1346020
    const v1, 0x7f0d26b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/listview/BetterListView;

    iput-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->n:Lcom/facebook/widget/listview/BetterListView;

    .line 1346021
    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->n:Lcom/facebook/widget/listview/BetterListView;

    iget-object p1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->f:Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

    invoke-virtual {v1, p1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1346022
    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->g:Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

    new-instance p1, LX/8Ru;

    invoke-direct {p1, p0}, LX/8Ru;-><init>(Lcom/facebook/privacy/selector/CustomPrivacyFragment;)V

    .line 1346023
    iput-object p1, v1, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->d:LX/8Rr;

    .line 1346024
    const v1, 0x7f0d26b9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/listview/BetterListView;

    iput-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->o:Lcom/facebook/widget/listview/BetterListView;

    .line 1346025
    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->o:Lcom/facebook/widget/listview/BetterListView;

    iget-object p1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->g:Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

    invoke-virtual {v1, p1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1346026
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->j:Landroid/view/View;

    new-instance v1, LX/8Rv;

    invoke-direct {v1, p0}, LX/8Rv;-><init>(Lcom/facebook/privacy/selector/CustomPrivacyFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1346027
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->k:Landroid/view/View;

    new-instance v1, LX/8Rw;

    invoke-direct {v1, p0}, LX/8Rw;-><init>(Lcom/facebook/privacy/selector/CustomPrivacyFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1346028
    return-void
.end method

.method public static a$redex0(Lcom/facebook/privacy/selector/CustomPrivacyFragment;Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x1

    .line 1346004
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1346005
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1346006
    invoke-virtual {p0, p1}, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->a(Ljava/util/List;)V

    .line 1346007
    invoke-virtual {p0, p2}, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->b(Ljava/util/List;)V

    .line 1346008
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 1346009
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 1346010
    return-void
.end method

.method public static b(Ljava/util/List;Ljava/util/List;Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;Lcom/facebook/privacy/selector/CustomPrivacyAdapter;LX/8Qe;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;",
            "Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;",
            "Lcom/facebook/privacy/selector/CustomPrivacyAdapter;",
            "LX/8Qe;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1345971
    if-eqz p2, :cond_2

    .line 1345972
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1345973
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1345974
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1345975
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1345976
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1345977
    invoke-static {p2, v0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->b(Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;)LX/8QL;

    move-result-object v3

    .line 1345978
    iget-object v4, p2, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;Z)V

    .line 1345979
    goto :goto_1

    .line 1345980
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1345981
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1345982
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1345983
    :cond_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1345984
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p3, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->c:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v5, v0

    .line 1345985
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1345986
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1345987
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1345988
    :cond_5
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8QL;

    .line 1345989
    const/4 v2, 0x0

    .line 1345990
    iget-object v8, v1, LX/8QL;->a:LX/8vA;

    sget-object v9, LX/8vA;->USER:LX/8vA;

    if-ne v8, v9, :cond_8

    move-object v2, v1

    .line 1345991
    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1345992
    iget-object v8, v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v2, v8

    .line 1345993
    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    .line 1345994
    :cond_7
    :goto_4
    if-eqz v2, :cond_6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1345995
    invoke-interface {v5, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1345996
    :cond_8
    iget-object v8, v1, LX/8QL;->a:LX/8vA;

    sget-object v9, LX/8vA;->FRIENDLIST:LX/8vA;

    if-ne v8, v9, :cond_7

    move-object v2, v1

    .line 1345997
    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    .line 1345998
    iget-object v8, v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->e:Ljava/lang/String;

    move-object v2, v8

    .line 1345999
    goto :goto_4

    .line 1346000
    :cond_9
    invoke-virtual {p3, v5}, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->a(Ljava/util/List;)V

    .line 1346001
    const v0, 0x7a88ad2a

    invoke-static {p3, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1346002
    invoke-interface {p4, v4}, LX/8Qe;->a(Ljava/util/List;)V

    .line 1346003
    :cond_a
    return-void
.end method

.method public static d(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1345959
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1345960
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1345961
    const/4 v1, 0x0

    .line 1345962
    iget-object v4, v0, LX/8QL;->a:LX/8vA;

    sget-object v5, LX/8vA;->USER:LX/8vA;

    if-ne v4, v5, :cond_1

    .line 1345963
    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1345964
    invoke-static {v0}, LX/8Rp;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    move-result-object v0

    .line 1345965
    :goto_1
    if-eqz v0, :cond_0

    .line 1345966
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1345967
    :cond_1
    iget-object v4, v0, LX/8QL;->a:LX/8vA;

    sget-object v5, LX/8vA;->FRIENDLIST:LX/8vA;

    if-ne v4, v5, :cond_3

    .line 1345968
    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    .line 1345969
    invoke-static {v0}, LX/8Rp;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;)Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    move-result-object v0

    goto :goto_1

    .line 1345970
    :cond_2
    return-object v2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public static e$redex0(Lcom/facebook/privacy/selector/CustomPrivacyFragment;)V
    .locals 4

    .prologue
    .line 1345937
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    .line 1345938
    const v1, 0x7f0d26ad

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1345939
    new-instance v1, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    invoke-direct {v1}, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;-><init>()V

    iput-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->s:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    .line 1345940
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1345941
    const v2, 0x7f0d26ad

    iget-object v3, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->s:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    invoke-virtual {v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1345942
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1345943
    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1345944
    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->t:LX/8Qe;

    if-eqz v0, :cond_0

    .line 1345945
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->s:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    new-instance v1, LX/8Rx;

    invoke-direct {v1, p0}, LX/8Rx;-><init>(Lcom/facebook/privacy/selector/CustomPrivacyFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a(LX/8Qe;)V

    .line 1345946
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->s:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->u:LX/0RV;

    .line 1345947
    iput-object v1, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->w:LX/0RV;

    .line 1345948
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->s:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->y:LX/0RV;

    .line 1345949
    iput-object v1, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->x:LX/0RV;

    .line 1345950
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->s:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    new-instance v1, LX/8Ry;

    invoke-direct {v1, p0}, LX/8Ry;-><init>(Lcom/facebook/privacy/selector/CustomPrivacyFragment;)V

    .line 1345951
    iput-object v1, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->m:LX/8Qf;

    .line 1345952
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->s:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    invoke-virtual {v0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->c()V

    .line 1345953
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->i:Landroid/view/View;

    const v1, 0x7f0d26ad

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->p:Landroid/view/View;

    .line 1345954
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->p:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1345955
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->q:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1345956
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->q:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1345957
    :cond_1
    return-void

    .line 1345958
    :cond_2
    const v1, 0x7f0d26ad

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    iput-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->s:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    goto :goto_0
.end method

.method public static k(Lcom/facebook/privacy/selector/CustomPrivacyFragment;)V
    .locals 4

    .prologue
    .line 1346030
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    .line 1346031
    const v1, 0x7f0d26ae

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1346032
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->a(Z)Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->v:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    .line 1346033
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1346034
    const v2, 0x7f0d26ae

    iget-object v3, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->v:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    invoke-virtual {v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1346035
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1346036
    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1346037
    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->w:LX/8Qe;

    if-eqz v0, :cond_0

    .line 1346038
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->v:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    new-instance v1, LX/8Rz;

    invoke-direct {v1, p0}, LX/8Rz;-><init>(Lcom/facebook/privacy/selector/CustomPrivacyFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a(LX/8Qe;)V

    .line 1346039
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->v:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->x:LX/0RV;

    .line 1346040
    iput-object v1, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->w:LX/0RV;

    .line 1346041
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->v:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->y:LX/0RV;

    .line 1346042
    iput-object v1, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->x:LX/0RV;

    .line 1346043
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->v:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    new-instance v1, LX/8S0;

    invoke-direct {v1, p0}, LX/8S0;-><init>(Lcom/facebook/privacy/selector/CustomPrivacyFragment;)V

    .line 1346044
    iput-object v1, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->m:LX/8Qf;

    .line 1346045
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->v:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    invoke-virtual {v0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->c()V

    .line 1346046
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->i:Landroid/view/View;

    const v1, 0x7f0d26ae

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->q:Landroid/view/View;

    .line 1346047
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->q:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1346048
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->p:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1346049
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1346050
    :cond_1
    return-void

    .line 1346051
    :cond_2
    const v1, 0x7f0d26ae

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    iput-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->v:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 1345934
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1345935
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v0}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v5

    check-cast v5, LX/3LP;

    invoke-static {v0}, LX/8Rp;->b(LX/0QB;)LX/8Rp;

    move-result-object v6

    check-cast v6, LX/8Rp;

    invoke-static {v0}, LX/8uo;->a(LX/0QB;)LX/8uo;

    move-result-object v7

    check-cast v7, LX/8uo;

    invoke-static {v0}, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->b(LX/0QB;)Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

    move-result-object v8

    check-cast v8, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

    invoke-static {v0}, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->b(LX/0QB;)Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

    move-result-object p1

    check-cast p1, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

    invoke-static {v0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v0

    check-cast v0, LX/2RQ;

    iput-object v3, v2, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->a:LX/0Sh;

    iput-object v4, v2, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->b:LX/0TD;

    iput-object v5, v2, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->c:LX/3LP;

    iput-object v6, v2, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->e:LX/8Rp;

    iput-object v7, v2, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->d:LX/8uo;

    iput-object v8, v2, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->f:Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

    iput-object p1, v2, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->g:Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

    iput-object v0, v2, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->h:LX/2RQ;

    .line 1345936
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1345932
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->f:Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

    invoke-virtual {v0, p1}, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->a(Ljava/util/List;)V

    .line 1345933
    return-void
.end method

.method public final b()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 1345907
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->y:LX/0RV;

    invoke-virtual {v0}, LX/0RV;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1345908
    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->u:LX/0RV;

    invoke-virtual {v1}, LX/0RV;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 1345909
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1345910
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1345911
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1345912
    :cond_0
    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->x:LX/0RV;

    invoke-virtual {v1}, LX/0RV;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 1345913
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 1345914
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1345915
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1345916
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1345917
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1345918
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1345919
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v7

    .line 1345920
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v1

    const/4 v8, 0x1

    if-ne v1, v8, :cond_2

    .line 1345921
    invoke-virtual {v7, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1345922
    invoke-virtual {v7, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1345923
    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->e:LX/8Rp;

    invoke-virtual {v1, v0}, LX/8Rp;->a(LX/1oS;)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1345924
    :cond_3
    invoke-virtual {v7, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1345925
    invoke-virtual {v7, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1345926
    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->e:LX/8Rp;

    invoke-virtual {v1, v0}, LX/8Rp;->a(LX/1oS;)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1345927
    :cond_4
    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v4}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1345928
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->a$redex0(Lcom/facebook/privacy/selector/CustomPrivacyFragment;Ljava/util/List;Ljava/util/List;)V

    .line 1345929
    :goto_3
    return-void

    .line 1345930
    :cond_5
    iget-object v6, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->b:LX/0TD;

    new-instance v0, LX/8S1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/8S1;-><init>(Lcom/facebook/privacy/selector/CustomPrivacyFragment;Ljava/util/HashSet;LX/0Pz;Ljava/util/HashSet;LX/0Pz;)V

    invoke-interface {v6, v0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->r:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1345931
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->a:LX/0Sh;

    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->r:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/8Rs;

    invoke-direct {v2, p0, v3, v5}, LX/8Rs;-><init>(Lcom/facebook/privacy/selector/CustomPrivacyFragment;LX/0Pz;LX/0Pz;)V

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_3
.end method

.method public final b(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1345905
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->g:Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

    invoke-virtual {v0, p1}, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->a(Ljava/util/List;)V

    .line 1345906
    return-void
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v0, 0x0

    .line 1345899
    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->p:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->p:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1345900
    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->p:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1345901
    :goto_0
    return v0

    .line 1345902
    :cond_0
    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->q:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->q:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 1345903
    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->q:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1345904
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 1345874
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1345875
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    .line 1345876
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v3

    .line 1345877
    iget-object v4, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->s:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    if-eqz v4, :cond_0

    .line 1345878
    iget-object v4, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->s:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    invoke-virtual {v3, v4}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 1345879
    iput-object v5, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->s:Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    .line 1345880
    :cond_0
    iget-object v4, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->v:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    if-eqz v4, :cond_1

    .line 1345881
    iget-object v4, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->v:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    invoke-virtual {v3, v4}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 1345882
    iput-object v5, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->v:Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    .line 1345883
    :cond_1
    invoke-virtual {v3}, LX/0hH;->b()I

    .line 1345884
    invoke-virtual {v1}, LX/0gc;->b()Z

    .line 1345885
    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->p:Landroid/view/View;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->p:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_4

    move v1, v0

    .line 1345886
    :goto_0
    iget-object v3, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->q:Landroid/view/View;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->q:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    move v2, v0

    .line 1345887
    :cond_2
    iput-object v5, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->p:Landroid/view/View;

    .line 1345888
    iput-object v5, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->q:Landroid/view/View;

    .line 1345889
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 1345890
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1345891
    check-cast v0, Landroid/view/ViewGroup;

    .line 1345892
    invoke-direct {p0, v3, v0}, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 1345893
    if-eqz v1, :cond_5

    .line 1345894
    invoke-static {p0}, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->e$redex0(Lcom/facebook/privacy/selector/CustomPrivacyFragment;)V

    .line 1345895
    :cond_3
    :goto_1
    return-void

    :cond_4
    move v1, v2

    .line 1345896
    goto :goto_0

    .line 1345897
    :cond_5
    if-eqz v2, :cond_3

    .line 1345898
    invoke-static {p0}, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->k(Lcom/facebook/privacy/selector/CustomPrivacyFragment;)V

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x5bb8bbf5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1345867
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1345868
    invoke-direct {p0, p1, v1}, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 1345869
    iget-object v2, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->j:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 1345870
    iget-object v2, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->k:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 1345871
    iget-object v2, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->l:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1345872
    iget-object v2, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->m:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1345873
    const/16 v2, 0x2b

    const v3, 0x43ebd4a4

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7bae1d30

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1345862
    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->r:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 1345863
    iget-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->r:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1345864
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->r:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1345865
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1345866
    const/16 v1, 0x2b

    const v2, 0x651889d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
