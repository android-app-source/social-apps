.class public Lcom/facebook/privacy/selector/AudienceFragmentDialog;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

.field public n:Lcom/facebook/privacy/selector/AudiencePickerFragment;

.field public o:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field private p:LX/8Qm;

.field public q:LX/8Q5;

.field public r:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1343611
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1343612
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/privacy/selector/AudienceFragmentDialog;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    iput-object v1, p1, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->r:LX/0ad;

    iput-object p0, p1, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->s:LX/0Uh;

    return-void
.end method

.method public static b(Lcom/facebook/privacy/selector/AudienceFragmentDialog;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1343613
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->s:LX/0Uh;

    const/16 v2, 0x440

    invoke-virtual {v1, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    .line 1343614
    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1343615
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->r:LX/0ad;

    sget-short v2, LX/8Po;->a:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1343616
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->b(Lcom/facebook/privacy/selector/AudienceFragmentDialog;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1343617
    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->n:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-virtual {v2}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1343618
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->n:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-virtual {v1}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->c()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    .line 1343619
    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->p:LX/8Qm;

    invoke-interface {v2, v1}, LX/8Qm;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1343620
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1343621
    goto :goto_0

    .line 1343622
    :cond_2
    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->m:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    if-eqz v2, :cond_0

    .line 1343623
    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->m:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-virtual {v2}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->S_()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1343624
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->m:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-virtual {v1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    .line 1343625
    iget-object v2, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->p:LX/8Qm;

    invoke-interface {v2, v1}, LX/8Qm;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1343626
    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 1343627
    new-instance v0, LX/8Qh;

    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->eK_()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LX/8Qh;-><init>(Lcom/facebook/privacy/selector/AudienceFragmentDialog;Landroid/content/Context;I)V

    return-object v0
.end method

.method public final a(LX/8Qm;)V
    .locals 1

    .prologue
    .line 1343628
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Qm;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->p:LX/8Qm;

    .line 1343629
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1343630
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1343631
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1343632
    if-eqz v0, :cond_0

    .line 1343633
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1343634
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 1343635
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x26735982

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1343636
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1343637
    const-class v1, Lcom/facebook/privacy/selector/AudienceFragmentDialog;

    invoke-static {v1, p0}, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->a(Ljava/lang/Class;LX/02k;)V

    .line 1343638
    const/4 v1, 0x0

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1343639
    const/16 v1, 0x2b

    const v2, -0x20fcd0cb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x223990e5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1343640
    if-eqz p3, :cond_0

    .line 1343641
    const/4 v0, 0x0

    const/16 v2, 0x2b

    const v3, 0x177b50e4

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1343642
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->b(Lcom/facebook/privacy/selector/AudienceFragmentDialog;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f030137

    :goto_1
    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v2, -0x59a741ce

    invoke-static {v2, v1}, LX/02F;->f(II)V

    goto :goto_0

    :cond_1
    const v0, 0x7f030138

    goto :goto_1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, -0x1

    const/16 v0, 0x2a

    const v1, -0x2ef75831

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1343643
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 1343644
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1343645
    if-nez v0, :cond_0

    .line 1343646
    const/16 v0, 0x2b

    const v2, 0x18f89536

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1343647
    :goto_0
    return-void

    .line 1343648
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1343649
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1343650
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1343651
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1343652
    iget-object v2, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v2, v2

    .line 1343653
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1343654
    const v0, -0x5923bb71

    invoke-static {v0, v1}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1343655
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1343656
    const v0, 0x7f0d05f9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->o:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1343657
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->o:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const p2, 0x7f0812fb

    invoke-virtual {v0, p2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 1343658
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const p2, 0x7f08001e

    invoke-virtual {p0, p2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 1343659
    iput-object p2, v0, LX/108;->g:Ljava/lang/String;

    .line 1343660
    move-object v0, v0

    .line 1343661
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 1343662
    iget-object p2, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->o:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 1343663
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->o:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance p2, LX/8Qk;

    invoke-direct {p2, p0}, LX/8Qk;-><init>(Lcom/facebook/privacy/selector/AudienceFragmentDialog;)V

    invoke-virtual {v0, p2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setOnToolbarButtonListener(LX/63W;)V

    .line 1343664
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->o:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance p2, LX/8Ql;

    invoke-direct {p2, p0}, LX/8Ql;-><init>(Lcom/facebook/privacy/selector/AudienceFragmentDialog;)V

    invoke-virtual {v0, p2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1343665
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->b(Lcom/facebook/privacy/selector/AudienceFragmentDialog;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1343666
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const p1, 0x7f0d05fa

    invoke-virtual {v0, p1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/selector/AudiencePickerFragment;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->n:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    .line 1343667
    new-instance v0, LX/8QF;

    invoke-direct {v0}, LX/8QF;-><init>()V

    iget-object p1, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->q:LX/8Q5;

    invoke-interface {p1}, LX/8Q5;->a()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object p1

    .line 1343668
    iput-object p1, v0, LX/8QF;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1343669
    move-object v0, v0

    .line 1343670
    iget-object p1, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->q:LX/8Q5;

    invoke-interface {p1}, LX/8Q5;->b()Z

    move-result p1

    .line 1343671
    iput-boolean p1, v0, LX/8QF;->b:Z

    .line 1343672
    move-object v0, v0

    .line 1343673
    invoke-virtual {v0}, LX/8QF;->a()Lcom/facebook/privacy/model/AudiencePickerInput;

    move-result-object v0

    .line 1343674
    iget-object p1, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->n:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    invoke-virtual {p1, v0}, Lcom/facebook/privacy/selector/AudiencePickerFragment;->a(Lcom/facebook/privacy/model/AudiencePickerInput;)V

    .line 1343675
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->n:Lcom/facebook/privacy/selector/AudiencePickerFragment;

    new-instance p1, LX/8Qj;

    invoke-direct {p1, p0}, LX/8Qj;-><init>(Lcom/facebook/privacy/selector/AudienceFragmentDialog;)V

    .line 1343676
    iput-object p1, v0, Lcom/facebook/privacy/selector/AudiencePickerFragment;->k:LX/8Qi;

    .line 1343677
    :cond_0
    :goto_0
    return-void

    .line 1343678
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const p1, 0x7f0d05fb

    invoke-virtual {v0, p1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->m:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    .line 1343679
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->m:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1343680
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->q:LX/8Q5;

    if-eqz v0, :cond_0

    .line 1343681
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->m:Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;

    iget-object p1, p0, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->q:LX/8Q5;

    invoke-virtual {v0, p1}, Lcom/facebook/privacy/selector/AudienceTypeaheadFragment;->a(LX/8Q5;)V

    goto :goto_0
.end method
