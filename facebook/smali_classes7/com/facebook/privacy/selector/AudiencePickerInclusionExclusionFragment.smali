.class public Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Sh;

.field public b:LX/0TD;

.field public c:LX/3LP;

.field public d:LX/8Rp;

.field public e:Landroid/view/inputmethod/InputMethodManager;

.field public f:LX/8tB;

.field public g:LX/8uo;

.field public h:LX/8RM;

.field public i:LX/0wM;

.field public j:LX/2RQ;

.field public k:LX/8RD;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;"
        }
    .end annotation
.end field

.field public q:LX/8R1;

.field public r:Landroid/view/View;

.field public s:Lcom/facebook/ui/search/SearchEditText;

.field private final t:Landroid/text/TextWatcher;

.field public final u:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1344483
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1344484
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->o:Ljava/util/ArrayList;

    .line 1344485
    new-instance v0, LX/8RB;

    invoke-direct {v0, p0}, LX/8RB;-><init>(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->t:Landroid/text/TextWatcher;

    .line 1344486
    new-instance v0, LX/8RC;

    invoke-direct {v0, p0}, LX/8RC;-><init>(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->u:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1344487
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1344488
    if-nez p1, :cond_0

    move v0, v1

    .line 1344489
    :goto_0
    return v0

    .line 1344490
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1344491
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1344492
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1344493
    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/621",
            "<+",
            "LX/8QL;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1344494
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1344495
    new-instance v3, Ljava/util/TreeMap;

    invoke-direct {v3}, Ljava/util/TreeMap;-><init>()V

    .line 1344496
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1344497
    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->m:Ljava/util/List;

    invoke-static {v1, v5}, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1344498
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1344499
    :cond_0
    invoke-virtual {v0}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v1

    .line 1344500
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Ljava/lang/String;->codePointAt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v5

    move-object v1, v5

    .line 1344501
    invoke-virtual {v3, v1}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1344502
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    invoke-virtual {v3, v1, v5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1344503
    :cond_1
    invoke-virtual {v3, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Pz;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1344504
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1344505
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oS;

    .line 1344506
    iget-object v5, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->d:LX/8Rp;

    invoke-virtual {v5, v0}, LX/8Rp;->a(LX/1oS;)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    move-result-object v0

    .line 1344507
    if-eqz v0, :cond_3

    .line 1344508
    iget-object v5, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;->e:Ljava/lang/String;

    move-object v5, v5

    .line 1344509
    iget-object v6, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->m:Ljava/util/List;

    invoke-static {v5, v6}, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1344510
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1344511
    :cond_4
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1344512
    :cond_5
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1344513
    new-instance v0, LX/623;

    const/4 v1, 0x0

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1344514
    invoke-virtual {v3}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1344515
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1344516
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pz;

    .line 1344517
    new-instance v3, LX/623;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-direct {v3, v1, v0}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1344518
    :cond_6
    new-instance v0, LX/623;

    const v1, 0x7f08130d

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1344519
    return-object v5
.end method

.method public static k(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V
    .locals 2

    .prologue
    .line 1344520
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 1344521
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setEnabled(Z)V

    .line 1344522
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setAlpha(F)V

    .line 1344523
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->r:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1344524
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->l(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V

    .line 1344525
    return-void
.end method

.method public static l(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V
    .locals 7

    .prologue
    .line 1344526
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->m(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1344527
    :goto_0
    return-void

    .line 1344528
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->q:LX/8R1;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->o:Ljava/util/ArrayList;

    .line 1344529
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1344530
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8QL;

    .line 1344531
    iget-object v5, v2, LX/8QL;->a:LX/8vA;

    sget-object v6, LX/8vA;->FRIENDLIST:LX/8vA;

    if-ne v5, v6, :cond_1

    .line 1344532
    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    .line 1344533
    invoke-static {v2}, LX/8Rp;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;)Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1344534
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8QL;

    .line 1344535
    iget-object v5, v2, LX/8QL;->a:LX/8vA;

    sget-object v6, LX/8vA;->USER:LX/8vA;

    if-ne v5, v6, :cond_3

    .line 1344536
    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1344537
    invoke-static {v2}, LX/8Rp;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1344538
    :cond_4
    move-object v1, v3

    .line 1344539
    invoke-interface {v0, v1}, LX/8R1;->a(Ljava/util/List;)V

    .line 1344540
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->f:LX/8tB;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->o:Ljava/util/ArrayList;

    .line 1344541
    iput-object v1, v0, LX/8tB;->i:Ljava/util/List;

    .line 1344542
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->f:LX/8tB;

    const v1, 0x5b903774

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public static m(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)Z
    .locals 1

    .prologue
    .line 1344482
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 1344543
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1344544
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v0}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v5

    check-cast v5, LX/3LP;

    invoke-static {v0}, LX/8RM;->c(LX/0QB;)LX/8RM;

    move-result-object v6

    check-cast v6, LX/8RM;

    invoke-static {v0}, LX/8Rp;->b(LX/0QB;)LX/8Rp;

    move-result-object v7

    check-cast v7, LX/8Rp;

    invoke-static {v0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v8

    check-cast v8, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v0}, LX/8tB;->b(LX/0QB;)LX/8tB;

    move-result-object v9

    check-cast v9, LX/8tB;

    invoke-static {v0}, LX/8uo;->a(LX/0QB;)LX/8uo;

    move-result-object v10

    check-cast v10, LX/8uo;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v11

    check-cast v11, LX/0wM;

    invoke-static {v0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v0

    check-cast v0, LX/2RQ;

    iput-object v3, v2, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->a:LX/0Sh;

    iput-object v4, v2, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->b:LX/0TD;

    iput-object v5, v2, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->c:LX/3LP;

    iput-object v7, v2, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->d:LX/8Rp;

    iput-object v8, v2, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->e:Landroid/view/inputmethod/InputMethodManager;

    iput-object v9, v2, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->f:LX/8tB;

    iput-object v10, v2, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->g:LX/8uo;

    iput-object v6, v2, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->h:LX/8RM;

    iput-object v11, v2, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->i:LX/0wM;

    iput-object v0, v2, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->j:LX/2RQ;

    .line 1344545
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 1344546
    invoke-static {}, LX/8RD;->values()[LX/8RD;

    move-result-object v0

    const-string v2, "Type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->k:LX/8RD;

    .line 1344547
    const-string v0, "FriendLists"

    invoke-static {v1, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->l:Ljava/util/List;

    .line 1344548
    const-string v0, "PreselectedMembers"

    invoke-static {v1, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->m:Ljava/util/List;

    .line 1344549
    if-nez p1, :cond_1

    .line 1344550
    :cond_0
    return-void

    .line 1344551
    :cond_1
    const-string v0, "Friends"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->n:Ljava/util/ArrayList;

    .line 1344552
    const-string v0, "SelectedMembers"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1344553
    if-eqz v0, :cond_0

    .line 1344554
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->o:Ljava/util/ArrayList;

    .line 1344555
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 1344556
    instance-of v2, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    if-eqz v2, :cond_3

    .line 1344557
    iget-object v2, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->o:Ljava/util/ArrayList;

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1344558
    :cond_3
    instance-of v2, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    if-eqz v2, :cond_2

    .line 1344559
    iget-object v2, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->o:Ljava/util/ArrayList;

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1344480
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->e:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1344481
    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1344478
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 1344479
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6c6dee8d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1344477
    const v1, 0x7f031029

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x31e4bea0

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0xbd8cf52

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1344470
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 1344471
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1344472
    iput-object v3, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1344473
    :cond_0
    iput-object v3, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->r:Landroid/view/View;

    .line 1344474
    iput-object v3, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    .line 1344475
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1344476
    const/16 v1, 0x2b

    const v2, 0x2bae6eb0

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2b6b0d6d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1344467
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->d()V

    .line 1344468
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1344469
    const/16 v1, 0x2b

    const v2, -0x39199bbb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1344430
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1344431
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1344432
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1344433
    instance-of v4, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    if-eqz v4, :cond_1

    .line 1344434
    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1344435
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1344436
    :cond_1
    instance-of v4, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    if-eqz v4, :cond_0

    .line 1344437
    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleFriendlistToken;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1344438
    :cond_2
    const-string v0, "SelectedMembers"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1344439
    const-string v0, "Friends"

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1344440
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 1344441
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->f:LX/8tB;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->h:LX/8RM;

    new-instance v2, LX/8RG;

    iget-object v3, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->k:LX/8RD;

    invoke-direct {v2, v3}, LX/8RG;-><init>(LX/8RD;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/8tB;->a(LX/8RK;LX/8RE;Z)V

    .line 1344442
    const v0, 0x7f0d26ce

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 1344443
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->f:LX/8tB;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1344444
    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->u:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1344445
    const v0, 0x7f0d26cf

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/8RA;

    invoke-direct {v1, p0}, LX/8RA;-><init>(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1344446
    const v0, 0x7f0d26cc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    .line 1344447
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->t:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1344448
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setLongClickable(Z)V

    .line 1344449
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    new-instance v1, LX/8R8;

    invoke-direct {v1, p0}, LX/8R8;-><init>(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1344450
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->i:LX/0wM;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02091b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const v3, -0x6f6b64

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1, v4, v4, v4}, Lcom/facebook/ui/search/SearchEditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1344451
    const v0, 0x7f0d26cd

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->r:Landroid/view/View;

    .line 1344452
    const/4 v2, 0x0

    .line 1344453
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0, v2}, Lcom/facebook/ui/search/SearchEditText;->setEnabled(Z)V

    .line 1344454
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0, v2}, Lcom/facebook/ui/search/SearchEditText;->setVisibility(I)V

    .line 1344455
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setAlpha(F)V

    .line 1344456
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->r:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1344457
    const v0, 0x7f0d26cb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/8R9;

    invoke-direct {v1, p0}, LX/8R9;-><init>(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1344458
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->m(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1344459
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->f:LX/8tB;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->n:Ljava/util/ArrayList;

    invoke-static {p0, v1}, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->b$redex0(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8tB;->a(Ljava/util/List;)V

    .line 1344460
    invoke-static {p0}, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->k(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V

    .line 1344461
    :goto_0
    iget-object v5, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v5}, Lcom/facebook/ui/search/SearchEditText;->requestFocus()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1344462
    new-instance v5, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment$3;

    invoke-direct {v5, p0}, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment$3;-><init>(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V

    .line 1344463
    iget-object v6, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->s:Lcom/facebook/ui/search/SearchEditText;

    const-wide/16 v7, 0x64

    invoke-virtual {v6, v5, v7, v8}, Lcom/facebook/ui/search/SearchEditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1344464
    :cond_0
    return-void

    .line 1344465
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->b:LX/0TD;

    new-instance v1, LX/8R6;

    invoke-direct {v1, p0}, LX/8R6;-><init>(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1344466
    iget-object v0, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->a:LX/0Sh;

    iget-object v1, p0, Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/8R7;

    invoke-direct {v2, p0}, LX/8R7;-><init>(Lcom/facebook/privacy/selector/AudiencePickerInclusionExclusionFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method
