.class public Lcom/facebook/privacy/selector/CustomPrivacyAdapter;
.super LX/1Cv;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Landroid/view/LayoutInflater;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/8Rr;

.field private final e:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1345779
    const-class v0, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1345798
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1345799
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->c:Ljava/util/List;

    .line 1345800
    new-instance v0, LX/8Rq;

    invoke-direct {v0, p0}, LX/8Rq;-><init>(Lcom/facebook/privacy/selector/CustomPrivacyAdapter;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->e:Landroid/view/View$OnClickListener;

    .line 1345801
    iput-object p1, p0, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->b:Landroid/view/LayoutInflater;

    .line 1345802
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/privacy/selector/CustomPrivacyAdapter;
    .locals 2

    .prologue
    .line 1345803
    new-instance v1, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-direct {v1, v0}, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;-><init>(Landroid/view/LayoutInflater;)V

    .line 1345804
    return-object v1
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1345780
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f031021

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 1345781
    check-cast p2, LX/8QL;

    .line 1345782
    check-cast p3, Landroid/widget/LinearLayout;

    .line 1345783
    const v0, 0x7f0d26bb

    invoke-virtual {p3, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1345784
    const v1, 0x7f0d26bc

    invoke-virtual {p3, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1345785
    const v2, 0x7f0d26bd

    invoke-virtual {p3, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/text/BetterTextView;

    .line 1345786
    const v3, 0x7f0d26be

    invoke-virtual {p3, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1345787
    invoke-virtual {p2}, LX/8QL;->g()I

    move-result v4

    if-lez v4, :cond_0

    .line 1345788
    invoke-virtual {p2}, LX/8QL;->g()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1345789
    invoke-virtual {v0, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1345790
    invoke-virtual {v1, v6}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1345791
    :goto_0
    invoke-virtual {p2}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1345792
    const v0, 0x7f0d26be

    invoke-virtual {v3, v0, p2}, Lcom/facebook/fbui/glyph/GlyphView;->setTag(ILjava/lang/Object;)V

    .line 1345793
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1345794
    return-void

    .line 1345795
    :cond_0
    invoke-virtual {p2}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1345796
    invoke-virtual {v0, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1345797
    invoke-virtual {v1, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1345773
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->c:Ljava/util/List;

    .line 1345774
    const v0, -0x65296ae6

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1345775
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1345776
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1345777
    iget-object v0, p0, Lcom/facebook/privacy/selector/CustomPrivacyAdapter;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1345778
    int-to-long v0, p1

    return-wide v0
.end method
