.class public Lcom/facebook/privacy/model/SelectablePrivacyData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/model/SelectablePrivacyData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

.field public final b:Z

.field public final c:Z

.field public final d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1343262
    new-instance v0, LX/8QU;

    invoke-direct {v0}, LX/8QU;-><init>()V

    sput-object v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/8QV;)V
    .locals 1

    .prologue
    .line 1343263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1343264
    iget-object v0, p1, LX/8QV;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iput-object v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1343265
    iget-object v0, p1, LX/8QV;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1343266
    iget-boolean v0, p1, LX/8QV;->c:Z

    iput-boolean v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    .line 1343267
    iget-boolean v0, p1, LX/8QV;->d:Z

    iput-boolean v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->c:Z

    .line 1343268
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1343270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1343271
    const-class v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iput-object v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1343272
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1343273
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    .line 1343274
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->c:Z

    .line 1343275
    return-void
.end method


# virtual methods
.method public final b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 2

    .prologue
    .line 1343246
    iget-object v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QP;

    move-result-object v0

    .line 1343247
    iget-boolean v1, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v1, v1

    .line 1343248
    if-eqz v1, :cond_0

    .line 1343249
    invoke-virtual {v0}, LX/8QP;->a()LX/8QP;

    .line 1343250
    :cond_0
    invoke-virtual {v0}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1343269
    iget-object v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v1, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)I

    move-result v0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1343251
    iget-object v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1343252
    if-nez v0, :cond_0

    .line 1343253
    const/4 v0, 0x0

    .line 1343254
    :goto_0
    return-object v0

    .line 1343255
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    if-nez v0, :cond_2

    .line 1343256
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1343257
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1343258
    :cond_2
    iget-object v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1343259
    invoke-static {v0}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0}, LX/8QP;->a()LX/8QP;

    move-result-object v0

    invoke-virtual {v0}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    .line 1343260
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1343261
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1343227
    if-ne p0, p1, :cond_1

    .line 1343228
    :cond_0
    :goto_0
    return v0

    .line 1343229
    :cond_1
    instance-of v2, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-nez v2, :cond_2

    move v0, v1

    .line 1343230
    goto :goto_0

    .line 1343231
    :cond_2
    check-cast p1, Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1343232
    iget-object v2, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v3, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v3, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v2, v3}, LX/2cA;->a(LX/1oS;LX/1oS;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    iget-boolean v3, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->c:Z

    iget-boolean v3, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->c:Z

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1343224
    iget-object v1, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v1

    .line 1343225
    if-nez v1, :cond_1

    .line 1343226
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/privacy/model/SelectablePrivacyData;->g()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->FRIENDS_OF_TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-eq v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/facebook/privacy/model/SelectablePrivacyData;->g()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final g()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;
    .locals 2

    .prologue
    .line 1343233
    iget-object v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1343234
    if-nez v0, :cond_0

    .line 1343235
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    .line 1343236
    :goto_0
    return-object v0

    .line 1343237
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1343238
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v0

    goto :goto_0

    .line 1343239
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1343240
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1343241
    iget-object v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1343242
    iget-object v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1343243
    iget-boolean v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1343244
    iget-boolean v0, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1343245
    return-void
.end method
