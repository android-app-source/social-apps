.class public Lcom/facebook/privacy/model/PrivacyParameterSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/privacy/model/PrivacyParameter;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1343158
    const-class v0, Lcom/facebook/privacy/model/PrivacyParameter;

    new-instance v1, Lcom/facebook/privacy/model/PrivacyParameterSerializer;

    invoke-direct {v1}, Lcom/facebook/privacy/model/PrivacyParameterSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1343159
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1343144
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/privacy/model/PrivacyParameter;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1343145
    if-nez p0, :cond_0

    .line 1343146
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1343147
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1343148
    invoke-static {p0, p1, p2}, Lcom/facebook/privacy/model/PrivacyParameterSerializer;->b(Lcom/facebook/privacy/model/PrivacyParameter;LX/0nX;LX/0my;)V

    .line 1343149
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1343150
    return-void
.end method

.method private static b(Lcom/facebook/privacy/model/PrivacyParameter;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1343151
    const-string v0, "value"

    iget-object v1, p0, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1343152
    const-string v0, "deny"

    iget-object v1, p0, Lcom/facebook/privacy/model/PrivacyParameter;->deny:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1343153
    const-string v0, "allow"

    iget-object v1, p0, Lcom/facebook/privacy/model/PrivacyParameter;->allow:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1343154
    const-string v0, "friends"

    iget-object v1, p0, Lcom/facebook/privacy/model/PrivacyParameter;->friends:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1343155
    const-string v0, "settings"

    iget-object v1, p0, Lcom/facebook/privacy/model/PrivacyParameter;->settings:Lcom/facebook/privacy/model/PrivacyParameter$Settings;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1343156
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1343157
    check-cast p1, Lcom/facebook/privacy/model/PrivacyParameter;

    invoke-static {p1, p2, p3}, Lcom/facebook/privacy/model/PrivacyParameterSerializer;->a(Lcom/facebook/privacy/model/PrivacyParameter;LX/0nX;LX/0my;)V

    return-void
.end method
