.class public final Lcom/facebook/privacy/model/PrivacyParameter$Settings;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/model/PrivacyParameter_SettingsDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/model/PrivacyParameter_SettingsSerializer;
.end annotation


# instance fields
.field public final noTagExpansion:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "no_tag_expansion"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1343091
    const-class v0, Lcom/facebook/privacy/model/PrivacyParameter_SettingsDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1343084
    const-class v0, Lcom/facebook/privacy/model/PrivacyParameter_SettingsSerializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1343088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1343089
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/privacy/model/PrivacyParameter$Settings;->noTagExpansion:Z

    .line 1343090
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 1343085
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1343086
    iput-boolean p1, p0, Lcom/facebook/privacy/model/PrivacyParameter$Settings;->noTagExpansion:Z

    .line 1343087
    return-void
.end method
