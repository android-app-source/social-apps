.class public Lcom/facebook/privacy/model/AudiencePickerModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/model/AudiencePickerModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final e:I

.field public final f:I

.field public final g:Z

.field public final h:Z

.field public final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1342772
    new-instance v0, LX/8QG;

    invoke-direct {v0}, LX/8QG;-><init>()V

    sput-object v0, Lcom/facebook/privacy/model/AudiencePickerModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/8QH;)V
    .locals 1

    .prologue
    .line 1342773
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1342774
    iget-object v0, p1, LX/8QH;->a:LX/0Px;

    iput-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    .line 1342775
    iget-object v0, p1, LX/8QH;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->b:LX/0Px;

    .line 1342776
    iget-object v0, p1, LX/8QH;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->c:LX/0Px;

    .line 1342777
    iget-object v0, p1, LX/8QH;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->d:LX/0Px;

    .line 1342778
    iget v0, p1, LX/8QH;->e:I

    iput v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->e:I

    .line 1342779
    iget v0, p1, LX/8QH;->f:I

    iput v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->f:I

    .line 1342780
    iget-boolean v0, p1, LX/8QH;->g:Z

    iput-boolean v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->g:Z

    .line 1342781
    iget-boolean v0, p1, LX/8QH;->h:Z

    iput-boolean v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->h:Z

    .line 1342782
    iget-object v0, p1, LX/8QH;->i:LX/0Px;

    iput-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->i:LX/0Px;

    .line 1342783
    iget-object v0, p1, LX/8QH;->j:LX/0Px;

    iput-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->j:LX/0Px;

    .line 1342784
    iget-boolean v0, p1, LX/8QH;->k:Z

    iput-boolean v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->k:Z

    .line 1342785
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1342786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1342787
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    .line 1342788
    if-nez v0, :cond_0

    .line 1342789
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1342790
    :goto_0
    iput-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    .line 1342791
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    .line 1342792
    if-nez v0, :cond_1

    .line 1342793
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1342794
    :goto_1
    iput-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->b:LX/0Px;

    .line 1342795
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1342796
    const-class v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1342797
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->c:LX/0Px;

    .line 1342798
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1342799
    const-class v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1342800
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->d:LX/0Px;

    .line 1342801
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->e:I

    .line 1342802
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->f:I

    .line 1342803
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->g:Z

    .line 1342804
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->h:Z

    .line 1342805
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    .line 1342806
    if-nez v0, :cond_2

    .line 1342807
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1342808
    :goto_2
    iput-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->i:LX/0Px;

    .line 1342809
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    .line 1342810
    if-nez v0, :cond_3

    .line 1342811
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1342812
    :goto_3
    iput-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->j:LX/0Px;

    .line 1342813
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->k:Z

    .line 1342814
    return-void

    .line 1342815
    :cond_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1342816
    :cond_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_1

    .line 1342817
    :cond_2
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_2

    .line 1342818
    :cond_3
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_3
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1342819
    const/4 v0, 0x0

    return v0
.end method

.method public final l()LX/8QH;
    .locals 1

    .prologue
    .line 1342820
    new-instance v0, LX/8QH;

    invoke-direct {v0, p0}, LX/8QH;-><init>(Lcom/facebook/privacy/model/AudiencePickerModel;)V

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1342821
    iget-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->a:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 1342822
    iget-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->b:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 1342823
    iget-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1342824
    iget-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1342825
    iget v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1342826
    iget v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1342827
    iget-boolean v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1342828
    iget-boolean v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->h:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1342829
    iget-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->i:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 1342830
    iget-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->j:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 1342831
    iget-boolean v0, p0, Lcom/facebook/privacy/model/AudiencePickerModel;->k:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1342832
    return-void
.end method
