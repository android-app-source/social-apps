.class public Lcom/facebook/privacy/model/PrivacyParameter_SettingsSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/privacy/model/PrivacyParameter$Settings;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1343191
    const-class v0, Lcom/facebook/privacy/model/PrivacyParameter$Settings;

    new-instance v1, Lcom/facebook/privacy/model/PrivacyParameter_SettingsSerializer;

    invoke-direct {v1}, Lcom/facebook/privacy/model/PrivacyParameter_SettingsSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1343192
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1343190
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/privacy/model/PrivacyParameter$Settings;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1343184
    if-nez p0, :cond_0

    .line 1343185
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1343186
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1343187
    invoke-static {p0, p1, p2}, Lcom/facebook/privacy/model/PrivacyParameter_SettingsSerializer;->b(Lcom/facebook/privacy/model/PrivacyParameter$Settings;LX/0nX;LX/0my;)V

    .line 1343188
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1343189
    return-void
.end method

.method private static b(Lcom/facebook/privacy/model/PrivacyParameter$Settings;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1343181
    const-string v0, "no_tag_expansion"

    iget-boolean v1, p0, Lcom/facebook/privacy/model/PrivacyParameter$Settings;->noTagExpansion:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1343182
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1343183
    check-cast p1, Lcom/facebook/privacy/model/PrivacyParameter$Settings;

    invoke-static {p1, p2, p3}, Lcom/facebook/privacy/model/PrivacyParameter_SettingsSerializer;->a(Lcom/facebook/privacy/model/PrivacyParameter$Settings;LX/0nX;LX/0my;)V

    return-void
.end method
