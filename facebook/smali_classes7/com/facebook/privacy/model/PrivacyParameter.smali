.class public final Lcom/facebook/privacy/model/PrivacyParameter;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/model/PrivacyParameterDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/model/PrivacyParameterSerializer;
.end annotation


# instance fields
.field public final allow:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "allow"
    .end annotation
.end field

.field public final deny:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "deny"
    .end annotation
.end field

.field public final friends:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "friends"
    .end annotation
.end field

.field public final settings:Lcom/facebook/privacy/model/PrivacyParameter$Settings;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "settings"
    .end annotation
.end field

.field public final value:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "value"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1343101
    const-class v0, Lcom/facebook/privacy/model/PrivacyParameterDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1343102
    const-class v0, Lcom/facebook/privacy/model/PrivacyParameterSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1343103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1343104
    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    .line 1343105
    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->allow:Ljava/lang/String;

    .line 1343106
    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->deny:Ljava/lang/String;

    .line 1343107
    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->friends:Ljava/lang/String;

    .line 1343108
    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->settings:Lcom/facebook/privacy/model/PrivacyParameter$Settings;

    .line 1343109
    return-void
.end method

.method public constructor <init>(LX/8QS;)V
    .locals 2

    .prologue
    .line 1343110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1343111
    iget-object v0, p1, LX/8QS;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    .line 1343112
    iget-object v0, p1, LX/8QS;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->allow:Ljava/lang/String;

    .line 1343113
    iget-object v0, p1, LX/8QS;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->deny:Ljava/lang/String;

    .line 1343114
    iget-boolean v0, p1, LX/8QS;->e:Z

    if-eqz v0, :cond_0

    .line 1343115
    new-instance v0, Lcom/facebook/privacy/model/PrivacyParameter$Settings;

    iget-boolean v1, p1, LX/8QS;->e:Z

    invoke-direct {v0, v1}, Lcom/facebook/privacy/model/PrivacyParameter$Settings;-><init>(Z)V

    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->settings:Lcom/facebook/privacy/model/PrivacyParameter$Settings;

    .line 1343116
    :goto_0
    iget-object v0, p1, LX/8QS;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->friends:Ljava/lang/String;

    .line 1343117
    return-void

    .line 1343118
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/privacy/model/PrivacyParameter;->settings:Lcom/facebook/privacy/model/PrivacyParameter$Settings;

    goto :goto_0
.end method
