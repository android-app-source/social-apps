.class public Lcom/facebook/privacy/model/AudiencePickerInput;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/model/AudiencePickerInput;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/privacy/model/SelectablePrivacyData;

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1342730
    new-instance v0, LX/8QE;

    invoke-direct {v0}, LX/8QE;-><init>()V

    sput-object v0, Lcom/facebook/privacy/model/AudiencePickerInput;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/8QF;)V
    .locals 1

    .prologue
    .line 1342731
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1342732
    iget-object v0, p1, LX/8QF;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerInput;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1342733
    iget-boolean v0, p1, LX/8QF;->b:Z

    iput-boolean v0, p0, Lcom/facebook/privacy/model/AudiencePickerInput;->b:Z

    .line 1342734
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1342735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1342736
    const-class v0, Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerInput;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1342737
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/model/AudiencePickerInput;->b:Z

    .line 1342738
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1342739
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1342740
    iget-object v0, p0, Lcom/facebook/privacy/model/AudiencePickerInput;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1342741
    iget-boolean v0, p0, Lcom/facebook/privacy/model/AudiencePickerInput;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1342742
    return-void
.end method
