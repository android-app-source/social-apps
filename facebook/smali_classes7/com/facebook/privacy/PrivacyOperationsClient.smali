.class public Lcom/facebook/privacy/PrivacyOperationsClient;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile k:Lcom/facebook/privacy/PrivacyOperationsClient;


# instance fields
.field public final c:LX/0aG;

.field public final d:LX/2sP;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1dw;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3R3;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1342076
    const-class v0, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1342077
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "set_privacy_education_state"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "set_composer_sticky_privacy"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "report_aaa_tux_action"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "report_aaa_only_me_action"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "report_nas_action"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "report_sticky_guardrail_action"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "report_inline_privacy_survey_action"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "report_privacy_checkup_action"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "edit_objects_privacy_operation_type"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "bulk_edit_album_privacy_operation_type"

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/facebook/privacy/PrivacyOperationsClient;->b:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/2sP;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/privacy/gating/IsDefaultPostPrivacyEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/2sP;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/1dw;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3R3;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1341981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1341982
    iput-object p1, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    .line 1341983
    iput-object p2, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->d:LX/2sP;

    .line 1341984
    iput-object p3, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->e:Ljava/util/concurrent/ExecutorService;

    .line 1341985
    iput-object p4, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->f:LX/0Ot;

    .line 1341986
    iput-object p5, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->g:LX/0Ot;

    .line 1341987
    iput-object p6, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->h:LX/0Ot;

    .line 1341988
    iput-object p7, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->i:LX/0Ot;

    .line 1341989
    iput-object p8, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->j:LX/0Or;

    .line 1341990
    return-void
.end method

.method private static a(Lcom/facebook/privacy/model/PrivacyOptionsResult;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1342078
    iget-object v3, p0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1342079
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1342080
    invoke-static {v0}, LX/2cA;->a(LX/1oR;)Lcom/facebook/privacy/model/PrivacyParameter;

    move-result-object v7

    .line 1342081
    invoke-static {p1}, LX/2cA;->a(LX/1oR;)Lcom/facebook/privacy/model/PrivacyParameter;

    move-result-object v8

    .line 1342082
    iget-object v9, v7, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    iget-object p0, v8, Lcom/facebook/privacy/model/PrivacyParameter;->value:Ljava/lang/String;

    invoke-static {v9, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 1342083
    invoke-static {v7}, LX/2cA;->a(Lcom/facebook/privacy/model/PrivacyParameter;)Ljava/lang/String;

    move-result-object v7

    .line 1342084
    invoke-static {v8}, LX/2cA;->a(Lcom/facebook/privacy/model/PrivacyParameter;)Ljava/lang/String;

    move-result-object v8

    .line 1342085
    invoke-static {v7, v8}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 1342086
    :cond_0
    :goto_1
    move v0, v5

    .line 1342087
    if-eqz v0, :cond_1

    move v0, v1

    .line 1342088
    :goto_2
    return v0

    .line 1342089
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 1342090
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1342091
    :cond_2
    const/4 v0, -0x1

    goto :goto_2

    .line 1342092
    :cond_3
    sget-object v8, LX/8QT;->CUSTOM:LX/8QT;

    invoke-virtual {v8}, LX/8QT;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    move v5, v6

    .line 1342093
    goto :goto_1

    .line 1342094
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v7

    invoke-static {v7}, LX/2cA;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object v7

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v8

    invoke-static {v8}, LX/2cA;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object v8

    invoke-static {v7, v8}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    .line 1342095
    if-eqz v7, :cond_0

    .line 1342096
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v7

    invoke-static {v7}, LX/2cA;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object v7

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v8

    invoke-static {v8}, LX/2cA;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object v8

    invoke-static {v7, v8}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    .line 1342097
    if-eqz v7, :cond_0

    move v5, v6

    .line 1342098
    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;
    .locals 12

    .prologue
    .line 1342099
    sget-object v0, Lcom/facebook/privacy/PrivacyOperationsClient;->k:Lcom/facebook/privacy/PrivacyOperationsClient;

    if-nez v0, :cond_1

    .line 1342100
    const-class v1, Lcom/facebook/privacy/PrivacyOperationsClient;

    monitor-enter v1

    .line 1342101
    :try_start_0
    sget-object v0, Lcom/facebook/privacy/PrivacyOperationsClient;->k:Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1342102
    if-eqz v2, :cond_0

    .line 1342103
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1342104
    new-instance v3, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, LX/2sP;->a(LX/0QB;)LX/2sP;

    move-result-object v5

    check-cast v5, LX/2sP;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    const/16 v7, 0xb16

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x259

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xafd

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xfbb

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x34c

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, Lcom/facebook/privacy/PrivacyOperationsClient;-><init>(LX/0aG;LX/2sP;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V

    .line 1342105
    move-object v0, v3

    .line 1342106
    sput-object v0, Lcom/facebook/privacy/PrivacyOperationsClient;->k:Lcom/facebook/privacy/PrivacyOperationsClient;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1342107
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1342108
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1342109
    :cond_1
    sget-object v0, Lcom/facebook/privacy/PrivacyOperationsClient;->k:Lcom/facebook/privacy/PrivacyOperationsClient;

    return-object v0

    .line 1342110
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1342111
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/privacy/model/SelectablePrivacyData;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/privacy/model/SelectablePrivacyData;
    .locals 11

    .prologue
    .line 1342112
    iget-object v8, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1342113
    invoke-static {v8, p1}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/model/PrivacyOptionsResult;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)I

    move-result v0

    .line 1342114
    if-gez v0, :cond_0

    .line 1342115
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iget-object v1, v8, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1342116
    new-instance v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v2, v8, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    iget-object v3, v8, Lcom/facebook/privacy/model/PrivacyOptionsResult;->primaryOptionIndices:LX/0Px;

    iget-object v4, v8, Lcom/facebook/privacy/model/PrivacyOptionsResult;->expandablePrivacyOptionIndices:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    iget-object v6, v8, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget v7, v8, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOptionIndex:I

    iget-object v8, v8, Lcom/facebook/privacy/model/PrivacyOptionsResult;->recentPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lcom/facebook/privacy/model/PrivacyOptionsResult;-><init>(LX/0Px;LX/0Px;LX/0Px;LX/0Px;ILcom/facebook/graphql/model/GraphQLPrivacyOption;ILcom/facebook/graphql/model/GraphQLPrivacyOption;ZZ)V

    move-object v1, v0

    move-object v0, p1

    .line 1342117
    :goto_0
    new-instance v2, LX/8QV;

    invoke-direct {v2}, LX/8QV;-><init>()V

    .line 1342118
    iput-object v1, v2, LX/8QV;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1342119
    move-object v1, v2

    .line 1342120
    invoke-virtual {v1, v0}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-static {p1}, LX/2cA;->g(LX/1oS;)Z

    move-result v1

    .line 1342121
    iput-boolean v1, v0, LX/8QV;->c:Z

    .line 1342122
    move-object v0, v0

    .line 1342123
    iget-boolean v1, p0, Lcom/facebook/privacy/model/SelectablePrivacyData;->c:Z

    .line 1342124
    iput-boolean v1, v0, LX/8QV;->d:Z

    .line 1342125
    move-object v0, v0

    .line 1342126
    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    .line 1342127
    return-object v0

    .line 1342128
    :cond_0
    iget-object v1, v8, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v8

    goto :goto_0
.end method

.method private a(LX/1MF;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1MF;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1342129
    if-eqz p2, :cond_0

    sget-object v0, Lcom/facebook/privacy/PrivacyOperationsClient;->b:Ljava/util/Set;

    invoke-interface {p1}, LX/1MF;->getOperationType()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1342130
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dw;

    invoke-virtual {v0, p1}, LX/1dw;->a(LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1342131
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/privacy/PrivacyOperationsClient;LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1MF;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1342132
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/1MF;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/privacy/PrivacyOperationsClient;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/8QJ;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/8QW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1342133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1342134
    sget-object v1, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1342135
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1342136
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1342137
    new-instance v1, LX/8Pb;

    invoke-direct {v1, p0}, LX/8Pb;-><init>(Lcom/facebook/privacy/PrivacyOperationsClient;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/0Px;Ljava/lang/String;J)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;",
            ">;",
            "Ljava/lang/String;",
            "J)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1342138
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1342139
    const-string v0, "params"

    new-instance v1, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams;-><init>(LX/0Px;Ljava/lang/String;J)V

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1342140
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v1, "report_privacy_checkup_action"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0xb6ea498

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 1342141
    invoke-static {p0, v0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0rS;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/privacy/model/PrivacyOptionsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1342065
    sget-object v0, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-ne p1, v0, :cond_1

    .line 1342066
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->d:LX/2sP;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2sP;->a(Z)Lcom/facebook/privacy/model/PrivacyOptionsResult;

    move-result-object v0

    .line 1342067
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1342068
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1342069
    :goto_0
    return-object v0

    .line 1342070
    :cond_1
    sget-object v0, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-ne p1, v0, :cond_2

    .line 1342071
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 1342072
    :cond_2
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1342073
    const-string v0, "privacy.data_freshness"

    invoke-virtual {p1}, LX/0rS;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1342074
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v1, "fetch_privacy_options"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0x6c765b53

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 1342075
    invoke-static {p0, v0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/8Pa;

    invoke-direct {v1, p0}, LX/8Pa;-><init>(Lcom/facebook/privacy/PrivacyOperationsClient;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/5nc;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5nc;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1342142
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1342143
    const-string v8, "params"

    new-instance v0, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-object v1, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;-><init>(LX/5nc;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1342144
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v1, "report_inline_privacy_survey_action"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x6bacbdae

    move-object v2, v7

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 1342145
    invoke-static {p0, v0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/5nf;JZLjava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5nf;",
            "JZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1342060
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1342061
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1342062
    const-string v8, "params"

    new-instance v0, Lcom/facebook/privacy/protocol/ReportNASActionParams;

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/privacy/protocol/ReportNASActionParams;-><init>(LX/5nf;JZLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1342063
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v1, "report_nas_action"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x144d2e5c

    move-object v2, v7

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 1342064
    invoke-static {p0, v0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/privacy/protocol/EditReviewPrivacyParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/privacy/protocol/EditReviewPrivacyParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1342056
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1342057
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1342058
    const-string v0, "editReviewPrivacyParams"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1342059
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v1, "feed_edit_review_privacy"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x8e43a6

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/0Px;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;",
            ">;Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1342052
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1342053
    const-string v0, "params"

    new-instance v1, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams;

    invoke-direct {v1, p1, p2}, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams;-><init>(Ljava/lang/String;LX/0Px;)V

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1342054
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v1, "edit_objects_privacy_operation_type"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x6850c39e

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 1342055
    invoke-direct {p0, v0, p3}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/1MF;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/5mv;LX/5mu;JLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/5mv;",
            "LX/5mu;",
            "J",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1342048
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1342049
    const-string v8, "params"

    new-instance v0, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;-><init>(Ljava/lang/String;LX/5mv;LX/5mu;JLjava/lang/String;)V

    invoke-virtual {v7, v8, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1342050
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v1, "bulk_edit_album_privacy_operation_type"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x6ca52a0f

    move-object v2, v7

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 1342051
    invoke-static {p0, v0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/5nx;Ljava/lang/Long;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/5nx;",
            "Ljava/lang/Long;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1342042
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1342043
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1342044
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1342045
    const-string v0, "params"

    new-instance v1, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;

    invoke-direct {v1, p1, p2, p3}, Lcom/facebook/privacy/protocol/SetPrivacyEducationStateParams;-><init>(Ljava/lang/String;LX/5nx;Ljava/lang/Long;)V

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1342046
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v1, "set_privacy_education_state"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x3fcc6c43

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 1342047
    invoke-static {p0, v0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+",
            "LX/0jT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1342016
    new-instance v0, LX/4IX;

    invoke-direct {v0}, LX/4IX;-><init>()V

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4IX;->a(Ljava/util/List;)LX/4IX;

    move-result-object v0

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->c()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4IX;->b(Ljava/util/List;)LX/4IX;

    move-result-object v0

    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->b()Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    move-result-object v1

    .line 1342017
    sget-object v2, LX/8Pf;->a:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1342018
    const/4 v2, 0x0

    :goto_0
    move-object v1, v2

    .line 1342019
    invoke-virtual {v0, v1}, LX/4IX;->a(Ljava/lang/String;)LX/4IX;

    move-result-object v0

    .line 1342020
    invoke-virtual {p4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->d()Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    move-result-object v1

    .line 1342021
    sget-object v2, LX/8Pf;->b:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 1342022
    const/4 v2, 0x0

    :goto_1
    move-object v1, v2

    .line 1342023
    if-eqz v1, :cond_0

    .line 1342024
    const-string v2, "tag_expansion_state"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1342025
    move-object v0, v0

    .line 1342026
    :cond_0
    new-instance v1, LX/4IY;

    invoke-direct {v1}, LX/4IY;-><init>()V

    .line 1342027
    const-string v2, "node_id"

    invoke-virtual {v1, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1342028
    move-object v1, v1

    .line 1342029
    const-string v2, "privacy"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1342030
    move-object v0, v1

    .line 1342031
    new-instance v1, LX/5nM;

    invoke-direct {v1}, LX/5nM;-><init>()V

    move-object v1, v1

    .line 1342032
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/5nM;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 1342033
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 1342034
    if-nez p2, :cond_1

    if-nez p3, :cond_1

    move-object v0, v6

    .line 1342035
    :goto_2
    return-object v0

    :cond_1
    new-instance v0, LX/8Pe;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/8Pe;-><init>(Lcom/facebook/privacy/PrivacyOperationsClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    invoke-static {v6, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_2

    .line 1342036
    :pswitch_0
    const-string v2, "EVERYONE"

    goto :goto_0

    .line 1342037
    :pswitch_1
    const-string v2, "FRIENDS"

    goto :goto_0

    .line 1342038
    :pswitch_2
    const-string v2, "FRIENDS_OF_FRIENDS"

    goto :goto_0

    .line 1342039
    :pswitch_3
    const-string v2, "SELF"

    goto :goto_0

    .line 1342040
    :pswitch_4
    const-string v2, "TAGGEES"

    goto :goto_1

    .line 1342041
    :pswitch_5
    const-string v2, "UNSPECIFIED"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1342010
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1342011
    invoke-static {p1}, LX/3R3;->b(LX/1oU;)Ljava/lang/String;

    move-result-object v2

    .line 1342012
    if-nez v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Cannot set local sticky privacy because: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1342013
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/privacy/PrivacyOperationsClient$3;

    const-string v2, "PrivacyOperationsClient"

    const-string v3, "UpdateStickPrivacySettings"

    invoke-direct {v1, p0, v2, v3, p1}, Lcom/facebook/privacy/PrivacyOperationsClient$3;-><init>(Lcom/facebook/privacy/PrivacyOperationsClient;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    const v2, 0x703b5767

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1342014
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 1342015
    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1342000
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1342001
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1342002
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "privacy_options_client_sticky_no_privacy_json"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No GraphAPI representation for option: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1342003
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot find privacy option for option: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/1ML;->immediateFailedFuture(Ljava/lang/Throwable;)LX/1ML;

    move-result-object v0

    .line 1342004
    :goto_0
    return-object v0

    .line 1342005
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 1342006
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1342007
    const-string v0, "params"

    new-instance v1, Lcom/facebook/privacy/protocol/SetComposerStickyPrivacyParams;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/facebook/privacy/protocol/SetComposerStickyPrivacyParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1342008
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->c:LX/0aG;

    const-string v1, "set_composer_sticky_privacy"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v4, Lcom/facebook/privacy/PrivacyOperationsClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x72897a41

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    .line 1342009
    invoke-static {p0, v0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/privacy/PrivacyOperationsClient;LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 4

    .prologue
    .line 1341991
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1341992
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1341993
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "privacy_options_client_cached_no_privacy_json"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No GraphAPI representation for option: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1341994
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot find privacy option for option: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1341995
    :cond_0
    invoke-static {p1}, LX/3R3;->b(LX/1oU;)Ljava/lang/String;

    move-result-object v1

    .line 1341996
    if-nez v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot set cached privacy option because: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1341997
    iget-object v0, p0, Lcom/facebook/privacy/PrivacyOperationsClient;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/privacy/PrivacyOperationsClient$7;

    const-string v2, "PrivacyOperationsClient"

    const-string v3, "UpdateSelectedPrivacySettings"

    invoke-direct {v1, p0, v2, v3, p1}, Lcom/facebook/privacy/PrivacyOperationsClient$7;-><init>(Lcom/facebook/privacy/PrivacyOperationsClient;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    const v2, -0x6d7c2b7d

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1341998
    return-void

    .line 1341999
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
