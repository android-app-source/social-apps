.class public Lcom/facebook/privacy/nux/PrivacyEducationBannerConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1343374
    const-class v0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;

    new-instance v1, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1343375
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1343376
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1343377
    if-nez p0, :cond_0

    .line 1343378
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1343379
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1343380
    invoke-static {p0, p1, p2}, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfigSerializer;->b(Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;LX/0nX;LX/0my;)V

    .line 1343381
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1343382
    return-void
.end method

.method private static b(Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1343383
    const-string v0, "seen_counts"

    iget-object v1, p0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;->mSeenCounts:Ljava/util/Map;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1343384
    const-string v0, "banner_expanded"

    iget-object v1, p0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;->mBannersExpanded:Ljava/util/Map;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1343385
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1343386
    check-cast p1, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfigSerializer;->a(Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;LX/0nX;LX/0my;)V

    return-void
.end method
