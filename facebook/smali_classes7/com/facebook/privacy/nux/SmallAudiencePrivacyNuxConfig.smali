.class public Lcom/facebook/privacy/nux/SmallAudiencePrivacyNuxConfig;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/nux/SmallAudiencePrivacyNuxConfigDeserializer;
.end annotation


# instance fields
.field public final includedCount:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "included_count"
    .end annotation
.end field

.field public final previousPrivacy:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "previous_privacy"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1343409
    const-class v0, Lcom/facebook/privacy/nux/SmallAudiencePrivacyNuxConfigDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1343410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1343411
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/privacy/nux/SmallAudiencePrivacyNuxConfig;->includedCount:I

    .line 1343412
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/privacy/nux/SmallAudiencePrivacyNuxConfig;->previousPrivacy:Ljava/lang/String;

    .line 1343413
    return-void
.end method
