.class public Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/nux/PrivacyEducationBannerConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/nux/PrivacyEducationBannerConfigSerializer;
.end annotation


# instance fields
.field public mBannersExpanded:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "banner_expanded"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public mSeenCounts:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "seen_counts"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1343347
    const-class v0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1343346
    const-class v0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfigSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1343348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1343349
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;->mSeenCounts:Ljava/util/Map;

    .line 1343350
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;->mBannersExpanded:Ljava/util/Map;

    .line 1343351
    return-void
.end method


# virtual methods
.method public final a(LX/8Qa;)I
    .locals 2

    .prologue
    .line 1343343
    iget-object v0, p0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;->mSeenCounts:Ljava/util/Map;

    invoke-virtual {p1}, LX/8Qa;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1343344
    iget-object v0, p0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;->mSeenCounts:Ljava/util/Map;

    invoke-virtual {p1}, LX/8Qa;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1343345
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/8Qa;I)V
    .locals 3

    .prologue
    .line 1343341
    iget-object v0, p0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;->mSeenCounts:Ljava/util/Map;

    invoke-virtual {p1}, LX/8Qa;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1343342
    return-void
.end method

.method public final a(LX/8Qa;Z)V
    .locals 3

    .prologue
    .line 1343339
    iget-object v0, p0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;->mBannersExpanded:Ljava/util/Map;

    invoke-virtual {p1}, LX/8Qa;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1343340
    return-void
.end method

.method public final b(LX/8Qa;)Z
    .locals 2

    .prologue
    .line 1343336
    iget-object v0, p0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;->mBannersExpanded:Ljava/util/Map;

    invoke-virtual {p1}, LX/8Qa;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1343337
    iget-object v0, p0, Lcom/facebook/privacy/nux/PrivacyEducationBannerConfig;->mBannersExpanded:Ljava/util/Map;

    invoke-virtual {p1}, LX/8Qa;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1343338
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
