.class public Lcom/facebook/nux/status/UpdateNuxStatusParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/nux/status/UpdateNuxStatusParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:LX/8DK;

.field public final e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1312492
    new-instance v0, LX/8DJ;

    invoke-direct {v0}, LX/8DJ;-><init>()V

    sput-object v0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1312493
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1312494
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->a:Ljava/lang/String;

    .line 1312495
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->b:Ljava/lang/String;

    .line 1312496
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->c:Ljava/lang/String;

    .line 1312497
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/8DK;->valueOf(Ljava/lang/String;)LX/8DK;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->d:LX/8DK;

    .line 1312498
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->e:LX/0P1;

    .line 1312499
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8DK;LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/8DK;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1312500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1312501
    iput-object p1, p0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->a:Ljava/lang/String;

    .line 1312502
    iput-object p2, p0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->b:Ljava/lang/String;

    .line 1312503
    iput-object p3, p0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->c:Ljava/lang/String;

    .line 1312504
    iput-object p4, p0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->d:LX/8DK;

    .line 1312505
    iput-object p5, p0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->e:LX/0P1;

    .line 1312506
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1312507
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1312508
    iget-object v0, p0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1312509
    iget-object v0, p0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1312510
    iget-object v0, p0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1312511
    iget-object v0, p0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->d:LX/8DK;

    invoke-virtual {v0}, LX/8DK;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1312512
    iget-object v0, p0, Lcom/facebook/nux/status/UpdateNuxStatusParams;->e:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1312513
    return-void
.end method
