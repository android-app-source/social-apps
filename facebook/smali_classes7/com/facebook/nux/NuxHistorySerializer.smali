.class public Lcom/facebook/nux/NuxHistorySerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/nux/NuxHistory;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1312280
    const-class v0, Lcom/facebook/nux/NuxHistory;

    new-instance v1, Lcom/facebook/nux/NuxHistorySerializer;

    invoke-direct {v1}, Lcom/facebook/nux/NuxHistorySerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1312281
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1312279
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/nux/NuxHistory;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1312273
    if-nez p0, :cond_0

    .line 1312274
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1312275
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1312276
    invoke-static {p0, p1, p2}, Lcom/facebook/nux/NuxHistorySerializer;->b(Lcom/facebook/nux/NuxHistory;LX/0nX;LX/0my;)V

    .line 1312277
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1312278
    return-void
.end method

.method private static b(Lcom/facebook/nux/NuxHistory;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1312269
    const-string v0, "lastAppearanceTime"

    iget-wide v2, p0, Lcom/facebook/nux/NuxHistory;->lastAppearanceTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1312270
    const-string v0, "isCompleted"

    iget-boolean v1, p0, Lcom/facebook/nux/NuxHistory;->isCompleted:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1312271
    const-string v0, "numAppearances"

    iget-wide v2, p0, Lcom/facebook/nux/NuxHistory;->numAppearances:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1312272
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1312268
    check-cast p1, Lcom/facebook/nux/NuxHistory;

    invoke-static {p1, p2, p3}, Lcom/facebook/nux/NuxHistorySerializer;->a(Lcom/facebook/nux/NuxHistory;LX/0nX;LX/0my;)V

    return-void
.end method
