.class public Lcom/facebook/nux/ui/NuxBubbleView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final e:LX/0wT;


# instance fields
.field public a:Z

.field public b:LX/8DO;

.field public c:LX/7gC;

.field private d:LX/4mU;

.field private f:LX/0wW;

.field private g:LX/0wd;

.field private h:I

.field private i:I

.field private j:I

.field private k:Landroid/view/LayoutInflater;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/LinearLayout;

.field private o:Landroid/widget/ImageView;

.field public p:LX/7zs;

.field private final q:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1312536
    const-wide/high16 v0, 0x4044000000000000L    # 40.0

    const-wide/high16 v2, 0x401c000000000000L    # 7.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/nux/ui/NuxBubbleView;->e:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1312534
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/nux/ui/NuxBubbleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1312535
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1312598
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1312599
    sget-object v0, LX/8DO;->GONE:LX/8DO;

    iput-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->b:LX/8DO;

    .line 1312600
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->c:LX/7gC;

    .line 1312601
    const-class v0, Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-static {v0, p0}, Lcom/facebook/nux/ui/NuxBubbleView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1312602
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->k:Landroid/view/LayoutInflater;

    const v1, 0x7f030c41

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1312603
    const v0, 0x7f0d1df5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->l:Landroid/widget/TextView;

    .line 1312604
    const v0, 0x7f0d1df6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->m:Landroid/widget/TextView;

    .line 1312605
    const v0, 0x7f0d1df4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->n:Landroid/widget/LinearLayout;

    .line 1312606
    sget-object v0, LX/03r;->NuxBubbleView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1312607
    const/16 v0, 0x2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->h:I

    .line 1312608
    iget v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->h:I

    invoke-virtual {p0, v0}, Lcom/facebook/nux/ui/NuxBubbleView;->setNubPosition(I)V

    .line 1312609
    const/16 v0, 0x3

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->i:I

    .line 1312610
    const/16 v0, 0x4

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->j:I

    .line 1312611
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->o:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1312612
    iget v2, p0, Lcom/facebook/nux/ui/NuxBubbleView;->i:I

    packed-switch v2, :pswitch_data_0

    .line 1312613
    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1312614
    :goto_0
    const/16 v0, 0x1

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 1312615
    iget-object v2, p0, Lcom/facebook/nux/ui/NuxBubbleView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v2

    .line 1312616
    packed-switch v0, :pswitch_data_1

    .line 1312617
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/facebook/nux/ui/NuxBubbleView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02198b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1312618
    const v0, 0x7f0d1df7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/nux/ui/NuxBubbleView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02198d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1312619
    const v0, 0x7f0d1df8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/nux/ui/NuxBubbleView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02198c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1312620
    :goto_1
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->n:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/facebook/nux/ui/NuxBubbleView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/nux/ui/NuxBubbleView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/nux/ui/NuxBubbleView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 1312621
    const/16 v0, 0x0

    invoke-static {p1, v1, v0}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v0

    .line 1312622
    iget-object v2, p0, Lcom/facebook/nux/ui/NuxBubbleView;->m:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1312623
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1312624
    new-instance v0, LX/8DL;

    invoke-direct {v0, p0}, LX/8DL;-><init>(Lcom/facebook/nux/ui/NuxBubbleView;)V

    iput-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->q:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1312625
    return-void

    .line 1312626
    :pswitch_0
    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    or-int/lit8 v2, v2, 0x3

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1312627
    iget v2, p0, Lcom/facebook/nux/ui/NuxBubbleView;->j:I

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    goto :goto_0

    .line 1312628
    :pswitch_1
    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    or-int/lit8 v2, v2, 0x5

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1312629
    iget v2, p0, Lcom/facebook/nux/ui/NuxBubbleView;->j:I

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_0

    .line 1312630
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/facebook/nux/ui/NuxBubbleView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f021988

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1312631
    const v0, 0x7f0d1df7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/nux/ui/NuxBubbleView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02198a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1312632
    const v0, 0x7f0d1df8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/nux/ui/NuxBubbleView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f021989

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method private a(LX/0wW;Landroid/view/LayoutInflater;LX/4mV;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1312633
    iput-object p1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->f:LX/0wW;

    .line 1312634
    iput-object p2, p0, Lcom/facebook/nux/ui/NuxBubbleView;->k:Landroid/view/LayoutInflater;

    .line 1312635
    invoke-virtual {p3, p0}, LX/4mV;->a(Landroid/view/View;)LX/4mU;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->d:LX/4mU;

    .line 1312636
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/nux/ui/NuxBubbleView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/nux/ui/NuxBubbleView;

    invoke-static {v2}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-static {v2}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    invoke-static {v2}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object v2

    check-cast v2, LX/4mV;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/nux/ui/NuxBubbleView;->a(LX/0wW;Landroid/view/LayoutInflater;LX/4mV;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/nux/ui/NuxBubbleView;F)V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 1312637
    float-to-double v0, p1

    const-wide v4, 0x3fee666660000000L    # 0.949999988079071

    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1312638
    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->d:LX/4mU;

    invoke-virtual {v1, v0}, LX/4mU;->e(F)V

    .line 1312639
    float-to-double v0, p1

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1312640
    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->d:LX/4mU;

    invoke-virtual {v1, v0}, LX/4mU;->a(F)V

    .line 1312641
    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->d:LX/4mU;

    invoke-virtual {v1, v0}, LX/4mU;->c(F)V

    .line 1312642
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1312592
    sget-object v0, LX/8DO;->REVEALING:LX/8DO;

    iput-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->b:LX/8DO;

    .line 1312593
    invoke-virtual {p0}, Lcom/facebook/nux/ui/NuxBubbleView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1312594
    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->q:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1312595
    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->q:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1312596
    return-void
.end method

.method private h()LX/0wd;
    .locals 3

    .prologue
    .line 1312643
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->f:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/nux/ui/NuxBubbleView;->e:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    new-instance v1, LX/8DN;

    invoke-direct {v1, p0}, LX/8DN;-><init>(Lcom/facebook/nux/ui/NuxBubbleView;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    .line 1312644
    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->c:LX/7gC;

    if-eqz v1, :cond_0

    .line 1312645
    new-instance v1, LX/8DM;

    invoke-direct {v1, p0}, LX/8DM;-><init>(Lcom/facebook/nux/ui/NuxBubbleView;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1312646
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1312647
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->n:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1312648
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1312649
    sget-object v0, LX/8DO;->REVEALING:LX/8DO;

    iput-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->b:LX/8DO;

    .line 1312650
    iget-boolean v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->a:Z

    if-nez v0, :cond_0

    .line 1312651
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p0, v0}, Lcom/facebook/nux/ui/NuxBubbleView;->a$redex0(Lcom/facebook/nux/ui/NuxBubbleView;F)V

    .line 1312652
    :goto_0
    return-void

    .line 1312653
    :cond_0
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->g:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1312654
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1312655
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 1312656
    sget-object v0, LX/8DO;->DISMISSING:LX/8DO;

    iput-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->b:LX/8DO;

    .line 1312657
    iget-boolean v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->a:Z

    if-nez v0, :cond_0

    .line 1312658
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/nux/ui/NuxBubbleView;->a$redex0(Lcom/facebook/nux/ui/NuxBubbleView;F)V

    .line 1312659
    :goto_0
    return-void

    .line 1312660
    :cond_0
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->g:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0
.end method

.method public final f()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 1312661
    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->g:LX/0wd;

    if-nez v1, :cond_1

    .line 1312662
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->g:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->g:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->d()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->g:LX/0wd;

    .line 1312663
    iget-wide v6, v1, LX/0wd;->l:D

    move-wide v4, v6

    .line 1312664
    cmpg-double v1, v2, v4

    if-ltz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBubbleLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
    .locals 1

    .prologue
    .line 1312665
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    return-object v0
.end method

.method public getBubbleRightPadding()I
    .locals 1

    .prologue
    .line 1312666
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v0

    return v0
.end method

.method public getNubRightMargin()I
    .locals 2

    .prologue
    .line 1312667
    iget v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->i:I

    packed-switch v0, :pswitch_data_0

    .line 1312668
    invoke-virtual {p0}, Lcom/facebook/nux/ui/NuxBubbleView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    :goto_0
    return v0

    .line 1312669
    :pswitch_0
    iget v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->j:I

    goto :goto_0

    .line 1312670
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/nux/ui/NuxBubbleView;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->j:I

    sub-int/2addr v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getPointerHeight()I
    .locals 1

    .prologue
    .line 1312597
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->o:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getPointerLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
    .locals 1

    .prologue
    .line 1312591
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->o:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    return-object v0
.end method

.method public getPointerWidth()I
    .locals 1

    .prologue
    .line 1312590
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->o:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, -0x33b8c05e    # -5.2231816E7f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1312581
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1312582
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->a:Z

    .line 1312583
    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->g:LX/0wd;

    if-eqz v1, :cond_0

    .line 1312584
    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->g:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->a()V

    .line 1312585
    :cond_0
    invoke-direct {p0}, Lcom/facebook/nux/ui/NuxBubbleView;->h()LX/0wd;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->g:LX/0wd;

    .line 1312586
    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->g:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1312587
    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->g:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->j()LX/0wd;

    .line 1312588
    invoke-direct {p0}, Lcom/facebook/nux/ui/NuxBubbleView;->g()V

    .line 1312589
    const/16 v1, 0x2d

    const v2, -0x6f208486

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x68fa79a1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1312574
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1312575
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->a:Z

    .line 1312576
    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->g:LX/0wd;

    if-eqz v1, :cond_0

    .line 1312577
    iget-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->g:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->a()V

    .line 1312578
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->g:LX/0wd;

    .line 1312579
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/nux/ui/NuxBubbleView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/nux/ui/NuxBubbleView;->q:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1312580
    const/16 v1, 0x2d

    const v2, 0x6dfbecca

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 1

    .prologue
    .line 1312570
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1312571
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->c:LX/7gC;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->o:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1312572
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->c:LX/7gC;

    invoke-interface {v0}, LX/7gC;->a()V

    .line 1312573
    :cond_0
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, -0x62a0320d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1312559
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomFrameLayout;->onSizeChanged(IIII)V

    .line 1312560
    iget v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->h:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    .line 1312561
    :goto_0
    iget v2, p0, Lcom/facebook/nux/ui/NuxBubbleView;->i:I

    packed-switch v2, :pswitch_data_0

    .line 1312562
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unsupported nub alignment"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    const v2, -0x301c49b1

    invoke-static {v2, v1}, LX/02F;->g(II)V

    throw v0

    .line 1312563
    :cond_0
    int-to-float v0, p2

    goto :goto_0

    .line 1312564
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/nux/ui/NuxBubbleView;->d:LX/4mU;

    int-to-float v3, p1

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual {v2, v3, v0}, LX/4mU;->a(FF)V

    .line 1312565
    :goto_1
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->p:LX/7zs;

    if-eqz v0, :cond_1

    .line 1312566
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->p:LX/7zs;

    invoke-interface {v0}, LX/7zs;->a()V

    .line 1312567
    :cond_1
    const v0, -0x26f95bb6

    invoke-static {v0, v1}, LX/02F;->g(II)V

    return-void

    .line 1312568
    :pswitch_1
    iget-object v2, p0, Lcom/facebook/nux/ui/NuxBubbleView;->d:LX/4mU;

    iget v3, p0, Lcom/facebook/nux/ui/NuxBubbleView;->j:I

    sub-int v3, p1, v3

    int-to-float v3, v3

    invoke-virtual {v2, v3, v0}, LX/4mU;->a(FF)V

    goto :goto_1

    .line 1312569
    :pswitch_2
    iget-object v2, p0, Lcom/facebook/nux/ui/NuxBubbleView;->d:LX/4mU;

    iget v3, p0, Lcom/facebook/nux/ui/NuxBubbleView;->j:I

    int-to-float v3, v3

    invoke-virtual {v2, v3, v0}, LX/4mU;->a(FF)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setBubbleBody(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1312557
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->m:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1312558
    return-void
.end method

.method public setBubbleParams(Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 1

    .prologue
    .line 1312555
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1312556
    return-void
.end method

.method public setBubbleTitle(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1312552
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1312553
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->l:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1312554
    return-void
.end method

.method public setListener(LX/7gC;)V
    .locals 0

    .prologue
    .line 1312550
    iput-object p1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->c:LX/7gC;

    .line 1312551
    return-void
.end method

.method public setNubPosition(I)V
    .locals 2

    .prologue
    .line 1312543
    iput p1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->h:I

    .line 1312544
    iget v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->h:I

    packed-switch v0, :pswitch_data_0

    .line 1312545
    const v0, 0x7f0d1df8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->o:Landroid/widget/ImageView;

    .line 1312546
    invoke-virtual {p0}, Lcom/facebook/nux/ui/NuxBubbleView;->getBubbleLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    const/16 v1, 0x51

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1312547
    :goto_0
    return-void

    .line 1312548
    :pswitch_0
    const v0, 0x7f0d1df7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->o:Landroid/widget/ImageView;

    .line 1312549
    invoke-virtual {p0}, Lcom/facebook/nux/ui/NuxBubbleView;->getBubbleLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    const/16 v1, 0x31

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 1312541
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1312542
    return-void
.end method

.method public setPointerParams(Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 1

    .prologue
    .line 1312539
    iget-object v0, p0, Lcom/facebook/nux/ui/NuxBubbleView;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1312540
    return-void
.end method

.method public setSizeChangeListener(LX/7zs;)V
    .locals 0
    .param p1    # LX/7zs;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1312537
    iput-object p1, p0, Lcom/facebook/nux/ui/NuxBubbleView;->p:LX/7zs;

    .line 1312538
    return-void
.end method
