.class public Lcom/facebook/nux/NuxHistory;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/nux/NuxHistoryDeserializer;
.end annotation


# instance fields
.field private a:J

.field private b:I

.field public isCompleted:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "isCompleted"
    .end annotation
.end field

.field public lastAppearanceTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "lastAppearanceTime"
    .end annotation
.end field

.field public numAppearances:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "numAppearances"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1312242
    const-class v0, Lcom/facebook/nux/NuxHistoryDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1312232
    const-class v0, Lcom/facebook/nux/NuxHistorySerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1312240
    const-wide/16 v0, 0x0

    invoke-direct {p0, v2, v0, v1, v2}, Lcom/facebook/nux/NuxHistory;-><init>(IJZ)V

    .line 1312241
    return-void
.end method

.method private constructor <init>(IJZ)V
    .locals 2

    .prologue
    .line 1312233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1312234
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/facebook/nux/NuxHistory;->numAppearances:J

    .line 1312235
    iput-wide p2, p0, Lcom/facebook/nux/NuxHistory;->lastAppearanceTime:J

    .line 1312236
    iput-boolean p4, p0, Lcom/facebook/nux/NuxHistory;->isCompleted:Z

    .line 1312237
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/nux/NuxHistory;->b:I

    .line 1312238
    const-wide/32 v0, 0x927c0

    iput-wide v0, p0, Lcom/facebook/nux/NuxHistory;->a:J

    .line 1312239
    return-void
.end method


# virtual methods
.method public final a(J)Lcom/facebook/nux/NuxHistory;
    .locals 1

    .prologue
    .line 1312243
    iput-wide p1, p0, Lcom/facebook/nux/NuxHistory;->lastAppearanceTime:J

    .line 1312244
    return-object p0
.end method

.method public final a(LX/0SG;)Lcom/facebook/nux/NuxHistory;
    .locals 2

    .prologue
    .line 1312221
    invoke-interface {p1}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/nux/NuxHistory;->lastAppearanceTime:J

    .line 1312222
    return-object p0
.end method

.method public final a(Z)Lcom/facebook/nux/NuxHistory;
    .locals 0

    .prologue
    .line 1312223
    iput-boolean p1, p0, Lcom/facebook/nux/NuxHistory;->isCompleted:Z

    .line 1312224
    return-object p0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 1312225
    iget-wide v0, p0, Lcom/facebook/nux/NuxHistory;->numAppearances:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/facebook/nux/NuxHistory;->numAppearances:J

    .line 1312226
    return-void
.end method

.method public final b(LX/0SG;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1312227
    iget-boolean v1, p0, Lcom/facebook/nux/NuxHistory;->isCompleted:Z

    if-eqz v1, :cond_1

    .line 1312228
    :cond_0
    :goto_0
    return v0

    .line 1312229
    :cond_1
    iget-wide v2, p0, Lcom/facebook/nux/NuxHistory;->numAppearances:J

    iget v1, p0, Lcom/facebook/nux/NuxHistory;->b:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 1312230
    invoke-interface {p1}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/facebook/nux/NuxHistory;->lastAppearanceTime:J

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lcom/facebook/nux/NuxHistory;->a:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 1312231
    const/4 v0, 0x1

    goto :goto_0
.end method
