.class public Lcom/facebook/webrtc/MediaInterface;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/facebook/jni/HybridData;)V
    .locals 0
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1210635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1210636
    iput-object p1, p0, Lcom/facebook/webrtc/MediaInterface;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1210637
    return-void
.end method

.method private native performAudioFileMixing(Ljava/lang/String;I)V
.end method

.method private native sendIntraFrame()V
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end method

.method private native setAcsCodec(I)V
.end method
