.class public Lcom/facebook/webrtc/WebrtcEngine;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1210654
    const-class v0, Lcom/facebook/webrtc/WebrtcEngine;

    sput-object v0, Lcom/facebook/webrtc/WebrtcEngine;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;Lcom/facebook/webrtc/IWebrtcUiInterface;Lcom/facebook/webrtc/IWebrtcConfigInterface;Lcom/facebook/webrtc/IWebrtcLoggingInterface;Lcom/facebook/webrtc/IWebrtcCallMonitorInterface;Lcom/facebook/webrtc/ConferenceCall$Listener;)V
    .locals 7

    .prologue
    .line 1210655
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1210656
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1210657
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1210658
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1210659
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1210660
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1210661
    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1210662
    const-string v0, "rtc"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    move-object v0, p2

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p1

    move-object v6, p7

    .line 1210663
    invoke-static/range {v0 .. v6}, Lcom/facebook/webrtc/WebrtcEngine;->initHybrid(Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;Lcom/facebook/webrtc/IWebrtcUiInterface;Lcom/facebook/webrtc/IWebrtcConfigInterface;Lcom/facebook/webrtc/IWebrtcLoggingInterface;Lcom/facebook/webrtc/IWebrtcCallMonitorInterface;Landroid/content/Context;Lcom/facebook/webrtc/ConferenceCall$Listener;)Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/webrtc/WebrtcEngine;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1210664
    return-void
.end method

.method private static native initHybrid(Lcom/facebook/webrtc/IWebrtcSignalingMessageInterface;Lcom/facebook/webrtc/IWebrtcUiInterface;Lcom/facebook/webrtc/IWebrtcConfigInterface;Lcom/facebook/webrtc/IWebrtcLoggingInterface;Lcom/facebook/webrtc/IWebrtcCallMonitorInterface;Landroid/content/Context;Lcom/facebook/webrtc/ConferenceCall$Listener;)Lcom/facebook/jni/HybridData;
.end method

.method private native setAudioOutputRoute(I)V
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1210665
    iget-object v0, p0, Lcom/facebook/webrtc/WebrtcEngine;->mHybridData:Lcom/facebook/jni/HybridData;

    if-eqz v0, :cond_0

    .line 1210666
    iget-object v0, p0, Lcom/facebook/webrtc/WebrtcEngine;->mHybridData:Lcom/facebook/jni/HybridData;

    invoke-virtual {v0}, Lcom/facebook/jni/HybridData;->resetNative()V

    .line 1210667
    :cond_0
    return-void
.end method

.method public final a(LX/7TP;)V
    .locals 1

    .prologue
    .line 1210668
    invoke-virtual {p1}, LX/7TP;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/webrtc/WebrtcEngine;->setAudioOutputRoute(I)V

    .line 1210669
    return-void
.end method

.method public native acceptCall(Ljava/lang/String;ZZZ)V
.end method

.method public native createConferenceHandle(Ljava/lang/String;Ljava/lang/String;[B)Lcom/facebook/webrtc/ConferenceCall;
.end method

.method public native endCall(JI)V
.end method

.method public native getMediaCaptureSink()Lcom/facebook/webrtc/MediaCaptureSink;
.end method

.method public native getMediaInterface()Lcom/facebook/webrtc/MediaInterface;
.end method

.method public native handleMultiwaySignalingMessage([B)V
.end method

.method public native isEndToEndEncrypted()Z
.end method

.method public native makeKeyPairAndCertificate()Ljava/lang/String;
.end method

.method public native onMessageSendError(JJILjava/lang/String;Ljava/lang/String;)V
.end method

.method public native onMessageSendSuccess(JJ)V
.end method

.method public native onMultiwayMessageSendError(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
.end method

.method public native onMultiwayMessageSendSuccess(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public native onThriftMessageFromPeer([BLjava/lang/String;)V
.end method

.method public native sendData(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public native sendDiscoveryRequest(J)V
.end method

.method public native sendDiscoveryResponse(JJLjava/lang/String;)V
.end method

.method public native sendEscalationRequest(ZLjava/lang/String;)V
.end method

.method public native sendEscalationResponse(ZLjava/lang/String;)V
.end method

.method public native sendEscalationSuccess()V
.end method

.method public native sendGameCommand([B)V
.end method

.method public native setAudioOn(Z)V
.end method

.method public native setBluetoothState(Z)V
.end method

.method public native setCameraId(Ljava/lang/String;)V
.end method

.method public native setDebugSwitch(Ljava/lang/String;Z)V
.end method

.method public native setMediaState(ZZZ)V
.end method

.method public native setRendererWindow(JLandroid/view/View;)V
.end method

.method public native setSelfRendererWindow(Landroid/view/SurfaceView;)V
.end method

.method public native setSpeakerOn(Z)V
.end method

.method public native setSupportedCallTypes([Ljava/lang/String;)V
.end method

.method public native setUseAppLevelCamera(Z)V
.end method

.method public native setVideoOn(Z)V
.end method

.method public native setVideoParameters(IIIIII)V
.end method

.method public native startCall(JLjava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;[B)V
.end method

.method public native startCustomCallToDevice(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZJLjava/lang/String;ZLjava/lang/String;[B)V
.end method
