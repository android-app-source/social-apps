.class public Lcom/facebook/video/videostreaming/LiveStreamingError;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public final a:Ljava/lang/Exception;

.field public final descripton:Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final domain:Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final errorCode:I
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final fullDescription:Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final reason:Ljava/lang/String;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1207015
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1207016
    iput p1, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->errorCode:I

    .line 1207017
    iput-object p2, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->domain:Ljava/lang/String;

    .line 1207018
    iput-object p3, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->reason:Ljava/lang/String;

    .line 1207019
    iput-object p4, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->descripton:Ljava/lang/String;

    .line 1207020
    iput-object p5, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->fullDescription:Ljava/lang/String;

    .line 1207021
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->a:Ljava/lang/Exception;

    .line 1207022
    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 1207023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1207024
    iput-object p1, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->a:Ljava/lang/Exception;

    .line 1207025
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->errorCode:I

    .line 1207026
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->domain:Ljava/lang/String;

    .line 1207027
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->reason:Ljava/lang/String;

    .line 1207028
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->descripton:Ljava/lang/String;

    .line 1207029
    invoke-static {p1}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->fullDescription:Ljava/lang/String;

    .line 1207030
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1207031
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1207032
    const-string v1, "Error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1207033
    iget v1, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->errorCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1207034
    const-string v1, ", Domain:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1207035
    iget-object v1, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->domain:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1207036
    const-string v1, ", Reason:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1207037
    iget-object v1, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->reason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1207038
    const-string v1, ", Description:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1207039
    iget-object v1, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->descripton:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1207040
    const-string v1, ", Full Description:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1207041
    iget-object v1, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->fullDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1207042
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
