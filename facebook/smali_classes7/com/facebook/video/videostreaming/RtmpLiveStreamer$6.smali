.class public final Lcom/facebook/video/videostreaming/RtmpLiveStreamer$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V
    .locals 0

    .prologue
    .line 1207222
    iput-object p1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$6;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 1207223
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$6;->a:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 1207224
    move v9, v1

    move-object v1, v7

    .line 1207225
    :goto_0
    const/4 v2, 0x3

    if-ge v9, v2, :cond_4

    .line 1207226
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->rtmpPublishUrl:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1207227
    :cond_0
    const/4 v2, 0x1

    :try_start_0
    invoke-static {v0, v2}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;Z)Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;
    :try_end_0
    .catch LX/2Oo; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v8

    .line 1207228
    if-eqz v8, :cond_2

    :try_start_1
    iget-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->J:LX/0Ac;

    iget-object v2, v8, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0Ac;->a(Ljava/lang/String;)LX/0Ab;

    move-result-object v1

    :goto_1
    iput-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->L:LX/0Ab;

    .line 1207229
    invoke-virtual {v8}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->a()LX/0lF;

    move-result-object v1

    .line 1207230
    instance-of v2, v1, LX/0m9;

    if-eqz v2, :cond_5

    .line 1207231
    check-cast v1, LX/0m9;

    .line 1207232
    const-string v2, "stream_network_speed_test_payload_timeout_in_seconds"

    new-instance v3, LX/0rQ;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, LX/0rQ;-><init>(I)V

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;LX/0lF;)LX/0lF;

    move-object v3, v1

    .line 1207233
    :goto_2
    new-instance v1, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    iget-object v4, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->j:LX/29a;

    iget-object v5, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->D:Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

    iget-object v6, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->L:LX/0Ab;

    move-object v2, v0

    invoke-direct/range {v1 .. v6}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;-><init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;LX/0lF;LX/29a;Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;LX/0Ab;)V

    iput-object v1, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;
    :try_end_1
    .catch LX/2Oo; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v7

    move-object v2, v8

    .line 1207234
    :goto_3
    iget-object v3, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->m:LX/1bK;

    if-eqz v3, :cond_1

    .line 1207235
    iget-object v3, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->w:Landroid/os/Handler;

    new-instance v4, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$5;

    invoke-direct {v4, v0, v2, v1}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$5;-><init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;Lcom/facebook/http/protocol/ApiErrorResult;)V

    const v1, 0x6e3295fa

    invoke-static {v3, v4, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1207236
    :cond_1
    return-void

    :cond_2
    move-object v1, v7

    .line 1207237
    goto :goto_1

    .line 1207238
    :catch_0
    move-exception v1

    move-object v3, v1

    move-object v2, v8

    .line 1207239
    :goto_4
    invoke-virtual {v3}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    .line 1207240
    sget-object v4, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a:Ljava/lang/String;

    const-string v5, "Unable to retrieve broadcast ID. "

    invoke-static {v4, v5, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 1207241
    :catch_1
    move-exception v1

    move-object v2, v1

    move-object v1, v8

    .line 1207242
    :goto_5
    sget-object v3, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a:Ljava/lang/String;

    const-string v4, "Unable to retrieve broadcast ID. "

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1207243
    const/16 v2, 0xa

    shl-int/2addr v2, v9

    int-to-long v3, v2

    :try_start_2
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1207244
    :cond_3
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_0

    .line 1207245
    :catch_2
    move-exception v2

    .line 1207246
    sget-object v3, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a:Ljava/lang/String;

    const-string v4, "Thread.sleep() threw InterruptedException "

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v2, v1

    move-object v1, v7

    .line 1207247
    goto :goto_3

    .line 1207248
    :catch_3
    move-exception v2

    goto :goto_5

    .line 1207249
    :catch_4
    move-exception v2

    move-object v3, v2

    move-object v2, v1

    goto :goto_4

    :cond_4
    move-object v2, v1

    move-object v1, v7

    goto :goto_3

    :cond_5
    move-object v3, v1

    goto :goto_2
.end method
