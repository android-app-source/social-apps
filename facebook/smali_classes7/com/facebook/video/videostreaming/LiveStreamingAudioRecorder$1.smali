.class public final Lcom/facebook/video/videostreaming/LiveStreamingAudioRecorder$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/7RU;


# direct methods
.method public constructor <init>(LX/7RU;)V
    .locals 0

    .prologue
    .line 1206749
    iput-object p1, p0, Lcom/facebook/video/videostreaming/LiveStreamingAudioRecorder$1;->a:LX/7RU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1206750
    const/16 v0, 0x1000

    :try_start_0
    new-array v3, v0, [B

    .line 1206751
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videostreaming/LiveStreamingAudioRecorder$1;->a:LX/7RU;

    iget-object v0, v0, LX/7RU;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/video/videostreaming/LiveStreamingAudioRecorder$1;->a:LX/7RU;

    iget-object v0, v0, LX/7RU;->a:Landroid/media/AudioRecord;

    if-eqz v0, :cond_2

    .line 1206752
    iget-object v0, p0, Lcom/facebook/video/videostreaming/LiveStreamingAudioRecorder$1;->a:LX/7RU;

    iget-object v0, v0, LX/7RU;->a:Landroid/media/AudioRecord;

    const/4 v1, 0x0

    const/16 v4, 0x1000

    invoke-virtual {v0, v3, v1, v4}, Landroid/media/AudioRecord;->read([BII)I

    move-result v4

    .line 1206753
    iget-object v0, p0, Lcom/facebook/video/videostreaming/LiveStreamingAudioRecorder$1;->a:LX/7RU;

    iget-object v0, v0, LX/7RU;->h:LX/1bJ;

    if-eqz v0, :cond_1

    if-lez v4, :cond_1

    .line 1206754
    iget-object v0, p0, Lcom/facebook/video/videostreaming/LiveStreamingAudioRecorder$1;->a:LX/7RU;

    iget-object v0, v0, LX/7RU;->h:LX/1bJ;

    invoke-static {v3, v4}, LX/7RU;->b([BI)D

    move-result-wide v6

    invoke-interface {v0, v6, v7}, LX/1bJ;->a(D)V

    .line 1206755
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videostreaming/LiveStreamingAudioRecorder$1;->a:LX/7RU;

    iget-object v0, v0, LX/7RU;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1206756
    iget-object v0, p0, Lcom/facebook/video/videostreaming/LiveStreamingAudioRecorder$1;->a:LX/7RU;

    iget-object v0, v0, LX/7RU;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videostreaming/LiveStreamingAudioRecorder$1;->a:LX/7RU;

    iget-object v0, v0, LX/7RU;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7RV;

    .line 1206757
    const/4 v6, 0x0

    invoke-virtual {v0, v3, v4, v6}, LX/7RV;->a([BIZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1206758
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1206759
    :catch_0
    move-exception v0

    .line 1206760
    iget-object v1, p0, Lcom/facebook/video/videostreaming/LiveStreamingAudioRecorder$1;->a:LX/7RU;

    iget-object v1, v1, LX/7RU;->h:LX/1bJ;

    if-eqz v1, :cond_2

    .line 1206761
    iget-object v1, p0, Lcom/facebook/video/videostreaming/LiveStreamingAudioRecorder$1;->a:LX/7RU;

    iget-object v1, v1, LX/7RU;->h:LX/1bJ;

    new-instance v2, Lcom/facebook/video/videostreaming/LiveStreamingError;

    invoke-direct {v2, v0}, Lcom/facebook/video/videostreaming/LiveStreamingError;-><init>(Ljava/lang/Exception;)V

    invoke-interface {v1, v2}, LX/1bJ;->a(Lcom/facebook/video/videostreaming/LiveStreamingError;)V

    .line 1206762
    :cond_2
    return-void
.end method
