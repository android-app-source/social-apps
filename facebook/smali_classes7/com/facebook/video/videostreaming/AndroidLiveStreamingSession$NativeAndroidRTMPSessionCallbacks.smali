.class public final Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/video/videostreaming/AndroidRTMPSessionCallbacks;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public a:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

.field public final synthetic b:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;)V
    .locals 0

    .prologue
    .line 1206604
    iput-object p1, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;->b:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1206605
    iput-object p2, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;->a:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    .line 1206606
    return-void
.end method


# virtual methods
.method public final completedSpeedTestWithStatus(Lcom/facebook/video/videostreaming/NetworkSpeedTest;)V
    .locals 4

    .prologue
    .line 1206594
    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;->b:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    .line 1206595
    iput-object p1, v0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->d:Lcom/facebook/video/videostreaming/NetworkSpeedTest;

    .line 1206596
    iget-object v0, p1, Lcom/facebook/video/videostreaming/NetworkSpeedTest;->state:Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;

    invoke-virtual {v0}, Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;->name()Ljava/lang/String;

    iget-boolean v0, p1, Lcom/facebook/video/videostreaming/NetworkSpeedTest;->speedTestPassesThreshold:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1206597
    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;->b:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-static {v0}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->f(Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;)Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    move-result-object v0

    .line 1206598
    if-eqz v0, :cond_0

    .line 1206599
    iget-object v1, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;->a:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    .line 1206600
    iget-object v2, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    if-eq v1, v2, :cond_1

    .line 1206601
    :cond_0
    :goto_0
    return-void

    .line 1206602
    :cond_1
    iget-object v2, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->m:LX/1bK;

    if-eqz v2, :cond_0

    .line 1206603
    iget-object v2, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->w:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$2;

    invoke-direct {v3, v0, p1}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$2;-><init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;Lcom/facebook/video/videostreaming/NetworkSpeedTest;)V

    const p0, 0x45ded7cd

    invoke-static {v2, v3, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final didDropPackets(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1206607
    return-void
.end method

.method public final didFailWithError(Lcom/facebook/video/videostreaming/LiveStreamingError;)V
    .locals 4

    .prologue
    .line 1206585
    sget-object v0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->b:Ljava/lang/String;

    const-string v1, "Broadcast Failed with error %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1206586
    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;->b:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    .line 1206587
    iput-object p1, v0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->e:Lcom/facebook/video/videostreaming/LiveStreamingError;

    .line 1206588
    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;->b:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-static {v0}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->f(Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;)Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    move-result-object v0

    .line 1206589
    iget-object v1, p1, Lcom/facebook/video/videostreaming/LiveStreamingError;->domain:Ljava/lang/String;

    const-string v2, "RTMP_SESSION_ERROR_DOMAIN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p1, Lcom/facebook/video/videostreaming/LiveStreamingError;->errorCode:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    .line 1206590
    :cond_0
    if-eqz v0, :cond_1

    .line 1206591
    iget-object v1, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;->a:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a(Lcom/facebook/video/videostreaming/LiveStreamingError;Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;)V

    .line 1206592
    :cond_1
    return-void
.end method

.method public final didFinish()V
    .locals 0

    .prologue
    .line 1206593
    return-void
.end method

.method public final didSendPackets(J)V
    .locals 8

    .prologue
    .line 1206580
    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;->b:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    .line 1206581
    iget-object p0, v0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mLiveE2ELatencyLogger:LX/0Ab;

    move-object v0, p0

    .line 1206582
    if-eqz v0, :cond_0

    .line 1206583
    const-string v2, "live_video_frame_sent"

    const-wide/16 v5, 0x0

    move-object v1, v0

    move-wide v3, p1

    invoke-static/range {v1 .. v6}, LX/0Ab;->a(LX/0Ab;Ljava/lang/String;JJ)V

    .line 1206584
    :cond_0
    return-void
.end method

.method public final didStartWithSpeedTestStatus(Lcom/facebook/video/videostreaming/NetworkSpeedTest;)V
    .locals 3

    .prologue
    .line 1206570
    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;->b:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    .line 1206571
    iput-object p1, v0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->d:Lcom/facebook/video/videostreaming/NetworkSpeedTest;

    .line 1206572
    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;->b:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-static {v0}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->f(Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;)Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    move-result-object v0

    .line 1206573
    if-eqz v0, :cond_0

    .line 1206574
    iget-object v1, p1, Lcom/facebook/video/videostreaming/NetworkSpeedTest;->state:Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;

    sget-object v2, Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;->Ignored:Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;

    if-ne v1, v2, :cond_0

    .line 1206575
    iget-object v1, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;->a:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    .line 1206576
    iget-object v2, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    if-eq v1, v2, :cond_1

    .line 1206577
    :cond_0
    :goto_0
    return-void

    .line 1206578
    :cond_1
    iget-object v2, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->m:LX/1bK;

    if-eqz v2, :cond_0

    .line 1206579
    iget-object v2, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->w:Landroid/os/Handler;

    new-instance p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$1;

    invoke-direct {p0, v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$1;-><init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    const p1, 0x5ea6f3af

    invoke-static {v2, p0, p1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final didUpdateStreamingInfo(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1206566
    return-void
.end method

.method public final willReconnectDueToError(Lcom/facebook/video/videostreaming/LiveStreamingError;)V
    .locals 4

    .prologue
    .line 1206568
    sget-object v0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->b:Ljava/lang/String;

    const-string v1, "Broadcast Failed with error %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1206569
    return-void
.end method

.method public final writeDidTimeout()V
    .locals 0

    .prologue
    .line 1206567
    return-void
.end method
