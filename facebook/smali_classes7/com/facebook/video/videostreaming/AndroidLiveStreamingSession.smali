.class public Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# static fields
.field private static final a:LX/0lC;

.field public static final b:Ljava/lang/String;


# instance fields
.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/videostreaming/RtmpLiveStreamer;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/video/videostreaming/NetworkSpeedTest;

.field public e:Lcom/facebook/video/videostreaming/LiveStreamingError;

.field private final mCallbacks:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private final mJsonConfig:LX/0lF;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field public final mLiveE2ELatencyLogger:LX/0Ab;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private final mRtmpSSLFactoryHolder:Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field

.field private final mXAnalyticsNative:Lcom/facebook/xanalytics/XAnalyticsNative;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1206608
    new-instance v0, LX/0lC;

    invoke-direct {v0}, LX/0lC;-><init>()V

    sput-object v0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->a:LX/0lC;

    .line 1206609
    const-class v0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->b:Ljava/lang/String;

    .line 1206610
    const-string v0, "android-live-streaming"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1206611
    sget-object v0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->a:LX/0lC;

    sget-object v1, LX/0mt;->ORDER_MAP_ENTRIES_BY_KEYS:LX/0mt;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0lC;->a(LX/0mt;Z)LX/0lC;

    .line 1206612
    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1206645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1206646
    iput-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1206647
    iput-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mJsonConfig:LX/0lF;

    .line 1206648
    iput-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mCallbacks:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;

    .line 1206649
    iput-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->c:Ljava/lang/ref/WeakReference;

    .line 1206650
    iput-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mXAnalyticsNative:Lcom/facebook/xanalytics/XAnalyticsNative;

    .line 1206651
    iput-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mRtmpSSLFactoryHolder:Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

    .line 1206652
    iput-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mLiveE2ELatencyLogger:LX/0Ab;

    .line 1206653
    return-void
.end method

.method public constructor <init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;LX/0lF;LX/29a;Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;LX/0Ab;)V
    .locals 10

    .prologue
    .line 1206623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1206624
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->c:Ljava/lang/ref/WeakReference;

    .line 1206625
    new-instance v0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;

    invoke-direct {v0, p0, p0}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;-><init>(Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;)V

    iput-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mCallbacks:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;

    .line 1206626
    iput-object p2, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mJsonConfig:LX/0lF;

    .line 1206627
    iget-object v0, p3, LX/29a;->c:Lcom/facebook/xanalytics/XAnalyticsNative;

    move-object v0, v0

    .line 1206628
    iput-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mXAnalyticsNative:Lcom/facebook/xanalytics/XAnalyticsNative;

    .line 1206629
    const-string v2, ""

    .line 1206630
    iput-object p4, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mRtmpSSLFactoryHolder:Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

    .line 1206631
    iput-object p5, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mLiveE2ELatencyLogger:LX/0Ab;

    .line 1206632
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mJsonConfig:LX/0lF;

    invoke-static {v0}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->a(LX/0lF;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1206633
    :goto_0
    const-wide/16 v6, 0x0

    .line 1206634
    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mLiveE2ELatencyLogger:LX/0Ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mLiveE2ELatencyLogger:LX/0Ab;

    .line 1206635
    iget-object v1, v0, LX/0Ab;->a:LX/0Ad;

    move-object v0, v1

    .line 1206636
    iget-boolean v1, v0, LX/0Ad;->a:Z

    move v0, v1

    .line 1206637
    if-eqz v0, :cond_0

    .line 1206638
    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mLiveE2ELatencyLogger:LX/0Ab;

    .line 1206639
    iget-object v1, v0, LX/0Ab;->a:LX/0Ad;

    move-object v0, v1

    .line 1206640
    iget-wide v8, v0, LX/0Ad;->b:J

    move-wide v6, v8

    .line 1206641
    :cond_0
    iget-object v3, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mCallbacks:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession$NativeAndroidRTMPSessionCallbacks;

    iget-object v4, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mXAnalyticsNative:Lcom/facebook/xanalytics/XAnalyticsNative;

    iget-object v5, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mRtmpSSLFactoryHolder:Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->initHybrid(Ljava/lang/String;Lcom/facebook/video/videostreaming/AndroidRTMPSessionCallbacks;Lcom/facebook/xanalytics/XAnalyticsNative;Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;J)Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1206642
    return-void

    .line 1206643
    :catch_0
    move-exception v0

    .line 1206644
    invoke-virtual {v0}, LX/28F;->printStackTrace()V

    goto :goto_0
.end method

.method private static a(LX/0lF;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1206620
    sget-object v0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->a:LX/0lC;

    const-class v1, Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, LX/0lC;->a(LX/0lG;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 1206621
    sget-object v1, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->a:LX/0lC;

    invoke-virtual {v1, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1206622
    return-object v0
.end method

.method public static f(Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;)Lcom/facebook/video/videostreaming/RtmpLiveStreamer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1206617
    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 1206618
    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    .line 1206619
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native initHybrid(Ljava/lang/String;Lcom/facebook/video/videostreaming/AndroidRTMPSessionCallbacks;Lcom/facebook/xanalytics/XAnalyticsNative;Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;J)Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1206615
    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->mXAnalyticsNative:Lcom/facebook/xanalytics/XAnalyticsNative;

    invoke-virtual {v0}, Lcom/facebook/xanalytics/XAnalyticsNative;->flush()V

    .line 1206616
    return-void
.end method

.method public final b()Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;
    .locals 1

    .prologue
    .line 1206614
    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->d:Lcom/facebook/video/videostreaming/NetworkSpeedTest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->d:Lcom/facebook/video/videostreaming/NetworkSpeedTest;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/NetworkSpeedTest;->state:Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;->Failed:Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1206613
    iget-object v0, p0, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->e:Lcom/facebook/video/videostreaming/LiveStreamingError;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public native close()V
.end method

.method public native closeWithoutEOS()V
.end method

.method public native computeNewBitrate(DLjava/util/Map;)D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)D"
        }
    .end annotation
.end method

.method public native getABRComputeInterval()I
.end method

.method public native getCurrentNetworkState()I
.end method

.method public native getStats(Z)Ljava/lang/String;
.end method

.method public native hasNetworkRecoveredFromWeak()Z
.end method

.method public native isNetworkWeak()Z
.end method

.method public native reinitializeWithConfig(Ljava/lang/String;Z)Z
.end method

.method public native sendAudioData(Ljava/nio/ByteBuffer;IIIII)V
.end method

.method public native sendStreamInterrupted()V
.end method

.method public native sendVideoData(Ljava/nio/ByteBuffer;IIIII)V
.end method
