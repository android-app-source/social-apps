.class public Lcom/facebook/video/videostreaming/RtmpLiveStreamer;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/7RS;
.implements LX/7RY;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile M:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

.field public static final a:Ljava/lang/String;


# instance fields
.field public final A:LX/09G;

.field public final B:LX/0lC;

.field public final C:LX/0Uh;

.field public final D:Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

.field private final E:LX/03V;

.field public F:LX/7RV;

.field public G:LX/7RV;

.field public H:LX/7Ra;

.field private I:LX/7RZ;

.field public J:LX/0Ac;

.field public K:LX/0Ab;

.field public L:LX/0Ab;

.field public final b:LX/0So;

.field public c:LX/7RU;

.field public volatile d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

.field public volatile e:Lcom/facebook/http/protocol/ApiErrorResult;

.field public f:LX/7Rc;

.field private final g:Landroid/os/Handler;

.field private final h:Landroid/os/HandlerThread;

.field public final i:Ljava/util/concurrent/ExecutorService;

.field public final j:LX/29a;

.field private final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public volatile l:LX/1bL;

.field public volatile m:LX/1bK;

.field public volatile n:LX/7Rh;

.field private final o:LX/46K;

.field public p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

.field public q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

.field private final r:LX/11H;

.field private s:Ljava/lang/String;

.field public t:Z

.field public u:Z

.field private v:LX/60m;

.field public w:Landroid/os/Handler;

.field public x:Z

.field public y:Z

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1207515
    const-class v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/11H;Ljava/util/concurrent/ExecutorService;LX/0So;LX/0Zr;LX/46K;LX/29a;LX/09G;LX/0lC;LX/0Uh;LX/03V;LX/7Ra;LX/7RZ;Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;LX/0Ac;)V
    .locals 3
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1207516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1207517
    sget-object v1, LX/7Rc;->STREAMING_OFF:LX/7Rc;

    iput-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    .line 1207518
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->t:Z

    .line 1207519
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    .line 1207520
    iput-object p1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->r:LX/11H;

    .line 1207521
    iput-object p2, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->i:Ljava/util/concurrent/ExecutorService;

    .line 1207522
    iput-object p3, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->b:LX/0So;

    .line 1207523
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->w:Landroid/os/Handler;

    .line 1207524
    const-string v1, "Live Stream Video Thread"

    invoke-virtual {p4, v1}, LX/0Zr;->a(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->h:Landroid/os/HandlerThread;

    .line 1207525
    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->h:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 1207526
    new-instance v1, LX/7Ri;

    iget-object v2, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->h:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, LX/7Ri;-><init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    .line 1207527
    iput-object p5, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->o:LX/46K;

    .line 1207528
    iput-object p6, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->j:LX/29a;

    .line 1207529
    iput-object p7, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->A:LX/09G;

    .line 1207530
    iput-object p8, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->B:LX/0lC;

    .line 1207531
    iput-object p9, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->C:LX/0Uh;

    .line 1207532
    iput-object p10, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->E:LX/03V;

    .line 1207533
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->k:Ljava/util/Map;

    .line 1207534
    iput-object p11, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->H:LX/7Ra;

    .line 1207535
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->D:Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

    .line 1207536
    new-instance v1, LX/7RU;

    iget-object v2, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->i:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v1, v2}, LX/7RU;-><init>(Ljava/util/concurrent/ExecutorService;)V

    iput-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    .line 1207537
    iput-object p12, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->I:LX/7RZ;

    .line 1207538
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->J:LX/0Ac;

    .line 1207539
    return-void
.end method

.method private B()V
    .locals 1

    .prologue
    .line 1207540
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    invoke-virtual {v0}, LX/7RU;->d()V

    .line 1207541
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    invoke-virtual {v0}, LX/7RV;->b()V

    .line 1207542
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    if-eqz v0, :cond_0

    .line 1207543
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    invoke-virtual {v0}, LX/7RV;->b()V

    .line 1207544
    :cond_0
    sget-object v0, LX/7Rc;->STREAMING_STOPPED:LX/7Rc;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    .line 1207545
    return-void
.end method

.method public static E(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V
    .locals 1

    .prologue
    .line 1207546
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->l:LX/1bL;

    if-eqz v0, :cond_0

    .line 1207547
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->l:LX/1bL;

    invoke-interface {v0}, LX/1bL;->g()V

    .line 1207548
    :cond_0
    return-void
.end method

.method public static J(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1207549
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->x:Z

    if-eqz v0, :cond_0

    .line 1207550
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    invoke-virtual {v0}, LX/7RU;->d()V

    .line 1207551
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    sget-object v1, LX/7Rc;->STREAMING_FINISHED:LX/7Rc;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    if-nez v0, :cond_2

    .line 1207552
    :cond_1
    :goto_0
    return-void

    .line 1207553
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->n:LX/7Rh;

    if-eqz v0, :cond_3

    .line 1207554
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->w:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$12;

    invoke-direct {v1, p0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$12;-><init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    const v2, -0x589fcb57

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1207555
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v0}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->close()V

    .line 1207556
    iput-object v3, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->s:Ljava/lang/String;

    .line 1207557
    iput-boolean v4, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->t:Z

    .line 1207558
    sget-object v0, LX/7Rc;->STREAMING_FINISHED:LX/7Rc;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    .line 1207559
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v0}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->a()V

    .line 1207560
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    invoke-virtual {v0, v1}, LX/7RU;->b(LX/7RV;)Z

    .line 1207561
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    if-eqz v0, :cond_4

    .line 1207562
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->H:LX/7Ra;

    invoke-virtual {v0}, LX/7Ra;->c()Z

    .line 1207563
    :cond_4
    iput-object v3, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    .line 1207564
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    if-eqz v0, :cond_5

    .line 1207565
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v0}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->close()V

    .line 1207566
    iput-object v3, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    .line 1207567
    :cond_5
    iput-boolean v4, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->y:Z

    .line 1207568
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->l:LX/1bL;

    if-eqz v0, :cond_6

    .line 1207569
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->w:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$13;

    invoke-direct {v1, p0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$13;-><init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    const v2, 0x101dc3cc

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1207570
    :cond_6
    goto :goto_0
.end method

.method public static L(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V
    .locals 1

    .prologue
    .line 1207571
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->l:LX/1bL;

    if-eqz v0, :cond_0

    .line 1207572
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->l:LX/1bL;

    invoke-interface {v0}, LX/1bL;->f()V

    .line 1207573
    :cond_0
    return-void
.end method

.method public static N(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V
    .locals 14

    .prologue
    .line 1207574
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    sget-object v1, LX/7Rc;->STREAMING_STOPPED:LX/7Rc;

    if-eq v0, v1, :cond_1

    .line 1207575
    :cond_0
    :goto_0
    return-void

    .line 1207576
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v0}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->sendStreamInterrupted()V

    .line 1207577
    iget-object v6, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget-object v6, v6, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoId:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->C:LX/0Uh;

    sget v7, LX/19n;->g:I

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1207578
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "video_broadcast/interrupt_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget-object v7, v7, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1207579
    iget-object v7, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->B:LX/0lC;

    invoke-virtual {v7}, LX/0lC;->e()LX/0m9;

    move-result-object v7

    .line 1207580
    const-string v8, "broadcast_id"

    iget-object v9, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget-object v9, v9, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->broadcastId:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1207581
    const-string v8, "time_position"

    iget-object v9, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    .line 1207582
    iget-wide v12, v9, LX/7RV;->i:J

    move-wide v10, v12

    .line 1207583
    invoke-virtual {v7, v8, v10, v11}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 1207584
    iget-object v8, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->A:LX/09G;

    invoke-virtual {v8, v6, v7}, LX/09G;->a(Ljava/lang/String;LX/0lF;)V

    .line 1207585
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget-wide v0, v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->sendStreamInterruptedIntervalInSeconds:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1207586
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget-wide v4, v3, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->sendStreamInterruptedIntervalInSeconds:J

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/video/videostreaming/RtmpLiveStreamer;
    .locals 3

    .prologue
    .line 1207587
    sget-object v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->M:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    if-nez v0, :cond_1

    .line 1207588
    const-class v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    monitor-enter v1

    .line 1207589
    :try_start_0
    sget-object v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->M:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1207590
    if-eqz v2, :cond_0

    .line 1207591
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->b(LX/0QB;)Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->M:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1207592
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1207593
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1207594
    :cond_1
    sget-object v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->M:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    return-object v0

    .line 1207595
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1207596
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;LX/0Ab;)V
    .locals 10

    .prologue
    .line 1207409
    iget-boolean v0, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->mIsDiskRecordingEnabled:Z

    iput-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    .line 1207410
    new-instance v0, LX/7RV;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->b:LX/0So;

    new-instance v2, LX/7Rf;

    invoke-direct {v2, p0}, LX/7Rf;-><init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    invoke-direct {v0, v1, p0, p2, v2}, LX/7RV;-><init>(LX/0So;LX/7RS;LX/0Ab;LX/7RW;)V

    iput-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    .line 1207411
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    invoke-virtual {v0}, LX/7RV;->a()V

    .line 1207412
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    if-nez v0, :cond_3

    .line 1207413
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    if-eqz v0, :cond_1

    .line 1207414
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->H:LX/7Ra;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    iget-object v2, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    .line 1207415
    iput-object v1, v0, LX/7Ra;->e:LX/7RV;

    .line 1207416
    iput-object v2, v0, LX/7Ra;->f:LX/7RV;

    .line 1207417
    iget-object v3, v0, LX/7Ra;->d:Ljava/io/File;

    if-nez v3, :cond_0

    .line 1207418
    iget-object v3, v0, LX/7Ra;->b:LX/1Er;

    const-string v4, "video_transcode"

    const-string v5, ".mp4"

    sget-object p2, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v3, v4, v5, p2}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v3

    iput-object v3, v0, LX/7Ra;->d:Ljava/io/File;

    .line 1207419
    :cond_0
    iget-object v3, v0, LX/7Ra;->d:Ljava/io/File;

    if-nez v3, :cond_1

    .line 1207420
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/7Ra;->i:Z

    .line 1207421
    sget-object v3, LX/7Ra;->a:Ljava/lang/String;

    const-string v4, "LiveStreamMux Unable to create output file"

    invoke-static {v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207422
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    iget-object v1, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    iget-object v2, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->audioStreamingConfig:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    iget-object v3, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v3}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->getABRComputeInterval()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, LX/7RV;->a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;I)V

    .line 1207423
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    if-eqz v0, :cond_2

    .line 1207424
    invoke-virtual {p1}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->a()LX/0lF;

    move-result-object v0

    .line 1207425
    new-instance v6, LX/60l;

    invoke-direct {v6}, LX/60l;-><init>()V

    .line 1207426
    const-string v4, "stream_disk_recording_space_check_interval_in_seconds"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    .line 1207427
    const-string v5, "stream_disk_recording_space_check_interval_in_seconds"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 1207428
    if-nez v4, :cond_4

    const-wide/16 v4, 0x0

    :goto_1
    iput-wide v4, v6, LX/60l;->a:D

    .line 1207429
    if-nez v7, :cond_5

    const/4 v4, 0x0

    :goto_2
    iput v4, v6, LX/60l;->b:I

    .line 1207430
    const/4 v4, 0x0

    const/4 v9, 0x0

    .line 1207431
    :try_start_0
    const-string v5, "android_video_profile"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 1207432
    if-eqz v5, :cond_6

    .line 1207433
    invoke-virtual {v5}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v5

    .line 1207434
    :goto_3
    new-instance v7, LX/60r;

    invoke-direct {v7}, LX/60r;-><init>()V

    const-string v8, "stream_disk_recording_video_width"

    invoke-virtual {v0, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    invoke-virtual {v8}, LX/0lF;->w()I

    move-result v8

    .line 1207435
    iput v8, v7, LX/60r;->a:I

    .line 1207436
    move-object v7, v7

    .line 1207437
    const-string v8, "stream_disk_recording_video_height"

    invoke-virtual {v0, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    invoke-virtual {v8}, LX/0lF;->w()I

    move-result v8

    .line 1207438
    iput v8, v7, LX/60r;->b:I

    .line 1207439
    move-object v7, v7

    .line 1207440
    const-string v8, "stream_disk_recording_video_bitrate"

    invoke-virtual {v0, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    invoke-virtual {v8}, LX/0lF;->w()I

    move-result v8

    .line 1207441
    iput v8, v7, LX/60r;->c:I

    .line 1207442
    move-object v7, v7

    .line 1207443
    const-string v8, "stream_disk_recording_video_fps"

    invoke-virtual {v0, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    invoke-virtual {v8}, LX/0lF;->w()I

    move-result v8

    .line 1207444
    iput v8, v7, LX/60r;->d:I

    .line 1207445
    move-object v7, v7

    .line 1207446
    const/4 v8, 0x0

    .line 1207447
    iput-boolean v8, v7, LX/60r;->e:Z

    .line 1207448
    move-object v7, v7

    .line 1207449
    iput-object v5, v7, LX/60r;->f:Ljava/lang/String;

    .line 1207450
    move-object v5, v7

    .line 1207451
    invoke-virtual {v5}, LX/60r;->a()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1207452
    :goto_4
    move-object v4, v4

    .line 1207453
    iput-object v4, v6, LX/60l;->c:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    .line 1207454
    :try_start_1
    new-instance v4, LX/60k;

    invoke-direct {v4}, LX/60k;-><init>()V

    const-string v5, "stream_disk_recording_audio_sample_rate"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-virtual {v5}, LX/0lF;->w()I

    move-result v5

    .line 1207455
    iput v5, v4, LX/60k;->a:I

    .line 1207456
    move-object v4, v4

    .line 1207457
    const-string v5, "stream_disk_recording_audio_channels"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-virtual {v5}, LX/0lF;->w()I

    move-result v5

    .line 1207458
    iput v5, v4, LX/60k;->c:I

    .line 1207459
    move-object v4, v4

    .line 1207460
    const-string v5, "stream_disk_recording_audio_bitrate"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-virtual {v5}, LX/0lF;->w()I

    move-result v5

    .line 1207461
    iput v5, v4, LX/60k;->b:I

    .line 1207462
    move-object v4, v4

    .line 1207463
    invoke-virtual {v4}, LX/60k;->a()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 1207464
    :goto_5
    move-object v4, v4

    .line 1207465
    iput-object v4, v6, LX/60l;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    .line 1207466
    move-object v0, v6

    .line 1207467
    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    iget-object v2, v0, LX/60l;->c:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    iget-object v0, v0, LX/60l;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v0, v3}, LX/7RV;->a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;I)V

    .line 1207468
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    invoke-virtual {v0, v1}, LX/7RU;->a(LX/7RV;)Z

    .line 1207469
    return-void

    .line 1207470
    :cond_3
    new-instance v0, LX/7RV;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->b:LX/0So;

    const/4 v2, 0x0

    new-instance v3, LX/7Rg;

    invoke-direct {v3, p0}, LX/7Rg;-><init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V

    invoke-direct {v0, v1, p0, v2, v3}, LX/7RV;-><init>(LX/0So;LX/7RS;LX/0Ab;LX/7RW;)V

    iput-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    .line 1207471
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    invoke-virtual {v0}, LX/7RV;->a()V

    goto/16 :goto_0

    .line 1207472
    :cond_4
    invoke-virtual {v4}, LX/0lF;->E()D

    move-result-wide v4

    goto/16 :goto_1

    .line 1207473
    :cond_5
    invoke-virtual {v7}, LX/0lF;->C()I

    move-result v4

    goto/16 :goto_2

    .line 1207474
    :catch_0
    move-exception v5

    .line 1207475
    sget-object v7, LX/60l;->e:Ljava/lang/Class;

    const-string v8, "Error getting VideoStreamingConfig"

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v5, v8, v9}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    :cond_6
    move-object v5, v4

    goto/16 :goto_3

    .line 1207476
    :catch_1
    move-exception v4

    .line 1207477
    sget-object v5, LX/60l;->e:Ljava/lang/Class;

    const-string v7, "Error getting AudioStreamingConfig"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v5, v4, v7, v8}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1207478
    const/4 v4, 0x0

    goto :goto_5
.end method

.method private static b(LX/0QB;)Lcom/facebook/video/videostreaming/RtmpLiveStreamer;
    .locals 15

    .prologue
    .line 1207597
    new-instance v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v1

    check-cast v1, LX/11H;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v4

    check-cast v4, LX/0Zr;

    invoke-static {p0}, LX/46K;->a(LX/0QB;)LX/46K;

    move-result-object v5

    check-cast v5, LX/46K;

    invoke-static {p0}, LX/29a;->a(LX/0QB;)LX/29a;

    move-result-object v6

    check-cast v6, LX/29a;

    invoke-static {p0}, LX/09G;->a(LX/0QB;)LX/09G;

    move-result-object v7

    check-cast v7, LX/09G;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v8

    check-cast v8, LX/0lC;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    .line 1207598
    new-instance v13, LX/7Ra;

    invoke-static {p0}, LX/7T6;->a(LX/0QB;)LX/61h;

    move-result-object v11

    check-cast v11, LX/61h;

    invoke-static {p0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v12

    check-cast v12, LX/1Er;

    invoke-direct {v13, v11, v12}, LX/7Ra;-><init>(LX/61h;LX/1Er;)V

    .line 1207599
    move-object v11, v13

    .line 1207600
    check-cast v11, LX/7Ra;

    .line 1207601
    new-instance v12, LX/7RZ;

    invoke-direct {v12}, LX/7RZ;-><init>()V

    .line 1207602
    move-object v12, v12

    .line 1207603
    move-object v12, v12

    .line 1207604
    check-cast v12, LX/7RZ;

    invoke-static {p0}, Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;->a(LX/0QB;)Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

    move-result-object v13

    check-cast v13, Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

    invoke-static {p0}, LX/0Ac;->b(LX/0QB;)LX/0Ac;

    move-result-object v14

    check-cast v14, LX/0Ac;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;-><init>(LX/11H;Ljava/util/concurrent/ExecutorService;LX/0So;LX/0Zr;LX/46K;LX/29a;LX/09G;LX/0lC;LX/0Uh;LX/03V;LX/7Ra;LX/7RZ;Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;LX/0Ac;)V

    .line 1207605
    return-object v0
.end method

.method public static declared-synchronized c(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;Z)Z
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1207606
    monitor-enter p0

    const/4 v0, 0x0

    move v3, v1

    .line 1207607
    :goto_0
    const/4 v2, 0x3

    if-ge v3, v2, :cond_3

    .line 1207608
    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    .line 1207609
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    if-nez v2, :cond_1

    .line 1207610
    invoke-static {p0, p1}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;Z)Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    .line 1207611
    :cond_1
    iget-object v2, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget-object v2, v2, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->rtmpPublishUrl:Ljava/lang/String;
    :try_end_1
    .catch LX/2Oo; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1207612
    :try_start_2
    iget-object v7, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->J:LX/0Ac;

    iget-object v8, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget-object v8, v8, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->videoId:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/0Ac;->a(Ljava/lang/String;)LX/0Ab;

    move-result-object v7

    :goto_1
    iput-object v7, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->K:LX/0Ab;

    .line 1207613
    new-instance v7, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    iget-object v8, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    invoke-virtual {v8}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;->a()LX/0lF;

    move-result-object v9

    iget-object v10, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->j:LX/29a;

    iget-object v11, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->D:Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;

    iget-object v12, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->K:LX/0Ab;

    move-object v8, p0

    invoke-direct/range {v7 .. v12}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;-><init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;LX/0lF;LX/29a;Lcom/facebook/video/rtmpssl/AndroidRtmpSSLFactoryHolder;LX/0Ab;)V

    iput-object v7, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    .line 1207614
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget-object v4, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->K:LX/0Ab;

    invoke-static {p0, v0, v4}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;LX/0Ab;)V
    :try_end_2
    .catch LX/2Oo; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1207615
    const/4 v0, 0x1

    .line 1207616
    :goto_2
    monitor-exit p0

    return v0

    .line 1207617
    :catch_0
    move-exception v0

    .line 1207618
    :try_start_3
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v2

    .line 1207619
    sget-object v3, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a:Ljava/lang/String;

    const-string v4, "Unable to retrieve broadcast ID. "

    invoke-static {v3, v4, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1207620
    iput-object v2, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->e:Lcom/facebook/http/protocol/ApiErrorResult;

    move v0, v1

    .line 1207621
    goto :goto_2

    .line 1207622
    :catch_1
    move-exception v0

    move-object v6, v0

    move-object v0, v2

    move-object v2, v6

    .line 1207623
    :goto_3
    sget-object v4, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a:Ljava/lang/String;

    const-string v5, "Unable to retrieve broadcast ID. "

    invoke-static {v4, v5, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1207624
    const/16 v2, 0xa

    shl-int/2addr v2, v3

    int-to-long v4, v2

    :try_start_4
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1207625
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 1207626
    :catch_2
    move-exception v0

    .line 1207627
    :try_start_5
    sget-object v2, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a:Ljava/lang/String;

    const-string v3, "Thread.sleep() threw InterruptedException "

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 1207628
    goto :goto_2

    .line 1207629
    :cond_3
    sget-object v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a:Ljava/lang/String;

    const-string v2, "Could not connect to RTMP server."

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move v0, v1

    .line 1207630
    goto :goto_2

    .line 1207631
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1207632
    :catch_3
    move-exception v2

    goto :goto_3

    .line 1207633
    :cond_4
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public static d(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;Z)Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;
    .locals 5

    .prologue
    .line 1207704
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->r:LX/11H;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->v:LX/60m;

    new-instance v2, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;

    iget-object v3, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->s:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->z:Z

    invoke-direct {v2, v3, v4, p1}, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;-><init>(Ljava/lang/String;ZZ)V

    const/4 v3, 0x0

    const-class v4, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    return-object v0
.end method

.method public static u(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1207634
    new-instance v0, LX/60m;

    invoke-direct {v0}, LX/60m;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->v:LX/60m;

    .line 1207635
    sget-object v0, LX/7Rc;->STREAMING_OFF:LX/7Rc;

    iput-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    .line 1207636
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    .line 1207637
    iput-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    .line 1207638
    iput-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    .line 1207639
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->H:LX/7Ra;

    const/4 v4, 0x0

    .line 1207640
    const/4 v2, 0x0

    iput-object v2, v0, LX/7Ra;->d:Ljava/io/File;

    .line 1207641
    iget-boolean v2, v0, LX/7Ra;->g:Z

    if-eqz v2, :cond_0

    .line 1207642
    sget-object v2, LX/7Ra;->a:Ljava/lang/String;

    const-string v3, "LiveStreamMux Muxer was not stopped after previous broadcast. Stopping it now"

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207643
    invoke-virtual {v0}, LX/7Ra;->c()Z

    .line 1207644
    :cond_0
    iput-boolean v4, v0, LX/7Ra;->i:Z

    .line 1207645
    iput-boolean v4, v0, LX/7Ra;->g:Z

    .line 1207646
    const-wide/16 v2, -0x1

    iput-wide v2, v0, LX/7Ra;->h:J

    .line 1207647
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->I:LX/7RZ;

    const/4 v2, -0x1

    .line 1207648
    iget-object v1, v0, LX/7RZ;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 1207649
    iget-object v1, v0, LX/7RZ;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 1207650
    iput v2, v0, LX/7RZ;->c:I

    .line 1207651
    iput v2, v0, LX/7RZ;->d:I

    .line 1207652
    iput v2, v0, LX/7RZ;->e:I

    .line 1207653
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    invoke-virtual {v0}, LX/7RU;->b()V

    .line 1207654
    return-void
.end method

.method public static z(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;)V
    .locals 3

    .prologue
    .line 1207655
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    sget-object v1, LX/7Rc;->STREAMING_STARTED:LX/7Rc;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    sget-object v1, LX/7Rc;->STREAMING_INIT_COMPLETE:LX/7Rc;

    if-ne v0, v1, :cond_2

    .line 1207656
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    sget-object v1, LX/7Rc;->STREAMING_STARTED:LX/7Rc;

    if-ne v0, v1, :cond_1

    .line 1207657
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    invoke-virtual {v0}, LX/7RU;->g()V

    .line 1207658
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    invoke-virtual {v0}, LX/7RV;->c()V

    .line 1207659
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    if-eqz v0, :cond_1

    .line 1207660
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    invoke-virtual {v0}, LX/7RV;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1207661
    :cond_1
    invoke-direct {p0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->B()V

    .line 1207662
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->l:LX/1bL;

    if-eqz v0, :cond_3

    .line 1207663
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->l:LX/1bL;

    invoke-interface {v0}, LX/1bL;->i()V

    .line 1207664
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1207665
    return-void

    .line 1207666
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->B()V

    throw v0
.end method


# virtual methods
.method public final a(III)I
    .locals 4

    .prologue
    .line 1207667
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1207668
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->l:LX/1bL;

    invoke-interface {v0}, LX/1bL;->ix_()Ljava/util/Map;

    move-result-object v0

    .line 1207669
    if-eqz v0, :cond_0

    .line 1207670
    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->k:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1207671
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->k:Ljava/util/Map;

    const-string v1, "base_system_version"

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207672
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    .line 1207673
    iget-object v1, v0, LX/7RV;->v:Ljava/util/Map;

    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v1

    move-object v0, v1

    .line 1207674
    if-eqz v0, :cond_1

    .line 1207675
    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->k:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1207676
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    int-to-double v2, p1

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->k:Ljava/util/Map;

    invoke-virtual {v0, v2, v3, v1}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->computeNewBitrate(DLjava/util/Map;)D

    move-result-wide v0

    double-to-int v0, v0

    .line 1207677
    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->I:LX/7RZ;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1207678
    div-int/lit16 v3, v0, 0x3e8

    .line 1207679
    iget p0, v1, LX/7RZ;->c:I

    if-eq v3, p0, :cond_2

    .line 1207680
    iget-object p0, v1, LX/7RZ;->a:Ljava/util/Map;

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-interface {p0, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207681
    iput v3, v1, LX/7RZ;->c:I

    .line 1207682
    :cond_2
    iget v3, v1, LX/7RZ;->d:I

    if-eq p2, v3, :cond_3

    .line 1207683
    iget-object v3, v1, LX/7RZ;->b:Ljava/util/Map;

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v3, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207684
    iput p2, v1, LX/7RZ;->d:I

    .line 1207685
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, v1, LX/7RZ;->e:I

    .line 1207686
    return v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 1207687
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 1207688
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 1207689
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->l:LX/1bL;

    if-eqz v0, :cond_0

    .line 1207690
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->w:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$9;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$9;-><init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;J)V

    const v2, 0x59edcb0c

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1207691
    :cond_0
    return-void
.end method

.method public final a(LX/1bJ;)V
    .locals 1
    .param p1    # LX/1bJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1207692
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    .line 1207693
    iput-object p1, v0, LX/7RU;->h:LX/1bJ;

    .line 1207694
    return-void
.end method

.method public final a(LX/1bK;)V
    .locals 0

    .prologue
    .line 1207695
    iput-object p1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->m:LX/1bK;

    .line 1207696
    return-void
.end method

.method public final a(LX/1bL;)V
    .locals 0

    .prologue
    .line 1207697
    iput-object p1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->l:LX/1bL;

    .line 1207698
    return-void
.end method

.method public final a(LX/7RX;)V
    .locals 3

    .prologue
    .line 1207699
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 1207700
    return-void
.end method

.method public final a(Lcom/facebook/video/videostreaming/LiveStreamingError;Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;)V
    .locals 3

    .prologue
    .line 1207511
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->p:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    if-eq p2, v0, :cond_1

    .line 1207512
    :cond_0
    :goto_0
    return-void

    .line 1207513
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->l:LX/1bL;

    if-eqz v0, :cond_0

    .line 1207514
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->w:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$3;-><init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;Lcom/facebook/video/videostreaming/LiveStreamingError;)V

    const v2, -0x5bf7a142

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1207701
    iput-object p1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->s:Ljava/lang/String;

    .line 1207702
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->t:Z

    .line 1207703
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1207381
    iput-boolean p1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->x:Z

    .line 1207382
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    if-nez v0, :cond_0

    .line 1207383
    :goto_0
    return-void

    .line 1207384
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->x:Z

    if-eqz v0, :cond_1

    .line 1207385
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    invoke-virtual {v0}, LX/7RU;->a()V

    .line 1207386
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    invoke-virtual {v0}, LX/7RU;->f()V

    goto :goto_0

    .line 1207387
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    invoke-virtual {v0}, LX/7RU;->c()V

    .line 1207388
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    invoke-virtual {v0}, LX/7RU;->d()V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1207379
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 1207380
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 1207377
    iput-boolean p1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->z:Z

    .line 1207378
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1207376
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    sget-object v1, LX/7Rc;->STREAMING_STARTED:LX/7Rc;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()LX/7Rc;
    .locals 1

    .prologue
    .line 1207375
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    return-object v0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1207373
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 1207374
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1207371
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    invoke-virtual {v0}, LX/7RU;->c()V

    .line 1207372
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 1207369
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1207370
    return-void
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 1207368
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v0}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->b()Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;

    move-result-object v0

    sget-object v1, Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;->Succeeded:Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v0}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->b()Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;

    move-result-object v0

    sget-object v1, Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;->Ignored:Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v0}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->c()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1207359
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1207360
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    if-eqz v0, :cond_0

    .line 1207361
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    invoke-virtual {v0}, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->b()Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;

    move-result-object v0

    sget-object v2, Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;->Succeeded:Lcom/facebook/video/videostreaming/NetworkSpeedTest$Status;

    if-ne v0, v2, :cond_1

    const-string v0, "SUCCEEDED"

    .line 1207362
    :goto_0
    const-string v2, "audio_live_session_speed_test"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207363
    const-string v0, "audio_live_session_broadcast_failed_error"

    iget-object v2, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->q:Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;

    .line 1207364
    iget-object p0, v2, Lcom/facebook/video/videostreaming/AndroidLiveStreamingSession;->e:Lcom/facebook/video/videostreaming/LiveStreamingError;

    move-object v2, p0

    .line 1207365
    iget-object v2, v2, Lcom/facebook/video/videostreaming/LiveStreamingError;->fullDescription:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207366
    :cond_0
    return-object v1

    .line 1207367
    :cond_1
    const-string v0, "FAILED"

    goto :goto_0
.end method

.method public final j()V
    .locals 3

    .prologue
    .line 1207357
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1207358
    return-void
.end method

.method public final k()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1207389
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->I:LX/7RZ;

    .line 1207390
    iget v1, v0, LX/7RZ;->e:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 1207391
    iget v1, v0, LX/7RZ;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1207392
    iget-object v2, v0, LX/7RZ;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1207393
    iget-object v2, v0, LX/7RZ;->a:Ljava/util/Map;

    iget v3, v0, LX/7RZ;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207394
    :cond_0
    iget-object v2, v0, LX/7RZ;->b:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1207395
    iget-object v2, v0, LX/7RZ;->b:Ljava/util/Map;

    iget v3, v0, LX/7RZ;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207396
    :cond_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1207397
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1207398
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1207399
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1207400
    const-string v5, "uplink_bwe"

    iget-object p0, v0, LX/7RZ;->b:Ljava/util/Map;

    invoke-interface {v3, v5, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207401
    const-string v5, "ul_bwe"

    iget-object p0, v0, LX/7RZ;->a:Ljava/util/Map;

    invoke-interface {v4, v5, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207402
    const-string v5, "bwe"

    invoke-interface {v2, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207403
    const-string v3, "video_mts"

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207404
    const-string v3, "media"

    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v2}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207405
    move-object v0, v1

    .line 1207406
    return-object v0
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 1207407
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1207408
    return-void
.end method

.method public final m()LX/0Ab;
    .locals 1

    .prologue
    .line 1207479
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->K:LX/0Ab;

    return-object v0
.end method

.method public final n()Landroid/os/Looper;
    .locals 1

    .prologue
    .line 1207480
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->h:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/video/videostreaming/LiveStreamEncoderSurface;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1207481
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1207482
    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    .line 1207483
    iget-object v2, v1, LX/7RV;->p:LX/7Re;

    move-object v1, v2

    .line 1207484
    if-eqz v1, :cond_0

    .line 1207485
    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->F:LX/7RV;

    .line 1207486
    iget-object v2, v1, LX/7RV;->p:LX/7Re;

    move-object v1, v2

    .line 1207487
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1207488
    iget-boolean v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    if-eqz v1, :cond_0

    .line 1207489
    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->G:LX/7RV;

    .line 1207490
    iget-object v2, v1, LX/7RV;->p:LX/7Re;

    move-object v1, v2

    .line 1207491
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1207492
    :cond_0
    return-object v0
.end method

.method public final p()V
    .locals 3

    .prologue
    .line 1207493
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 1207494
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 1207495
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->c:LX/7RU;

    invoke-virtual {v0}, LX/7RU;->f()V

    .line 1207496
    return-void
.end method

.method public final r()V
    .locals 3

    .prologue
    .line 1207497
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->g:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1207498
    return-void
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 1207499
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->x:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->y:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Ljava/io/File;
    .locals 10

    .prologue
    .line 1207500
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    sget-object v1, LX/7Rc;->STREAMING_FINISHED:LX/7Rc;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    if-nez v0, :cond_1

    .line 1207501
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->E:LX/03V;

    sget-object v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getDvrFile failed preconditions - state:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->f:LX/7Rc;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " enabled:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->u:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207502
    const/4 v0, 0x0

    .line 1207503
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->H:LX/7Ra;

    const/4 v4, 0x0

    .line 1207504
    iget-boolean v5, v0, LX/7Ra;->i:Z

    if-eqz v5, :cond_2

    .line 1207505
    sget-object v5, LX/7Ra;->a:Ljava/lang/String;

    const-string v6, "Muxing Failed for DVR"

    invoke-static {v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207506
    :goto_1
    move-object v0, v4

    .line 1207507
    goto :goto_0

    .line 1207508
    :cond_2
    iget-object v5, v0, LX/7Ra;->d:Ljava/io/File;

    if-eqz v5, :cond_3

    iget-object v5, v0, LX/7Ra;->d:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_4

    .line 1207509
    :cond_3
    sget-object v5, LX/7Ra;->a:Ljava/lang/String;

    const-string v6, "Unable to create valid muxed file for DVR"

    invoke-static {v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1207510
    :cond_4
    iget-object v4, v0, LX/7Ra;->d:Ljava/io/File;

    goto :goto_1
.end method
