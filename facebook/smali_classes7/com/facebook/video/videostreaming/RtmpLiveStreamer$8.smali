.class public final Lcom/facebook/video/videostreaming/RtmpLiveStreamer$8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;ZZ)V
    .locals 0

    .prologue
    .line 1207254
    iput-object p1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$8;->c:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iput-boolean p2, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$8;->a:Z

    iput-boolean p3, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$8;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1207255
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$8;->c:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->m:LX/1bK;

    if-nez v0, :cond_0

    .line 1207256
    :goto_0
    return-void

    .line 1207257
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$8;->a:Z

    if-eqz v0, :cond_1

    .line 1207258
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$8;->c:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->m:LX/1bK;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$8;->c:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iget-object v1, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->d:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iget-boolean v2, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$8;->b:Z

    invoke-interface {v0, v1, v2}, LX/1bK;->a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;Z)V

    goto :goto_0

    .line 1207259
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$8;->c:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->m:LX/1bK;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$8;->c:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iget-object v1, v1, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->e:Lcom/facebook/http/protocol/ApiErrorResult;

    iget-boolean v2, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$8;->b:Z

    invoke-interface {v0, v1, v2}, LX/1bK;->a(Lcom/facebook/http/protocol/ApiErrorResult;Z)V

    goto :goto_0
.end method
