.class public final Lcom/facebook/video/videostreaming/RtmpLiveStreamer$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

.field public final synthetic b:Lcom/facebook/http/protocol/ApiErrorResult;

.field public final synthetic c:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/videostreaming/RtmpLiveStreamer;Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;Lcom/facebook/http/protocol/ApiErrorResult;)V
    .locals 0

    .prologue
    .line 1207216
    iput-object p1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$5;->c:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iput-object p2, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$5;->a:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    iput-object p3, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$5;->b:Lcom/facebook/http/protocol/ApiErrorResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1207217
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$5;->c:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->m:LX/1bK;

    if-nez v0, :cond_0

    .line 1207218
    :goto_0
    return-void

    .line 1207219
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$5;->a:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    if-eqz v0, :cond_1

    .line 1207220
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$5;->c:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->m:LX/1bK;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$5;->a:Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    invoke-interface {v0, v1, v2}, LX/1bK;->a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;Z)V

    goto :goto_0

    .line 1207221
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$5;->c:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->m:LX/1bK;

    iget-object v1, p0, Lcom/facebook/video/videostreaming/RtmpLiveStreamer$5;->b:Lcom/facebook/http/protocol/ApiErrorResult;

    invoke-interface {v0, v1, v2}, LX/1bK;->a(Lcom/facebook/http/protocol/ApiErrorResult;Z)V

    goto :goto_0
.end method
