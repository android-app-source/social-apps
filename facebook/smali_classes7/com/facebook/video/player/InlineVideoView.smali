.class public Lcom/facebook/video/player/InlineVideoView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/7Kc;


# instance fields
.field public a:LX/19d;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/19P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:LX/16V;

.field private final f:Landroid/util/AttributeSet;

.field private final g:I

.field private final h:LX/7LJ;

.field private final i:LX/7LK;

.field private j:Lcom/facebook/video/engine/VideoPlayerParams;

.field private k:Z

.field public l:Z

.field private m:Z

.field private n:Z

.field private o:LX/04D;

.field private p:LX/04G;

.field public q:I

.field public r:I

.field private s:Z

.field private t:Z

.field public u:LX/2q7;

.field public v:LX/2pf;

.field public w:LX/2qI;

.field private x:LX/2p8;

.field public y:LX/7LL;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1197858
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/video/player/InlineVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1197859
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1197860
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/InlineVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1197861
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1197862
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1197863
    new-instance v0, LX/16V;

    invoke-direct {v0}, LX/16V;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->e:LX/16V;

    .line 1197864
    new-instance v0, LX/7LJ;

    invoke-direct {v0, p0}, LX/7LJ;-><init>(Lcom/facebook/video/player/InlineVideoView;)V

    iput-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->h:LX/7LJ;

    .line 1197865
    new-instance v0, LX/7LK;

    invoke-direct {v0, p0}, LX/7LK;-><init>(Lcom/facebook/video/player/InlineVideoView;)V

    iput-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->i:LX/7LK;

    .line 1197866
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    iput-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->o:LX/04D;

    .line 1197867
    sget-object v0, LX/04G;->INLINE_PLAYER:LX/04G;

    iput-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->p:LX/04G;

    .line 1197868
    iput-boolean v1, p0, Lcom/facebook/video/player/InlineVideoView;->s:Z

    .line 1197869
    iput-boolean v1, p0, Lcom/facebook/video/player/InlineVideoView;->t:Z

    .line 1197870
    sget-object v0, LX/7LL;->NONE:LX/7LL;

    iput-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->y:LX/7LL;

    .line 1197871
    const v0, 0x7f03092f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1197872
    const-class v0, Lcom/facebook/video/player/InlineVideoView;

    invoke-static {v0, p0}, Lcom/facebook/video/player/InlineVideoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1197873
    iput-object p2, p0, Lcom/facebook/video/player/InlineVideoView;->f:Landroid/util/AttributeSet;

    .line 1197874
    iput p3, p0, Lcom/facebook/video/player/InlineVideoView;->g:I

    .line 1197875
    return-void
.end method

.method private a(ILX/04g;)V
    .locals 2

    .prologue
    .line 1197876
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "seekTo:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1197877
    invoke-direct {p0}, Lcom/facebook/video/player/InlineVideoView;->f()V

    .line 1197878
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0, p1, p2}, LX/2q7;->a(ILX/04g;)V

    .line 1197879
    return-void
.end method

.method private a(LX/04g;LX/7K4;)V
    .locals 2

    .prologue
    .line 1197880
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "start:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1197881
    invoke-direct {p0}, Lcom/facebook/video/player/InlineVideoView;->f()V

    .line 1197882
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0, p1, p2}, LX/2q7;->a(LX/04g;LX/7K4;)V

    .line 1197883
    iget-boolean v0, p0, Lcom/facebook/video/player/InlineVideoView;->k:Z

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/player/InlineVideoView;->a(ZLX/04g;)V

    .line 1197884
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/player/InlineVideoView;->l:Z

    .line 1197885
    return-void
.end method

.method private static a(Lcom/facebook/video/player/InlineVideoView;LX/19d;LX/19P;LX/1C2;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1197886
    iput-object p1, p0, Lcom/facebook/video/player/InlineVideoView;->a:LX/19d;

    iput-object p2, p0, Lcom/facebook/video/player/InlineVideoView;->b:LX/19P;

    iput-object p3, p0, Lcom/facebook/video/player/InlineVideoView;->c:LX/1C2;

    iput-object p4, p0, Lcom/facebook/video/player/InlineVideoView;->d:Landroid/os/Handler;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/player/InlineVideoView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/video/player/InlineVideoView;

    invoke-static {v3}, LX/19d;->b(LX/0QB;)LX/19d;

    move-result-object v0

    check-cast v0, LX/19d;

    invoke-static {v3}, LX/19P;->a(LX/0QB;)LX/19P;

    move-result-object v1

    check-cast v1, LX/19P;

    invoke-static {v3}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v2

    check-cast v2, LX/1C2;

    invoke-static {v3}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/video/player/InlineVideoView;->a(Lcom/facebook/video/player/InlineVideoView;LX/19d;LX/19P;LX/1C2;Landroid/os/Handler;)V

    return-void
.end method

.method private b(LX/04g;)V
    .locals 2

    .prologue
    .line 1197887
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "replay:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1197888
    invoke-direct {p0}, Lcom/facebook/video/player/InlineVideoView;->f()V

    .line 1197889
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->c(LX/04g;)V

    .line 1197890
    return-void
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 1197891
    invoke-direct {p0}, Lcom/facebook/video/player/InlineVideoView;->f()V

    .line 1197892
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0}, LX/2q7;->i()Z

    move-result v0

    return v0
.end method

.method private e()LX/2p8;
    .locals 2

    .prologue
    .line 1197922
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->a:LX/19d;

    sget-object v1, LX/2p7;->NORMAL:LX/2p7;

    invoke-virtual {v0, v1}, LX/19d;->a(LX/2p7;)LX/2p8;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 13

    .prologue
    const/4 v4, -0x1

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 1197893
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    if-eqz v0, :cond_3

    .line 1197894
    iget-boolean v0, p0, Lcom/facebook/video/player/InlineVideoView;->t:Z

    if-nez v0, :cond_1

    .line 1197895
    const v0, 0x7f0d177e

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/InlineVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1197896
    iget-object v1, p0, Lcom/facebook/video/player/InlineVideoView;->x:LX/2p8;

    if-eqz v1, :cond_2

    .line 1197897
    iget-object v1, p0, Lcom/facebook/video/player/InlineVideoView;->x:LX/2p8;

    invoke-virtual {v1}, LX/2p8;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1197898
    iget-object v1, p0, Lcom/facebook/video/player/InlineVideoView;->x:LX/2p8;

    invoke-virtual {v1, v0}, LX/2p8;->a(Landroid/view/ViewGroup;)V

    .line 1197899
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    iget-object v1, p0, Lcom/facebook/video/player/InlineVideoView;->x:LX/2p8;

    invoke-interface {v0, v1}, LX/2q7;->a(LX/2p8;)V

    .line 1197900
    iput-boolean v11, p0, Lcom/facebook/video/player/InlineVideoView;->t:Z

    .line 1197901
    :cond_1
    :goto_0
    return-void

    .line 1197902
    :cond_2
    iget-object v1, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v1}, LX/2q7;->k()Landroid/view/View;

    move-result-object v1

    .line 1197903
    if-eqz v1, :cond_1

    .line 1197904
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v3, 0x11

    invoke-direct {v2, v4, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1197905
    iput-boolean v11, p0, Lcom/facebook/video/player/InlineVideoView;->t:Z

    .line 1197906
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->b:LX/19P;

    .line 1197907
    iget-object v2, v0, LX/19P;->d:Ljava/util/List;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1197908
    goto :goto_0

    .line 1197909
    :cond_3
    invoke-direct {p0}, Lcom/facebook/video/player/InlineVideoView;->e()LX/2p8;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->x:LX/2p8;

    .line 1197910
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->b:LX/19P;

    invoke-virtual {p0}, Lcom/facebook/video/player/InlineVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/video/player/InlineVideoView;->f:Landroid/util/AttributeSet;

    iget v3, p0, Lcom/facebook/video/player/InlineVideoView;->g:I

    iget-object v4, p0, Lcom/facebook/video/player/InlineVideoView;->h:LX/7LJ;

    iget-object v5, p0, Lcom/facebook/video/player/InlineVideoView;->i:LX/7LK;

    iget-object v6, p0, Lcom/facebook/video/player/InlineVideoView;->c:LX/1C2;

    iget-boolean v7, p0, Lcom/facebook/video/player/InlineVideoView;->m:Z

    iget-boolean v8, p0, Lcom/facebook/video/player/InlineVideoView;->s:Z

    if-nez v8, :cond_6

    move v8, v11

    :goto_1
    iget-object v9, p0, Lcom/facebook/video/player/InlineVideoView;->j:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/facebook/video/player/InlineVideoView;->j:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v9, v9, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-eqz v9, :cond_7

    move v9, v11

    :goto_2
    iget-object v10, p0, Lcom/facebook/video/player/InlineVideoView;->o:LX/04D;

    invoke-virtual/range {v0 .. v10}, LX/19P;->a(Landroid/content/Context;Landroid/util/AttributeSet;ILX/2pf;LX/2qI;LX/1C2;ZZZLX/04D;)LX/2q7;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    .line 1197911
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0}, LX/2q7;->q()LX/16V;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/player/InlineVideoView;->e:LX/16V;

    invoke-virtual {v0, v1}, LX/16V;->a(LX/16V;)V

    .line 1197912
    iget-boolean v0, p0, Lcom/facebook/video/player/InlineVideoView;->t:Z

    if-nez v0, :cond_4

    move v12, v11

    :cond_4
    invoke-static {v12}, LX/0PB;->checkState(Z)V

    .line 1197913
    const v0, 0x7f0d177e

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/InlineVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1197914
    iget-object v1, p0, Lcom/facebook/video/player/InlineVideoView;->x:LX/2p8;

    invoke-virtual {v1}, LX/2p8;->b()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1197915
    iget-object v1, p0, Lcom/facebook/video/player/InlineVideoView;->x:LX/2p8;

    invoke-virtual {v1, v0}, LX/2p8;->a(Landroid/view/ViewGroup;)V

    .line 1197916
    :cond_5
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    iget-object v1, p0, Lcom/facebook/video/player/InlineVideoView;->x:LX/2p8;

    invoke-interface {v0, v1}, LX/2q7;->a(LX/2p8;)V

    .line 1197917
    iput-boolean v11, p0, Lcom/facebook/video/player/InlineVideoView;->t:Z

    .line 1197918
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    iget-object v1, p0, Lcom/facebook/video/player/InlineVideoView;->o:LX/04D;

    invoke-interface {v0, v1}, LX/2q7;->a(LX/04D;)V

    .line 1197919
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->j:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_1

    .line 1197920
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->j:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/InlineVideoView;->setVideoData(Lcom/facebook/video/engine/VideoPlayerParams;)V

    goto/16 :goto_0

    :cond_6
    move v8, v12

    .line 1197921
    goto :goto_1

    :cond_7
    move v9, v12

    goto :goto_2
.end method

.method public static g(Lcom/facebook/video/player/InlineVideoView;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1197939
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    if-nez v0, :cond_1

    .line 1197940
    :cond_0
    :goto_0
    return-void

    .line 1197941
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->x:LX/2p8;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->x:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1197942
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->x:LX/2p8;

    invoke-virtual {v0}, LX/2p8;->a()V

    .line 1197943
    iput-boolean v1, p0, Lcom/facebook/video/player/InlineVideoView;->t:Z

    goto :goto_0

    .line 1197944
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0}, LX/2q7;->k()Landroid/view/View;

    move-result-object v1

    .line 1197945
    if-eqz v1, :cond_0

    .line 1197946
    :try_start_0
    const v0, 0x7f0d177e

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/InlineVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1197947
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 1197948
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/player/InlineVideoView;->t:Z

    .line 1197949
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->b:LX/19P;

    .line 1197950
    if-eqz v1, :cond_3

    .line 1197951
    const/4 p0, -0x1

    .line 1197952
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    iget-object v2, v0, LX/19P;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_5

    .line 1197953
    iget-object v2, v0, LX/19P;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1197954
    if-ne v2, v1, :cond_4

    .line 1197955
    :goto_2
    if-ltz v3, :cond_3

    .line 1197956
    iget-object v2, v0, LX/19P;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1197957
    :cond_3
    iget-object v2, v0, LX/19P;->d:Ljava/util/List;

    invoke-static {v2}, LX/13m;->a(Ljava/util/Collection;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1197958
    goto :goto_0

    .line 1197959
    :catch_0
    move-exception v0

    .line 1197960
    throw v0

    .line 1197961
    :cond_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_5
    move v3, p0

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/04g;)V
    .locals 1

    .prologue
    .line 1197964
    sget-object v0, LX/7K4;->a:LX/7K4;

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/InlineVideoView;->a(LX/04g;LX/7K4;)V

    .line 1197965
    return-void
.end method

.method public final a(LX/7K4;)V
    .locals 1

    .prologue
    .line 1197962
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    invoke-direct {p0, v0, p1}, Lcom/facebook/video/player/InlineVideoView;->a(LX/04g;LX/7K4;)V

    .line 1197963
    return-void
.end method

.method public final a(ZLX/04g;)V
    .locals 2

    .prologue
    .line 1197931
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "muteAudio: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1197932
    invoke-direct {p0}, Lcom/facebook/video/player/InlineVideoView;->f()V

    .line 1197933
    iget-boolean v0, p0, Lcom/facebook/video/player/InlineVideoView;->n:Z

    if-eqz v0, :cond_0

    .line 1197934
    const/4 p1, 0x0

    .line 1197935
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/video/player/InlineVideoView;->k:Z

    .line 1197936
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0, p1, p2}, LX/2q7;->a(ZLX/04g;)V

    .line 1197937
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1197938
    invoke-direct {p0}, Lcom/facebook/video/player/InlineVideoView;->d()Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1197929
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    invoke-direct {p0, v0}, Lcom/facebook/video/player/InlineVideoView;->b(LX/04g;)V

    .line 1197930
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1197926
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    if-nez v0, :cond_0

    .line 1197927
    const/4 v0, 0x0

    .line 1197928
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0}, LX/2q7;->i()Z

    move-result v0

    goto :goto_0
.end method

.method public final f_(I)V
    .locals 1

    .prologue
    .line 1197924
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/InlineVideoView;->a(ILX/04g;)V

    .line 1197925
    return-void
.end method

.method public getBufferPercentage()I
    .locals 1

    .prologue
    .line 1197923
    const/4 v0, 0x0

    return v0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 1197855
    invoke-direct {p0}, Lcom/facebook/video/player/InlineVideoView;->f()V

    .line 1197856
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0}, LX/2q8;->b()I

    move-result v0

    return v0
.end method

.method public getIsVideoCompleted()Z
    .locals 1

    .prologue
    .line 1197857
    iget-boolean v0, p0, Lcom/facebook/video/player/InlineVideoView;->l:Z

    return v0
.end method

.method public getLastStartPosition()I
    .locals 1

    .prologue
    .line 1197767
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0}, LX/2q7;->m()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSubtitles()LX/7QP;
    .locals 1

    .prologue
    .line 1197768
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0}, LX/2q7;->o()LX/7QP;

    move-result-object v0

    return-object v0
.end method

.method public getTrimStartPositionMs()I
    .locals 1

    .prologue
    .line 1197769
    const/4 v0, 0x0

    return v0
.end method

.method public getTypedEventBus()LX/16V;
    .locals 1

    .prologue
    .line 1197770
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->e:LX/16V;

    return-object v0
.end method

.method public getVideoPlayer()LX/2q7;
    .locals 1

    .prologue
    .line 1197771
    invoke-direct {p0}, Lcom/facebook/video/player/InlineVideoView;->f()V

    .line 1197772
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    return-object v0
.end method

.method public getVideoSourceType()LX/097;
    .locals 2

    .prologue
    .line 1197773
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->j:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->j:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1197774
    :cond_0
    const/4 v0, 0x0

    .line 1197775
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->j:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    goto :goto_0
.end method

.method public getVideoUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1197776
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->j:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->j:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1197777
    :cond_0
    const/4 v0, 0x0

    .line 1197778
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->j:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    goto :goto_0
.end method

.method public getVideoViewCurrentPosition()I
    .locals 1

    .prologue
    .line 1197779
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0}, LX/2q8;->b()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVideoViewDurationInMillis()I
    .locals 1

    .prologue
    .line 1197780
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0}, LX/2q7;->l()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x5dc89e26

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1197781
    const/16 v1, 0x2d

    const v2, -0x781122d0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x7576e193

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1197782
    iget-object v1, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    if-eqz v1, :cond_0

    .line 1197783
    invoke-static {p0}, Lcom/facebook/video/player/InlineVideoView;->g(Lcom/facebook/video/player/InlineVideoView;)V

    .line 1197784
    iget-object v1, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    .line 1197785
    iget-object v2, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v2}, LX/2q7;->q()LX/16V;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/video/player/InlineVideoView;->e:LX/16V;

    .line 1197786
    new-instance v5, Lcom/facebook/common/eventbus/TypedEventBus$4;

    invoke-direct {v5, v2, v3}, Lcom/facebook/common/eventbus/TypedEventBus$4;-><init>(LX/16V;LX/16V;)V

    invoke-static {v2, v5}, LX/16V;->a(LX/16V;Ljava/lang/Runnable;)V

    .line 1197787
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    .line 1197788
    invoke-interface {v1}, LX/2q7;->f()V

    .line 1197789
    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onDetachedFromWindow()V

    .line 1197790
    const/16 v1, 0x2d

    const v2, -0x2a99f542

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    .line 1197791
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->y:LX/7LL;

    sget-object v1, LX/7LL;->NONE:LX/7LL;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0}, LX/2q7;->k()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/facebook/video/player/InlineVideoView;->q:I

    if-lez v0, :cond_2

    iget v0, p0, Lcom/facebook/video/player/InlineVideoView;->r:I

    if-lez v0, :cond_2

    .line 1197792
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1197793
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1197794
    iget v2, p0, Lcom/facebook/video/player/InlineVideoView;->q:I

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/video/player/InlineVideoView;->r:I

    int-to-float v3, v3

    div-float v3, v2, v3

    .line 1197795
    int-to-float v2, v1

    int-to-float v4, v0

    div-float/2addr v2, v4

    .line 1197796
    iget-object v4, p0, Lcom/facebook/video/player/InlineVideoView;->y:LX/7LL;

    sget-object v5, LX/7LL;->CENTER_CROP:LX/7LL;

    if-ne v4, v5, :cond_0

    cmpg-float v4, v3, v2

    if-ltz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/facebook/video/player/InlineVideoView;->y:LX/7LL;

    sget-object v5, LX/7LL;->CENTER_INSIDE:LX/7LL;

    if-ne v4, v5, :cond_3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    :cond_1
    const/4 v2, 0x1

    .line 1197797
    :goto_0
    if-eqz v2, :cond_4

    .line 1197798
    int-to-float v0, v1

    div-float/2addr v0, v3

    float-to-int v0, v0

    move v2, v1

    move v1, v0

    .line 1197799
    :goto_1
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0}, LX/2q7;->k()Landroid/view/View;

    move-result-object v3

    .line 1197800
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1197801
    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1197802
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1197803
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1197804
    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1197805
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;->onMeasure(II)V

    .line 1197806
    return-void

    .line 1197807
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 1197808
    :cond_4
    int-to-float v1, v0

    mul-float/2addr v1, v3

    float-to-int v1, v1

    move v2, v1

    move v1, v0

    .line 1197809
    goto :goto_1
.end method

.method public setAlwaysPlayVideoUnmuted(Z)V
    .locals 0

    .prologue
    .line 1197810
    iput-boolean p1, p0, Lcom/facebook/video/player/InlineVideoView;->n:Z

    .line 1197811
    return-void
.end method

.method public setChannelEligibility(LX/04H;)V
    .locals 1

    .prologue
    .line 1197812
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    if-eqz v0, :cond_0

    .line 1197813
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(LX/04H;)V

    .line 1197814
    :cond_0
    return-void
.end method

.method public setIsVideoCompleted(Z)V
    .locals 2

    .prologue
    .line 1197815
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setIsVideoCompleted:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1197816
    iput-boolean p1, p0, Lcom/facebook/video/player/InlineVideoView;->l:Z

    .line 1197817
    return-void
.end method

.method public setOriginalPlayReason(LX/04g;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1197818
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    if-eqz v0, :cond_0

    .line 1197819
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->d(LX/04g;)V

    .line 1197820
    :cond_0
    return-void
.end method

.method public setPauseMediaPlayerOnVideoPause(Z)V
    .locals 2

    .prologue
    .line 1197821
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setPauseMediaPlayerOnVideoPause: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1197822
    iput-boolean p1, p0, Lcom/facebook/video/player/InlineVideoView;->m:Z

    .line 1197823
    return-void
.end method

.method public setPlayerOrigin(LX/04D;)V
    .locals 1

    .prologue
    .line 1197824
    iput-object p1, p0, Lcom/facebook/video/player/InlineVideoView;->o:LX/04D;

    .line 1197825
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    if-eqz v0, :cond_0

    .line 1197826
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(LX/04D;)V

    .line 1197827
    :cond_0
    return-void
.end method

.method public setPlayerType(LX/04G;)V
    .locals 1

    .prologue
    .line 1197828
    invoke-direct {p0}, Lcom/facebook/video/player/InlineVideoView;->f()V

    .line 1197829
    iput-object p1, p0, Lcom/facebook/video/player/InlineVideoView;->p:LX/04G;

    .line 1197830
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    if-eqz v0, :cond_0

    .line 1197831
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(LX/04G;)V

    .line 1197832
    :cond_0
    return-void
.end method

.method public setScaleType(LX/7LL;)V
    .locals 1

    .prologue
    .line 1197833
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->y:LX/7LL;

    if-eq p1, v0, :cond_0

    .line 1197834
    iput-object p1, p0, Lcom/facebook/video/player/InlineVideoView;->y:LX/7LL;

    .line 1197835
    invoke-virtual {p0}, Lcom/facebook/video/player/InlineVideoView;->requestLayout()V

    .line 1197836
    :cond_0
    return-void
.end method

.method public setSubtitleListener(LX/2qI;)V
    .locals 0

    .prologue
    .line 1197837
    iput-object p1, p0, Lcom/facebook/video/player/InlineVideoView;->w:LX/2qI;

    .line 1197838
    return-void
.end method

.method public setSubtitles(LX/7QP;)V
    .locals 1

    .prologue
    .line 1197839
    invoke-direct {p0}, Lcom/facebook/video/player/InlineVideoView;->f()V

    .line 1197840
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v0, p1}, LX/2q7;->a(LX/7QP;)V

    .line 1197841
    return-void
.end method

.method public setVideoData(Lcom/facebook/video/engine/VideoPlayerParams;)V
    .locals 11

    .prologue
    .line 1197842
    const-string v0, "InlineVideoView.setVideoData"

    const v1, 0x6352ee31

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1197843
    :try_start_0
    iget-object v0, p1, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    if-eqz v0, :cond_0

    .line 1197844
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setVideoData:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ", "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1197845
    :cond_0
    iput-object p1, p0, Lcom/facebook/video/player/InlineVideoView;->j:Lcom/facebook/video/engine/VideoPlayerParams;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1197846
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/video/player/InlineVideoView;->f()V

    .line 1197847
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, v2}, LX/2q7;->a(Lcom/facebook/video/engine/VideoPlayerParams;LX/2qi;Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1197848
    :goto_0
    const v0, 0x73e68d1b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1197849
    return-void

    .line 1197850
    :catch_0
    move-exception v9

    .line 1197851
    :try_start_2
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoView;->c:LX/1C2;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error setting video path. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/video/player/InlineVideoView;->p:LX/04G;

    iget-object v3, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v4, p1, Lcom/facebook/video/engine/VideoPlayerParams;->a:LX/0Px;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v6}, LX/2q7;->g()LX/04D;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/video/player/InlineVideoView;->u:LX/2q7;

    invoke-interface {v7}, LX/2q7;->r()Ljava/lang/String;

    move-result-object v7

    const/4 v10, 0x0

    move-object v8, p1

    invoke-virtual/range {v0 .. v10}, LX/1C2;->a(Ljava/lang/String;LX/04G;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;LX/04D;Ljava/lang/String;LX/098;Ljava/lang/Exception;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1197852
    :catchall_0
    move-exception v0

    const v1, 0x2dbdfcac

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public setVideoListener(LX/2pf;)V
    .locals 0

    .prologue
    .line 1197853
    iput-object p1, p0, Lcom/facebook/video/player/InlineVideoView;->v:LX/2pf;

    .line 1197854
    return-void
.end method
