.class public abstract Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:F

.field public final b:F


# direct methods
.method public constructor <init>(FF)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1197988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1197989
    cmpg-float v0, p1, p2

    if-gez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1197990
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1197991
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_2

    :goto_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1197992
    iput p1, p0, Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;->a:F

    .line 1197993
    iput p2, p0, Lcom/facebook/video/player/RichVideoPlayerScheduledRunnable;->b:F

    .line 1197994
    return-void

    :cond_0
    move v0, v2

    .line 1197995
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1197996
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1197997
    goto :goto_2
.end method
