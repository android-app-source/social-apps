.class public Lcom/facebook/video/player/VideoController;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/resources/ui/FbTextView;

.field private final b:Landroid/widget/LinearLayout;

.field private final c:Landroid/widget/LinearLayout;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/ImageButton;

.field private final f:Landroid/widget/ImageButton;

.field private final g:Landroid/widget/ImageView;

.field private final h:Landroid/widget/SeekBar;

.field public final i:Landroid/widget/TextView;

.field public final j:Landroid/widget/TextView;

.field public final k:Landroid/os/Handler;

.field public final l:LX/7Lc;

.field public m:LX/7L3;

.field public n:LX/7Kc;

.field public o:Z

.field public p:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/subtitles/controller/SubtitleAdapter;",
            ">;"
        }
    .end annotation
.end field

.field public q:I

.field private r:LX/04g;

.field private s:Z

.field public t:Z

.field public u:LX/7Kq;

.field public v:Z

.field private final w:Landroid/widget/SeekBar$OnSeekBarChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1198266
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/video/player/VideoController;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198267
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1198268
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/player/VideoController;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1198269
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1198270
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1198271
    iput-boolean v0, p0, Lcom/facebook/video/player/VideoController;->o:Z

    .line 1198272
    iput-boolean v0, p0, Lcom/facebook/video/player/VideoController;->s:Z

    .line 1198273
    iput-boolean v0, p0, Lcom/facebook/video/player/VideoController;->t:Z

    .line 1198274
    new-instance v0, LX/7Lb;

    invoke-direct {v0, p0}, LX/7Lb;-><init>(Lcom/facebook/video/player/VideoController;)V

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->w:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1198275
    const v0, 0x7f03072f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1198276
    const v0, 0x7f0d1330

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->b:Landroid/widget/LinearLayout;

    .line 1198277
    invoke-direct {p0}, Lcom/facebook/video/player/VideoController;->h()V

    .line 1198278
    const v0, 0x7f0d1332

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->c:Landroid/widget/LinearLayout;

    .line 1198279
    invoke-direct {p0}, Lcom/facebook/video/player/VideoController;->i()V

    .line 1198280
    const v0, 0x7f0d1331

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->d:Landroid/widget/ImageView;

    .line 1198281
    const v0, 0x7f0d1333

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->e:Landroid/widget/ImageButton;

    .line 1198282
    const v0, 0x7f0d1334

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->f:Landroid/widget/ImageButton;

    .line 1198283
    const v0, 0x7f0d0c13

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->g:Landroid/widget/ImageView;

    .line 1198284
    const v0, 0x7f0d132a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->h:Landroid/widget/SeekBar;

    .line 1198285
    const v0, 0x7f0d1329

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->i:Landroid/widget/TextView;

    .line 1198286
    const v0, 0x7f0d1335

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->j:Landroid/widget/TextView;

    .line 1198287
    const v0, 0x7f0d1336

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1198288
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->a:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/7LT;

    invoke-direct {v1, p0}, LX/7LT;-><init>(Lcom/facebook/video/player/VideoController;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1198289
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->d:Landroid/widget/ImageView;

    new-instance v1, LX/7LU;

    invoke-direct {v1, p0}, LX/7LU;-><init>(Lcom/facebook/video/player/VideoController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1198290
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->e:Landroid/widget/ImageButton;

    new-instance v1, LX/7LV;

    invoke-direct {v1, p0}, LX/7LV;-><init>(Lcom/facebook/video/player/VideoController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1198291
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->f:Landroid/widget/ImageButton;

    new-instance v1, LX/7LW;

    invoke-direct {v1, p0}, LX/7LW;-><init>(Lcom/facebook/video/player/VideoController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1198292
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->g:Landroid/widget/ImageView;

    new-instance v1, LX/7LX;

    invoke-direct {v1, p0}, LX/7LX;-><init>(Lcom/facebook/video/player/VideoController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1198293
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->h:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/facebook/video/player/VideoController;->w:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1198294
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->h:Landroid/widget/SeekBar;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1198295
    new-instance v0, LX/7Ld;

    invoke-direct {v0, p0}, LX/7Ld;-><init>(Lcom/facebook/video/player/VideoController;)V

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->k:Landroid/os/Handler;

    .line 1198296
    new-instance v0, LX/7Lc;

    invoke-direct {v0, p0}, LX/7Lc;-><init>(Lcom/facebook/video/player/VideoController;)V

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->l:LX/7Lc;

    .line 1198297
    new-instance v0, LX/7LY;

    invoke-direct {v0, p0}, LX/7LY;-><init>(Lcom/facebook/video/player/VideoController;)V

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->u:LX/7Kq;

    .line 1198298
    return-void
.end method

.method public static synthetic c(Lcom/facebook/video/player/VideoController;)I
    .locals 1

    .prologue
    .line 1198299
    invoke-direct {p0}, Lcom/facebook/video/player/VideoController;->m()I

    move-result v0

    return v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 1198300
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoController;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02152b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 1198301
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 1198302
    sget-object v1, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeX(Landroid/graphics/Shader$TileMode;)V

    .line 1198303
    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setGravity(I)V

    .line 1198304
    iget-object v1, p0, Lcom/facebook/video/player/VideoController;->b:Landroid/widget/LinearLayout;

    invoke-static {v1, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1198305
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1198306
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoController;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02152a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 1198307
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 1198308
    sget-object v1, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeX(Landroid/graphics/Shader$TileMode;)V

    .line 1198309
    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setGravity(I)V

    .line 1198310
    iget-object v1, p0, Lcom/facebook/video/player/VideoController;->c:Landroid/widget/LinearLayout;

    invoke-static {v1, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1198311
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/player/VideoController;->s:Z

    .line 1198312
    return-void
.end method

.method public static j(Lcom/facebook/video/player/VideoController;)V
    .locals 1

    .prologue
    .line 1198313
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoController;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1198314
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->n:LX/7Kc;

    invoke-interface {v0}, LX/7Kc;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1198315
    invoke-direct {p0}, Lcom/facebook/video/player/VideoController;->l()V

    .line 1198316
    :cond_0
    :goto_0
    return-void

    .line 1198317
    :cond_1
    invoke-direct {p0}, Lcom/facebook/video/player/VideoController;->k()V

    goto :goto_0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1198235
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->f:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1198236
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->e:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1198237
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1198318
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->e:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1198319
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->f:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1198320
    return-void
.end method

.method private m()I
    .locals 6

    .prologue
    .line 1198321
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoController;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/video/player/VideoController;->o:Z

    if-eqz v0, :cond_2

    .line 1198322
    :cond_0
    const/4 v0, 0x0

    .line 1198323
    :cond_1
    :goto_0
    return v0

    .line 1198324
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->n:LX/7Kc;

    invoke-interface {v0}, LX/7Kc;->getVideoViewCurrentPosition()I

    move-result v0

    .line 1198325
    if-lez v0, :cond_3

    .line 1198326
    iget-object v1, p0, Lcom/facebook/video/player/VideoController;->n:LX/7Kc;

    invoke-interface {v1}, LX/7Kc;->getTrimStartPositionMs()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1198327
    :cond_3
    iget-object v1, p0, Lcom/facebook/video/player/VideoController;->n:LX/7Kc;

    invoke-interface {v1}, LX/7Kc;->getVideoViewDurationInMillis()I

    move-result v1

    .line 1198328
    if-lt v1, v0, :cond_1

    .line 1198329
    iget-object v2, p0, Lcom/facebook/video/player/VideoController;->h:Landroid/widget/SeekBar;

    if-eqz v2, :cond_5

    .line 1198330
    if-lez v1, :cond_4

    .line 1198331
    const-wide/16 v2, 0x3e8

    int-to-long v4, v0

    mul-long/2addr v2, v4

    int-to-long v4, v1

    div-long/2addr v2, v4

    .line 1198332
    iget-object v4, p0, Lcom/facebook/video/player/VideoController;->h:Landroid/widget/SeekBar;

    long-to-int v2, v2

    invoke-virtual {v4, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1198333
    :cond_4
    iget-object v2, p0, Lcom/facebook/video/player/VideoController;->n:LX/7Kc;

    invoke-interface {v2}, LX/7Kc;->getBufferPercentage()I

    move-result v2

    .line 1198334
    iget-object v3, p0, Lcom/facebook/video/player/VideoController;->h:Landroid/widget/SeekBar;

    mul-int/lit8 v2, v2, 0xa

    invoke-virtual {v3, v2}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    .line 1198335
    :cond_5
    iget-object v2, p0, Lcom/facebook/video/player/VideoController;->j:Landroid/widget/TextView;

    if-eqz v2, :cond_6

    .line 1198336
    iget-object v2, p0, Lcom/facebook/video/player/VideoController;->j:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "-"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-int/2addr v1, v0

    int-to-long v4, v1

    invoke-static {v4, v5}, LX/7LQ;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1198337
    :cond_6
    iget-object v1, p0, Lcom/facebook/video/player/VideoController;->i:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 1198338
    iget-object v1, p0, Lcom/facebook/video/player/VideoController;->i:Landroid/widget/TextView;

    int-to-long v2, v0

    invoke-static {v2, v3}, LX/7LQ;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/04g;)V
    .locals 1

    .prologue
    .line 1198339
    sget-object v0, LX/7K4;->a:LX/7K4;

    invoke-virtual {p0, p1, v0}, Lcom/facebook/video/player/VideoController;->a(LX/04g;LX/7K4;)V

    .line 1198340
    return-void
.end method

.method public final a(LX/04g;LX/7K4;)V
    .locals 2

    .prologue
    .line 1198341
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/player/VideoController;->v:Z

    .line 1198342
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->m:LX/7L3;

    if-eqz v0, :cond_0

    .line 1198343
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->m:LX/7L3;

    invoke-virtual {v0, p1}, LX/7L3;->a(LX/04g;)V

    .line 1198344
    :cond_0
    invoke-direct {p0}, Lcom/facebook/video/player/VideoController;->l()V

    .line 1198345
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoController;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1198346
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->n:LX/7Kc;

    invoke-interface {v0, p2}, LX/7Kc;->a(LX/7K4;)V

    .line 1198347
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoController;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1198348
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->p:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2q9;

    invoke-virtual {v0}, LX/2q9;->b()V

    .line 1198349
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->m:LX/7L3;

    if-eqz v0, :cond_2

    .line 1198350
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->m:LX/7L3;

    invoke-virtual {v0, p1}, LX/7L3;->b(LX/04g;)V

    .line 1198351
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->k:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1198352
    return-void

    .line 1198353
    :cond_3
    iput-object p1, p0, Lcom/facebook/video/player/VideoController;->r:LX/04g;

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1198262
    if-eqz p1, :cond_0

    .line 1198263
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/video/player/VideoController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a004b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1198264
    :goto_0
    return-void

    .line 1198265
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/video/player/VideoController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0042

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1198354
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->n:LX/7Kc;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/04g;)V
    .locals 2

    .prologue
    .line 1198250
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/player/VideoController;->v:Z

    .line 1198251
    invoke-direct {p0}, Lcom/facebook/video/player/VideoController;->k()V

    .line 1198252
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoController;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1198253
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->n:LX/7Kc;

    invoke-interface {v0}, LX/7Kc;->b()V

    .line 1198254
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoController;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1198255
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->p:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2q9;

    invoke-virtual {v0}, LX/2q9;->c()V

    .line 1198256
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->k:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1198257
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->u:LX/7Kq;

    if-eqz v0, :cond_2

    .line 1198258
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->u:LX/7Kq;

    invoke-interface {v0}, LX/7Kq;->a()V

    .line 1198259
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->m:LX/7L3;

    if-eqz v0, :cond_3

    .line 1198260
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->m:LX/7L3;

    invoke-virtual {v0, p1}, LX/7L3;->c(LX/04g;)V

    .line 1198261
    :cond_3
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1198249
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->p:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->p:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 1198247
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoController;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/7LZ;

    invoke-direct {v1, p0}, LX/7LZ;-><init>(Lcom/facebook/video/player/VideoController;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1198248
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 1198244
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/VideoController;->setVisibility(I)V

    .line 1198245
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoController;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/7La;

    invoke-direct {v1, p0}, LX/7La;-><init>(Lcom/facebook/video/player/VideoController;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1198246
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1198239
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoController;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1198240
    invoke-virtual {p0}, Lcom/facebook/video/player/VideoController;->d()V

    .line 1198241
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->u:LX/7Kq;

    if-eqz v0, :cond_1

    .line 1198242
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->u:LX/7Kq;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/7Kq;->a(Z)V

    .line 1198243
    :cond_1
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1198238
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setChromeInteractionListener(LX/7Kq;)V
    .locals 0

    .prologue
    .line 1198233
    iput-object p1, p0, Lcom/facebook/video/player/VideoController;->u:LX/7Kq;

    .line 1198234
    return-void
.end method

.method public setCurrentTimeMs(I)V
    .locals 4

    .prologue
    .line 1198231
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->i:Landroid/widget/TextView;

    int-to-long v2, p1

    invoke-static {v2, v3}, LX/7LQ;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1198232
    return-void
.end method

.method public setListener(LX/7L3;)V
    .locals 0

    .prologue
    .line 1198229
    iput-object p1, p0, Lcom/facebook/video/player/VideoController;->m:LX/7L3;

    .line 1198230
    return-void
.end method

.method public setPlaying(Z)V
    .locals 0

    .prologue
    .line 1198226
    iput-boolean p1, p0, Lcom/facebook/video/player/VideoController;->v:Z

    .line 1198227
    invoke-static {p0}, Lcom/facebook/video/player/VideoController;->j(Lcom/facebook/video/player/VideoController;)V

    .line 1198228
    return-void
.end method

.method public setSkipSeekIfNoDuration(Z)V
    .locals 0

    .prologue
    .line 1198224
    iput-boolean p1, p0, Lcom/facebook/video/player/VideoController;->t:Z

    .line 1198225
    return-void
.end method

.method public setSubtitleAdapter(LX/2q9;)V
    .locals 1
    .param p1    # LX/2q9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1198222
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->p:Ljava/lang/ref/WeakReference;

    .line 1198223
    return-void
.end method

.method public setVideoController(LX/7Kc;)V
    .locals 1

    .prologue
    .line 1198217
    iput-object p1, p0, Lcom/facebook/video/player/VideoController;->n:LX/7Kc;

    .line 1198218
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->r:LX/04g;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1198219
    iget-object v0, p0, Lcom/facebook/video/player/VideoController;->r:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/VideoController;->a(LX/04g;)V

    .line 1198220
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/player/VideoController;->r:LX/04g;

    .line 1198221
    :cond_0
    return-void
.end method
