.class public final Lcom/facebook/video/player/FullScreenVideoPlayer$13;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/graphics/drawable/Drawable;

.field public final synthetic b:Lcom/facebook/video/player/FullScreenVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/FullScreenVideoPlayer;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1196689
    iput-object p1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer$13;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iput-object p2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer$13;->a:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 1196690
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer$13;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1196691
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer$13;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1196692
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer$13;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer$13;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->c(Lcom/facebook/video/player/FullScreenVideoPlayer;Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1196693
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer$13;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/video/player/FullScreenVideoPlayer;->K:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-static {v1, v0}, LX/1r0;->a(Landroid/widget/RelativeLayout$LayoutParams;Landroid/graphics/Rect;)V

    .line 1196694
    :goto_0
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer$13;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->setAlpha(F)V

    .line 1196695
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer$13;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->b(Lcom/facebook/video/player/FullScreenVideoPlayer;Z)V

    .line 1196696
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer$13;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    .line 1196697
    invoke-static {v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->s$redex0(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    .line 1196698
    return-void

    .line 1196699
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer$13;->b:Lcom/facebook/video/player/FullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method
