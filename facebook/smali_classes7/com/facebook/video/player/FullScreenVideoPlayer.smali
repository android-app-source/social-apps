.class public Lcom/facebook/video/player/FullScreenVideoPlayer;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final M:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final N:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Z

.field public B:I

.field public C:I

.field public D:Lcom/facebook/video/analytics/VideoPlayerInfo;

.field public E:Landroid/net/Uri;

.field public F:LX/097;

.field public G:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/video/engine/VideoDataSource;",
            ">;"
        }
    .end annotation
.end field

.field public H:I

.field public I:I

.field public J:I

.field public K:Landroid/widget/RelativeLayout$LayoutParams;

.field public L:LX/7Ks;

.field private O:Z

.field public P:Landroid/widget/ImageView;

.field private Q:Landroid/widget/ImageView;

.field public R:Landroid/view/WindowManager;

.field private S:Ljava/util/concurrent/Executor;

.field private T:LX/03V;

.field private U:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private V:LX/19P;

.field private W:Landroid/media/AudioManager;

.field public a:Landroid/widget/RelativeLayout;

.field private aA:Landroid/view/View;

.field private aB:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/animation/Animator$AnimatorListener;",
            ">;"
        }
    .end annotation
.end field

.field public aC:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/animation/Animator$AnimatorListener;",
            ">;"
        }
    .end annotation
.end field

.field private aD:Z

.field private aE:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private aF:I

.field private aG:F

.field public aH:LX/7LF;

.field public aI:LX/7Kb;

.field private aJ:LX/19Z;

.field private aK:LX/1bf;

.field private aL:LX/1bf;

.field private aM:LX/1Ad;

.field private aN:LX/0Yd;

.field private aO:LX/7Jt;

.field private aP:Z

.field private aQ:LX/3FK;

.field private aR:LX/19a;

.field private aS:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final aT:Landroid/animation/Animator$AnimatorListener;

.field private final aU:LX/7LG;

.field public final aV:LX/7Kx;

.field public final aW:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private aX:Landroid/animation/Animator$AnimatorListener;

.field public final aY:LX/7LE;

.field private final aZ:LX/7L3;

.field private aa:LX/7QP;

.field private ab:Z

.field private ac:Landroid/view/Window;

.field private ad:Landroid/view/WindowManager$LayoutParams;

.field private ae:LX/7LB;

.field public af:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/16I;",
            ">;"
        }
    .end annotation
.end field

.field public ag:LX/1CX;

.field public ah:Z

.field public ai:Z

.field private aj:Z

.field private ak:LX/7LH;

.field private al:LX/7LP;

.field private am:Z

.field private an:Landroid/view/ViewGroup;

.field public ao:Z

.field private ap:LX/7LC;

.field private final aq:LX/7LD;

.field public ar:LX/394;

.field public as:Landroid/media/MediaPlayer$OnCompletionListener;

.field private at:I

.field public au:LX/04g;

.field public av:I

.field private aw:LX/2q3;

.field public ax:LX/16V;

.field public ay:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public az:Landroid/view/View;

.field public b:LX/7Kd;

.field public c:Landroid/view/View;

.field public d:Lcom/facebook/video/player/VideoController;

.field public e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public f:Lcom/facebook/video/player/VideoSpecText;

.field public g:LX/0Sh;

.field public h:Lcom/facebook/video/subtitles/views/FbSubtitleView;

.field public i:Landroid/net/Uri;

.field public j:LX/7Kq;

.field public k:LX/0Aq;

.field public l:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

.field public m:LX/0J8;

.field public n:LX/0J7;

.field public o:LX/0JY;

.field public p:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

.field public q:LX/0Jd;

.field public r:LX/7Kr;

.field public s:LX/1C2;

.field public t:LX/0So;

.field public u:LX/03V;

.field public v:LX/7KR;

.field public w:Z

.field public x:Z

.field public y:Z

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1197453
    const-class v0, Lcom/facebook/video/player/FullScreenVideoPlayer;

    sput-object v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->M:Ljava/lang/Class;

    .line 1197454
    const-class v0, Lcom/facebook/video/player/FullScreenVideoPlayer;

    const-string v1, "video_cover"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/player/FullScreenVideoPlayer;->N:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1197413
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1197414
    new-instance v0, LX/0Aq;

    invoke-direct {v0}, LX/0Aq;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->k:LX/0Aq;

    .line 1197415
    new-instance v0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-direct {v0}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->l:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1197416
    new-instance v0, LX/0J8;

    invoke-direct {v0}, LX/0J8;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->m:LX/0J8;

    .line 1197417
    new-instance v0, LX/0J7;

    invoke-direct {v0}, LX/0J7;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->n:LX/0J7;

    .line 1197418
    new-instance v0, LX/0JY;

    invoke-direct {v0}, LX/0JY;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->o:LX/0JY;

    .line 1197419
    new-instance v0, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    invoke-direct {v0}, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->p:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    .line 1197420
    new-instance v0, LX/0Jd;

    invoke-direct {v0}, LX/0Jd;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->q:LX/0Jd;

    .line 1197421
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->w:Z

    .line 1197422
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ah:Z

    .line 1197423
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ai:Z

    .line 1197424
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aj:Z

    .line 1197425
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->x:Z

    .line 1197426
    new-instance v0, LX/7LH;

    invoke-direct {v0, p0}, LX/7LH;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ak:LX/7LH;

    .line 1197427
    iput-boolean v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->am:Z

    .line 1197428
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->z:Z

    .line 1197429
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->A:Z

    .line 1197430
    iput-boolean v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ao:Z

    .line 1197431
    iput v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->C:I

    .line 1197432
    new-instance v0, LX/7LC;

    invoke-direct {v0, p0}, LX/7LC;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ap:LX/7LC;

    .line 1197433
    new-instance v0, Lcom/facebook/video/analytics/VideoPlayerInfo;

    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    invoke-direct {v0, v1}, Lcom/facebook/video/analytics/VideoPlayerInfo;-><init>(LX/04G;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->D:Lcom/facebook/video/analytics/VideoPlayerInfo;

    .line 1197434
    new-instance v0, LX/7LD;

    invoke-direct {v0, p0}, LX/7LD;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aq:LX/7LD;

    .line 1197435
    sget-object v0, LX/097;->FROM_STREAM:LX/097;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->F:LX/097;

    .line 1197436
    iput v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->H:I

    .line 1197437
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->av:I

    .line 1197438
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aB:LX/0am;

    .line 1197439
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aC:LX/0am;

    .line 1197440
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aE:LX/0am;

    .line 1197441
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aG:F

    .line 1197442
    sget-object v0, LX/7LF;->NOP:LX/7LF;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aH:LX/7LF;

    .line 1197443
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aP:Z

    .line 1197444
    new-instance v0, LX/7Kw;

    invoke-direct {v0, p0}, LX/7Kw;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aT:Landroid/animation/Animator$AnimatorListener;

    .line 1197445
    new-instance v0, LX/7LG;

    invoke-direct {v0, p0}, LX/7LG;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aU:LX/7LG;

    .line 1197446
    new-instance v0, LX/7Ky;

    invoke-direct {v0, p0}, LX/7Ky;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aV:LX/7Kx;

    .line 1197447
    new-instance v0, LX/7Kz;

    invoke-direct {v0, p0}, LX/7Kz;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aW:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 1197448
    new-instance v0, LX/7L0;

    invoke-direct {v0, p0}, LX/7L0;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aX:Landroid/animation/Animator$AnimatorListener;

    .line 1197449
    new-instance v0, LX/7LE;

    invoke-direct {v0, p0}, LX/7LE;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aY:LX/7LE;

    .line 1197450
    new-instance v0, LX/7L3;

    invoke-direct {v0, p0}, LX/7L3;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aZ:LX/7L3;

    .line 1197451
    invoke-direct {p0, p1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->a(Landroid/content/Context;)V

    .line 1197452
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1197373
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1197374
    new-instance v0, LX/0Aq;

    invoke-direct {v0}, LX/0Aq;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->k:LX/0Aq;

    .line 1197375
    new-instance v0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-direct {v0}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->l:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1197376
    new-instance v0, LX/0J8;

    invoke-direct {v0}, LX/0J8;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->m:LX/0J8;

    .line 1197377
    new-instance v0, LX/0J7;

    invoke-direct {v0}, LX/0J7;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->n:LX/0J7;

    .line 1197378
    new-instance v0, LX/0JY;

    invoke-direct {v0}, LX/0JY;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->o:LX/0JY;

    .line 1197379
    new-instance v0, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    invoke-direct {v0}, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->p:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    .line 1197380
    new-instance v0, LX/0Jd;

    invoke-direct {v0}, LX/0Jd;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->q:LX/0Jd;

    .line 1197381
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->w:Z

    .line 1197382
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ah:Z

    .line 1197383
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ai:Z

    .line 1197384
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aj:Z

    .line 1197385
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->x:Z

    .line 1197386
    new-instance v0, LX/7LH;

    invoke-direct {v0, p0}, LX/7LH;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ak:LX/7LH;

    .line 1197387
    iput-boolean v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->am:Z

    .line 1197388
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->z:Z

    .line 1197389
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->A:Z

    .line 1197390
    iput-boolean v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ao:Z

    .line 1197391
    iput v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->C:I

    .line 1197392
    new-instance v0, LX/7LC;

    invoke-direct {v0, p0}, LX/7LC;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ap:LX/7LC;

    .line 1197393
    new-instance v0, Lcom/facebook/video/analytics/VideoPlayerInfo;

    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    invoke-direct {v0, v1}, Lcom/facebook/video/analytics/VideoPlayerInfo;-><init>(LX/04G;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->D:Lcom/facebook/video/analytics/VideoPlayerInfo;

    .line 1197394
    new-instance v0, LX/7LD;

    invoke-direct {v0, p0}, LX/7LD;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aq:LX/7LD;

    .line 1197395
    sget-object v0, LX/097;->FROM_STREAM:LX/097;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->F:LX/097;

    .line 1197396
    iput v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->H:I

    .line 1197397
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->av:I

    .line 1197398
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aB:LX/0am;

    .line 1197399
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aC:LX/0am;

    .line 1197400
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aE:LX/0am;

    .line 1197401
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aG:F

    .line 1197402
    sget-object v0, LX/7LF;->NOP:LX/7LF;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aH:LX/7LF;

    .line 1197403
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aP:Z

    .line 1197404
    new-instance v0, LX/7Kw;

    invoke-direct {v0, p0}, LX/7Kw;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aT:Landroid/animation/Animator$AnimatorListener;

    .line 1197405
    new-instance v0, LX/7LG;

    invoke-direct {v0, p0}, LX/7LG;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aU:LX/7LG;

    .line 1197406
    new-instance v0, LX/7Ky;

    invoke-direct {v0, p0}, LX/7Ky;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aV:LX/7Kx;

    .line 1197407
    new-instance v0, LX/7Kz;

    invoke-direct {v0, p0}, LX/7Kz;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aW:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 1197408
    new-instance v0, LX/7L0;

    invoke-direct {v0, p0}, LX/7L0;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aX:Landroid/animation/Animator$AnimatorListener;

    .line 1197409
    new-instance v0, LX/7LE;

    invoke-direct {v0, p0}, LX/7LE;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aY:LX/7LE;

    .line 1197410
    new-instance v0, LX/7L3;

    invoke-direct {v0, p0}, LX/7L3;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aZ:LX/7L3;

    .line 1197411
    invoke-direct {p0, p1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->a(Landroid/content/Context;)V

    .line 1197412
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1197333
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1197334
    new-instance v0, LX/0Aq;

    invoke-direct {v0}, LX/0Aq;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->k:LX/0Aq;

    .line 1197335
    new-instance v0, Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    invoke-direct {v0}, Lcom/facebook/video/analytics/VideoFeedStoryInfo;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->l:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1197336
    new-instance v0, LX/0J8;

    invoke-direct {v0}, LX/0J8;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->m:LX/0J8;

    .line 1197337
    new-instance v0, LX/0J7;

    invoke-direct {v0}, LX/0J7;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->n:LX/0J7;

    .line 1197338
    new-instance v0, LX/0JY;

    invoke-direct {v0}, LX/0JY;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->o:LX/0JY;

    .line 1197339
    new-instance v0, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    invoke-direct {v0}, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->p:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    .line 1197340
    new-instance v0, LX/0Jd;

    invoke-direct {v0}, LX/0Jd;-><init>()V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->q:LX/0Jd;

    .line 1197341
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->w:Z

    .line 1197342
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ah:Z

    .line 1197343
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ai:Z

    .line 1197344
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aj:Z

    .line 1197345
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->x:Z

    .line 1197346
    new-instance v0, LX/7LH;

    invoke-direct {v0, p0}, LX/7LH;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ak:LX/7LH;

    .line 1197347
    iput-boolean v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->am:Z

    .line 1197348
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->z:Z

    .line 1197349
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->A:Z

    .line 1197350
    iput-boolean v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ao:Z

    .line 1197351
    iput v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->C:I

    .line 1197352
    new-instance v0, LX/7LC;

    invoke-direct {v0, p0}, LX/7LC;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ap:LX/7LC;

    .line 1197353
    new-instance v0, Lcom/facebook/video/analytics/VideoPlayerInfo;

    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    invoke-direct {v0, v1}, Lcom/facebook/video/analytics/VideoPlayerInfo;-><init>(LX/04G;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->D:Lcom/facebook/video/analytics/VideoPlayerInfo;

    .line 1197354
    new-instance v0, LX/7LD;

    invoke-direct {v0, p0}, LX/7LD;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aq:LX/7LD;

    .line 1197355
    sget-object v0, LX/097;->FROM_STREAM:LX/097;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->F:LX/097;

    .line 1197356
    iput v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->H:I

    .line 1197357
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->av:I

    .line 1197358
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aB:LX/0am;

    .line 1197359
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aC:LX/0am;

    .line 1197360
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aE:LX/0am;

    .line 1197361
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aG:F

    .line 1197362
    sget-object v0, LX/7LF;->NOP:LX/7LF;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aH:LX/7LF;

    .line 1197363
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aP:Z

    .line 1197364
    new-instance v0, LX/7Kw;

    invoke-direct {v0, p0}, LX/7Kw;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aT:Landroid/animation/Animator$AnimatorListener;

    .line 1197365
    new-instance v0, LX/7LG;

    invoke-direct {v0, p0}, LX/7LG;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aU:LX/7LG;

    .line 1197366
    new-instance v0, LX/7Ky;

    invoke-direct {v0, p0}, LX/7Ky;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aV:LX/7Kx;

    .line 1197367
    new-instance v0, LX/7Kz;

    invoke-direct {v0, p0}, LX/7Kz;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aW:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 1197368
    new-instance v0, LX/7L0;

    invoke-direct {v0, p0}, LX/7L0;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aX:Landroid/animation/Animator$AnimatorListener;

    .line 1197369
    new-instance v0, LX/7LE;

    invoke-direct {v0, p0}, LX/7LE;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aY:LX/7LE;

    .line 1197370
    new-instance v0, LX/7L3;

    invoke-direct {v0, p0}, LX/7L3;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aZ:LX/7L3;

    .line 1197371
    invoke-direct {p0, p1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->a(Landroid/content/Context;)V

    .line 1197372
    return-void
.end method

.method private A()V
    .locals 2

    .prologue
    .line 1197331
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1197332
    return-void
.end method

.method private B()LX/7Le;
    .locals 3

    .prologue
    .line 1197330
    new-instance v0, LX/7Le;

    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Landroid/content/Context;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7Le;-><init>(LX/7Kd;Lcom/facebook/common/callercontext/CallerContext;)V

    return-object v0
.end method

.method private C()LX/7Jv;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1197299
    iget-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->y:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v0}, LX/7Kd;->a()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 1197300
    :goto_0
    iget-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->y:Z

    if-eqz v2, :cond_4

    move v2, v1

    .line 1197301
    :goto_1
    if-gez v2, :cond_5

    .line 1197302
    iget-object v3, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->u:LX/03V;

    sget-object v4, Lcom/facebook/video/player/FullScreenVideoPlayer;->M:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Found a negative current position %d"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1197303
    :goto_2
    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->k:LX/0Aq;

    .line 1197304
    iget v3, v2, LX/0Aq;->b:I

    move v2, v3

    .line 1197305
    if-gez v2, :cond_1

    .line 1197306
    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->u:LX/03V;

    sget-object v3, Lcom/facebook/video/player/FullScreenVideoPlayer;->M:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Found a negative last start position %d"

    iget-object v5, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->k:LX/0Aq;

    .line 1197307
    iget v6, v5, LX/0Aq;->b:I

    move v5, v6

    .line 1197308
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v1

    .line 1197309
    :cond_1
    if-le v2, v1, :cond_2

    move v2, v1

    .line 1197310
    :cond_2
    new-instance v3, LX/7Ju;

    invoke-direct {v3}, LX/7Ju;-><init>()V

    .line 1197311
    iput-boolean v0, v3, LX/7Ju;->a:Z

    .line 1197312
    move-object v0, v3

    .line 1197313
    iget-boolean v3, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->y:Z

    .line 1197314
    iput-boolean v3, v0, LX/7Ju;->b:Z

    .line 1197315
    move-object v0, v0

    .line 1197316
    iput v1, v0, LX/7Ju;->c:I

    .line 1197317
    move-object v0, v0

    .line 1197318
    iput v2, v0, LX/7Ju;->d:I

    .line 1197319
    move-object v0, v0

    .line 1197320
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->m:LX/0J8;

    .line 1197321
    iget-boolean v2, v1, LX/0J8;->a:Z

    move v1, v2

    .line 1197322
    iput-boolean v1, v0, LX/7Ju;->e:Z

    .line 1197323
    move-object v0, v0

    .line 1197324
    sget-object v1, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    .line 1197325
    iput-object v1, v0, LX/7Ju;->h:LX/04g;

    .line 1197326
    move-object v0, v0

    .line 1197327
    invoke-virtual {v0}, LX/7Ju;->a()LX/7Jv;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    .line 1197328
    goto :goto_0

    .line 1197329
    :cond_4
    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v2}, LX/7Kd;->getVideoViewCurrentPosition()I

    move-result v2

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method private E()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1197176
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->y:Z

    .line 1197177
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/VideoController;->a(LX/04g;)V

    .line 1197178
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    invoke-virtual {v0}, Lcom/facebook/video/player/VideoController;->e()V

    .line 1197179
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->x:Z

    .line 1197180
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->k()V

    .line 1197181
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1197283
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 1197284
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1197285
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 1197286
    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v2, v1, v0, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1197287
    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1197288
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1197289
    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 1197290
    invoke-static {v0, p0}, LX/1r0;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 9

    .prologue
    const/4 v1, -0x1

    .line 1197234
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1197235
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1197236
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0038

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->I:I

    .line 1197237
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a004f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->J:I

    .line 1197238
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->I:I

    invoke-direct {v0, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1197239
    const v0, 0x7f030730

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1197240
    const v0, 0x7f0d1337

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->a:Landroid/widget/RelativeLayout;

    .line 1197241
    const v0, 0x7f0d133e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->P:Landroid/widget/ImageView;

    .line 1197242
    const v0, 0x7f0d1327

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->c:Landroid/view/View;

    .line 1197243
    const v0, 0x7f0d1339

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/VideoController;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    .line 1197244
    const v0, 0x7f0d133f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/subtitles/views/FbSubtitleView;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->h:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    .line 1197245
    const v0, 0x7f0d133a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1197246
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->K:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1197247
    const v0, 0x7f0d133d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->Q:Landroid/widget/ImageView;

    .line 1197248
    const v0, 0x7f0d1340

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/VideoSpecText;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->f:Lcom/facebook/video/player/VideoSpecText;

    .line 1197249
    const-class v0, Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-static {v0, p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1197250
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->W:Landroid/media/AudioManager;

    .line 1197251
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->f:Lcom/facebook/video/player/VideoSpecText;

    iget-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ab:Z

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/VideoSpecText;->a(Z)V

    .line 1197252
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ac:Landroid/view/Window;

    .line 1197253
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/4 v3, 0x2

    const/16 v4, 0x308

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ad:Landroid/view/WindowManager$LayoutParams;

    .line 1197254
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ad:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1197255
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/7Kd;->setVideoViewClickable(Z)V

    .line 1197256
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    new-instance v1, LX/7L2;

    invoke-direct {v1, p0}, LX/7L2;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    invoke-interface {v0, v1}, LX/7Kd;->setVideoViewOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 1197257
    new-instance v0, LX/7L5;

    invoke-direct {v0, p0}, LX/7L5;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1197258
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    new-instance v1, LX/7L6;

    invoke-direct {v1, p0}, LX/7L6;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    invoke-interface {v0, v1}, LX/7Kd;->setVideoViewOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 1197259
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    new-instance v1, LX/7L7;

    invoke-direct {v1, p0}, LX/7L7;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    invoke-interface {v0, v1}, LX/7Kd;->setDelayedCompletionListener(LX/3FJ;)V

    .line 1197260
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    new-instance v1, LX/7L8;

    invoke-direct {v1, p0}, LX/7L8;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    invoke-interface {v0, v1}, LX/7Kd;->setVideoViewOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 1197261
    new-instance v0, LX/7LP;

    invoke-direct {v0, p1}, LX/7LP;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->al:LX/7LP;

    .line 1197262
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->al:LX/7LP;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ak:LX/7LH;

    .line 1197263
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v0, LX/7LP;->a:Ljava/lang/ref/WeakReference;

    .line 1197264
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->al:LX/7LP;

    invoke-interface {v0, v1}, LX/7Kd;->setVideoViewMediaController(Landroid/widget/MediaController;)V

    .line 1197265
    new-instance v0, LX/7L9;

    invoke-direct {v0, p0}, LX/7L9;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->j:LX/7Kq;

    .line 1197266
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->j:LX/7Kq;

    .line 1197267
    iput-object v1, v0, Lcom/facebook/video/player/VideoController;->u:LX/7Kq;

    .line 1197268
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->p()V

    .line 1197269
    new-instance v0, LX/7Kr;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->s:LX/1C2;

    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->k:LX/0Aq;

    iget-object v3, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->l:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    iget-object v4, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->m:LX/0J8;

    iget-object v5, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->n:LX/0J7;

    iget-object v6, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->o:LX/0JY;

    iget-object v7, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->p:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    iget-object v8, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->D:Lcom/facebook/video/analytics/VideoPlayerInfo;

    invoke-direct/range {v0 .. v8}, LX/7Kr;-><init>(LX/1C2;LX/0Aq;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/0J8;LX/0J7;LX/0JY;Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoPlayerInfo;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    .line 1197270
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    iget-boolean v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->O:Z

    .line 1197271
    iput-boolean v1, v0, LX/7Kr;->m:Z

    .line 1197272
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    new-instance v1, LX/09M;

    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->t:LX/0So;

    iget-object v3, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aR:LX/19a;

    iget-object v4, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aS:LX/0Ot;

    invoke-direct {v1, v2, v3, v4}, LX/09M;-><init>(LX/0So;LX/19a;LX/0Ot;)V

    .line 1197273
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/09M;

    iput-object v2, v0, LX/7Kr;->n:LX/09M;

    .line 1197274
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aZ:LX/7L3;

    .line 1197275
    iput-object v1, v0, Lcom/facebook/video/player/VideoController;->m:LX/7L3;

    .line 1197276
    sget-object v0, LX/1vY;->VIDEO:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1197277
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v0}, LX/7Kd;->c()Landroid/view/View;

    move-result-object v0

    sget-object v1, LX/1vY;->VIDEO:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1197278
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    sget-object v1, LX/1vY;->MEDIA_CONTROLS:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1197279
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->h:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    sget-object v1, LX/1vY;->SUBTITLE:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1197280
    new-instance v0, LX/7LB;

    invoke-direct {v0, p0}, LX/7LB;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ae:LX/7LB;

    .line 1197281
    return-void

    .line 1197282
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private a(Landroid/view/WindowManager;LX/0Sh;Ljava/util/concurrent/Executor;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/19P;Ljava/lang/Boolean;LX/0So;LX/1C2;LX/03V;LX/7KR;LX/2q3;LX/7Kb;LX/19Z;LX/0Ot;LX/1CX;LX/1Ad;LX/7Jt;LX/0ad;LX/0Uh;LX/19a;LX/0Ot;)V
    .locals 4
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/WindowManager;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/19P;",
            "Ljava/lang/Boolean;",
            "LX/0So;",
            "LX/1C2;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/7KR;",
            "LX/2q3;",
            "LX/7Kb;",
            "LX/19Z;",
            "LX/0Ot",
            "<",
            "LX/16I;",
            ">;",
            "LX/1CX;",
            "LX/1Ad;",
            "LX/7Jt;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/19a;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1197209
    iput-object p1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->R:Landroid/view/WindowManager;

    .line 1197210
    iput-object p2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->g:LX/0Sh;

    .line 1197211
    iput-object p3, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->S:Ljava/util/concurrent/Executor;

    .line 1197212
    iput-object p4, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->T:LX/03V;

    .line 1197213
    iput-object p5, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->U:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1197214
    iput-object p6, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->V:LX/19P;

    .line 1197215
    if-eqz p7, :cond_0

    invoke-virtual {p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    :goto_0
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ab:Z

    .line 1197216
    iput-object p8, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->t:LX/0So;

    .line 1197217
    iput-object p9, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->s:LX/1C2;

    .line 1197218
    iput-object p10, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->u:LX/03V;

    .line 1197219
    iput-object p11, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->v:LX/7KR;

    .line 1197220
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aI:LX/7Kb;

    .line 1197221
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aJ:LX/19Z;

    .line 1197222
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->af:LX/0Ot;

    .line 1197223
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ag:LX/1CX;

    .line 1197224
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aM:LX/1Ad;

    .line 1197225
    new-instance v2, LX/16V;

    invoke-direct {v2}, LX/16V;-><init>()V

    iput-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ax:LX/16V;

    .line 1197226
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aw:LX/2q3;

    .line 1197227
    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aw:LX/2q3;

    iget-object v3, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ax:LX/16V;

    invoke-virtual {v2, v3}, LX/2q3;->a(LX/16V;)V

    .line 1197228
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aO:LX/7Jt;

    .line 1197229
    new-instance v2, LX/3FK;

    move-object/from16 v0, p19

    move-object/from16 v1, p20

    invoke-direct {v2, v0, v1}, LX/3FK;-><init>(LX/0ad;LX/0Uh;)V

    iput-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aQ:LX/3FK;

    .line 1197230
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aR:LX/19a;

    .line 1197231
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aS:LX/0Ot;

    .line 1197232
    return-void

    .line 1197233
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 26

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v24

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/video/player/FullScreenVideoPlayer;

    invoke-static/range {v24 .. v24}, LX/0q4;->a(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-static/range {v24 .. v24}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static/range {v24 .. v24}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static/range {v24 .. v24}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static/range {v24 .. v24}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v24 .. v24}, LX/19P;->a(LX/0QB;)LX/19P;

    move-result-object v8

    check-cast v8, LX/19P;

    invoke-static/range {v24 .. v24}, LX/3Gf;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-static/range {v24 .. v24}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v10

    check-cast v10, LX/0So;

    invoke-static/range {v24 .. v24}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v11

    check-cast v11, LX/1C2;

    invoke-static/range {v24 .. v24}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static/range {v24 .. v24}, LX/7KR;->a(LX/0QB;)LX/7KR;

    move-result-object v13

    check-cast v13, LX/7KR;

    invoke-static/range {v24 .. v24}, LX/2q3;->a(LX/0QB;)LX/2q3;

    move-result-object v14

    check-cast v14, LX/2q3;

    invoke-static/range {v24 .. v24}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static/range {v24 .. v24}, LX/7Kb;->a(LX/0QB;)LX/7Kb;

    move-result-object v15

    check-cast v15, LX/7Kb;

    invoke-static/range {v24 .. v24}, LX/19X;->a(LX/0QB;)LX/19Z;

    move-result-object v16

    check-cast v16, LX/19Z;

    const/16 v17, 0x2cc

    move-object/from16 v0, v24

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {v24 .. v24}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v18

    check-cast v18, LX/1CX;

    invoke-static/range {v24 .. v24}, LX/1Ad;->a(LX/0QB;)LX/1Ad;

    move-result-object v19

    check-cast v19, LX/1Ad;

    const-class v20, LX/7Jt;

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/7Jt;

    invoke-static/range {v24 .. v24}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v21

    check-cast v21, LX/0ad;

    invoke-static/range {v24 .. v24}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v22

    check-cast v22, LX/0Uh;

    invoke-static/range {v24 .. v24}, LX/19a;->a(LX/0QB;)LX/19a;

    move-result-object v23

    check-cast v23, LX/19a;

    const/16 v25, 0x259

    invoke-static/range {v24 .. v25}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v24

    invoke-direct/range {v2 .. v24}, Lcom/facebook/video/player/FullScreenVideoPlayer;->a(Landroid/view/WindowManager;LX/0Sh;Ljava/util/concurrent/Executor;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/19P;Ljava/lang/Boolean;LX/0So;LX/1C2;LX/03V;LX/7KR;LX/2q3;LX/7Kb;LX/19Z;LX/0Ot;LX/1CX;LX/1Ad;LX/7Jt;LX/0ad;LX/0Uh;LX/19a;LX/0Ot;)V

    return-void
.end method

.method private b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1197204
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aE:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1197205
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aE:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1197206
    :goto_0
    return-object v0

    .line 1197207
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->a:Landroid/widget/RelativeLayout;

    invoke-static {v0}, LX/1r0;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1197208
    invoke-direct {p0, p1, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/video/player/FullScreenVideoPlayer;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1197198
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 1197199
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    invoke-interface {v0}, LX/7Kd;->c()Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->I:I

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1197200
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    invoke-interface {v0}, LX/7Kd;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 1197201
    if-nez p1, :cond_0

    .line 1197202
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->p()V

    .line 1197203
    :cond_0
    return-void
.end method

.method public static c(Lcom/facebook/video/player/FullScreenVideoPlayer;Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 1197195
    invoke-direct {p0, p1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1197196
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aE:LX/0am;

    .line 1197197
    return-object v0
.end method

.method private c(LX/04g;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1197182
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->x()V

    .line 1197183
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->setAlpha(F)V

    .line 1197184
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->I:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1197185
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 1197186
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->k()V

    .line 1197187
    invoke-static {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->t(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    .line 1197188
    invoke-static {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->w(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    .line 1197189
    iget-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ah:Z

    if-eqz v0, :cond_1

    .line 1197190
    iget-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ai:Z

    if-eqz v0, :cond_0

    .line 1197191
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->R:Landroid/view/WindowManager;

    invoke-interface {v0, p0}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 1197192
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ai:Z

    .line 1197193
    :cond_0
    iput-boolean v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ah:Z

    .line 1197194
    :cond_1
    return-void
.end method

.method private c(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1197468
    const/16 v1, -0x3ec

    if-eq p1, v1, :cond_0

    const/16 v1, -0x3ef

    if-eq p1, v1, :cond_0

    if-ne p1, v0, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->E:Landroid/net/Uri;

    invoke-static {v1}, LX/1m0;->f(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/04g;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1197535
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    invoke-interface {v0}, LX/7Kd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aY:LX/7LE;

    .line 1197536
    iget-boolean v2, v0, LX/7LE;->b:Z

    move v0, v2

    .line 1197537
    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1197538
    :goto_0
    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v2}, LX/7Kd;->getVideoViewCurrentPosition()I

    move-result v2

    .line 1197539
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->C()LX/7Jv;

    move-result-object v3

    .line 1197540
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v4

    invoke-interface {v4}, LX/7Kd;->b()V

    .line 1197541
    iget-object v4, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ax:LX/16V;

    new-instance v5, LX/2qT;

    sget-object v6, LX/7IB;->b:LX/7IB;

    invoke-direct {v5, v2, v6}, LX/2qT;-><init>(ILX/7IA;)V

    invoke-virtual {v4, v5}, LX/16V;->a(LX/1AD;)V

    .line 1197542
    iget-object v4, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    invoke-virtual {v4, v1}, Lcom/facebook/video/player/VideoController;->setPlaying(Z)V

    .line 1197543
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->h:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-virtual {v1}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->e()V

    .line 1197544
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->h:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-virtual {v1}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->b()V

    .line 1197545
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aJ:LX/19Z;

    iget-object v4, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->E:Landroid/net/Uri;

    invoke-static {v4}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v4

    invoke-virtual {v1, v4}, LX/19Z;->a(I)V

    .line 1197546
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aq:LX/7LD;

    invoke-virtual {v1, p1, v3}, LX/7LD;->a(LX/04g;LX/7Jv;)V

    .line 1197547
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    iget-boolean v3, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->z:Z

    invoke-virtual {v1, p1, v3, v0, v2}, LX/7Kr;->a(LX/04g;ZZI)V

    .line 1197548
    return-void

    :cond_0
    move v0, v1

    .line 1197549
    goto :goto_0
.end method

.method public static d(Lcom/facebook/video/player/FullScreenVideoPlayer;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 1197529
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->az:Landroid/view/View;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1197530
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->K:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getAnimationOrigin()Landroid/graphics/Rect;

    move-result-object v1

    invoke-static {v0, v1}, LX/1r0;->a(Landroid/widget/RelativeLayout$LayoutParams;Landroid/graphics/Rect;)V

    .line 1197531
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->K:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->K:Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v3, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->K:Landroid/widget/RelativeLayout$LayoutParams;

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1197532
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->requestLayout()V

    .line 1197533
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->g:LX/0Sh;

    new-instance v1, Lcom/facebook/video/player/FullScreenVideoPlayer$15;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/player/FullScreenVideoPlayer$15;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 1197534
    return-void
.end method

.method private d(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1197528
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->G:LX/0Px;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->G:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->H:I

    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->G:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    const/16 v1, -0x3ec

    if-eq p1, v1, :cond_0

    const/16 v1, -0x3ef

    if-eq p1, v1, :cond_0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(I)V
    .locals 2

    .prologue
    .line 1197524
    iput p1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aF:I

    .line 1197525
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->r()V

    .line 1197526
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->g:LX/0Sh;

    new-instance v1, Lcom/facebook/video/player/FullScreenVideoPlayer$20;

    invoke-direct {v1, p0}, Lcom/facebook/video/player/FullScreenVideoPlayer$20;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 1197527
    return-void
.end method

.method private getAnimationOrigin()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1197521
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->az:Landroid/view/View;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1197522
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->az:Landroid/view/View;

    invoke-static {v0}, LX/1r0;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1197523
    invoke-static {v0, p0}, LX/1r0;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method private getAnimationTarget()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1197518
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aA:Landroid/view/View;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1197519
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aA:Landroid/view/View;

    invoke-static {v0}, LX/1r0;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1197520
    invoke-static {v0, p0}, LX/1r0;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method private getCurrentPlaceholderPosition()Landroid/graphics/Rect;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1197512
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aU:LX/7LG;

    iget-boolean v1, v1, LX/7LG;->a:Z

    if-nez v1, :cond_1

    .line 1197513
    :cond_0
    :goto_0
    return-object v0

    .line 1197514
    :cond_1
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getTopLevelDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1197515
    if-eqz v1, :cond_0

    .line 1197516
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->a:Landroid/widget/RelativeLayout;

    invoke-static {v0}, LX/1r0;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1197517
    invoke-direct {p0, v1, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 1197506
    iget-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->am:Z

    if-nez v0, :cond_1

    .line 1197507
    :cond_0
    :goto_0
    return-void

    .line 1197508
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    if-eqz v0, :cond_0

    .line 1197509
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    invoke-interface {v0}, LX/7Kd;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->an:Landroid/view/ViewGroup;

    .line 1197510
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->an:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v1

    invoke-interface {v1}, LX/7Kd;->c()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1197511
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->am:Z

    goto :goto_0
.end method

.method public static q(Lcom/facebook/video/player/FullScreenVideoPlayer;)V
    .locals 2

    .prologue
    .line 1197463
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->P:Landroid/widget/ImageView;

    const v1, 0x7f020bbf

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1197464
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->u()V

    .line 1197465
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/VideoController;->b(LX/04g;)V

    .line 1197466
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->x:Z

    .line 1197467
    return-void
.end method

.method private r()V
    .locals 3

    .prologue
    .line 1197500
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->K:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1197501
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->K:Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1197502
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->K:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1197503
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->K:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1197504
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->requestLayout()V

    .line 1197505
    return-void
.end method

.method public static s$redex0(Lcom/facebook/video/player/FullScreenVideoPlayer;)V
    .locals 1

    .prologue
    .line 1197497
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->setAlpha(F)V

    .line 1197498
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->g()V

    .line 1197499
    return-void
.end method

.method private setVideoRenderRotation(F)V
    .locals 1

    .prologue
    .line 1197494
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v0}, LX/7Kd;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1197495
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v0, p1}, LX/7Kd;->setVideoViewRotation(F)V

    .line 1197496
    :cond_0
    return-void
.end method

.method public static t(Lcom/facebook/video/player/FullScreenVideoPlayer;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1197490
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 1197491
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    invoke-interface {v0}, LX/7Kd;->c()Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->J:I

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1197492
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    invoke-interface {v0}, LX/7Kd;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 1197493
    return-void
.end method

.method private u()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f000000    # 0.5f

    .line 1197484
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->P:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 1197485
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->P:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 1197486
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->P:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1197487
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->P:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1197488
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->P:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aT:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1197489
    return-void
.end method

.method private v()V
    .locals 4

    .prologue
    .line 1197477
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->setVisibility(I)V

    .line 1197478
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->setAlpha(F)V

    .line 1197479
    const/4 v0, 0x1

    invoke-static {v0}, LX/0PM;->a(I)Ljava/util/HashMap;

    move-result-object v0

    .line 1197480
    const-string v1, "android.intent.action.HEADSET_PLUG"

    new-instance v2, LX/7L1;

    invoke-direct {v2, p0}, LX/7L1;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1197481
    new-instance v1, LX/0Yd;

    invoke-direct {v1, v0}, LX/0Yd;-><init>(Ljava/util/Map;)V

    iput-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aN:LX/0Yd;

    .line 1197482
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aN:LX/0Yd;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1197483
    return-void
.end method

.method public static w(Lcom/facebook/video/player/FullScreenVideoPlayer;)V
    .locals 2

    .prologue
    .line 1197469
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aN:LX/0Yd;

    if-eqz v0, :cond_0

    .line 1197470
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aN:LX/0Yd;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1197471
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aN:LX/0Yd;

    .line 1197472
    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->setVisibility(I)V

    .line 1197473
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->p()V

    .line 1197474
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->V:LX/19P;

    invoke-virtual {v0}, LX/19P;->c()V

    .line 1197475
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->clearAnimation()V

    .line 1197476
    return-void
.end method

.method private x()V
    .locals 2

    .prologue
    .line 1197291
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ac:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1197292
    iget-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ah:Z

    if-eqz v0, :cond_2

    .line 1197293
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ad:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v1, v1, -0x401

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1197294
    iget-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ai:Z

    if-eqz v0, :cond_1

    .line 1197295
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->R:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ad:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, p0, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1197296
    :cond_0
    :goto_0
    return-void

    .line 1197297
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aj:Z

    goto :goto_0

    .line 1197298
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ac:Landroid/view/Window;

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method private y()V
    .locals 2

    .prologue
    .line 1197455
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ac:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1197456
    iget-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ah:Z

    if-eqz v0, :cond_2

    .line 1197457
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ad:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v1, v1, 0x400

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1197458
    iget-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ai:Z

    if-eqz v0, :cond_1

    .line 1197459
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->R:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ad:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, p0, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1197460
    :cond_0
    :goto_0
    return-void

    .line 1197461
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aj:Z

    goto :goto_0

    .line 1197462
    :cond_2
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ac:Landroid/view/Window;

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    goto :goto_0
.end method

.method private z()V
    .locals 2

    .prologue
    .line 1196929
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1196930
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1197059
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ae:LX/7LB;

    invoke-virtual {v0, v1}, LX/7LB;->removeMessages(I)V

    .line 1197060
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    invoke-virtual {v0}, Lcom/facebook/video/player/VideoController;->d()V

    .line 1197061
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ae:LX/7LB;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, LX/7LB;->sendEmptyMessageDelayed(IJ)Z

    .line 1197062
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1197056
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->v()V

    .line 1197057
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->g:LX/0Sh;

    new-instance v1, Lcom/facebook/video/player/FullScreenVideoPlayer$13;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/player/FullScreenVideoPlayer$13;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 1197058
    return-void
.end method

.method public final a(Z)V
    .locals 7

    .prologue
    .line 1197036
    if-eqz p1, :cond_0

    .line 1197037
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->s:LX/1C2;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->p:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    .line 1197038
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1197039
    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->L:LX/7Ks;

    invoke-virtual {v2}, LX/7Ks;->c()LX/162;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->L:LX/7Ks;

    invoke-virtual {v3}, LX/7Ks;->b()LX/04D;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->D:Lcom/facebook/video/analytics/VideoPlayerInfo;

    .line 1197040
    iget-object v5, v4, Lcom/facebook/video/analytics/VideoPlayerInfo;->a:LX/04G;

    move-object v4, v5

    .line 1197041
    iget-object v5, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v5}, LX/7Kd;->getVideoViewCurrentPosition()I

    move-result v5

    iget-object v6, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->L:LX/7Ks;

    invoke-virtual {v6}, LX/7Ks;->d()Z

    move-result v6

    invoke-virtual/range {v0 .. v6}, LX/1C2;->a(Ljava/lang/String;LX/0lF;LX/04D;LX/04G;IZ)LX/1C2;

    .line 1197042
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getCurrentMediaTimeResetable()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->C:I

    .line 1197043
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->E:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->i:Landroid/net/Uri;

    .line 1197044
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->E:Landroid/net/Uri;

    .line 1197045
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->at:I

    .line 1197046
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->l()V

    .line 1197047
    return-void

    .line 1197048
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->s:LX/1C2;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->p:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    .line 1197049
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1197050
    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->L:LX/7Ks;

    invoke-virtual {v2}, LX/7Ks;->c()LX/162;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->L:LX/7Ks;

    invoke-virtual {v3}, LX/7Ks;->b()LX/04D;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->D:Lcom/facebook/video/analytics/VideoPlayerInfo;

    .line 1197051
    iget-object v5, v4, Lcom/facebook/video/analytics/VideoPlayerInfo;->a:LX/04G;

    move-object v4, v5

    .line 1197052
    iget-object v5, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v5}, LX/7Kd;->getVideoViewCurrentPosition()I

    move-result v5

    iget-object v6, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->L:LX/7Ks;

    invoke-virtual {v6}, LX/7Ks;->d()Z

    move-result v6

    invoke-virtual/range {v0 .. v6}, LX/1C2;->b(Ljava/lang/String;LX/0lF;LX/04D;LX/04G;IZ)LX/1C2;

    goto :goto_0

    .line 1197053
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->L:LX/7Ks;

    .line 1197054
    iget-object v1, v0, LX/7Ks;->a:Landroid/net/Uri;

    move-object v0, v1

    .line 1197055
    goto :goto_1
.end method

.method public final a(LX/04g;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1197022
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v0}, LX/7Kd;->a()Z

    .line 1197023
    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    iget-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->y:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->B:I

    .line 1197024
    :goto_0
    iget-object v3, v2, LX/7Kr;->e:LX/0Aq;

    invoke-virtual {v3, v0}, LX/0Aq;->c(I)V

    .line 1197025
    iget-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->z:Z

    if-nez v0, :cond_1

    move v0, v1

    .line 1197026
    :goto_1
    return v0

    .line 1197027
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v0}, LX/7Kd;->getVideoViewCurrentPosition()I

    move-result v0

    goto :goto_0

    .line 1197028
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ac:Landroid/view/Window;

    if-eqz v0, :cond_2

    .line 1197029
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ac:Landroid/view/Window;

    const/16 v2, 0x80

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 1197030
    :cond_2
    iput-boolean v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->z:Z

    .line 1197031
    invoke-direct {p0, p1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->d(LX/04g;)V

    .line 1197032
    invoke-direct {p0, p1}, Lcom/facebook/video/player/FullScreenVideoPlayer;->c(LX/04g;)V

    .line 1197033
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    invoke-virtual {v0}, LX/7Kr;->a()V

    .line 1197034
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->v:LX/7KR;

    new-instance v1, LX/7KS;

    invoke-direct {v1}, LX/7KS;-><init>()V

    invoke-virtual {v0, v1}, LX/16V;->a(LX/1AD;)V

    .line 1197035
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final a(Landroid/media/MediaPlayer;II)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1196988
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->L:LX/7Ks;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->L:LX/7Ks;

    .line 1196989
    iget-boolean v1, v0, LX/7Ks;->b:Z

    move v0, v1

    .line 1196990
    if-eqz v0, :cond_1

    .line 1196991
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->E:Landroid/net/Uri;

    invoke-virtual {v0, v1, p2, p3}, LX/7Kr;->a(Landroid/net/Uri;II)V

    .line 1196992
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->L:LX/7Ks;

    .line 1196993
    iput-boolean v5, v0, LX/7Ks;->b:Z

    .line 1196994
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->i:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->E:Landroid/net/Uri;

    .line 1196995
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getCurrentMediaTimeResetable()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->C:I

    .line 1196996
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->l()V

    .line 1196997
    :cond_0
    :goto_0
    return v4

    .line 1196998
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aP:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aQ:LX/3FK;

    iget-boolean v0, v0, LX/3FK;->i:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    invoke-interface {v0}, LX/7Kd;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1196999
    :cond_2
    invoke-direct {p0, p2}, Lcom/facebook/video/player/FullScreenVideoPlayer;->c(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1197000
    iput-boolean v4, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aP:Z

    .line 1197001
    const-string v0, "FullScreen VideoView network error what=%d/extra=%d; url: %s retrying by bypassing VideoServer"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->E:Landroid/net/Uri;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1197002
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->T:LX/03V;

    sget-object v2, Lcom/facebook/video/player/FullScreenVideoPlayer;->M:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1197003
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aQ:LX/3FK;

    iget-boolean v0, v0, LX/3FK;->i:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    invoke-interface {v0}, LX/7Kd;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1197004
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getCurrentMediaTimeResetable()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->C:I

    .line 1197005
    iget v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->C:I

    if-nez v0, :cond_3

    .line 1197006
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    invoke-interface {v0}, LX/7Kd;->getSeekPosition()I

    move-result v0

    iput v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->C:I

    .line 1197007
    iget v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->C:I

    if-gez v0, :cond_3

    .line 1197008
    iput v5, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->C:I

    .line 1197009
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->g:LX/0Sh;

    new-instance v1, Lcom/facebook/video/player/FullScreenVideoPlayer$8;

    invoke-direct {v1, p0}, Lcom/facebook/video/player/FullScreenVideoPlayer$8;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1197010
    :cond_4
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->E:Landroid/net/Uri;

    invoke-static {v0}, LX/1m0;->e(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->E:Landroid/net/Uri;

    goto :goto_1

    .line 1197011
    :cond_5
    invoke-direct {p0, p2}, Lcom/facebook/video/player/FullScreenVideoPlayer;->d(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1197012
    iget v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->H:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->H:I

    .line 1197013
    iget v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->H:I

    if-gez v0, :cond_6

    .line 1197014
    iput v5, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->H:I

    .line 1197015
    :cond_6
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->G:LX/0Px;

    iget v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->H:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/engine/VideoDataSource;

    .line 1197016
    if-eqz v0, :cond_7

    .line 1197017
    iget-object v1, v0, Lcom/facebook/video/engine/VideoDataSource;->b:Landroid/net/Uri;

    iput-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->E:Landroid/net/Uri;

    .line 1197018
    iget-object v0, v0, Lcom/facebook/video/engine/VideoDataSource;->f:LX/097;

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->F:LX/097;

    .line 1197019
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->g:LX/0Sh;

    new-instance v1, Lcom/facebook/video/player/FullScreenVideoPlayer$9;

    invoke-direct {v1, p0}, Lcom/facebook/video/player/FullScreenVideoPlayer$9;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    goto/16 :goto_0

    .line 1197020
    :cond_7
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->E:Landroid/net/Uri;

    invoke-virtual {v0, v1, p2, p3}, LX/7Kr;->a(Landroid/net/Uri;II)V

    .line 1197021
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->S:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/video/player/FullScreenVideoPlayer$10;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/player/FullScreenVideoPlayer$10;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;Landroid/media/MediaPlayer;)V

    const v2, 0x24f80d6b

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1196985
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ae:LX/7LB;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/7LB;->removeMessages(I)V

    .line 1196986
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    invoke-virtual {v0}, Lcom/facebook/video/player/VideoController;->d()V

    .line 1196987
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1196982
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ae:LX/7LB;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/7LB;->removeMessages(I)V

    .line 1196983
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    invoke-virtual {v0}, Lcom/facebook/video/player/VideoController;->c()V

    .line 1196984
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1196971
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aD:Z

    .line 1196972
    invoke-static {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->t(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    .line 1196973
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->invalidate()V

    .line 1196974
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->j()V

    .line 1196975
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->a()V

    .line 1196976
    iget-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->x:Z

    if-nez v0, :cond_0

    .line 1196977
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->r()V

    .line 1196978
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->k()V

    .line 1196979
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->au:LX/04g;

    invoke-virtual {v0, v1}, LX/7Kr;->a(LX/04g;)V

    .line 1196980
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->g:LX/0Sh;

    new-instance v1, Lcom/facebook/video/player/FullScreenVideoPlayer$7;

    invoke-direct {v1, p0}, Lcom/facebook/video/player/FullScreenVideoPlayer$7;-><init>(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 1196981
    return-void
.end method

.method public final f()V
    .locals 10

    .prologue
    .line 1196952
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aI:LX/7Kb;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->p:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    .line 1196953
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1196954
    iget v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->B:I

    int-to-long v2, v2

    const/4 v4, 0x0

    .line 1196955
    if-eqz v1, :cond_0

    iget-object v5, v0, LX/7Kb;->d:Ljava/lang/String;

    if-nez v5, :cond_2

    .line 1196956
    :cond_0
    :goto_0
    move v0, v4

    .line 1196957
    if-nez v0, :cond_1

    .line 1196958
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    iget v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->B:I

    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-interface {v2}, LX/7Kd;->getVideoViewDurationInMillis()I

    invoke-virtual {v0, v1}, LX/7Kr;->a(I)V

    .line 1196959
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aI:LX/7Kb;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->p:Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    .line 1196960
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1196961
    iget v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->B:I

    int-to-long v2, v2

    .line 1196962
    iget-object v4, v0, LX/7Kb;->b:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    iput-wide v4, v0, LX/7Kb;->e:J

    .line 1196963
    iput-object v1, v0, LX/7Kb;->d:Ljava/lang/String;

    .line 1196964
    iput-wide v2, v0, LX/7Kb;->f:J

    .line 1196965
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->y:Z

    .line 1196966
    return-void

    .line 1196967
    :cond_2
    iget-object v5, v0, LX/7Kb;->d:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-wide v6, v0, LX/7Kb;->f:J

    cmp-long v5, v2, v6

    if-nez v5, :cond_0

    iget-object v5, v0, LX/7Kb;->b:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    iget-wide v8, v0, LX/7Kb;->e:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x32

    cmp-long v5, v6, v8

    if-gez v5, :cond_0

    .line 1196968
    iget-object v4, v0, LX/7Kb;->c:LX/03V;

    if-eqz v4, :cond_3

    .line 1196969
    iget-object v4, v0, LX/7Kb;->c:LX/03V;

    const-string v5, "Video"

    const-string v6, "Finished event happening too often"

    invoke-virtual {v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1196970
    :cond_3
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1196942
    iget-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aD:Z

    if-eqz v0, :cond_0

    .line 1196943
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->v()V

    .line 1196944
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->j()V

    .line 1196945
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->k()V

    .line 1196946
    invoke-static {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->t(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    .line 1196947
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->invalidate()V

    .line 1196948
    :goto_0
    return-void

    .line 1196949
    :cond_0
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->z()V

    .line 1196950
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->y()V

    .line 1196951
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->r()V

    goto :goto_0
.end method

.method public getCurrentMediaTime()I
    .locals 1

    .prologue
    .line 1196941
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    invoke-interface {v0}, LX/7Kd;->getVideoViewCurrentPosition()I

    move-result v0

    return v0
.end method

.method public getCurrentMediaTimeResetable()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1196936
    iget-boolean v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->y:Z

    if-nez v1, :cond_0

    .line 1196937
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v1

    invoke-interface {v1}, LX/7Kd;->getVideoViewCurrentPosition()I

    move-result v1

    .line 1196938
    if-gez v1, :cond_1

    .line 1196939
    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->u:LX/03V;

    sget-object v3, Lcom/facebook/video/player/FullScreenVideoPlayer;->M:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getCurrentMediaTimeResetable got a negative value "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1196940
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public getCurrentVolume()I
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 1196933
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->W:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 1196934
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->W:Landroid/media/AudioManager;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    .line 1196935
    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, v1

    return v0
.end method

.method public getDismissOnComplete()Z
    .locals 1

    .prologue
    .line 1196932
    iget-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ao:Z

    return v0
.end method

.method public getFullScreenListener()LX/394;
    .locals 1

    .prologue
    .line 1196931
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ar:LX/394;

    return-object v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 1196928
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    return v0
.end method

.method public getSubtitles()LX/7QP;
    .locals 1

    .prologue
    .line 1197121
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aa:LX/7QP;

    return-object v0
.end method

.method public getVideoView()LX/7Kd;
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 1197164
    const v0, 0x7f0d1338

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1197165
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    if-nez v1, :cond_0

    .line 1197166
    new-instance v1, LX/7Ke;

    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/7Ke;-><init>(Landroid/content/Context;)V

    .line 1197167
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1197168
    const/16 v3, 0xd

    invoke-virtual {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1197169
    invoke-interface {v1}, LX/7Kd;->c()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1197170
    invoke-interface {v1}, LX/7Kd;->c()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1197171
    iput-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    .line 1197172
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/VideoController;->setVideoController(LX/7Kc;)V

    .line 1197173
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aQ:LX/3FK;

    iget-boolean v1, v1, LX/3FK;->i:Z

    .line 1197174
    iput-boolean v1, v0, Lcom/facebook/video/player/VideoController;->t:Z

    .line 1197175
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->b:LX/7Kd;

    return-object v0
.end method

.method public final h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1197160
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->K:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1197161
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->K:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1197162
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->requestLayout()V

    .line 1197163
    return-void
.end method

.method public final i()V
    .locals 4

    .prologue
    .line 1197151
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->v()V

    .line 1197152
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->I:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0}, Landroid/graphics/drawable/ColorDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    .line 1197153
    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1197154
    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1197155
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1197156
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 1197157
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->b(Lcom/facebook/video/player/FullScreenVideoPlayer;Z)V

    .line 1197158
    return-void

    .line 1197159
    :array_0
    .array-data 4
        0x0
        0xff
    .end array-data
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 1197148
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1197149
    :goto_0
    return-void

    .line 1197150
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->c:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final k()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 1197143
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1197144
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1197145
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->Q:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 1197146
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->Q:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1197147
    :cond_1
    return-void
.end method

.method public final l()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1197131
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aw:LX/2q3;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->E:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/2q3;->a(Landroid/net/Uri;)V

    .line 1197132
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aw:LX/2q3;

    iget v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->at:I

    .line 1197133
    iput v1, v0, LX/2q3;->h:I

    .line 1197134
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->E:Landroid/net/Uri;

    invoke-interface {v0, v1}, LX/7Kd;->setVideoViewPath$48ad1708(Landroid/net/Uri;)V

    .line 1197135
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aJ:LX/19Z;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->E:Landroid/net/Uri;

    invoke-static {v1}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v1

    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->B()LX/7Le;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/19Z;->a(ILX/7IG;)V

    .line 1197136
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    iget v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->C:I

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/VideoController;->setCurrentTimeMs(I)V

    .line 1197137
    new-instance v0, LX/7K4;

    iget v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->C:I

    iget v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->C:I

    invoke-direct {v0, v1, v2}, LX/7K4;-><init>(II)V

    .line 1197138
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    sget-object v2, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/video/player/VideoController;->a(LX/04g;LX/7K4;)V

    .line 1197139
    iput-boolean v3, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->y:Z

    .line 1197140
    iput-boolean v3, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->x:Z

    .line 1197141
    iput-boolean v3, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aP:Z

    .line 1197142
    return-void
.end method

.method public final o()V
    .locals 2

    .prologue
    .line 1197128
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aJ:LX/19Z;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->E:Landroid/net/Uri;

    invoke-static {v1}, LX/1m0;->a(Landroid/net/Uri;)I

    move-result v1

    .line 1197129
    sget-object p0, LX/7IJ;->INSEEK:LX/7IJ;

    invoke-virtual {v0, v1, p0}, LX/19Z;->a(ILX/7IJ;)V

    .line 1197130
    return-void
.end method

.method public onClick()V
    .locals 2

    .prologue
    .line 1197063
    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v0

    invoke-interface {v0}, LX/7Kd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1197064
    invoke-static {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->q(Lcom/facebook/video/player/FullScreenVideoPlayer;)V

    .line 1197065
    :goto_0
    return-void

    .line 1197066
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->P:Landroid/widget/ImageView;

    const v1, 0x7f020bc0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1197067
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->u()V

    .line 1197068
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->E()V

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 1197122
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1197123
    iget-boolean v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->z:Z

    move v0, v0

    .line 1197124
    if-nez v0, :cond_1

    .line 1197125
    :cond_0
    :goto_0
    return-void

    .line 1197126
    :cond_1
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aF:I

    if-eq v0, v1, :cond_0

    .line 1197127
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->e(I)V

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x51cad5fe

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1197069
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onDetachedFromWindow()V

    .line 1197070
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ai:Z

    .line 1197071
    const/16 v1, 0x2d

    const v2, -0x5b11341b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1197116
    const-string v0, "FullScreenVideoPlayer.onMeasure"

    const v1, 0x4efa7ce

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1197117
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;->onMeasure(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1197118
    const v0, -0x2fd1f7a7

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1197119
    return-void

    .line 1197120
    :catchall_0
    move-exception v0

    const v1, 0x3d1d84b7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public setDismissOnComplete(Z)V
    .locals 0

    .prologue
    .line 1197114
    iput-boolean p1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ao:Z

    .line 1197115
    return-void
.end method

.method public setLogEnteringStartEvent(Z)V
    .locals 1

    .prologue
    .line 1197110
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    .line 1197111
    iget-object p0, v0, LX/7Kr;->h:LX/0J7;

    .line 1197112
    iput-boolean p1, p0, LX/0J7;->e:Z

    .line 1197113
    return-void
.end method

.method public setLogExitingPauseEvent(Z)V
    .locals 1

    .prologue
    .line 1197106
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->r:LX/7Kr;

    .line 1197107
    iget-object p0, v0, LX/7Kr;->h:LX/0J7;

    .line 1197108
    iput-boolean p1, p0, LX/0J7;->f:Z

    .line 1197109
    return-void
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0

    .prologue
    .line 1197104
    iput-object p1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->as:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 1197105
    return-void
.end method

.method public setPlaceholder(LX/7LF;)V
    .locals 3

    .prologue
    .line 1197086
    sget-object v0, LX/7LF;->ENTER_ANIMATE:LX/7LF;

    if-ne p1, v0, :cond_0

    .line 1197087
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ay:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1197088
    :cond_0
    iput-object p1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aH:LX/7LF;

    .line 1197089
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aK:LX/1bf;

    .line 1197090
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aL:LX/1bf;

    .line 1197091
    if-nez v1, :cond_2

    if-nez v0, :cond_2

    .line 1197092
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1197093
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->A()V

    .line 1197094
    return-void

    .line 1197095
    :cond_2
    if-eqz v0, :cond_3

    if-eqz v1, :cond_4

    .line 1197096
    iget-object v2, v1, LX/1bf;->b:Landroid/net/Uri;

    move-object v2, v2

    .line 1197097
    iget-object p1, v0, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, p1

    .line 1197098
    invoke-virtual {v2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1197099
    :cond_3
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aM:LX/1Ad;

    sget-object v2, Lcom/facebook/video/player/FullScreenVideoPlayer;->N:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aU:LX/7LG;

    invoke-virtual {v0, v2}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1197100
    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1197101
    iput-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aL:LX/1bf;

    goto :goto_0

    .line 1197102
    :cond_4
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aU:LX/7LG;

    iget-boolean v0, v0, LX/7LG;->a:Z

    if-eqz v0, :cond_1

    .line 1197103
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->aU:LX/7LG;

    invoke-virtual {v0}, LX/7LG;->b()V

    goto :goto_0
.end method

.method public setShouldLoopVideo(Z)V
    .locals 0

    .prologue
    .line 1197084
    iput-boolean p1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->A:Z

    .line 1197085
    return-void
.end method

.method public setSubtitles(LX/7QP;)V
    .locals 3
    .param p1    # LX/7QP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1197072
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/7QP;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1197073
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/VideoController;->setSubtitleAdapter(LX/2q9;)V

    .line 1197074
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->h:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-virtual {v0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->e()V

    .line 1197075
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->h:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-virtual {v0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->b()V

    .line 1197076
    :goto_0
    return-void

    .line 1197077
    :cond_1
    new-instance v0, LX/7QP;

    invoke-direct {v0, p1}, LX/7QP;-><init>(LX/7QP;)V

    .line 1197078
    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->h:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    iget-object v2, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->ap:LX/7LC;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->a(LX/2pw;LX/7QP;)V

    .line 1197079
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->d:Lcom/facebook/video/player/VideoController;

    iget-object v1, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->h:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    .line 1197080
    iget-object v2, v1, Lcom/facebook/video/subtitles/views/FbSubtitleView;->e:LX/2q9;

    move-object v1, v2

    .line 1197081
    invoke-virtual {v0, v1}, Lcom/facebook/video/player/VideoController;->setSubtitleAdapter(LX/2q9;)V

    .line 1197082
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->h:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-virtual {p0}, Lcom/facebook/video/player/FullScreenVideoPlayer;->getVideoView()LX/7Kd;

    move-result-object v1

    invoke-interface {v1}, LX/7Kd;->getVideoViewCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->c(I)V

    .line 1197083
    iget-object v0, p0, Lcom/facebook/video/player/FullScreenVideoPlayer;->h:Lcom/facebook/video/subtitles/views/FbSubtitleView;

    invoke-virtual {v0}, Lcom/facebook/video/subtitles/views/FbSubtitleView;->c()V

    goto :goto_0
.end method
